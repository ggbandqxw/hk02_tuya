/**
 ***************************************************************************************************
 *
 * @file    app_task.h
 *
 * @brief   Routes ALL messages to/from APP
 *
 * Copyright (C) Eker 2021
 *
 ***************************************************************************************************
 */

#ifndef __APP_TASK_H__
#define __APP_TASK_H__

/**
 ***************************************************************************************************
 * @addtogroup APP
 * @{
 ***************************************************************************************************
 */

/*
 * INCLUDE FILES
 ***************************************************************************************************
 */
#include "rwip_task.h"

/*
 * DEFINES
 ***************************************************************************************************
 */
// Number of APP Task Instances
#define APP_IDX_MAX     1

/*
 * ENUMERATIONS
 ***************************************************************************************************
 */
// States of APP task
enum appm_state
{
    // Initialization state
    APPM_INIT,
    // Database create state
    APPM_CREATE_DB,
    // Ready State
    APPM_READY,
    // Connected state
    APPM_CONNECTED,

    // Number of defined states.
    APPM_STATE_MAX
};

enum app_msg_id
{
    APPM_DUMMY_MSG = TASK_FIRST_MSG(TASK_ID_APP),
};

/// @} APP

#endif // __APP_TASK_H__
