/**
 ***************************************************************************************************
 *
 * @file app_common.c
 *
 * @brief some common function interface
 *
 * Copyright (C) Eker 2021
 *
 *
 ***************************************************************************************************
 */

/*
 * INCLUDE FILES
 ***************************************************************************************************
 */
#include "app.h"

/*
 * MACROS
 ***************************************************************************************************
 */

/*
 * TYPEDEFS
 ***************************************************************************************************
 */

/*
 * CONSTANTS
 ***************************************************************************************************
 */

/*
 * LOCAL VARIABLES
 ***************************************************************************************************
 */
static uint8_t enc_key[GAP_KEY_LEN] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
                                       0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
/*
 * GLOBAL VARIABLES
 ***************************************************************************************************
 */

/*
 * LOCAL FUNCTIONS
 ***************************************************************************************************
 */
/**
 ***************************************************************************************************
 * @brief 
 *
 * @param[in] 
 *
 * @return 
 ***************************************************************************************************
 */

/*
 * GLOBAL FUNCTIONS
 ***************************************************************************************************
 */
/**
 ***************************************************************************************************
 * @brief BT controller PHY request
 *
 * @param void
 *
 * @return void
 ***************************************************************************************************
 */
void app_phy_req(void)
{
    struct gapc_set_phy_cmd *cmd = KE_MSG_ALLOC(GAPC_SET_PHY_CMD,
                                                TASK_GAPC, TASK_APP,
                                                gapc_set_phy_cmd);
    cmd->operation = GAPC_SET_PHY;
    cmd->tx_phy    = GAP_PHY_LE_2MBPS;
    cmd->rx_phy    = GAP_PHY_LE_2MBPS;
    cmd->phy_opt   = GAPC_PHY_OPT_LE_CODED_ALL_RATES;
    ke_msg_send(cmd);
}

/**
 ***************************************************************************************************
 * @brief BLE packet size request
 *
 * @param void
 *
 * @return void
 ***************************************************************************************************
 */
void app_le_pkt_size_req(void)
{
    struct gapc_set_le_pkt_size_cmd *cmd = KE_MSG_ALLOC(GAPC_SET_LE_PKT_SIZE_CMD,
                                                        TASK_GAPC, TASK_APP,
                                                        gapc_set_le_pkt_size_cmd);
    cmd->operation = GAPC_SET_LE_PKT_SIZE;
    cmd->tx_octets = LE_MAX_OCTETS;
    cmd->tx_time   = LE_MAX_TIME;
    ke_msg_send(cmd);
}

/**
 ***************************************************************************************************
 * @brief MTU exchange request
 *
 * @param void
 *
 * @return void
 ***************************************************************************************************
 */
void app_mtu_exchange_req(void)
{
    struct gattc_exc_mtu_cmd *cmd = KE_MSG_ALLOC(GATTC_EXC_MTU_CMD,
                                                 TASK_GATTC, TASK_APP,
                                                 gattc_exc_mtu_cmd);
    cmd->operation = GATTC_MTU_EXCH;
    cmd->seq_num = 0;
    ke_msg_send(cmd);
}

/**
 ***************************************************************************************************
 * @brief Send request of HW AES data encryption
 *
 * @param[in] data: data to be encrypted, 16 bytes once
 *
 * @return void
 ***************************************************************************************************
 */
void app_aes_encrypt(uint8_t *data)
{
    struct gapm_use_enc_block_cmd *cmd = KE_MSG_ALLOC(GAPM_USE_ENC_BLOCK_CMD, TASK_GAPM, TASK_APP,
                                                      gapm_use_enc_block_cmd);
    cmd->operation = GAPM_USE_ENC_BLOCK;
    memcpy(cmd->operand_1, data, GAP_KEY_LEN);
    memcpy(cmd->operand_2, enc_key, GAP_KEY_LEN);
    ke_msg_send(cmd);
}

/**
 ***************************************************************************************************
 * @brief Enable or disable one frequecy single tone
 *
 * @param[in] enable: true -> enable, false -> disable
 * @param[in] freq: rf frequecy, 2402 - 2480 MHz
 *
 * @return void
 ***************************************************************************************************
 */
void app_rf_carrier_enable(bool enable, uint32_t freq)
{
    if(enable)
    {
        pmu_lowpower_prevent(PMU_LP_USER);

        pmu_ana_enable(enable, PMU_ANA_RF);

        CPM_ANA_CLK_ENABLE();

        // frequency
        REGW(&HS_DAIF->FREQ_CFG0, MASK_2REG(DAIF_FREQ_REG_ME, 1, DAIF_FREQ_REG_MO, freq));

        // SDM
        REGW(&HS_DAIF->PLL_CTRL1, MASK_2REG(DAIF_DIGI_DIN_BYPASS, 1, DAIF_DIGI_DIN_REG, 0));
        REGW(&HS_DAIF->PLL_CTRL2, MASK_2REG(DAIF_DIN_SDM_TX_ME, 1, DAIF_DATA_SYNC_BYPASS, 1));

        // TX
        REGW(&HS_DAIF->VCO_CTRL0, MASK_3REG(DAIF_TRX_DBG, 1, DAIF_RX_EN_MO, 0, DAIF_TX_EN_MO, 0));
        co_delay_10us(10);
        REGW(&HS_DAIF->VCO_CTRL0, MASK_3REG(DAIF_TRX_DBG, 1, DAIF_RX_EN_MO, 0, DAIF_TX_EN_MO, 1));

        CPM_ANA_CLK_RESTORE();
    }
    else
    {
        CPM_ANA_CLK_ENABLE();

        REGW(&HS_DAIF->FREQ_CFG0, MASK_1REG(DAIF_FREQ_REG_ME, 0));
        REGW(&HS_DAIF->PLL_CTRL1, MASK_2REG(DAIF_DIGI_DIN_BYPASS, 0, DAIF_DIGI_DIN_REG, 0));
        REGW(&HS_DAIF->PLL_CTRL2, MASK_2REG(DAIF_DIN_SDM_TX_ME, 0, DAIF_DATA_SYNC_BYPASS, 0));
        REGW(&HS_DAIF->VCO_CTRL0, MASK_3REG(DAIF_TRX_DBG, 0, DAIF_RX_EN_MO, 0, DAIF_TX_EN_MO, 0));

        CPM_ANA_CLK_RESTORE();

        co_delay_us(100);

        pmu_ana_enable(enable, PMU_ANA_RF);

        pmu_lowpower_allow(PMU_LP_USER);
    }
}
