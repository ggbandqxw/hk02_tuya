/**
 ***************************************************************************************************
 *
 * @file app.h
 *
 * @brief Application entry point
 *
 * Copyright (C) Eker 2021
 *
 *
 ***************************************************************************************************
 */

#ifndef __APP_H__
#define __APP_H__

#include <stdint.h>
#include "l2cm.h"
#include "co_utils.h"
#include "co_timer.h"
#include "gapc.h"
#include "gapc_task.h"
#include "gapm_task.h"
#include "gattc_task.h"
#include "rwapp_config.h"
#include "app_common.h"
#if (NVDS_SUPPORT)
#include "nvds.h"
#endif
#include "peripheral.h"
#include "app_adv.h"
#if (BLE_APP_ONMICRO_DFU)
#include "dfu.h"
#include "onmicro_dfu.h"
#include "app_onmicro_dfu.h"
#endif
#if (BLE_APP_PRIVACY)
#include "app_privacy.h"
#endif
#if (BLE_APP_SEC)
#include "app_sec.h"
#endif
#include "app_task.h"
#include "app_utils.h"
#if (BLE_APP_WHITE_LIST)
#include "app_whl.h"
#endif

#if (BLE_APP_CENTRAL_SUPPORT)
#include "prf.h"
#include "app_central.h"
#include "app_gattc.h"
#endif

#include "ble_tys.h"
#include "ble_tys_task.h"
#include "app_tys.h"



#define APP_DEVICE_NAME_MAX_LEN             20

#define APP_HANDLER(handler_name, msgid, param, dest, src) app_##handler_name##_handler(param)

#define APP_HANDLERS(subtask)    {&subtask##_msg_handler_list[0], ARRAY_LEN(subtask##_msg_handler_list)}

enum APP_EVENT
{
    APP_EVT_CONNECTED,
    APP_EVT_DISCONNECTED,
    APP_EVT_BONDED,
    APP_EVT_CONN_PARAM_UPDATED,
};
struct app_subtask_handlers
{
    // Pointer to the message handler table
    const struct ke_msg_handler *p_msg_handler_tab;
    // Number of messages handled
    uint16_t msg_cnt;
};

struct app_env_tag
{
    /// Connection handle
    uint16_t conhdl;
    /// Connection Index
    uint8_t conidx;

    /// Last initialized profile
    uint8_t next_svc;

    /// Device Name length
    uint8_t dev_name_len;
    /// Device Name
    uint8_t dev_name[APP_DEVICE_NAME_MAX_LEN];

    // BT Address
    struct gap_bdaddr addr;

    /// Local device IRK
    uint8_t loc_irk[16];

    /// Counter used to generate IRK
    uint8_t rand_cnt;

    struct gapc_conn_param conn_param;

    uint8_t latency_disabled;

#if (BLE_APP_CENTRAL_SUPPORT)
    struct app_central_tag central;
#endif
};
extern struct app_env_tag app_env;

extern void gapm_set_bdaddr(bd_addr_t *addr);

void appm_reg_svc_itf(void);
bool appm_add_svc(void);
void appm_update_param(uint8_t conidx, struct gapc_conn_param *conn_param);
void appm_disconnect(uint8_t conidx, uint8_t reason);

void lld_con_slave_latency_ignore(uint8_t link_id, bool ignore);

#if(BLE_APP_SEC)
void app_gap_set_local_irk(void);
#endif

#endif /* __APP_H__ */
