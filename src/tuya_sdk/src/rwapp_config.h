#ifndef __RWAPP_CONFIG_H__
#define __RWAPP_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

// <<< Use Configuration Wizard in Context Menu >>>

/**************************************************************************************************/
// <h> System Configuration
// <o> Internal DCDC
// <0=>Disable <1=>Enable
#define CONFIG_INTERNAL_DCDC                                    1

// <o> Use 32K in deep sleep
// <0=>Disable <1=>Enable
#define CONFIG_32K_IN_DEEP_SLEEP                                1

// <o> Default Ctune of 32M Xtal <0-31>
// <i> Value range: 0-31, unit: 0.75pF
#define XTAL32M_CTUNE_DEFAULT                                   15

// <o> 32K Clock Selection
// <0=>PMU_32K_SEL_RC <1=>PMU_32K_SEL_32768HZ_XTAL
#define CONFIG_32K_CLOCK                                        0

// <o> Sleep mode
// <0=>Disable <1=>Enable
#define CONFIG_SLEEP_MODE                                       1

// <q> Security
#define BLE_APP_SEC                                             0

// <q> Whitelist
#define BLE_APP_WHITE_LIST                                      0

// <q> Privacy
#define BLE_APP_PRIVACY                                         0

// <o> Tuya MTU <23-247>
#define TY_GAP_LE_MTU                                           247
// </h>

/**************************************************************************************************/
// <h> Device Information
// <s> Device Name
#define APP_DFLT_DEVICE_NAME                                    "Demo_name"
#define APP_DFLT_DEVICE_NAME_LEN                                (sizeof(APP_DFLT_DEVICE_NAME))

// <s> Bluetooth Address
// <i> If it has been written data in NVDS_TAG_BD_ADDRESS,
// <i> then the BT address will be the NVDS_TAG_BD_ADDRESS value.
#define LOCAL_DEFAULT_ADDR                                      "3C:5B:86:4D:23:DC"

// <o> Appearance
// <0=>BLE_APPEARANCE_UNKNOWN
// <384=>BLE_APPEARANCE_GENERIC_REMOTE_CONTROL
// <960=>BLE_APPEARANCE_GENERIC_HID
// <961=>BLE_APPEARANCE_HID_KEYBOARD
// <962=>BLE_APPEARANCE_HID_MOUSE
// <963=>BLE_APPEARANCE_HID_JOYSTICK
// <964=>BLE_APPEARANCE_HID_GAMEPAD
#define CONFIG_BT_DEVICE_APPEARANCE                             0
//</h>

/**************************************************************************************************/
// <h> Connection Parameters
// <i> Interval Time = interval * 1.25 ms * (APP_BLE_CONN_LATENCY+1)
// <i> Timeout Time = APP_BLE_CONN_TIMEOUT * 10 ms
// <o0> Min interval (unit: 1.25ms)
// <o1> Max interval (unit: 1.25ms)
// <o2> Slave Latency
// <o3> Super timeout (unit: 10ms)
#define APP_BLE_CONN_INTERVAL_MIN                               16  // unit: 1.25ms
#define APP_BLE_CONN_INTERVAL_MAX                               16  // unit: 1.25ms
#define APP_BLE_CONN_LATENCY                                    49
#define APP_BLE_CONN_TIMEOUT                                    500 // unit: 10ms
// </h>

/**************************************************************************************************/
// <h> Log Level
// <o> APP
// <0=>LOG_LEVEL_OFF  <1=>LOG_LEVEL_ERROR <2=>LOG_LEVEL_WARNING
// <3=>LOG_LEVEL_INFO <4=>LOG_LEVEL_DEBUG <5=>LOG_LEVEL_VERBOSE
// <i> app.c, app_task.c
#define APP_LOG_LEVEL_APP                                       2
// <o> ADV
// <0=>LOG_LEVEL_OFF  <1=>LOG_LEVEL_ERROR <2=>LOG_LEVEL_WARNING
// <3=>LOG_LEVEL_INFO <4=>LOG_LEVEL_DEBUG <5=>LOG_LEVEL_VERBOSE
// <i> app_adv.c
#define APP_LOG_LEVEL_ADV                                       3
// <o> Security
// <0=>LOG_LEVEL_OFF  <1=>LOG_LEVEL_ERROR <2=>LOG_LEVEL_WARNING
// <3=>LOG_LEVEL_INFO <4=>LOG_LEVEL_DEBUG <5=>LOG_LEVEL_VERBOSE
// <i> app_sec.c
#define APP_LOG_LEVEL_SEC                                       2
// <o> Tuya service
// <0=>LOG_LEVEL_OFF  <1=>LOG_LEVEL_ERROR <2=>LOG_LEVEL_WARNING
// <3=>LOG_LEVEL_INFO <4=>LOG_LEVEL_DEBUG <5=>LOG_LEVEL_VERBOSE
// <i> app_sec.c
#define APP_LOG_LEVEL_SEC                                       2
// </h>

// <<< end of configuration section >>>

#ifdef __cplusplus
}
#endif

#endif /* __RWAPP_CONFIG_H__ */
