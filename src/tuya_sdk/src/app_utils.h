#ifndef __APP_UTILS_H__
#define __APP_UTILS_H__

#include "gap.h"

enum
{
    UNIT_0_625_MS = 625,        /**< Number of microseconds in 0.625 milliseconds. */
    UNIT_1_25_MS  = 1250,       /**< Number of microseconds in 1.25 milliseconds. */
    UNIT_10_MS    = 10000       /**< Number of microseconds in 10 milliseconds. */
};

/**@brief Macro for converting milliseconds to ticks.
 *
 * @param[in] TIME          Number of milliseconds to convert.
 * @param[in] RESOLUTION    Unit to be converted to in [us/ticks].
 */
#define MSEC_TO_UNITS(TIME, RESOLUTION) (((TIME) * 1000) / (RESOLUTION))

int str_2_bdaddr(const char *str, bd_addr_t *addr);
uint8_t char_2_hex(const char c);
uint8_t checksum(uint8_t *pdata, uint16_t len);

#endif
