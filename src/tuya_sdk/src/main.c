/**
 ***************************************************************************************************
 *
 * @file main.c
 *
 * @brief Application entry point
 *
 * Copyright (C) Eker 2021
 *
 *
 ***************************************************************************************************
 */
#include "rwip.h"
#include "ty_ble.h"
#include "ty_system.h"
#include "tuya_ble_api.h"
#include "tuya_ble_log.h"
#include "tuya_ble_sdk_test.h"
#include "app.h"
#include "gpio_uart.h"
#if (TUYA_BLE_SPP_ENABLE)
#include "tuya_ble_app_uart_common_handler.h"
#endif

// #define POWER_DEBUG

#ifdef POWER_DEBUG
#include "sysdump.h"

static tuya_ble_timer_t dump_timer;

static void dump_timer_handler(tuya_ble_timer_t timer)
{
    sysdump();
}
#endif

static void app_gapm_reset(void)
{
    // Reset the stack
    struct gapm_reset_cmd *p_cmd = KE_MSG_ALLOC(GAPM_RESET_CMD,
                                                TASK_GAPM, TASK_APP,
                                                gapm_reset_cmd);

    p_cmd->operation = GAPM_RESET;

    ke_msg_send(p_cmd);
}

static void peripheral_init(void)
{
    gpio_open();
    gpio_uart_init();
}

static void hardware_init(void)
{	
    peripheral_init();
}



int main(void)
{
    ty_system_init(0);

    ty_ble_init();

    hardware_init();
    tuya_ble_sdk_demo_init();

#if TUYA_BLE_SDK_TEST
    tuya_ble_sdk_test_init();
    ty_system_init(1);
#endif

#if (TUYA_BLE_SPP_ENABLE)
    tuya_ble_uart_common_init();
#endif

    app_gapm_reset();

#ifdef POWER_DEBUG

    tuya_ble_timer_create(&dump_timer, 3000, TUYA_BLE_TIMER_REPEATED, dump_timer_handler);
    tuya_ble_timer_start(dump_timer);
#endif

    while(1)
    {
        ty_system_mainloop();
        rwip_schedule();
    }
}
