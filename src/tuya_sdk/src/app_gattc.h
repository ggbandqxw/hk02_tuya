/**
 ****************************************************************************************
 *
 * @file app_gattc.h
 *
 * @brief Application Module entry point
 *
 * Copyright (C) RivieraWaves 2009-2015
 *
 *
 ****************************************************************************************
 */

#ifndef APP_GATTC_H_
#define APP_GATTC_H_

/**
 ****************************************************************************************
 * @addtogroup APP
 * @ingroup RICOW
 *
 * @brief Application Module entry point
 *
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "rwip_config.h"     // SW configuration
#include <stdint.h>          // Standard Integer Definition
#include "ke_task.h"         // Kernel Task Definition
#include "co_debug.h"         // Kernel Task Definition
#include "gattc_task.h"

/*
 * STRUCTURES DEFINITION
 ****************************************************************************************
 */

/*
 * GLOBAL VARIABLES DECLARATIONS
 ****************************************************************************************
 */

/// Application environment

/// Table of message handlers
extern const struct app_subtask_handlers app_gattc_handlers;

/*
 * FUNCTIONS DECLARATION
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 *
 * Application Functions
 *
 ****************************************************************************************
 */
uint16_t app_gattc_send_data(uint8_t handle, uint8_t *pdata, uint16_t len);

uint16_t app_gattc_send_data_no_resp(uint8_t handle, uint8_t *pdata, uint16_t len);

void app_gattc_read_data_by_uuid(uint8_t *uuid, uint8_t uuid_len);

void app_gattc_read_data_by_uuid_ex(uint8_t conidx, struct gattc_read_by_uuid *param);

/**
 ****************************************************************************************
 * @brief Initialize Application Module
 ****************************************************************************************
 */

// Some other functions

/// @} APP

#endif // APP_CLIENT_H_
