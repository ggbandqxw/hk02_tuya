/**
 ***************************************************************************************************
 *
 * @file app_adv.h
 *
 * @brief Advertising application entry point
 *
 * Copyright (C) Eker 2021
 *
 *
 ***************************************************************************************************
 */
#ifndef __APP_ADV_H__
#define __APP_ADV_H__

#define ADV_DATA_MAX_LEN                31      // 31 bytes

typedef void (*timeout_cb_t)(void);

typedef struct
{
    // ADV data flag
    uint8_t adv_data_flag;

    // ADV data len
    uint8_t adv_data_len;
    // ADV data
    uint8_t adv_data[ADV_DATA_MAX_LEN];

    // response data len
    uint8_t res_data_len;
    // response data
    uint8_t res_data[ADV_DATA_MAX_LEN];

    // general ADV interval
    uint16_t adv_interval;
    // general ADV timeout
    uint16_t adv_timeout;

    // peer device address
    struct gap_bdaddr peer_addr;

    // directed ADV interval
    uint16_t dir_adv_interval;
    // directed ADV timeout
    uint16_t dir_adv_timeout;

    // ADV type
    uint8_t adv_type;

    // ADV rf power
    uint8_t adv_power;

    // ADV channel map, each bit of lowest 3 bits corresponds to a ADV channel (37, 38, 39)
    uint8_t channel_map;
} adv_cfg_t;

/**
 ***************************************************************************************************
 * The following functions are gapm request indicate or operation complete event handler.
 * User should not use these APIs, they are called in file app_task.c.
 ***************************************************************************************************
 */
void app_adv_created_ind_handler(struct gapm_activity_created_ind const *p_param);
void app_adv_stopped_ind_handler(struct gapm_activity_stopped_ind const *p_param);
void app_adv_cmp_evt_handler(struct gapm_cmp_evt const *p_param);

/**
 ***************************************************************************************************
 * The following functions are advertising control APIs.
 * User should use these APIs to implement other advertising logic if they want.
 ***************************************************************************************************
 */
void app_adv_start_gen(void);
void app_adv_start_dir(void);
void app_adv_stop(void);
void app_adv_set_timeout_cb(timeout_cb_t timeout_cb);

void app_adv_init(adv_cfg_t *adv_cfg);
void app_adv_set_adv_data_and_scan_resp_data(adv_cfg_t *adv_cfg);
void app_adv_set_adv_param(adv_cfg_t *adv_cfg);

#endif
