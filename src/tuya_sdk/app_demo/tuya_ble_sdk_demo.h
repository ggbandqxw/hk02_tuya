/**
****************************************************************************
* @file      tuya_ble_sdk_demo.h
* @brief     tuya_ble_sdk_demo
* @author    suding
* @version   V1.0.0
* @date      2020-04
* @note
******************************************************************************
* @attention
*
* <h2><center>&copy; COPYRIGHT 2020 Tuya </center></h2>
*/


#ifndef __TUYA_BLE_SDK_DEMO_H__
#define __TUYA_BLE_SDK_DEMO_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDE
 */
#include "stdint.h"

#define DT_RAW     0
#define DT_BOOL    1
#define DT_VALUE   2
#define DT_INT     DT_VALUE
#define DT_STRING  3
#define DT_ENUM    4
#define DT_BITMAP  5
#define DT_CHAR    7       // char类型（1字节）
#define DT_UCHAR   8       // unsigned char类型（1字节）
#define DT_SHORT   9       // char类型（2字节）
#define DT_USHORT  10      // char类型(2字节)
#define DT_LMT    DT_USHORT

#define DT_VALUE_LEN 4 // int
#define DT_BOOL_LEN 1
#define DT_ENUM_LEN 1
#define DT_BITMAP_MAX 4 // 1/2/4
#define DT_STR_MAX 255
#define DT_RAW_MAX 255
#define DT_INT_LEN DT_VALUE_LEN

//tuya_ble_sdk data type
#define APP_PORT_DT_RAW           DT_RAW    
#define APP_PORT_DT_BOOL          DT_BOOL   
#define APP_PORT_DT_VALUE         DT_VALUE  
#define APP_PORT_DT_INT           DT_INT    
#define APP_PORT_DT_STRING        DT_STRING 
#define APP_PORT_DT_ENUM          DT_ENUM   
#define APP_PORT_DT_BITMAP        DT_BITMAP 
#define APP_PORT_DT_CHAR          DT_CHAR   
#define APP_PORT_DT_UCHAR         DT_UCHAR  
#define APP_PORT_DT_SHORT         DT_SHORT  
#define APP_PORT_DT_USHORT        DT_USHORT 
#define APP_PORT_DT_LMT           DT_LMT    
//tuya_ble_sdk data type len
#define APP_PORT_DT_VALUE_LEN     DT_VALUE_LEN
#define APP_PORT_DT_BOOL_LEN      DT_BOOL_LEN
#define APP_PORT_DT_ENUM_LEN      DT_ENUM_LEN
#define APP_PORT_DT_BITMAP_MAX    DT_BITMAP_MAX
#define APP_PORT_DT_STR_MAX       DT_STR_MAX
#define APP_PORT_DT_RAW_MAX       DT_RAW_MAX
#define APP_PORT_DT_INT_LEN       DT_INT_LEN



#define  WR_BSC_OPEN_METH_CREATE                  1   //基础功能-添加开锁方式
#define  WR_BSC_OPEN_METH_DELETE                  2   //基础功能-删除开锁方式
#define  WR_BSC_OPEN_METH_MODIFY                  3   //基础功能-修改开锁方式
#define  WR_BSC_OPEN_METH_FREEZE                  4   //基础功能-冻结开锁方式
#define  WR_BSC_OPEN_METH_UNFREEZE                5   //基础功能-解冻开锁方式
#define  OW_BSC_OPEN_WITH_BT                      6   //基础功能-蓝牙开锁
#define  OR_BSC_OPEN_WITH_BT_REPORT               7   //基础功能-蓝牙开锁反馈，弃用
#define  OR_STS_BATTERY_PERCENT                   8   //状态同步-电量百分比
#define  OR_STS_BATTERY_POSITION                  9   //状态同步-电量档位
#define  OR_STS_CHILD_LOCK                        10  //状态同步-童锁
#define  OR_STS_ANTI_LOCK                         11  //状态同步-上提反锁
#define  OR_LOG_OPEN_WITH_FINGER                  12  //开锁记录-指纹
#define  OR_LOG_OPEN_WITH_PW                      13  //开锁记录-普通密码
#define  OR_LOG_OPEN_WITH_DYNAMIC_PW              14  //开锁记录-动态密码
#define  OR_LOG_OPEN_WITH_CARD                    15  //开锁记录-门卡
#define  OR_LOG_OPEN_WITH_KEY                     16  //开锁记录-机械钥匙
#define  OR_LOG_DOOR_STATE                        17  //开锁记录-门的状态    //?
#define  OR_LOG_OPEN_INSIDE                       18  //开锁记录-门从内侧打开
#define  OR_LOG_OPEN_WITH_BT                      19  //开锁记录-蓝牙开锁
//#define  OR_RFU_DOOR_OPENED                       20  //未被使用-门被打开
#define  WR_STS_CLOSE_RECORD                      20  //关锁记录
#define  OR_LOG_ALARM_REASON                      21  //警报记录-各种
#define  OR_LOG_ALARM_HIJACK                      22  //警报记录-劫持报警
#define  OR_RFU_ALARM_LOW_POWER                   23  //未被使用-低电报警
#define  OR_STS_DOORBELL_RING                     24  //状态同步-门铃提醒
#define  WR_SET_MESSAGE_SWITCH                    25  //锁的设置-短信通知
#define  WR_SET_DOOR_BELL                         26  //锁的设置-门铃音效
#define  WR_SET_LOCK_VOLUME                       27  //锁的设置-锁的音量
#define  WR_SET_LOCK_LANGUAGE                     28  //锁的设置-锁的语言切换
#define  WR_SET_WELCOME_WORDS                     29  //锁的设置-显示屏欢迎词
#define  WR_SET_KEY_TONE                          30  //锁的设置-按键音调
#define  WR_SET_NAVIGATE_VOLUME                   31  //锁的设置-锁的本地导航音量
#define  WR_STS_REVERSE_LOCK                      32  //状态同步-反锁状态
#define  WR_SET_AUTO_LOCK_SWITCH                  33  //锁的设置-自动落锁开关
#define  WR_SET_COMBINE_LOCK                      34  //锁的设置-单一开锁与组合开锁切换
#define  WR_BSC_OPEN_METH_SYNC                    35  //基础功能-同步开锁方式
#define  WR_SET_TIMER_LOCK                        36  //锁的设置-定时落锁
#define  WR_SET_TIMER_AUTO_LOCK                   37  //锁的设置-定时自动落锁
#define  WR_SET_FINGER_NUM                        38  //锁的设置-指纹录入次数
#define  OR_LOG_OPEN_WITH_FACE                    39  //开锁记录-人脸
#define  OR_STS_DOOR_STATE                        40  //状态同步-门开合状态
#define  OR_LOG_OPEN_WITH_EYE                     41  //开锁记录-虹膜
#define  OR_LOG_OPEN_WITH_PALM_PRINT              42  //开锁记录-掌纹
#define  OR_LOG_OPEN_WITH_FINGER_VEIN             43  //开锁记录-指静脉
#define  OR_STS_AUTH_LOCK_OUTTIME                 45  //状态同步-自动落锁倒计时
#define  WR_SET_HAND_LOCK                         46  //锁的设置-手动落锁
#define  OR_STS_LOCK_STATE                        47  //状态同步-锁开合状态
#define  WR_SET_MOTOR_DIRECTION                   48  //锁的设置-锁贴电机转动方向
#define  WR_SET_USER_FREEZE                       49  //锁的设置-冻结用户
#define  WR_SET_USER_UNFREEZE                     50  //锁的设置-解冻用户
#define  WR_BSC_TEMP_PW_CREAT                     51  //基础功能-添加临时密码
#define  WR_BSC_TEMP_PW_DELETE                    52  //基础功能-删除临时密码
#define  WR_BSC_TEMP_PW_MODIFY                    53  //基础功能-修改临时密码
#define  WR_BSC_OPEN_METH_SYNC_NEW                54  //基础功能-同步开锁方式（数据量大）
#define  OR_LOG_OPEN_WITH_TMP_PWD                 55  //开锁记录-临时密码
#define  WR_SET_MOTOR_TORQUE                      56  //锁的设置-电机扭力
#define  OR_LOG_OPEN_WITH_COMBINE                 57  //开锁记录-组合开锁
#define  WR_SET_AWAYHOME_ARMING_SWITCH            58  //锁的设置-离家布防
#define  WR_BSC_GUIDE_PAGE                        59  //基础功能-引导功能
#define  WR_BSC_OPEN_WITH_NOPWD_REMOTE_SETKEY     60  //基础功能-免密远程开锁配置
#define  WR_BSC_OPEN_WITH_NOPWD_REMOTE            61  //基础功能-免密远程开锁
#define  OR_LOG_OPEN_WITH_REMOTE_PHONE            62  //开锁记录-远程手机开锁
#define  OR_LOG_OPEN_WITH_REMOTE_VOICE            63  //开锁记录-远程音箱开锁
#define  WR_BSC_SET_T0                            64  //基础功能-离线密码-T0时间下发
#define  OR_LOG_ALARM_OFFLINE_PWD_CLEAR_SINGLE    65  //警报记录-离线密码-清除单条密码
#define  OR_LOG_ALARM_OFFLINE_PWD_CLEAR_ALL       66  //警报记录-离线密码-清除所有密码
#define  OR_LOG_OPEN_WITH_OFFLINE_PWD             67  //开锁记录-离线密码
#define  WR_SET_SPECIAL_FUNCTION                  68  //锁的设置-特殊功能
#define  OR_BSC_GET_OFFLINE_RECORD                69  //基础功能-获取门锁记录

#define  WR_BSC_SET_MASTER_SRAND_NUM              70  //基础功能-配置主机随机数
#define  WR_BSC_OPEN_WITH_MASTER                  71  //基础功能-开关锁
#define  OR_LOG_OPEN_WITH_COMMON                  72  //开锁记录-开关锁记录
#define  WR_BSC_OPEN_WITH_REMOTE_SETKEY           73  //基础功能-远程开锁配置

#define  OR_LOG_OPEN_WITH_COMBINE_NEW             74  //开锁记录-新组合开锁记录
#define  WR_SET_LOCK_VERIFY_SWITCH                75  //锁的设置-上锁校验开关
#define  WR_SET_MUTE_MODE_SWITCH                  76  //锁的设置-勿扰模式
#define  WR_SET_MUTE_MODE_TIME                    77  //锁的设置-勿扰
#define  WR_SET_SPECIAL_CONTROL                   78  //锁的设置-特殊控制

#define  WR_ANTILOST_FIND_DEVICE                  81  //防丢功能-寻找设备
#define  WR_ANTILOST_SOUND_VOLUME                 82  //防丢功能-音量
#define  WR_ANTILOST_SOUND_TYPE                   83  //防丢功能-类型
#define  WR_ANTILOST_FIND_HOST                    84  //防丢功能-寻找手机
#define  WR_ANTILOST_DISCONN_ALARM_SWITCH         85  //防丢功能-断连报警



//closedoor reason
typedef enum
{
CLOSE_DOOR_DEAULT=0,
CLOSE_DOOR_FAR_AWAY,//远程关锁
CLOSE_DOOR_REMOTE_VOICE,//远程语音关锁
CLOSE_DOOR_GEOFENCING,//地理围栏关锁
CLOSE_DOOR_BLE,//蓝牙关锁
CLOSE_DOOR_PARTS,	//门锁配件关锁
CLOSE_DOOR_AUTO,//自动关锁
CLOSE_DOOR_HAND,//本地手动关锁
} closedoor_reason_t;

/*********************************************************************
 * CONSTANTS
 */
typedef enum
{
    LOCK_TIMER_CONN_PARAM_UPDATE = 0x00,
    LOCK_TIMER_DELAY_REPORT,
    LOCK_TIMER_BONDING_CONN,
    LOCK_TIMER_RESET_WITH_DISCONN,
    LOCK_TIMER_CONN_MONITOR,
    LOCK_TIMER_APP_TEST_OUTTIME,
    LOCK_TIMER_APP_TEST_RESET_OUTTIME,
    LOCK_TIMER_ACTIVE_REPORT,
    LOCK_TIMER_RESET_WITH_DISCONN2,
    LOCK_TIMER_COMMUNICATION_MONITOR,
    LOCK_TIMER_IBEACON,
    LOCK_TUMER_MAX,
} lock_timer_t;


typedef enum
{
    DELAY_REPORT_TYPE_CREAT_SUB_REPORT = 0x00,
    DELAY_REPORT_TYPE_OPEN_RECORD_REPORT,
    DELAY_REPORT_TYPE_OPEN_RECORD_REPORT_COMMON,
} delay_report_t;

 //open method
typedef enum
{
    OPEN_METH_BASE = 0,
    OPEN_METH_PASSWORD,
    OPEN_METH_DOORCARD,
    OPEN_METH_FINGER,
    OPEN_METH_FACE,
    OPEN_METH_PALM, //掌纹
    OPEN_METH_VEIN, //指静脉
    OPEN_METH_TEMP_PW, //实际的临时密码值为0xF0，此处为方便存储演示，取值0x05
    OPEN_METH_MAX,
} open_meth_t;


//reg state
typedef enum
{
    REG_STAGE_STSRT    = 0x00,
    REG_STAGE_RUNNING  = 0xFC,
    REG_STAGE_FAILED   = 0xFD,
    REG_STAGE_CANCEL   = 0xFE,
    REG_STAGE_COMPLETE = 0xFF,
} reg_stage_t;


//reg failed reason
typedef enum
{
    REG_NOUSE_DEFAULT_VALUE  = 0x00,
    REG_FAILD_OUTTIME  = 0x00,
    REG_FAILD_FAILED   = 0x01,
    REG_FAILD_REPEAT   = 0x02,
    REG_FAILD_NO_HARDID  = 0x03,
    REG_FAILD_INVALID_PW  = 0x04,
    REG_FAILD_INVALID_PW_LEN  = 0x05,
    REG_FAILD_INVALID_OPEN_METH  = 0x06,
    REG_FAILD_FINGER_RUNNING  = 0x07,
    REG_FAILD_CARD_RUNNING  = 0x08,
    REG_FAILD_FACE_RUNNING  = 0x08,
    REG_FAILD_WONG_NUMBER  = 0xFE,
} reg_failed_reason_t;


typedef struct
{
    uint32_t len;
    uint8_t  value[];
} app_evt_data_t;

typedef struct 
{
    uint8_t evt;
    union 
    {
        app_evt_data_t data;
    };
} app_evt_param_t;



//open with master
typedef struct
{
    uint16_t masterid;
    uint16_t slaveid;
    uint8_t  srand[8];
    uint8_t  operation;
    uint32_t timestamp;
    uint8_t  open_meth;
    uint8_t  open_meth_info[256];
} open_with_master_t;

typedef struct
{
    uint16_t slaveid;
    uint16_t masterid;
    uint8_t  srand[8];
    uint8_t  operation;
    uint32_t timestamp;
    uint8_t  open_meth;
    uint8_t  result;
} open_with_master_result_t;

//open with bt
typedef struct
{
    uint8_t open;
    uint8_t memberid;
} open_meth_with_bt_t;
typedef struct
{
    uint8_t result;
    uint8_t memberid;
} open_meth_with_bt_result_t;

 //create open method
typedef struct
{
    open_meth_t meth;
    reg_stage_t stage;
    uint8_t admin_falg;
    uint8_t memberid;
    uint8_t hardid;
    uint8_t time[17];
    uint8_t valid_num;
    uint8_t password_len;
    uint8_t password[10];
} open_meth_creat_t;
typedef struct
{
    open_meth_t meth;
    reg_stage_t stage;
    uint8_t admin_falg;
    uint8_t memberid;
    uint8_t hardid;
    uint8_t reg_num;
    uint8_t result;
} open_meth_creat_result_t;


//delete open method
typedef struct
{
    open_meth_t meth;   
    reg_stage_t stage;  
    uint8_t admin_falg; 
    uint8_t memberid;   
    uint8_t hardid;     
    uint8_t delete_style;
} open_meth_delete_t;

typedef struct
{
    open_meth_t meth;   
    reg_stage_t stage;  
    uint8_t admin_falg; 
    uint8_t memberid;   
    uint8_t hardid;     
    uint8_t delete_style;
    uint8_t result;     
} open_meth_delete_result_t;

//modify open method
typedef struct
{
    open_meth_t meth;    
    reg_stage_t stage;   
    uint8_t admin_falg;  
    uint8_t memberid;    
    uint8_t hardid;      
    uint8_t time[17];    
    uint8_t cycle;       
    uint8_t password_len;
    uint8_t password[10];
} open_meth_modify_t;
typedef struct
{
    open_meth_t meth;
    reg_stage_t stage;
    uint8_t amin_falg;
    uint8_t memberid; 
    uint8_t hardid;   
    uint8_t cycle;    
    uint8_t result;   
} open_meth_modify_result_t;


/*********************************************************************
 * CONSTANT
 */
//PID - product id, DID - device id
//FIR - firmware, FVER - firmware version, HVER - hardware version
#define TY_DEVICE_NAME        "lock"


//#define TY_DEVICE_PID         "h62dimxw"
//#define TY_DEVICE_MAC         "DC234D865B3C"
//#define TY_DEVICE_DID         "tuya89c177a3235b" //16Bytes
//#define TY_DEVICE_AUTH_KEY    "n0KgVUqoPNdANo015tS2Rbl83KaXj5ZG" //32Bytes

#define TY_DEVICE_PID         "udljlhvk"
#define TY_DEVICE_MAC         "DC23510E9AC6"
#define TY_DEVICE_DID         "uuidfc5de53b28fd" //16Bytes
#define TY_DEVICE_AUTH_KEY    "39j0LMSgm0gdOokmOD8bb7CqnvQU4HE4" //32Bytes


#define TY_DEVICE_FIR_NAME    "tuya_ble_sdk_demo_hs6621"
#define TY_DEVICE_FVER_NUM    0x00000300
#define TY_DEVICE_FVER_STR    "3.0"
#define TY_DEVICE_HVER_NUM    0x00000100
#define TY_DEVICE_HVER_STR    "1.0"

#define TY_ADV_INTERVAL       100   //range: 20~10240ms
#define TY_CONN_INTERVAL_MIN  180   //range: 7.5~4000ms
#define TY_CONN_INTERVAL_MAX  200   //range: 7.5~4000ms

//event id
typedef enum {
    APP_EVT_0,
    APP_EVT_1,
    APP_EVT_2,
    APP_EVT_3,
    APP_EVT_4,
} custom_evtid_t;

/*********************************************************************
 * STRUCT
 */
#pragma pack(1)
typedef struct {
    uint8_t dp_id;
    uint8_t dp_type;
    uint16_t dp_data_len;  //3.x版本长度1字节；4.x两个字节+++
    uint8_t dp_data[256];
} demo_dp_t;

typedef struct {
    uint32_t len;
    uint8_t  value[];
} custom_evt_data_t;
#pragma pack()

typedef void (*tuya_ble_app_master_result_handler_t)(uint32_t evt, uint8_t* buf, uint32_t size);

/*********************************************************************
 * EXTERNAL VARIABLE
 */
extern demo_dp_t g_cmd;
extern demo_dp_t g_rsp;

/*********************************************************************
 * EXTERNAL FUNCTION
 */
void tuya_ble_sdk_demo_init(void);
void tuya_ble_custom_evt_send(custom_evtid_t evtid);
void tuya_ble_custom_evt_send_with_data(custom_evtid_t evtid, void* buf, uint32_t size);
void tuya_ble_disconnect_and_reset_timer_init(void);
void tuya_ble_update_conn_param_timer_init(void);
void tuya_ble_disconnect_and_reset_timer_start(void);
void tuya_ble_update_conn_param_timer_start(void);


#ifdef __cplusplus
}
#endif

#endif //__TUYA_BLE_SDK_DEMO_H__

