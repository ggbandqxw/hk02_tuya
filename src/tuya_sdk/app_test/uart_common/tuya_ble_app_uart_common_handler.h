/**
 * \file tuya_ble_app_uart_common_handler.h
 *
 * \brief
 */
/*
 *  Copyright (C) 2014-2019, Tuya Inc., All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  This file is part of tuya ble sdk
 */

#ifndef _TUYA_BLE_APP_UART_COMMON_HANDLER_H_
#define _TUYA_BLE_APP_UART_COMMON_HANDLER_H_

#include <stdint.h>
#include "tuya_ble_internal_config.h"

#ifdef __cplusplus
extern "C" {
#endif


/* COMMAND - General protocol */
#define UART_COMMON_CMD_HEARTBEAT                               0x00
#define UART_COMMON_CMD_GET_MCU_INFO                            0x01
#define UART_COMMON_CMD_GET_MODULE_WORK_MODE                    0x02
#define UART_COMMON_CMD_SEND_MODULE_WORK_STATE                  0x03
#define UART_COMMON_CMD_RESET_MODULE                            0x04
#define UART_COMMON_CMD_RESET_MODULE_NEW                        0x05
#define UART_COMMON_CMD_SEND_CMD                                0x06
#define UART_COMMON_CMD_REPORT_STATUS                           0x07
#define UART_COMMON_CMD_QUERY_STATUS                            0x08
#define UART_COMMON_CMD_UNBIND_MODULE                           0x09
#define UART_COMMON_CMD_GET_MODULE_CONN_STATE                   0x0A
#define UART_COMMON_CMD_GET_MODULE_VERSION                      0xA0
#define UART_COMMON_CMD_RESTORE_FACTORY                         0xA1
#define UART_COMMON_CMD_REPORT_RECORD_DATA                      0xE0
#define UART_COMMON_CMD_GET_TIME                                0xE1

/* COMMAND - OTA */
#define UART_COMMON_CMD_GET_MCU_VERSION                         0xE8
#define UART_COMMON_CMD_SEND_MCU_VERSION                        0xE9
#define UART_COMMON_CMD_OTA_REQUEST                             0xEA
#define UART_COMMON_CMD_OTA_FILE_INFO                           0xEB
#define UART_COMMON_CMD_OTA_FILE_OFFSET                         0xEC
#define UART_COMMON_CMD_OTA_DATA                                0xED
#define UART_COMMON_CMD_OTA_END                                 0xEE

/* COMMAND - MP test */
#define UART_COMMON_CMD_RF_TEST                                 0x0E

/* COMMAND - Low energy */
#define UART_COMMON_CMD_WAKEUP_MODULE                           0xB0
#define UART_COMMON_CMD_SET_WAKEUP_PIN                          0x0E
#define UART_COMMON_CMD_SET_SYS_TIMER                           0xE4
#define UART_COMMON_CMD_ENABLE_LOW_ENERGY                       0xE5

/* COMMAND - Extend */
#define UART_COMMON_CMD_REPORT_DP_WITH_FLAG                     0xA4
#define UART_COMMON_CMD_STORE_BIG_DATA                          0xB5

/* COMMAND - BLE */
#define UART_COMMON_CMD_START_ADV                               0xA3
#define UART_COMMON_CMD_REQUEST_ONLINE                          0xA5
#define UART_COMMON_CMD_SET_CONN_INTERVAL                       0xB1
#define UART_COMMON_CMD_HID                                     0xBA
#define UART_COMMON_CMD_SET_ADV_NAME                            0xBB
#define UART_COMMON_CMD_CHANGE_ADV_INTERVAL                     0xE2
#define UART_COMMON_CMD_DISCONN_BLE                             0xE7

/* COMMAND - Lock */
#define UART_COMMON_CMD_LOCK_OFFLINE_PWD                        0xA2
#define UART_COMMON_CMD_LOCK_SETTING                            0xA6
#define UART_COMMON_CMD_LOCK_CHECK_DYNAMIC_PWD                  0xA7
#define UART_COMMON_CMD_LOCK_IBEACON                            0xA8
#define UART_COMMON_CMD_LOCK_DYNAMIC_PWD                        0xE6


void tuya_ble_uart_common_init(void);

void tuya_ble_uart_common_mcu_ota_data_from_ble_handler(uint16_t cmd, uint8_t * recv_data, uint32_t recv_len);

void tuya_ble_uart_common_process(uint8_t * p_in_data, uint16_t in_len);

void tuya_ble_common_uart_send_cmd(uint8_t *pdata, uint8_t len);

#ifdef __cplusplus
}
#endif

#endif // _TUYA_BLE_APP_UART_COMMON_HANDLER_H_
