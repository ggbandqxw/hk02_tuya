#include "ty_flash.h"
#include "peripheral.h"

/*********************************************************************
 * CONSTANT
 */
            
/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * VARIABLE
 */

/*********************************************************************
 * FUNCTION
 */
uint32_t ty_flash_init(void)
{
    return 0;
}

uint32_t ty_flash_read(uint32_t addr, uint8_t* buf, uint32_t size)
{
    sfs_enable();
    sfs_read(addr, buf, size);
    return 0;
}

uint32_t ty_flash_write(uint32_t addr, const uint8_t* buf, uint32_t size)
{
    sfs_enable();
    sfs_write(addr, buf, size);
    return 0;
}

uint32_t ty_flash_erase(uint32_t addr, uint32_t num)
{
    sfs_enable();
    sfs_erase(addr, num);
    return 0;
}
