#include "ty_adc.h"
#include "peripheral.h"

/*********************************************************************
 * CONSTANT
 */

/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * VARIABLE
 */

/*********************************************************************
 * FUNCTION
 */
uint32_t ty_adc_init(ty_adc_t* p_adc)
{
    if (p_adc->channel > 7)
    {
        TY_PRINTF("Error: ADC channel must be less than 7.");
        return -1;
    }
    return 0;
}

uint32_t ty_adc_start(ty_adc_t* p_adc)
{
    if (p_adc->channel > 7)
    {
        TY_PRINTF("Error: ADC channel must be less than 7.");
        return -1;
    }
    switch (p_adc->channel)
    {
        case 0:
            pinmux_config(8, PINMUX_ANALOG_CH0_PIN8_CFG);
            p_adc->value = adc_battery_voltage_read_by_single_pin(
                            ADC_CHANNEL_EXTERN_CH0, ADC_PGA_GAIN_0P125, NULL, 10);
            break;

        case 1:
            pinmux_config(9, PINMUX_ANALOG_CH1_PIN9_CFG);
            p_adc->value = adc_battery_voltage_read_by_single_pin(
                            ADC_CHANNEL_EXTERN_CH1, ADC_PGA_GAIN_0P125, NULL, 10);
            break;

        case 2:
            pinmux_config(2, PINMUX_ANALOG_CH2_PIN2_CFG);
            p_adc->value = adc_battery_voltage_read_by_single_pin(
                            ADC_CHANNEL_EXTERN_CH2, ADC_PGA_GAIN_0P125, NULL, 10);
            break;

        case 3:
            pinmux_config(3, PINMUX_ANALOG_CH3_PIN3_CFG);
            p_adc->value = adc_battery_voltage_read_by_single_pin(
                            ADC_CHANNEL_EXTERN_CH3, ADC_PGA_GAIN_0P125, NULL, 10);
            break;

        case 4:
            pinmux_config(10, PINMUX_ANALOG_CH4_PIN10_CFG);
            p_adc->value = adc_battery_voltage_read_by_single_pin(
                            ADC_CHANNEL_EXTERN_CH4, ADC_PGA_GAIN_0P125, NULL, 10);
            break;

        case 5:
            pinmux_config(11, PINMUX_ANALOG_CH5_PIN11_CFG);
            p_adc->value = adc_battery_voltage_read_by_single_pin(
                            ADC_CHANNEL_EXTERN_CH5, ADC_PGA_GAIN_0P125, NULL, 10);
            break;

        case 6:
            pinmux_config(12, PINMUX_ANALOG_CH6_PIN12_CFG);
            p_adc->value = adc_battery_voltage_read_by_single_pin(
                            ADC_CHANNEL_EXTERN_CH6, ADC_PGA_GAIN_0P125, NULL, 10);
            break;

        case 7:
            pinmux_config(7, PINMUX_ANALOG_CH7_PIN7_CFG);
            p_adc->value = adc_battery_voltage_read_by_single_pin(
                            ADC_CHANNEL_EXTERN_CH7, ADC_PGA_GAIN_0P125, NULL, 10);
            break;

        default:
            break;
    }
    return 0;
}

uint32_t ty_adc_stop(ty_adc_t* p_adc)
{
    return 0;
}

uint32_t ty_adc_control(ty_adc_t* p_adc, uint8_t cmd, void* arg)
{
    return 0;
}

uint32_t ty_adc_uninit(ty_adc_t* p_adc)
{
    return 0;
}
