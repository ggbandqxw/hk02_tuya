#include "ty_uart.h"
#include "tuya_ble_api.h"
#include "peripheral.h"
#include "uart.h"


/*********************************************************************
 * CONSTANT
 */
#define UART0_BAUDRATE          115200
#define UART1_BAUDRATE          115200

#define PIN_UART0_TX            5
#define PIN_UART0_RX            6

#define PIN_UART1_TX            0
#define PIN_UART1_RX            1

/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * VARIABLE
 */

/*********************************************************************
 * VARIABLE
 */

/*********************************************************************
 * FUNCTION
 */
static void uart0_rx_cb(uint8_t data)
{
}

static void uart1_rx_cb(uint8_t data)
{
    uint8_t rx_char = data;
    tuya_ble_common_uart_receive_data(&rx_char, 1);
}

uint32_t ty_uart_init(void)
{
    pinmux_config(PIN_UART1_TX, PINMUX_UART1_SDA_O_CFG);
    pinmux_config(PIN_UART1_RX, PINMUX_UART1_SDA_I_CFG);
    uart_open(HS_UART1, UART1_BAUDRATE, UART_FLOW_CTRL_DISABLED, uart1_rx_cb);
    return 0;
}

uint32_t ty_uart2_init(void)
{
    pinmux_config(PIN_UART0_TX, PINMUX_UART0_SDA_O_CFG);
    pinmux_config(PIN_UART0_RX, PINMUX_UART0_SDA_I_CFG);
    uart_open(HS_UART0, UART0_BAUDRATE, UART_FLOW_CTRL_DISABLED, uart0_rx_cb);
    return 0;
}

uint32_t ty_uart_send(const uint8_t* buf, uint32_t size)
{
    uart_send_block(HS_UART1, buf, size);
    return 0;
}

uint32_t ty_uart2_send(const uint8_t* buf, uint32_t size)
{
    uart_send_block(HS_UART0, buf, size);
    return 0;
}

uint32_t ty_uart_uninit(void)
{
    uart_close(HS_UART1);
    pinmux_config(PIN_UART1_TX, PINMUX_GPIO_MODE_CFG);
    pinmux_config(PIN_UART1_RX, PINMUX_GPIO_MODE_CFG);
    return 0;
}

uint32_t ty_uart2_uninit(void)
{
    uart_close(HS_UART0);
    pinmux_config(PIN_UART0_TX, PINMUX_GPIO_MODE_CFG);
    pinmux_config(PIN_UART0_RX, PINMUX_GPIO_MODE_CFG);
    return 0;
}




