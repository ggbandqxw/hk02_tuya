#include "ty_rtc.h"
#include "peripheral.h"

/*********************************************************************
 * CONSTANT
 */
#define LOC_TIME_ZONE               8

/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * VARIABLE
 */

/*********************************************************************
 * FUNCTION
 */
/*********************************************************
FN: 
*/
static void ty_rtc_handler(const struct tm * time)
{
    // TY_PRINTF(">>> %s", __FUNCTION__);
}

/*********************************************************
FN: 
*/
uint32_t ty_rtc_init(void)
{
    rtc_init();
    return 0;
}

/*********************************************************
FN: 
*/
uint32_t ty_rtc_set_time(uint32_t timestamp)
{
    struct tm loc_time;
    loc_time = *localtime((const time_t*)&timestamp);
    loc_time.tm_hour += LOC_TIME_ZONE;
    // TY_PRINTF(">>> set time: %s, timestamp: %d", asctime(&loc_time), timestamp);
    rtc_time_set(&loc_time, ty_rtc_handler);
    return 0;
}

/*********************************************************
FN: 
*/
uint32_t ty_rtc_get_time(uint32_t * p_timestamp)
{
    struct tm loc_time;
    rtc_time_get(&loc_time);
    *p_timestamp = (uint32_t)mktime(&loc_time);
    // TY_PRINTF(">>> get time: %s, timestamp: %d", asctime(&loc_time), *p_timestamp);
    return 0;
}

/*********************************************************
FN: 
*/
uint32_t ty_rtc_get_local_time(uint32_t * p_timestamp)
{
    return 0;
}

/*********************************************************
FN: 
*/
uint32_t ty_rtc_get_old_time(uint32_t old_local_timestamp, uint32_t * p_timestamp)
{
    return 0;
}

/*********************************************************
FN: 
*/
uint32_t ty_rtc_control(uint8_t cmd, void * arg)
{
    return 0;
}

/*********************************************************
FN: 
*/
uint32_t ty_rtc_uninit(void)
{
    rtc_stop();
    return 0;
}
