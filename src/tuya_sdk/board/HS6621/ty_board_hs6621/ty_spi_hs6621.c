#include "ty_spi.h"
#include "peripheral.h"

/*********************************************************************
 * CONSTANT
 */
#undef TY_SPI_PIN_MISO
#undef TY_SPI_PIN_MOSI
#undef TY_SPI_PIN_CLK
#undef TY_SPI_PIN_CSN

#define TY_SPI_PIN_MISO             9
#define TY_SPI_PIN_MOSI             10
#define TY_SPI_PIN_CLK              11
#define TY_SPI_PIN_CSN              12

#define TY_SPI_FREQ                 4000000

/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * VARIABLE
 */

/*********************************************************************
 *  FUNCTION
 */
/*********************************************************
FN: 
*/
void ty_spi_init(void)
{
    // pinmux
    pinmux_config(TY_SPI_PIN_MISO,  PINMUX_SPI0_MST_SDA_I_CFG); /*MISO*/
    pinmux_config(TY_SPI_PIN_MOSI,  PINMUX_SPI0_MST_SDA_O_CFG); /*MOSI*/
    pinmux_config(TY_SPI_PIN_CLK,  PINMUX_SPI0_MST_SCK_CFG);    /*SCLK*/
    pinmux_config(TY_SPI_PIN_CSN,  PINMUX_SPI0_MST_CSN_CFG);    /*CSN */

    // SPI init
    spi_open(HS_SPI0, SPI_MODE_MASTER, SPI_TRANS_MODE_0, TY_SPI_FREQ);
}

/*********************************************************
FN: 
*/
void ty_spi_csn_set(bool pinState)
{
}

/*********************************************************
FN: 
*/
void ty_spi_readWriteData(uint8_t *pWriteData, uint8_t *pReadData, uint8_t writeDataLen)
{
    spi_master_cs_low(HS_SPI0);
    spi_master_exchange(HS_SPI0, pWriteData, pReadData, writeDataLen);
    spi_master_cs_high(HS_SPI0);
}

/*********************************************************
FN: open SPI
*/
void ty_spi_enable(void)
{
    spi_open(HS_SPI0, SPI_MODE_MASTER, SPI_TRANS_MODE_0, TY_SPI_FREQ);
}

/*********************************************************
FN: close SPI
*/
void ty_spi_disable(void)
{
    spi_close(HS_SPI0);
    pinmux_config(TY_SPI_PIN_MISO,  PINMUX_GPIO_MODE_CFG);
    pinmux_config(TY_SPI_PIN_MOSI,  PINMUX_GPIO_MODE_CFG);
    pinmux_config(TY_SPI_PIN_CLK,  PINMUX_GPIO_MODE_CFG);
    pinmux_config(TY_SPI_PIN_CSN,  PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(TY_SPI_PIN_MISO) | BITMASK(TY_SPI_PIN_MISO) |
                        BITMASK(TY_SPI_PIN_MISO) | BITMASK(TY_SPI_PIN_MISO),
                        PMU_PIN_MODE_PU);
}
