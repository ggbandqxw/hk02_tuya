#include "ty_i2c.h"
#include "peripheral.h"

/*********************************************************************
 * CONSTANT
 */
#define TY_I2C_PIN_SCL                  7
#define TY_I2C_PIN_SDA                  8
#define TY_I2C_PIN_SCL_MASK             BITMASK(TY_I2C_PIN_SCL)
#define TY_I2C_PIN_SDA_MASK             BITMASK(TY_I2C_PIN_SDA)

// #define TY_I2C_USE_HW
// #define TY_I2C_OLED_EN
/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * VARIABLE
 */
#ifndef TY_I2C_USE_HW
static const i2c_sim_config_t i2c_sim_cfg = {TY_I2C_PIN_SCL, TY_I2C_PIN_SDA, 1};
#endif

/*********************************************************************
 * FUNCTION
 */
/*********************************************************
FN:
*/
static void ty_i2c_scan_address(void)
{
    uint8_t address;
    uint8_t sample_data;
    bool detected_device = false;

    TY_PRINTF("I2C scanner started.");

    for (address = 1; address <= 127; address++)
    {
    #ifdef TY_I2C_USE_HW
        i2c_master_read(address, &sample_data, sizeof(sample_data));
    #else
        i2c_sim_master_read_mem(&i2c_sim_cfg, address, 0, 0, &sample_data, sizeof(sample_data));
    #endif

        if (sample_data != 0)
        {
            detected_device = true;
            TY_PRINTF("I2C device detected at address 0x%x.", address);
        }
    }

    if (!detected_device)
    {
        TY_PRINTF("No device was found.");
    }

    TY_PRINTF("I2C device scan ended.");
}

/*********************************************************
FN:
*/
uint32_t ty_i2c_init(void)
{
#ifdef TY_I2C_USE_HW
    pinmux_config(TY_I2C_PIN_SCL, PINMUX_I2C_MST_SCK_CFG);
    pinmux_config(TY_I2C_PIN_SDA, PINMUX_I2C_MST_SDA_CFG);
    pmu_pin_mode_set(TY_I2C_PIN_SCL_MASK | TY_I2C_PIN_SDA_MASK, PMU_PIN_MODE_OD);
    i2c_open(I2C_MODE_MASTER, 400000);
#else
    pinmux_config(TY_I2C_PIN_SCL, PINMUX_GPIO_MODE_CFG);
    pinmux_config(TY_I2C_PIN_SDA, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(TY_I2C_PIN_SCL_MASK | TY_I2C_PIN_SDA_MASK, PMU_PIN_MODE_OD);
    i2c_sim_config(&i2c_sim_cfg);
#endif

    // ty_i2c_scan_address();
    return 0;
}

/*********************************************************
FN:
*/
uint32_t ty_i2c_start(void)
{
    return 0;
}

/*********************************************************
FN:
*/
uint32_t ty_i2c_stop(void)
{
    return 0;
}

/*********************************************************
FN:
*/
uint32_t ty_i2c_control(uint8_t cmd, void* arg)
{
    return 0;
}

/*********************************************************
FN:
*/
uint32_t ty_i2c_uninit(void)
{
#ifdef TY_I2C_USE_HW
    i2c_close();
#endif

#ifdef TY_I2C_OLED_EN
    pinmux_config(TY_I2C_PIN_SCL, PINMUX_GPIO_MODE_CFG);
    pinmux_config(TY_I2C_PIN_SDA, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(TY_I2C_PIN_SCL_MASK | TY_I2C_PIN_SDA_MASK, PMU_PIN_MODE_PU);
#endif
    return 0;
}

/*********************************************************
FN:
*/
uint32_t ty_i2c_send(const uint8_t addr, const uint8_t* buf, uint32_t size)
{
#ifdef TY_I2C_USE_HW
    i2c_master_write(addr, (uint8_t*)buf, size);
#eldef TY_I2C_OLED_EN
    i2c_sim_master_write_mem(&i2c_sim_cfg, addr, 0, 0, (uint8_t*)buf, 1);//����δ��
#endif
    return 0;
}



/*********************************************************************
 * CONSTANT
 */
#define DELAY_CNT               1

enum
{
    GPIO_INPUT_MODE,
    GPIO_OUTPUT_MODE,
};

/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * VARIABLE
 */

/*********************************************************************
 * FUNCTION
 */
/*********************************************************
FN:
*/
void i2c_delay(unsigned long tim_1us)
{
    co_delay_us(tim_1us);
}

static void i2c_sda_pin_mode_set(uint8_t mode, uint8_t level)
{

    if(mode == GPIO_INPUT_MODE)
    {
        gpio_set_direction(TY_I2C_PIN_SDA_MASK, GPIO_INPUT);
    }
    else if(mode == GPIO_OUTPUT_MODE)
    {
        gpio_set_direction(TY_I2C_PIN_SDA_MASK, GPIO_OUTPUT);
    }

}

static void i2c_sda_pin_set(uint8_t level)
{

    if (level == 0)
    {
        gpio_write(TY_I2C_PIN_SDA_MASK, GPIO_LOW);
    }
    else if (level == 1)
    {
        gpio_write(TY_I2C_PIN_SDA_MASK, GPIO_HIGH);
    }
}

static void i2c_scl_pin_set(uint8_t level)
{
    if (level == 0)
    {
        gpio_write(TY_I2C_PIN_SCL_MASK, GPIO_LOW);
    }
    else if (level == 1)
    {
        gpio_write(TY_I2C_PIN_SCL_MASK, GPIO_HIGH);
    }
}

static uint8_t i2c_sda_pin_status_get(void)
{
    return gpio_read(TY_I2C_PIN_SDA_MASK)==0 ? 0 : 1;
}

/**
 * @description: i2c ack func
 * @param {type} none
 * @return: none
 */
static void i2c_ack(void)
{
    i2c_scl_pin_set(0);
    i2c_delay(DELAY_CNT);

    i2c_sda_pin_mode_set(GPIO_OUTPUT_MODE, 0);
    i2c_sda_pin_set(0);
    i2c_delay(DELAY_CNT);

    i2c_scl_pin_set(1);
    i2c_delay(DELAY_CNT);
    i2c_scl_pin_set(0);
    i2c_delay(DELAY_CNT);
}

/**
 * @description: i2c none ack func
 * @param {type} none
 * @return: none
 */
static void i2c_noack(void)
{
    i2c_sda_pin_mode_set(GPIO_OUTPUT_MODE, 1);
    i2c_sda_pin_set(1);

    i2c_delay(DELAY_CNT);
    i2c_scl_pin_set(1);
    i2c_delay(DELAY_CNT);
    i2c_scl_pin_set(0);
    i2c_delay(DELAY_CNT);
}

/**
 * @description: i2c wait ack
 * @param {type} none
 * @return: rev ack return true else return false
 */
static uint8_t i2c_wait_ack(void)
{
    uint8_t cnt = 50;

    i2c_sda_pin_mode_set(GPIO_INPUT_MODE, 1);/* set input and release SDA */
    i2c_sda_pin_set(1);
    i2c_delay(DELAY_CNT);

    i2c_scl_pin_set(0);       /* put down SCL ready to cheack SCA status */
    i2c_delay(DELAY_CNT);

    i2c_scl_pin_set(1);
    i2c_delay(DELAY_CNT);

    while(i2c_sda_pin_status_get()) /* get ack */
    {
        cnt--;
        if (cnt==0)
        {
            i2c_scl_pin_set(0);
            return false;
        }
        i2c_delay(DELAY_CNT);
    }

    i2c_scl_pin_set(0);
    i2c_delay(DELAY_CNT);
    return true;
}

/**
 * @description: i2c start signal
 * @param {type} none
 * @return: none
 */
void i2c_start(void)
{
    i2c_sda_pin_mode_set(GPIO_OUTPUT_MODE, 1);    //SDA output mode

    i2c_scl_pin_set(1);
    i2c_sda_pin_set(1);
    i2c_delay(DELAY_CNT);

    i2c_sda_pin_set(0);
    i2c_delay(DELAY_CNT);

    i2c_scl_pin_set(0);
    i2c_delay(DELAY_CNT);
}

/**
 * @description: i2c stop signal
 * @param {type} none
 * @return: none
 */
void i2c_stop(void)
{
    i2c_sda_pin_mode_set(GPIO_OUTPUT_MODE, 0);     //SDA input mode

    i2c_scl_pin_set(0);
    i2c_sda_pin_set(0);
    i2c_delay(DELAY_CNT);

    i2c_scl_pin_set(1);
    i2c_delay(DELAY_CNT);

    i2c_sda_pin_set(1);
    i2c_delay(DELAY_CNT);
}

/**
 * @description: send one byte to i2c bus
 * @param {uint8_t} data send to i2c
 * @return: none
 */
void i2c_send_byte(uint8_t data)
{
    uint8_t idx = 0;
    i2c_scl_pin_set(0);
    i2c_sda_pin_mode_set(GPIO_OUTPUT_MODE, 1);

    for(idx=0; idx<8; idx++)
    {
        if (data & 0x80)
        {
            i2c_sda_pin_set(1);
        }
        else
        {
            i2c_sda_pin_set(0);
        }
        i2c_delay(DELAY_CNT);

        i2c_scl_pin_set(1);
        i2c_delay(DELAY_CNT);

        i2c_scl_pin_set(0);
        i2c_delay(DELAY_CNT);

        data <<= 1;
    }
}

/**
 * @description: send bytes to i2c bus
 * @param {type} none
 * @return: none
 */
void i2c_send_bytes(uint8_t adderss_cmd, uint8_t *buff, uint8_t len)
{
    uint8_t idx;
    i2c_send_byte(adderss_cmd);
    i2c_wait_ack();

    for (idx=0; idx<len; idx++)
    {
        i2c_send_byte(buff[idx]);
        i2c_wait_ack();
    }
}

/**
 * @description: recive one byte from i2c bus
 * @param {type} none
 * @return: none
 */
void i2c_rcv_byte(uint8_t *data)
{
    uint8_t idx;
    i2c_sda_pin_mode_set(GPIO_INPUT_MODE, 1);
    i2c_delay(25);

    for (idx=0; idx<8; idx++)
    {
        i2c_scl_pin_set(0);
        i2c_delay(DELAY_CNT);

        i2c_scl_pin_set(1);
        *data = *data << 1;
        if(i2c_sda_pin_status_get())
        {
            *data |= 1;
        }
        i2c_delay(DELAY_CNT);
    }

    i2c_scl_pin_set(0);
}

/**
 * @description: recive bytes from i2c bus,last byte none ack
 * @param {type} none
 * @return: none
 */
void i2c_rcv_bytes(uint8_t adderss_cmd, uint8_t *buff, uint8_t len)
{
    uint8_t idx;
    i2c_send_byte(adderss_cmd);
    i2c_wait_ack();

    for (idx=0; idx<len; idx++)
    {
        i2c_rcv_byte(&buff[idx]);

        if (idx < len-1)
        {
            i2c_ack();
        }
        else
        {
            i2c_noack();
        }
    }
}

void i2c_soft_cfg(uint8_t adderss_cmd,uint8_t reg_addr,uint8_t data)
{
    i2c_start();
    i2c_send_byte(adderss_cmd);
    i2c_wait_ack();
    i2c_send_byte(reg_addr);
    i2c_wait_ack();
    i2c_send_byte(data);
    i2c_wait_ack();
    i2c_stop();
}

void i2c_write_reg(uint8_t address_cmd,uint8_t register_addr)
{
    i2c_start();
    i2c_send_bytes(address_cmd, &register_addr, 1);
    i2c_stop();
}

void i2c_soft_gpio_init(void)
{
    pinmux_config(TY_I2C_PIN_SCL, PINMUX_GPIO_MODE_CFG);
    pinmux_config(TY_I2C_PIN_SDA, PINMUX_GPIO_MODE_CFG);
    gpio_set_direction(TY_I2C_PIN_SCL_MASK, GPIO_OUTPUT);
    gpio_set_direction(TY_I2C_PIN_SDA_MASK, GPIO_OUTPUT);
}
