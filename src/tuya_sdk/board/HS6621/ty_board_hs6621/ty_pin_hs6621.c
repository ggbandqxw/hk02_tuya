#include "ty_pin.h"
#include "ty_ble.h"
#include "ty_uart.h"
#include "ty_rtc.h"
#include "ty_pwm.h"
#include "ty_adc.h"
#include "ty_i2c.h"
#include "tuya_ble_sdk_test.h"
#include "peripheral.h"


/*********************************************************************
 * CONSTANT
 */

/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * VARIABLE
 */

/*********************************************************************
 * FUNCTION
 */
static void pin_irq_handler(uint32_t pin_mask)
{
}

uint32_t ty_pin_init(uint8_t pin, ty_pin_mode_t mode)
{
    // gpio_open();
    // pinmux_config(pin, PINMUX_GPIO_MODE_CFG);

    // // pin mode
    // switch (mode & TY_PIN_MODE_MASK)
    // {
    //     case TY_PIN_PULL_UP:
    //         pmu_pin_mode_set(BITMASK(pin), PMU_PIN_MODE_PU);
    //         break;
        
    //     case TY_PIN_PULL_DOWN:
    //         pmu_pin_mode_set(BITMASK(pin), PMU_PIN_MODE_PD);
    //         break;

    //     case TY_PIN_PULL_NONE:
    //     default:
    //         break;
    // }

    // // direction
    // switch (mode & TY_PIN_INOUT_MASK)
    // {
    //     case TY_PIN_IN_FL:
    //         pmu_pin_mode_set(BITMASK(pin), PMU_PIN_MODE_FLOAT);
    //     case TY_PIN_IN:
    //     case TY_PIN_IN_IRQ:
    //         gpio_set_direction(BITMASK(pin), GPIO_INPUT);
    //         break;

    //     case TY_PIN_OUT_PP:
    //         pmu_pin_mode_set(BITMASK(pin), PMU_PIN_MODE_PP);
    //         gpio_set_direction(BITMASK(pin), GPIO_OUTPUT);
    //         break;

    //     case TY_PIN_OUT_OD:
    //         pmu_pin_mode_set(BITMASK(pin), PMU_PIN_MODE_OD);
    //         gpio_set_direction(BITMASK(pin), GPIO_OUTPUT);
    //         break;

    //     default:
    //         break;
    // }

    // // output level
    // switch (mode & TY_PIN_INIT_MASK)
    // {
    //     case TY_PIN_INIT_LOW:
    //         gpio_write(BITMASK(pin), GPIO_LOW);
    //         break;

    //     case TY_PIN_INIT_HIGH:
    //         gpio_write(BITMASK(pin), GPIO_HIGH);
    //         break;

    //     default:
    //         break;
    // }

    // // irq mode
    // switch (mode & TY_PIN_IRQ_MASK)
    // {
    //     case TY_PIN_IRQ_RISE:
    //         gpio_set_interrupt(BITMASK(pin), GPIO_RISING_EDGE);
    //         break;

    //     case TY_PIN_IRQ_FALL:
    //         gpio_set_interrupt(BITMASK(pin), GPIO_FALLING_EDGE);
    //         break;

    //     case TY_PIN_IRQ_RISE_FALL:
    //         gpio_set_interrupt(BITMASK(pin), GPIO_BOTH_EDGE);
    //         break;

    //     case TY_PIN_IRQ_LOW:
    //         gpio_set_interrupt(BITMASK(pin), GPIO_LOW_LEVEL);
    //         break;

    //     case TY_PIN_IRQ_HIGH:
    //         gpio_set_interrupt(BITMASK(pin), GPIO_HIGH_LEVEL);
    //         break;

    //     default:
    //         break;
    // }
    // if ((mode & TY_PIN_IRQ_MASK) != 0)
    // {
    //     gpio_set_interrupt_callback(pin_irq_handler);
    // }

    return 0;
}

uint32_t ty_pin_set(uint8_t pin, ty_pin_level_t level)
{
    if (level == TY_PIN_LOW)
    {
        gpio_write(BITMASK(pin), GPIO_LOW);
    }
    else if (level == TY_PIN_HIGH)
    {
        gpio_write(BITMASK(pin), GPIO_HIGH);
    }
    return 0;
}

uint32_t ty_pin_get(uint8_t pin, ty_pin_level_t* p_level)
{
    if (gpio_read(BITMASK(pin)) == 0)
    {
        *p_level = TY_PIN_LOW;
    }
    else
    {
        *p_level = TY_PIN_HIGH;
    }
    return 0;
}

uint32_t ty_pin_control(uint8_t pin, uint8_t cmd, void* arg)
{
    return 0;
}

/*********************************************************
FN: 
*/
uint32_t ty_pin_uninit(uint8_t pin, ty_pin_mode_t mode)
{
    return 0;
}







