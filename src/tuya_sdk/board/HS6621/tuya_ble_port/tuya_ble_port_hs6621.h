/**
****************************************************************************
* @file      tuya_ble_port_nRF52832.h
* @brief     tuya_ble_port_nRF52832
* @author    suding
* @version   V1.0.0
* @date      2020-04
* @note
******************************************************************************
* @attention
*
* <h2><center>&copy; COPYRIGHT 2020 Tuya </center></h2>
*/


#ifndef __TUYA_BLE_PORT_HS6621_H__
#define __TUYA_BLE_PORT_HS6621_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDE
 */
#include "tuya_ble_config.h"
#include "ty_system.h"
#include "elog.h"

/*********************************************************************
 * CONSTANT
 */
#if (TUYA_BLE_LOG_ENABLE || TUYA_APP_LOG_ENABLE)
    #define TUYA_BLE_PRINTF(...)        log_d(__VA_ARGS__)
    #define TUYA_BLE_HEXDUMP(...)       ty_system_log_hexdump("", 8, __VA_ARGS__)
#else
    #define TUYA_BLE_PRINTF(...)
    #define TUYA_BLE_HEXDUMP(...)
#endif

void tuya_ble_device_enter_critical(void);
void tuya_ble_device_exit_critical(void);

/*********************************************************************
 * STRUCT
 */

/*********************************************************************
 * EXTERNAL VARIABLE
 */

/*********************************************************************
 * EXTERNAL FUNCTION
 */


#ifdef __cplusplus
}
#endif

#endif //__TUYA_BLE_PORT_HS6621_H__
