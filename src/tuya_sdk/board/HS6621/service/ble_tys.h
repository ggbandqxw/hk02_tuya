/**
 ***************************************************************************************************
 *
 * @file ble_tys.h
 *
 * @brief Tuya service transport profile implementation.
 *
 * Copyright (C) Eker 2021
 *
 *
 ***************************************************************************************************
 */
#ifndef __BLE_TYS_H__
#define __BLE_TYS_H__

#include "rwip_config.h"
#include "prf_types.h"
#include "prf.h"
#include "ble_tys_task.h"
#include "attm.h"
#include "co_fifo.h"
/*
 * MACROS
 ***************************************************************************************************
 */
// Maximum number of transport task instances
#define TYS_IDX_MAX                     BLE_CONNECTION_MAX

#define TYS_UPLOAD_MAX_LEN              (BLE_MAX_OCTETS-4-3) // l2cap header 4B, notify header 3B
#define TYS_MTU_TO_NTF_WRTCMD_LEN(n)    ((n) - 3)
#define TYS_MTO_TO_NTF_WRTCMD_LEN(n)    ((n) - 7)

/*
 * DEFINES
 ***************************************************************************************************
 */
// Possible states of the TYS task
enum
{
    // Idle state
    TYS_IDLE,
    // Connected state
    TYS_CONNECTED,
    // Send data
    TYS_BUSY,

    // Number of defined states.
    TYS_STATE_MAX,
};

// Attributes State Machine
enum
{
    TYS_IDX_SVC,

    TYS_IDX_WRITE_CHAR,
    TYS_IDX_WRITE_VAL,

    TYS_IDX_NTF_CHAR,
    TYS_IDX_NTF_VAL,
    TYS_IDX_NTF_CFG,

    TYS_IDX_READ_CHAR,
    TYS_IDX_READ_VAL,

    TYS_IDX_NB,
};

/*
 * TYPE DEFINITIONS
 ***************************************************************************************************
 */
// Transport Profile environment variable
struct tys_env_tag
{
    // profile environment
    prf_env_t prf_env;
    // On-going operation
    struct ke_msg * operation;
    // Services Start Handle
    uint16_t start_hdl;
    // TY_SERVER task state
    ke_state_t state[TYS_IDX_MAX];
    // Notification configuration of peer devices.
    uint8_t ntf_cfg[TYS_IDX_MAX];
    // Max mtu;
    uint16_t mtu[TYS_IDX_MAX];
    uint16_t tx_len[TYS_IDX_MAX];
    uint16_t perfect_once_tx_length[TYS_IDX_MAX];

    // for send buffer.
    uint32_t  send_pool_size;
    uint32_t  send_pool_num;
    co_fifo_t send_fifo[TYS_IDX_MAX];
    uint8_t   send_pool_inner[TYS_IDX_MAX];
    uint8_t  *send_pool[TYS_IDX_MAX];
};


/*
 * GLOBAL VARIABLE DECLARATIONS
 ***************************************************************************************************
 */
/**
 ***************************************************************************************************
 * @brief Retrieve TY service profile interface
 *
 * @return TY service profile interface
 ***************************************************************************************************
 */
const struct prf_task_cbs* ty_server_prf_itf_get(void);

/**
 ***************************************************************************************************
 * @brief Retrieve Attribute handle from service and attribute index
 *
 * @param[in] svc_idx TY_SERVER Service index
 * @param[in] att_idx Attribute index
 *
 * @return TY_SERVER attribute handle or INVALID HANDLE if nothing found
 ***************************************************************************************************
 */
uint16_t ty_server_get_att_handle(uint8_t att_idx);

/**
 ***************************************************************************************************
 * @brief Retrieve Service and attribute index form attribute handle
 *
 * @param[out] handle  Attribute handle
 * @param[out] svc_idx Service index
 * @param[out] att_idx Attribute index
 *
 * @return Success if attribute and service index found, else Application error
 ***************************************************************************************************
 */
uint8_t ty_server_get_att_idx(uint16_t handle, uint8_t *att_idx);

/**
 ***************************************************************************************************
 * Initialize task handler
 *
 * @param task_desc Task descriptor to fill
 ***************************************************************************************************
 */
void ty_server_task_init(struct ke_task_desc *task_desc);

#endif /* __BLE_TYS_H__ */
