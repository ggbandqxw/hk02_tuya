/**
 ***************************************************************************************************
 *
 * @file ble_tys_task.h
 *
 * @brief Tuya service transport task implementation.
 *
 * Copyright (C) Eker 2021
 *
 *
 ***************************************************************************************************
 */
#ifndef __BLE_TYS_TASK_H__
#define __BLE_TYS_TASK_H__

#include <stdint.h>
#include "rwip_task.h" // Task definitions

/*
 * DEFINES
 ***************************************************************************************************
 */
#define TASK_ID_TYS             TASK_ID_USER

/*
 * TYPE DEFINITIONS
 ***************************************************************************************************
 */
// ty server error id
enum ty_server_error_id
{
    TY_SERVER_ERROR_OK       = 0,
    TY_SERVER_ERROR_NO_BUFF  = 1,
    TY_SERVER_ERROR_NO_CONN  = 2,
    TY_SERVER_ERROR_NO_NOTI  = 3,
};

// ty server message id
enum ty_server_msg_id
{
    // Start the Server - at connection used to restore bond data
    TY_SERVER_ENABLE_REQ = TASK_FIRST_MSG(TASK_ID_TYS),//!< @ref struct ty_server_enable_req
    // Confirmation of the Server start
    TY_SERVER_ENABLE_RSP, //!<  @ref struct ty_server_enable_req
    // Inform APP that Notification Configuration has been changed
    TY_SERVER_NTF_CFG_IND, //!< @ref struct ty_server_ntf_cfg_ind
    TY_SERVER_WRITE_IND,
    // Send notification to gatt client
    TY_SERVER_SEND_NTF_CMD, //!< @ref struct ty_server_send_ntf_cmd
    TY_SERVER_TIMEOUT_TIMER,
    TY_SERVER_BUFFER_EMPTY,
    TY_SERVER_BUFFER_FULL,
    TY_SERVER_ERROR,
};

/*
 * APIs Structures
 ***************************************************************************************************
 */
#define TY_ATT_UUID_128_LEN       16
#define TY_ATT_UUID_16_LEN         2

struct tys_sdp_att
{
    // Attribute UUID Length
    uint8_t  uuid_len;
    // Attribute UUID
    uint8_t  uuid[TY_ATT_UUID_128_LEN];
};

// ty sdp service database
struct tys_sdp_svc
{
    // Service UUID Length
    uint8_t  uuid_len;
    // Service UUID
    uint8_t  uuid[TY_ATT_UUID_128_LEN];
    // attribute information present in the service
    struct tys_sdp_att write_no_res;
    struct tys_sdp_att notify;
    struct tys_sdp_att write;
};

// Parameters for the database creation
struct ty_server_db_cfg
{
    uint16_t connect_num; // max connectd <= BLE_CONNECTION_MAX
    uint16_t fifo_size;
    uint8_t *fifo_buffer;
    uint8_t svc_type; // type=0: use database(16 uuid); type=1: use database(128 uuid); type=2: user custom
    struct tys_sdp_svc svc;
};

// Parameters of the @ref TY_SERVER_ENABLE_REQ message
struct ty_server_enable_req
{
    // connection index
    uint8_t  conidx;
    // Notification Configuration
    uint8_t  ntf_cfg;
};

// Parameters of the @ref TY_SERVER_ENABLE_RSP message
struct ty_server_enable_rsp
{
    // connection index
    uint8_t conidx;
    //status
    uint8_t status;
};

// Parameters of the @ref TY_SERVER_NTF_CFG_IND message
struct ty_server_ntf_cfg_ind
{
    // connection index
    uint8_t  conidx;
    // Notification Configuration
    uint8_t  ntf_cfg;
};

// Parameters of the @ref TY_SERVER_SEND_NTF_CMD message
struct ty_server_send_ntf_cmd
{
    // connection index
    uint8_t  conidx;
    uint16_t length;
    uint8_t value[__ARRAY_EMPTY];
};


// Parameters of the @ref TY_SERVER_WRITE_IND message
struct ty_server_write_ind
{
    // connection index
    uint8_t  conidx;
    // Handle of the attribute that has to be written
    uint16_t handle;
    uint16_t offset;
    uint16_t length;
    uint8_t value[__ARRAY_EMPTY];
};

// buffer empty event data structure
struct ty_server_buffer_empty_evt
{
    // GATT request type
    uint8_t operation;
    // Status of the request
    uint8_t status;
};

// buffer full event data structure
struct ty_server_buffer_full_evt
{
    uint8_t operation;
    uint8_t status;
};

// error event data structure
struct ty_server_error_evt
{
    // GATT request type
    uint16_t operation;
    // Status of the request
    uint8_t status;
};

/**
 ***************************************************************************************************
 * @brief get free size
 *
 * @param[in] conidx: the connection index
 * @return the free size
 ***************************************************************************************************
 */
uint32_t ty_server_get_free_size(uint8_t conidx);

/**
 ***************************************************************************************************
 * @brief check fifo is full
 *
 * @param[in] conidx: the connection index
 * @return 0: buffer not full. 1: buffer full
 ***************************************************************************************************
 */
uint8_t ty_server_check_fifo_full(uint8_t conidx);

/**
 ***************************************************************************************************
 * @brief set mtu size
 *
 * @param[in] conidx: the connection index
 * @param[in] mtu: mtu size
 ***************************************************************************************************
 */
void ty_server_set_max_mtu(uint8_t conidx, uint16_t mtu);

/**
 ***************************************************************************************************
 * @brief set data len
 *
 * @param[in] conidx: the connection index
 * @param[in] tx_len: max tx_len
 ***************************************************************************************************
 */
void ty_server_set_data_len(uint8_t conidx, uint16_t tx_len);

/**
 ***************************************************************************************************
 * @brief get the perfect once tx len
 *
 * @param[in] conidx: the connection index
 * @param[in] mto: mto len
 * @param[in] char_len: char len
 ***************************************************************************************************
 */
uint16_t ty_server_perfect_once_tx_length(uint16_t mtu, uint16_t mto, uint16_t char_len);

#endif /* __BLE_TYS_TASK_H__ */
