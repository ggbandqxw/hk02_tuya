/**
 * @file rwapp_config.h
 * @brief
 * @date Wed, Sep  5, 2018  5:44:25 PM
 * @author liqiang
 *
 * @addtogroup APP_SIMPLE_SERVER_CONFIG rwapp_config.h
 * @ingroup APP_SIMPLE_SERVER
 * @brief 使能profile，安全(sec)，白名单(whitelist)，隐私(privacy)
 * @details
 *
 * @{
 */

#ifndef __RWAPP_CONFIG_H__
#define __RWAPP_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */


/*********************************************************************
 * MACROS
 */

/// enable/disable security connect
#define BLE_APP_SEC           0

/// enable/disable simple server profile
#define BLE_APP_SIMPLE_SERVER 0

/* enable/disable 电信服务 0x4354 profile*/
#define BLE_APP_CTC_SERVER 0
#define TASK_ID_CTC_SERVER (TASK_ID_USER)

/* enable/disable 安居微棠/零匙 0x0001 profile*/
#define BLE_APP_LKM3_SERVER 0
#define TASK_ID_LKM3_SERVER (TASK_ID_USER + 1)

/* BLE -> APP 0XFFE0 */
#define BLETOAPP_SERVER 0
#define TASK_ID_BLEAPP_SERVER (TASK_ID_USER + 2)

/* enable/disable 凌度HK01 0xFF01 profile*/
#ifdef BLE_ENABLE
#define BLE_APP_HK01_SERVER 1
#else
#define BLE_APP_HK01_SERVER 0
#endif
#define TASK_ID_HK01_SERVER (TASK_ID_USER + 3)

/// enable/disable white list
#define BLE_APP_WHITE_LIST   0
/// enable/disable privacy
#define BLE_APP_PRIVACY      0

/// enable/disable dis profile
#define BLE_APP_DIS 0

#define XTAL32M_CTUNE_DEFAULT    6

#define CONFIG_MP_TEST_ENABLE    0
// <q> Frequency Calibration
#define CONFIG_FREQ_CALIB_ENABLE 0

#if (CONFIG_FREQ_CALIB_ENABLE)
// 32M Xtal Load Capacitance Index
#define NVDS_TAG_XTAL32M_CTUNE   0x04
#endif

#if (BLE_APP_DIS)
///Device Information Service Server Role
#define BLE_DIS_SERVER       1

/// Manufacturer Name Value
#define APP_DIS_MANUFACTURER_NAME       ("OnMicro")
/// Manufacturer Name Value size 
#define APP_DIS_MANUFACTURER_NAME_LEN   (sizeof(APP_DIS_MANUFACTURER_NAME))

/// Model Number String Value
#define APP_DIS_MODEL_NB_STR            ("Model Nbr 0.2")
/// Model Number String Value len
#define APP_DIS_MODEL_NB_STR_LEN        (sizeof(APP_DIS_MODEL_NB_STR))

/// Serial Number
#define APP_DIS_SERIAL_NB_STR           ("SerialNum0.2")
/// Serial Number len
#define APP_DIS_SERIAL_NB_STR_LEN       (sizeof(APP_DIS_SERIAL_NB_STR))

/// Firmware Revision
#define APP_DIS_FIRM_REV_STR            ("6621A3")
/// Firmware Revision len
#define APP_DIS_FIRM_REV_STR_LEN        (5)

/// System ID Value - LSB -> MSB
#define APP_DIS_SYSTEM_ID               ("\x00\x00\x01\x02\x00\x03\x04\x05")
/// System ID Value len
#define APP_DIS_SYSTEM_ID_LEN           (8)

/// Hardware Revision String
#define APP_DIS_HARD_REV_STR           ("1.3")
/// Hardware Revision String len
#define APP_DIS_HARD_REV_STR_LEN       (sizeof(APP_DIS_HARD_REV_STR))

/// Software Revision String
#define APP_DIS_SW_REV_STR              ("1.0.1.2")
/// Software Revision String len
#define APP_DIS_SW_REV_STR_LEN          (sizeof(APP_DIS_SW_REV_STR))

/// IEEE
#define APP_DIS_IEEE                    ("\x52\x54\x4B\x42\x65\x65\x49\x45\x45\x45\x44\x61\x74\x61\x6C\x69\x73\x74\x00")
/// IEEE len
#define APP_DIS_IEEE_LEN                (19)

/**
 * PNP ID Value - LSB -> MSB
 *      Vendor ID Source : 0x02 (USB Implementer’s Forum assigned Vendor ID value)
 *      Vendor ID : 0x045E      (Microsoft Corp)
 *      Product ID : 0x0040
 *      Product Version : 0x0300
 */
#define APP_DIS_PNP_ID               ("\x01\xBF\x01\xB8\x32\x79\x49")
/// dis pnp id len
#define APP_DIS_PNP_ID_LEN           (7)
#endif
/// the dis features
#define APP_DIS_FEATURES             (DIS_ALL_FEAT_SUP)


//TUYA_DEMO
/**************************************************************************************************/
// <h> System Configuration
// <o> Internal DCDC
// <0=>Disable <1=>Enable
#define CONFIG_INTERNAL_DCDC                                    1

// <o> Use 32K in deep sleep
// <0=>Disable <1=>Enable
#define CONFIG_32K_IN_DEEP_SLEEP                                1

// <o> Default Ctune of 32M Xtal <0-31>
// <i> Value range: 0-31, unit: 0.75pF
#define XTAL32M_CTUNE_DEFAULT                                   15

// <o> 32K Clock Selection
// <0=>PMU_32K_SEL_RC <1=>PMU_32K_SEL_32768HZ_XTAL
#define CONFIG_32K_CLOCK                                        0

// <o> Sleep mode
// <0=>Disable <1=>Enable
#define CONFIG_SLEEP_MODE                                       1

// <q> Security
#define BLE_APP_SEC                                             0 //tuya=0;1

// <q> Whitelist
#define BLE_APP_WHITE_LIST                                      0

// <q> Privacy
#define BLE_APP_PRIVACY                                         0

// <o> Tuya MTU <23-247>
#define TY_GAP_LE_MTU                                           247
// </h>

/**************************************************************************************************/
// <h> Device Information
// <s> Device Name
#define APP_DFLT_DEVICE_NAME                                    "Demo_name"
#define APP_DFLT_DEVICE_NAME_LEN                                (sizeof(APP_DFLT_DEVICE_NAME))

// <s> Bluetooth Address
// <i> If it has been written data in NVDS_TAG_BD_ADDRESS,
// <i> then the BT address will be the NVDS_TAG_BD_ADDRESS value.
#define LOCAL_DEFAULT_ADDR                                      "3C:5B:86:4D:23:DC"
/**************************************************************************************************/

// <o> Appearance
// <0=>BLE_APPEARANCE_UNKNOWN
// <384=>BLE_APPEARANCE_GENERIC_REMOTE_CONTROL
// <960=>BLE_APPEARANCE_GENERIC_HID
// <961=>BLE_APPEARANCE_HID_KEYBOARD
// <962=>BLE_APPEARANCE_HID_MOUSE
// <963=>BLE_APPEARANCE_HID_JOYSTICK
// <964=>BLE_APPEARANCE_HID_GAMEPAD
#define CONFIG_BT_DEVICE_APPEARANCE                             0
//</h>
/**************************************************************************************************/
// <h> Connection Parameters
// <i> Interval Time = interval * 1.25 ms * (APP_BLE_CONN_LATENCY+1)
// <i> Timeout Time = APP_BLE_CONN_TIMEOUT * 10 ms
// <o0> Min interval (unit: 1.25ms)
// <o1> Max interval (unit: 1.25ms)
// <o2> Slave Latency
// <o3> Super timeout (unit: 10ms)
#define APP_BLE_CONN_INTERVAL_MIN                               16  // unit: 1.25ms
#define APP_BLE_CONN_INTERVAL_MAX                               16  // unit: 1.25ms
#define APP_BLE_CONN_LATENCY                                    49
#define APP_BLE_CONN_TIMEOUT                                    500 // unit: 10ms
// </h>

/**************************************************************************************************/
// <h> Log Level
// <o> APP
// <0=>LOG_LEVEL_OFF  <1=>LOG_LEVEL_ERROR <2=>LOG_LEVEL_WARNING
// <3=>LOG_LEVEL_INFO <4=>LOG_LEVEL_DEBUG <5=>LOG_LEVEL_VERBOSE
// <i> app.c, app_task.c
#define APP_LOG_LEVEL_APP                                       2
// <o> ADV
// <0=>LOG_LEVEL_OFF  <1=>LOG_LEVEL_ERROR <2=>LOG_LEVEL_WARNING
// <3=>LOG_LEVEL_INFO <4=>LOG_LEVEL_DEBUG <5=>LOG_LEVEL_VERBOSE
// <i> app_adv.c
#define APP_LOG_LEVEL_ADV                                       3
// <o> Security
// <0=>LOG_LEVEL_OFF  <1=>LOG_LEVEL_ERROR <2=>LOG_LEVEL_WARNING
// <3=>LOG_LEVEL_INFO <4=>LOG_LEVEL_DEBUG <5=>LOG_LEVEL_VERBOSE
// <i> app_sec.c
#define APP_LOG_LEVEL_SEC                                       2
// <o> Tuya service
// <0=>LOG_LEVEL_OFF  <1=>LOG_LEVEL_ERROR <2=>LOG_LEVEL_WARNING
// <3=>LOG_LEVEL_INFO <4=>LOG_LEVEL_DEBUG <5=>LOG_LEVEL_VERBOSE
// <i> app_sec.c
#define APP_LOG_LEVEL_SEC                                       2
// </h>

// <<< end of configuration section >>>
/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * EXTERN VARIABLES
 */


/*********************************************************************
 * EXTERN FUNCTIONS
 */


#ifdef __cplusplus
}
#endif

#endif

/** @} */

