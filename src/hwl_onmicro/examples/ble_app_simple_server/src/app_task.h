/**
 ****************************************************************************************
 *
 * @file app_task.h
 *
 * @brief Header file - APPTASK.
 *
 * Copyright (C) RivieraWaves 2009-2015
 *
 *
 ****************************************************************************************
 */

#ifndef APP_TASK_H_
#define APP_TASK_H_

/**
 ****************************************************************************************
 * @addtogroup APP_SIMPLE_SERVER_TASK app_task.h
 * @ingroup APP_SIMPLE_SERVER
 * @brief Routes ALL messages to/from APP block.
 *
 * The APPTASK is the block responsible for bridging the final application with the
 * RWBLE software host stack. It communicates with the different modules of the BLE host,
 * i.e. ref SMP, ref GAP and ref GATT.
 *
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "rwip_config.h"             // SW configuration

#if (BLE_APP_SIMPLE_SERVER)
#include "app_simple_server.h"              // SIMPLE_SERVER Module Definition
#include "simple_server_task.h"
#endif //(BLE_APP_SIMPLE_SERVER)

#if (BLE_APP_CTC_SERVER)
#include "app_ctc_server.h"              // CTC_SERVER Module Definition
#include "ctc_server_task.h"
#endif //(BLE_APP_CTC_SERVER)

#if (BLE_APP_LKM3_SERVER)
#include "app_lkm3_server.h"              // CTC_SERVER Module Definition
#include "lkm3_server_task.h"
#endif //(BLE_APP_LKM3_SERVER)

#if (BLETOAPP_SERVER)
#include "app_bleapp_server.h"            // BLEAPP_SERVER Module Definition
#include "bleapp_server_task.h"
#endif //(BLETOAPP_SERVER)

#if (BLE_APP_HK01_SERVER)
#include "app_hk01_server.h"              // HK01_SERVER Module Definition
#include "hk01_server_task.h"
#endif //(BLE_APP_HK01_SERVER)

#if (BLE_APP_PRESENT)

#include <stdint.h>         // Standard Integer
#include "rwip_task.h"      // Task definitions
#include "ke_task.h"        // Kernel Task

/*
 * DEFINES
 ****************************************************************************************
 */

/// Number of APP Task Instances
#define APP_IDX_MAX                 (1)

/*
 * ENUMERATIONS
 ****************************************************************************************
 */

/// States of APP task
enum appm_state
{
    /// Initialization state
    APPM_INIT,
    /// Database create state
    APPM_CREATE_DB,
    /// Ready State
    APPM_READY,
    /// Connected state
    APPM_CONNECTED,

    /// Number of defined states.
    APPM_STATE_MAX
};


/// APP Task messages
/*@TRACE*/
enum app_msg_id
{
    APPM_DUMMY_MSG = TASK_FIRST_MSG(TASK_ID_APP),

    #if (BLE_APP_HT)
    /// Timer used to refresh the temperature measurement value
    APP_HT_MEAS_INTV_TIMER,
    #endif //(BLE_APP_HT)

    #if (BLE_APP_HID)
    /// Timer used to disconnect the moue if no activity is detecter
    APP_HID_MOUSE_TIMEOUT_TIMER,
    #endif //(BLE_APP_HID)
};


/*
 * GLOBAL VARIABLE DECLARATIONS
 ****************************************************************************************
 */

#endif //(BLE_APP_PRESENT)

/// @} APPTASK

#endif // APP_TASK_H_
