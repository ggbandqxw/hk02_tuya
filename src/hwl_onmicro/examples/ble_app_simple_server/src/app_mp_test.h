/**
 ***************************************************************************************************
 *
 * @file app_mp_test.h
 *
 * @brief Functions for mass production.
 *
 * Copyright (C) Eker 2021
 *
 *
 ***************************************************************************************************
 */
#ifndef __APP_MP_H__
#define __APP_MP_H__
//#include "app_key.h"

#if (CONFIG_MP_TEST_ENABLE)

/*
 * INCLUDE FILES
 ***************************************************************************************************
 */

/*
 * MACROS
 ***************************************************************************************************
 */

/*
 * TYPEDEFS
 ***************************************************************************************************
 */
typedef enum
{
    MP_TEST_MODE_INVALID = 0,
    MP_TEST_APP_1_MODE,
    MP_TEST_APP_2_MODE,
    MP_TEST_APP_3_MODE,
    MP_TEST_APP_4_MODE,
    MP_TEST_APP_5_MODE,
    MP_TEST_APP_6_MODE,
    MP_TEST_APP_7_MODE,
    MP_TEST_APP_8_MODE,
    MP_TEST_UART_MODE,
    MP_TEST_HCI_MODE,
} mp_test_mode_t;

/*
 * EXTERN VARIABLES
 ***************************************************************************************************
 */

/*
 * EXTERN FUNCTIONS
 ***************************************************************************************************
 */

/*
 * GLOBAL FUNCTIONS
 ***************************************************************************************************
 */
#if (CONFIG_MP_TEST_KEY)
void mp_test_mode_check(T_KEY_INDEX_DEF key_index_1, T_KEY_INDEX_DEF key_index_2);
#endif 
/**
 ***************************************************************************************************
 * @brief Set MP test mode
 *
 * @param[in] mode: MP test mode, see @ref mp_test_mode_t
 *
 * @return void
 ***************************************************************************************************
 */
void mp_test_mode_set(mp_test_mode_t mode);

bool mp_test_is_fast_pair_mode(mp_test_mode_t mode);
/**
 ***************************************************************************************************
 * @brief Get MP test mode
 *
 * @param void
 *
 * @return MP test mode, see @ref mp_test_mode_t
 ***************************************************************************************************
 */
mp_test_mode_t mp_test_mode_get(void);

/**
 ***************************************************************************************************
 * @brief 1-8 MP test mode check
 *
 * @param void
 *
 * @return void
 ***************************************************************************************************
 */
void mp_test_app_mode_check(void);

/**
 ***************************************************************************************************
 * @brief UART MP test mode check
 *
 * @param void
 *
 * @return void
 ***************************************************************************************************
 */
void mp_test_uart_mode_check(void);

/**
 ***************************************************************************************************
 * @brief HCI MP test mode check
 *
 * @param void
 *
 * @return void
 ***************************************************************************************************
 */
void mp_test_hci_mode_check(void);

/**
 ***************************************************************************************************
 * @brief Is MP test mode or not?
 *
 * @param void
 *
 * @return true: is mp test mode, false: isn't mp test mode
 ***************************************************************************************************
 */
bool mp_test_get_state(void);

/**
 ***************************************************************************************************
 * @brief UART hardware init for MP test
 *
 * @param void
 *
 * @return void
 ***************************************************************************************************
 */
void mp_test_uart_init(void);

/**
 ***************************************************************************************************
 * @brief Report the value of keyscan
 *
 * @param[in] key_val: value of a key
 *
 * @return void
 ***************************************************************************************************
 */
void mp_test_report_keyscan_data(uint16_t key_val);

/**
 ***************************************************************************************************
 * @brief Report the result of frequency calibration
 *
 * @param void
 *
 * @return void
 ***************************************************************************************************
 */
void mp_test_report_freq_calib_result(void);

void mp_test_enter_single_tone_mode_set(void);
void power_on_flag_check(void);

#endif /* CONFIG_MP_TEST_ENABLE */

#endif /* __APP_MP_H__ */
