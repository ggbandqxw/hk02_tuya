/**
 ****************************************************************************************
 *
 * @file app.h
 *
 * @brief Application entry point
 *
 * Copyright (C) RivieraWaves 2009-2015
 *
 *
 ****************************************************************************************
 */

#ifndef APP_H_
#define APP_H_

/**
 ****************************************************************************************
 * @addtogroup APP_SIMPLE_SERVER_APP app.h
 * @ingroup APP_SIMPLE_SERVER
 *
 * @brief Application entry point.
 *
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "rwip_config.h"     // SW configuration

#if (BLE_APP_PRESENT)

#include <stdint.h>          // Standard Integer Definition
#include <co_bt.h>           // Common BT Definitions
#include "arch.h"            // Platform Definitions
#include "gapc.h"            // GAPC Definitions

#if (NVDS_SUPPORT)
#include "nvds.h"
#endif // (NVDS_SUPPORT)

#include "app_adv.h"
#if (BLE_APP_WHITE_LIST)
#include "app_whl.h"
#endif
#if (BLE_APP_PRIVACY)
#include "app_privacy.h"
#endif

#if (CONFIG_MP_TEST_ENABLE)
#include "app_mp_test.h"
#if (CONFIG_FREQ_CALIB_ENABLE)
#include "app_freq_calib.h"
#endif
#endif

#include "ble_tys.h"
#include "ble_tys_task.h"
#include "app_tys.h"


/*
 * DEFINES
 ****************************************************************************************
 */

/// Maximal length of the Device Name value
#define APP_DEVICE_NAME_MAX_LEN      20 //(18)

/*
 * MACROS
 ****************************************************************************************
 */
#define APP_HANDLER(handler_name, msgid, param, dest, src) app_##handler_name##_handler(param)
/// the profile task handler list
#define APP_HANDLERS(subtask)    {&subtask##_msg_handler_list[0], ARRAY_LEN(subtask##_msg_handler_list)}
#define APP_HANDLERS2(subtask)   {&subtask##_msg_handler_list[0], 7}

enum APP_EVENT
{
    APP_EVT_CONNECTED,
    APP_EVT_DISCONNECTED,
    APP_EVT_BONDED,
    APP_EVT_CONN_PARAM_UPDATED,
};

/*
 * ENUMERATIONS
 ****************************************************************************************
 */

#if (NVDS_SUPPORT)
/// List of Application NVDS TAG identifiers
enum app_nvds_tag
{
    //tag
    NVDS_TAG_APP_FIRST = NVDS_TAG_APP_SPECIFIC_FIRST,

    // tag length
    NVDS_LEN_APP_FIRST   = 1,
};
#endif // (NVDS_SUPPORT)

/*
 * TYPE DEFINITIONS
 ****************************************************************************************
 */

/// Structure containing information about the handlers for an application subtask
struct app_subtask_handlers
{
    /// Pointer to the message handler table
    const struct ke_msg_handler *p_msg_handler_tab;
    /// Number of messages handled
    uint16_t msg_cnt;
};

///@struct app_env_tag 
///Application environment structure
struct app_env_tag
{
    /// Connection handle
    uint16_t conhdl;
    /// Connection Index
    uint8_t  conidx;

    /// Last initialized profile
    uint8_t next_svc;

    /// Device Name length
    nvds_tag_len_t dev_name_len;
    /// Device Name
    uint8_t dev_name[APP_DEVICE_NAME_MAX_LEN];

    // BT Address
    struct gap_bdaddr addr;

    /// Local device IRK
    uint8_t loc_irk[KEY_LEN];

    /// Counter used to generate IRK
    uint8_t rand_cnt;

    struct gapc_conn_param conn_param;

    uint8_t latency_disabled;

#if (CONFIG_MP_TEST_ENABLE)
    uint8_t uart_keyscan;
#if (CONFIG_FREQ_CALIB_ENABLE)
    uint8_t xtal32m_ctune;
    uint8_t freq_calib_start;
    uint16_t freq_local_handle;
    uint16_t freq_peer_handle;
    bool power_on_flag;
    struct gap_bdaddr addr;  // BT Address
    bool single_tone_test_enable;
#endif /* CONFIG_FREQ_CALIB_ENABLE */
#endif /* CONFIG_MP_TEST_ENABLE */
};

/*
 * GLOBAL VARIABLE DECLARATION
 ****************************************************************************************
 */

/// Application environment
#if (CONFIG_MP_TEST_ENABLE || CONFIG_FREQ_CALIB_ENABLE) 
extern void gapm_set_bdaddr(bd_addr_t *addr);
#endif 

extern struct app_env_tag app_env; /// Application environment

extern void gapm_set_bdaddr(bd_addr_t *addr);

void appm_reg_svc_itf(void);
bool appm_add_svc(void);
void lld_con_slave_latency_ignore(uint8_t link_id, bool ignore);

/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Initialize the BLE demo application.
 * @return void.
 ****************************************************************************************
 */
void appm_init(void);

/**
 ****************************************************************************************
 * @brief Add a required service in the database
 * @return true: add server success; false: add server fail;
 ****************************************************************************************
 */
bool appm_add_svc(void);

/**
 ****************************************************************************************
 * @brief Send to request to update the connection parameters
 * @param[in] conn_param: the connect param. see struct gapc_conn_param
 * @return void.
 ****************************************************************************************
 */
void appm_update_param(uint8_t conidx, struct gapc_conn_param * conn_param);

/**
 ****************************************************************************************
 * @brief Send a disconnection request
 * @param[in] conidx: the connect index.
 * @return void.
 ****************************************************************************************
 */
void appm_disconnect(uint8_t conidx, uint8_t reason);

/**
 ****************************************************************************************
 * @brief Retrieve device name
 *
 * @param[out] name: device name
 *
 * @return name length
 ****************************************************************************************
 */
uint8_t appm_get_dev_name(uint8_t* name);

#endif //(BLE_APP_PRESENT)

/// @} APP

#endif // APP_H_
