#ifndef __APP_FREQ_CALIB_H__
#define __APP_FREQ_CALIB_H__

#include <stdint.h>

void freq_calib_service_init(void);
void freq_calib_gattc_write(uint16_t handle, uint8_t *data, uint8_t len);
void freq_calib_connected_cb(void);
void freq_calib_disconnected_cb(uint8_t reason);
void freq_calib_gatt_write_cb(uint16_t handle, uint8_t *data, uint8_t len);

#endif
