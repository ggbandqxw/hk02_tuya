[2021-02-25] [HS6621A3][FreeRTOS] update A3 lib: pmu_power_status also consider RTOS support
[2021-02-27] [HS6621A3][FreeRTOS] update A3 lib: fix clearing PMU timer status may loop forerver issue
[2021-03-02] [HS6621CA2] update A2 lib: split connect_param and connect_update_ind flow to prevent conflict with encrypt flow for Honor8
[2021-03-02] [HS6621A3] update A3 lib: 1.use CRC confirm author point 2.fix rw-digital CRC-ok but length-err/llid-err issue 3.fix RW em buffer thread safety issue
[2021-03-04] [HS6621CA2] update A2 lib: 为了更好的兼容性PHY和data length不再根据GAPM_SET_DEV_CONFIG_CMD里的配置自动请求了
[2021-03-23] [HS6621A3][HS6621CA2] update lib: 硬件I2C加入超时机制，并且加入数字推荐的挂死解锁机制
[2021-04-17] [HS6621CA2] update A2 lib: 修复在蓝牙工作时flash写擦可能导致系统死机的问题（只有用ARMCC编译器会出问题）
[2021-05-18] [HS6621CA2] update A2 lib: 修复LDO_VCO在睡眠后未设置成模拟要求的新的值的问题，这个问题会有概率会导致某些芯片RF不工作

2021-06-01:
	* 问题描述: 在用ARMCLANG编译器时，随机数函数rand()每次上电返回固定数值
	* 具体原因: 随机数种子使用硬件随机数和flash的UID的异或计算得出的，因为UID的获取异常导致armclang的优化不对了。
	* 解决方案: 修复sf_read_uid()/sf_read_uid_ex()读取长度错误的问题
	* 影响芯片: HS6621A3/HS6621CA2/HS6621CB1
	* 修复版本: r2483

2021-06-01:
	* 问题描述: 小米遥控器在某些电视上出现语音异常、连接断开等问题
	* 具体原因: 遥控器在干扰大的时候会关闭高斯滤波，关高斯滤波会导致RF的path delay减少1us。
				某个版本去掉了PHY的AA_DETECT导致path delay的参数变动，而小米遥控使用了这个版本前的path delay参数，导致tifs异常
	* 解决方案: 为了防止以后别的应用出现类似问题，库里加入了增减path delay的函数：bb_txpath_dly_increase
	* 注意问题: 开关高斯的函数未在库中提供，一般不建议使用，如果需要，需要和研发联系。
	* 影响芯片: HS6621CA2/HS6621CB1
	* 修复版本: r2484/r2486

2021-06-01:
	* 新增功能: 添加FreeRTOS支持
	* 影响芯片: HS6621CA2
	* 添加版本: r2487

2021-06-16:
	* 问题描述: 跑64MHz主频时，调用pmu_force_reboot函数导致reboot死机
	* 具体原因: 跑64MHz主频时需要修改DVDD电压到1.15v，但是系统初始化时会重新加载DVDD电压的校准值到默认值1.0v
	* 解决方案: pmu_force_reboot中调用pmu_xtal32m_x2_close来关闭64MHz
	* 影响芯片: HS6621CA2
	* 修复版本: r2497

2021-06-18:
	* 问题描述: 钟科反馈客户用6621CG跑在64MHz主频时，出现有些芯片在太阳晒的时候温度升高，导致死机。跟着芯片走，其他芯片没问题。
	* 具体原因: 跑64MHz主频时会修改DVDD电压到1.15V，这时候RAM的timming还是1.0V的，导致RAM有概率出问题，高温加剧了这个问题
	* 解决方案: 跑64MHz主频时同时修改RAM的timming到1.1V
	* 影响芯片: HS6621CA2
	* 修复版本: r2499

2021-07-02:
	* 问题描述: HS6621C在跑2.4G代码时出现有些芯片RF异常
	* 具体原因: 之前发现的LDO_VCO的问题，2.4G没有覆盖到
	* 解决方案: 将LDO_VCO的控制挪到pmu_ana_enable(true, PMU_ANA_RF)函数中, 并且其调用要后于calib_rf_restore()
	* 影响芯片: HS6621CA2/HS6621CB1
	* 修复版本: r2520/r2521

2021-07-16:
	* 问题描述: HS6621C鼠标在跑2.4G模式睡眠后出现距离近的问题
	* 具体原因: 射频参数里的DCOC的一个表恢复的时候，需要打开某个时钟才行。
	* 解决方案: 射频校准数据恢复时再打开这个时钟即可。
	* 影响芯片: HS6621CA2/HS6621CB1
	* 修复版本: r2540

2021-07-29:
	* 问题描述: HS6621C芯片在使用过程中发现如果GPIO9没有使用的话，第二次进睡眠功耗会高8~9uA。第一次睡眠功耗没问题。
	* 具体原因: 唤醒后GPIO9的pinmux恢复错误，软件维护的默认pinmux表和实际的有差别，导致唤醒后pinmux值变成debug mode导致。之前也发现类似的问题，但是测试时只测试了第一次睡眠。
	* 解决方案: 修改软件维护的默认的pinmux表
	* 影响芯片: HS6621CA2/HS6621CB1
	* 修复版本: r2553

2021-10-25:
	* 问题描述: HS6621C芯片使用xtal32k快速上下电异常，且进入睡眠异常
	* 具体原因: porb信号产生依赖于rc32k，如果关闭rc32k，快速上下电可能造成死锁。参考肖金红20210724和20210726邮件
	* 解决方案: rc32k常开永远不关，且pd_ckopmu32k永远保持0
	* 影响芯片: HS6621CA2
	* 修复版本: r2607

2021-10-27:
	* 问题描述: GCC不带FPU的库和带FPU的工程没法一起编译，这点和KEIL不同
	* 解决方案: 编译的库中带FPU参数：-mfloat-abi=hard -mfpu=fpv4-sp-d16
	* 影响芯片: ALL
	* 修复版本: r2612

2021-11-09:
	* 新增内容: 增加通用RTOS接口: CMSIS RTOS2 (基于FreeRTOS), Demo工程在：examples/ble_app_simple_server_cmsisos2_freertos
	* 新增版本: r2627

2021-11-09:
	* 修改内容: 所有Example的工程目录或者工程名中包含"_hs6621c_"统一改成"_hs6621x_", 防止客户在使用hs6621p芯片时因为工程名而产生困惑
				带hs6621标识的工程只给hs6621系列芯片使用，带hs6621x标识的芯片提供给hs6621c和hs6621p系列芯片使用
	* 修改版本: r2628

2021-12-9:
	* 问题描述: 测试时发现co_timer的定时可能比RTC慢，测试时发现pmu_timer_time()获取的HS_PMU->TIMER_CNT寄存器可能读取到错误的值
	* 解决方案: 多次读取HS_PMU->TIMER_CNT寄存器，直到连续读到2次相同值，才认为读到的值正确
	* 影响芯片: ALL
	* 修复版本: r2664

2021-12-30:
	* 问题描述: 看门狗复位后，芯片不不能睡眠（每次芯片睡眠后就马上唤醒）
	* 解决方案: 每次复位，软件强制将HS_PMU->WDT_STATUS写0
	* 影响芯片: HS6621CB/HS6621P
	* 修复版本: r2677

2022-03-08:
	* 问题描述: 优化FT ADC程序
	* 解决方案: 优化FT ADC程序
	* 影响芯片: HS6621P
	* 修复版本: r2741

2022-03-08:
	* 问题描述: 部分手机在连接间隔比较小的情况下连接稳定性比较差，容易出现断链
	* 解决方案: 蓝牙硬件的一个唤醒时间需要调整到更大值，以防止蓝牙唤醒时出现有段时间丢时钟以至于断链的问题
	* 影响芯片: HS6621P
	* 修复版本: r2741

2022-04-19:
	* 问题描述: 多广播多连接应用中，可能导致最后广播无法正常工作
	* 解决方案: 蓝牙协议栈考虑这种情况
	* 影响芯片: ALL
	* 修复版本: r2759

2022-05-16:
	* 问题描述: NVDS可能返回0x02错误，导致NVDS读写出错
	* 解决方案: crc检查全0数据会返回0，这样就无法识别出是错误的tag，造成后续tag的查询出错. 检查时同时判断SN不能为0
	* 影响芯片: OM6621CB/OM6621P
	* 修复版本: r2781

2022-05-16:
	* 问题描述: 增加ADC的gain=0.25,vcm=750mV校准的校准数据
	* 影响芯片: OM6621P
	* 修复版本: r2786

2022-08-20:
	* 问题描述: 修复连接状态下空包读取的RSSI值异常的问题
	* 影响芯片: ALL
	* 修复版本: r2917

2022-10-19:
	* 问题描述: 蓝牙异常断开，通过sniffer看是锚点偏了
	* 解决方案: 睡眠时将RW的时钟切换到RC32M就可以解决问题，否则如果芯片没有真正睡眠，导致数字时钟切换异常，以至于连接断开
	* 影响芯片: OM6621CC2/OM6621P
	* 修复版本: r2950

2022-11-09:
	* 问题描述: OpenHarmony支持需求
	* 解决方案: 增加liteos_m kernel及bounds_checking_function组件，kernel base commit id：3f54fdc8984c28f4b90203a2983e819bc74357ca
	* 影响芯片: OM6621P
	* 修复版本: r2965
