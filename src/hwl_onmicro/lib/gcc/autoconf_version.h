/**
 * @file autoconf_version.h
 * @brief
 * @date Wed 17 Feb 2021 10:15:03 AM CST
 * @author liqiang
 *
 * @defgroup
 * @ingroup
 * @brief
 * @details
 *
 * @{
 */

#ifndef __AUTOCONF_VERSION_H__
#define __AUTOCONF_VERSION_H__

#ifdef __cplusplus
extern "C"
{ /*}*/
#endif

/*********************************************************************
 * INCLUDES
 */


/*********************************************************************
 * MACROS
 */
#define CONFIG_HS6621CC2_RELEASE
#define CONFIG_USER_APP
#ifndef CONFIG_XIP_FLASH_ALL
#define CONFIG_XIP_FLASH_ALL
#endif
/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * EXTERN VARIABLES
 */


/*********************************************************************
 * EXTERN FUNCTIONS
 */


#ifdef __cplusplus
/*{*/ }
#endif

#endif

/** @} */

