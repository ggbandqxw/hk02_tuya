
该SDK现支持如下芯片：
- HS6621
- HS6621C
- HS6621CB
- HS6621CC
- OM6621P

通过lib文件夹下的switch_to_xxx.xxx来切换不同芯片:
- switch_to_a3.xxx  切换lib以支持HS6621 A3芯片
- switch_to_c2.xxx  切换lib以支持HS6621C A2芯片
- switch_to_cb1.xxx 切换lib以支持HS6621C B1芯片
- switch_to_cc2.xxx 切换lib以支持HS6621C C2芯片
- switch_to_p1.xxx  切换lib以支持OM6621P A1芯片

通过switch_to_xxx修改的库文件请参看其脚本内的描述

各个芯片的库文件主要包括：
- ROM中程序的符号: hs6621x_rom_peripheral_xx.symdefs, hs6621x_rom_xx.lib
- 驱动编译的库文件：hs6621x_rom_peripheral_ext_xx.lib, hs6621x_lib_xx.lib

如果库和ROM里的驱动不能满足需求，可按如下操作加入程序源文件：
- 删除 lib/keil/xxx/hs6621x_rom_peripheral_xx.symdefs 中相关的符号
- 删除 lib/keil/xxx/hs6621x_rom_xx.lib 中相关的符号
- 删除 lib/keil/xxx/armlinkx_xxx.via 中相关的.o文件
- hardware/peripheral文件夹下有所有驱动的源代码, 加入工程即可
注意：pmu.c calib.c calib_repair.c cpm.c pinmux.c sf_base.c属于系统外设，关联较多，轻易不要加入源代码使用，仅做参考之用。

