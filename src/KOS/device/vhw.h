/**
  * @file 		vhw.h
  * @brief		主要定义底层硬件设备类型以及虚拟设备
  *
  * @copyright  Copyright (c) 2017~2020 ShenZhen Dxtc Technology Co., Ltd. 
  * All rights reserved.
  * 
  * @version    v3.1
  * @author		FengLi
  * @date 		2021-08-02
  * 
  * @note		鼎新同创·智能锁
  * 
  * @par 修改日志:
  * <1> 2021/08/02 v3.1 FengLi 创建初始版本
  *******************************************************************/
#ifndef __VHW_H__
#define __VHW_H__

/** @brief   底层硬件设备类型 */
typedef enum
{
    vPIN       = 0x00, ///< IO设备
    vUART      = 0x01, ///< 串口设备
    vSPI       = 0x02, ///< SPI设备
    vIIC       = 0x03, ///< IIC设备
    vADC       = 0x04, ///< ADC设备
    vPWM       = 0x05, ///< PWM设备
    vIPC       = 0x06, ///< IPC设备
    vCRYPTO    = 0x07, ///< 硬件加解密设备
    vEEPROM    = 0x08, ///< 存储设备
    vCAP_SENSE = 0x09, ///< 电容感应设备
    vTIMER     = 0x0A, ///< 定时器设备
    vCPU       = 0x0B, ///< CPU设备
    vRTC       = 0x0C, ///< RTC设备
    vAUDIO     = 0x0D, ///< AUDIO设备
    vFLASH     = 0x0E, ///< FLASH设备
	vCOUNTER   = 0x0F, ///< COUNTER设备
    vBLE       = 0x10, ///< BLE设备
    vWIFI      = 0x11, ///< WIFI设备
    vNETWORK   = 0x12, ///< 网络设备
    vCAMERA    = 0x13, ///< 摄像头设备
    vDEVICE_QTY,
}DeviceType_enum_t;

/** @brief   虚拟设备（具体关联到哪个硬件由底层驱动程序去映射） */
typedef enum
{
    vPIN_L0 = vPIN << 8, ///< LED0
    vPIN_L1,             ///< LED1
    vPIN_L2,             ///< LED2
    vPIN_L3,             ///< LED3
    vPIN_L4,             ///< LED4
    vPIN_L5,             ///< LED5
    vPIN_L6,             ///< LED6
    vPIN_L7,             ///< LED7
    vPIN_L8,             ///< LED8
    vPIN_L9,             ///< LED9
    vPIN_L10,            ///< LED*
    vPIN_L11,            ///< LED#
    vPIN_L12,            ///< LED_OPEN
    vPIN_L13,            ///< LED_CLOSE
    vPIN_L14,            ///< LED_RED
    vPIN_L15,            ///< LED_GREEN
    vPIN_L16,            ///< LED_BAT  电量灯-白灯
    vPIN_L17,            ///< LED_FS
    vPIN_L18,            ///< 键盘整体背光控制
    vPIN_L19,            ///< LED_LP   低电量灯-红灯
    vPIN_L20,            ///< 红灯-笑脸
    vPIN_L21,            ///< 绿灯-笑脸
    vPIN_L22,            ///< 蓝灯-笑脸
    vPIN_L23,            ///< 上锁按键灯
    vPIN_L24,            ///< LED_BACK_R 后板红灯
    vPIN_L25,            ///< LED_BACK_G 后板绿灯
    vPIN_L26,            ///< LED_BELL 前板门铃灯
    vPIN_L27,            ///< 门磁蓝牙配网灯
    vPIN_L28,            ///< 屏幕按钮白灯
    vPIN_L29,            ///< 门铃灯
    vPIN_L30,            ///< 刷卡指示灯
    vPIN_L31,            ///< 白灯-logo
    vPIN_L32,            ///< 红灯-logo
    vPIN_L33,            ///< 绿灯-logo
    vPIN_L34,            ///< 蓝灯-logo
    
    vPIN_C0,             ///< 控制类IO, 光耦电源控制
    vPIN_C1,             ///< 语音使能脚
    vPIN_C2,             ///< MOTO_NSLEEP脚控制
    vPIN_C3,             ///< EN_ADC 电池采样电路使能
    vPIN_C4,             ///< 卡片CS
    vPIN_C5,             ///< 卡片复位控制NPD
    vPIN_C6,             ///< IR电源控制
    vPIN_C7,             ///< FACE POWER
    vPIN_C8,             ///< XUN-MEI POWER
    vPIN_C9,             ///< EN_RTC_POWER
    vPIN_C10,            ///< MOTO_R
    vPIN_C11,            ///< MOTO_F
    vPIN_C12,            ///< 后板锁体电源控制
    vPIN_C13,            ///< Finger-CS
    vPIN_C14,            ///< Finger-RST
    vPIN_C15,            ///< DSENSOR_POWER
    vPIN_C16,            ///< DSENSOR_EN
    vPIN_C17,            ///< TDK_PROG
    vPIN_C18,            ///< TDK_INT_DIR
    vPIN_C19,            ///< 语音电源使能脚
    vPIN_C20,            ///< 后板门铃控制脚
    vPIN_C21,            ///< TRWO1090——POWER-EN电源控制脚
    vPIN_C22,            ///< oled-cs
    vPIN_C23,            ///< oled-rst
    vPIN_C24,            ///< oled-da
    vPIN_C25,            ///< oled-power
    vPIN_C26,            ///< STK-power
    vPIN_C27,            ///< 指静脉Fingervein-power,单独IO口控制，(飞利浦901VP，指静脉代替指纹)
    vPIN_C28,            ///< 指静脉Fingervein-power,595控制，(KDS的K8603，指静脉与指纹共存)    
    vPIN_C29,            ///< fpt-power enable. (屏下指纹的电源使能)
    vPIN_C30,            ///< 掌静脉的电源控制脚  
    vPIN_C31,            ///< 功放静音控制脚
    vPIN_C32,            ///< 人脸和掌静脉的5V电源控制脚
    vPIN_C33,            ///< lcd背光灯
    vPIN_C34,            ///< 电池iic-SDA
    vPIN_C35,            ///< 电池iic-SCL
    vPIN_C36,            ///< NB_N306 NB模组唤醒口
    vPIN_C37,            ///< NB_N306 NB模组复位口
    vPIN_C38,            ///< TOUCH-RST
    vPIN_C39,            ///< AUDIO_ONELIN DATA
    vPIN_C40,            ///< AUDIO_ONELIN Busy
    vPIN_C41,            ///< 福电池供电使能脚  
    vPIN_C42,            ///< 主电池给电机供电
    vPIN_C43,            ///< 副电池给电机供电
    vPIN_C44,            ///< 扩展模块复位脚 Zwave
    vPIN_C45,            ///< 电机供电电压控制脚（电压高时降压）
    vPIN_C46,            ///< 唤醒MC60模组的引脚
    //vPIN_C47,            ///< 控制MC60模组供电的引脚

    vPIN_B9,             ///< SET  后板设置键
    vPIN_B10,            ///< RESET  恢复出厂键
    vPIN_B11,            ///< CLOSE  关锁键（单机关锁、双击开锁、长按反锁）
    vPIN_B12,            ///< OPEN   开锁键（双击开锁）
    vPIN_B13,            ///< DAMAGE 防撬按键
    vPIN_B14,            ///< M-KEY  机械钥匙
    vPIN_B15,            ///< COVER CHECK 滑盖
    vPIN_B16,            ///< 蓝牙配对绑定按键
    vPIN_B20,            ///< 上锁光耦
    vPIN_B21,            ///< 开锁光耦
    vPIN_B22,            ///< 上锁霍尔-left
    vPIN_B23,            ///< 上锁霍尔-right
    vPIN_B24,            ///< 开锁霍尔

	vPIN_B25,            ///< 叉舌感应 门状态检测SLOT
    vPIN_B26,            ///< 主锁舌缩进感应(开锁到位感应) 开锁到位检测OP2
    vPIN_B27,            ///< 主锁舌伸出感应(上锁到位感应) 上锁到位检测OP3
    vPIN_B28,            ///< 回拖感应 回拖检测OP1
    
    vPIN_B29,            ///< 单独的触摸门铃
    vPIN_B30,            ///< 门磁霍尔感应
    vPIN_B31,            ///< 门磁霍尔感应2
    vPIN_B32,            ///< 屏幕电容按键
    vPIN_B33,            ///< 上锁光耦2
    vPIN_B34,            ///< Homekit按键

    vPIN_I0,             ///< Gsensor-INT
    vPIN_I1,             ///< os2os-WAKE
    vPIN_I2,             ///< Extend-INT
    vPIN_I3,             ///< VIDEO-INT
    vPIN_I4,             ///< Card-INT
    vPIN_I5,             ///< Finger-INT
    vPIN_I6,             ///< TOUCH-INT1
    vPIN_I7,             ///< TOUCH-INT2
    vPIN_I8,             ///< ZYM-INT2
    vPIN_I9,             ///< HALL_MOTO-INT2
    vPIN_I10,            ///< HALL_KN-INT2
    vPIN_I11,            ///< BAT-INT
    vPIN_I12,            ///< DSENSOR-INT
    vPIN_I13,            ///< TRWO1090-INT接近感应的中断管脚
    vPIN_I14,            ///< USB的电源IO检测
    vPIN_I15,            ///< 触摸板的中断
    vPIN_I16,            ///< 独立门铃触摸芯片中断
    vPIN_I17,            ///< 指静脉中断Fingervein-INT
    vPIN_I18,            ///< PIR-INT
    vPIN_I19,            ///< PG_LCD_INT,显示屏的中断脚
    vPIN_I20,            ///< 米家模块唤醒中断检测
    vPIN_I21, 			 ///< TSM12-INT
    vPIN_I22, 			 ///< MC60-INT, MC60唤醒mcu的引脚
    vPIN_EXT,            ///< AW9523B-INT脚 

    vPIN_PX,             ///< 特殊虚拟PIN设备（与磐芯触摸芯片通信的接口、读磐芯版本号）

    vPIN_E1,             ///< 从设备使能脚（短连接WIFI模块电源使能）

    vUART_0 = vUART << 8,///< LOG DEBUG
    vUART_1,             ///< os2os接口
    vUART_2,             ///< Extend_kds
    vUART_3,             ///< FACE
    vUART_4,             ///< XUN-MEI
	vUART_5,			 ///< 升级串口,USB虚拟串口
    vUART_6,			 ///< 触摸板串口
    vUART_7,			 ///< 指静脉Fingervein串口
    vUART_8,			 ///< 屏下指纹串口
    vUART_9,             ///< PG_LCD的B2B通信口
    vUART_10,            ///< F331 指纹模组
    vUART_11,            ///< 掌静脉串口
    vUART_12,            ///< MC60 Homekit模组串口

    vSPI_0 = vSPI << 8,  ///< Card
    vSPI_1,              ///< Finger
    vSPI_2,              ///< oled
    vSPI_3,              ///< lcd

    vIIC_0 = vIIC << 8,  ///< Gsensor
    vIIC_1,              ///< Msensor_left
    vIIC_2,              ///< Msensor_right
    vIIC_3,              ///< DSENSOR-IIC 距离传感器（AMS/TDK）
    vIIC_4,              ///< PSENSOR-IIC 位置传感器（智游者）
    vIIC_5,              ///< 电池iic-航芯电池加密-模拟的iic
    vIIC_6,              ///< 飞睿5.8G雷达
    vIIC_7,              ///< 电池iic-英飞凌的电池加密
    vIIC_8,              ///< TSM12_HWL
    vIIC_9,              ///< 模拟IICA IO
    vIIC_10,             ///< 模拟IICB IO

    vADC_0 = vADC << 8,  ///< 电池电量采集
    vADC_1,              ///< 电机检测
    vADC_2,              ///< 卡片检测
	vADC_3,              ///< 应急电源检测
    vADC_4,              ///< PIR检测

    vPWM_0 = vPWM << 8,  ///< Beep
    vPWM_1,              ///< MOTO_A PWM
    vPWM_2,              ///< MOTO_B PWM
    vPWM_3,              ///< 笑脸灯PWM

    vIPC_0 = vIPC << 8,  ///< Extend_ipc
    vIPC_1,              ///< os2os接口

    vTIMER_0 = vTIMER << 8,        	///< 系统滴答定时器（1ms）
    vTIMER_1,                      	///< 低功耗定时器（300ms）
    vTIMER_2,                      	///< 普通定时器（1ms）
   
    vCPU_0 = vCPU << 8,            	///< CPU设备
   
    vEEPROM_0 = vEEPROM << 8,      	///< NV存储设备
    vEEPROM_1,      	            ///< OTA 数据缓存区

	vFLASH_0 = vFLASH << 8,         ///< 语音数据存储FLASH
	vFLASH_1,      				    ///< 指纹数据存储FLASH
	vFLASH_2,						///< MCU内部FLASH
   
    vRTC_0 = vRTC << 8,            	///< RTC设备
    
    vCAP_SENSE0 = vCAP_SENSE << 8, 	///< 电容感应设备（键盘）
    vCAP_SENSE1, 	                ///< 电容感应设备（门铃键）
    vCAP_SENSE2,                    ///< TSM12设备
    
    vAUDIO_0 = vAUDIO << 8,         ///< AUDIO设备
	vAUDIO_1,                       ///< AUDIO一线设备
    
	vCOUNTER_0 = vCOUNTER << 8,		///< 计数器设备
	
    vBLE_0 = vBLE << 8,             ///< 蓝牙设备（vBLE_0用来给BLE组件与手机交互）
    vBLE_1,                         ///< 蓝牙设备（其他都是用来给KOS通信用的、连接子设备）
    vBLE_2,                         ///< 蓝牙设备
    vBLE_3,                         ///< 蓝牙设备
    vBLE_4,                         ///< 蓝牙设备

    vWIFI_0 = vWIFI << 8,           ///< WIFI设备
    vWIFI_1,                        ///< WIFI配网

    vNETWORK_0 = vNETWORK << 8,     ///< 网络设备

    vCAMERA_0 = vCAMERA << 8,       ///< 摄像头设备

	vNOHARDWARE = 0XFFFF,			///< 无设备
}VirtualHardware_enum_t;

#endif
