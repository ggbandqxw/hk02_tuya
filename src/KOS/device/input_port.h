/**
  * @file 		input_port.h
  * @brief		输入端口扫描 底层驱动程序
  *
  * @copyright  Copyright (c) 2017~2020 ShenZhen Dxtc Technology Co., Ltd. 
  * All rights reserved.
  * 
  * @version    v3.1
  * @author		FengLi
  * @date 		2020-12-03
  * 
  * @note		鼎新同创·智能锁
  * 
  * @par 修改日志:
  * <1> 2020-12-03 v3.1 FengLi 创建初始版本
  *******************************************************************/
#ifndef __INPUT_PORT_H__
#define __INPUT_PORT_H__

#include "vhw.h"

#define INPUT_PORT_RELEASE_MIN_TIME (50)  ///< 默认的连按时间限制——50MS  最小松开时间
#define INPUT_PORT_RELEASE_MAX_TIME (500) ///< 默认的连按时间限制——500MS 最大松开时间

/** @brief   默认的长按保持时间 */
#define INPUT_PORT_LONG_PRESS_TIME  (1000)

#define INPUT_PORT_LOGIC_LOW        (0) ///< 端口逻辑（有效电平）——低电平有效
#define INPUT_PORT_LOGIC_HIGH       (1) ///< 端口逻辑（有效电平）——高电平有效 
 
#define INPUT_PORT_FUNC_SINGLE      (0) ///< 端口功能配置——单一功能按钮（支持：按下、松开、长按功能；动作产生立即生效）
#define INPUT_PORT_FUNC_MULTI       (1) ///< 端口功能配置——多功能按钮  （支持：按下、松开、长按、连按功能；松手动作会有几百ms的延迟）
#define INPUT_PORT_FUNC_SPECIAL     (2) ///< 端口功能配置——特殊功能按钮（支持：按下、松开、长按功能；动作产生立即生效）

#define INPUT_PORT_STA_UNKNOW       (0) ///< 输入端口状态——未知
#define INPUT_PORT_STA_RELEASE      (1) ///< 输入端口状态——松开
#define INPUT_PORT_STA_PRESS        (2) ///< 输入端口状态——按下
#define INPUT_PORT_STA_LONG_PRESS   (3) ///< 输入端口状态——长按


/** @brief   输入端口结构 */
typedef struct
{
    char *name;                 ///< 端口名称
    VirtualHardware_enum_t pin; ///< 管脚号
    uint8_t logic;              ///< 端口逻辑（高电平有效 或 低电平有效）
    uint8_t func;               ///< 端口功能（单一功能，多功能）
}InputPort_stu_t;


/** @brief   输入端口回调函数类型 */
typedef void (*InputPortCB_fun_t)(char *name, uint8_t status, uint8_t times);


/* 给组件层调用的接口 */
void InputPort_EnableProt(char *name);
void InputPort_DisableProt(char *name);
ErrorStatus InputPort_SetProtTimeInterval(char *name, uint16_t minTime, uint16_t maxTime);
ErrorStatus InputPort_SetProtLongPressTime(char *name, uint16_t time);
ErrorStatus InputPort_AddCtrlLine(char *name, uint32_t interval, VirtualHardware_enum_t  ctrl_pin, uint8_t logic);
ErrorStatus InputPort_SetCtrlLine(char *name, uint32_t active);
ErrorStatus InputPort_Registered(InputPort_stu_t *port, uint8_t portQty, InputPortCB_fun_t pCallback);


/* 给设备层调用的接口 */
void InputPort_Scan(uint8_t mask, uint16_t interval);
uint32_t InputPort_CheckSpecialGpio(void);

#endif /* __INPUT_PORT_H__ */
