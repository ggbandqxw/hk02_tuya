/**
  * @file 		device.h
  * @brief		设备层头文件，主要说明端口的输入输出类型，以及相关函数的定义。
  *
  * @copyright  Copyright (c) 2017~2020 ShenZhen Dxtc Technology Co., Ltd. 
  * All rights reserved.
  * 
  * @version    v3.1
  * @author		FengLi
  * @date 		2021-08-02
  * 
  * @note		鼎新同创·智能锁
  * 
  * @par 修改日志:
  * <1> 2021/08/02 v3.1 FengLi 创建初始版本
  *******************************************************************/
#ifndef __DEVICE_H__
#define __DEVICE_H__

#include "stdint.h"
#include "vhw.h"
#include "osal.h"
#include "input_port.h"
#include "build_info.h"
#include IOCFG_H



/** @brief   PIN-MODE */
typedef enum
{
    PIN_MODE_INPUT_ANALOG = 0x10, ///< 模拟高阻
    PIN_MODE_INPUT_PULLUP,        ///< 输入上拉
    PIN_MODE_INPUT_PULLDOWN,      ///< 输入下拉
    PIN_MODE_INPUT_HIGHZ,         ///< 输入浮空（数字高阻）
    PIN_MODE_S_DRIVE,             ///< 推晚输出 Strong Drive.
    PIN_MODE_OD_DRIVE,            ///< 开漏输出 Open Drain Drives
}PinMode_enum_t;

/** @brief   设备控制块数据结构  */
typedef struct
{
    uint32_t devParam;  ///< 设备参数

   /** @brief 
     * 每个设备都有enable接口, 组件层执行启动事件时，会调用底层的enable接口
     */
    int8_t (*enable)(VirtualHardware_enum_t dev);

    /** @brief 
     * 每个设备都有disable接口，组件层执行休眠事件时，会调用底层的disable接口
     */
    int8_t (*disable)(VirtualHardware_enum_t dev);
    
    /** @brief   大部分设备有read接口，不同的设备，param参数含义会不一样  */
    int32_t (*read)(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param);
    /** @brief  大部分设备有wirte接口，不同的设备，param参数含义会不一样  */
    int32_t (*write)(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param);

    int8_t (*set_uart_baudrate)(VirtualHardware_enum_t dev, uint32_t baudrate);          ///< set_uart_baudrate是UART设备专用接口
    int8_t (*set_pwm)(VirtualHardware_enum_t dev, uint32_t frequency, float duty_cycle); ///< set_pwm是PWM设备专用接口
    int8_t (*crypto)(Crypto_enum_t param, void *src, uint32_t len, void *dst, void *key);///< crypto是加解密设备专用接口
}Device_stu_t;


/* 给组件层调用的接口 */
int8_t Device_RegisteredCB(VirtualHardware_enum_t dev, void (*cb)(VirtualHardware_enum_t, void *, uint32_t));
int8_t Device_ConfigCB(VirtualHardware_enum_t dev, FunctionalState status);
int8_t Device_Enable(VirtualHardware_enum_t dev);
int8_t Device_Disable(VirtualHardware_enum_t dev);
int32_t Device_Read(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param);
int32_t Device_Write(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param);
int8_t Device_SetUartBaudrate(VirtualHardware_enum_t dev, uint32_t baudrate);
int8_t Device_SetPwm(VirtualHardware_enum_t dev, uint32_t frequency, float duty_cycle);
int8_t Device_Crypto(Crypto_enum_t param, void *src, uint32_t len, void *dst, void *key);


/* 给底层驱动调用的接口 */
Device_stu_t *Device_GetDeviceCtrlBlock(DeviceType_enum_t dev_type);
void Device_IRQHandler(VirtualHardware_enum_t dev, void *data, uint32_t len);


/* 其他接口 */
void Device_Init(void);
void Device_ClearInterrupt(void);
uint32_t Device_DeepSleep(uint32_t time);
void Device_EnterCritical(void);
void Device_ExitCritical(void);


/* CPU操作指令和相关接口 */
#define CPU_DELAYUS           1  ///< 延时微秒
#define CPU_DELAYMS           2  ///< 延时毫秒
#define CPU_SLEEP             3  ///< 休眠
#define CPU_RESET             4  ///< 复位
#define CPU_FEEDDOG           5  ///< 喂狗
#define CPU_KILLDOG           6  ///< 杀狗
#define CPU_PEND_SV           7  ///< PendSV

#define Device_FeedDog()      Device_Write(vCPU_0, (void *)CPU_FEEDDOG, 0, 0)  ///< 喂狗
#define Device_KillDog()      Device_Write(vCPU_0, (void *)CPU_KILLDOG, 0, 0)  ///< 杀狗
#define Device_CpuReset(m)    Device_Write(vCPU_0, (void *)CPU_RESET,   0, m)  ///< cpu-reset
#define Device_CpuSleep(ms)   Device_Write(vCPU_0, (void *)CPU_SLEEP,   0, ms) ///< cpu-sleep
#define Device_DelayUs(us)    Device_Write(vCPU_0, (void *)CPU_DELAYUS, 0, us) ///< 阻塞微秒延时
#define Device_DelayMs(ms)    Device_Write(vCPU_0, (void *)CPU_DELAYMS, 0, ms) ///< 阻塞毫秒延时
#define Device_TrigPendSV()   Device_Write(vCPU_0, (void *)CPU_PEND_SV, 0, 0)  ///< 触发pendSV

#endif /* __DEVICE_H__ */
