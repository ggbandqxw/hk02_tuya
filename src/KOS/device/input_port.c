/**
  * @file 		input_port.c
  * @brief		输入端口扫描 底层驱动程序
  *
  * @copyright  Copyright (c) 2020~2030 ShenZhen Dxtc Technology Co., Ltd. 
  * All rights reserved.
  *
  * @version	v3.1
  * @author		FengLi
  * @date 		2018-12-18
  *
  * @note		占用了一个定时器资源，使用TIMER3定时扫描端口状态
  *             实现消抖、长按、短按、单机、双击等状态检测
  * 
  * @par 修改日志:
  * <1> 2018/12/18 v3.1 FengLi 创建初始版本
  *******************************************************************/
#include <stdio.h>
#include "device.h"
#include "input_port.h"

/** @brief   最大可以注册12个输入端口 */
#define	INPUT_PORT_MAX_QTY    12

/** @brief   读取输入端口硬件状态 */
#define INPUT_PORT_READ_GPIO(pin, logic) ((logic == INPUT_PORT_LOGIC_LOW) ? Device_Read(pin, NULL, 0, 0) : !Device_Read(pin, NULL, 0, 0))

/** @brief   端口控制线的操作 */
#define INPUT_PORT_READ_CTRL_LINE                 INPUT_PORT_READ_GPIO
/** @brief   端口控制线 输出使能，logic为有效电平 */
#define INPUT_PORT_CTRL_LINE_DISABLE(pin, logic)  ((logic == INPUT_PORT_LOGIC_LOW) ? Device_Write(pin, NULL, 0, 1) : Device_Write(pin, NULL, 0, 0))
/** @brief   端口控制线 输出禁能，logic为有效电平 */
#define INPUT_PORT_CTRL_LINE_ENABLE(pin, logic)   ((logic == INPUT_PORT_LOGIC_LOW) ? Device_Write(pin, NULL, 0, 0) : Device_Write(pin, NULL, 0, 1))
 

/** @brief   端口控制线结构 */
typedef struct 
{
    VirtualHardware_enum_t ctrl_pin;  ///< 虚拟引脚（设备）
    uint8_t  logic;      ///< 控制线逻辑（高电平有效 或 低电平有效）
    uint16_t active;     ///< 活跃时间
    uint16_t interval;   ///< 扫描间隔
    uint16_t itv_count;  ///< 扫描间隔计数器
}InputPort_CtrlLine_stu_t;

/** @brief   输入端口硬件信息结构 */
static struct
{
    volatile InputPort_stu_t port;          ///< 端口
    volatile InputPort_CtrlLine_stu_t ctrl; ///< 端口控制线
    volatile FunctionalState en;            ///< 端口使能状态（有些端口需要动态控制，使能或者禁能）

    volatile uint8_t gpioBuf; ///< io-buf
    volatile uint16_t timing; ///< 计时器

    volatile InputPortCB_fun_t pFunction;   ///< 端口回调
    volatile uint8_t status;                ///< 端口当前状态（松开、按下、长按）
    volatile uint8_t times;                 ///< 连按次数（0:单击、1:双击、2:三击 …… ）

    uint16_t releaseMinTime;///< 最小松手时间
    uint16_t releaseMaxTime;///< 最大松手时间

    uint16_t longPressTime; ///< 长按保持时间
} InputPortInfo[INPUT_PORT_MAX_QTY];




static int s_strcmp(const char *s1, const char *s2)
{
    if (s1 == NULL || s2 == NULL)
    {
        return -1;
    }
    else
    {
        return strcmp(s1, s2);
    }
}

//! //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
  * @brief  控制线处理
  * @details 详细说明:无
  *         
  * @param[in]  pCtrl：控制线端口信息
  * @param[in]  iobuf：当前这个输入端口的buf数据
  *         
  * @return 【1】：不扫描  【0】：扫描
  */
static int InputPort_CtrlLineProcess(InputPort_CtrlLine_stu_t *pCtrl, uint8_t iobuf)
{
    /* 当前端口处于非活跃状态（活跃时间为0） */
    if (pCtrl->active == 0)
    {
        if (pCtrl->interval == 0xFFFF)
        {
            return 1;//< 非活跃状态下interval为FFFF：表示不扫描
        }

        if (++pCtrl->itv_count < pCtrl->interval)
        {
            return 1;
        }
    }

    /* 当前端口处于活跃状态、或者处于非活跃状态下但是扫描interval时间到了 */
    if (INPUT_PORT_READ_CTRL_LINE(pCtrl->ctrl_pin, pCtrl->logic) == 0)//< 当前控制线是使能状态
    {
        if (pCtrl->active == 0xFFFF)
        {
            return 0;//< 永久活跃
        }
        else if (pCtrl->active > 0)
        {
            pCtrl->active--;
        }

        if ((pCtrl->active == 0) && (iobuf == 0xff || iobuf == 0))
        {
            INPUT_PORT_CTRL_LINE_DISABLE(pCtrl->ctrl_pin, pCtrl->logic);
            pCtrl->itv_count = 0;
        }
        return 0;//< 正常扫描
    }
    else
    {
        INPUT_PORT_CTRL_LINE_ENABLE(pCtrl->ctrl_pin, pCtrl->logic);
        return 1;//< 当这次扫描，不处理
    }
}

/**
  * @brief   输入端口扫描
  * @details 详细说明:该函数在定时器中断里面周期调用，实现端口消抖、状态检测
  *         
  * @return  NULL
  */
void InputPort_Scan(uint8_t mask, uint16_t interval)
{
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (InputPortInfo[i].pFunction == NULL || InputPortInfo[i].en != ENABLE)
        {
            continue; //< 该端口未注册或者未启动
        }

        int io = INPUT_PORT_READ_GPIO(InputPortInfo[i].port.pin, InputPortInfo[i].port.logic);
        if (io < 0)
        {
            continue;
        }

        if (InputPortInfo[i].ctrl.ctrl_pin != 0XFFFF)//< 当前端口有控制线
        {
            if ( InputPort_CtrlLineProcess((InputPort_CtrlLine_stu_t *)&InputPortInfo[i].ctrl, (InputPortInfo[i].gpioBuf << 1) | io) )
            {
                continue;//< 当前控制线状态不对或者处于非活跃状态：就不读取端口状态了
            }
        }
		
        InputPortInfo[i].gpioBuf <<= 1;
        InputPortInfo[i].gpioBuf |= io;

        if ((InputPortInfo[i].gpioBuf & mask) == 0X00)
        {
            /* 硬件IO稳定按下 */
            if (InputPortInfo[i].status == INPUT_PORT_STA_RELEASE || InputPortInfo[i].status == INPUT_PORT_STA_UNKNOW)
            {
                /* 多功能按钮，连按条件： 2*10ms < 松手时间 < 2*100ms*/
                if (InputPortInfo[i].timing > InputPortInfo[i].releaseMinTime && InputPortInfo[i].timing < InputPortInfo[i].releaseMaxTime && InputPortInfo[i].port.func == INPUT_PORT_FUNC_MULTI)
                {
                    InputPortInfo[i].times++; //< 连按计数
                }
                else
                {
                    InputPortInfo[i].times = 0;
                }
                InputPortInfo[i].timing = 0;
                InputPortInfo[i].status = INPUT_PORT_STA_PRESS; //< 标记为按下
                InputPortInfo[i].pFunction(InputPortInfo[i].port.name, InputPortInfo[i].status, InputPortInfo[i].times);   
            }

            /* 按下状态计时 */
            if (InputPortInfo[i].timing < InputPortInfo[i].longPressTime)
            {
                InputPortInfo[i].timing += interval;
                if (InputPortInfo[i].timing >= InputPortInfo[i].longPressTime) //< 长按时间到了
                {
                    InputPortInfo[i].status = INPUT_PORT_STA_LONG_PRESS; //< 标记为长按
                    InputPortInfo[i].pFunction(InputPortInfo[i].port.name, InputPortInfo[i].status, InputPortInfo[i].times);
                    InputPortInfo[i].times = 0XFF;                    
                }
            }
        }
        else if ((InputPortInfo[i].gpioBuf & mask) == mask)
        {
            /* 硬件IO稳定松开 */
            if (InputPortInfo[i].status != INPUT_PORT_STA_RELEASE)
            {
                InputPortInfo[i].timing = 0; //< 计时器清零

                InputPortInfo[i].status = INPUT_PORT_STA_RELEASE;
                if (InputPortInfo[i].port.func != INPUT_PORT_FUNC_MULTI)
                {
                    InputPortInfo[i].pFunction(InputPortInfo[i].port.name, InputPortInfo[i].status, InputPortInfo[i].times);
                }
            }

            if (InputPortInfo[i].timing < InputPortInfo[i].releaseMaxTime)
            {
                InputPortInfo[i].timing += interval;
                if (InputPortInfo[i].timing >= InputPortInfo[i].releaseMaxTime && InputPortInfo[i].port.func == INPUT_PORT_FUNC_MULTI)
                {
                    InputPortInfo[i].pFunction(InputPortInfo[i].port.name, InputPortInfo[i].status, InputPortInfo[i].times);
                }
            }
        }
        else
        {
            /* 硬件IO状态未知 */
        }
    }
}

//! //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
  * @brief   配置IO口模式为唤醒状态
  * @details 详细说明:无
  *         
  * @return  NULL
  */
static inline void InputPort_SpecialGpioWakeup(void)
{
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (InputPortInfo[i].pFunction == NULL || InputPortInfo[i].port.func != INPUT_PORT_FUNC_SPECIAL)
        {
            continue;
        }
        Device_Enable(InputPortInfo[i].port.pin);
    }
}

/**
  * @brief   特殊IO配置为低功耗状态
  * @details 详细说明:无
  *         
  * @return  NULL
  */
static inline void InputPort_SpecialGpioSleep(void)
{
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (InputPortInfo[i].pFunction == NULL || InputPortInfo[i].port.func != INPUT_PORT_FUNC_SPECIAL)
        {
            continue;
        }
        Device_Disable(InputPortInfo[i].port.pin);
    }
}

/**
  * @brief   检查特殊IO，有状态变化就返回对应的PIN
  * @details 详细说明:休眠状态下，设备层会调用该函数
  *         
  * @return  虚拟引脚（设备）
  */
uint32_t InputPort_CheckSpecialGpio(void)
{
    /* 配置特殊IO为唤醒状态 */
    InputPort_SpecialGpioWakeup();

    /* 延时一会儿，等IO电平稳定 */
    Device_DelayUs(10);

    /* 读取io电平 */
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (InputPortInfo[i].pFunction == NULL || InputPortInfo[i].port.func != INPUT_PORT_FUNC_SPECIAL)
        {
            continue;
        }

        int32_t last_sta = InputPortInfo[i].status;
        int32_t ioStatus = INPUT_PORT_READ_GPIO(InputPortInfo[i].port.pin, InputPortInfo[i].port.logic);
        if (ioStatus < 0)
        {
            continue;
        }

        last_sta = (last_sta == INPUT_PORT_STA_LONG_PRESS) ?  INPUT_PORT_STA_PRESS : last_sta;
        ioStatus = (ioStatus != 0) ? INPUT_PORT_STA_RELEASE : INPUT_PORT_STA_PRESS;
        if (ioStatus != last_sta)
        {
            return InputPortInfo[i].port.pin;
        }
    }

    /* 配置特殊IO口为低功耗状态  */
    InputPort_SpecialGpioSleep();
    return vNOHARDWARE;
}

//! //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
  * @brief   注册并初始化一个端口，配置一个硬件定时器周期扫描端口状态
  * @details 详细说明:无
  *         
  * @param[in]  port：需要注册的端口
  * @param[in]  portQty：注册的端口数量
  * @param[in]  pCallback：回调函数指针
  *         
  * @return 【SUCCESS】：成功   【ERROR】：失败
  */
ErrorStatus InputPort_Registered(InputPort_stu_t *port, uint8_t portQty, InputPortCB_fun_t pCallback)
{
    int i,j;

    if (port == NULL || pCallback == NULL)
    {
        return ERROR;
    }
    
    for (j = 0; j < portQty; j++)
    {
        if ((port[j].pin >> 8) != vPIN)
        {
            return ERROR;
        }
    }

    for (i = 0, j = 0; i < INPUT_PORT_MAX_QTY && j < portQty; i++)
    {
        if (InputPortInfo[i].pFunction == NULL)
        {
            for (; j < portQty; j++) 
            {
                if (Device_Read(port[j].pin, NULL, 0, 0) >= 0) //< 当前PIN是有效的（底层挂载了驱动）
                {
                    InputPortInfo[i].port = port[j++];
                    InputPortInfo[i].pFunction = pCallback;
                    InputPortInfo[i].status = INPUT_PORT_STA_UNKNOW;
                    InputPortInfo[i].times = 0XFF;
                    InputPortInfo[i].gpioBuf = 0XAA;
                    InputPortInfo[i].en = DISABLE;

                    InputPortInfo[i].releaseMinTime = INPUT_PORT_RELEASE_MIN_TIME;//< 最小松手时间
                    InputPortInfo[i].releaseMaxTime = INPUT_PORT_RELEASE_MAX_TIME;//< 最大松手时间

                    InputPortInfo[i].longPressTime = INPUT_PORT_LONG_PRESS_TIME;
                    InputPortInfo[i].ctrl.ctrl_pin = vNOHARDWARE;
                    break;
                }
            }
        }
    }
    return ((j == portQty) ? SUCCESS : ERROR);
}

/**
  * @brief   使能一个端口
  * @details 详细说明:无
  *         
  * @param[in]  name：端口名称
  *         
  * @return NULL
  */
void InputPort_EnableProt(char *name)
{
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (s_strcmp(name, InputPortInfo[i].port.name) == 0)
        {
            Device_EnterCritical();
            InputPortInfo[i].en = ENABLE;
            InputPortInfo[i].gpioBuf = 0XAA;
            Device_ExitCritical();
        }
    }
}

/**
  * @brief   禁能一个端口
  * @details 详细说明:无
  *         
  * @param[in]  name：端口名称
  *         
  * @return NULL
  */
void InputPort_DisableProt(char *name)
{
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (s_strcmp(name, InputPortInfo[i].port.name) == 0)
        {
            Device_EnterCritical();
            InputPortInfo[i].en = DISABLE;
            Device_ExitCritical();
        }
    }
}

/**
  * @brief   修改一个端口的连按时间间隔
  * @details 详细说明:无
  *         
  * @param[in]  name：端口名称
  * @param[in]  minTime：最小松手时间
  * @param[in]  maxTime：最大松手时间
  *         
  * @return 【SUCCESS】：成功   【ERROR】：失败
  */
ErrorStatus InputPort_SetProtTimeInterval(char *name, uint16_t minTime, uint16_t maxTime)
{
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (s_strcmp(name, InputPortInfo[i].port.name) == 0)
        {
            Device_EnterCritical();
            InputPortInfo[i].releaseMinTime = minTime;
            InputPortInfo[i].releaseMaxTime = maxTime;
            Device_ExitCritical();
            return SUCCESS;
        }
    }
    return ERROR;
}

/**
  * @brief   修改一个端口的长按保持时间
  * @details 详细说明:无
  *         
  * @param[in]  name：端口名称
  * @param[in]  time：长按时间
  *         
  * @return 【SUCCESS】：成功   【ERROR】：失败
  */
ErrorStatus InputPort_SetProtLongPressTime(char *name, uint16_t time)
{
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (s_strcmp(name, InputPortInfo[i].port.name) == 0)
        {
            Device_EnterCritical();
            InputPortInfo[i].longPressTime = time;
            Device_ExitCritical();
            return SUCCESS;
        }
    }
    return ERROR;
}

/**
  * @brief   给一个输入端口添加控制线
  * @details 详细说明:无
  *         
  * @param[in]  name：端口名称
  * @param[in]  interval：非活跃状态下的扫描间隔（0xFFFF表示不扫描）
  * @param[in]  ctrl_pin：控制PIN
  * @param[in]  logic：控制线电平逻辑
  *         
  * @return 【SUCCESS】：成功   【ERROR】：失败
  */
ErrorStatus InputPort_AddCtrlLine(char *name, uint32_t interval, VirtualHardware_enum_t ctrl_pin, uint8_t logic)
{
    if ((ctrl_pin >> 8) != vPIN)
    {
        return ERROR;
    }

    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (s_strcmp(name, InputPortInfo[i].port.name) == 0)
        {
            Device_EnterCritical();
            InputPortInfo[i].ctrl.ctrl_pin = ctrl_pin;
            InputPortInfo[i].ctrl.logic = logic;
            InputPortInfo[i].ctrl.interval = interval;
            InputPortInfo[i].ctrl.itv_count = 0;
            InputPortInfo[i].ctrl.active = 0;
            Device_ExitCritical();
            return SUCCESS;
        }
    }
    return ERROR;
}

/**
  * @brief   设置一个输入端口的活跃时间（添加了控制线的端口才有意义）
  * @details 详细说明:活跃时间段：控制线一直使能，非活跃时间段：控制线每间隔interval使能一下
  *         
  * @param[in]  name：端口名称
  * @param[in]  active：活跃时间
  *         
  * @return 【SUCCESS】：成功   【ERROR】：失败
  */
ErrorStatus InputPort_SetCtrlLine(char *name, uint32_t active)
{
    for (int i = 0; i < INPUT_PORT_MAX_QTY; i++)
    {
        if (s_strcmp(name, InputPortInfo[i].port.name) == 0)
        {
            Device_EnterCritical();
            InputPortInfo[i].ctrl.itv_count = 0;
            if (InputPortInfo[i].ctrl.active < active || active == 0)
            {
                InputPortInfo[i].ctrl.active = active;
            }
            Device_ExitCritical();
            return SUCCESS;
        }
    }
    return ERROR;
}
