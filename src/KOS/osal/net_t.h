#ifndef __NET_T_H__
#define __NET_T_H__

/*************************************************
 * WIFI
 ************************************************/

/* wifi --> application msg_type */
typedef enum
{
    WIFI_MSG_JOIN_FAIL,    //wifi连接失败
    WIFI_MSG_DISCONNECTED, //wifi已断开连接
    WIFI_MSG_CONNECTED,    //wifi已连接
    WIFI_MSG_SCAN_REPLY,   //wifi扫描回复
    WIFI_MSG_PWD_ERR,      //wifi连接密码错误
    WIFI_MSG_NO_AP_FOUND,  //wifi未能找到目标AP
}WiFiMsgType_enum_t;

/* WIFI-CTRL */
typedef enum
{
    WIFI_CTRL_CONNECT      = 0, //连接wifi命令
    WIFI_CTRL_DISCONNECT   = 1, //断开wifi命令
    WIFI_CTRL_SCAN         = 2, //扫描wifi命令
    WIFI_CTRL_CONNECT_FAST = 3, //快速连接wifi命令
    WIFI_CTRL_RESET_RECONNECT = 4, //复位wifi重连间隔
    WIFI_CTRL_DISENABLE    =5,//禁用wifi 但是不清ssid 低电量调用
}WifiCtrl_enum_t;

typedef enum
{
    WIFI_CFG_MSG_AP_SUCCEED,     //AP开启成功
    WIFI_CFG_MSG_AP_FAIL,        //AP开启失败
    WIFI_CFG_MSG_AP_STA_ADD,     //STA 连接AP
    WIFI_CFG_MSG_AP_STA_DEL,     //STA 断开AP
    WIFI_CFG_MSG_SOCKET_SUCCEED, //socket连接成功
    WIFI_CFG_MSG_SOCKET_FAIL,    //socket开启失败
    WIFI_CFG_MSG_DELAY,          //延时
    WIFI_CFG_MSG_NET_STS,        //配网状态
    WIFI_CFG_MSG_TO_CONNECT,     //正在配网
    WIFI_CFG_MSG_CFG_NET_RESULT, //配网结果
    WIFI_CFG_MSG_EXIT,           //退出配网
} WiFiCfgMsgType_enum_t;
typedef enum
{
    WIFI_CFG_CTRL_AP_START,           //wifi开启AP模式
    WIFI_CFG_CTRL_AP_STOP,            //wifi关闭AP模式
    WIFI_CFG_CTRL_TRY_CONNECT_SERVER, //尝试连接服务器
    WIFI_CFG_CTRL_SYNC_INFO,          //同步ap配网信息
} WifiCfgCtrl_enum_t;

#pragma pack(1)
/* wifi连接参数 */
typedef struct
{
    char ssid[33];    //wifi ssid
    char pwd[65];     //wifi password
}WifiConnectPara_stu_t;

/* WIFI-AP信息 */
typedef struct
{
    int8_t  rssi;     //RSSI of the AP
    uint8_t bssid[6]; //BSSID of the AP
    int     channel;  //Channel of the AP
    char    ssid[33];    //wifi ssid
}WifiApInfo_stu_t;

/* AP扩展信息 */
typedef struct
{
    uint8_t mac[6];     //MAC地址
    uint32_t ip;        //IP地址
    uint32_t gateway;   //网关
    uint32_t netmask;   //掩码
    uint32_t dns0;      //DNS0
    uint32_t dns1;      //DNS1
}WifiApInfoExt_stu_t;

/* WIFI-快连数据结构 */
typedef struct
{
    WifiConnectPara_stu_t connect;
    WifiApInfo_stu_t ap;
}WifiFastConnectPara_stu_t;

/* Scan ap-info */
typedef struct
{
    uint8_t rssi;  //rssi
    char ssid[33]; //ssid
}ApInfo_stu_t;

/* Scan ap-list */
typedef struct
{
    uint8_t qty;
    ApInfo_stu_t ap[0];
}ApList_stu_t;

typedef struct
{
    uint8_t lockRand[32]; //随机数密文
    uint8_t eSN[13 + 1];  //eSN
    uint8_t functionCode; //功能集
    uint8_t cfg_or_ate;   //配网或产测
} Ap_Cfg_Lock_Info;

/*************************************************
 * NETWORK
 ************************************************/

typedef struct
{
    uint8_t cer_esn[14];  //ESN，字符串要加结束符
    uint8_t cer_pwd[12];  //PWD
} Cer_Set_Esn_Pwd;

/* NETWORK-CTRL */
typedef enum
{
    NETWORK_CTRL_MQTT_CONNECT    = 0, //连接mqtt服务器
    NETWORK_CTRL_MQTT_DISCONNECT = 1, //断开mqtt服务器
    NETWORK_CTRL_MQTT_SEND       = 2, //发送mqtt数据
    NETWORK_CTRL_HTTP_DOWNLOAD   = 3, //http下载文件（下载固件）
    NETWORK_CTRL_GET_DFU_DATA    = 4, //获取固件升级数据（获取已经下载好的固件数据）
    NETWORK_CTRL_SET_ESN_AND_PWD = 5, //设置ESN和PASSWPRORD,用作证书更新
}NetworkCtrl_enum_t;

/* 网络底层通知上层应用的消息枚举  driver -> network -> application */
typedef enum
{
    NETWORK_MSG_MQTT_OFFLINE = 0x10,  //mqtt状态改变：离线
    NETWORK_MSG_MQTT_ONLINE,          //mqtt状态改变：上线
    NETWORK_MSG_MQTT_RECV,            //mqtt收到数据

    NETWORK_MSG_FW_DL_SUCCESS = 0x20, //固件下载成功
    NETWORK_MSG_FW_DL_ERROR,          //固件下载失败：HTTP下载出错
    NETWORK_MSG_FW_DL_ERROR_LEN,      //固件下载失败：长度错误，文件太大存不下
    NETWORK_MSG_FW_DL_ERROR_STORE,    //固件下载失败：存储错误，下载过程中文件存储失败
    NETWORK_MSG_FW_DL_ERROR_MD5,      //固件MD5校验失败
    NETWORK_MSG_FW_DATA,              //固件数据
}NetworkMsgType_enum_t;


#pragma pack(1)
/* mqtt登录信息 */
typedef struct
{
#ifndef DOMAIN_NAME_MAX
    uint8_t  domain[30];        //mqtt域名
#else
    uint8_t  domain[DOMAIN_NAME_MAX];        //mqtt域名
#endif
    uint16_t port;              //mqtt端口号
    uint16_t keepAliveInterval; //mqtt保活时间（s）
    uint8_t  qos;               //qos级别 
    uint8_t  clientId[17];      //mqtt登录客户端id
    uint8_t  userName[14];      //mqtt登录用户名
    uint8_t  password[41];      //mqtt登录密码
    uint8_t  subTopicQty;       //订阅的主题数量
    uint8_t  subTopic[0][50];   //主题
}MqttConnectInfo_stu_t;

/**
 *  固件参数数据结构，这个结构体有两个场景会用到
 *  
 *  1、应用层调用Network_HttpDownload下载固件（带URL）时，会带上这个结构体的信息。
 * 
 *  2、应用层调用Network_GetDfuData读取固件的第0包数据时，NETWORK组件要publish这个结构体的内容给应用层（不带URL）
 *     所以NETWORK底层存储下载好的固件的同时，还要存储固件附属信息（如下结构体）（URL不用存）
 **/
typedef struct
{
    uint8_t  devNum;    //设备编号
    uint32_t fileLen;   //固件长度
    uint8_t  md5[32+1]; //固件MD5
    uint8_t  url[0];    //下载链接
}FmDlParam_stu_t;

/* mqtt数据结构 */
typedef struct
{
    uint8_t  qos;           //mqtt消息等级
    uint8_t  topic[50];     //主题
    uint16_t payload_len;   //数据域长度
    uint8_t  payload[0];    //数据域
}MqttData_t;
#pragma pack()




/*************************************************
 * BLE
 ************************************************/

/* BLE消息类型（底层往上报的消息类型） */
typedef enum
{
    PUBLISH_APP_CMD,    // 收到BLE-MASTER下发的命令
    PUBLISH_BLE_MAC,    // BLE启动成功，带MAC地址
    PUBLISH_BLE_STATUS, // 蓝牙连接状态（连接/断开）
    PUBLISH_BLE_ADV_CLOSE, // 上报广播关闭信息给应用层
    PUBLISH_BLE_BIND_STATE, // 上报蓝牙绑定状态给应用层，应用层去断讯美的电
    PUBLISH_HEART_BEAT,     //心跳
    PUBLISH_CALIBRATION_FREQ, //上报设备GATT和频偏
}PublishType_enum_t;

/* BLE控制类型（应用层下发的数据类型） */
typedef enum
{
    BLE_CTRL_SET_ADV,  //设置BLE广播
    BLE_CTRL_SET_PARAM,//设置BLE参数
    BLE_CTRL_DATA,     //发数据给BLE-MASTER
    BLE_CTRL_WIFI_DATA,//组件发配网数据给BLE-MASTER
    BLE_CTRL_CONNECT,  //控制连接
    BLE_CTRL_SEND_PWDB,//下发PWDb
    BLE_CTRL_DOOR_LOCK,//下发DOORLOCK服务参数
    BLE_CTRL_RECONNECT,//重新连接（重新扫描连接）
    BLE_CTRL_DISCONNECT, //断开连接
    BLE_CTRL_CALIBRATION, //开启校准
    BLE_CTRL_RADIOFREQEN, //射频开关
    BLE_CTRL_MODULE_PARAM,//下发BLE模块参数
    BLE_CTRL_SET_MAC,      //设置广播地址
    BlE_CTRL_ENCRYPTION,   //加密 
    BLE_CTRL_SET_BINDFLAG, //设置绑定标志
}BleCtrl_enum_t;

typedef enum
{
    BT_CB_UNKNOWN = 0,            //未知
    BT_CB_DISCONNECT = 1,         //断开连接
    BT_CB_ALREADY_CONNECTED = 2,  //已连接
    BT_CB_STACK_READY = 3,     //蓝牙协议栈OK   
    BT_CB_APP_NOTIFY_EN = 4,  
}BT_CB_TYPE_ENUM_T;

#pragma pack(1)
/* BLE-PARAM 结构体 */
typedef struct
{
    uint8_t esn[13+1];   //ESN
    uint8_t pwd1[12];    //PWD
    uint8_t model[5];    //设置锁型号
    uint8_t version[20]; //设置软件版本号
    uint8_t func;        //功能集参数
    uint32_t bleVersion; //蓝牙协议版本
}BleParam_stu_t;

/* BLE广播数据结构 */
typedef struct
{
    uint8_t adv[31];  //广播包
    uint8_t adv_len;  //广播包长度
    uint8_t scan[31]; //扫描响应包
    uint8_t scan_len; //扫描响应包长度
}BleAdv_stu_t;
#pragma pack()


/* 摄像头控制参数 */
typedef enum
{
    CAMERA_CTRL_SEND_DATA,     //发数据给camera host
    CAMERA_CTRL_SENSOR_PWR_ON, //控制camera图像传感器上电（同时host也会上电）
    CAMERA_CTRL_HOST_PWR_ON,   //控制camera主控上电（图像传感器不上电）
    CAMERA_CTRL_PWR_OFF,       //控制camera主控和传感器同时断电
}CameraCtrl_enum_t;


/*************************************************
 * LCD
 ************************************************/

/* lcd屏控制参数 */
typedef enum
{
    LCD_SET_DEVICE = 0, // 设置屏驱动
    LCD_DISPLAY_PIC,    // 显示图片
}LcdCtrl_enum_t;

#endif /* __NET_T_H__ */
