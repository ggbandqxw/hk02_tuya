/**
  * @file 		osport.h
  * @brief		多核通信管理模块、支持全双工多端口收发等逻辑函数的头（说明）文件
  *
  * @copyright  Copyright (c) 2017~2020 ShenZhen Dxtc Technology Co., Ltd. 
  * All rights reserved.
  * 
  * @version    v3.1
  * @author		FengLi
  * @date 		2021-10-28
  * 
  * @note		鼎新同创·智能锁
  * 
  * @par 修改日志:
  * <1> 2021/08/02 v3.1 FengLi 创建初始版本
  *******************************************************************/
#ifndef __OSPORT_H__
#define __OSPORT_H__

#include "osal.h"
#include "device.h"

#define OSPORT_MAX_QTY               5   ///< 最多支持5个虚拟端口 -- 最多支持5个KOS从机
#ifdef KOS_PARAM_OSPORT_vTASK_MAX_QTY
#define OSPORT_vTASK_MAX_QTY         KOS_PARAM_OSPORT_vTASK_MAX_QTY
#else
#define OSPORT_vTASK_MAX_QTY         8   ///< 每个端口最多支持6个vTASK -- 每个从机最多支持8个TASK（组件）
#endif
#define OSPORT_vTASK_NV_SIZE         20  ///< 每个vTASK的NV为20字节 -- 从机上面每个组件的NV前20字节会同步到主机端


/** @brief  多核通信最大数据长度  */
#ifdef KOS_PARAM_OSPORT_PACKET_MAX_LEN
#define OSPORT_PACKET_MAX_LEN        KOS_PARAM_OSPORT_PACKET_MAX_LEN
#else
#define OSPORT_PACKET_MAX_LEN        128
#endif

#define OSPORT_CMD_FLAG_ACK          0x80   ///< 命令字最高位为1表示ACK包

/** 
 * @brief  命令字定义
 */
#define OSPORT_CMD_HELLO             0x01   ///< HELLO命令：启动时会发HELLO
#define OSPORT_CMD_SYNC              0x02   ///< SYNC命令：由子设备发出，包含子设备的TASK信息、NV信息、Rm/Rs
#define OSPORT_CMD_MBOX              0x03   ///< MBOX命令
#define OSPORT_CMD_PUBLISH           0x04   ///< PUBLISH命令
#define OSPORT_CMD_UPDATE_SLEEP      0x05   ///< 主设备给子设备刷新休眠时间
#define OSPORT_CMD_MULTI_PACK_0      0x06   ///< 大数据包分多包传输命令（首包、不含具体数据、只传输包信息）
#define OSPORT_CMD_MULTI_PACK_N      0x07   ///< 大数据包分多包传输命令（数据包、传输具体数据内容）

void OSAL_OsPortInit(void);
void OSAL_OsPortProcess(void);
ErrorStatus OSAL_OsPortSend(VirtualHardware_enum_t port, uint8_t cmd, uint8_t param1, uint8_t param2, void *msg, uint16_t len);
ErrorStatus OSAL_OsPortCheckWakeDevice(VirtualHardware_enum_t wake_dev);
ErrorStatus OSAL_OsportConfig(FunctionalState sta);

#ifdef KOS_PARAM_OSPORT_CRYPTO
uint8_t OSAL_OsportSearchAccountInfo(VirtualHardware_enum_t port, uint8_t *esn);
ErrorStatus OSAL_OsportGetAccountListInfo(uint8_t index,uint8_t *esn,uint8_t *dev_info);
void OSAL_OsportSaveRmRs(VirtualHardware_enum_t port, uint32_t rm, uint32_t rs);
uint32_t OSAL_OsportCreateRm(VirtualHardware_enum_t port, uint32_t number);
uint32_t OSAL_OsportCreateRs(VirtualHardware_enum_t port, uint32_t rm);
ErrorStatus OSAL_OsportInquireAccountListDevnum(uint8_t devnum);
#endif


#endif
