#ifndef __BATTERY_H__
#define __BATTERY_H__
#include "osal.h"

#define BATTERY_LOW_POWER_I    10 //10% 
#define BATTERY_LOW_POWER_II   20 //20% 

/* Battery组件消息类型 */
typedef uint8_t BatteryMsg_t;


/* 读电量接口 */
static uint8_t Battery_Get(void);
uint8_t Battery_Ate_Get(void);

#endif /* __BATTERY_H__ */

