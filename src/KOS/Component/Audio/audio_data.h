#ifndef __AUDIO_DATA_H__
#define __AUDIO_DATA_H__
#include "osal.h"

//定义AUDIO 缓存大小
#define AUDIO_BUFF_SIZE   (8*1024) 

#pragma pack(1)

/* 当前正在播放的语音信息 */
typedef struct
{
	uint32_t addr;   		//地址
	uint32_t length; 		//长度
}SoundInfo_stu_t;		

#pragma pack()

/**
  * @brief  获取语音信息
  * @note   
  */
extern ErrorStatus Audio_GetInfo(uint16_t audioIndex, SoundInfo_stu_t *soundInfo,uint8_t *flag);

/**
  * @brief  读取音乐数据，并解码成16位wav
  * @note   启动播放时，一次性填充2个buf
  *         
  * @param  pBuf:解析后的数据
  * @param  flashAddr: ADPCM数据地址
  * @param  readSize: 读取ADPCM数据的个数
  */
extern void Audio_ReadAdpcmAndDecode(uint8_t *pBuf, uint32_t flashAddr, uint16_t readSize);

#endif /* __AUDIO_DATA_H__ */
