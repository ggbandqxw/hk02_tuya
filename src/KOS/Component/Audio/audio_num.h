#ifndef __AUDIO_NUM_H__
#define __AUDIO_NUM_H__

#define YI_QI_DONG_BU_FANG 0
#define KA_PIAN_YI_CUN_ZAI 0  // 可清除func_card
//#define KA_PIAN_KU_WEI_KONG 0
#define QING_SHU_RU_MI_MA_HUO_ZHI_WEN 0

#define QING_SHUA_FANG_WEI_MA 0                                                                       //请刷防伪码
#define MEN_SUO_WEI_JI_HUO_QING_SHU_RU_JI_HUO_MA 0                                                    //门锁未激活，请输入激活码
#define JI_HUO_CHENG_GONG 0                                                                           //激活成功

typedef enum
{
    WU_XIAO_YU_YIN = 0,                                    // 无效语音不播放声音
    MENG_LING_YIN,                                         // 000.门铃音叮咚
    HUAN_YING_YIN,                                         // 001.欢迎音咚咚
    ZHI_WEN_YIN_DO,                                        // 002.指纹音Do
    AN_JIAN_YIN_DI,                                        // 003.按键音Di
    BAO_JING_YIN_DA_DA_DA,                                 // 004.报警音
    YAN_ZHENG_CHENG_GONG,                                  // 005.成功提示音
    YAN_ZHENG_SHI_BAI,                                     // 006.失败提示音
    YU_YIN_A,                                              // A
    YU_YIN_B,                                              // B
    YU_YIN_C,                                              // C
    YU_YIN_D,                                              // D
    YU_YIN_E,                                              // E
    YU_YIN_F,                                              // F
    YU_YIN_G,                                              // G
    YU_YIN_H,                                              // H
    YU_YIN_I,                                              // I
    YU_YIN_J,                                              // J
    YU_YIN_K,                                              // K
    YU_YIN_L,                                              // L
    YU_YIN_M,                                              // M
    YU_YIN_N,                                              // N
    YU_YIN_O,                                              // O
    YU_YIN_P,                                              // P
    YU_YIN_Q,                                              // Q
    YU_YIN_R,                                              // R
    YU_YIN_S,                                              // S
    YU_YIN_T,                                              // T
    YU_YIN_U,                                              // U
    YU_YIN_V,                                              // V
    YU_YIN_W,                                              // W
    YU_YIN_X,                                              // X
    YU_YIN_Y,                                              // Y
    YU_YIN_Z,                                              // Z
    YU_YIN_0,                                              // 0
    YU_YIN_1,                                              // 1
    YU_YIN_2,                                              // 2
    YU_YIN_3,                                              // 3
    YU_YIN_4,                                              // 4
    YU_YIN_5,                                              // 5
    YU_YIN_6,                                              // 6
    YU_YIN_7,                                              // 7
    YU_YIN_8,                                              // 8
    YU_YIN_9,                                              // 9
    QING_YAN_ZHENG_GUAN_LI_YUAN_MI_MA,                     // 请验证管理员密码
    TIAN_JIA_GUAN_LI_YUAN_YONG_HU_QING_AN,                 // 添加管理员用户请按
    SHU_RU_CUO_WU,                                         // 输入错误
    QING_SHU_RU_BIAN_HAO,                                  // 请输入编号
    YONG_HU_BU_CUN_ZAI,                                    // 用户不存在
    SHAN_CHU_CHENG_GONG,                                   // 删除成功
    TIAN_JIA_YONG_HU_QING_AN,                              // 添加用户请按
    SHAN_CHU_YONG_HU_QING_AN,                              // 删除用户请按
    QING_AN_SHOU_ZHI,                                      // 请按手指
    QING_SHU_RU_XIN_MI_MA,                                 // 请输入新密码
    QING_ZAI_CI_SHU_RU_XIN_MI_MA,                          // 请再次输入新密码
    SHE_ZHI_CHENG_GONG,                                    // 设置成功
    NIAN,                                                  // 年
    QING_SHU_RU_YUAN_MI_MA,                                // 请输入原密码
    YUE,                                                   // 月
    RI,                                                    // 日
    SHI,                                                   // 时
    FEN,                                                   // 分
    CHU_SHI_HUA_CHENG_GONG,                                // 初始化成功
    QING_AN_ZHI_WEN_HUO_SHU_RU_MI_MA,                      // 请按指纹或输入密码
    TIAN_JIA_CHENG_GONG,                                   // 添加成功
    JIA_QI_MO_SHI_YI_KAI_QI,                               // 假期模式已开启
    MI_MA_YI_CUN_ZAI,                                      // 密码已存在
    MI_MA_KU_YI_MAN,                                       // 密码库已满
    DIAN_CHI_DIAN_LIANG_BU_ZU_QING_GENG_HUAN_DIAN_CHI,     // 电池电量不足请更换电池
    QING_NA_KAI_SHOU_ZHI_ZAI_AN_YI_CI,                     // 请拿开手指再按一次
    TUI_CHU_SHE_ZHI,                                       // 退出设置
    HUI_FU_CHU_CHANG_SHE_ZHI,                              // 恢复出厂设置
    QUE_REN_QING_AN_KAI_SUO_JIAN,                          // 确认请按开锁键
    FAN_HUI_QING_AN_SHANG_SUO_JIAN,                        // 返回请按上锁键
    BAN_BEN_HAO,                                           // 版本号
    CAO_ZUO_CHENG_GONG,                                    // 操作成功
    CHANG_KAI_MO_SHI_QING_ZHU_YI_AN_QUAN,                  // 常开模式请注意安全
    CHANG_KAI_YI_QU_XIAO,                                  // 常开已取消
    SHU_RU_CUO_WU_QING_CHONG_XIN_SHU_RU,                   // 输入错误请重新输入
    KAI_QI_QING_AN,                                        // 开启请按
    GUAN_BI_QING_AN,                                       // 关闭请按
    SHE_ZHI_SHI_MIAO_ZI_DONG_SHANG_SUO_QING_AN,            // 设置10秒自动上锁请按
    SHE_ZHI_ER_SHI_MIAO_ZI_DONG_SHANG_SUO_QING_AN,         // 设置20秒自动上锁请按
    SHE_ZHI_SAN_SHI_MIAO_ZI_DONG_SHANG_SUO_QING_AN,        // 设置30秒自动上锁请按
    QING_DENG_DAI,                                         // 请等待
    ZHI_WEN_KU_YI_MAN,                                     // 指纹库已满
    YI_GUAN_SUO,                                           // 已关锁
    YI_KAI_SUO,                                            // 已开锁
    SUO_FANG_XIANG_SHE_ZHI_CHENG_GONG,                     // 锁方向设置成功
    ZI_DONG_SHANG_SUO_SHE_ZHI_QING_AN,                     // 自动上锁设置请按
    ZHENG_ZAI_PEI_WANG,                                    // 正在配网
    QING_SHU_RU_ZHI_WEN_HUO_MI_MA_HUO_KA_PIAN,             // 请输入指纹或密码或卡片
    QING_SHUA_KA,                                          // 请刷卡
    KA_PIAN_KU_YI_MAN,                                     // 卡片库已满
    QING_SHU_RU_MI_MA_HUO_KA_PIAN,                         // 请输入密码或卡片
    YI_HUI_FU_DAO_CHU_CHANG_SHE_ZHI,                       // 已恢复到出厂设置
    KIA_PIAN_YI_CUN_ZAI,                                   // 卡片已存在
    JIA_QI_MO_SHI_YI_GUAN_BI,                              // 假期模式已关闭
    TIAN_JIA_SHI_BAI,                                      // 添加失败
    FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN,       // 返回上级菜单请按上锁键
    AN_BIAN_HAO_SHAN_CHU_QING_AN,                          // 按编号删除请按
    AN_SHU_RU_NEI_RONG_SHAN_CHU_QING_AN,                   // 按输入内容删除请按
    QING_KONG_SUO_YOU_YONG_HU_QING_AN,                     // 清空所有用户请按
    ZI_DONG_SHANG_SUO_GONG_NENG_QING_AN,                   // 自动上锁功能请按
    KAI_QI_ZI_DONG_SHANG_SUO_GONG_NENG_QING_AN,            // 开启自动上锁功能请按
    GUAN_BI_ZI_DONG_SHANG_SUO_GONG_NENG_QING_AN,           // 关闭自动上锁功能请按
    YIN_LIANG_DIAO_JIE_QING_AN,                            // 音量调节请按
    DI_YIN_QING_AN,                                        // 低音请按
    ZHONG_YIN_QING_AN,                                     // 中音请按
    GAO_YIN_QING_AN,                                       // 高音请按
    JIA_QI_MO_SHI_QING_AN,                                 // 假期模式请按
    HUI_FU_CHU_CHANG_SHE_ZHI_QING_AN_JIE_SUO_JIAN_QUE_REN, // 恢复出厂设置请按解锁键确认
    TIAN_JIA_PU_TONG_MI_MA_QING_AN,                        // 添加普通密码请按
    TIAN_JIA_GUAN_LI_MI_MA_QING_AN,                        // 添加管理密码请按
    JING_YIN_QING_AN,                                      // 静音请按
    SHAN_CHU_MI_MA_QING_AN,                                // 删除密码请按
    TIAN_JIA_MI_MA_QING_AN,                                // 添加密码请按
    QING_KONG_SUO_YOU_MI_MA_QING_AN,                       // 清空所有密码请按

    YI_FAN_SUO,                                            // 已反锁
    MEN_WEI_SHANG_SUO,                                     // 门未上锁
    BIAN_HAO_BU_CUN_ZAI,                                   // 编号不存在
    QING_SHU_RU_MI_MA_YI_KAI_SUO_JIAN_JIE_SHU,             // 请输入密码以开锁键结束
    YI_KAI_MEN,                                            // 已开门
    SHAN_CHU_SHI_BAI_WEI_AN_QUAN_MO_SHI,                   // 删除失败为安全模式
    SHE_ZHI_SHI_BAI,                                       // 设置失败
    SHAN_CHU_SHI_BAI,                                      // 删除失败
    MI_MA_KU_WEI_KONG,                                     // 密码库为空
    ZHI_WEN_KU_WEI_KONG,                                   // 指纹库为空
    QING_SHU_RU_LIANG_WEI_BIAN_HAO,                        // 请输入两位编号
    MI_MA_GUO_YU_JIAN_DAN,                                 // 密码过于简单
    QING_XIU_GAI_GUAN_LI_MI_MA,                            // 请修改管理密码
    YI_JIN_RU_GUAN_LI_MO_SHI,                              // 已进入管理模式
    XI_TONG_YI_SUO_DING_QING_SHAO_HOU_ZAI_SHI,             // 系统已锁定请稍后再试
    YI_TUI_CHU_GUAN_LI_MO_SHI,                             // 已退出管理模式
    YI_KAI_SUO_JIAN_JIE_SHU,                               // 以开锁键结束
    QING_AN_ZHI_WEN,                                       // 请按指纹
    QING_CHONG_XIN_SHU_RU,                                 // 请重新输入
    LIANG_CI_SHU_RU_DE_MI_MA_BU_YI_ZHI,                    // 两次输入的密码不一致
    JING_YIN_MO_SHI,                                       // 静音模式
    YU_YIN_MO_SHI,                                         // 语音模式
    QING_KAI_MEN,                                          // 请开门
    ZI_DONG_SHANG_SUO_YI_KAI_QI,                           // 自动上锁已开启
    ZI_DONG_SHANG_SUO_YI_GUAN_BI,                          // 自动上锁已关闭
    BIAN_HAO_YI_CUN_ZAI,                                   // 编号已存在

    TIAN_JIA_PU_TONG_YONG_HU_QING_AN,                      // 添加普通用户请按
    TIAN_JIA_ZHI_WEN_QING_AN,                              // 添加指纹请按
    TIAN_JIA_PU_TONG_ZHI_WEN_QING_AN,                      // 添加普通指纹请按
    TIAN_JIA_GUAN_LI_ZHI_WEN_QING_AN,                      // 添加管理指纹请按
    SHAN_CHU_ZHI_WEN_QING_AN,                              // 删除指纹请按
    SHAN_CHU_QUAN_BU_ZHI_WEN_QING_AN,                      // 删除全部指纹请按
    XIU_GAI_GUAN_LI_YUAN_YONG_HU_QING_AN,                  // 修改管理员用户请按
    QING_SHU_RU_LIANG_WEI_ZI_DONG_SHANG_SUO_SHI_JIAN_YI_KAI_SUO_JIAN_JIE_SHU, //请输入两位自动上锁时间,以开锁键结束
    TIAN_JIA_CHENG_GONG_BIAN_HAO,                          // 添加成功,编号
    TIAN_JIA_KA_PIAN_QING_AN,                              // 添加卡片请按
    KA_PIAN_BU_CUN_ZAI,                                    // 卡片不存在
    KA_PIAN_KU_WEI_KONG,                                   // 卡片库为空
    QING_CHONG_XIN_SHUA_KA,                                // 请重新刷卡
    QING_SHU_RU_SAN_WEI_BIAN_HAO,                          // 请输入三位编号
    YAN_ZHENG_SHI_BAI_2,                                   // 验证失败 -- 真实语音
    QING_SHU_RU_ZI_DONG_SHANG_SUO_SHI_JIAN_YI_KAI_SUO_JIAN_JIE_SHU, //请输入自动上锁时间,以开锁键结束
    XIU_GAI_GUAN_LI_YUAN_MI_MA_QING_AN,                         // 修改管理员密码请按 
    MI_MA_CUO_WU,                                          // 密码错误 --
    YAN_ZHENG_CHENG_GONG_2,                                // 验证成功 --真实语音
    QING_XIU_GAI_GUAN_LI_MI_MA_FOR_RESET = 254,            // 这条非实际语音，只用于判断是否为恢复出厂设置所播放的语音
    AUDIO_MAX,
} D13_AUDIO_ADDR_E;

/* 定义语言偏移量 */
#define AUDIO_LANGUAGE_ADDR_OFFSET 200  // 必须大于最后一条有效语音
#define STOP_PLAY 0xFFFF 

#endif