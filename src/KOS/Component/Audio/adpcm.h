#ifndef __ADPCM_H__
#define __ADPCM_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include "stdint.h"



void ADPCM_Init(void);
int16_t ADPCM_Decode(uint8_t code);



#ifdef __cplusplus
}
#endif

#endif /* __ADPCM_H__ */
