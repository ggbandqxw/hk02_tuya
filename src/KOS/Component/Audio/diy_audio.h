#ifndef __DIY_AUDIO_H__
#define __DIY_AUDIO_H__

#include "stdint.h"
#include "audio_data.h"

// 旗舰机支持4句个性化语音
// 203 请开门
// 4   报警音哒哒哒
// 102 系统已锁定，请稍后再试
// 125 已开门

#ifndef DIY_MAX_NUM
#define DIY_MAX_NUM (5) // DIY总个数
#endif

/* 个性化语音 */
#define DIY_DATA_MAX 1024      // 每次应用层传送数据最大值

// 外部flash
#define DIY_MAX_LEN (160*1024) // 单句个性化语音内容最大长度
#define FLASH_SIZE_DIY_AUDIO 1024 * 1024 // 个性化语音大小1M
#define OUT_FLASH_SIZE (16*1024*1024) // 外部flash总大小

#define OUT_FLASH_DIY_AUDIO_ADDR (OUT_FLASH_SIZE - FLASH_SIZE_DIY_AUDIO-4*1024) // 个性化语音地址

#pragma pack(1)
/* 存储NV结构体 */
typedef struct
{
    struct
    {
        uint16_t SN;
        uint32_t len;
        uint8_t addr_info; // 0-5
        char md5[33];
    } DIYAudioInfo[DIY_MAX_NUM]; // 最多存5条个性化语音
    uint8_t add_num; // 0-5
    uint8_t nvInitFlag;
} DIYAudioInfo_stu_t;

/* 传输个性化语音结构体 */
typedef struct
{
    uint32_t addr_offect; // 地址偏移
    uint16_t pack_len; // 单包大小
    uint8_t data[DIY_DATA_MAX];
} sendAudioMsg_stu_t;

/* 首包结构体 */
typedef struct
{
    uint16_t SN;
    uint32_t total_len; // 总大小
    char md5[33];
} titleAudioMsg_stu_t;

/* 上报结构体 */
typedef struct
{
    uint16_t SN;
    char md5[33];
} customAudioMsg_stu_t;

#pragma pack()

void Audio_DIYInit(void);
void Audio_WriteDIY(sendAudioMsg_stu_t *msg);
void Audio_TitleDIY(titleAudioMsg_stu_t *msg);
void Audio_DeletDIY(uint8_t *msg);
uint8_t Audio_FindSN(uint16_t sn);
ErrorStatus Audio_GetDIYInfo(uint8_t addr_info,SoundInfo_stu_t *soundInfo);

#endif /* __DIY_AUDIO_H__ */
