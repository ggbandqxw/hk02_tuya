#include "mn25713.h"
// #define MN_DEBUG	1
#define MN25713_LOG(format, ...) __OSAL_LOG(C_BLUE format C_NONE, ##__VA_ARGS__)
#if defined MN_DEBUG
#define __MN25713_LOG(format, ...) __OSAL_LOG(C_BLUE format C_NONE, ##__VA_ARGS__)
#else
#define __MN25713_LOG(format, ...)
#endif

#define VHW_IIC_MN vIIC_0



static uint8_t sensor_type=0xff;//初始化成功与否的标志
/**
  * @brief  阻塞延时
  * @note   
  * @param  ms：时间ms
  */
static void MN_Delay(uint16_t ms)
{
	Device_DelayMs(ms);
}

/**
  * @brief  读寄存器
  * @note   
  * @param  reg_addr：寄存器地址
  * @return 读到的寄存器值
  */
static uint8_t MN_ReadReg(uint8_t reg_addr)
{
	uint8_t data[4] = {0};
	if (Device_Write(VHW_IIC_MN, &reg_addr, 1, IIC_ADDR_MN) == 0)
	{
		#ifdef R6A40_A
		MN_Delay(1);
		#elif R701B_A
		MN_Delay(1);
		#endif
		Device_Read(VHW_IIC_MN, data, 4, IIC_ADDR_MN);
	}
	return data[0];
}

/**
  * @brief  写寄存器
  * @note   
  * @param  reg_addr：寄存器地址
  * 		value：写入的值
  * @return 读到的寄存器值
  */
static uint8_t MN_WriteReg(uint8_t reg_addr, uint8_t value)
{
	uint8_t buffer[2] = {reg_addr, value};
	Device_Write(VHW_IIC_MN, buffer, 2, IIC_ADDR_MN);
	return 0;
}

#if defined(MN_DEBUG)
static void MN_Dump_Reg(void)
{
	uint8_t reg[8] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
	int i = 0;
	uint8_t val = 0;
	for (i = 0; i < 8; i++)
	{
		val = MN_ReadReg(reg[i]);
		__MN25713_LOG("Dump reg(%02X) = %02X\r\n", reg[i], val);
	}
}
#endif

static void MN_chip_refresh(void)
{
	MN_WriteReg(0xfd, 0x8e);//chip refresh+
	MN_WriteReg(0xfe, 0x22);
	MN_WriteReg(0xfe, 0x02);
	MN_WriteReg(0xfd, 0x00);//chip refresh-
}

static void MN_Init(void)
{
	uint16_t ps, ps_thd_h, ps_thd_l;

	MN_WriteReg(DEVREG_RESET, (EPL_POWER_ON | EPL_RESETN_RUN));//power on
	MN_WriteReg(DEVREG_PS_STATUS, (EPL_CMP_RESET | EPL_UN_LOCK));//ps cmp rest
	MN_WriteReg(DEVREG_PS_STATUS, (EPL_CMP_RUN | EPL_UN_LOCK));//ps cmp run
	MN_WriteReg(DEVREG_ALS_STATUS, (EPL_CMP_RESET | EPL_UN_LOCK));//als cmp rest
	MN_WriteReg(DEVREG_ALS_STATUS, (EPL_CMP_RUN | EPL_UN_LOCK));//als cmp run
	MN_WriteReg(DEVREG_RESET, (EPL_POWER_OFF | EPL_RESETN_RESET));//power off
	
	MN_WriteReg(DEVREG_ALS_CONFIG, (EPL_ALS_INTT_1024 | EPL_GAIN_LOW));//als integration time, gain
	MN_WriteReg(DEVREG_ALS_FILT, (EPL_RS_0 | EPL_PSALS_OSR_11 | EPL_CYCLE_16));//als rs, cycle
	MN_WriteReg(DEVREG_PS_CONFIG, (EPL_PS_INTT_272 | EPL_GAIN_LOW));//ps integration time, gain
	MN_WriteReg(DEVREG_PS_FILT, (EPL_RS_0 | EPL_PSALS_OSR_11 | EPL_CYCLE_16));//ps rs, cycle
	//调试红外灯的电流大小
	#ifdef IR_WEAK_POWER
	MN_WriteReg(DEVREG_LED_CONFIG, (EPL_PS_PRE | EPL_IR_ON_CTRL_ON | EPL_IR_MODE_CURRENT | EPL_IR_DRIVE_10));//ps ir setting
	#else
	MN_WriteReg(DEVREG_LED_CONFIG, (EPL_PS_PRE | EPL_IR_ON_CTRL_ON | EPL_IR_MODE_CURRENT | EPL_IR_DRIVE_50));//ps ir setting
	#endif
	MN_WriteReg(DEVREG_PS_INT, (EPL_INT_CTRL_ALS_OR_PS | EPL_PERIST_1 | EPL_INTTY_DISABLE));//ps intt_control, persister, intt_type
	MN_WriteReg(DEVREG_ALS_INT, (EPL_ALS_PRE | EPL_ALS_INT_CHSEL_1 | EPL_PERIST_1 | EPL_INTTY_DISABLE));//als intt_channel, persister, intt_type
	

	MN_WriteReg(DEVREG_ALS_ILTL, 0x00);//THDL1_ALS
	MN_WriteReg(DEVREG_ALS_ILTH, 0x00);//THDL2_ALS
	MN_WriteReg(DEVREG_ALS_IHTL, 0xff);//THDH1_ALS
	MN_WriteReg(DEVREG_ALS_IHTH, 0xff);//THDH2_ALS

	MN_WriteReg(DEVREG_RESET, (EPL_POWER_ON | EPL_RESETN_RUN));//power on
	MN_WriteReg(DEVREG_ENABLE, (EPL_WAIT_0_MS | EPL_MODE_PS));//ps enable

	MN_Delay(10);
	ps = MN_Read_PS();	
	MN_Delay(10);
	ps += MN_Read_PS();
	ps = ps>>1;
	
	ps_thd_h = ps + 500;
	ps_thd_l = ps + 180;

	MN_WriteReg(DEVREG_PS_ILTL, (ps_thd_l & 0x00ff));//THDL1_PS  
	MN_WriteReg(DEVREG_PS_ILTH, (ps_thd_l & 0xff00)>> 8);//THDL2_PS  
	MN_WriteReg(DEVREG_PS_IHTL, (ps_thd_h & 0x00ff));//THDH1_PS
	MN_WriteReg(DEVREG_PS_IHTH, (ps_thd_h & 0xff00)>> 8);//THDH2_PS	

	__MN25713_LOG("high thd = %d\r\n",(uint16_t)ps_thd_h);
	__MN25713_LOG("low thd = %d\r\n",(uint16_t)ps_thd_l);
}

void MN_SoftPowerOn(void)
{
	MN_Delay(2);
	MN_chip_refresh();
	MN_Delay(10);
	uint8_t chip_id = MN_ReadReg(DEVREG_REV_ID);
	MN25713_LOG("mn29xxx id : %02X\r\n", chip_id);
	if(chip_id != 0x88)
	{
		sensor_type = 0;
		__MN25713_LOG("sensor error\r\n");
	}
	if(sensor_type !=0)
	{
		MN_Init();
	}

#if defined(MN_DEBUG)
	MN_Dump_Reg();
#endif
}

uint8_t MN_Get_sensortype(void)
{
	return sensor_type;
}

void MN_SoftPowerOff(void)
{
	MN_WriteReg(DEVREG_RESET, (EPL_POWER_OFF | EPL_RESETN_RESET));
	MN_WriteReg(DEVREG_ENABLE, (EPL_MODE_IDLE));
}

uint16_t MN_Read_PS(void)
{
	uint16_t ps_data, ir_data;
	ir_data = ((MN_ReadReg(DEVREG_PS_ADATAH) << 8) | MN_ReadReg(DEVREG_PS_ADATAL)); //IR_DATA
	ps_data = ((MN_ReadReg(DEVREG_PS_RDATAH) << 8) | MN_ReadReg(DEVREG_PS_RDATAL)); //PS_DATA
	__MN25713_LOG("ir_data : %d, ps_data : %d\r\n", ir_data, ps_data);
	return ps_data;
}
