#ifndef __IRSENSOR__H__
#define __IRSENSOR__H__

#include "component.h"

/* IRsensor组件消息类型 */
typedef struct 
{
    UserAction_enum_t action;   //动作
    uint16_t distance;	        //距离
    #ifdef IR_SENSOR_YIMING
    uint16_t addr;              //传感器地址
    #endif
}IRsensorMsg_t;

#endif

