#include "stk3x3x.h"
// #define STK_DEBUG	1
#define STK3X3X_LOG(format, ...) __OSAL_LOG(C_BLUE format C_NONE, ##__VA_ARGS__)
#if defined STK_DEBUG
#define __STK3X3X_LOG(format, ...) __OSAL_LOG(C_BLUE format C_NONE, ##__VA_ARGS__)
#else
#define __STK3X3X_LOG(format, ...)
#endif

#define VHW_IIC_STK vIIC_0

static uint8_t sensor_type=0xff;//初始化成功与否的标志
/**
  * @brief  阻塞延时
  * @note   
  * @param  ms：时间ms
  */
static void STK_Delay(uint16_t ms)
{
	Device_DelayMs(ms);
}

/**
  * @brief  读寄存器
  * @note   
  * @param  reg_addr：寄存器地址
  * @return 读到的寄存器值
  */
static uint8_t STK_ReadReg(uint8_t reg_addr)
{
	uint8_t data[4] = {0};
	if (Device_Write(VHW_IIC_STK, &reg_addr, 1, IIC_ADDR_STK) == 0)
	{
		#ifdef R6A40_A
		STK_Delay(1);
		#elif R701B_A
		STK_Delay(1);
		#endif
		Device_Read(VHW_IIC_STK, data, 4, IIC_ADDR_STK);
	}
	return data[0];
}

/**
  * @brief  写寄存器
  * @note   
  * @param  reg_addr：寄存器地址
  * 		value：写入的值
  * @return 读到的寄存器值
  */
static uint8_t STK_WriteReg(uint8_t reg_addr, uint8_t value)
{
	uint8_t buffer[2] = {reg_addr, value};
	Device_Write(VHW_IIC_STK, buffer, 2, IIC_ADDR_STK);
	return 0;
}

#if defined(STK_DEBUG)
static void STK_Show_Allreg(void)
{
	uint8_t reg[13] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x10, 0x3E, 0x3F, 0xA0, 0xA1, 0xDB, 0xF4};
	int i = 0;
	uint8_t val = 0;
	for (i = 0; i < 13; i++)
	{
		val = STK_ReadReg(reg[i]);
		__STK3X3X_LOG("stk reg(%02X) = %02X\r\n", reg[i], val);
	}
}
#endif

/**
  * @brief  软件复位
  * @note   
  */
static void STK_sw_reset(void)
{
	STK_WriteReg(0x80, 0x99); //W any data to reset the chip
}

/**********************************************************************
Description: set to SNR based MPX control. Call this function before 
             tune to one specific channel
Parameters:
None
Return Value:
None
**********************************************************************/
static void STK_Init(void)
{
	uint16_t ps, ps_thd_h, ps_thd_l;

	/*
	0x05 only enable ps;0x02 only enable als; 
	0x00 disable all &go into a low power standby mode
	*/
	STK_WriteReg(0x00, 0x00);
	STK_WriteReg(0x01, 0xb2); //0x31 default//0x71 //PSCTRL 	/* ps_persistance=4, ps_gain=64X, PS_IT=0.371ms */
	STK_WriteReg(0x02, 0x32); //0x7b 1.5s//0x39 94.72ms default//0x38 //ALSCTRL 	/* als_persistance=4, als_gain=64X, ALS_IT=378.88*4=1515.52ms */
	#ifdef STK3311_X
		#ifdef IR_WEAK_POWER
		STK_WriteReg(0x03, 0x7f); //LED /* 100mA IRDR, X/64 LED duty ,X是低六位*/
		#else
		STK_WriteReg(0x03, 0xff); //LED /* 100mA IRDR, X/64 LED duty ,X是低六位*/
		#endif
	#else
	STK_WriteReg(0x03, 0x80); //LED /* 100mA IRDR, X/64 LED duty ,X是低六位*/
	#endif
	/*
    INT 0x08=INT_ALS[3]=1'b1 only enable als 
    wile(INT_ALS[3]=1'b1)
    0x0c=INT_PS[2:0] 3'b100 ,system pre-defined sequence (wk test yes);
    0x09=INT_PS[2:0] 3'b001,interrupt is issued while FLG_NF is toggled;
    0x0a=INT_PS[2:0] 3'b010/3'b011, FLAG Mode;
    0x0f=INT_PS[2:0] 3'b101/3'b110/3'b111 , issue continuous interrupt
    */
	STK_WriteReg(0x04, 0x01);
	//WAIT TIME
	#ifdef STK3311_X
	STK_WriteReg(0x05, 0x01);		//01:2*5.93ms=12 ms
	#else
	STK_WriteReg(0x05, 0x09);		//09:10*1.54ms=15.ms
	#endif
	STK_WriteReg(0x0A, 0xFF); //THDH1_ALS
	STK_WriteReg(0x0B, 0xFF); //THDH2_ALS
	STK_WriteReg(0x0C, 0x00); //THDL1_ALS
	STK_WriteReg(0x0D, 0x00); //0xf0//THDL2_ALS

	STK_WriteReg(0xA1, 0x0F);
	STK_WriteReg(0xDB, 0x14);
	//STK_WriteReg(0xF4, 0x00);

	STK_WriteReg(0x00, 0x05);
	STK_Delay(2);
	ps = STK_Read_PS();

	STK_Delay(2);
	ps += STK_Read_PS();
	ps = ps >> 1;

	// STK_WriteReg(0x00, 0x05);
	// STK_WriteReg(0x05, 0x07); //WAIT		50ms

	ps_thd_h = ps + 150; //120; //300  1580
	ps_thd_l = ps + 100; //110; //250   	1560

	STK_WriteReg(0x06, (ps_thd_h & 0xFF00) >> 8); //THDH1_PS
	STK_WriteReg(0x07, (ps_thd_h & 0x00FF));	  //THDH2_PS
	STK_WriteReg(0x08, (ps_thd_l & 0xFF00) >> 8); //THDL1_PS
	STK_WriteReg(0x09, (ps_thd_l & 0x00FF));	  //THDL2_PS
}
//STK3331传感器的芯片id是0x53，STK3311的传感器的芯片ID是0x12
void STK_SoftPowerOn(void)
{
	STK_Delay(2);
	STK_sw_reset();
	STK_Delay(10);
	uint8_t id = STK_ReadReg(0x3E);
	STK3X3X_LOG("stk3x3x id : %02X\r\n", id);
	if(id == 0x5e)
	{
		sensor_type = 0x5e;
		__STK3X3X_LOG("sensor type is stk3331-A\r\n");
	}
	else if(id == 0x53)
	{
		sensor_type = 0x53;
		__STK3X3X_LOG("sensor type is stk3331-x\r\n");
	}
	else if(id == 0x12)
	{
		sensor_type = 0x12;
		__STK3X3X_LOG("sensor type is stk3311\r\n");
	}
	else
	{
		sensor_type = 0;
		__STK3X3X_LOG("sensor error\r\n");
	}
	if(sensor_type !=0)
	{
		STK_Init();
	}

#if defined(STK_DEBUG)
	STK_Show_Allreg();
#endif
}

uint8_t STK_Get_sensortype(void)
{
	return sensor_type;
}

void STK_SoftPowerOff(void)
{
	STK_WriteReg(0x00, 0x30);
	STK_WriteReg(0x8b, 0x75);
	STK_WriteReg(0x8a, 0x7f);
}

uint16_t STK_Read_PS(void)
{
	uint16_t ps_data = ((STK_ReadReg(0x11) << 8) | STK_ReadReg(0x12)); //PS_DATA
	__STK3X3X_LOG("ps_data : %d\r\n", ps_data);
	return ps_data;
}
