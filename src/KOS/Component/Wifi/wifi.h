/**
  * @file       wifi.h
  * @brief      
  * @copyright  Copyright (c) 2020~2030 ShenZhen Dxtc Tech. Co., Ltd.
  * All rights reserved.
  * @version    V1.0
  * @author     TianXubin
  * @date       2021-11-12 10:39:45
  * @note       鼎新同创·智能锁
  *
  *****************************************************************************/

#ifndef __WIFI__H__
#define __WIFI__H__

#include "osal.h"

#pragma pack(1)

/* wifi组件消息类型（publish） */
typedef struct
{
    uint8_t   msg_type; //消息类型
    union
    {
        ApList_stu_t     ap_list;  //扫描到的ap-list     --- 当 msg_type == WIFI_MSG_SCAN_REPLY 有效
        WifiApInfo_stu_t ap_info;  //当前已连接的ap-info --- 当 msg_type == WIFI_MSG_CONNECTED 有效
    };
} WifiMsg_t;
#pragma pack()



/**
  * @brief  复位WiFi重连间隔（应用层成功连上服务器时调用）
  * @note
  */
#define Wifi_ResetReconnect()                 \
({                                            \
    uint8_t temp = WIFI_CTRL_RESET_RECONNECT; \
    OSAL_MboxPost(COMP_WIFI, 0, &temp, 1);    \
})


#define Wifi_DisEnable()                 \
({                                            \
    uint8_t temp = WIFI_CTRL_DISENABLE; \
    OSAL_MboxPost(COMP_WIFI, 0, &temp, 1);    \
})
/**
  * @brief  连接WiFi（配网时才调用）
  * @note
  * @param  ap_info：ap信息（该参数是一个结构体指针 WifiConnectPara_stu_t）
  */
#define Wifi_Connect(ap_info)                                             \
({                                                                        \
    uint8_t *temp = OSAL_Malloc(sizeof(WifiConnectPara_stu_t) + 1);       \
    temp[0] = (uint8_t)WIFI_CTRL_CONNECT;                                 \
    memcpy(temp + 1, ap_info, sizeof(WifiConnectPara_stu_t));             \
    OSAL_MboxPost(COMP_WIFI, 0, temp, sizeof(WifiConnectPara_stu_t) + 1); \
    OSAL_Free(temp);                                                      \
})

/**
  * @brief  断开WiFi
  * @note
  * @param  param：0：永久关闭WIFI
  *                1：关闭WIFI，进入间隔重连逻辑
  */
#define Wifi_Disconnect(param)                       \
({                                                   \
    uint8_t temp[2] = {(uint8_t)WIFI_CTRL_DISCONNECT, param}; \
    OSAL_LOG("\r\n%s\r\n",__FUNCTION__);\
    OSAL_MboxPost(COMP_WIFI, 0, temp, sizeof(temp)); \
})

/* 扫描wifi热点 */
#define Wifi_Scan()                                  \
({                                                   \
    uint8_t temp[2] = {(uint8_t)WIFI_CTRL_SCAN, 0};  \
    OSAL_MboxPost(COMP_WIFI, 0, temp, sizeof(temp)); \
})

/* 获取WIFI-MAC */
#define WiFi_ReadMac(_mac)                       \
({                                               \
    OSAL_NvReadGlobal(COMP_WIFI, 0, 0, _mac, 6); \
})

#endif
