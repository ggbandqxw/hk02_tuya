#ifndef __RTC_COMP_H__
#define __RTC_COMP_H__

#include "component.h"
#define RTC_COMP_NV_INIT_FLAG 0xFA // 初始化标志
#define RTC_ALARM_NUM 3           // 最大闹钟数量

#pragma pack(1)
/* RTC类型 */
typedef struct
{
    uint32_t timestamp; // 闹钟时刻
    uint32_t cycle;     // 闹钟周期
    uint8_t repeat;     // 重复标志（SET:重复创建该闹钟；RESET:单次创建该闹钟）;
    uint8_t run;        // SET:当前正在正在执行的闹钟 RESET:未执行该闹钟
    char name[20];
} RtcAlarmInfor_stu_t;
typedef struct
{
    uint8_t nvInitFlag;
    RtcAlarmInfor_stu_t Alarm[RTC_ALARM_NUM];
} RtcCompNv_stu_t;
#pragma pack()


/* Rtc闹钟控制接口 */
#define RtcAlarm_Set(_name, _cycle, _rep)                \
    ({                                                   \
        RtcAlarmInfor_stu_t temp = {0};                    \
        strcpy(temp.name, (char *)_name);                \
        temp.cycle = _cycle;                             \
        temp.repeat = _rep;                              \
        OSAL_MboxPost(COMP_RTC, 0, &temp, sizeof(temp)); \
    })

#endif /* __RTC_COMP_H__ */
