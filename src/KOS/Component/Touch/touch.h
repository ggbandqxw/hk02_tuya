#ifndef __TOUCH_H__
#define __TOUCH_H__

#include "component.h"

/* Touch组件消息类型 */
typedef struct 
{
    UserAction_enum_t action;   //动作
    char keyNum;                //按键字符
    uint8_t times;              //连按次数
}TouchMsg_t;



#endif /* __TOUCH_H__ */
