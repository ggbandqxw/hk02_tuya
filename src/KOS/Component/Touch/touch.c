#include "device.h"
#include "component.h"


/* 调试打印接口 */
#define TOUCH_LOG(format, ...) __OSAL_LOG("[touch.c] " C_GREEN format C_NONE "\r\n", ##__VA_ARGS__)

/* 触摸键扫描事件 */
#define EVENT_SCAN_TOUCH_KEY (0X00000001)
#define EVENT_RESET_CAP_SENSOR (0X00000002)
#define EVENT_LONG_PRESS (0X00000004)

/* 定义虚拟硬件 */
#define TOUCH_VIRTUALHARDWARE vCAP_SENSE2
#define VHW_IRQ_TOUCH vPIN_I21
/* 定义扫描时长 */
#define TOUCH_SCAN_TIME 10
#define TOUCH_LONG_PRESS_TIME 2000
static uint8_t lastKey = 0;

/**
  * @brief  读取按钮动作
  * @note   
  *         
  * @return 有动作返回SUCCESS  无动作返回ERROR
  */
static void TouchTask_ReadAction(void)
{
	char keyValue = 0;
	TouchMsg_t touchMsg;
	static uint16_t longPressCnt = 1;
	// Device_Write(vPIN_C5, NULL, 0, 0);//卡片复位控制
	Device_Read(TOUCH_VIRTUALHARDWARE, &keyValue, sizeof(keyValue), 0);
	if (keyValue != 0) //按下了
	{
		if (keyValue != lastKey) //第一次读到按键：短按
		{
			OSAL_EventDelete(COMP_TOUCH, EVENT_LONG_PRESS);
			OSAL_EventSingleCreate(COMP_TOUCH, EVENT_LONG_PRESS, TOUCH_LONG_PRESS_TIME, EVT_PRIORITY_MEDIUM);
			longPressCnt = 1;
			if (lastKey)
			{
				touchMsg.keyNum = lastKey;
				touchMsg.action = USER_ACTION_RELEASE;
				touchMsg.times = 0;
				OSAL_MessagePublish(&touchMsg, sizeof(touchMsg));
			}
			touchMsg.keyNum = keyValue;
			touchMsg.action = USER_ACTION_PRESS;
			touchMsg.times = 0;
			OSAL_MessagePublish(&touchMsg, sizeof(touchMsg));
		}
		// else if (longPressCnt) //读取到同样的按键值：长按
		// {
		// 	if (++longPressCnt >= 200) //长按计时：约2s
		// 	{
		// 		longPressCnt = 0;
		// 		touchMsg.keyNum = keyValue;
		// 		touchMsg.action = USER_ACTION_LONG_PRESS;
		// 		touchMsg.times = 0;
		// 		OSAL_MessagePublish(&touchMsg, sizeof(touchMsg));
		// 	}
		// }
	}
	else //松手了
	{
		OSAL_EventDelete(COMP_TOUCH, EVENT_LONG_PRESS);
		longPressCnt = 1; //清除长按计数
		if (lastKey != 0)
		{
			touchMsg.keyNum = lastKey;
			touchMsg.action = USER_ACTION_RELEASE;
			touchMsg.times = 0;
			OSAL_MessagePublish(&touchMsg, sizeof(touchMsg));
		}
	}
	lastKey = keyValue;
}
// static void Touch_IrqHandle(VirtualHardware_enum_t dev, void *data, uint32_t len)
// {
// 	OSAL_EventCreateFromISR(COMP_TOUCH);
// }
/**
 * @brief  触摸唤醒逻辑处理
 * @param
 * @return
 */
static int32_t Touch_WakeHandle(uint32_t dev)
{
	if (dev == vTIMER_1)
	{
		int32_t res = Device_Read(TOUCH_VIRTUALHARDWARE, NULL, 0, 1);
		//休眠定时读接近按键
		return (uint32_t)res;
	}
	else if (dev == vCAP_SENSE0)
	{
		//触摸中断唤醒（国芯平台触摸是中断唤醒）
		return 1; //键盘唤醒
	}
	else if (dev == vCAP_SENSE1)
	{
		//触摸中断唤醒（国芯平台触摸是中断唤醒）
		return 2; //门铃键唤醒
	}
	else if (dev == vPIN_I21)
	{
		return 1;
	}
	return 0;
}
// COMPONENT_WAKEUP_EXPORT(COMP_TOUCH, Touch_WakeHandle, vTIMER_1);
// COMPONENT_WAKEUP_EXPORT(COMP_TOUCH, Touch_WakeHandle, vCAP_SENSE0);
// COMPONENT_WAKEUP_EXPORT(COMP_TOUCH, Touch_WakeHandle, vCAP_SENSE1);
COMPONENT_WAKEUP_EXPORT(COMP_TOUCH, Touch_WakeHandle, vPIN_I21); //

/**
  * @brief  Touch任务函数
  *
  * @note   1.任务函数内不能写阻塞代码
  *         2.任务函数每次运行只处理一个事件
  *         
  * @param  event：当前任务的所有事件
  *
  * @return 返回未处理的事件
  */
static uint32_t Touch_Task(uint32_t event)
{
	/* 系统启动事件 */
	if (event & EVENT_SYS_START)
	{
		TOUCH_LOG("Touch task start\r\n");
		OSAL_EventRepeatCreate(COMP_TOUCH, EVENT_SCAN_TOUCH_KEY, TOUCH_SCAN_TIME, EVT_PRIORITY_MEDIUM);
		Device_Enable(TOUCH_VIRTUALHARDWARE);
		// static uint8_t init_flag = 0;
		// if (!init_flag)
		// {
		// 	init_flag = 1;
		// 	/* 注册中断回调 */
		// 	Device_RegisteredCB(VHW_IRQ_TOUCH, Touch_IrqHandle);
		// }
		return (event ^ EVENT_SYS_START);
	}
	/* 系统中断事件 */
	// if (event & EVENT_SYS_ISR)
	// {
	// 	TOUCH_LOG("cTouchard irq\r\n");
	// 	//TouchTask_ReadAction();
	// 	return (event ^ EVENT_SYS_ISR);
	// }
	/* 系统休眠事件 */
	if (event & EVENT_SYS_SLEEP)
	{
		Device_Disable(TOUCH_VIRTUALHARDWARE);
		OSAL_EventDelete(COMP_TOUCH, EVENT_SCAN_TOUCH_KEY);
		TOUCH_LOG("Touch task sleep\r\n");
		return (event ^ EVENT_SYS_SLEEP);
	}
	/* 扫描触摸按键 */
	if (event & EVENT_SCAN_TOUCH_KEY)
	{
		 TouchTask_ReadAction();
		return (event ^ EVENT_SCAN_TOUCH_KEY);
	}
	if (event & EVENT_RESET_CAP_SENSOR)
	{
		return (event ^ EVENT_RESET_CAP_SENSOR);
	}
	if (event & EVENT_LONG_PRESS)
	{
		TouchMsg_t touchMsg;
		touchMsg.keyNum = lastKey;
		touchMsg.action = USER_ACTION_LONG_PRESS;
		touchMsg.times = 0;
		OSAL_MessagePublish(&touchMsg, sizeof(touchMsg));
		return (event ^ EVENT_LONG_PRESS);
	}
	return 0;
}
COMPONENT_TASK_EXPORT(COMP_TOUCH, Touch_Task, 0);
