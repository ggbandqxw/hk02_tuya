#ifndef _FINGERVEIN_TASK_H_
#define _FINGERVEIN_TASK_H_

#include "component.h"
#include "device.h"


/* 指纹动作类型 */
typedef enum 
{
    FINGERVEIN_ACTION_FINGER_IN,				//检测到指静脉触摸中断
	FINGERVEIN_ACTION_ENROLL_GET_IMAGE_OK,		//注册获取图像成功
	FINGERVEIN_ACTION_VERIFY_GET_IMAGE_OK,		//验证获取图像成功
	FINGERVEIN_ACTION_GET_IMAGE_ERR,			//获取图像失败
	FINGERVEIN_ACTION_VERIFY_OK,				//校验成功
	FINGERVEIN_ACTION_VERIFY_ERR,				//校验失败
	FINGERVEIN_ACTION_ADD_ERR,					//添加失败
	FINGERVEIN_ACTION_ADD_OK,					//添加成功
	FINGERVEIN_ACTION_ENTER_AGAIN,				//再次输入
	FINGERVEIN_ACTION_WAIT_FINGER_AWAY,			//等待手指拿开
    FINGERVEIN_ACTION_ATE_TEST_RESULT,			//上报指静脉整机测试的结果
}FingerveinAction_enum_t;

/* Finger组件消息类型 */
typedef struct 
{
    FingerveinAction_enum_t action;			//指静脉动作
	uint8_t fingerveinNumber;				//指静脉编号
}FingerveinMsg_t;

/* TASK工作模式*/
typedef enum
{
	FINGERVEIN_WORK_MODE_VERIFY, //验证模式
	FINGERVEIN_WORK_MODE_ADD,    //录入模式
	FINGERVEIN_WORK_MODE_DISABLE //禁用模式
} Fingervein_WorkMode_enum_t;

#define FINGERVEIN_VERSION_LEN			9								/* 指静脉版本号长度 */
#define LOCKMODEL_LEN					5								/* lockmodel长度 */
#pragma pack(1)
typedef struct
{
	uint8_t lockModel[LOCKMODEL_LEN + 1];									//指静脉固件对应的lockmodel，+1,是字符串后面要有"\0"的结束符，						
	uint8_t fingerveinVersion[FINGERVEIN_VERSION_LEN + 1]; 					//指静脉软件版本
} NvFingervein_stu_t;
#pragma pack()

/* 指纹组件邮箱主题（APP->COMP） */
#define FINGERVEIN_SETWORKMODE          0
#define FINGERVEIN_DELETEIMAGE       	1


/* 设置FINGER工作模式 */
#define Fingervein_WorkModeSet(mode, fvn_num)                  \
({                                                         \
    uint8_t temp[3] = {FINGERVEIN_SETWORKMODE, mode, fvn_num}; \
                                                           \
    OSAL_MboxPost(COMP_FINGERVEIN, 0, temp, sizeof(temp));     \
})

/* 删除指静脉模板 */
#define Fingervein_DeleteImage(startNum, nums)                  \
({                                                            \
    uint8_t temp[3] = {FINGERVEIN_DELETEIMAGE, startNum, nums}; \
                                                              \
    OSAL_MboxPost(COMP_FINGERVEIN, 0, temp, sizeof(temp));        \
})


/* 获取指静脉固件对应的lockmodel */
#define Fingervein_GetModel(model)                               \
({                                                          \
    (OSAL_NvReadGlobal(COMP_FINGERVEIN,                          \
                       0,                   \
                       OSAL_OFFSET(NvFingervein_stu_t, lockModel), \
                       model, 5) != ERROR) ? SUCCESS : ERROR; \
})

/* 获取指静脉固件版本 */
#define Fingervein_GetVersion(ver)                               \
({                                                          \
    (OSAL_NvReadGlobal(COMP_FINGERVEIN,                          \
                       0,                   \
                       OSAL_OFFSET(NvFingervein_stu_t, fingerveinVersion), \
                       ver, 9) != ERROR) ? SUCCESS : ERROR; \
})


static ErrorStatus Fingervein_Read_Version(uint8_t *pData, uint8_t *pLen);

#endif