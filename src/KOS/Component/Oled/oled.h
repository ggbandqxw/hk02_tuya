#ifndef __OLED_H__
#define __OLED_H__
#include "component.h"

#define POP_UP_WINDOW_TIME 1500

#if defined(MENU_TYPE_OLD)
/* BOX类型 */
typedef enum
{
	BOX_TYPE_ERROR,
	BOX_TYPE_SELECT,		//选择框（交互型）
	BOX_TYPE_INPUT,			//输入框（交互型）
	BOX_TYPE_LIST,			//记录列表框（交互型）
	BOX_TYPE_TEXT,			//文本框（非交互型）
	BOX_TYPE_BATTERY,		//显示电池电量
	BOX_TYPE_RTC,			//显示年月日时分秒
	BOX_TYPE_VARIABLE_TEXT, //可变文本框
} BoxType_enum_t;

/* BOX数据结构 */
typedef struct
{
	FunctionalState state;	   // box状态：使能、禁能
	const BoxType_enum_t type; // box类型
	const FlagStatus hide;	   //隐藏输入标志
	const uint8_t xpos;		   // x y：坐标
	const uint8_t ypos;
	uint16_t current; //当前光标
	uint16_t len;	  //数据长度
	void *const data; //数据指针
} BoxData_stu_t;

/* 选择框数据结构 */
typedef struct
{
	const char item;		  //条目编号
	void (*const pFun)(void); //当前条目生效后，对应的功能函数
	const uint16_t menu;	  //当前条目对应的子菜单
	const uint16_t audio[2];  //当前条目的语音数据
	const char *ui[2];		  //当前条目的显示数据
} SelectBoxData_stu_t;

/* 输入框数据结构 */
typedef struct
{
	uint8_t *buffer;				  //输入缓存
	const char *dis_format;			  //显示格式
	void (*pFun)(uint8_t *, uint8_t); //函数功能
	uint8_t cur_mode;				  //光标模式
} InputBoxData_stu_t;

/* 文本框数据结构 */
typedef struct
{
	const char *text[2]; //文本字符串
} TextBoxData_s1tu_t;

typedef char (*ListBoxStr_t)[4][17]; //列表消息框数据类型

typedef enum
{
	UiType_UPDATE,		 //显示屏UI更新消息
	UiType_POPUPS,		 //显示屏UI弹窗消息
	UiType_BREAK_POPUPS, //打断UI弹框消息
} UiType_enum_t;

/* 显示（UI）消息结构 */
typedef struct
{
	UiType_enum_t type;
	BoxData_stu_t **boxdata;
	uint8_t language;
	const char **pop_ups0; //弹框数据内容0
	const char **pop_ups1; //弹框数据内容1
	const char **pop_ups2; //弹框数据内容2
	uint16_t time;		   // 0:永久弹窗
} UiMsg_stu_t;

extern const char *uiPopUpsTextString[][2];

/* 弹窗事件类型 */
typedef enum
{
	POPUPS_Arming,			 //已启动布防
	POPUPS_PlsEnterPwd,		 //请输入用户密码，以#号键结束
	POPUPS_VerifySuccess,	 //验证成功
	POPUPS_WelcomeHome,		 //欢迎回家
	POPUPS_VerifyFailed,	 //验证失败
	POPUPS_SystemLocked,	 //系统已锁定 //锁定5分钟，数字闪烁显示时间
	POPUPS_CardOrFp,		 //请刷卡或按指纹
	POPUPS_PwdOrCard,		 //请输入密码或卡片
	POPUPS_PwdOrFP,			 //请输入密码或指纹
	POPUPS_MasterMode,		 //已进入管理模式
	POPUPS_ExitMasterMode,	 //已退出管理模式
	POPUPS_SettingFailed,	 //设置失败
	POPUPS_SettingSucceed,	 //设置成功
	POPUPS_ScanAgain,		 //再放一次
	POPUPS_EnterAgain,		 //再输入一次
	POPUPS_AddFailed,		 //添加失败
	POPUPS_AddSucceed,		 //添加成功
	POPUPS_DelSucceed,		 //删除成功
	POPUPS_DelFailed,		 //删除失败
	POPUPS_InputError,		 //输入错误
	POPUPS_NumNotExist,		 //编号不存在
	POPUPS_NumExist,		 //编号已存在
	POPUPS_CardsEmpty,		 //卡片库为空
	POPUPS_CardNotExist,	 //卡片不存在
	POPUPS_CardExist,		 //卡片已存在
	POPUPS_CardsFull,		 //卡片库已满
	POPUPS_FPLibEmpty,		 //指纹库为空
	POPUPS_FPNotExist,		 //指纹不存在
	POPUPS_FPLibFull,		 //指纹库已满
	POPUPS_PwdLibEmpty,		 //密码库为空
	POPUPS_PwdExists,		 //密码已存在
	POPUPS_PwdLibFull,		 //密码库已满
	POPUPS_PwdNotSame,		 //两次密码不相同
	POPUPS_PwdTooSimple,	 //密码过于简单
	POPUPS_PwdRange,		 //密码长度6~12
	POPUPS_Wait,			 //请稍后...
	POPUPS_FactoryReset,	 //已恢复到出厂设置
	POPUPS_LowPower,		 //电量低请更换电池
	POPUPS_VoicePromptOn,	 //语音模式
	POPUPS_VoicePromptOff,	 //静音模式
	POPUPS_RecordEmpty,		 //开门记录为空
	POPUPS_OpenError,		 //非正常开锁
	POPUPS_TamperAlarm,		 //防撬报警
	POPUPS_BleConnected,	 //蓝牙已连接
	POPUPS_ScanCode,		 //请刷防伪码
	POPUPS_Code,			 //防伪码
	POPUPS_AGAINST,			 //已反锁
	POPUPS_PressFP,			 //请按指纹
	POPUPS_SwipingCard,		 //请刷卡
	POPUPS_ActivateSuccess,	 //激活成功
	POPUPS_SafeMode,		 //安全模式
	POPUPS_TryAgainLater,	 //请稍后再试
	POPUPS_PleaseEnterAgain, //请重新输入
	POPUPS_ManageMode,		 //管理模式
	POPUPS_BleOpen,			 //蓝牙已开启
	POPUPS_BleClose,		 //蓝牙已开启
	POPUPS_MoveFinger,		 //请拿开手指
	POPUPS_PutItAgain,		 //再放一次
	POPUPS_TurnLeftFinger,	 //向左微微移动手指
	POPUPS_TurnRightFinger,	 //向右微微移动手指
	POPUPS_UpWardFinger,	 //向上微微移动手指
	POPUPS_DownWardFinger,	 //向下微微移动手指
	POPUPS_RESTORING,		 //正在恢复请稍后
	POPUPS_ABNORMAL_LOCK,	 //锁体异常
	POPUPS_PROTECT_ALARM,	 //布防报警
	POPUPS_UNSET_PWD,		 //未设置密码
	POPUPS_NO_ADD_USER_CARD, //为添加用户卡
	POPUPS_NO_ADD_FPT,		 //未添加指纹
	POPUPS_ForSafeMode,		 //为安全模式
	POPUPS_NormalMode,		 //正常模式
	POPUPS_NUM_OVERFLOW,	 //编号超出范围
	POPUPS_FIRMARE_VERSION,	 //固件版本
	POPUPS_BLE_MAC,			 //蓝牙MAC
	POPUPS_OLED_TEST_WHITE,	 // OLED测试显示白色
	POPUPS_OLED_TEST_BLACK,	 // OLED测试显示黑色
	POPUPS_PID,				 //PID
	// POPUPS_CompilingDate, //编译日期
	POPUPS_NULL = 0XFF,
} PopUpsEvent_enum_t;
#endif


#if defined(OLED_FUNCTION)

#define Oled_UpDateMenuDisplay(_plist)                    \
	({                                                    \
		UiMsg_stu_t temp;                                \
		temp.type = UiType_UPDATE;                       \
		temp.language = Menu_GetSysLanguage();           \
		temp.boxdata = (BoxData_stu_t **)_plist;         \
		OSAL_MboxPost(COMP_OLED, 0, &temp, sizeof(temp)); \
	})
#else

#define Oled_UpDateMenuDisplay(_plist) 

#endif

#if defined(OLED_FUNCTION)
#define Oled_PopUpDisplay(_event1, _event2, _time)                                                                              \
	({                                                                                                                          \
		UiMsg_stu_t temp;                                                                                                      \
		temp.type = UiType_POPUPS;                                                                                             \
		temp.language = Menu_GetSysLanguage();                                                                                 \
		temp.pop_ups0 = (POPUPS_NULL == (PopUpsEvent_enum_t)_event1) ? NULL : uiPopUpsTextString[(PopUpsEvent_enum_t)_event1]; \
		temp.pop_ups1 = (POPUPS_NULL == (PopUpsEvent_enum_t)_event2) ? NULL : uiPopUpsTextString[(PopUpsEvent_enum_t)_event2]; \
		temp.pop_ups2 = NULL;                                                                                                  \
		temp.time = (uint16_t)_time;                                                                                           \
		OSAL_MboxPost(COMP_OLED, 0, &temp, sizeof(temp));                                                                       \
	})
#else

#define Oled_PopUpDisplay(_event1, _event2, _time)      

#endif

#if defined(OLED_FUNCTION)
#define Oled_PopUpVaribaleText(event, pText1, pText2, _time)                                                                 \
	({                                                                                                                      \
		static uint8_t *pdatatext[2][2];                                                                                    \
		static uint8_t text1[17] = {0}, text2[17] = {0};                                                                    \
		UiMsg_stu_t temp;                                                                                                  \
		if ((uint8_t *)pText1 != NULL)                                                                                      \
		{                                                                                                                   \
			memcpy(text1, (uint8_t *)pText1, 16);                                                                           \
			pdatatext[0][0] = text1;                                                                                        \
			pdatatext[0][1] = text1;                                                                                        \
		}                                                                                                                   \
		if ((uint8_t *)pText2 != NULL)                                                                                      \
		{                                                                                                                   \
			memcpy(text2, (uint8_t *)pText2, 16);                                                                           \
			pdatatext[1][0] = text2;                                                                                        \
			pdatatext[1][1] = text2;                                                                                        \
		}                                                                                                                   \
		temp.type = UiType_POPUPS;                                                                                         \
		temp.language = Menu_GetSysLanguage();                                                                             \
		temp.pop_ups0 = (POPUPS_NULL == (PopUpsEvent_enum_t)event) ? NULL : uiPopUpsTextString[(PopUpsEvent_enum_t)event]; \
		temp.pop_ups1 = ((uint8_t *)pText1 == NULL) ? NULL : (const char **)pdatatext[0];                                  \
		temp.pop_ups2 = ((uint8_t *)pText2 == NULL) ? NULL : (const char **)pdatatext[1];                                  \
		temp.time = (uint16_t)_time;                                                                                        \
		OSAL_MboxPost(COMP_OLED, 0, &temp, sizeof(temp));                                                                   \
	})
#else
#define Oled_PopUpVaribaleText(event, pText1, pText2, _time)      

#endif
#if defined(OLED_FUNCTION)

#define Oled_BreakPopUpDisplay()                          \
	({                                                    \
		UiMsg_stu_t temp;                                \
		temp.type = UiType_BREAK_POPUPS;               \
		OSAL_MboxPost(COMP_OLED, 0, &temp, sizeof(temp));\
	})
#else

#define Oled_BreakPopUpDisplay() 

#endif
#endif
