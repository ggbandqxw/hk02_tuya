/**
  ******************************************************************************
  * @file         oled_task.c
  * @brief        应用层OLED任务功能逻辑
  * @author       Leo
  * @date         2018-12-25
  * @version      v001
  * @copyright    dxtc
  * 
  * @note         凯迪仕·智能锁
  *
  ******************************************************************************
  */
#include "oled_display.h"
#include "oled.h"


#if defined(OLED_FUNCTION)
    
/* 调试打印接口 */
#define OLED_LOG(format, ...) OSAL_LOG(C_YELLOW format C_NONE, ##__VA_ARGS__)
/* OLED测试事件 */
#define EVENT_OLED_TEST  (0X00000001)
#define EVENT_OLED_POPUP_FINISH  (0X00000002)
#define EVENT_OLED_REFRESH_MENU  (0X00000004)

#define MAIN_MENU_REFRESH_TIME   300



#if defined(OLED_FUNCTION)
/* 弹窗UI文本内容 */
const char *uiPopUpsTextString[][2] = {
	"已启动布防", "Defense mode on",
	"请输入密码", "Input PIN code",
	"验证成功", "Verify succeed",
	"欢迎回家", "Welcome home",
	"验证失败", "Verify failed",
	"系统已锁定", "System locked", //锁定5分钟，数字闪烁显示时间
	"请刷卡或按指纹", "Card or finger",
	"请输入密码或卡片", "PIN or card",
	"请输入密码或指纹", "PIN or finger",
	"已进入管理模式", "Master mode",
	"已退出管理模式", "Exit master mode",
	"设置失败", "Setting failed",
	"设置成功", "Settings succeed",
	"再放一次", "Scan again",
	"再输入一次", "Enter again",
	"添加失败", "Add failed",
	"添加成功", "Add success",
	"删除成功", "Del success",
	"删除失败", "Del failed",
	"输入错误", "Input error",
	"编号不存在", "Num not exist",
	"编号已存在", "Num exist",
	"卡片库为空", "Cards empty",
	"卡片不存在", "Card not exist",
	"卡片已存在", "Card exist",
	"卡片库已满", "Cards full",
	"指纹库为空", "FP lib empty",
	"指纹不存在", "FP not exist",
	"指纹库已满", "FP Lib full",
	"密码库为空", "PIN Lib empty",
	"密码已存在", "PIN exists",
	"密码库已满", "PIN Lib full",
	"两次密码不一致", "PIN not same",
	"密码过于简单", "PIN too simple",
	"长度:6-12", "Length:6-12",
	"请稍后...", "Wait...",
	"已恢复到出厂设置", "Factory reset",
	"电量低请更换电池", "Low power",
	"语音模式", "Voice mode",
	"静音模式", "Silent mode",
	"开门记录为空", "Record empty",
	"非正常开锁", "Abnormal unlock",
	"防撬报警", "Tamper alarm",
	"蓝牙已连接", "BLE connected",
	"请刷防伪码", "Scan code",
	"防伪码:", "Code:",
	"已反锁", "Against locked",
	"请按指纹", "Press finger",
	"请刷卡", "Swiping card",
	"激活成功", "Activate success",
	"安全模式", "Safe mode",
	"请稍后再试", "Try again later",
	"请重新输入", "Enter again",
	"管理模式", "Manage mode",
	"蓝牙已开启", "Ble open",
	"蓝牙已关闭", "Ble close",
	"请拿开手指", "Move finger",
	"再放一次", "Put it again",
	"向左微微移动手指", "Turn left",
	"向右微微移动手指", "Turn right",
	"向上微微移动手指", "Up ward",
	"向下微微移动手指", "Down ward",
	"正在恢复请稍后", "Restoring...",
	"锁体异常", "Abnormal lock",
	"布防报警", "Protection alarm",
	"未设置密码", "UnSet Pwd",
	"未添加用户卡", "No add card",
	"未添加指纹", "No add fpt",
	"为安全模式", "Safe mode",
	"正常模式", "Normal mode",
	"编号超出范围", "Num overflow",
	"固件版本:", "Firmware version",
	"蓝牙Mac:", "Ble mac:",
	"WHITE", "WHITE", //弹框全白
	"BLACK", "BLACK", //弹框全黑
	"PID", "PID",	  //pid
};
#endif

/* OLED设备句柄 */
// static DeviceHandle_t oledDevHandle = NULL;
static uint8_t g_sysLanguage =0;//系统语言
/* 当前页面BOX列表 */
static BoxData_stu_t **g_boxList = NULL;



static  FlagStatus  g_refreshMenuFlag =RESET;//刷新菜单标志
static  uint16_t      g_recordLastCurrent =0;//记录查询的上一次光标位置
static  FlagStatus   g_recordInitFlag =SET;
static uint16_t  lastBoxListCurrent =0XFFFF;
 static  FlagStatus  gRefreshRateChange =RESET;//刷新率改变标志
typedef enum
{
	POPUP_FINISH,//弹框完成
	POPUP_RUNNING,//弹框中
	POPUP_RUNNING_AND_RECEIVE,//弹框中又接收到新的弹框
}PopupStatusType_enum_t;

#define MSG_BUFF_LEN   4
static  PopupStatusType_enum_t  g_popupStatus =POPUP_FINISH;
static  UiMsg_stu_t g_PopUpMsgBuff[MSG_BUFF_LEN];
static uint8_t g_WritePopUpMsgIndex =0;//写BUFF消息缓存索引
static uint8_t g_ReadPopUpMsgIndex =0;//读BUFF消息缓存索引
/* 显示类型 */ 
typedef enum
{
	NORMAL_DISPLAY,//正显
	ABNORMAL_DISPLAY,	//反显
}DisplayType_enum_t;



typedef enum
{
	OLED_CHINESE,
	OLED_ENGLISH,
}LanguageType_enum_t;

/**
  * @brief  hex转字符
  *         
  * @note   
  * @param  len: hex数组长度
  * @param  inchar：输入hex数组首地址
  * @param  outtxt：转换输出成字符数组首地址
  */
// static void OledTask_Hex2Str (uint8_t *inchar,  int len, uint8_t *outtxt)
// {
//         unsigned char hbit,lbit;
//         unsigned int i;
//   	for(i=0;i<len;i++)
// 	{
// 		hbit = (*(inchar+i)&0xf0)>>4;
// 		lbit = *(inchar+i)&0x0f;
// 		if (hbit>9) outtxt[2*i]='A'+hbit-10;
// 		else outtxt[2*i]='0'+hbit;
// 		if (lbit>9) outtxt[2*i+1]='A'+lbit-10;
// 		else    outtxt[2*i+1]='0'+lbit;
// 	 }
// }

/**
  * @brief  清除显示缓存数据
  *         
  * @note   只清除显示缓存，不做实际清屏操作
  * @param                        
  */
static void OledTask_ClearDisplay(void)
{
	Oled_DisplayClear();
}



/**
  * @brief  显示文本
  *         
  * @note   
  * @param  pText：文本UI 数据指针
  */
static void OledTask_DisplayText(BoxData_stu_t *pText)
{
	if (pText->state)
	{
		TextBoxData_s1tu_t  *boxTest =  pText->data;
		Oled_DisStringInPutDisplayMemory(pText->xpos, pText->ypos, boxTest->text[(int)g_sysLanguage], 0, 0);
	}
}

/**
  * @brief  显示可变文本内容  文本内容不是固定的
  *         
  * @note   
  * @param  pText：文本UI 数据指针
  */
static void OledTask_DisplayVariableText(BoxData_stu_t *pText)
{
	if (pText->state)
	{
		int len =0;
		InputBoxData_stu_t  *boxTest =  pText->data;
		len = strlen((char*)boxTest->buffer);
		uint8_t x1=0;
		if(len <16)
		{
			if(len <16)
			{
				x1 =(128-len*8)/2;
			}
			Oled_DisStringInPutDisplayMemory(x1, 3, (const char*)boxTest->buffer, 0, 0);
		}
		else
		{
			uint8_t dataText1[17]={0};
			uint8_t dataText2[17]={0};
			memcpy(dataText1,boxTest->buffer,16);
			len = strlen((char*)boxTest->buffer+16);
			memcpy(dataText2,boxTest->buffer+16,len);
			Oled_DisStringInPutDisplayMemory(0, 3,(const char*)dataText1, 0, 0);	
			if(len <16)
			{
				x1 =(128-len*8)/2;
			}
			Oled_DisStringInPutDisplayMemory(x1, 5,(const char*)dataText2, 0, 0);
		}
		
	}
}


/**
  * @brief  显示选择框文本内容
  *         
  * @note   
  * @param  pText：文本UI 数据指针
  */
static void OledTask_DisplaySelectBox(BoxData_stu_t *pText)
{
	uint8_t i =0;
    static uint8_t startline=0;
	uint16_t focus;
	if (pText->state)
	{
		SelectBoxData_stu_t  *boxTest =(SelectBoxData_stu_t  *)pText->data;
		if(pText->len <4)//长度小于4的话光标显示位置等于i位置
		{
			for (i=0; i<4 && i<pText->len; i++)
	        	{
	        		focus = (i == pText->current)?0XFFFF:0x0000;
				Oled_DisStringInPutDisplayMemory(pText->xpos, i*2+pText->ypos, boxTest[i].ui[g_sysLanguage], focus, 0);
			}
		}
		else
		{
			if(pText->current ==0)
			{
				startline =0;
			}
			else
			{
				if(pText->current > startline +3)//判断当前光标是否超出当前显示的位置
				{
					startline = pText->current-3;
				}
				else if(pText->current < startline)//判断当前光标是否超出当前显示的位置
				{
					startline =startline-1;
				}
			}
			for (i=0; i<4 && i<pText->len; i++)
	        {
	        	focus = (i +startline== pText->current)?0XFFFF:0x0000;
				Oled_DisStringInPutDisplayMemory(pText->xpos, i*2+pText->ypos, boxTest[i+startline].ui[g_sysLanguage], focus, 0);
			}
			
		}
	}
}

/**
  * @brief显示电池电量
  *         
  * @note   
  * @param  pText：电池电量消息box指针  光标数据为电池电量值
  */
static void OledTask_DisplayBattery(BoxData_stu_t *pText)
{
	
	if (pText->state)
	{
		//pText->current 将光标值作为电量显示值
		//  OLED_LOG("battery:%d\r\n",pText->current);
		 if(pText->current >=90) pText->current =100;
		 Oled_DisElectricity(pText->xpos,pText->ypos, pText->current/10);
	}
}

/**
  * @brief 显示主页面时间跟日期
  *         
  * @note   
  * @param  pText：RTC时间日期消息BOX内容，具体
  *数据内容通过函数指针获取
  */
static void OledTask_DisplayRtcTime(BoxData_stu_t *pText)
{
	uint32_t timestamp;

	if(pText->state)
	{
		uint8_t rtcTimeHex[6];
		uint8_t date[]="20  -  -  ";
		uint8_t time[]="  :  :  ";
		ErrorStatus (*pFun)(uint8_t*,TimeType) = (ErrorStatus (*)(uint8_t *,TimeType))pText->data;
		pFun(rtcTimeHex,T_TIME);
		
		timestamp = OSAL_Time2UTC(rtcTimeHex,0)+28800; //UTC时间+8小时 显示中国时区时间
		OSAL_UTC2Time(timestamp,rtcTimeHex);
		
		date[2]=(rtcTimeHex[0]/10)+'0'; date[3]=(rtcTimeHex[0]%10)+'0';
		date[5]=(rtcTimeHex[1]/10)+'0'; date[6]=(rtcTimeHex[1]%10)+'0';
		date[8]=(rtcTimeHex[2]/10)+'0'; date[9]=(rtcTimeHex[2]%10)+'0';
		
		time[0]=(rtcTimeHex[3]/10)+'0'; time[1]=(rtcTimeHex[3]%10)+'0';
		time[3]=(rtcTimeHex[4]/10)+'0'; time[4]=(rtcTimeHex[4]%10)+'0';
		time[6]=(rtcTimeHex[5]/10)+'0'; time[7]=(rtcTimeHex[5]%10)+'0';
		Oled_DisStringInPutDisplayMemory(pText->xpos, pText->ypos, (const char*)date, 0, 0);
		Oled_DisStringInPutDisplayMemory(pText->xpos, 2+pText->ypos,(const char*)time, 0, 0);
	}
}



static void OledTask_DisplayRecordList(BoxData_stu_t *pText)
{

	uint8_t i =0;
	static ListBoxStr_t  pSrc=NULL;
	uint16_t focus;
	static uint16_t startline =0;
	ListBoxStr_t (*pFun)(uint16_t, uint16_t ) = (ListBoxStr_t (*)(uint16_t, uint16_t ))pText->data;
	if(pText ->len >1)  
	{
		
	}
	else
	{
		g_recordLastCurrent =0;
	}
	if(pFun !=NULL)
	{
		OLED_LOG("g_recordLastCurrent:%d,pText->current:%d,g_recordInitFlag:%d\r\n",g_recordLastCurrent,pText->current,g_recordInitFlag);
		if(pText->current != g_recordLastCurrent  || (g_recordInitFlag == SET))
		{
			if(g_recordLastCurrent == pText->len-1 && pText->current ==0)
			{
				g_recordLastCurrent =1;
			}
			else if(pText->current == pText->len-1)
			{
				g_recordLastCurrent =pText->current -1;
			}
			if(pText->current == g_recordLastCurrent)
			{
				pText->current =pText->current+1;
				if(pText->current ==pText->len) pText->current =0;
				g_recordInitFlag =RESET;
			}
			pSrc =pFun(pText->current,g_recordLastCurrent);
			g_recordLastCurrent = pText->current ;
			if(pText->len >1)
			{
				if(pText->len <2)
				{
					for (i=0; i<2 && i<pText->len; i++)
			        	{
			        		focus = (i == pText->current)?0XFFFF:0x0000;
						Oled_DisStringInPutDisplayMemory(pText->xpos, pText->ypos+i*4, pSrc[g_sysLanguage][i], focus, 0);
						Oled_DisStringInPutDisplayMemory(pText->xpos, pText->ypos+i*4+2, pSrc[g_sysLanguage][i], focus, 0);
					}
				}
				else
				{

					if(pText->current ==0 ||g_recordInitFlag ==SET)
					{
						startline =0;
					}
					else
					{
						if(pText->current > startline +1)//判断当前光标是否超出当前显示的位置
						{
							startline = pText->current-1;
						}
						else if(pText->current < startline)
						{
							//startline =startline-1;
							startline = pText->current;
						}
					}
					for(i =0;i<2;i++)
					{
						focus = (i +startline== pText->current)?0XFFFF:0x0000;//高亮显示
                        if(g_recordInitFlag ==SET)
						{
							(i==0) ? (focus=0XFFFF) : (focus=0x0000);
						}
						Oled_DisStringInPutDisplayMemory(pText->xpos, pText->ypos+i*4, pSrc[g_sysLanguage][i*2], focus, 0);
						Oled_DisStringInPutDisplayMemory(pText->xpos, pText->ypos+i*4+2, pSrc[g_sysLanguage][i*2+1], focus, 0);
					}
				}
			}
			else//只有一条记录
			{
				for(i =0;i<2;i++)
				{
					Oled_DisStringInPutDisplayMemory(pText->xpos, pText->ypos+i*2, pSrc[g_sysLanguage][i], 0xffff, 0);
				}
			}
			
			if(g_recordInitFlag  ==SET)
			{
				g_recordInitFlag =RESET;
			}
		}
	}
	else
	{
		OLED_LOG("DisplayRecordList is NULL\r\n");
	}
}

/**
  * @brief  显示输入框
  *         
  * @note   
  * @param  pInputBox：输入框UI 数据指针
  */
static void OledTask_DisplayInputBox(BoxData_stu_t *pInputBox)
{
	static uint8_t flashFlag =0;
	uint8_t  inputLen;
 	uint8_t i;
  	char stringBuf[17] = {0};
		
	if (pInputBox->state == ENABLE)
	{
		InputBoxData_stu_t   *pInputBuf = (InputBoxData_stu_t*)pInputBox->data;

		if(pInputBuf->dis_format ==NULL)
		{
			inputLen = (pInputBox->current >= 15) ? 15 : pInputBox->current;
			
			memset(stringBuf, ' ', 16);
			for (i=0; i<inputLen; i++)
			{
				stringBuf[i] = (pInputBox->hide == SET) ? ('*') : (pInputBuf->buffer[i]+'0');
			}
			stringBuf[inputLen] = (flashFlag==0)? '_' : ' ';//显示光标

			Oled_DisStringInPutDisplayMemory(pInputBox->xpos, pInputBox->ypos,(const char *)stringBuf, 0, 0);
		}
		else
		{
			char index=0 ;
			strcpy(stringBuf, pInputBuf->dis_format);
			for (i=0; i<strlen(stringBuf); i++)
			{
				if (stringBuf[i] == ' ')
				{
					if ((flashFlag ==0) && (index == pInputBox->current%6)) 
					{
						stringBuf[i] = '_';
					}
					else
					{
						stringBuf[i] = pInputBuf->buffer[index] + '0';
					}
					index++;
				}
			}
			Oled_DisStringInPutDisplayMemory(pInputBox->xpos, pInputBox->ypos,(const char *)stringBuf, 0, 0);
		}
		flashFlag = !flashFlag;
	}
}






/**
  * @brief  显示当前菜单UI
  *         
  * @note 为防止OLED不断地重复刷屏，现在改为先将显示内容更新到显示显存，
  * 将整屏的内容放置入显存后再统一刷屏，一个界面只刷一次全屏，因此必须掉
  * 用Oled_RefreshDisplay(oledDevHandle)函数，否则显示内容则无更新或者不显示
  * @param  pWidget：UI列表指针
  */
static void OledTask_DisplayMenuUi(BoxData_stu_t **pWidget)
{
	BoxData_stu_t *pCommon;
	OledTask_ClearDisplay();
	for (; *pWidget != NULL; pWidget++)
	{
		pCommon = *pWidget;
		switch (pCommon->type)//UI类型
		{
			case BOX_TYPE_SELECT:OledTask_DisplaySelectBox(*pWidget);break;//选择框（交互型）
			case BOX_TYPE_INPUT:OledTask_DisplayInputBox(*pWidget);break;  //输入框（交互型）
			case BOX_TYPE_TEXT: OledTask_DisplayText(*pWidget); break; //文本框（非交互型）
			case BOX_TYPE_BATTERY:OledTask_DisplayBattery(*pWidget);break;//显示电池电量
			case BOX_TYPE_RTC: OledTask_DisplayRtcTime(*pWidget); break;//显示时间日期
			case BOX_TYPE_LIST:OledTask_DisplayRecordList(*pWidget);break;//记录列表查询
			case BOX_TYPE_VARIABLE_TEXT:OledTask_DisplayVariableText(*pWidget);//显示可变的文本内容
			case BOX_TYPE_ERROR:break;
			default: break;
		}
	}
	//OLED_LOG("OledTask_DisplayMenuUi\r\n");
	Oled_RefreshDisplay();
}

/**
  * @brief  清除菜单页面显示
  *         
  * @note   
  * @param  pWidget：UI列表指针  当本次菜单与上一次
  *                                  菜单数据不一样时才清屏
  */
static void OledTask_ClearMenuDisplay(BoxData_stu_t **boxlist)
{
	static BoxData_stu_t** preBoxList =NULL;
	BoxData_stu_t *pSrc = NULL;
	FlagStatus refreshRateChange =RESET;
	if(preBoxList !=boxlist )
	{
		Oled_DisplayClear();
		preBoxList =boxlist;
		for(; *boxlist !=NULL ; boxlist++)
		{
			pSrc =*boxlist; 
			if(pSrc->type ==  BOX_TYPE_LIST)
			{
				g_recordInitFlag = SET;
				refreshRateChange = SET;
				gRefreshRateChange =SET;
				g_recordLastCurrent = (pSrc->current  < 599) ?  g_recordLastCurrent  = pSrc->current +1 : 0 ;
			}
		}
		if(refreshRateChange == RESET)
		{
			if(gRefreshRateChange ==SET)
			{
				gRefreshRateChange =RESET;
				OSAL_EventDelete(COMP_OLED, EVENT_OLED_REFRESH_MENU);
				OSAL_EventRepeatCreate(COMP_OLED, EVENT_OLED_REFRESH_MENU, MAIN_MENU_REFRESH_TIME, EVT_PRIORITY_MEDIUM);
			}
		}
		else
		{
			OSAL_EventDelete(COMP_OLED, EVENT_OLED_REFRESH_MENU);
			OSAL_EventRepeatCreate(COMP_OLED, EVENT_OLED_REFRESH_MENU, MAIN_MENU_REFRESH_TIME -150, EVT_PRIORITY_MEDIUM);
		}
		
	}
}



/**
  * @brief 显示弹框消息
  *         
  * @note 为防止OLED不断地重复刷屏，现在改为先将显示内容更新到显示显存，
  * 将整屏的内容放置入显存后再统一刷屏，一个界面只刷一次全屏，因此必须掉
  * 用Oled_RefreshDisplay(oledDevHandle)函数，否则显示内容则无更新或者不显示
  * @param    text:待显示的文本内容         
  * @param    time:显示时间 单位ms
  */
static void OledTask_DisplayPopUp(const char**text1 , const char** text2, const char** text3, uint16_t time)
{
	uint8_t x1 =0, x2 =0,x3 =0;
	int len =0;
	// if(!OSAL_EventDelete(NULL, EVENT_OLED_REFRESH_MENU))
	// {
	// 	OLED_LOG("Delete error\r\n");
	// 	OSAL_EventDelete(NULL, EVENT_OLED_REFRESH_MENU);
	// }
	// else
	// {
	// 	OLED_LOG("Delete succeed\r\n");
	// }
	OSAL_EventDelete(COMP_OLED, EVENT_OLED_REFRESH_MENU);
	OledTask_ClearDisplay();
	if(OLED_CHINESE == g_sysLanguage)
	{
		if(text1 != NULL)
		{
			len =strlen(text1[g_sysLanguage])/2;//居中计算
			if(len <8) x1 =(128-len*16)/2;
		}
		if(text2 != NULL)
		{
			len =strlen(text2[g_sysLanguage])/2;//居中计算
			if(len <8)x2 =(128-len*16)/2;
		}
		if(text3 != NULL)
		{
			len =strlen(text3[g_sysLanguage])/2;//居中计算
			if(len <8)x3 =(128-len*16)/2;
		}
	}
	else
	{
		if(text1 != NULL)
		{
		    len =strlen(text1[g_sysLanguage]);
			if(len <16) x1 =(128-len*8)/2;
		}
	    if(text2 != NULL)
		{
		    len =strlen(text2[g_sysLanguage]);
			if(len <16)x2 =(128-len*8)/2;
		}
		if(text3 != NULL)
		{
		    len =strlen(text3[g_sysLanguage]);
			if(len <16) x3 =(128-len*8)/2;
		}
	}

    if(text1 != NULL  && text2 ==NULL && text3 ==NULL)
	{
		Oled_DisStringInPutDisplayMemory(x1, 3, text1[g_sysLanguage], 0, 0);
	}
	else if(text1 != NULL  && text2 !=NULL && text3 ==NULL)
	{
		Oled_DisStringInPutDisplayMemory(x1, 2, text1[g_sysLanguage], 0, 0);
		Oled_DisStringInPutDisplayMemory(x2, 4, text2[g_sysLanguage], 0, 0);
	}
	else if(text1 != NULL  && text2 !=NULL && text3 !=NULL)
	{
		Oled_DisStringInPutDisplayMemory(x1, 1, text1[g_sysLanguage], 0, 0);
		Oled_DisStringInPutDisplayMemory(x2, 3, text2[g_sysLanguage], 0, 0);
		Oled_DisStringInPutDisplayMemory(x3, 5, text3[g_sysLanguage], 0, 0);
	}
	if(time == 0)
	{
		g_refreshMenuFlag =RESET;
	}
	else
	{
		OSAL_EventSingleCreate(COMP_OLED, EVENT_OLED_POPUP_FINISH, time, EVT_PRIORITY_MEDIUM);
	}
	if(text1 != NULL  || text2 !=NULL || text3!=NULL)
	{
		Oled_RefreshDisplay();
	}
	
}
/**
  * @brief 开始刷新菜单显示内容
  *         
  * @note  
  * @param     
  * @param    
  */
static void  OledTask_StartRefreshDisplay(void)
{
	if(g_refreshMenuFlag == RESET)
	{
		OledTask_ClearDisplay();
		OSAL_EventDelete(COMP_OLED, EVENT_OLED_POPUP_FINISH);
		OSAL_EventRepeatCreate(COMP_OLED, EVENT_OLED_REFRESH_MENU, (gRefreshRateChange ==SET) ? MAIN_MENU_REFRESH_TIME -150 : MAIN_MENU_REFRESH_TIME, EVT_PRIORITY_MEDIUM);
		g_refreshMenuFlag =SET;
	}
}




//测试OLED
static void OledTask_Test(void)
{
	// static uint16_t mode = 0;
	// Oled_DisStringInPutDisplayMemory(10, 0, "helloworld", mode, 0);
	// mode = ~mode;
}


/**
  * @brief  OLED任务 系统消息处理函数
  * @note   
  *         
  * @param  pMsg：收到的消息
  */
static void OledTask_SysMsgProcess(UiMsg_stu_t* pMsg)
{
	OLED_LOG("pMsg->type:%d\r\n",pMsg->type);
	if (pMsg->type== UiType_POPUPS)
	  {
		char **productTest =NULL; 
		g_sysLanguage = pMsg->language;
		productTest =(char **)pMsg->pop_ups0;
		if(strncmp(productTest[0], "WHITE", 5) ==0|| strncmp(productTest[0], "BLACK", 5) ==0)
		{
			OSAL_EventDelete(COMP_OLED, EVENT_OLED_REFRESH_MENU);
			OSAL_EventDelete(COMP_OLED, EVENT_OLED_POPUP_FINISH);
			if(strncmp(productTest[0], "WHITE", 5) == 0)
			{
				Oled_AllWhiteTest();
				OSAL_EventSingleCreate(COMP_OLED, EVENT_OLED_POPUP_FINISH, pMsg->time, EVT_PRIORITY_MEDIUM);
				Oled_RefreshDisplay();
				g_popupStatus =POPUP_RUNNING;
				return;
			}
			else if(strncmp(productTest[0], "BLACK", 5) == 0)
			{
				OledTask_ClearDisplay();
				OSAL_EventSingleCreate(COMP_OLED, EVENT_OLED_POPUP_FINISH, pMsg->time, EVT_PRIORITY_MEDIUM);
				Oled_RefreshDisplay();
				g_popupStatus =POPUP_RUNNING;
				return;
			}
		}
		else
		{
			if(g_popupStatus == POPUP_FINISH)
			{
				g_popupStatus =POPUP_RUNNING;
				OledTask_DisplayPopUp(pMsg->pop_ups0,pMsg->pop_ups1,pMsg->pop_ups2,pMsg->time); 
				OLED_LOG("Receive Popup\r\n");
			}
			else
			{
				g_popupStatus = POPUP_RUNNING_AND_RECEIVE;
				memcpy(&g_PopUpMsgBuff[g_WritePopUpMsgIndex],pMsg,sizeof(g_PopUpMsgBuff[0]));
				g_WritePopUpMsgIndex = (g_WritePopUpMsgIndex == MSG_BUFF_LEN) ? 0 : g_WritePopUpMsgIndex+1;
				OLED_LOG("Receive WritePopIndex:%d\r\n",g_WritePopUpMsgIndex);
			}
		}
		g_recordInitFlag = SET;
	  }
	  else if(pMsg->type== UiType_BREAK_POPUPS)
	  {
		if(g_popupStatus != POPUP_FINISH)
		{
			OSAL_EventDelete(COMP_OLED, EVENT_OLED_POPUP_FINISH);
			g_ReadPopUpMsgIndex = 0;
			g_WritePopUpMsgIndex =0;
			g_popupStatus = POPUP_FINISH;
			lastBoxListCurrent = 0xFFFF;
			g_recordInitFlag = SET; 
			OSAL_EventRepeatCreate(COMP_OLED, EVENT_OLED_REFRESH_MENU, (gRefreshRateChange ==SET) ? MAIN_MENU_REFRESH_TIME -150 : MAIN_MENU_REFRESH_TIME, EVT_PRIORITY_MEDIUM);
		}
	  }
	  else  if (pMsg->type== UiType_UPDATE)
	  {
			g_boxList =pMsg->boxdata;
			g_sysLanguage = pMsg->language;
			OledTask_ClearMenuDisplay(g_boxList);
	  }
}
#endif

/**
  * @brief  OLED任务函数
  *
  * @note   1.任务函数内不能写阻塞代码
  *         2.任务函数每次运行只处理一个事件
  *         3.任务函数必须添加到osal_config.h文件里面
  *         
  * @param  event：当前任务的所有事件
  *
  * @return 返回未处理的事件
  */
static uint32_t Oled_Task(uint32_t event)
{
#if defined(OLED_FUNCTION)
	/* 系统消息事件 */
	if (event & EVENT_SYS_MBOX)
    {
        UiMsg_stu_t temp;
        while (OSAL_MboxAccept(&temp))
        {
			OledTask_SysMsgProcess(&temp);
        }
        return ( event ^ EVENT_SYS_MBOX );
    }

	/* 系统启动事件 */
	if (event & EVENT_SYS_START)
	{
		OLED_LOG("Oled task start\r\n");
		Oled_Open();
		Oled_Clear();
		g_refreshMenuFlag =RESET;
		OledTask_StartRefreshDisplay();
		return ( event ^ EVENT_SYS_START );
	}
	if(event &EVENT_OLED_POPUP_FINISH)
	{
		if(g_ReadPopUpMsgIndex != g_WritePopUpMsgIndex)
		{
			OLED_LOG("Popuping ReadIndex:%d,WriteIndex:%d\r\n",g_ReadPopUpMsgIndex,g_WritePopUpMsgIndex);
			OledTask_DisplayPopUp(g_PopUpMsgBuff[g_ReadPopUpMsgIndex].pop_ups0,g_PopUpMsgBuff[g_ReadPopUpMsgIndex].pop_ups1,g_PopUpMsgBuff[g_ReadPopUpMsgIndex].pop_ups2,g_PopUpMsgBuff[g_ReadPopUpMsgIndex].time); 
			g_ReadPopUpMsgIndex = (g_ReadPopUpMsgIndex == MSG_BUFF_LEN) ? 0: g_ReadPopUpMsgIndex+1;
			g_popupStatus = POPUP_RUNNING_AND_RECEIVE;
			
		}
		else
		{
			lastBoxListCurrent = 0xFFFF;
			g_popupStatus =POPUP_FINISH;
			OSAL_EventRepeatCreate(COMP_OLED, EVENT_OLED_REFRESH_MENU, (gRefreshRateChange ==SET) ? MAIN_MENU_REFRESH_TIME -150 : MAIN_MENU_REFRESH_TIME, EVT_PRIORITY_MEDIUM);
			OLED_LOG("Popup finish ReadIndex:%d,WriteIndex:%d\r\n",g_ReadPopUpMsgIndex,g_WritePopUpMsgIndex);
		}
		return ( event ^ EVENT_OLED_POPUP_FINISH );
	}

	if (event & EVENT_OLED_REFRESH_MENU)
	{
		static BoxData_stu_t *pLastBoxList =NULL;
		static void * lastBoxData =NULL;
		pLastBoxList = *g_boxList;
		//菜单内容没更新则不继续刷屏 若框类型为输入框时要持续刷屏
		if(pLastBoxList->current != lastBoxListCurrent ||lastBoxData !=pLastBoxList->data ||BOX_TYPE_INPUT == pLastBoxList->type)
		{
			if(g_popupStatus == POPUP_FINISH)
			{
				OledTask_DisplayMenuUi(g_boxList);
				lastBoxListCurrent = pLastBoxList->current;
				lastBoxData = pLastBoxList->data;
			}
			
		}
		return ( event ^ EVENT_OLED_REFRESH_MENU );
	}
	
	/* 系统休眠事件 */
	if (event & EVENT_SYS_SLEEP)
	{
		OLED_LOG("Oled task sleep\r\n");
		OledTask_ClearDisplay();
		Oled_RefreshDisplay();
		Oled_Close();
		return ( event ^ EVENT_SYS_SLEEP );
	}

	/* OLED测试事件 */
	if (event & EVENT_OLED_TEST)
	{
		OledTask_Test();
		return ( event ^ EVENT_OLED_TEST );
	}
#endif
	return 0;
}


COMPONENT_TASK_EXPORT(COMP_OLED, Oled_Task, 0);


//思路：
// 1.同一时间要显示多个弹窗时，UI任务把所有弹窗一起展示。
// 1.显示屏任务根据光标变化自动刷新BOX
// 2.菜单切换发消息给显示屏任务
// 3.弹窗：发消息给显示屏任务








