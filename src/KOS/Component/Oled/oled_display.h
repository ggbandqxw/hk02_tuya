#ifndef __OLED_DISPLAY_H__
#define __OLED_DISPLAY_H__
#include "device.h"
#include "component.h"
void Oled_DisString(uint8_t x, uint8_t y,const char*pStr, uint16_t disMode, uint8_t bold);
void Oled_DisStringInPutDisplayMemory(uint8_t x, uint8_t y,const char*pStr, uint16_t disMode, uint8_t bold);
void Oled_DisElectricity(uint8_t x, uint8_t y, uint8_t electricity);
void Oled_DisplayClear(void);
void Oled_RefreshDisplay(void);
void Oled_AllWhiteTest(void);
void Oled_Open(void);
void Oled_Close(void);
void Oled_Refresh_Gram(uint8_t *pdata);
void Oled_Clear(void);
#endif 
