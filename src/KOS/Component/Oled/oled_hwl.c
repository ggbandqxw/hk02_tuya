/**
  ******************************************************************************
  * @file         oled.c
  * @brief        oled底层驱动程序
  * @author       liujia
  * @date         2022-1-25
  * @version      v001
  * @copyright    dxtc
  * 
  * @note         凯迪仕·智能锁
  *
  ******************************************************************************
  */
#include "device.h"

#define VHW_SPI_OLED   vSPI_2
#define VHW_CS_OLED    vPIN_C22
#define VHW_RST_OLED   vPIN_C23
#define VHW_DA_OLED   vPIN_C24
#define VHW_DA_POWER   vPIN_C25

#define GPIO_OLED_DC_H()    Device_Write(VHW_DA_OLED, NULL, 0, 1)
#define GPIO_OLED_DC_L()    Device_Write(VHW_DA_OLED, NULL, 0, 0)

#define GPIO_OLED_RES_H()   Device_Write(VHW_RST_OLED, NULL, 0, 1)
#define GPIO_OLED_RES_L()   Device_Write(VHW_RST_OLED, NULL, 0, 0)

#define GPIO_OLED_CS_H()   Device_Write(VHW_CS_OLED, NULL, 0, 1)
#define GPIO_OLED_CS_L()   Device_Write(VHW_CS_OLED, NULL, 0, 0)

#define GPIO_OLED_POWER_ON()   Device_Write(VHW_DA_POWER, NULL, 0, 1)
#define GPIO_OLED_POWER_OFF()   Device_Write(VHW_DA_POWER, NULL, 0, 0)

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

//SPI
static void Oled_Spi_Init(void)
{
    Device_Enable(VHW_SPI_OLED);
}

//SPI写一个字节
static uint8_t SPI_Oled_ReadWriteByte(uint8_t txData)
{
	uint8_t rxData = 0;
	
  Device_Write(VHW_SPI_OLED, &txData, 1, (uint32_t)&rxData);
	return rxData;
}

/**
  * @brief  OLED端口初始化
  *
  * @note   OLED使用了硬件SPI2
	*         P2.2    CLK  -- CLK
	*         P2.0    DATA -- MOSI
	*         P2.4    CS
	*         P5.3    A0(DC)
	*         P2.3    RES
  */
static void Oled_PortInit(void)
{
	Oled_Spi_Init();
	GPIO_OLED_CS_H();
}


/**
  * @brief  往OLED屏里面，写入1个字节
  *
  * @note
  * @param  cmd：1写入数据  0写入命令
  * @param  dat：需要发出的数据
  */
static void Oled_WriteByte(uint8_t dat, uint8_t cmd)
{
  if (cmd)
  {
    GPIO_OLED_DC_H();
  }
  else
  {
    GPIO_OLED_DC_L();
  }
  GPIO_OLED_CS_L();
  SPI_Oled_ReadWriteByte(dat);
  GPIO_OLED_CS_H();
}

/**
  * @brief  OLED刷屏
  *
  * @note
  * @
  */
void Oled_Refresh_Gram(uint8_t *pdata)
{
  uint8_t i,j;
  uint8_t x = 2;
  uint8_t buf[3];


  for(i=0; i<=7; i++)
  {
    GPIO_OLED_DC_L();
    GPIO_OLED_CS_L();
    buf[0]=0xb0+i;
    buf[1]=(x>>4)|0x10;
    buf[2]=x&0x0f;
    // Spi2_WriteBytes(buf,3);
    SPI_Oled_ReadWriteByte(buf[0]);
    SPI_Oled_ReadWriteByte(buf[1]);
    SPI_Oled_ReadWriteByte(buf[2]);
    GPIO_OLED_CS_H();

    for(j=0;j<128;j++)
    {
      GPIO_OLED_DC_H();
      GPIO_OLED_CS_L();
      SPI_Oled_ReadWriteByte(*(pdata+i*128+j));
      GPIO_OLED_CS_H();
    }
  }
}

void Oled_Clear(void)
{
  uint8_t i,j;
  uint8_t x = 2;
  uint8_t buf[3];

  for(i=0; i<=7; i++)
  {
    GPIO_OLED_DC_L();
    GPIO_OLED_CS_L();
    buf[0]=0xb0+i;
    buf[1]=(x>>4)|0x10;
    buf[2]=x&0x0f;
    // Spi2_WriteBytes(buf,3);
    SPI_Oled_ReadWriteByte(buf[0]);
    SPI_Oled_ReadWriteByte(buf[1]);
    SPI_Oled_ReadWriteByte(buf[2]);

    GPIO_OLED_CS_H();

    for(j=0;j<128;j++)
    {
      GPIO_OLED_DC_H();
      GPIO_OLED_CS_L();
      SPI_Oled_ReadWriteByte(0);
      GPIO_OLED_CS_H();
    }
  }
}


/**
  * @brief  OLED初始化
  *
  * @note
  */
static void Oled_init(void)
{
  /*
	Oled_PortInit();  //端口初始化
	OSAL_DelayUs(20);
	GPIO_OLED_RES_L();//OLED复位
	CyDelayUs(100);
	GPIO_OLED_RES_H();
	OSAL_DelayUs(60);
*/
	Oled_PortInit();  //端口初始化
	OSAL_DelayUs(5);
	GPIO_OLED_RES_L();//OLED复位
	OSAL_DelayUs(20);
	GPIO_OLED_RES_H();
	OSAL_DelayUs(5);

	Oled_WriteByte(0xAE,OLED_CMD);//--turn off oled panel
	Oled_WriteByte(0x02,OLED_CMD);//---SET low column address
	Oled_WriteByte(0x10,OLED_CMD);//---SET high column address
	Oled_WriteByte(0x40,OLED_CMD);//--SET start line address  SET Mapping RAM Display Start Line (0x00~0x3F)
	Oled_WriteByte(0x81,OLED_CMD);//--SET contrast control register
	Oled_WriteByte(0xCF,OLED_CMD);// SET SEG Output Current Brightness

	Oled_WriteByte(0xA1,OLED_CMD);//--SET SEG/Column Mapping     0xa0左右反置 0xa1正常
	Oled_WriteByte(0xC8,OLED_CMD);//SET COM/Row Scan Direction   0xc0上下反置 0xc8正常

	Oled_WriteByte(0xA6,OLED_CMD);//--SET normal display
	Oled_WriteByte(0xA8,OLED_CMD);//--SET multiplex ratio(1 to 64)
	Oled_WriteByte(0x3f,OLED_CMD);//--1/64 duty
	Oled_WriteByte(0xD3,OLED_CMD);//-SET display offSET	Shift Mapping RAM Counter (0x00~0x3F)
	Oled_WriteByte(0x00,OLED_CMD);//-not offSET
	Oled_WriteByte(0xd5,OLED_CMD);//--SET display clock divide ratio/oscillator frequency
	Oled_WriteByte(0x80,OLED_CMD);//--SET divide ratio, SET Clock as 100 Frames/Sec
	Oled_WriteByte(0xD9,OLED_CMD);//--SET pre-charge period
	Oled_WriteByte(0xF1,OLED_CMD);//SET Pre-Charge as 15 Clocks & Discharge as 1 Clock
	Oled_WriteByte(0xDA,OLED_CMD);//--SET com pins hardware configuration
	Oled_WriteByte(0x12,OLED_CMD);
	Oled_WriteByte(0xDB,OLED_CMD);//--SET vcomh
	Oled_WriteByte(0x40,OLED_CMD);//SET VCOM Deselect Level
	Oled_WriteByte(0x20,OLED_CMD);//-SET Page Addressing Mode (0x00/0x01/0x02)
	Oled_WriteByte(0x02,OLED_CMD);//
	Oled_WriteByte(0x8D,OLED_CMD);//--SET Charge Pump enable/disable
	Oled_WriteByte(0x14,OLED_CMD);//--SET(0x10) disable
	Oled_WriteByte(0xA4,OLED_CMD);// Disable Entire Display On (0xa4/0xa5)
	Oled_WriteByte(0xA6,OLED_CMD);// Disable Inverse Display On (0xa6/a7)
	Oled_WriteByte(0xAF,OLED_CMD);//--turn on oled panel

	Oled_Clear();
	//OSAL_DelayUs(20);
	Oled_WriteByte(0xAF,OLED_CMD); /*display ON*/
}


/**
  * @brief  LED设备 open接口
  *         
  * @note   开启设备电源（唤醒）
  */
void Oled_Open(void)
{
  //todo    set gpio
  GPIO_OLED_POWER_ON();
  Oled_init(); 
}

/**
  * @brief  LED设备 close接口
  *         
  * @note   关闭设备电源（休眠）
  */
void Oled_Close(void)
{
    //关显示、关电源
  	Oled_Clear();
    Oled_WriteByte(0X8D,OLED_CMD);  //SET DCDC命令
    Oled_WriteByte(0X10,OLED_CMD);  //DCDC OFF
    Oled_WriteByte(0XAE,OLED_CMD);  //DISPLAY OFF
    OSAL_DelayUs(20);
    GPIO_OLED_POWER_OFF();//colse OLED
    //todo  set gpio
}



