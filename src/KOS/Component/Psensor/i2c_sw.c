/** 
  *****************************************************************************
  * @file    		    : i2c_sw.c
  * @author         : Department 1, R&D Center, Security SoC Division
  * @version        : V1.0.0
  * @date           : 14-June-2016
  * @test processor : STM32F405RGT
  * @test compiler  : IAR ARM 7.7
  * @brief          : Functions of I2C Hardware Dependent Part of IL005 Physical Layer
  *                   Using GPIO For Communication
  *****************************************************************************
  * Copyright (c) 2016 ICTK Co., LTD. All rights reserved.
  */

#include "i2c_sw.h"
#include "device.h"
#include "component.h"
#include "i2c_drv.h"

__EFRAM static uint8_t (**i2c_api)(uint8_t);

#define IIC_START     0
#define IIC_STOP      1
#define IIC_SEND_ACK  2
#define IIC_SEND_NAK  3
#define IIC_READ_ACK  4
#define IIC_WAKEUP    5
#define IIC_SEND_BYTE 6
#define IIC_READ_BYTE 7

#define IIC_LOG(format, ...) OSAL_LOG(C_CYAN format C_NONE, ##__VA_ARGS__)
#define __IIC_LOG(format, ...) __OSAL_LOG(C_CYAN format C_NONE, ##__VA_ARGS__)

/**
  * @brief  This function initializes and enables the I2C peripheral.
  */
void i2c_init(void)
{
    // I2c_Soft_Init();
    i2c_api = (uint8_t(**)(uint8_t))(Device_GetDeviceCtrlBlock(vIIC)->devParam);
}

/**
  * @brief  This function creates a Start condition (SDA low, then SCL low).
  * @return status of the operation
  */
IL005_StatusTypeDef i2c_start(void)
{
    if (i2c_api == NULL)
    {
        return IL005_ERROR;
    }
    i2c_api[IIC_START](0);
    return IL005_OK;
}

/**
  * @brief  This function creates a Stop condition (SCL high, then SDA high).
  */
void i2c_stop(void)
{
    if (i2c_api == NULL)
    {
        return;
    }
    i2c_api[IIC_STOP](0);
}

/**
  * @brief  Enable acknowledging data.
  */
void i2c_ack(void)
{
    if (i2c_api == NULL)
    {
        return;
    }
    i2c_api[IIC_SEND_ACK](0);
}

/**
  * @brief  Disable acknowledging data for the last byte.
  */
void i2c_noack(void)
{
    if (i2c_api == NULL)
    {
        return;
    }
    i2c_api[IIC_SEND_NAK](0);
}

/**
  * @brief  This function checks acknowledging data.
  * @return status of the operation
  */
IL005_StatusTypeDef i2c_waitack(void)
{
    if (i2c_api == NULL)
    {
        return IL005_ERROR;
    }
    return (i2c_api[IIC_READ_ACK](0)) ? IL005_I2C_NO_ACK : IL005_OK;
}

/**
  * @brief  This I2C function generates a Wake-up pulse.
  * @param  delay_time Wake-up low duration
  * @return status of the operation
  */
IL005_StatusTypeDef i2c_wakeup(uint16_t delay_time)
{
    if (i2c_api == NULL)
    {
        return IL005_ERROR;
    }
    return (i2c_api[IIC_WAKEUP](delay_time)) ? IL005_WAKEUP_FAIL : IL005_OK;
}

/**
  * @brief  This function sends one byte to an I2C device.
  * @param  sendbyte one byte to send
  */
void i2c_sendbyte(uint8_t sendbyte)
{
    if (i2c_api == NULL)
    {
        return;
    }
    i2c_api[IIC_SEND_BYTE](sendbyte);
}

/**
  * @brief  This function receives one byte from an I2C device.
  * @return received byte
  */
uint8_t i2c_receivebyte(void)
{
    if (i2c_api == NULL)
    {
        return 0;
    }
    return i2c_api[IIC_READ_BYTE](0);
}

/************************ (c) COPYRIGHT 2016 ICTK Co., LTD. *****END OF FILE*****/
