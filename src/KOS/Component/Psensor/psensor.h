#ifndef _PSENSOR_H_
#define _PSENSOR_H_

#include "component.h"

typedef enum
{
    PSENSOR_OPENED,       //检测到开门
    PSENSOR_CLOSED,       //检测到关门
    PSENSOR_CAL_FAILED,   //校准失败
    PSENSOR_CAL_FINISHED, //校准完成
} Psensor_Msg_enum_t;

/* Psensor组件消息类型 */
typedef struct
{
    Psensor_Msg_enum_t msg;
} PsensorMsg_t;

/* 传感器校准，应用层在开门前且上锁状态下调用该接口，并等待校准完成后再执行开锁操作 */
static ErrorStatus Psensor_Calibration(void);

#endif
