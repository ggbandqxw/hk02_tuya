/*******************************************************************************
* BMI055门开关状态识别算法
* 修改日期：20210527
* 应用客户：鼎新同创
* 注意：
*----------------------------
* 深圳智游者科技有限公司 邓强
* TEL：13488985445
* Email：denzil.deng@smartwalker.cn
* Web：www.smartwalker.cn
* 深圳智游者科技有限公司
* 深圳地址：深圳市龙岗区龙城街道华兴路26号天汇大厦6楼610
* 上海地址：上海市浦东新区航头镇航都路16号赫轩产业园1幢4楼411-A室
* 成都地址：成都市武侯区天府软件园D5_B30
*******************************************************************************/

#ifndef __FUSION_H__
#define __FUSION_H__

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
 
  // IL005 I/O Error Code
typedef enum 
{
  IL005_OK                          = 0x00,
  IL005_ERROR                       = 0x01,
  IL005_BUSY                        = 0x02,
  IL005_TIMEOUT                     = 0x03,
  IL005_RESYNC_WITH_WAKEUP          = 0x04,
  IL005_WAKEUP_FAIL                 = 0x05,
  IL005_I2C_START_FAIL              = 0x06,
  IL005_I2C_NO_ACK                  = 0x07,
  IL005_OWI_SEND_FAIL               = 0x08,
  IL005_OWI_RECEIVE_FAIL            = 0x09,
  IL005_COMM_FAIL                   = 0x0A,
  IL005_CRC_ERROR                   = 0x0B,
  IL005_PARAMETER_ERROR             = 0x0C,
  IL005_INVALID_SIZE                = 0x0D,
  IL005_NO_DATA                     = 0x0E,
  IL005_INVAILD_DATA                = 0x0F,
  IL005_INVAILD_COMMAND             = 0x10,
  IL005_CERT_ERROR                  = 0x11,
} IL005_StatusTypeDef;



extern void (*Drive_i2c_init)(void);
extern void (*IL005_i2c_init)(void);
extern IL005_StatusTypeDef (*IL005_i2c_start)(void);
extern void (*IL005_i2c_stop)(void);
extern void (*IL005_i2c_ack)(void);
extern void (*IL005_i2c_noack)(void);
extern IL005_StatusTypeDef (*IL005_i2c_waitack)(void);
extern IL005_StatusTypeDef (*IL005_i2c_wakeup)(uint16_t delay_time);
extern void (*IL005_i2c_sendbyte)(uint8_t sendbyte);
extern uint8_t (*IL005_i2c_receivebyte)(void); 
extern void (*IL005_delay_ms)(uint32_t nTime); //该延时函数应竟可能精确，否则可能会出现读芯片异常
extern void (*IL005_delay_us)(uint32_t nTime); //该延时函数应竟可能精确，否则可能会出现读芯片异常

/*******************************************************************************************************
/函数校准初始化
short sensordata[6]:GyrX,GyrY,GyrZ,AccX,AccY,AccZ
int magEnable:地磁数据是否更新
int setDire:设置使用的轴（0,1,2）
return :0x0000：地磁场和陀螺全部校准成功
        0x0001~0x000F：地磁校准完成，陀螺正在校准
        0x0011~0x001F：地磁和陀螺均没有校准完成
        0x0010：地磁正在校准，陀螺校准完成
				0x01xx：加密芯片秘钥异常
				0x02xx：加密芯片驱动异常
********************************************************************************************************/
short fusion_init(short sensordata[9],int magEnable,int setDire);


/*******************************************************************************************************
short sensordata[6]:GyrX,GyrY,GyrZ,AccX,AccY,AccZ,Magx,MagY,MagZ
int magEnable:当前磁场计数据是否有效1有效，0无效
short TimeCount：调用间隔时间，单位ms
float angles[3]：当前的角度，单位0.001度
float AngleValve:角度阀值
float magValve:磁场阀值
short Mode:当前模式，0全功率模式，1低功耗模式，2纯地磁模式
return： 0当前关门状态
         1当前开门状态
********************************************************************************************************/
short fuison_func(short sensordata[9],int magEnable, short deltTime, float angles[3],float AngleValve,float magValve,short Mode) ;


/*******************************************************************************************************
算法复位
********************************************************************************************************/
void resetFusion(void);


/*******************************************************************************************************
角度清零
********************************************************************************************************/
void fuison_claerAngle(void);

 
/***************************************************************************
读取当前lib版本信息
***************************************************************************/
short fusion_ReadLibVersions(void);
	
	
/***************************************************************************
需要保存的数据长度读取
***************************************************************************/
unsigned short fuison_mag_door_ReadLen(void);
	
	
/***************************************************************************
需要保存的数据读取
***************************************************************************/
void fuison_mag_door_ReadStaticData(unsigned char *staticdata);
	
	
/***************************************************************************
需要保存的数据写入	
***************************************************************************/
void fuison_mag_door_WriteStaticData(unsigned char *staticdata);
	
	
/***************************************************************************
根据磁场计判断是否运动
***************************************************************************/
short fuison_mag_stateOfMotion(short mag_value[3]);

#endif
