#ifndef __ZYM568_H
#define __ZYM568_H

unsigned char ZYM568_Get_Data(short mag[3], short dtim);
unsigned char ZYM568_init(void);
unsigned char ZYM568_lowpowermode(void);

#endif
