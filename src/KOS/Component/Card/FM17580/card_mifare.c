/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file Mifare.c
 *
 * Project: Mifare reader with RC522
 *
 * Workfile: Mifare.c
 * $Author: Bob Jiang $
 * $Revision: 1.0 $
 * $Date: Wed Aug 17 2005 $
 *
 * Comment:
 *  All the ISO14443-3 protocol and mifare command set are all implemented here.
 *  All the founctions in this file is totally independent of hardware.
 *  The source can be ported to other platforms very easily.
 *
 *  The interrupt pin of the reader IC is not conntected and no interrupt needed.
 *  All protocol relevant timing constraints are generated
 *  by the internal timer of the reader module.
 *
 *  Therefore the function M522PcdCmd is very important for understanding
 *  of the communication between reader and card.
 *
 *
 * History:
 *
 *
*/

#include <string.h>
#include "card_mifare.h"
#include "card_nxp520.h"

#define FeedIWDG() Device_FeedDog()
typedef struct
{
    unsigned char cmd;            //!< command code
    char status;                  //!< communication status
    unsigned char nBytesSent;     //!< how many bytes already sent
    unsigned char nBytesToSend;   //!< how many bytes to send
    unsigned char nBytesReceived; //!< how many bytes received
    unsigned short nBitsReceived; //!< how many bits received
    unsigned char collPos;        //collision position
} MfCmdInfo;
/*
#define ResetInfo(info)\                                    // ?
info.cmd = 0;\
info.status = STATUS_SUCCESS;\
info.nBytesSent = 0;\
info.nBytesToSend  = 0;\
info.nBytesReceived = 0;\
info.nBitsReceived  = 0;\
info.collPos = 0;

    */
#define ResetInfo(info)           \
    info.cmd = 0;                 \
    info.status = STATUS_SUCCESS; \
    info.nBytesSent = 0;          \
    info.nBytesToSend = 0;        \
    info.nBytesReceived = 0;      \
    info.nBitsReceived = 0;       \
    info.collPos = 0;

volatile MfCmdInfo MInfo;
volatile MfCmdInfo *MpIsrInfo = 0;
static unsigned char SerBuffer[56];

//void putchar(unsigned char);

//unsigned char n,a=0x01,b=0x02,c=0x03;

/////////////////////////////////////////////////////////////////////
//功    能：写RC632寄存器
//参数说明：Address[IN]:寄存器地址
//          value[IN]:写入的值
/////////////////////////////////////////////////////////////////////
static void RcSetReg(unsigned char Address, unsigned char value)
{
    Nxp520_Write_Reg(Address, value);
}

/////////////////////////////////////////////////////////////////////
//功    能：读RC632寄存器
//参数说明：Address[IN]:寄存器地址
//返    回：读出的值
/////////////////////////////////////////////////////////////////////
static unsigned char RcGetReg(unsigned char Address)
{
    return Nxp520_Read_Reg(Address);
}

/*************************************************
Function:       RcModifyReg
Description:
     Change some bits of the register
Parameter:
     RegAddr       The address of the regitster
     ModifyVal        The value to change to, set or clr?
     MaskByte      Only the corresponding bit '1' is valid,
Return:
     None
**************************************************/
static void RcModifyReg(unsigned char RegAddr, unsigned char ModifyVal, unsigned char MaskByte)
{
    unsigned char RegVal;
    RegVal = RcGetReg(RegAddr);
    if (ModifyVal)
    {
        RegVal |= MaskByte;
    }
    else
    {
        RegVal &= (~MaskByte);
    }
    RcSetReg(RegAddr, RegVal);
}

/*************************************************
Function:       SetPowerDown
Description:
     set the rc522 enter or exit power down mode
Parameter:
     ucFlag     0   --  exit power down mode
                !0  --  enter power down mode
Return:
     short      status of implement
**************************************************/
void SetPowerDown(unsigned char ucFlag)
{
    unsigned char RegVal;

    /* Note: The bit Power Down can not be set when the SoftReset command has been activated. */
    if (ucFlag)
    {
        RegVal = RcGetReg(JREG_COMMAND); //enter power down mode
        RegVal |= 0x10;
        RcSetReg(JREG_COMMAND, RegVal);
    }
    else
    {
        RegVal = RcGetReg(JREG_COMMAND); //disable power down mode
        RegVal &= (~0x10);
        RcSetReg(JREG_COMMAND, RegVal);
    }
}

/*************************************************
Function:       SetTimeOut
Description:
     Adjusts the timeout in 100us steps
Parameter:
     uiMicroSeconds   the time to set the timer(100us as a step)
Return:
     short      status of implement
**************************************************/
short SetTimeOut(unsigned int uiMicroSeconds)
{
    unsigned int RegVal;
    unsigned char TmpVal;
    RegVal = uiMicroSeconds / 100;
    /* NOTE: The supported hardware range is bigger, since the prescaler here
       is always set to 100 us. */
    if (RegVal >= 0xfff)
    {
        return STATUS_INVALID_PARAMETER;
    }
    RcModifyReg(JREG_TMODE, 1, JBIT_TAUTO); //发送后启动定时器

    RcSetReg(JREG_TPRESCALER, 0xa6); //

    TmpVal = RcGetReg(JREG_TMODE);
    TmpVal &= 0xf0;
    TmpVal |= 0x02;
    RcSetReg(JREG_TMODE, TmpVal); //一共是678

    RcSetReg(JREG_TRELOADLO, ((unsigned char)(RegVal & 0xff)));
    RcSetReg(JREG_TRELOADHI, ((unsigned char)((RegVal >> 8) & 0xff))); //注意时间的误差
    return STATUS_SUCCESS;
}

/*************************************************
Function:       ChangeRc522BaudRate
Description:
     Changes the serial speed of the RC522.
     Note that the speed of the host interface (UART on PC) has to be also set to the
     appropriate one.
Parameter:
     baudrate   new baudrate for rc522
Return:
     short      status of implement
**************************************************/
short ChangeRc522BaudRate(unsigned long baudrate)
{
    short status = STATUS_SUCCESS;
    unsigned char setRegVal;
    switch (baudrate)
    {
    case 9600:
        setRegVal = 0xEB;
        break;

    case 14400:
        setRegVal = 0xDA;
        break;

    case 19200:
        setRegVal = 0xCB;
        break;

    case 38400:
        setRegVal = 0xAB;
        break;

    case 57600:
        setRegVal = 0x9A;
        break;

    case 115200:
        setRegVal = 0x7A;
        break;

    case 128000:
        setRegVal = 0x74;
        break;

    default:
        status = STATUS_INVALID_PARAMETER;
        break;
    }

    /* Set the appropriate value */
    if (status == STATUS_SUCCESS)
        RcSetReg(JREG_SERIALSPEED, setRegVal);
    /* Now the RC522 is set to the new speed*/
    return status;
}

/*************************************************
Function:       Rc522Init
Description:
     initialize rc522 as a mifare reader
Parameter:
     NONE
Return:
     short      status of implement
**************************************************/
void Rc522Init(void)
{
    //    unsigned char RegVal;
    //
    //    RcSetReg(JREG_COMMAND, 0x0f); /*reset the RC522*/
    //
    //    /* disable Crypto1 bit*/
    //    RcModifyReg(JREG_STATUS2, 0, JBIT_CRYPTO1ON);
    //
    //    /* do not touch bits: InvMod in register TxMode */
    //    RegVal = RcGetReg(JREG_TXMODE);
    //    RegVal = (unsigned char)(RegVal & JBIT_INVMOD);
    //    RegVal = (unsigned char)(RegVal | JBIT_CRCEN | (RCO_VAL_RF106K << RC522_SPEED_SHL_VALUE));
    //    /* TxCRCEn = 1; TxSpeed = x; InvMod, TXMix = 0; TxFraming = 0 */
    //    RcSetReg(JREG_TXMODE, RegVal);
    //
    //    /* do not touch bits: RxNoErr in register RxMode */
    //    RegVal = RcGetReg(JREG_RXMODE);
    //    RegVal = (unsigned char)(RegVal & JBIT_RXNOERR);
    //    RegVal = (unsigned char)(RegVal | JBIT_CRCEN | (RCO_VAL_RF106K << RC522_SPEED_SHL_VALUE));
    //    /* RxCRCEn = 1; RxSpeed = x; RxNoErr, RxMultiple = 0; TxFraming = 0 */
    //    RcSetReg(JREG_RXMODE, RegVal);
    //
    //    // /* ADDIQ = 10b; FixIQ = 1; RFU = 0; TauRcv = 11b; TauSync = 01b  */
    //    // RcSetReg(JREG_DEMOD, 0x4D);	     //通道选择 默认0x4D
    //
    //    /* RxGain = 4*/
    //    RcSetReg(JREG_RFCFG, 0x48);//多余？ 默认0x48
    //
    //    /* do settings common for all functions */
    //    RcSetReg(JREG_RXTRESHOLD, 0x84);    /* MinLevel = 5; CollLevel = 5 */ //位解码器阀值的选择默认0x84
    //
    //    RcSetReg(JREG_GSN, 0x80 | 0x08);     /* CWGsN = 0xF; ModGsN = 0x4 */  //电导率 默认0x88
    //
    //    // RcSetReg(JREG_MODWIDTH, 0x26);      /* Modwidth = 0x26 */  //载波调制宽度 默认0x26
    //
    //
    //    /* 节约功耗，此处先不开天线 */
    //
    //    // /* Set the timer to auto mode, 5ms using operation control commands before HF is switched on to
    //    //  * guarantee Iso14443-3 compliance of Polling procedure
    //	//  */
    //    // SetTimeOut(5000);
    //
    //    // /* Activate the field  */
    //    // RcModifyReg(JREG_TXCONTROL, 1, JBIT_TX2RFEN | JBIT_TX1RFEN);
    //
    //    // /* start timer manually to check the initial waiting time */
    //    // RcModifyReg(JREG_CONTROL, 1, JBIT_TSTARTNOW);
    //
    //    // /*
    //	//  * After switching on the timer wait until the timer interrupt occures, so that
    //    //  * the field is on and the 5ms delay have been passed.
    //	//  */
    //    // do {
    //    //     RegVal = RcGetReg(JREG_COMMIRQ);
    //    // }
    //    // while(!(RegVal & JBIT_TIMERI));
    //
    //
    //    // /* Clear the status flag afterwards */
    //    // RcSetReg(JREG_COMMIRQ, JBIT_TIMERI);
    //
    //    /*
    //	 * Reset timer 1 ms using operation control commands (AutoMode and Prescaler are the same)
    //	 * set reload value
    //	 */
    //    SetTimeOut(5000);
    //
    //    RcSetReg(JREG_WATERLEVEL, 0x07);	//默认值0x08
    //    // RcSetReg(JREG_TXSEL, 0x10);		   // 默认值0x10
    //    // RcSetReg(JREG_RXSEL, 0x84);		   // 默认值0x84
    //
    //	RcSetReg(JREG_MODE , 0x39);         //设置crc初值
    //	RcSetReg(JREG_TXASK , 0x40);        //强制100%ASK调制
    //	// RcSetReg(JREG_CONTROL , 0x10);      //PN512的设置
    //
    //    /* Activate receiver for communication
    //       The RcvOff bit and the PowerDown bit are cleared, the command is not changed. */
    //    RcSetReg(JREG_COMMAND, JCMD_IDLE);
    //
    //	/* Set timeout for REQA, ANTICOLL, SELECT to 200us */
    //	SetTimeOut(2000);
}

/*************************************************
Function:       M522PcdCmd
Description:
     implement a command
Parameter:
     cmd            command code
     ExchangeBuf    saved the data will be send to card and the data responed from the card
     info           some information for the command
Return:
     short      status of implement
**************************************************/
short M522PcdCmd(unsigned char cmd,
                 unsigned char *ExchangeBuf,
                 MfCmdInfo *info)
{
    short status = STATUS_SUCCESS;
    short istatus = STATUS_SUCCESS;

    unsigned char commIrqEn = 0;
    unsigned char divIrqEn = 0;
    unsigned char waitForComm = JBIT_ERRI | JBIT_TXI;
    unsigned char waitForDiv = 0;
    unsigned char doReceive = 0;
    unsigned char i;
    unsigned char getRegVal, setRegVal;
    unsigned int counter;

    FeedIWDG();

    /*remove all Interrupt request flags that are used during function,
    keep all other like they are*/
    RcSetReg(JREG_COMMIRQ, waitForComm);
    RcSetReg(JREG_DIVIRQ, waitForDiv);
    RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);

    /*disable command or set to transceive*/
    getRegVal = RcGetReg(JREG_COMMAND);
    if (cmd == JCMD_TRANSCEIVE)
    {
        /*re-init the transceive command*/
        setRegVal = (getRegVal & ~JMASK_COMMAND) | JCMD_TRANSCEIVE;
        RcSetReg(JREG_COMMAND, setRegVal); //0c
    }
    else
    {
        /*clear current command*/
        setRegVal = (getRegVal & ~JMASK_COMMAND);
        RcSetReg(JREG_COMMAND, setRegVal);
    }
    MpIsrInfo = info;
    switch (cmd)
    {
    case JCMD_IDLE: /* values are 00, so return immediately after all bytes written to FIFO */
        waitForComm = 0;
        waitForDiv = 0;
        break;
    case JCMD_CALCCRC: /* values are 00, so return immediately after all bytes written to FIFO */
        waitForComm = 0;
        waitForDiv = 0;
        break;
    case JCMD_TRANSMIT:
        commIrqEn = JBIT_TXI | JBIT_TIMERI;
        waitForComm = JBIT_TXI;
        break;
    case JCMD_RECEIVE:
        commIrqEn = JBIT_RXI | JBIT_TIMERI | JBIT_ERRI;
        waitForComm = JBIT_RXI | JBIT_TIMERI | JBIT_ERRI;
        doReceive = 1;
        break;
    case JCMD_TRANSCEIVE:
        commIrqEn = JBIT_RXI | JBIT_TIMERI | JBIT_ERRI;
        waitForComm = JBIT_RXI | JBIT_TIMERI | JBIT_ERRI;
        doReceive = 1;
        break;
    case JCMD_AUTHENT:
        commIrqEn = JBIT_IDLEI | JBIT_TIMERI | JBIT_ERRI;
        waitForComm = JBIT_IDLEI | JBIT_TIMERI | JBIT_ERRI;
        break;
    case JCMD_SOFTRESET: /* values are 0x00 for IrqEn and for waitFor, nothing to do */
        waitForComm = 0;
        waitForDiv = 0;
        break;
    default:
        status = STATUS_UNSUPPORTED_COMMAND;
    }
    if (status == STATUS_SUCCESS)
    {
        /* activate necessary communication Irq's */
        getRegVal = RcGetReg(JREG_COMMIEN);
        RcSetReg(JREG_COMMIEN, getRegVal | commIrqEn);

        /* activate necessary other Irq's */
        getRegVal = RcGetReg(JREG_DIVIEN);
        RcSetReg(JREG_DIVIEN, getRegVal | divIrqEn);

        /*write data to FIFO*/
        //        if(MpIsrInfo->nBytesToSend > 2)log_putchar("send:");
        for (i = 0; i < MpIsrInfo->nBytesToSend; i++)
        {
            RcSetReg(JREG_FIFODATA, ExchangeBuf[i]);
            //            if(MpIsrInfo->nBytesToSend > 2)
            //            {
            //                log_putchar("%#x ", ExchangeBuf[i]);
            //            }
        }
        //         if(MpIsrInfo->nBytesToSend > 2)log_putchar("\r\n");
        /*do seperate action if command to be executed is transceive*/
        if (cmd == JCMD_TRANSCEIVE)
        {
            /*TRx is always an endless loop, Initiator and Target must set STARTSEND.*/

            // n=RcGetReg(JREG_BITFRAMING);
            // putchar(n);
            RcModifyReg(JREG_BITFRAMING, 1, JBIT_STARTSEND);

            // RcSetReg(JREG_BITFRAMING,0x87);
            //	cf=0;
            //	 n=RcGetReg(JREG_COMMAND);
            //	putchar(n);
            //	 DelayUs(30);
        }
        else
        {
            getRegVal = RcGetReg(JREG_COMMAND);
            RcSetReg(JREG_COMMAND, (getRegVal & ~JMASK_COMMAND) | cmd);
        }

        /*polling mode*/
        getRegVal = 0;
        setRegVal = 0;
        counter = 0; /*Just for debug*/
                     //        start_count();
        while (!(waitForComm ? (waitForComm & setRegVal) : 1) ||
               !(waitForDiv ? (waitForDiv & getRegVal) : 1))
        {
            FeedIWDG();
            setRegVal = RcGetReg(JREG_COMMIRQ);
            getRegVal = RcGetReg(JREG_DIVIRQ);
            counter++;
            // // if(counter > 0x0100)
            if (counter > 0x3000)
                break;
        }
        //        if(MpIsrInfo->nBytesToSend > 2) log_putchar("wait cnt(%d) ", get_runtime());

        /*store IRQ bits for clearance afterwards*/
        waitForComm = (unsigned char)(waitForComm & setRegVal);
        waitForDiv = (unsigned char)(waitForDiv & getRegVal);

        /*set status to Timer Interrupt occurence*/
        if (setRegVal & JBIT_TIMERI)
        {
            //            if(MpIsrInfo->nBytesToSend > 2) log_printf("time out\r\n");
            istatus = STATUS_IO_TIMEOUT;
        }
    }

    //		n=RcGetReg(JREG_COMMIRQ);
    //	putchar(n);

    //		 n=RcGetReg(JREG_ERROR);
    //	putchar(n);

    //		 n=RcGetReg(JREG_STATUS1);
    //	putchar(n);

    //	 n=RcGetReg(JREG_FIFOLEVEL);
    //	putchar(n);

    //	 n=RcGetReg(JREG_WATERLEVEL);
    //	putchar(n);

    /*disable all interrupt sources*/
    RcModifyReg(JREG_COMMIEN, 0, commIrqEn);

    RcModifyReg(JREG_DIVIEN, 0, divIrqEn);

    if (doReceive && (istatus == STATUS_SUCCESS))
    {
        /*read number of bytes received (used for error check and correct transaction*/
        MpIsrInfo->nBytesReceived = RcGetReg(JREG_FIFOLEVEL);
        // nbytes = MpIsrInfo->nBytesReceived;
        getRegVal = RcGetReg(JREG_CONTROL);
        MpIsrInfo->nBitsReceived = (unsigned char)(getRegVal & 0x07);
        // nbits = MpIsrInfo->nBitsReceived;
        // putchar(MpIsrInfo->nBytesReceived);

        getRegVal = RcGetReg(JREG_ERROR);
        /*set status information if error occured*/
        if (getRegVal)
        {
            if (getRegVal & JBIT_COLLERR)
                istatus = STATUS_COLLISION_ERROR; /* Collision Error */
            else if (getRegVal & JBIT_PARITYERR)
                istatus = STATUS_PARITY_ERROR; /* Parity Error */

            if (getRegVal & JBIT_PROTERR)
                istatus = STATUS_PROTOCOL_ERROR; /* Protocoll Error */
            else if (getRegVal & JBIT_BUFFEROVFL)
                istatus = STATUS_BUFFER_OVERFLOW; /* BufferOverflow Error */
            else if (getRegVal & JBIT_CRCERR)
            { /* CRC Error */
                if (MpIsrInfo->nBytesReceived == 0x01 &&
                    (MpIsrInfo->nBitsReceived == 0x04 ||
                     MpIsrInfo->nBitsReceived == 0x00))
                { /* CRC Error and only one byte received might be a Mifare (N)ACK */
                    ExchangeBuf[0] = RcGetReg(JREG_FIFODATA);
                    MpIsrInfo->nBytesReceived = 1;
                    istatus = STATUS_ACK_SUPPOSED; /* (N)ACK supposed */
                }
                else

                    //putchar(MpIsrInfo->nBytesReceived);
                    //    putchar(MpIsrInfo->nBitsReceived);

                    istatus = STATUS_CRC_ERROR; /* CRC Error */
            }
            else if (getRegVal & JBIT_TEMPERR)
                istatus = STATUS_RC522_TEMP_ERROR; /* Temperature Error */
            if (getRegVal & JBIT_WRERR)
                istatus = STATUS_FIFO_WRITE_ERROR; /* Error Writing to FIFO */
            if (istatus == STATUS_SUCCESS)
                istatus = STATUS_ERROR_NY_IMPLEMENTED; /* Error not yet implemented, shall never occur! */

            /* if an error occured, clear error register before IRQ register */
            RcSetReg(JREG_ERROR, 0);
        }

        /*read data from FIFO and set response parameter*/
        if (istatus != STATUS_ACK_SUPPOSED)
        {
            for (i = 0; i < MpIsrInfo->nBytesReceived; i++)
            {
                ExchangeBuf[i] = RcGetReg(JREG_FIFODATA);
            }
            /*in case of incomplete last byte reduce number of complete bytes by 1*/
            if (MpIsrInfo->nBitsReceived && MpIsrInfo->nBytesReceived)
                MpIsrInfo->nBytesReceived--;
        }
    }
    RcSetReg(JREG_COMMIRQ, waitForComm);
    RcSetReg(JREG_DIVIRQ, waitForDiv);
    RcSetReg(JREG_FIFOLEVEL, JBIT_FLUSHBUFFER);
    RcSetReg(JREG_COMMIRQ, JBIT_TIMERI);
    RcSetReg(JREG_BITFRAMING, 0);
    return istatus;
}

/*************************************************
Function:       Request
Description:
     REQA, request to see if have a ISO14443A card in the field
Parameter:
     req_code   command code(ISO14443_3_REQALL or ISO14443_3_REQIDL)
     atq        the buffer to save the answer to request from the card
Return:
     short      status of implement
**************************************************/
short Request(unsigned char req_code, unsigned char *atq)
{
    short status = STATUS_SUCCESS;
    /************* initialize *****************/
    Nxp520_Antenna_On(); /* 此处开天线 */

    /* 有些必须加延时 */
    OSAL_DelayUs(900);
    OSAL_DelayUs(900);
    OSAL_DelayUs(900);

    RcModifyReg(JREG_STATUS2, 0, JBIT_CRYPTO1ON); /* disable Crypto if activated before */
    RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL);    //active values after coll
    RcModifyReg(JREG_TXMODE, 0, JBIT_CRCEN);      //disable TxCRC and RxCRC
    RcModifyReg(JREG_RXMODE, 0, JBIT_CRCEN);
    RcSetReg(JREG_BITFRAMING, REQUEST_BITS);

    /* set necessary parameters for transmission */
    ResetInfo(MInfo);
    SerBuffer[0] = req_code;
    MInfo.nBytesToSend = 1;

    /* Set timeout for REQA, ANTICOLL, SELECT*/
    SetTimeOut(300);

    status = M522PcdCmd(JCMD_TRANSCEIVE,
                        SerBuffer,
                        (MfCmdInfo *)&MInfo);

    if (status) // error occured
    {
        *atq = 0;
    }
    else
    {
        if (MInfo.nBytesReceived != 2) // 2 bytes expected
        {
            *atq = 0;
            status = STATUS_BITCOUNT_ERROR;
        }
        else
        {
            status = STATUS_SUCCESS;
            memcpy(atq, SerBuffer, 2);
        }
    }

    return status;
}

/*************************************************
Function:       CascAnticoll
Description:
     Functions to split anticollission and select internally.
     NOTE: this founction is used internal only, and cannot call by application program
Parameter:
     sel_code   command code
     bitcount   the bit counter of known UID
     snr        the UID have known
Return:
     short      status of implement
**************************************************/
short CascAnticoll(unsigned char sel_code,
                   unsigned char bitcount,
                   unsigned char *snr)
{
    short istatus = STATUS_SUCCESS;

    unsigned char i;
    unsigned char complete = 0; /* signs end of anticollission loop */
    unsigned char rbits = 0;    /* number of total received bits */
    unsigned char nbits = 0;    /* */
    unsigned char nbytes = 0;   /* */
    unsigned char byteOffset;   /* stores offset for ID copy if uncomplete last byte was sent */

    /* initialise relvant bytes in internal buffer */
    for (i = 2; i < 7; i++)
        SerBuffer[i] = 0x00;

    /* disable TxCRC and RxCRC */
    RcModifyReg(JREG_TXMODE, 0, JBIT_CRCEN);
    RcModifyReg(JREG_RXMODE, 0, JBIT_CRCEN);

    /* activate deletion of bits after coll */
    RcSetReg(JREG_COLL, 0);

    /* init parameters for anticollision */
    while (!complete && (istatus == STATUS_SUCCESS))
    {
        /* if there is a communication problem on the RF interface, bcnt
            could be larger than 32 - folowing loops will be defective. */
        if (bitcount > SINGLE_UID_LENGTH)
        {
            istatus = STATUS_INVALID_PARAMETER;
            continue;
        }

        /* prepare data length */
        nbits = (unsigned char)(bitcount % BITS_PER_BYTE);
        nbytes = (unsigned char)(bitcount / BITS_PER_BYTE);
        if (nbits)
            nbytes++;

        /* prepare data buffer */
        SerBuffer[0] = sel_code;
        SerBuffer[1] = (unsigned char)(NVB_MIN_PARAMETER + ((bitcount / BITS_PER_BYTE) << UPPER_NIBBLE_SHIFT) + nbits); //NVB特点决定
        for (i = 0; i < nbytes; i++)
            SerBuffer[2 + i] = snr[i]; /* copy serial number to tranmit buffer */

        /* set TxLastBits and RxAlign to number of bits sent */ //
        RcSetReg(JREG_BITFRAMING, (unsigned char)((nbits << UPPER_NIBBLE_SHIFT) | nbits));

        /* prepare data for common transceive */
        ResetInfo(MInfo);
        MInfo.nBytesToSend = (unsigned char)(nbytes + 2);

        SetTimeOut(300);
        M522PcdCmd(JCMD_TRANSCEIVE, SerBuffer, (MfCmdInfo *)&MInfo);

        if (istatus == STATUS_COLLISION_ERROR || istatus == STATUS_SUCCESS)
        {
            /* store number of received data bits and bytes internaly */
            rbits = (unsigned char)(MInfo.nBitsReceived + (MInfo.nBytesReceived << 3) - nbits);

            if ((rbits + bitcount) > COMPLETE_UID_BITS) //40
            {
                istatus = STATUS_BITCOUNT_ERROR;
                continue;
            }

            /* increment number of bytes received if also some bits received */
            if (MInfo.nBitsReceived)
                MInfo.nBytesReceived++;

            /* reset offset for data copying */
            byteOffset = 0;
            /* if number of bits sent are not 0, write first received byte in last of sent */
            if (nbits)
            { /* last byte transmitted and first byte received are the same */
                snr[nbytes - 1] |= SerBuffer[0];
                byteOffset++;
            }

            for (i = 0; i < (4 - nbytes); i++)
                snr[nbytes + i] = SerBuffer[i + byteOffset];

            if (istatus == STATUS_COLLISION_ERROR)
            {
                /* calculate new bitcount value */
                bitcount = (unsigned char)(bitcount + rbits);
                istatus = STATUS_SUCCESS;
            }
            else
            {
                if ((snr[0] ^ snr[1] ^ snr[2] ^ snr[3]) != SerBuffer[i + byteOffset])
                {
                    istatus = STATUS_WRONG_UID_CHECKBYTE;
                    continue;
                }
                complete = 1;
            }
        }
    }

    /* clear RxAlign and TxLastbits */
    RcSetReg(JREG_BITFRAMING, 0);

    /* activate values after coll */
    RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL);
    return istatus;
}

/*************************************************
Function:       Select
Description:
     selecte a card to response the following command
     NOTE: this founction is used internal only, and cannot call by application program
Parameter:
     sel_code   command code
     snr        buffer to store the card UID
     sak        the byte to save the ACK from card
Return:
     short      status of implement
**************************************************/
short Select(unsigned char sel_code, unsigned char *snr, unsigned char *sak)
{
    short status = STATUS_SUCCESS;
    /* define local variables */
    unsigned char i;
    /* activate CRC */
    RcModifyReg(JREG_TXMODE, 1, JBIT_CRCEN);
    RcModifyReg(JREG_RXMODE, 1, JBIT_CRCEN);

    /* prepare data stream */
    SerBuffer[0] = sel_code;          /* command code */
    SerBuffer[1] = NVB_MAX_PARAMETER; /* parameter */
    for (i = 0; i < 4; i++)
        SerBuffer[2 + i] = snr[i];                                     /* serial numbner bytes 1 to 4 */
    SerBuffer[6] = (unsigned char)(snr[0] ^ snr[1] ^ snr[2] ^ snr[3]); /* serial number check byte */

    /* prepare data for common transceive */
    ResetInfo(MInfo);
    MInfo.nBytesToSend = 0x07;
    SetTimeOut(300);
    status = M522PcdCmd(JCMD_TRANSCEIVE, SerBuffer, (MfCmdInfo *)&MInfo);

    if (status == STATUS_SUCCESS)
    {
        if (MInfo.nBytesReceived == SAK_LENGTH && MInfo.nBitsReceived == 0)
            *sak = SerBuffer[0];
        else
            status = STATUS_BITCOUNT_ERROR;
    }
    return status;
}

/*************************************************
Function:       AnticollSelect
Description:
     Anticollision loop and selecte a card to operate
Parameter:
     bcnt       bit counter of known UID
     snr        buffer to store the card UID
Return:
     short      status of implement
**************************************************/
short AnticollSelect(unsigned char *pId, unsigned char *pLen, unsigned char *pSak)
{
    unsigned char i;
    short status = STATUS_SUCCESS;
    unsigned char length, casc_code, length_in, sak, tmpSnr[12];
    length_in = 0;
    /* do loop for max. cascade level */
    for (i = 0; i < MAX_CASCADE_LEVELS; i++) //MAX_CASCADE_LEVELS=3
    {
        if (length_in)
        {
            if (length_in > SINGLE_UID_LENGTH) //SINGLE_UID_LENGTH=0x20
            {
                length = SINGLE_UID_LENGTH;
                length_in -= SINGLE_UID_LENGTH;
            }
            else
            {
                length = length_in;
                length_in = 0;
            }
        }
        else
        {
            length = 0;
        }

        switch (i)
        {
        case 1:
            casc_code = SELECT_CASCADE_LEVEL_2;
            memcpy(pId, tmpSnr + 1, 3);
            break;
        case 2:
            casc_code = SELECT_CASCADE_LEVEL_3;
            memcpy(pId + 3, tmpSnr + 4, 3);
            break;
        default:
            casc_code = SELECT_CASCADE_LEVEL_1;
            break;
        }

        if (length != SINGLE_UID_LENGTH && status == STATUS_SUCCESS)
            /* do anticollission with selected level */
            status = CascAnticoll(casc_code,
                                  length,
                                  tmpSnr + i * 4);

        if (status == STATUS_SUCCESS)
        {
            /* select 1st cascade level uid */
            status = Select(casc_code, tmpSnr + i * 4, &sak);

            /* check if further cascade level is used */
            if (status == STATUS_SUCCESS)
            {
                /* increase number of received bits in parameter */
                length_in = (unsigned char)(SINGLE_UID_LENGTH * (i + 1)); //the actually length of the UID, you can return it.

                /* check if cascade bit is set */
                if (!(sak & CASCADE_BIT))
                {
                    break;
                }
            }
        }
        else
        {
            break;
        }
    }
    switch (i)
    {
    case 1:
        memcpy(pId + 3, tmpSnr + 4, 4); //copy UID to snr buffer without CT(0x88)
        *pLen = 7;
        break;
    case 2:
        memcpy(pId + 6, tmpSnr + 4, 4);
        *pLen = 10;
        break;
    default:
        memcpy(pId, tmpSnr, 4);
        *pLen = 4;
        break;
    }

    *pSak = sak; //返回SAK
    return status;
}
short AnticollSelect1(unsigned char bcnt, unsigned char *snr, unsigned char *SAK, unsigned char *num_s)
{
    unsigned char i;
    short status = STATUS_SUCCESS;
    unsigned char length, casc_code, length_in, sak, tmpSnr[12];
    length_in = bcnt;
    /* do loop for max. cascade level */
    for (i = 0; i < MAX_CASCADE_LEVELS; i++) //MAX_CASCADE_LEVELS=3
    {
        if (length_in)
        {
            if (length_in > SINGLE_UID_LENGTH) //SINGLE_UID_LENGTH=0x20
            {
                length = SINGLE_UID_LENGTH;
                length_in -= SINGLE_UID_LENGTH;
            }
            else
            {
                length = length_in;
                length_in = 0;
            }
        }
        else
        {
            length = 0;
        }

        switch (i)
        {
        case 1:
            casc_code = SELECT_CASCADE_LEVEL_2;
            memcpy(snr, tmpSnr + 1, 3);
            break;
        case 2:
            casc_code = SELECT_CASCADE_LEVEL_3;
            memcpy(snr + 3, tmpSnr + 4, 3);
            break;
        default:
            casc_code = SELECT_CASCADE_LEVEL_1;
            break;
        }

        if (length != SINGLE_UID_LENGTH && status == STATUS_SUCCESS)
            /* do anticollission with selected level */
            status = CascAnticoll(casc_code,
                                  length,
                                  tmpSnr + i * 4);

        if (status == STATUS_SUCCESS)
        {
            /* select 1st cascade level uid */
            status = Select(casc_code, tmpSnr + i * 4, &sak);

            /* check if further cascade level is used */
            if (status == STATUS_SUCCESS)
            {
                /* increase number of received bits in parameter */
                bcnt = (unsigned char)(SINGLE_UID_LENGTH * (i + 1)); //the actually length of the UID, you can return it.

                /* check if cascade bit is set */
                if (!(sak & CASCADE_BIT))
                {
                    break;
                }
            }
        }
        else
        {
            break;
        }
    }
    switch (i)
    {
    case 1:
        memcpy(snr + 3, tmpSnr + 4, 4); //copy UID to snr buffer without CT(0x88)
        *num_s = 7;
        break;
    case 2:
        memcpy(snr + 6, tmpSnr + 4, 4);
        *num_s = 10;
        break;
    default:
        memcpy(snr, tmpSnr, 4);
        *num_s = 4;
        break;
    }

    *SAK = sak; //返回SAK
    return status;
}

/*************************************************
Function:       HaltA
Description:
     halt the current selected card
Parameter:
     NONE
Return:
     short      status of implement
**************************************************/
short HaltA(void)
{
    short status = STATUS_SUCCESS;
    /* initialise data buffer */
    SerBuffer[0] = HALTA_CMD;
    SerBuffer[1] = HALTA_PARAM;

    ResetInfo(MInfo);
    MInfo.nBytesToSend = HALTA_CMD_LENGTH;
    SetTimeOut(200);
    status = M522PcdCmd(JCMD_TRANSCEIVE, SerBuffer, (MfCmdInfo *)&MInfo);

    if (status == STATUS_IO_TIMEOUT)
        status = STATUS_SUCCESS;
    return status;
}

/*************************************************
Function:      RATS
Description:
     
Parameter:
     req_code   E0
	 pa			parameter bytes
     ats        the buffer to save the answer to request from the card
	 number
Return:
     short      status of implement
**************************************************/
short RATS(unsigned char req_code, unsigned char pa, unsigned char *ats, unsigned char *number)
{
    char status = STATUS_SUCCESS;

    /************* initialize *****************/

    RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL); //active receive values after coll

    /* set necessary parameters for transmission */
    ResetInfo(MInfo);
    SerBuffer[0] = req_code;
    SerBuffer[1] = pa;

    MInfo.nBytesToSend = 2;

    /* Set timeout for  RATS*/
    SetTimeOut(6000); //5000

    status = M522PcdCmd(JCMD_TRANSCEIVE,
                        SerBuffer,
                        (MfCmdInfo *)&MInfo);

    if (status) // error occured
    {
        *ats = 0;
    }
    else
    {
        if (MInfo.nBytesReceived < 3) // 2 bytes expected
        {
            *ats = 0;
            status = STATUS_BITCOUNT_ERROR;
        }
        else
        {
            status = STATUS_SUCCESS;
            memcpy(ats, SerBuffer, MInfo.nBytesReceived);
        }
    }

    *number = MInfo.nBytesReceived;
    //	putchar(MInfo.nBytesReceived);
    //	putchar(*number);
    //   z=RcGetReg(JREG_ERROR);
    //	putchar(z);
    //	putchar(MInfo.nBytesReceived);

    //	z=RcGetReg(JREG_COMMIRQ);
    //	putchar(z);
    //	z=RcGetReg(JREG_RXMODE);
    //	putchar(z);

    return status;
}

/*************************************************
Function:      PPS
Description:
     
Parameter:
     PPSS   		parameter bytes
	 PPS1			parameter bytes
     pps        the buffer to save the answer to request from the card
	 number		The number of bytes are received
Return:
     short      status of implement
**************************************************/
short PPS(unsigned char PPSS, unsigned char PPS1, unsigned char *pps, unsigned char *number)
{
    char status = STATUS_SUCCESS;

    /************* initialize *****************/

    RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL); //active receive values after coll

    /* set necessary parameters for transmission */
    ResetInfo(MInfo);
    SerBuffer[0] = PPSS;
    SerBuffer[1] = PPS0;
    SerBuffer[2] = PPS1;
    MInfo.nBytesToSend = 3;

    /* Set timeout for  RATS*/
    SetTimeOut(5000);

    status = M522PcdCmd(JCMD_TRANSCEIVE,
                        SerBuffer,
                        (MfCmdInfo *)&MInfo);

    if (status) // error occured
    {
        *pps = 0;
    }
    else
    {
        if (MInfo.nBytesReceived < 1) // 2 bytes expected
        {
            *pps = 0;
            status = STATUS_BITCOUNT_ERROR;
        }
        else
        {
            status = STATUS_SUCCESS;
            memcpy(pps, SerBuffer, MInfo.nBytesReceived);
        }
    }

    *number = MInfo.nBytesReceived;

    return status;
}
/*************************************************
Function:     DESELECT
Description:   
     
Parameter:
     PCB  		parameter bytes
	 CID		parameter bytes
     SDE        the buffer to save the answer to request from the card
	 number		The number of bytes are received
Return:
     short      status of implement
**************************************************/
short DESELECT(unsigned char PCB, unsigned char CID, unsigned char *SDE, unsigned char *number)
{
    char i, status = STATUS_SUCCESS;

    /************* initialize *****************/

    RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL); //active receive values after coll

    /* set necessary parameters for transmission */
    ResetInfo(MInfo);
    SerBuffer[0] = PCB;
    SerBuffer[1] = CID;

    MInfo.nBytesToSend = 2;

    /* Set timeout for  RATS*/
    SetTimeOut(5000);

    status = M522PcdCmd(JCMD_TRANSCEIVE,
                        SerBuffer,
                        (MfCmdInfo *)&MInfo);

    if (status) // error occured
    {
        *SDE = 0;
    }
    else
    {
        if (MInfo.nBytesReceived < 1) // 1 bytes expected
        {
            *SDE = 0;
            status = STATUS_BITCOUNT_ERROR;
        }
        else
        {
            status = STATUS_SUCCESS;
            memcpy(SDE, SerBuffer, MInfo.nBytesReceived);
        }
    }

    //    z=RcGetReg(JREG_ERROR);
    //	putchar(z);
    //	 z=RcGetReg(JREG_STATUS1);
    //	putchar(z);
    //	putchar(status);
    //	putchar(MInfo.nBytesReceived);

    for (i = 0; i < MInfo.nBytesReceived; i++)
    {
        //	putchar(SerBuffer[i]);
    }

    //	putchar(MInfo.nBytesReceived);
    *number = MInfo.nBytesReceived;
    // 	putchar(*number);
    //	z=MInfo.nBytesReceived;
    //	putchar(z);

    return status;
}

/*************************************************
Function:      APDU
Description:   transceive  command of the cos
     
Parameter:
	  *J_O
     *comm      command
	  slen		The number of the command bytes 
     *resp      response
	 *rlen		The number of the response bytes
Return:
     short      status of implement
**************************************************/
short APDU(unsigned char *J_O, unsigned char *comm, unsigned char slen, unsigned char *resp, unsigned char *rlen)
{
    static char s_cnt = 1;
    char i;
    short status = STATUS_SUCCESS;

    /************* initialize *****************/

    RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL); //active receive values after coll

    /* set necessary parameters for transmission */
    ResetInfo(MInfo);

    s_cnt = !s_cnt;
    *J_O = s_cnt;
    if (comm[1] == 0xA4)
    {
        *J_O = 0;
        s_cnt = 0;
    }
    if (*J_O % 2)
    {
        SerBuffer[0] = 0x0B;
        SerBuffer[1] = 0x01;
    }
    else
    {
        SerBuffer[0] = 0x0A;
        SerBuffer[1] = 0x01;
    }
    for (i = 0; i < slen; i++)
    {
        SerBuffer[i + 2] = comm[i];
    }

    MInfo.nBytesToSend = slen + 2;

    /* Set timeout for  APDU */
    SetTimeOut(12000);
    RcModifyReg(JREG_TMODE, 0, JBIT_TAUTO); //发送后启动定时器

    // log_printf("SerBuffer[0]:%#x", SerBuffer[0]);
    status = M522PcdCmd(JCMD_TRANSCEIVE,
                        SerBuffer,
                        (MfCmdInfo *)&MInfo);

    //putchar(MInfo.nBytesToSend);

    //    z = RcGetReg(JREG_ERROR);
    //	log_printf("JREG_ERROR: %#x",z);
    //    z = RcGetReg(JREG_STATUS1);
    //	log_printf("JREG_STATUS1: %#x",z);

    //    log_printf("status: 0x%#x",status);
    //	putchar(MInfo.nBytesReceived);

    if (status) // error occured
    {
        *resp = 0;
    }
    else
    {
        if (MInfo.nBytesReceived < 2) // 2 bytes expected
        {
            *resp = 0;
            status = STATUS_BITCOUNT_ERROR;
        }
        else
        {
            status = STATUS_SUCCESS;
            memcpy(resp, SerBuffer, MInfo.nBytesReceived);
        }
    }

    // for(i=0;i<MInfo.nBytesReceived;i++)
    // {
    //     printf("%#x ",SerBuffer[i]);
    // }
    // printf("\r\n");
    *rlen = MInfo.nBytesReceived;
    return status;
}

/*************************************************
Function:      APDU_ED
Description:   transceive  command of the cos
     
Parameter:
     *comm      command
	  slen		The number of the command bytes 
     *resp      response
	 *rlen		The number of the response bytes
Return:
     short      status of implement
**************************************************/
short APDU_ED(unsigned char *comm, unsigned char slen, unsigned char *resp, unsigned char *rlen)
{
    char i;
    short status = STATUS_SUCCESS;

    /************* initialize *****************/

    RcSetReg(JREG_COLL, JBIT_VALUESAFTERCOLL); //active receive values after coll

    /* set necessary parameters for transmission */
    ResetInfo(MInfo);

    for (i = 0; i < slen; i++)
    {
        SerBuffer[i] = comm[i];
    }

    for (i = 0; i < slen; i++)
    {
        //		putchar(SerBuffer[i]) ;
    }

    MInfo.nBytesToSend = slen;

    /* Set timeout for  APDU*/
    SetTimeOut(50000);

    status = M522PcdCmd(JCMD_TRANSCEIVE,
                        SerBuffer,
                        (MfCmdInfo *)&MInfo);

    //	 z=RcGetReg(JREG_ERROR);
    //	putchar(z);
    //	 z=RcGetReg(JREG_STATUS1);
    //	putchar(z);

    //	putchar(status);
    //	putchar(MInfo.nBytesReceived);

    if (status) // error occured
    {
        *resp = 0;
    }
    else
    {
        if (MInfo.nBytesReceived < 2) // 2 bytes expected
        {
            *resp = 0;
            status = STATUS_BITCOUNT_ERROR;
        }
        else
        {
            status = STATUS_SUCCESS;

            memcpy(resp, SerBuffer, MInfo.nBytesReceived);
        }
    }

    for (i = 0; i < MInfo.nBytesReceived; i++)
    {
        //	putchar(SerBuffer[i]);
    }

    *rlen = MInfo.nBytesReceived;
    //	putchar(*rlen);

    return status;
}
