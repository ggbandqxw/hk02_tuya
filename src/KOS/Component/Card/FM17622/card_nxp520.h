#ifndef __NXP520_H__
#define __NXP520_H__

#include "device.h"

#define VHW_SPI_CARD   vSPI_0
#define VHW_CS_CARD    vPIN_C4
#define VHW_RST_CARD   vPIN_C5

/////////////////////////////////////////////////////////////////////
//等待PICC返回的循环，和低功耗，波形相关
/////////////////////////////////////////////////////////////////////
#define TOLERANT_WAIT_LOOP 35

/////////////////////////////////////////////////////////////////////
//天线设置 单天线 1 2 还是双天线
/////////////////////////////////////////////////////////////////////
#define ANT_CONFIG ANT_TXD

/////////////////////////////////////////////////////////////////////
//天线模式
/////////////////////////////////////////////////////////////////////
#define ANT_TX1 1
#define ANT_TX2 2
#define ANT_TXD 3 //双天线

/////////////////////////////////////////////////////////////////////
//MF520命令字
/////////////////////////////////////////////////////////////////////
#define PCD_IDLE 0x00        //取消当前命令
#define PCD_MEM 0x01         //储存25字节数据到内部BUFF
#define PCD_CALCCRC 0x03     //CRC计算
#define PCD_TRANSMIT 0x04    //发送数据
#define PCD_NOCMDCHANGE 0x07 //设置CommandReg 不改变里面的Command

#define PCD_RECEIVE 0x08    //接收数据
#define PCD_TRANSCEIVE 0x0C //发送并接收数据
#define PCD_AUTHENT 0x0E    //验证密钥
#define PCD_RESETPHASE 0x0F //复位

/////////////////////////////////////////////////////////////////////
//Mifare_One卡片命令字
/////////////////////////////////////////////////////////////////////
#define PICC_REQIDL 0x26    //寻天线区内未进入休眠状态
#define PICC_REQALL 0x52    //寻天线区内全部卡
#define PICC_ANTICOLL1 0x93 //防冲撞
#define PICC_ANTICOLL2 0x95 //防冲撞
#define PICC_ANTICOLL3 0x97 //防冲撞3
#define PICC_AUTHENT1A 0x60 //验证A密钥
#define PICC_AUTHENT1B 0x61 //验证B密钥
#define PICC_READ 0x30      //读块
#define PICC_WRITE 0xA0     //写块
#define PICC_DECREMENT 0xC0 //扣款
#define PICC_INCREMENT 0xC1 //充值
#define PICC_RESTORE 0xC2   //调块数据到缓冲区
#define PICC_TRANSFER 0xB0  //保存缓冲区中数据
#define PICC_HALT 0x50      //休眠

/////////////////////////////////////////////////////////////////////
//MFRC520 寄存器定义
/////////////////////////////////////////////////////////////////////
// Page 0 :  Command and status
#define RFU00 0x00
#define CommandReg 0x01
#define ComIEnReg 0x02
#define DivlEnReg 0x03
#define ComIrqReg 0x04
#define DivIrqReg 0x05
#define ErrorReg 0x06
#define Status1Reg 0x07
#define Status2Reg 0x08
#define FIFODataReg 0x09
#define FIFOLevelReg 0x0A
#define WaterLevelReg 0x0B
#define ControlReg 0x0C
#define BitFramingReg 0x0D
#define CollReg 0x0E
#define RFU0F 0x0F
//PAGE 1 : Command
#define RFU10 0x10
#define ModeReg 0x11
#define TxModeReg 0x12
#define RxModeReg 0x13
#define TxControlReg 0x14
#define TxAutoReg 0x15
#define TxSelReg 0x16
#define RxSelReg 0x17
#define RxThresholdReg 0x18
#define DemodReg 0x19
#define RFU1A 0x1A
#define RFU1B 0x1B
#define MfTxReg 0x1C
#define MfRxReg 0x1D
#define RFU1E 0x1E
#define RFU1F 0x1F
//Page 2 : Configuration
#define RFU20 0x20
#define CRCResultRegM 0x21
#define CRCResultRegL 0x22
#define RFU23 0x23
#define ModWidthReg 0x24
#define RFU25 0x25
#define RFCfgReg 0x26
#define GsNReg 0x27
#define CWGsCfgReg 0x28
#define ModGsCfgReg 0x29
#define TModeReg 0x2A
#define TPrescalerReg 0x2B
#define TReloadRegH 0x2C
#define TReloadRegL 0x2D
#define TCounterValueRegH 0x2E
#define TCounterValueRegL 0x2F
//Page 3: Test register
#define RFU30 0x30
#define TestSel1Reg 0x31
#define TestSel2Reg 0x32
#define TestPinEnReg 0x33
#define TestPinValueReg 0x34
#define TestBusReg 0x35
#define AutoTestReg 0x36
#define VersionReg 0x37
#define AnalogTestReg 0x38
#define TestDAC1Reg 0x39
#define TestDAC2Reg 0x3A
#define TestADCReg 0x3B
#define RFU3C 0x3C
#define RFU3D 0x3D
#define RFU3E 0x3E
#define RFU3F 0x3F

/////////////////////////////////////////////////////////////////////
//和MF522通讯时返回的错误代码
/////////////////////////////////////////////////////////////////////
#define MI_OK 0
#define MI_NOTAGERR (-1)
#define MI_ERR (-2)
#define MI_COLLERR 3
#define MI_CT 1            // CASCADE TAG
#define DEF_FIFO_LENGTH 64 //FIFO size=64byte
#define MAXRLEN 18

#define MF_CS_H Device_Write(VHW_CS_CARD, NULL, 0, 1)
#define MF_CS_L Device_Write(VHW_CS_CARD, NULL, 0, 0)

#define MF_NRST_H Device_Write(VHW_RST_CARD, NULL, 0, 1)
#define MF_NRST_L Device_Write(VHW_RST_CARD, NULL, 0, 0)

uint8_t Spi_ReadWrite_Byte(uint8_t byte);
void Nxp520_Portinit(void);
void Set_Waitcmd_Loop(uint16_t times);
uint16_t Get_Waitcmd_Loop(void);
void Nxp520_Io_Init(void);
void Nxp520_Write_Reg(uint8_t addr, uint8_t date);
uint8_t Nxp520_Read_Reg(uint8_t addr);
void Nxp520_Antenna_On(void);
void Nxp520_Antenna_Off(void);

signed char Nxp520_Reset(void);
uint8_t Nxp520_ReadCardId(uint8_t *pId, uint16_t *pType);

signed char Nxp520_Request(unsigned char req_code, unsigned char *pTagType);
char Nxp520_Anticoll(unsigned char *pSnr);
char Nxp520_Select(unsigned char *pSnr);
char Nxp520_Auth_State(unsigned char auth_mode, unsigned char addr, unsigned char *pKey, unsigned char *pSnr);
char Nxp520_Read(unsigned char addr, unsigned char *pData);
char Nxp520_write(unsigned char addr, unsigned char *pData);

char Nxp520_Halt(void);
signed char Lowpower_Request(unsigned char req_code, unsigned char *pTagType);
void Set_Reg_Mask(unsigned char reg, unsigned char mask);
void Nxp520_Antenna_On(void);
void Nxp520_Antenna_Off(void);
void Nxp520_FindCard(void);
uint8_t Nxp520_SearchCard(void);
uint8_t UL_PcdAnticoll(unsigned char *pSnr);
//uint8_t Low_FindCard(void);
signed char Lowpower_ReadCardrequest(void);
#endif
