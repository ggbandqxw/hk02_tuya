#ifndef __CARD_CRYPTO_H__
#define __CARD_CRYPTO_H__
    
#include "device.h"

/* 密钥类型 */
typedef enum{
    DesEncryptKeyType           = 0x30, /*!< DES加密密钥类型 */
    DesDecryptKeyType           = 0x31, /*!< DES解密密钥类型 */
    DesMacKeyType               = 0x32, /*!< DESMAC密钥类型 */
    InternalKeyType             = 0x34, /*!< 内部密钥类型 */
    FileLineProtectKeyType      = 0x36, /*!< 文件线路保护密钥类型 */
    UnblockPinKeyType           = 0x37, /*!< 解锁口令密钥类型 */
    ReloadPinKeyType            = 0x38, /*!< 重载口令密钥类型 */
    ExternalAuthKeyType         = 0x39, /*!< 外部认证密钥类型 */
    PinKeyType                  = 0x3A, /*!< 口令密钥类型 */
    UpdateOverdrawLimitKeyType  = 0x3C, /*!< 修改透支限额密钥类型 */
    CreditForUnloadKeyType      = 0x3D, /*!< 圈提密钥类型 */
    PurchaseKeyType             = 0x3E, /*!< 消费密钥类型 */
    CreditForLoadKeyType        = 0x3F, /*!< 圈存密钥类型 */
}KeyType_TypeDef;   

/* EF文件类型 */
typedef enum{
    BinaryFileType              = 0x28, /*!< 二进制文件类型 */
    FixedLengthRecordFileType   = 0x2A, /*!< 定长记录文件类型 */
    VarLengthRecordFileType     = 0x2C, /*!< 变长记录文件类型 */
    LoopFileType                = 0x2E, /*!< 循环文件类型 */ 
    PBOCFileType                = 0x2F, /*!< PBOC ED/EP类型 */
    KeyFileType                 = 0x3F, /*!< 密钥文件类型 */
}EFFileType_Typedef;

/* 取随机数个数 */
typedef enum{
    Challenge_4Bytes    = 4, /* 4字节 */
    Challenge_8Bytes    = 8, /* 8字节 */
}ChallengeNum_Typedef;

/* 内部认证类型 */
typedef enum{
    InternalAuth_Encrypt    = 0x00, /* 加密计算 */
    InternalAuth_Decrypt    = 0x01, /* 解密计算 */
    InternalAuth_MAC        = 0x02, /* MAC计算 */
}InternalAuthType_Typedef;

typedef enum{
    MODE_ENCRYPTION,
    MODE_DECRYPTION,
}CryptoMode_enum_t;

/* 卡片类型 */
#define NO_CARD     0
#define CPU_CARD    1
#define M1_CARD     2

/* 验证失败错误类型 */
#define CARD_ERR_NO_ADD  1 //未添加的卡片
#define CARD_ERR_UNKNOW  2 //未知卡片（未经过鼎新同创发卡）
#define CARD_ERR_TYPE    3 //卡片类型错误



/* 默认ID和名称 */
#define Default_MF_ID       0x3F00
// #define Default_MF_NAME     "KDS.SYS.DDF01"

#define Default_DF_ID        0xDF01
#define Default_DF_NAME      "KDS.APP.DDF01"

#define Default_KeyFile_Id  0x0000

#define Default_ExKey_Id    0x00
#define Default_EncKey_Id   0x01


/* 默认密钥 */
#define Default_MF_ExKey    "DxtcSmartLockM"
#define Default_Df_ExKey    "DxtcSmartLockD"
#define Default_Df_SamKey   "DxtcSmartLockS"
#define Default_Df_PinKey   "1314"

#define Tdes_Key_Len        16
#define Des_Key_Len         8

/* SW1,SW2返回值 */
#define SW_OperateSuccessfully      0x9000
#define SW_ParamP1P2Err             0x6A86


ErrorStatus CardCrypto_VerifyCard(uint8_t *pCardId, uint8_t idLen, uint8_t type, uint8_t *err);
ErrorStatus CardCrypto_AddCard(uint8_t *pCardId, uint8_t idLen, uint8_t type);
uint8_t CardCrypto_ReadCardId(uint8_t *pCardId, uint8_t *pType);


#endif /* __CARD_CRYPTO_H__ */
