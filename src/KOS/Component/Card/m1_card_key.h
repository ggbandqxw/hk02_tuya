#ifndef __M1_CARD_KEY_H__
#define __M1_CARD_KEY_H__

#include "osal.h"

ErrorStatus CardEncrypt_CardIdToKey(uint8_t *pId, uint8_t idLen, uint8_t *pKey);

#endif /* __M1_CARD_KEY_H__ */
