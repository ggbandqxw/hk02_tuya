#ifndef __CARD_HAL_H__
#define __CARD_HAL_H__

#include "device.h"

#define VHW_SPI_CARD vSPI_0
#define VHW_CS_CARD vPIN_C4
#define VHW_RST_CARD vPIN_C5

#define MF_CS_H Device_Write(VHW_CS_CARD, NULL, 0, 1)
#define MF_CS_L Device_Write(VHW_CS_CARD, NULL, 0, 0)

#define MF_NRST_H Device_Write(VHW_RST_CARD, NULL, 0, 1)
#define MF_NRST_L Device_Write(VHW_RST_CARD, NULL, 0, 0)


unsigned char ReadRawRC(unsigned char Address);
void WriteRawRC(unsigned char Address, unsigned char value);
void SetBitMask(unsigned char reg, unsigned char mask);
void ClearBitMask(unsigned char reg, unsigned char mask);

#endif
