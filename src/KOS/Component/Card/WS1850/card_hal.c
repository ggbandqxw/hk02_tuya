#include "nfc.h"
#include "card_hal.h"

/**
  * @brief  读写一个字节
  * @note   
  *         
  * @param  TxData:要写入的字节
  * @return 返回值:读取到的数据
  */
uint8_t SPI_Card_ReadWriteByte(uint8_t txData)
{
	uint8_t rxData = 0;
	Device_Write(VHW_SPI_CARD, &txData, 1, &rxData);
	return rxData;
}

/////////////////////////////////////////////////////////////////////
//功    能：读寄存器
//参数说明：Address[IN]:寄存器地址
//返    回：读出的值
/////////////////////////////////////////////////////////////////////
unsigned char ReadRawRC(unsigned char Address)
{
	unsigned char ucAddr;
	unsigned char ucResult = 0;
	MF_CS_L;
	ucAddr = ((Address << 1) & 0x7E) | 0x80;

	SPI_Card_ReadWriteByte(ucAddr);
	ucResult = SPI_Card_ReadWriteByte(0);
	MF_CS_H;
	return ucResult;
}

/////////////////////////////////////////////////////////////////////
//功    能：写寄存器
//参数说明：Address[IN]:寄存器地址
//          value[IN]:写入的值
/////////////////////////////////////////////////////////////////////
void WriteRawRC(unsigned char Address, unsigned char value)
{
	unsigned char ucAddr;

	MF_CS_L;
	ucAddr = ((Address << 1) & 0x7E);

	SPI_Card_ReadWriteByte(ucAddr);
	SPI_Card_ReadWriteByte(value);
	MF_CS_H;
}

/////////////////////////////////////////////////////////////////////
//??    ???????????÷??
//???????÷??reg[IN]:?????÷???·
//          mask[IN]:??????
/////////////////////////////////////////////////////////////////////
void SetBitMask(unsigned char reg, unsigned char mask)
{
	char tmp = 0x0;
	tmp = ReadRawRC(reg);
	WriteRawRC(reg, tmp | mask); // set bit mask
}

/////////////////////////////////////////////////////////////////////
//??    ???????????÷??
//???????÷??reg[IN]:?????÷???·
//          mask[IN]:??????
/////////////////////////////////////////////////////////////////////
void ClearBitMask(unsigned char reg, unsigned char mask)
{
	char tmp = 0x0;
	tmp = ReadRawRC(reg);
	WriteRawRC(reg, tmp & ~mask); // clear bit mask
}

