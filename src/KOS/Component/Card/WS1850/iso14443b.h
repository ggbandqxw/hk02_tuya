/**
 ****************************************************************
 * @file iso14443b.h
 *
 * @brief 
 *
 * @author 
 *
 *
 ****************************************************************
 */

#ifndef ISO_14443B_H
#define ISO_14443B_H

#include "device.h"

/////////////////////////////////////////////////////////////////////
//ISO14443B COMMAND
/////////////////////////////////////////////////////////////////////
#define ISO14443B_ANTICOLLISION 0x05
#define ISO14443B_ATTRIB 0x1D
#define ISO14443B_HLTB 0x50

#define FSDI 8 //Frame Size for proximity coupling Device, in EMV test. 身份证必须FSDI = 8

/////////////////////////////////////////////////////////////////////
//函数原型
/////////////////////////////////////////////////////////////////////
unsigned char PcdRequestB(unsigned char req_code, unsigned char AFI, unsigned char N, unsigned char *ATQB);
unsigned char PcdSlotMarker(unsigned char N, unsigned char *ATQB);
unsigned char PcdAttriB(unsigned char *PUPI, unsigned char dsi_dri, unsigned char pro_type, unsigned char CID, unsigned char *answer);
unsigned char GetIdcardNum(unsigned char *ATQB);

#endif
