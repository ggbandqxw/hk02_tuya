#ifndef __CARD_H__
#define __CARD_H__

#include "component.h"

/* Card组件消息类型 */
typedef struct
{
    uint8_t cardLen;    //卡片长度
    uint8_t cardId[10]; //卡片id
} CardMsg_t;

typedef enum
{
    CARD_MODE_ADD,
    CARD_MODE_VERIFY,
} CardMode_enum_t;

static ErrorStatus Card_SetWorkMode(CardMode_enum_t mode);
static ErrorStatus Card_SetFunction(FunctionalState status);

#endif /* __CARD_H__ */
