#ifndef __MC60_UART_H__
#define __MC60_UART_H__

#define EXTEND_ITIC

#define MAX_PACKAGE_LEN              128     //最大包长

//typedef void (*extkds_send_cb_fun_t)(uint8_t cmd, void *, uint16_t); //发送回调类型
typedef void (*mc60_send_cb_fun_t)( uint16_t cmd, void *, uint16_t); //发送回调类型

//typedef void (*extkds_send_cb_fun_t)(uint8_t cmd, void *, uint16_t); //发送回调类型

typedef enum
{
    MC60_RECV_CMD_PACKET,
    MC60_EXEC_CB,
} MC60MsgType_enum_t;

//uint8_t cmd;     extkds_send_cb_fun_t
/* ExtendKds组件消息类型 */  
typedef struct
{
    MC60MsgType_enum_t msgType;
    mc60_send_cb_fun_t cb;
    uint16_t cmd;
    uint8_t times;
    uint16_t len;
    uint8_t data[];
} Mc60UartMsg_t; //

#define Mc60_Send(cmd_in, data_in, len_in, cb_in)                                         \
({                                                                                          \
    Mc60UartMsg_t *msg = OSAL_Malloc(sizeof(Mc60UartMsg_t) + len_in);                     \
    if (msg)                                                                                \
    {                                                                                       \
        msg->msgType = MC60_RECV_CMD_PACKET;                                                 \
        msg->cb = cb_in;                                                                    \
        msg->cmd = cmd_in;                                                                  \
        msg->len = len_in;                                                                  \
        memcpy(msg->data, data_in, len_in);                                                 \
        OSAL_MboxPost(COMP_MC60_UART, 0, (uint8_t *)msg, sizeof(Mc60UartMsg_t) + len_in); \
        OSAL_Free(msg);                                                                     \
    }                                                                                       \
})


#endif /* __MC60_UART_H__ */
