#include "device.h"
#include "motor524.h"

/* 调试打印接口 */
#define Motor524_LOG(format, ...) __OSAL_LOG("[motor524.c] " C_PURPLE format C_NONE, ##__VA_ARGS__)

#define VHW_MOTO_R vPWM_2
#define VHW_MOTO_F vPWM_1

//初始化
void Motor524_Init(void)
{
}

//电机左转
void Motor524_TurnLeft(void)
{
	Device_Disable(VHW_MOTO_R);
	Device_Enable(VHW_MOTO_F);
	Device_DelayUs(5);
	// Device_SetPwm(VHW_MOTO_R, 0, 0);
	Device_SetPwm(VHW_MOTO_F, 0, 70);
}

//电机右转
void Motor524_TurnRight(void)
{
	Device_Disable(VHW_MOTO_F);
	Device_Enable(VHW_MOTO_R);
	Device_DelayUs(5);
	// Device_SetPwm(VHW_MOTO_F, 0, 0);
	Device_SetPwm(VHW_MOTO_R, 0, 70);
}

//电机刹车
void Motor524_Brake(void)
{
	Device_Enable(VHW_MOTO_R);
	Device_Enable(VHW_MOTO_F);
	Device_DelayUs(5);
	Device_SetPwm(VHW_MOTO_R, 0, 100);
	Device_SetPwm(VHW_MOTO_F, 0, 100);
}

//电机停止
void Motor524_Stop(void)
{
	// Device_SetPwm(VHW_MOTO_R, 0, 0);
	// Device_SetPwm(VHW_MOTO_F, 0, 0);
	// Device_DelayUs(5);
	Device_Disable(VHW_MOTO_R);
	Device_Disable(VHW_MOTO_F);
}
