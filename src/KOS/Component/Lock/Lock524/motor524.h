
#ifndef __MOTOR524_H__
#define __MOTOR524_H__
#include "stdint.h"

//初始化
extern void Motor524_Init(void);

//电机左转
extern void Motor524_TurnLeft(void);

//电机右转
extern void Motor524_TurnRight(void);

//电机刹车
extern void Motor524_Brake(void);

//电机停止
extern void Motor524_Stop(void);

#endif
