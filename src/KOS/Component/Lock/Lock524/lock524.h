#ifndef _LOCK_TASK_H_
#define _LOCK_TASK_H_
    
#include "application.h"


/* 应用层锁体控制方式 */
typedef enum
{
	LOCK_CTRL_UNLOCK,//开锁
	LOCK_CTRL_LOCKED,//上锁
	LOCK_CTRL_ANTI_LOCK,     //反锁
	LOCK_CTRL_VERIFY_KEYS_OK,//验证密钥成功
}LockControlMethods_enum_t;

/* 锁体运行结果 */
typedef enum
{
	/* 锁体运行结果 */
	LOCK_RES_BUSY  =1,		      //锁体忙
	LOCK_RES_LOCKED =2,		      //已上锁
	LOCK_RES_UNLOCK =3, 		  //已开锁
	LOCK_RES_LOCKED_OK =4,        //上锁成功
	LOCK_RES_UNLOCK_OK =5,   	  //开锁成功
	LOCK_RES_NOT_LOCKED =6,       //门未上锁
	LOCK_RES_ABNORMAL =7,     	  //门锁异常
	LOCK_RES_FORCE_UNLOCK  =8,    //暴力开锁（撬锁）
	DOOR_RES_OPEN  =9,   		  //已开门
	DOOR_RES_CLOSE  =10,   		  //已关门
  LOCK_RES_UNLOCK_FAILED =11, //开门堵转报警

} LockResType_enum_t;

/**
  * @brief  锁体任务主动上报锁体消息到APP注册回调接口
  * @note   
  *         
  * @param  pFunc：通知上层回调函数
  * @param  result：回调的接口
  */
extern void LockEvent_RegistedCallback(void (*pFunc)(LockResType_enum_t result));
/**
  * @brief  用户控制锁体开锁/关锁接口
  * @param  control: 只能是LOCK_CTRL_UNLOCK、LOCK_CTRL_LOCKED其他值无效    
  * @param  pFunc:控制锁体执行的结果，以回调的形式通知上层
  * @note   该函数以回调形式，在底层的中断函数里面调用
  */
extern void LockTask_UserControlOpenCloseLock(LockControlMethods_enum_t control ,void (*pFunc)(uint32_t control_result));
/**
  * @brief  锁体模式设置
  * @param  button_status: 按键状态 
  * @note   NULL
  */
extern void LockTask_WorkModeSet(uint8_t mode);
/**
  * @brief  锁体处理唤醒源处理
  * @param  wake_source: 唤醒源
  * @note   NULL
  */
extern void LockTask_ProcessWakeUpSource(WakeType_enum_t wake_source);




#endif















































