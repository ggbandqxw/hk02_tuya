#ifndef __MOTORBIGPLUS_H__
#define __MOTORBIGPLUS_H__
#include "stdint.h"

//初始化
extern void MotorBigPlus_Init(void);

//电机左转
extern void MotorBigPlus_TurnLeft(void);

//电机右转
extern void MotorBigPlus_TurnRight(void);

//电机刹车
extern void MotorBigPlus_Brake(void);

//电机停止
extern void MotorBigPlus_Stop(void);

//设置电机方向
extern void MotorBigPlus_SetDirection(uint8_t dir);

#endif
