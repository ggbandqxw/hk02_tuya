#include "device.h"
#include "motor_bigplus.h"

static uint8_t motorBigPlus_direction = 1;


#define VHW_MOTO_R		vPWM_1
#define VHW_MOTO_F		vPWM_2

//初始化
void MotorBigPlus_Init(void)
{
	
}

//电机左转
void MotorBigPlus_TurnLeft(void)
{
	if (motorBigPlus_direction == 2)
    {
		Device_Enable(VHW_MOTO_R);
		Device_DelayUs(5);
		Device_SetPwm(VHW_MOTO_F, 0, 0);	
		Device_SetPwm(VHW_MOTO_R, 0, 70);

	}
	else
	{
		Device_Enable(VHW_MOTO_R);
		Device_DelayUs(5);
		Device_SetPwm(VHW_MOTO_R, 0, 0);
		Device_SetPwm(VHW_MOTO_F, 0, 70);

	}

}

//电机右转
void MotorBigPlus_TurnRight(void)
{
	if (motorBigPlus_direction == 2)
    {
		Device_Enable(VHW_MOTO_R);
		Device_DelayUs(5);
		Device_SetPwm(VHW_MOTO_R, 0, 0);
		Device_SetPwm(VHW_MOTO_F, 0, 70);

	}
	else
	{
		Device_Enable(VHW_MOTO_R);
		Device_DelayUs(5);
		Device_SetPwm(VHW_MOTO_F, 0, 0);	
		Device_SetPwm(VHW_MOTO_R, 0, 70);

	}
}

//电机刹车
void MotorBigPlus_Brake(void)
{
	Device_Enable(VHW_MOTO_R);
	Device_DelayUs(5);
	Device_SetPwm(VHW_MOTO_R, 0, 100);
	Device_SetPwm(VHW_MOTO_F, 0, 100);

}

//电机停止
void MotorBigPlus_Stop(void)
{
	Device_SetPwm(VHW_MOTO_R, 0, 0);
	Device_SetPwm(VHW_MOTO_F, 0, 0);
	Device_DelayUs(5);
	Device_Disable(VHW_MOTO_R);
	Device_Disable(VHW_MOTO_F);

}

//设置电机方向
void MotorBigPlus_SetDirection(uint8_t dir)
{
    motorBigPlus_direction = dir;
}
