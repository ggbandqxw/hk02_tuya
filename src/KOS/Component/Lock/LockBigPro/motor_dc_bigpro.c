#include "device.h"
#include "motor_bigpro.h"
//直流电机所用的文件
static uint8_t motorBig_direction = 1;
static uint8_t motor_last_turn_dir = 0;

#define VHW_MOTO_IN1		vPIN_C10
#define VHW_MOTO_IN2		vPIN_C11

//初始化
void MotorBigPro_Init(void)
{
	
}

//电机左转
void MotorBigPro_TurnLeft(void)
{
	motor_last_turn_dir = 0;
	if (motorBig_direction == 1)
    {
		Device_Write(VHW_MOTO_IN1, NULL, 0, 0);
		Device_Write(VHW_MOTO_IN2, NULL, 0, 1);
	}
	else
	{
		Device_Write(VHW_MOTO_IN1, NULL, 0, 1);
		Device_Write(VHW_MOTO_IN2, NULL, 0, 0);
	}
}

//电机右转
void MotorBigPro_TurnRight(void)
{
	motor_last_turn_dir = 1;
	if (motorBig_direction == 1)
    {
		Device_Write(VHW_MOTO_IN1, NULL, 0, 1);
		Device_Write(VHW_MOTO_IN2, NULL, 0, 0);
	}
	else
	{
		Device_Write(VHW_MOTO_IN1, NULL, 0, 0);
		Device_Write(VHW_MOTO_IN2, NULL, 0, 1);
	}
}

//电机停止
void MotorBigPro_Stop(void)
{
		Device_Write(VHW_MOTO_IN1, NULL, 0, 0);
		Device_Write(VHW_MOTO_IN2, NULL, 0, 0);
}

//电机刹车
void MotorBigPro_Brake(void)
{
	
		Device_Write(VHW_MOTO_IN1, NULL, 0, 1);
		Device_Write(VHW_MOTO_IN2, NULL, 0, 1);
}

//设置电机方向
void MotorBigPro_SetDirection(uint8_t dir)
{
    motorBig_direction = dir;
}

void MotorBigPro_Disable(void)
{

}