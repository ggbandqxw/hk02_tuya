#ifndef __MOTOR_BIGPRO_H__
#define __MOTOR_BIGPRO_H__
#include "stdint.h"

//初始化
extern void MotorBigPro_Init(void);

//电机左转
extern void MotorBigPro_TurnLeft(void);

//电机右转
extern void MotorBigPro_TurnRight(void);

//电机刹车
extern void MotorBigPro_Brake(void);

//电机停止
extern void MotorBigPro_Stop(void);

//设置电机方向
extern void MotorBigPro_SetDirection(uint8_t dir);

extern void MotorBigPro_Disable(void);

#endif
