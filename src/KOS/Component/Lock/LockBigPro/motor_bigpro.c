#include "device.h"
#include "motor_bigpro.h"

static uint8_t motorBig_direction = 1;
static uint8_t motor_last_turn_dir = 0;

#define VHW_MOTO_FR		vPIN_C10
#define VHW_MOTO_BR		vPIN_C11
#define VHW_MOTO_EN		vPIN_C12


//初始化
void MotorBigPro_Init(void)
{
	
}

//电机左转
void MotorBigPro_TurnLeft(void)
{
	motor_last_turn_dir = 0;
	if (motorBig_direction == 1)
    {
		Device_Write(VHW_MOTO_FR, NULL, 0, 0);
	}
	else
	{
		Device_Write(VHW_MOTO_FR, NULL, 0, 1);
	}
	Device_Write(VHW_MOTO_BR, NULL, 0, 1);
	Device_Write(VHW_MOTO_EN, NULL, 0, 1);

}

//电机右转
void MotorBigPro_TurnRight(void)
{
	motor_last_turn_dir = 1;
	if (motorBig_direction == 1)
    {
		Device_Write(VHW_MOTO_FR, NULL, 0, 1);
	}
	else
	{
		Device_Write(VHW_MOTO_FR, NULL, 0, 0);
	}
	Device_Write(VHW_MOTO_BR, NULL, 0, 1);
	Device_Write(VHW_MOTO_EN, NULL, 0, 1);
}

//电机停止
void MotorBigPro_Stop(void)
{
	Device_Write(VHW_MOTO_EN, NULL, 0, 0);

}

//电机刹车
void MotorBigPro_Brake(void)
{
	
	Device_Write(VHW_MOTO_BR, NULL, 0, 0);



}

//设置电机方向
void MotorBigPro_SetDirection(uint8_t dir)
{
    motorBig_direction = dir;
}

void MotorBigPro_Disable(void)
{
	Device_Write(VHW_MOTO_FR, NULL, 0, 0);
	Device_Write(VHW_MOTO_BR, NULL, 0, 0);
	Device_Write(VHW_MOTO_EN, NULL, 0, 0);
}