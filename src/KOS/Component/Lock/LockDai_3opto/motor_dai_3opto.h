#ifndef __MOTOR_H__
#define __MOTOR_H__

#define MOTO_DIR_CCW 0  //逆时针方向
#define MOTO_DIR_CW  1  //顺时针方向

/* 电机状态类型 */
typedef enum
{
	MOTOR_STA_LOCKED,               //电机上锁状态：上锁
    MOTOR_STA_LOCKED_BRAKE,         //电机上锁状态：刹车
    MOTOR_STA_LOCKED_BACK,          //电机上锁状态：回拖
    MOTOR_STA_LOCKED_BACK_BRAKE,     //电机上锁回拖状态：刹车
	MOTOR_STA_UNLOCK,               //电机开锁状态
    MOTOR_STA_UNLOCK_BRAKE,         //电机开锁状态：刹车
	MOTOR_STA_UNLOCK_BACK,          //电机开锁状态：回拖
	MOTOR_STA_UNLOCK_BACK_BRAKE,  //电机上锁回拖状态：刹车
    MOTOR_STA_LOCKED_RESET,         //电机上锁回位状态
    MOTOR_STA_LOCKED_RESET_BRAKE,   //电机上锁回位状态：刹车
    MOTOR_STA_UNLOCK_RESET,         //电机开锁回位状态
    MOTOR_STA_UNLOCK_RESET_BRAKE,   //电机开锁回位状态：刹车
    MOTOR_STA_STOP,                 //电机停止状态（暂停）
	MOTOR_STA_IDLE,                 //电机空闲
}MotorStatus_enum_t;
#pragma pack(1)
typedef struct
{
    uint8_t g_autoCalFlag;
    uint8_t motor_direction;
    uint8_t version[13]; // 后板版本号
} LockNvData_stu_t;
#pragma pack()
extern void Motor_TurnLeft(void);
extern void Motor_TurnRight(void);
extern void Motor_Brake(void);
extern void Motor_Stop(void);

extern void Motor_PwmInit(void);
extern void Motor_ChangeDirection(void);
extern void Motor_SetDirection(uint8_t dir);
extern uint8_t Motor_GetDirection(void);
extern void Motor_UpdateDirection(void);
extern void Motor_SaveDirection(void);
ErrorStatus Motor_LockMotorDirSet(uint8_t lock_motor_dir);

#endif
