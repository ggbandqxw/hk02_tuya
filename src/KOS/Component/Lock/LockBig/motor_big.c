#include "device.h"
#include "motor_big.h"

static uint8_t motorBig_direction = 1;

#define VHW_MOTO_R		vPIN_C10
#define VHW_MOTO_F		vPIN_C11


//初始化
void MotorBig_Init(void)
{
	
}

//电机左转
void MotorBig_TurnLeft(void)
{
	if (motorBig_direction == 2)
    {
		Device_Write(VHW_MOTO_R, NULL, 0, 1);
		Device_Write(VHW_MOTO_F, NULL, 0, 0);
	}
	else
	{
		Device_Write(VHW_MOTO_R, NULL, 0, 0);
		Device_Write(VHW_MOTO_F, NULL, 0, 1);
	}

}

//电机右转
void MotorBig_TurnRight(void)
{
	if (motorBig_direction == 2)
    {
		Device_Write(VHW_MOTO_R, NULL, 0, 0);
		Device_Write(VHW_MOTO_F, NULL, 0, 1);
	}
	else
	{
		Device_Write(VHW_MOTO_R, NULL, 0, 1);
		Device_Write(VHW_MOTO_F, NULL, 0, 0);
	}
}

//电机刹车
void MotorBig_Brake(void)
{
	Device_Write(VHW_MOTO_R, NULL, 0, 1);
	Device_Write(VHW_MOTO_F, NULL, 0, 1);
}

//电机停止
void MotorBig_Stop(void)
{
	Device_Write(VHW_MOTO_R, NULL, 0, 0);
	Device_Write(VHW_MOTO_F, NULL, 0, 0);
}

//设置电机方向
void MotorBig_SetDirection(uint8_t dir)
{
    motorBig_direction = dir;
}
