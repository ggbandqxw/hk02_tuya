#ifndef __MOTORBIG_H__
#define __MOTORBIG_H__
#include "stdint.h"

//初始化
extern void MotorBig_Init(void);

//电机左转
extern void MotorBig_TurnLeft(void);

//电机右转
extern void MotorBig_TurnRight(void);

//电机刹车
extern void MotorBig_Brake(void);

//电机停止
extern void MotorBig_Stop(void);

//设置电机方向
extern void MotorBig_SetDirection(uint8_t dir);

#endif
