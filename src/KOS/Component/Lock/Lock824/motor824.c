#include "device.h"
#include "motor824.h"

/* 调试打印接口 */
#define MOTOR824_LOG(format, ...)  				__OSAL_LOG("[motor824.c] " C_PURPLE format C_NONE, ##__VA_ARGS__)

#define VHW_MOTO_R		vPWM_2
#define VHW_MOTO_F		vPWM_1

//初始化
void Motor824_Init(void)
{
	
}

//电机左转
void Motor824_TurnLeft(void)
{
	Device_Enable(VHW_MOTO_R);
	Device_DelayUs(5);
	Device_SetPwm(VHW_MOTO_R, 0, 0);
	Device_SetPwm(VHW_MOTO_F, 0, 70);
}

//电机右转
void Motor824_TurnRight(void)
{
	Device_Enable(VHW_MOTO_R);
	Device_DelayUs(5);
	Device_SetPwm(VHW_MOTO_F, 0, 0);	
	Device_SetPwm(VHW_MOTO_R, 0, 70);
}

//电机刹车
void Motor824_Brake(void)
{
	Device_Enable(VHW_MOTO_R);
	Device_DelayUs(5);
	Device_SetPwm(VHW_MOTO_R, 0, 100);
	Device_SetPwm(VHW_MOTO_F, 0, 100);
}

//电机停止
void Motor824_Stop(void)
{
	Device_SetPwm(VHW_MOTO_R, 0, 0);
	Device_SetPwm(VHW_MOTO_F, 0, 0);
	Device_DelayUs(5);
	Device_Disable(VHW_MOTO_R);
	Device_Disable(VHW_MOTO_F);
}
