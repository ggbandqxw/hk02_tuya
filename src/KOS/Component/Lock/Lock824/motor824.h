#ifndef __MOTOR824_H__
#define __MOTOR824_H__
#include "stdint.h"

//初始化
extern void Motor824_Init(void);

//电机左转
extern void Motor824_TurnLeft(void);

//电机右转
extern void Motor824_TurnRight(void);

//电机刹车
extern void Motor824_Brake(void);

//电机停止
extern void Motor824_Stop(void);

#endif
