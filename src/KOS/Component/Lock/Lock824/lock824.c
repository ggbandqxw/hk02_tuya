#include "device.h"
#include "component.h"
#include "motor824.h"

/* 调试打印接口 */
#define LOCK824_LOG(format, ...)			//OSAL_LOG("[lock824.c] " C_PURPLE format C_NONE, ##__VA_ARGS__)

/* 电机电流采集的ADC通道 */
#define LOCK_DCI_ADC_CHANNEL				vADC_1

/* 事件 */
#define EVENT_MOTOR_MONITOR        			(0X00000001)  	//电机监控事件

/* 环形缓存长度、大小定义 */
#define LOCK_RING_BUFFER_LEN				(5)

/* 电机监测时间间隔 */
#define MOTOR_MONITOR_TIME					(10)	//电机监测时间间隔 10ms

/* 电机控制参数 */
#define MOTOR_PARAM_MAX_REPEAT_TIMES		3    	//最大重复次数（上锁/开锁失败后，会自动重复3次）
#define MOTOR_PARAM_MAX_DCI_VALUE			140  	//电机堵转限制电流280mA 采样电阻0.5Ω (12bitADC)
#define MOTOR_PARAM_LOCKED_MORE_TIME		50   	//上锁过推时间
#define MOTOR_PARAM_STOP_TIME1				1000 	//电机暂停时间1（电机运行正常运行过程中状态切换需要的暂停时间）
 
#define MOTOR_PARAM_STOP_TIME2				100  	//电机暂停时间2（开锁堵转暂停时间、电机运行流程结束最后的暂停时间）
   
#define MOTOR_PARAM_TURN_TIMEOUT			2000 	//电机正常运行，超时时间
#define MOTOR_PARAM_BACK_TIMEOUT			1000 	//电机回拖运行，超时时间
#define MOTOR_PARAM_DCI_DETECT_DELAY_TIME	80   	//电机启动后，电流检测延迟时间
#define MOTOR_PARAM_BRAKE_TIME				30   	//电机刹车时间

/* 电机状态类型 */
typedef enum
{
	MOTOR_STA_LOCKED,          			//电机上锁状态：上锁
	MOTOR_STA_LOCKED_MORE_TIME,			//电机上锁状态：过推
	MOTOR_STA_UNLOCK,          			//电机开锁状态
	MOTOR_STA_BACK,            			//电机回拖状态：回拖
	MOTOR_STA_BACK_BRAKE,      			//电机回拖状态：刹车
	MOTOR_STA_STOP,            			//电机停止状态（暂停）
	MOTOR_STA_IDLE,            			//电机空闲
}MotorStatus_enum_t;

/* 电机状态数据 */
static struct
{
	volatile MotorStatus_enum_t last;    	//电机上一次的活动状态  只能是：MOTOR_STA_LOCKED  MOTOR_STA_UNLOCK
	volatile MotorStatus_enum_t current; 	//电机当前状态	
	volatile uint32_t startTime;         	//电机当前状态开始时间
	volatile uint32_t timeout;           	//电机当前状态超时时间
}motorStatus = {MOTOR_STA_IDLE, MOTOR_STA_IDLE};

/* 电机运行错误情况 */
volatile static enum
{
	ERR_NO_ERROR,      					//无错误
	ERR_LOCK_ERROR,    					//锁体错误（电机运转结束时，传感器状态不对）
	ERR_LOCKED_TIMEOUT,					//上锁超时
	ERR_UNLOCK_TIMEOUT,					//开锁超时
	ERR_BACK_TIMEOUT,  					//回拖超时
	ERR_LOCKED_DCI,    					//上锁过流
	ERR_UNLOCK_DCI,    					//开锁过流
	ERR_BACK_DCI,      					//回拖过流
}motorError = ERR_NO_ERROR;

/* 传感器状态类型 */
typedef enum
{
	LOCK_SENSOR_UNKNOW,  				//传感器状态未知
	LOCK_SENSOR_SET,     				//传感器状态：到位（感应到了）
	LOCK_SENSOR_LONG_SET,				//传感器状态：长时间到位（长按）
	LOCK_SENSOR_RESET,   				//传感器状态：未到位（没感应到）
}SensorStatus_enum_t;

typedef struct 
{
    UserAction_enum_t action;   		//动作
    char *name;                 		//传感器名称
}LockSensorAction_t;

/* 锁体传感器状态 */
volatile SensorStatus_enum_t doorSensor = LOCK_SENSOR_UNKNOW;  //门传感器
volatile SensorStatus_enum_t lockedSensor = LOCK_SENSOR_UNKNOW;//上锁传感器
volatile SensorStatus_enum_t unlockSensor = LOCK_SENSOR_UNKNOW;//开锁传感器
volatile SensorStatus_enum_t backSensor = LOCK_SENSOR_UNKNOW;  //回拖传感器

/* 锁体状态类型 */
typedef enum
{
	LOCK_STA_UNKNOW 			= 0X00,			//未知状态
	
	LOCK_STA_LOCKED           	= 0X01, 		//上锁状态
	LOCK_STA_LOCKED_USER      	= 0X11, 		//用户控制上锁
	LOCK_STA_LOCKED_REPEAT    	= 0X41, 		//堵转重新上锁
	
	LOCK_STA_UNLOCK           	= 0X02, 		//开锁状态
	LOCK_STA_UNLOCK_USER      	= 0X12, 		//用户控制开锁
	LOCK_STA_UNLOCK_DOOROPEN  	= 0X32, 		//开门（上锁过程中门开了）自动开锁
	LOCK_STA_UNLOCK_BACK      	= 0X42, 		//开门自动回拖、休眠自动回拖
    LOCK_STA_UNLOCK_REPEAT    	= 0X52, 		//堵转重新开锁
}LockStatus_enum_t;

volatile static LockStatus_enum_t lockStatus;	//锁状态
volatile static uint8_t repeatCount;   			//重复上锁计数
volatile static uint8_t isrPendFlag;         	//中断挂起标志

static RingBufferHandle_t rbHandle = NULL;

/* 锁体上报消息标志 */
static FlagStatus lockReportMsgFlag = RESET;
__EFRAM static uint8_t slot_status,op2_status;
static lock_ram_flag = UINT32_FLAG;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
  * @brief  切换电机状态
  * @note   该函数会在任务里面调用，也会在中断函数里面调用，所以该函数要做保护处理
  *         这里使用 挂起标志 来实现互斥处理。
  *
  * @param  status：需要切换的状态
  * @param  timeout：该状态的超时时间
  */
static void Lock824_SwitchMotorStatus(MotorStatus_enum_t status, uint32_t timeout)
{
	isrPendFlag++;
	
	if (MOTOR_STA_LOCKED == motorStatus.current ||  MOTOR_STA_UNLOCK == motorStatus.current)
	{
		/* 保存电机上次的活动状态 */
		motorStatus.last = motorStatus.current;
	}
	
	motorStatus.current = status;
	motorStatus.timeout = timeout;
	motorStatus.startTime = OSAL_GetTickCount();

	if (MOTOR_STA_BACK == motorStatus.current)
	{
		if (MOTOR_STA_LOCKED == motorStatus.last)
		{
			/* 上锁回拖 */
			Motor824_TurnLeft();//上锁回拖操作跟开锁操作控制电机的方向是一致的
		}
		else if (MOTOR_STA_UNLOCK == motorStatus.last || MOTOR_STA_IDLE == motorStatus.last)
		{
			/* 开锁回拖 */
			Motor824_TurnRight();//开锁回拖操作跟上锁操作控制电机的方向是一致的
		}
	}
	else if (MOTOR_STA_BACK_BRAKE == motorStatus.current)
	{
		/* 回拖刹车 */
		Motor824_Brake();
	}
	else if (MOTOR_STA_LOCKED == motorStatus.current || MOTOR_STA_LOCKED_MORE_TIME == motorStatus.current)
	{
		/* 电机上锁 */
		Motor824_TurnRight();
	}
	else if (MOTOR_STA_UNLOCK == motorStatus.current)
	{
		/* 电机开锁 */
		Motor824_TurnLeft();
	}
	else
	{
		/* 电机停止 */
		Motor824_Stop();
	}
	
	isrPendFlag--;
}

/**
  * @brief  处理上锁流程结束
  *         
  * @note   
  */
static void Lock824_MotorLockedEnd(void)
{
	LockMsg_t lockMsg;
    memset(&lockMsg, 0, sizeof(lockMsg));
	if (doorSensor == LOCK_SENSOR_RESET)
	{
		/* 上锁过程中门开了：自动执行开锁 */
		motorError = ERR_NO_ERROR;
		lockStatus = LOCK_STA_UNLOCK_DOOROPEN;
		Lock824_SwitchMotorStatus(MOTOR_STA_UNLOCK, MOTOR_PARAM_TURN_TIMEOUT);
	}
	else 
	{
		if ( motorError == ERR_LOCKED_DCI
			&& repeatCount <  MOTOR_PARAM_MAX_REPEAT_TIMES - 1
			&& lockedSensor == LOCK_SENSOR_RESET )
		{
			LOCK824_LOG("Not lock (DCI ERROR)\r\n");

			/* 堵转重新上锁 */
			repeatCount++;
			motorError = ERR_NO_ERROR;
			lockStatus = LOCK_STA_LOCKED_REPEAT;
			Lock824_SwitchMotorStatus(MOTOR_STA_LOCKED, MOTOR_PARAM_TURN_TIMEOUT);
		}
		else if (backSensor == LOCK_SENSOR_RESET)
		{
			/* 回拖未到位 */
			motorError = ERR_NO_ERROR;
			Lock824_SwitchMotorStatus(MOTOR_STA_BACK, MOTOR_PARAM_BACK_TIMEOUT);
		}
		else if (backSensor == LOCK_SENSOR_SET)
		{
			/* 回拖已到位 */
			if (lockedSensor == LOCK_SENSOR_SET)
			{
				//上锁成功
				LOCK824_LOG("locked succeed, motorError:%d   lockStatus:%X\r\n", motorError, lockStatus);
                lockStatus = LOCK_STA_LOCKED;
				if (lockReportMsgFlag == SET)
				{
					lockReportMsgFlag = RESET;			
					lockMsg.lock = LOCK_ACTION_LOCKED;
					OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
				}
			}
			else
			{
				//上锁失败
				motorError = ERR_LOCK_ERROR;
				LOCK824_LOG("locked failed, motorError:%d   lockStatus:%X\r\n", motorError, lockStatus);
				if (lockReportMsgFlag == SET)
				{
					lockReportMsgFlag = RESET;
					lockMsg.lock = LOCK_ACTION_ABNORMAL;
					OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
				}
			}
			OSAL_EventDelete(COMP_LOCK, EVENT_MOTOR_MONITOR);
			motorError = ERR_NO_ERROR;
			motorStatus.current = MOTOR_STA_IDLE;
		}
	}
}

/**
  * @brief  处理开锁流程结束
  *         
  * @note   
  */
static void Lock824_MotorUnlockEnd(void)
{
	LockMsg_t lockMsg;
    memset(&lockMsg, 0, sizeof(lockMsg));
	if ( motorError == ERR_UNLOCK_DCI
		&& repeatCount <  MOTOR_PARAM_MAX_REPEAT_TIMES - 1
		&& unlockSensor == LOCK_SENSOR_RESET )
	{
		LOCK824_LOG("Unlock DCI ERROR\r\n");
		
		/* 堵转重新开锁 */
		repeatCount++;
		motorError = ERR_NO_ERROR;
		lockStatus = LOCK_STA_UNLOCK_REPEAT;
		Lock824_SwitchMotorStatus(MOTOR_STA_UNLOCK, MOTOR_PARAM_TURN_TIMEOUT);
	}
    else if (backSensor == LOCK_SENSOR_RESET && (doorSensor == LOCK_SENSOR_RESET || motorError != ERR_NO_ERROR)) 
	{
		/* 门开了或者产生了错误，并且未回拖 */
		LOCK824_LOG("motorError:%d\r\n", motorError);
		Lock824_SwitchMotorStatus(MOTOR_STA_BACK, MOTOR_PARAM_BACK_TIMEOUT);
	}
	else
	{
		if (motorError == ERR_NO_ERROR)
		{
			//开锁成功-回拖成功
			LOCK824_LOG("unlock succeed, motorError:%d   lockStatus:%X\r\n", motorError, lockStatus);
			if (lockReportMsgFlag == SET)
			{
				lockReportMsgFlag = RESET;

				if (backSensor == LOCK_SENSOR_SET || backSensor == LOCK_SENSOR_LONG_SET)
				{
					lockMsg.lock = LOCK_ACTION_UNLOCK_BACK;
					OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
				}
			}
		}
		else
		{
			//开锁失败
			LOCK824_LOG("unlock failed, motorError:%d   lockStatus:%X\r\n", motorError, lockStatus);
			if (lockReportMsgFlag == SET)
			{
				lockReportMsgFlag = RESET;
				lockMsg.lock = LOCK_ACTION_ABNORMAL;
				OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
			}
		}
		//end:
		OSAL_EventDelete(COMP_LOCK, EVENT_MOTOR_MONITOR);
        motorError = ERR_NO_ERROR;
		motorStatus.current = MOTOR_STA_IDLE;
	}
}

/**
  * @brief  电机运行结束处理
  *         
  * @note   
  */
static void Lock824_MotorTurnEnd(void)
{
	if (motorError == ERR_BACK_TIMEOUT || motorError == ERR_BACK_DCI)
	{
		LockMsg_t lockMsg;
		memset(&lockMsg, 0, sizeof(lockMsg));
		/* 产生了回拖错误：锁体异常 */
		LOCK824_LOG("back error, motorError:%d\r\n", motorError);
		if (lockReportMsgFlag == SET)
		{
			lockReportMsgFlag = RESET;
			lockMsg.lock = LOCK_ACTION_ABNORMAL;
			OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
		}
		
		//end:
		OSAL_EventDelete(COMP_LOCK, EVENT_MOTOR_MONITOR);
		motorError = ERR_NO_ERROR; 
		motorStatus.current = MOTOR_STA_IDLE;
	}
	else if ((lockStatus & 0X0F) == LOCK_STA_LOCKED)
	{
		/* 处理上锁流程结束 */
		Lock824_MotorLockedEnd();
	}
	else if ((lockStatus & 0X0F) == LOCK_STA_UNLOCK)
	{
		/* 处理开锁流程结束 */
		Lock824_MotorUnlockEnd();
	}
}

/**
  * @brief  电机运行超时
  *         
  * @note   
  */
static void Lock824_MotorTimeout(void)
{
	switch (motorStatus.current)
	{
		case MOTOR_STA_LOCKED: motorError = ERR_LOCKED_TIMEOUT; break;//上锁超时
		case MOTOR_STA_UNLOCK: motorError = ERR_UNLOCK_TIMEOUT; break;//开锁超时
		case MOTOR_STA_BACK:   motorError = ERR_BACK_TIMEOUT;   break;//回拖超时
		default: break;
	}
	
	if (MOTOR_STA_STOP == motorStatus.current)         
	{
		/* 电机STOP 超时 */
		Lock824_MotorTurnEnd();
	}
	else if (MOTOR_STA_BACK_BRAKE == motorStatus.current)
	{
		/* 回拖刹车超时 */
		Lock824_SwitchMotorStatus(MOTOR_STA_STOP, MOTOR_PARAM_STOP_TIME2);
	}
    else
    {
		/* 上锁超时、开锁超时、回拖超时、上锁过推超时 */
		Lock824_SwitchMotorStatus(MOTOR_STA_STOP, MOTOR_PARAM_STOP_TIME1);
    }
}

/**
  * @brief  检查电机电流
  *         
  * @note   只有以下三种电机状态，才需要检查电流：
  *         MOTOR_STA_LOCKED：上锁状态
  *         MOTOR_STA_UNLOCK：开锁状态
  *         MOTOR_STA_BACK：回拖状态
  */
static void Lock824_MotorCheckDCI(void)
{
	uint16_t adcValue;
	uint8_t overflowCnt = 0;
	
	switch (motorStatus.current)
	{
		case MOTOR_STA_LOCKED: 
		case MOTOR_STA_UNLOCK: 
		case MOTOR_STA_BACK: break;
		default: return;
	}

	do /* 有过流时：就连续获取10次DCI */
	{
		adcValue = (uint16_t)Device_Read(LOCK_DCI_ADC_CHANNEL, NULL, 0, 0);
		overflowCnt++;
	}while (adcValue >= MOTOR_PARAM_MAX_DCI_VALUE && overflowCnt < 10);
    
	if (overflowCnt >= 10)
	{
        uint32_t time = MOTOR_PARAM_STOP_TIME1;
		LockMsg_t lockMsg;
		memset(&lockMsg, 0, sizeof(lockMsg));
		
		/* 连续10次过流：标记错误情况，电机进入STOP状态 */
		if (motorStatus.current == MOTOR_STA_LOCKED)//上锁过流
		{
            if(lockedSensor != LOCK_SENSOR_SET) //主锁舌上锁到位，回拖没到位时上锁锁，如果没有这一句会出现先报门未上锁，再报已关门。
            {
    			motorError = ERR_LOCKED_DCI; 
				lockMsg.lock = LOCK_ACTION_DCI_ERROR;
				OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
            }
        }
        else if (motorStatus.current == MOTOR_STA_UNLOCK)//开锁过流
        {
			motorError = ERR_UNLOCK_DCI;
            time = MOTOR_PARAM_STOP_TIME2;   
        }
        else if (motorStatus.current == MOTOR_STA_BACK)//回拖过流
        {
		    motorError = ERR_BACK_DCI; 
        }
		Lock824_SwitchMotorStatus(MOTOR_STA_STOP, time);
	}
}

/**
  * @brief  电机运行状态监控（电机监控事件）
  *         
  * @note   监控电机电流、超时
  *         电机状态不等于MOTOR_STA_IDLE时：该函数每间隔10ms运行一次
  */
static void Lock824_MotorMonitor(void)
{
    const uint32_t xTicks = OSAL_GetTickCount();
    uint32_t pastTime;
    pastTime = OSAL_PastTime(xTicks, motorStatus.startTime);  
	  
	if (pastTime >= motorStatus.timeout)
	{
		/* 处理电机当前状态超时 */
		Lock824_MotorTimeout();
	}
	else if (pastTime >= MOTOR_PARAM_DCI_DETECT_DELAY_TIME)
	{
		/* 检查电机运行电流（DCI） */
		Lock824_MotorCheckDCI();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
  * @brief  上锁接口函数
  *         
  * @note   
  * @param  status：锁体工作状态
  */
static ErrorStatus Lock824_Locked(LockStatus_enum_t status)
{
	if ( (doorSensor == LOCK_SENSOR_SET || doorSensor == LOCK_SENSOR_LONG_SET)
		&& (motorStatus.current == MOTOR_STA_IDLE) )
	{
		if (lockedSensor == LOCK_SENSOR_SET && backSensor == LOCK_SENSOR_SET)
		{
			motorError = ERR_NO_ERROR;
			lockStatus = LOCK_STA_LOCKED;//已上锁
		}
		else
		{
			repeatCount = 0;
			lockStatus = status;
			OSAL_EventRepeatCreate(COMP_LOCK, EVENT_MOTOR_MONITOR, MOTOR_MONITOR_TIME, EVT_PRIORITY_HIGH);
			Lock824_SwitchMotorStatus(MOTOR_STA_LOCKED, MOTOR_PARAM_TURN_TIMEOUT);
			return SUCCESS;
		}
	}
	return ERROR;
}

/**
  * @brief  开锁接口函数
  *         
  * @note   
  * @param  status：锁体工作状态
  */
static ErrorStatus Lock824_Unlock(LockStatus_enum_t status)
{
	LOCK824_LOG("motorStatus.current is %d\r\n",motorStatus.current);
	if (motorStatus.current == MOTOR_STA_IDLE)
	{
		if (unlockSensor == LOCK_SENSOR_SET)
		{
			motorError = ERR_NO_ERROR;
			lockStatus = LOCK_STA_UNLOCK;//已开锁
		}
		else
		{
            repeatCount = 0;
			lockStatus = status;
			OSAL_EventRepeatCreate(COMP_LOCK, EVENT_MOTOR_MONITOR, MOTOR_MONITOR_TIME, EVT_PRIORITY_HIGH);
			Lock824_SwitchMotorStatus(MOTOR_STA_UNLOCK, MOTOR_PARAM_TURN_TIMEOUT);
			return SUCCESS;
		}
	}
	return ERROR;
}

/**
  * @brief  开锁回拖接口函数
  *         
  * @note   
  */
static ErrorStatus Lock824_UnlockBack(void)
{
	if (motorStatus.current == MOTOR_STA_IDLE)
	{
		if (unlockSensor == LOCK_SENSOR_SET && backSensor == LOCK_SENSOR_RESET)
		{
			lockStatus = LOCK_STA_UNLOCK_BACK;
			OSAL_EventRepeatCreate(COMP_LOCK, EVENT_MOTOR_MONITOR, MOTOR_MONITOR_TIME, EVT_PRIORITY_HIGH);
			Lock824_SwitchMotorStatus(MOTOR_STA_BACK, MOTOR_PARAM_BACK_TIMEOUT);
			return SUCCESS;
		}
	}
	return ERROR;
}

/**
  * @brief  处理门状态改变
  *         
  * @note   
  */
static void Lock824_DoorStatusChange(void)
{
    static FlagStatus powerOnFlag = SET;
	LockMsg_t lockMsg;
	memset(&lockMsg, 0, sizeof(lockMsg));
    
	if (doorSensor == LOCK_SENSOR_LONG_SET)
	{
        if (powerOnFlag == SET)
        {
            powerOnFlag = RESET;
            return;//第一次上电，门关闭不做自动上锁
        }
		lockMsg.door = USER_ACTION_LONG_PRESS;
		OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
	}
	else
	{
		/* 开门、关门：发消息给主任务 */
		lockMsg.door = (doorSensor == LOCK_SENSOR_RESET) ? USER_ACTION_RELEASE :USER_ACTION_PRESS;
		OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
		
		/* 开门处理（正常开门：执行回拖    异常开门：撬锁） */
		if (doorSensor == LOCK_SENSOR_RESET)
		{
			if ( motorStatus.current == MOTOR_STA_IDLE
				&& lockedSensor == LOCK_SENSOR_SET
				&& backSensor == LOCK_SENSOR_SET )
			{
				lockMsg.lock = LOCK_ACTION_FORCE_UNLOCK;		//强行开锁（撬锁）
				OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
				// 前板自己会处理
			}
			else
			{
				// Lock824_UnlockBack();//开门自动回拖-接受前板命令进行回拖
			}
            powerOnFlag = RESET;
		}
	}
}

/**
  * @brief  处理上锁传感器改变
  *         
  * @note   该函数用来判断是否产生机械上锁
  */
static void Lock824_LockedSensorChange(void)
{
    LockMsg_t lockMsg;

	if ( (MOTOR_STA_IDLE == motorStatus.current && lockedSensor == LOCK_SENSOR_SET && backSensor == LOCK_SENSOR_SET)
		&& (doorSensor == LOCK_SENSOR_SET || doorSensor == LOCK_SENSOR_LONG_SET) )
	{
		memset(&lockMsg, 0, sizeof(lockMsg));
		
		/* 发消息给主任务：机械上锁 */
		LOCK824_LOG("Mech locked success\r\n");		
		
		lockMsg.lock = LOCK_ACTION_LOCKED_MACH;		//机械上锁成功
		OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
	}
    else if (MOTOR_STA_IDLE == motorStatus.current && lockedSensor != LOCK_SENSOR_SET)
    {
		memset(&lockMsg, 0, sizeof(lockMsg));
		
		/* 发消息给主任务：锁状态未知 */
		LOCK824_LOG("Lock status unknow\r\n");		
		
		lockMsg.lock = LOCK_ACTION_UNKNOW;
		OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
    }
}

/**
  * @brief  处理开锁传感器改变
  *         
  * @note   该函数用来判断是否产生机械开锁
  */
static void Lock824_UnlockSensorChange(void)
{
	if (unlockSensor == LOCK_SENSOR_SET)
	{
		LockMsg_t lockMsg;
		memset(&lockMsg, 0, sizeof(lockMsg));
        
        if (MOTOR_STA_IDLE == motorStatus.current)
        {
    		/* 发消息给主任务：机械开锁 */
    		LOCK824_LOG("Mech unlock success\r\n");
			lockMsg.lock = LOCK_ACTION_UNLOCK;		//开锁成功
			OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
        }
        else
        {
            if (lockStatus == LOCK_STA_UNLOCK_DOOROPEN)
            {
                /* 上锁过程中门开了，然后自动执行了开锁成功（开锁流程已结束，未回拖） */
                lockMsg.lock = LOCK_ACTION_NO_LOCKED;	
                OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
            }
            else
            {
                /* 开锁成功（开锁流程已结束，未回拖） */
                lockMsg.lock = LOCK_ACTION_UNLOCK_NO_BACK;	
                OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
            }
        }
    }
}

/**
  * @brief  处理传感器状态改变
  *         
  * @note   
  */
static void Lock824_ProcessSensorStatusChange(void)
{
	__EFRAM static SensorStatus_enum_t lastDoorSensor = LOCK_SENSOR_UNKNOW;
	__EFRAM static SensorStatus_enum_t lastLockedSensor = LOCK_SENSOR_UNKNOW;
	__EFRAM static SensorStatus_enum_t lastUnlockSensor = LOCK_SENSOR_UNKNOW;
	
	if (lastLockedSensor != lockedSensor)
	{
		lastLockedSensor = lockedSensor;
		Lock824_LockedSensorChange();
	}
	
	if (lastUnlockSensor != unlockSensor)
	{
		lastUnlockSensor = unlockSensor;
		Lock824_UnlockSensorChange();
	}
	
	if (lastDoorSensor != doorSensor)
	{
		lastDoorSensor = doorSensor;
		Lock824_DoorStatusChange();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
  * @brief  门传感器动作
  * @note   
  *         
  * @param  status：门传感器状态
  *         1：传感器到位
  *         0：未到位
  */
static void Lock824_DoorSensorAction(UserAction_enum_t status)
{
	if (status == USER_ACTION_RELEASE)
	{
		/* 已开门 */
		doorSensor = LOCK_SENSOR_RESET;
	}
	else if (status == USER_ACTION_PRESS)
	{
		/* 已关门 */
		doorSensor = LOCK_SENSOR_SET;
	}
	else if (status == USER_ACTION_LONG_PRESS)
	{
		/* 长时间关门 */
		doorSensor = LOCK_SENSOR_LONG_SET;
	}
}

/**
  * @brief  开锁传感器动作
  * @note   
  *         
  * @param  status：开锁传感器状态
  *         1：传感器到位
  *         0：未到位
  */
static void Lock824_UnlockSensorAction(UserAction_enum_t status)
{
	if (status == INPUT_PORT_STA_PRESS)
	{
		unlockSensor = LOCK_SENSOR_SET;

		if (MOTOR_STA_UNLOCK == motorStatus.current)
		{
			/* 切换电机状态 */
			Lock824_SwitchMotorStatus(MOTOR_STA_STOP, MOTOR_PARAM_STOP_TIME1);
		}
	}
	else if (status == INPUT_PORT_STA_RELEASE)
	{
		unlockSensor = LOCK_SENSOR_RESET;
	}
}

/**
  * @brief  上锁传感器动作
  * @note   
  *         
  * @param  status：上锁传感器状态
  *         1：传感器到位
  *         0：未到位
  */
static void Lock824_LockedSensorAction(UserAction_enum_t status)
{
	if (status == INPUT_PORT_STA_PRESS)
	{
		lockedSensor = LOCK_SENSOR_SET;

		if (MOTOR_STA_LOCKED == motorStatus.current)
		{
			/* 切换电机状态 */
			Lock824_SwitchMotorStatus(MOTOR_STA_LOCKED_MORE_TIME, MOTOR_PARAM_LOCKED_MORE_TIME);
		}
	}
	else if (status == INPUT_PORT_STA_RELEASE)
	{
		lockedSensor = LOCK_SENSOR_RESET;
	}
}

/**
  * @brief  回拖传感器动作
  * @note   
  *         
  * @param  status：回拖传感器状态
  *         1：传感器到位
  *         0：未到位
  */
static void Lock824_BackSensorAction(UserAction_enum_t status)
{
	if (status == INPUT_PORT_STA_PRESS)
	{
		backSensor = LOCK_SENSOR_SET;

		if (MOTOR_STA_BACK == motorStatus.current)
		{
			/* 切换电机状态 */
			Lock824_SwitchMotorStatus(MOTOR_STA_BACK_BRAKE, MOTOR_PARAM_BRAKE_TIME);
		}
	}
	else if (status == INPUT_PORT_STA_RELEASE)
	{
		backSensor = LOCK_SENSOR_RESET;
	}
}

static void Lock824_ProcessISR(void)
{
   	LockSensorAction_t sensor;

	isrPendFlag++;
	while (OSAL_RingBufferRead(rbHandle, &sensor) == SUCCESS)
	{
		// LOCK824_LOG("%s: action:%d\r\n", sensor.name, sensor.action);

		/* 保存传感器状态、切换电机状态 */
		if (strcmp(sensor.name, "L_DOOR") == 0)
		{
			// printf("\r\nL_lock\r\n");
			Lock824_DoorSensorAction(sensor.action);
		}
		else if (strcmp(sensor.name, "L_UNLOCK") == 0)
		{
			// printf("\r\nL_unlock\r\n");
			Lock824_UnlockSensorAction(sensor.action);
		}
		else if (strcmp(sensor.name, "L_LOCKED") == 0)
		{
			// printf("\r\nL_locked\r\n");
			Lock824_LockedSensorAction(sensor.action);
		}
		else if (strcmp(sensor.name, "L_BACK") == 0)
		{
			// printf("\r\nL_back\r\n");
			Lock824_BackSensorAction(sensor.action);
		}
	}
	isrPendFlag--;
}

/**
  * @brief  锁体传感器中断 回调
  *         
  * @note   该函数以回调形式，在底层的中断函数里面调用
  */
static void Lock824_SensorStatusChange(char *name, uint8_t status, uint8_t times)
{
	LockSensorAction_t sensor;

	/* 锁体传感器动作暂存缓存区 */
	sensor.name = name;
	sensor.action = (UserAction_enum_t)status;
	OSAL_RingBufferWrite(rbHandle, &sensor);

    /* 产生ISR事件 */
    OSAL_EventCreateFromISR(COMP_LOCK);
    // printf("\r\nlock-action:%d,name:%s,times:%d\r\n",status,name,times);
 
    if (isrPendFlag == 0)
    {
        Lock824_ProcessISR();
    }
}

/**
  * @brief  锁体任务 处理用户上锁消息
  * @note   
  *         
  * @param  pMsg：消息指针
  */
static void Lock824_ProcessLockedMsg(void)
{
	LockMsg_t lockMsg;
	memset(&lockMsg, 0, sizeof(lockMsg));
	if ( Lock824_Locked(LOCK_STA_LOCKED_USER) == SUCCESS)
	{
		/* 电机正在上锁 */		
		LOCK824_LOG("User start locked\r\n");
    }
	else
	{
		LOCK824_LOG("lock fail, doorSensor:%d, motorStatus.current:%d\r\n", doorSensor, motorStatus.current);	
		/* 启动上锁失败：门未关、锁体正忙、已上锁 */
		if (doorSensor == LOCK_SENSOR_RESET)			//门没关(门未上锁)
		{
			lockMsg.lock = LOCK_ACTION_NO_LOCKED;		//门未上锁
			OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
		}
		else if (motorStatus.current != MOTOR_STA_IDLE)
		{
			lockMsg.lock = LOCK_ACTION_BUSY;			//锁体正忙
			OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
		}
		else
		{
			lockMsg.lock = LOCK_ACTION_LOCKED;
			OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
		}
    }
}

/**
  * @brief  锁体任务 处理用户开锁消息
  * @note   
  *         
  * @param  pMsg：消息指针
  */
static void Lock824_ProcessUnlockMsg(void)
{
	LockMsg_t lockMsg;
	memset(&lockMsg, 0, sizeof(lockMsg));
	if ( Lock824_Unlock(LOCK_STA_UNLOCK_USER) == SUCCESS)
	{
		/* 电机正在开锁 */
		LOCK824_LOG("User start unlock\r\n");
	}
	else
	{
		LOCK824_LOG("unlock fail\r\n");	
		/* 启动开锁失败：锁体正忙、已开锁 */
		if (motorStatus.current != MOTOR_STA_IDLE)			/* 锁体正忙 */
		{
			lockMsg.lock = LOCK_ACTION_BUSY;				//锁体正忙
			OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
		}
		else
		{
			if (backSensor == LOCK_SENSOR_SET || backSensor == LOCK_SENSOR_LONG_SET)
			{
				lockMsg.lock = LOCK_ACTION_UNLOCK_BACK;		//开锁成功（开锁流程已结束，已回拖）
				OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
			}
			else
			{
				lockMsg.lock = LOCK_ACTION_UNLOCK_NO_BACK;	//开锁成功（开锁流程已结束，未回拖）
				OSAL_MessagePublish(&lockMsg, sizeof(lockMsg));
			}
			
		}
	}
}

//取反锁体状态（如果门开着，总是执行开锁）
static void Lock824_ProcessInverseMsg(void)
{
	if (doorSensor == LOCK_SENSOR_RESET || lockStatus == LOCK_STA_LOCKED)
	{
		Lock824_ProcessUnlockMsg();
	}
	else
	{
		Lock824_ProcessLockedMsg();
	}
}

//开锁回拖
static void Lock824_ProcessRebackMsg()
{
    LOCK824_LOG("Lock824_UnlockBack\r\n");
	Lock824_UnlockBack();
}

/* Lock组件API函数类型 */
static ErrorStatus Lock_Ctrl_Sub(LockCtrl_enum_t ctrl)
{
	lockReportMsgFlag = SET;
    switch((uint8_t)ctrl)
    {
        case LOCK_CTRL_LOCKED:
			Lock824_ProcessLockedMsg();
            break;
        case LOCK_CTRL_UNLOCK:
			Lock824_ProcessUnlockMsg();
            break;
		case LOCK_CTRL_INVERSE:
			Lock824_ProcessInverseMsg();
            break; 
		case LOCK_CTRL_UNLOCK_BACK:
			Lock824_ProcessRebackMsg();
            break;
        default:
            break;
    }
    return SUCCESS;
}

static void Lock824_ProcessMbox(uint8_t *msg)
{
    switch (msg[0])
    {
		case LOCK_MBOX_CTRL:
		{
			uint8_t ctrl;
			ctrl = msg[1];
			Lock_Ctrl_Sub((LockCtrl_enum_t)ctrl);
		}
			break;
		default:
			
			break;
    }
}

/**
  * @brief  锁体启动
  *         
  * @note   
  */
static void Lock824_Start(void)
{
	if (rbHandle == NULL)
    {
		/* 创建环形缓存（暂存传感器状态） */
        rbHandle = OSAL_RingBufferCreate(LOCK_RING_BUFFER_LEN, sizeof(LockSensorAction_t));
		/* 注册锁体传感器端口 */
		InputPort_stu_t sensor_list[] = {
        #ifdef FM15L0XX
			{"L_DOOR",		vPIN_B25, 	INPUT_PORT_LOGIC_HIGH, 	INPUT_PORT_FUNC_SPECIAL},//LOCK_SLOT -- 叉舌感应（特殊扫描方式、复旦微用到了）
        #else
			{"L_DOOR",		vPIN_B25, 	INPUT_PORT_LOGIC_HIGH, 	INPUT_PORT_FUNC_SINGLE},//LOCK_SLOT -- 叉舌感应（中断方式、昂瑞微用到了）
        #endif
			{"L_UNLOCK",	vPIN_B26, 	INPUT_PORT_LOGIC_LOW, 	INPUT_PORT_FUNC_SINGLE}, //LOCK_OP2  -- 主锁舌缩进感应(开锁到位感应)
			{"L_LOCKED",	vPIN_B27, 	INPUT_PORT_LOGIC_LOW, 	INPUT_PORT_FUNC_SINGLE}, //LOCK_OP3  -- 主锁舌伸出感应(上锁到位感应)
			{"L_BACK",  	vPIN_B28, 	INPUT_PORT_LOGIC_LOW, 	INPUT_PORT_FUNC_SINGLE}, //LOCK_OP1  -- 回拖感应
		};
		InputPort_Registered(sensor_list, OSAL_LENGTH(sensor_list), Lock824_SensorStatusChange);
	}
    InputPort_EnableProt("L_DOOR");
    InputPort_EnableProt("L_UNLOCK");
    InputPort_EnableProt("L_LOCKED");
    InputPort_EnableProt("L_BACK");
}

/**
  * @brief  锁体任务函数
  *
  * @note   1.任务函数内不能写阻塞代码
  *         2.任务函数每次运行只处理一个事件
  *         3.任务函数必须添加到osal_config.h文件里面
  *         
  * @param  event：当前任务的所有事件
  *
  * @return 返回未处理的事件
  */
static uint32_t Lock824_Task(uint32_t event)
{
	/* 系统启动事件 */
	if (event & EVENT_SYS_START)
	{
		LOCK824_LOG("Lock task start\r\n");	
        Device_Enable(vADC_1);
        Device_Enable(vPWM_1);
		Device_Enable(vPWM_2);
        Device_Enable(vPIN_B25);
		//add by liujia:此处电源的1代表开启，并不能代表电平，需要去对应的底层查看时候有无电平置反的操作
		Device_Write(vPIN_C0,NULL,0,1);
		Device_Write(vPIN_C2,NULL,0,1);
		Lock824_Start();
		OSAL_EventSingleCreate(COMP_LOCK, EVENT_SYS_ISR, 0, EVT_PRIORITY_HIGH);    
        return ( event ^ EVENT_SYS_START );
	}
    
	if (event & EVENT_SYS_MBOX)
    {
        uint8_t buffer[30] = {0};
        while (OSAL_MboxAccept(buffer))
        {
            Lock824_ProcessMbox(buffer);
        }
        return ( event ^ EVENT_SYS_MBOX );
    }
	
    /* 系统休眠事件 */
	if (event & EVENT_SYS_SLEEP)
	{
		LOCK824_LOG("lock enter to sleep\r\n");

        InputPort_DisableProt("L_LOCKED");
        InputPort_DisableProt("L_BACK");
		Device_Write(vPIN_C0,NULL,0,0);
		Device_Write(vPIN_C2,NULL,0,0);
        Device_Disable(vADC_1);
        Device_Disable(vPWM_1);
		Device_Disable(vPWM_2);
        Device_Disable(vPIN_B25);
		//休眠前获取SLOT状态
		slot_status = Device_Read(vPIN_B25,NULL,0,0);
		op2_status = Device_Read(vPIN_B26,NULL,0,0);
		return ( event ^ EVENT_SYS_SLEEP );
    }

	/* 锁体中断事件 */
	if (event & EVENT_SYS_ISR)
	{
        Lock824_ProcessISR();
		Lock824_ProcessSensorStatusChange();
		return ( event ^ EVENT_SYS_ISR );
	}
	
	/* 电机监控事件 */
	if (event & EVENT_MOTOR_MONITOR)
	{
		Lock824_MotorMonitor();
        if (motorStatus.current != MOTOR_STA_IDLE)//非空闲状态下不允许休眠
        {
            OSAL_SetTaskStatus(TASK_STA_ACTIVE);
        }
        else
        {
            OSAL_SetTaskStatus(TASK_STA_NORMAL);
        }
		return ( event ^ EVENT_MOTOR_MONITOR );
	}
	return 0;
}

COMPONENT_TASK_EXPORT(COMP_LOCK, Lock824_Task, 4);

/**
 * @brief  Lcok定时唤醒回调
 *
 * @param  mode：模式
 * @return 
 */
static int32_t Lock_TimedWakeupHandle(uint32_t dev)
{
    /* RAM数据未丢失，inputport会唤醒系统，这里wakehandle不用做任何处理 */
    if (lock_ram_flag == UINT32_FLAG)
    {
        return -1;
    }

	uint8_t result = 0;
	if(slot_status == Device_Read(vPIN_B25,NULL,0,0))
	{
		// printf("slot is same\r\n");
	}
	else
	{
		// printf("slot is different\r\n");
		result = 1;
	}

	if(op2_status == Device_Read(vPIN_B26,NULL,0,0))
	{
		// printf("op2 is same\r\n");
	}
	else
	{
		// printf("op2 is different\r\n");
		result = 1;
	}
	return result;
}
COMPONENT_WAKEUP_EXPORT(COMP_LOCK, Lock_TimedWakeupHandle, vTIMER_1); 

/**
  * @brief  BUTTON唤醒逻辑处理
  * @param  
  * @return 
  */
static int32_t Lock824_WakeHandle(uint32_t dev)
{
    /* RAM数据未丢失，b会唤醒系统，这里wakehandle不用做任何处理 */
    if (lock_ram_flag == UINT32_FLAG)
    {
        return -1;
    }

	return 1;
}
COMPONENT_WAKEUP_EXPORT(COMP_LOCK, Lock824_WakeHandle, vPIN_B26);