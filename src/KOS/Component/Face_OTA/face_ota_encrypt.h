#ifndef __FACE_OTA_ENCRYPT_H
#define __FACE_OTA_ENCRYPT_H

#include "face_ota.h"

#define AES_DEBUG

#define RANDOM_SIZE 4
#define FACE_KEY_SIZE 16 //人脸AES密钥长度

#define PROTOCOL_MID_OFFSET 2
#define PROTOCOL_HEADER_SIZE 6
#define MAX_SEND_SIZE 4000

extern const uint8_t faceOTA_KeyIndex[FACE_KEY_SIZE];

void FaceOTAEncrypt_GenerateRandomNumber(uint8_t *pRandom);
void FaceOTAEncrypt_GenencKey(uint8_t *pRandom);
void FaceOTAEncrypt_Set_EnFlag(uint8_t flag);
void FaceOTAEncrypt_SenseTime_Encrypt_Decrypt(uint8_t EncryptDirMode, uint8_t *input, const uint16_t *inputLen, uint8_t *result, uint16_t *resultLen);
uint8_t FaceOTAEncrypt_Get_EnFlag(void);
#endif
