#include "component.h"
#include "device.h"

#define EXTEND_MIJIA_OTA_LOG(format, ...)   OSAL_LOG(C_CYAN "[Extend_MiJia_OTA.c] " format C_NONE, ##__VA_ARGS__)

#define MAX_PACKAGE_LEN 2048 //最大包长

/* 定义虚拟硬件接口 */
#define EXTEND_MIJIA_VHWL       vUART_4

#define EXTEND_INT_MCU_VHWL     vPIN_I20

/* UART事件处理周期 */
#define EXTEND_MIJIA_EVT_PROC_CYCLE (10)

/* UART事件 */
#define EVENT_EXTEND_MIJIA (1 << 0)
#define EVENT_EXTEND_MIJIA_SEND_REQ (1 << 1)

/* EXTEND_MIJIA环形接收缓存 */
#ifndef EXTEND_MIJIA_RB_RXBUF_LENGTH
#define EXTEND_MIJIA_RB_RXBUF_LENGTH (128) // EXTEND_MIJIA 接收环形缓存长度
#endif

///////////// 定义协议解析层数据结构 /////////////////////////////////////////////////////////////////////////////////////////
#define PROTOCOL_SEND_TIMES 3           //锁发送命令包的最大次数
#define PROTOCOL_RESEND_CMD_TIMEOUT 200 //锁重发命令包间隔时间ms0
#define PROTOCOL_SEND_ACK_TIME_GAP 20   //锁发出多包ACK的间隔时间ms
///////////// 串口通信协议：数据包结构 /////////////////////////////////////////////////////////////////////////////////////////
#pragma pack(1)


/*米家协议结构*/
typedef struct
{
    uint8_t stx[3]; //协议头
    uint8_t cmd;    //命令字
    uint16_t len;    //数据长度
    uint8_t data[]; //数据 + 1位校验和
} ProtocolMi_stu_t;

#pragma pack()

// tx命令包结构
typedef struct
{
    uint8_t buffer[MAX_PACKAGE_LEN]; //发送缓存
    uint16_t len;                    //数据长度
    uint32_t acktimeout;             //等待ACK的超时时间（为0：不需要ACK）
    ExtMi_send_cb_fun_t cb;          //发送命令回调（为NULL：不需要回调）
} uart_txcmd_packet_stu_t;

/* 命令包发送状态信息 */
static struct
{
    uart_txcmd_packet_stu_t data; //数据
    uint32_t timeStamp;           //最后一次发送的时间戳
    uint8_t cnt;                  //发送次数（可能重发）
    enum
    {                    //当前发送的状态
        TX_STA_IDLE,     //空闲
        TX_STA_WAIT_ACK, //等待模块回复应答包
    } status;
    RingBufferHandle_t tbHandle;
} CmdPacketTxInfo;

/* 命令包接收状态信息：记录蓝牙和Uart两个设备的发送状态 */
static struct
{
    uint8_t protocolBuf[MAX_PACKAGE_LEN]; //协议解析缓存
    uint8_t protocolCount;                //协议解析计数变量
    RingBufferHandle_t rbHandle;
    enum
    {
        RX_STA_IDLE,     //空闲
        RX_STA_SEND_ACK, //正在发送多包ACK
    } status;
} CmdPacketRxInfo;

/* 定义发送缓存 */
static uint8_t uart_tx_buffer[MAX_PACKAGE_LEN] = {0};
static uint16_t tx_ctrl_flg = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
  * @brief  发送数据给应用层
  * @param  psn：文件分包编号
  *         psn为FFFD：表示数据域为文件名和文件长度（ASCII）
  *         psn为FFFF：表示文件传输结束（正常结束）
  *         psn为FFFE：表示文件传输结束（ymodem出错）
  *         其他值：正常的数据包编号
  * 
  * @param  data：数据内容
  * @param  size：数据包长度
  */
static void Extend_MIjiaOta_PublishToApp(uint16_t psn, uint8_t *data, uint16_t size)
{
    Extend_MIjiaOta_Msg_stu_t *Extend_MijiaOta_msg = NULL;
    Extend_MijiaOta_msg = (Extend_MIjiaOta_Msg_stu_t *)OSAL_Malloc(sizeof(Extend_MIjiaOta_Msg_stu_t) + size);
    if (Extend_MijiaOta_msg != NULL)
    {
        Extend_MijiaOta_msg->psn = psn;
        Extend_MijiaOta_msg->size = size;
        memcpy(Extend_MijiaOta_msg->data, data, size);
        OSAL_MessagePublish(Extend_MijiaOta_msg, sizeof(Extend_MIjiaOta_Msg_stu_t) + size);
        OSAL_Free(Extend_MijiaOta_msg);
    }
}
   




/**
 * @brief  打印HEX数据，最多只能打印32字节
 * @note
 *
 * @param  pData：数据指针
 * @param  len：数据长度
 */
static void ExtMi_PrintfHexData(char *Msg, uint8_t *pData, uint16_t len)
{
    char *str_buffer = OSAL_Malloc(len * 3 + 1);
    if (str_buffer != NULL)
    {
        memset(str_buffer, 0, len * 3 + 1);
        EXTEND_MIJIA_OTA_LOG("%s:%d(Bytes)\r\n", Msg, len);
        if (str_buffer != NULL)
        {
            for (int i = 0; i < len; i++)
            {
                sprintf(&str_buffer[i * 3], "%02X ", pData[i]);
            }
            EXTEND_MIJIA_OTA_LOG("%s\r\n\r\n", str_buffer);
        }
    }
    if (str_buffer)
        OSAL_Free(str_buffer);
}

/**
 * @brief  计算校验和
 * @note
 *
 * @param  pData：数据指针
 * @param  len：数据长度
 * @return 成功返回校验和
 */
static uint8_t ExtMi_CalcCheckSum(uint8_t *pData, uint16_t len)
{
    uint8_t sum = 0;

    for (; len; len--)
    {
        sum += *pData++;
    }
    return sum;
}

/**
 * @brief  获取发送缓存区的状态
 * @note
 *
 * @return 缓存区为空返回0
 */
static uint8_t ExtMi_GetTxBufferStatus(void)
{
    return tx_ctrl_flg;
}

/**
 * @brief  加载待发送的数据
 * @note   将要发出的数据，填入发送缓存区
 *
 * @param  pData：数据指针
 * @param  len：数据长度
 * @return 成功返回SUCCESS，失败返回ERROR
 */
static ErrorStatus ExtMi_LoadingDataToBeSent(uint8_t *pData, uint16_t len)
{
    uint8_t *pSendBuf = uart_tx_buffer;
    EXTEND_MIJIA_OTA_LOG("uart_tx_buffer[3]:%02X\r\n", pSendBuf[3]);
    if (tx_ctrl_flg == 0)
    {
        tx_ctrl_flg = len;
        memcpy(pSendBuf, pData, len);
        return SUCCESS;
    }
    EXTEND_MIJIA_OTA_LOG("Loading Tx data failed\r\n");
    return ERROR;
}

/**
 * @brief  处理发送缓存
 * @note   将发送缓存区的数据，通过串口发出，并处理Int控制逻辑
 *
 */
static void ExtMi_ProcessTxBuffer(void)
{
    uint8_t *pSendBuf = uart_tx_buffer;

    if (tx_ctrl_flg > 0)
    {

        if (tx_ctrl_flg & 0X8000)
        {
            /* 调用硬件层write接口，发出数据 */
            ExtMi_PrintfHexData("Uart send packet", pSendBuf, tx_ctrl_flg & 0X7FFF);
            Device_Write(EXTEND_MIJIA_VHWL, pSendBuf, tx_ctrl_flg & 0X7FFF, 0);

            tx_ctrl_flg = 0;
        }
        else
        {
            tx_ctrl_flg |= 0X8000;
        }
    }
}


static ErrorStatus ExtMi_SendAck(uint8_t cmd, void *data, uint16_t len)
{
    uint16_t checksum_len = 0;
    ErrorStatus res = ERROR;
    ProtocolMi_stu_t *sdata = (ProtocolMi_stu_t *)OSAL_Malloc(sizeof(ProtocolMi_stu_t) + len+1);

    sdata->stx[0] = 0X4E;
    sdata->stx[1] = 0X54;
    sdata->stx[2] = 0X46;

    sdata->len = len;
    sdata->cmd = cmd;
    if(data != NULL)
    {
        memcpy(sdata->data, data, len);
    }
    sdata->data[sdata->len] = ExtMi_CalcCheckSum(&sdata->cmd, sdata->len + 3);

    EXTEND_MIJIA_OTA_LOG("start send ack...%d\r\n",sizeof(ProtocolMi_stu_t));

    CmdPacketRxInfo.status = RX_STA_IDLE;
    
    if (Device_Write(EXTEND_MIJIA_VHWL, sdata, sizeof(ProtocolMi_stu_t) + len+1, 0) == 0)
    {
        res = SUCCESS;
        ExtMi_PrintfHexData("ExtMi Send Ack", (uint8_t *)sdata, sizeof(ProtocolMi_stu_t) + len+1);
    }

    if (sdata)
        OSAL_Free(sdata);

    return res;
}

uint8_t ExtMi_rx_ota_pack(uint8_t *pdata,uint16_t len)
{
    uint16_t pack_id;
    
    pack_id = pdata[0]*256+pdata[1];
    EXTEND_MIJIA_OTA_LOG("pack id [%d]\r\n",pack_id);
    Extend_MIjiaOta_PublishToApp(pack_id,&pdata[2],len-2);

}

/**
 * @brief  处理接收的命令包
 * @note
 *
 * @param  pCmdPacket：数据包指针
 */
static void ExtMi_ProcessRxCmdPacket(ProtocolMi_stu_t *pCmdPacket)
{
    uint16_t len = pCmdPacket->len;
    static uint8_t last_len = 0; //用来保存上一次的长度，比如收到0xB0没有进行ACK，下次收到更长的指令就内存溢出了
    EXTEND_MIJIA_OTA_LOG("Recv packet, cmd: 0x%02X\r\n", pCmdPacket->cmd);
    //ExtMi_PrintfHexData("rev data", (uint8_t *)pCmdPacket, len + 6);
    static uint8_t ack_data[3] = {0x6F,0x6B,0x0d};

    if (CmdPacketTxInfo.status == TX_STA_WAIT_ACK && (pCmdPacket->cmd == CmdPacketTxInfo.data.buffer[3]))
    {
        CmdPacketTxInfo.status = TX_STA_IDLE;
        //ExtMi_Send_OtaReq_cb(pCmdPacket->cmd,)
    }
    else
    {
        switch(pCmdPacket->cmd)
        {
            case 0x67:  ExtMi_rx_ota_pack(pCmdPacket->data,len);
                        ExtMi_SendAck(pCmdPacket->cmd,ack_data,3);
                break;
            case 0x65:  Extend_MIjiaOta_PublishToApp(0xFFFF,NULL, 0);
                        ExtMi_SendAck(pCmdPacket->cmd,ack_data,3);
                break;
            default:
                ExtMi_SendAck(pCmdPacket->cmd,NULL,0);
                break;
        }

    }

}

/**
 * @brief  解析接收的数据包
 * @note
 *
 * @param  pData：数据包地址
 */
static void ExtMi_ParseRxPacket(uint8_t *pData)
{
    if (pData == NULL)
    {
        return;
    }

    ProtocolMi_stu_t *pPacke = (ProtocolMi_stu_t *)pData;
    if (pPacke->stx[0] == 0x43 && pPacke->stx[1] == 0x4D && pPacke->stx[2] == 0x44)
    {
        ExtMi_ProcessRxCmdPacket(pPacke);
    }
}

/**
 * @brief  从底层环形缓存获取一包数据并解析
 *
 * @note   底层驱动收到数据，会暂存在环形缓存区
 *
 * @return 返回解析后的数据包地址，解析失败返回NULL
 */
static uint8_t *ExtMi_GetOnePacket(void)
{
    static uint8_t getPacketTimeoutCnt = 0;
    static uint8_t protocolBuff[MAX_PACKAGE_LEN * 2];
    static uint16_t cnt = 0;
    ProtocolMi_stu_t *ProtocolMi;
    uint16_t i;
    uint8_t checksum;
    uint8_t tmpData = 0;
    uint16_t tmpLen = 0;

    tmpLen = OSAL_RingBufferGetValidSize(CmdPacketRxInfo.rbHandle);
    if (tmpLen == 0)
    {
        /* 连续3次没有读取到数据（30ms） */
        if (++getPacketTimeoutCnt >= 3)
        {
            getPacketTimeoutCnt = 0;
            cnt = 0; //清0解析计数
        }
        return NULL;
    }
    getPacketTimeoutCnt = 0;

    for (; tmpLen; tmpLen--)
    {
        OSAL_RingBufferRead(CmdPacketRxInfo.rbHandle, &tmpData);
        protocolBuff[cnt++] = tmpData;
    }

    if (cnt < 6)
    {
        return NULL;
    }

    for (i = 0; i < cnt - 5; i++)
    {
        if (cnt - i < 6)
        {
            return NULL;
        }
        if (protocolBuff[i] == 0x43 && protocolBuff[i + 1] == 0x4D && protocolBuff[i + 2] == 0x44)
        {
            ProtocolMi = (ProtocolMi_stu_t *)&protocolBuff[i];
            if (cnt - i < ProtocolMi->len + 6)
            {
                return NULL;
            }
            

            EXTEND_MIJIA_OTA_LOG("checksum start %x ,%x\r\n",protocolBuff[i + 3],ProtocolMi->len + 3);

            checksum = ExtMi_CalcCheckSum(&protocolBuff[i + 3], ProtocolMi->len + 3);
            EXTEND_MIJIA_OTA_LOG("checksum %x ,%x\r\n",checksum,ProtocolMi->data[ProtocolMi->len]);
            if (checksum == ProtocolMi->data[ProtocolMi->len])
            {
                memcpy(CmdPacketRxInfo.protocolBuf, &protocolBuff[i], ProtocolMi->len + 6);
                CmdPacketRxInfo.protocolCount = ProtocolMi->len + 6;

                cnt -= (i + ProtocolMi->len + 6);
                memmove(protocolBuff, protocolBuff[i + ProtocolMi->len + 6], cnt);

                return CmdPacketRxInfo.protocolBuf;
            }
            else
            {
                EXTEND_MIJIA_OTA_LOG("checksum  ERR...\r\n");
            }
        }
    }
    cnt = 0;
    return NULL;
}


/**
  * @brief  重发命令包，并处理超时逻辑
  *
  * @note   MCU发出命令包后，超时没有收到确认包，就重发
  */
static void ExtKds_ResendCmdPacket(void)
{
	uint8_t send_timeout_msg = 0;
	uint32_t currentTime = OSAL_GetTickCount();

	if (CmdPacketTxInfo.status == TX_STA_WAIT_ACK)//等待应答包
	{
        if (OSAL_PastTime(currentTime, CmdPacketTxInfo.timeStamp) > CmdPacketTxInfo.data.acktimeout)//ERR:等待应答包超时
		{
            EXTEND_MIJIA_OTA_LOG("wait ack timeout\r\n");
			send_timeout_msg = 1;
			CmdPacketTxInfo.status = TX_STA_IDLE;

		}
	}

	/* exec timeout callback */
    if(send_timeout_msg)
    {
        if(CmdPacketTxInfo.data.cb != NULL)
        {
            uint8_t cmd = ((ProtocolMi_stu_t *)CmdPacketTxInfo.data.buffer)->cmd;
            EXTEND_MIJIA_OTA_LOG("cmd=[%d]\r\n",cmd);
            CmdPacketTxInfo.data.cb(cmd,NULL,0);
        }
        
    }
}


/**
 * @brief  Uart任务 通信协议处理
 *
 * @note   该函数会每间隔10ms执行一次
 */
static void ExtMi_ProtocolPro(void)
{
    if (ExtMi_GetTxBufferStatus() != 0)
    {
        ExtMi_ProcessTxBuffer(); //处理发送缓存中的数据
    }
    else
    {
        ExtMi_ParseRxPacket(ExtMi_GetOnePacket()); //解析数据
        ExtKds_ResendCmdPacket();
    }
}

/**
 * @brief  判断协议层是否为空闲状态
 * @note
 *
 * @return 空闲：返回SUCCESS， 正忙：返回ERROR
 */
static ErrorStatus ExtMi_IsProtocolIdle(void)
{
    /* 当前没有发出命令、发送缓存为空、接收的命令也处理完成了，才能发出新命令 */
    if (ExtMi_GetTxBufferStatus() == 0 && CmdPacketRxInfo.status == RX_STA_IDLE && CmdPacketTxInfo.status == RX_STA_IDLE)
    {
        return SUCCESS;
    }
    return ERROR;
}

/**
 * @brief  Uart任务 协议层初始化
 *
 * @note
 */
static void ExtMi_ProtocolInit(void)
{
    CmdPacketTxInfo.status = TX_STA_IDLE;
    CmdPacketTxInfo.cnt = 0;
    CmdPacketRxInfo.status = RX_STA_IDLE;
    memset(CmdPacketRxInfo.protocolBuf, 0, sizeof(CmdPacketRxInfo.protocolBuf));
    CmdPacketRxInfo.protocolCount = 0;
}





//处理TX队列
static void ExtMi_ProcessTxQueue(void)
{
    uart_txcmd_packet_stu_t txcmd_packet = {0};

    if (ExtMi_IsProtocolIdle() != SUCCESS)
    {
        return;
    }
    if (OSAL_RingBufferRead(CmdPacketTxInfo.tbHandle, &txcmd_packet) != SUCCESS)
    {
        return;
    }
    CmdPacketTxInfo.data = txcmd_packet;
    CmdPacketTxInfo.timeStamp = OSAL_GetTickCount();
    CmdPacketTxInfo.cnt = 1;
    CmdPacketTxInfo.status = TX_STA_WAIT_ACK;
    ExtMi_LoadingDataToBeSent(CmdPacketTxInfo.data.buffer, CmdPacketTxInfo.data.len);
}

/**
 * @brief  串口接收回调
 *
 * @return
 */
static void ExtMi_Receivecb(VirtualHardware_enum_t dev, void *data, uint32_t len)
{
    uint32_t i = 0;
    uint8_t *p = (uint8_t *)data;

    /* 收到的数据写入缓存 */
    for (i = 0; i < len; i++)
    {
        OSAL_RingBufferWrite(CmdPacketRxInfo.rbHandle, p + i);
    }
}


void ExtMi_Send_Data(uint8_t cmd , uint8_t *data , uint16_t len , ExtMi_send_cb_fun_t cb)
{
    uart_txcmd_packet_stu_t *send_packer = NULL;
    ProtocolMi_stu_t *sdata;

    send_packer = (uart_txcmd_packet_stu_t *)OSAL_Malloc(sizeof(uart_txcmd_packet_stu_t));
    sdata = (ProtocolMi_stu_t *)send_packer->buffer;
    sdata->stx[0] = 0X4E;
    sdata->stx[1] = 0X54;
    sdata->stx[2] = 0X46;

    sdata->len = len;
    sdata->cmd = cmd;
    if(data != NULL)
    {
        memcpy(sdata->data, data, len);
    }
    
    sdata->data[sdata->len] = ExtMi_CalcCheckSum(&sdata->cmd, sdata->len + 2);

    send_packer->len = sizeof(ProtocolMi_stu_t) + len + 1;
    send_packer->cb = cb;
    if(cmd == 0x66)
    {
        send_packer->acktimeout = 1500;
    }
    else
    {
        send_packer->acktimeout = PROTOCOL_RESEND_CMD_TIMEOUT;
    }
    

    if (CmdPacketTxInfo.tbHandle != NULL)
    {
        if (OSAL_RingBufferWrite(CmdPacketTxInfo.tbHandle, send_packer) == SUCCESS)
        {
            OSAL_Free(send_packer);
        }
    }
}

void ExtMi_Send_OtaReq_cb(uint8_t cmd, void *data, uint16_t len)
{
    if(cmd == 0x66 && len==0)
    {
        
       ExtMi_Send_Data(0x66,NULL,0,ExtMi_Send_OtaReq_cb);
    }
}

/**
 * @brief  UART任务函数
 *
 * @note   1.任务函数内不能写阻塞代码
 *         2.任务函数每次运行只处理一个事件
 *
 * @param  event：当前任务的所有事件
 *
 * @return 返回未处理的事件
 */
static uint32_t ExtMi_Ota_Task(uint32_t event)
{
    static uint8_t uart_init_flag;
    /* 系统启动事件 */
    if (event & EVENT_SYS_START)
    {
        // Device_Write(EXTEND_INT_RF_VHWL, NULL, 0, 0);
        if (uart_init_flag == 0)
        {
            /* 创建RX环形缓存 */
            CmdPacketRxInfo.rbHandle = OSAL_RingBufferCreate(MAX_PACKAGE_LEN, 1);
            /* 创建TX环形缓存 */
            CmdPacketTxInfo.tbHandle = OSAL_RingBufferCreate(5, sizeof(uart_txcmd_packet_stu_t));
            /* 注册串口接收回调 */
            Device_RegisteredCB(EXTEND_MIJIA_VHWL, ExtMi_Receivecb);
            /* 初始化UART协议层 */
            ExtMi_ProtocolInit();
            uart_init_flag = 1;
        }
        OSAL_EventRepeatCreate(COMP_EXTEND_MIJIA_OTA, EVENT_EXTEND_MIJIA, EXTEND_MIJIA_EVT_PROC_CYCLE, EVT_PRIORITY_MEDIUM);

        // OSAL_EventRepeatCreate(COMP_EXTEND_MIJIA_OTA, EVENT_EXTEND_MIJIA_SEND_REQ, 200, EVT_PRIORITY_MEDIUM);

        EXTEND_MIJIA_OTA_LOG("ExtMi_Task start\r\n");
        Device_Enable(EXTEND_MIJIA_VHWL);

        ExtMi_Send_Data(0x66,NULL,0,ExtMi_Send_OtaReq_cb);


        return (event ^ EVENT_SYS_START);
    }

    /* 串口处理事件 */
    if (event & EVENT_EXTEND_MIJIA)
    {
        ExtMi_ProtocolPro();
        ExtMi_ProcessTxQueue();
        return (event ^ EVENT_EXTEND_MIJIA);
    }

    // /* 串口处理事件 */
    // if (event & EVENT_EXTEND_MIJIA_SEND_REQ)
    // {
        
    //     return (event ^ EVENT_EXTEND_MIJIA_SEND_REQ);
    // }


    /* 系统休眠事件 */
    if (event & EVENT_SYS_SLEEP)
    {
        return (event ^ EVENT_SYS_SLEEP);
    }
    return 0;
}
COMPONENT_TASK_EXPORT(COMP_EXTEND_MIJIA_OTA, ExtMi_Ota_Task, 0);


