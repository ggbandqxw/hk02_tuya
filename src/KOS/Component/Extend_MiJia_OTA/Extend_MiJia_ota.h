#ifndef __EXTEND_MIJIA_OTA_H__
#define __EXTEND_MIJIA_OTA_H__


typedef struct
{
    /* PSN为FFFD：表示数据域为文件名和文件长度（ASCII）
     * PSN为FFFE：表示文件传输结束（ymodem出错）
     * PSN为FFFF：表示文件传输结束（正常结束）
     * PSN为其他值：表示包编号
     */
    uint16_t psn;        

    uint16_t size;   //数据域有效大小
    uint8_t  data[]; //数据域
}Extend_MIjiaOta_Msg_stu_t;


#endif /* __EXTEND_MIJIA_OTA_H__ */