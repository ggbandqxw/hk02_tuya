#include "palmvein_protocol.h"
#include "palmvein.h"
#include "palmvein_com.h"
#include "palmvein_encrypt.h"



#define PALMVEIN_PROTOCOL_LOG(format, ...) OSAL_LOG(C_LIGHT_CYAN format C_NONE, ##__VA_ARGS__)




extern void PalmveinCom_TxCmdRingBuffWrite(Palmvein_P_TxCmdMsg_stu_t *pData);
extern void PalmveinCom_FastSendMsg(Palmvein_P_TxCmdMsg_stu_t *pData);
/*******************************************************************************/
/******************       发送消息给掌静脉模组处理          ************************/
/*******************************************************************************/

/**
  * @brief  发消息给队列
  *
  * @param  cmd：命令
  * @param  pData：数据域指针
  * @param  dataLen：数据长度
  * @param  cb：reply回调函数
  */
static void Palmvein_P_SendCmdToRingBuff(uint8_t cmd, uint8_t *pData, uint16_t dataLen, PalmVeinMessageCallback_fun_t cb, uint32_t timeout)
{
	Palmvein_P_TxCmdMsg_stu_t msgData;
	msgData.msgID = cmd;
	msgData.cb = cb;
	msgData.timeout = timeout;
	msgData.data_len = dataLen;
	memcpy(msgData.dataBuf, pData, dataLen);
	PalmveinCom_TxCmdRingBuffWrite(&msgData);

}

/**
  * @brief  直接发送消息（不经过队列）
  *
  * @param  cmd：命令
  * @param  pData：数据域指针
  * @param  dataLen：数据长度
  * @param  cb：reply回调函数
  */
static void Palmvein_P_SendCmd(uint8_t cmd, uint8_t *pData, uint16_t dataLen, PalmVeinMessageCallback_fun_t cb, uint32_t timeout)
{
	Palmvein_P_TxCmdMsg_stu_t msgData;
	msgData.msgID = cmd;
	msgData.cb = cb;
	msgData.timeout = timeout;
	msgData.data_len = dataLen;
	memcpy(msgData.dataBuf, pData, dataLen);
	PalmveinCom_FastSendMsg(&msgData);
}


/*******************************************************************************/
/******************       掌静脉模组note信息处理          *************************/
/*******************************************************************************/

/**
  * @brief  处理掌静脉模组上电准备好note信息
  * @note   开机时模块向主控发送NID_READY
  *
  */
void Palmvein_P_Nid_Ready(void)
{
	PALMVEIN_PROTOCOL_LOG("Palmvein power on ready");
	Palmvein_DealParam_stu_t DealParam;
	DealParam.event = PALMVEIN_EVENT_POWER_ON_READY; //模组上电完成
	Palmvein_Event_Deal(&DealParam);					//掌静脉模组事件处理
}

/**
  * @brief  处理掌静脉模组返回的掌静脉状态信息
  * @note   验证和录入过程中会收到对应信息
  *	
  */
void Palmvein_P_Nid_Palmvein_Status(uint8_t *pNoteDataBuf)
{
	s_note_data_palmvein *pPalmveinStatus = (s_note_data_palmvein *)pNoteDataBuf;
	Palmvein_DealParam_stu_t DealParam;

	DealParam.result = pPalmveinStatus->status;
	DealParam.event = PALMVEIN_EVENT_NID_PALMVEIN_STATUS; //录入和验证掌静脉状态信息返回:掌静脉状态信息返回
	DealParam.data = pPalmveinStatus;

	PALMVEIN_PROTOCOL_LOG(C_LIGHT_BLUE C_COLOR "PalmveinStatus:state:%d" C_NONE, pPalmveinStatus->status);
	Palmvein_Event_Deal(&DealParam);			  //掌静脉模组事件处理

}

/**
  * @brief  处理掌静脉模组OTA状态信息
  * @note   OTA升级过程中会收到对应信息
  *
  */
void Palmvein_P_Nid_Ota_Done(uint8_t *pNoteDataBuf)
{
	Palmvein_DealParam_stu_t DealParam;

	switch (pNoteDataBuf[0])
	{
		case 0: //OTA sucess
			DealParam.result = RESULT_SUCCESS_REPORT;
			break;

		case 1: //OTA fail
		default:
			DealParam.result = RESULT_FAIL_REPORT;
			break;
	}

	DealParam.event = PALMVEIN_EVENT_OTA_FINISH; //OTA结束
	Palmvein_Event_Deal(&DealParam);		//掌静脉模组事件处理

}


extern int32_t Palmvein_Search_Index(uint8_t *userId);
extern int32_t Palmvein_Search_Compare(uint8_t *userId);
extern void Palmvein_Read_All_UserId(void);
/*******************************************************************************/
/***************       操作指令的reply回调函数处理   START    ********************/
/*******************************************************************************/



/**
  * @brief  对比版本号
  * 
  * @param  
  * @note
  *
  * @return 
  */
static ErrorStatus Funmenu_Compare_Vesion(uint8_t *pData, uint8_t dataLen, uint8_t *pVersion, uint8_t *versionLen)
{
	ErrorStatus ret = ERROR;
	uint8_t version_type[] = {'V', 0, '.', 0, '.', 0, 0, 0}; //0不参与匹配
	uint8_t i, j;
	uint8_t len_buf = OSAL_LENGTH(version_type);
	for (i = 0; i < dataLen - len_buf; i++)
	{
		for (j = 0; j < len_buf; j++)
		{
			if ((pData[i + j] != version_type[j]) && (version_type[j] != 0))
				break;
		}

		if (j == len_buf)
		{
			for (j = 0; j < len_buf; j++)
			{
				pVersion[j] = pData[i + j];
			}
			*versionLen = len_buf;
			ret = SUCCESS;
			break;
		}
	}
	return ret;
}


static ErrorStatus _get_version(uint8_t *pData, uint8_t dataLen, uint8_t *pVersion, uint8_t *pModel)
{
	ErrorStatus ret = ERROR;
	uint8_t i = 0, j = 0;
	uint8_t head = 0;
	uint8_t len_buf = 0;

	/* 字符截取 */
	for (i = 0; i < dataLen; i++)
	{
		if (!(pData[i] >= 0x20 && pData[i] <= 0x7E)) //可显字符
		{
			break;
		}
	}
	len_buf = i;
	if (len_buf >= dataLen)
	{
		return ERROR;
	}

	/* 解析型号 */
	for (i = 0; i < len_buf; i++)
	{
		if (pData[i] == '_')
		{
			break;
		}
	}
	if (i <= PALMVEIN_MODEL_LEN)
	{
		memcpy(pModel, pData, i);
	}

	/* 解析版本号 */
	for (i = 0; i < len_buf; i++)
	{
		if (pData[i] >= 'A' && pData[i] <= 'Z') //最后一个大写字母后面为版本号
		{
			memset(pVersion, 0, j);
			head = 1;
			j = 0;
		}	
		if (head)
		{
			pVersion[j++] = pData[i];
		}
	}

	if (j && j < PALMVEIN_VERSION_LEN)
	{
		/* 校验版本号格式 */
		for (i = 1; i < j; i++)
		{
			if (!((pVersion[i] >= '0' && pVersion[i] <= '9') || pVersion[i] == '.'))
			{
				memset(pVersion, 0, PALMVEIN_VERSION_LEN);
				return ERROR;
			}
		}
		ret = SUCCESS;
	}
	return ret;
}




extern ErrorStatus Palmvein_WritePalmveinVersion(uint8_t *pData, uint8_t len);
extern ErrorStatus Palmvein_Write_Model(uint8_t *pData);
/**
  * @brief  0x30获取软件版本消息给掌静脉模组回调
  * 
  * @note
  */
static void Palmvein_P_Get_Version_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_reply_version_data *pdatabuf = (s_msg_reply_version_data *)data;
	Palmvein_DealParam_stu_t DealParam;

	uint8_t version[PALMVEIN_VERSION_LEN] = {0};
	uint8_t model[PALMVEIN_MODEL_LEN] = {0};
	uint8_t versionLen = 0;

	DealParam.event = PALMVEIN_EVENT_SET_FINISH;

	switch (result)
	{
	case MR_SUCCESS: //成功
		if (SUCCESS == _get_version(pdatabuf->version_info, VERSION_INFO_BUFFER_SIZE, version, model))
		{
			DealParam.result = RESULT_SUCCESS_REPORT;
			PALMVEIN_PROTOCOL_LOG("get ver success: model: %s, ver: %s",model,version);
			Palmvein_WritePalmveinVersion(version, versionLen);
			Palmvein_Write_Model(model);
		}
		else
		{
			PALMVEIN_PROTOCOL_LOG("get ver error: model: %s, ver: %s",model,version);
		}
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT;
		break;
	}

	Palmvein_Event_Deal(&DealParam);		 //掌静脉模组事件处理
}

/**
  * @brief  0x53设置临时加密key reply回调
  * 
  * @param  
  * @note
  *
  * @return 
  */
static void Palmvein_P_DebugEncrytionKeyNum_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	Palmvein_DealParam_stu_t DealParam;

	DealParam.result = PALMVEIN_TASK_MSGID_MAX; //无效值
	DealParam.event = PALMVEIN_EVENT_DEBUG_ENC_READY;
	switch (result)
	{
	case MR_SUCCESS: //成功
		DealParam.result = RESULT_SUCCESS_REPORT; 
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT; 
		break;
	}
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}

/**
  * @brief  0x50初始化随机数（设置加密模式）reply回调
  * 
  * @note
  */
static void Palmvein_P_InitEncryption_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_reply_init_encryption_data *pdata = (s_msg_reply_init_encryption_data *)data;
	Palmvein_DealParam_stu_t DealParam;

	DealParam.result = PALMVEIN_TASK_MSGID_MAX; //无效值
	DealParam.event = PALMVEIN_EVENT_INIT_ENCRYPTION_READY;

	switch (result)
	{
	case MR_SUCCESS:								//成功
		DealParam.data = pdata->device_id;
		DealParam.result = RESULT_SUCCESS_REPORT; 
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT; 
		break;
	}
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}


/**
  * @brief  掌静脉单次录入reply结果回调
  * @param  result：结果
  * @param  data：返回的数据
  * @param  len: 数据长度
  * @note
  */
static void Palmvein_P_EnrollSingle_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	s_msg_reply_enroll_data *pdatabuf = (s_msg_reply_enroll_data *)data;
	Palmvein_DealParam_stu_t DealParam;

	
	DealParam.data = pdatabuf;
	switch (result)
	{
	case MR_SUCCESS:
		//单帧录入
		DealParam.result = RESULT_SUCCESS_REPORT;	  //录入成功
		DealParam.event = PALMVEIN_EVENT_ENROLL_SUCCESS; 
		break;

	case MR_FAILED4_PALMVEINENROLLED:					  //掌静脉已录入
		DealParam.result = RESULT_PALMVEIN_ENROLLED;
		DealParam.event = PALMVEIN_EVENT_ENROLL_FAIL; 
		break;

	case MR_FAILED4_MAXUSER: //录入超过最大用户数量
		PALMVEIN_PROTOCOL_LOG("palmvein enroll max user");
	default:
		DealParam.result = RESULT_FAIL_REPORT;		  //失败
		DealParam.event = PALMVEIN_EVENT_ENROLL_FAIL; 
		break;
	}
	PALMVEIN_PROTOCOL_LOG("send enrolling result = %d, num_L=0X%2X, num_H=0X%2X, num=0X%2X", result, pdatabuf->user_id_leb, pdatabuf->user_id_heb, pdatabuf->capture_num);
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}

/**
  * @brief  掌静脉录入reply结果回调
  * @param  result：结果
  * @param  data：返回的数据
  * @param  len: 数据长度
  * @note
  */
static void Palmvein_P_Enroll_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	s_msg_reply_enroll_data *pdatabuf = (s_msg_reply_enroll_data *)data;
	Palmvein_DealParam_stu_t DealParam;
	DealParam.data = pdatabuf;

	PALMVEIN_PROTOCOL_LOG("enroll capture num=0X%2X", pdatabuf->capture_num);
	if(pdatabuf->capture_num < 6)	//抓拍次数小于5
	{
		if(result != ACK_RES_TIMEOUT)
		{
			return ;
		}	
	}

	switch (result)
	{
	case MR_SUCCESS:
		//单帧录入
		DealParam.result = RESULT_SUCCESS_REPORT;	  //录入成功
		DealParam.event = PALMVEIN_EVENT_ENROLL_SUCCESS; 
		break;

	case MR_FAILED4_PALMVEINENROLLED:					  //掌静脉已录入
		DealParam.result = RESULT_PALMVEIN_ENROLLED;
		DealParam.event = PALMVEIN_EVENT_ENROLL_FAIL; 
		break;

	case ACK_RES_TIMEOUT:
		DealParam.result = RESULT_FAIL_REPORT;		  //失败
		DealParam.event = PALMVEIN_EVENT_TIMEOUT; 
		break;

	case MR_FAILED4_MAXUSER: //录入超过最大用户数量
		PALMVEIN_PROTOCOL_LOG("palmvein enroll max user");
	default:
		DealParam.result = RESULT_FAIL_REPORT;		  //失败
		DealParam.event = PALMVEIN_EVENT_ENROLL_FAIL; 
		break;
	}
	PALMVEIN_PROTOCOL_LOG( C_LIGHT_CYAN C_COLOR "enrolling result = %d, num_L=0X%2X, num_H=0X%2X" C_NONE , result, pdatabuf->user_id_leb, pdatabuf->user_id_heb);
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}


/**
  * @brief  掌静脉验证reply回调
  * 
  * @note
  */
void Palmvein_P_Verity_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	s_msg_reply_verify_data *pdatabuf = (s_msg_reply_verify_data *)data;
	Palmvein_DealParam_stu_t DealParam;

	//DealParam.event = PALMVEIN_EVENT_VERIFY_SUCCESS;
	DealParam.data = pdatabuf;

	switch (result)
	{
	case MR_SUCCESS: //成功
		DealParam.result = RESULT_SUCCESS_REPORT;
		DealParam.event = PALMVEIN_EVENT_VERIFY_SUCCESS; 


		/* 比对本地id库 */
		uint16_t palmvein_id = (pdatabuf->user_id_heb << 8 ) | pdatabuf->user_id_leb;
		PALMVEIN_PROTOCOL_LOG("verify search index");
		if(Palmvein_Search_Index((uint8_t *)&palmvein_id) != pdatabuf->user_name[0])
		{
			/* 删除无用id */
			PALMVEIN_PROTOCOL_LOG("can't find id, delete and verify again\r\n");
			Palmvein_P_Send_Start_DelUser_CMD(palmvein_id);
			/* 重新验证 */
			Palmvein_P_Send_StartVerity_CMD(1);
			return;
		}


		break;

	case MR_FAILED4_WITHOUTSMILE:
		PALMVEIN_PROTOCOL_LOG("no smile\r\n");
		DealParam.result = RESULT_VERIFY_FAIL_REPORT;
		DealParam.event = PALMVEIN_EVENT_VERIFY_FAIL; //掌静脉验证结束
		break;
	
	case MR_FAILED4_UNKNOWNUSER:
		PALMVEIN_PROTOCOL_LOG("unknown palmvein\r\n");
		DealParam.result = RESULT_VERIFY_FAIL_REPORT;
		DealParam.event = PALMVEIN_EVENT_VERIFY_FAIL; //掌静脉验证结束
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT;
		DealParam.event = PALMVEIN_EVENT_VERIFY_FAIL; //掌静脉验证结束
		break;
	}

	if(result == 0)
	{
		PALMVEIN_PROTOCOL_LOG(C_LIGHT_CYAN C_COLOR "verity result success, usernum = %02d,id = 0x%02x%02x" C_NONE, 
					pdatabuf->user_name[0], pdatabuf->user_id_heb, pdatabuf->user_id_leb);
	}
	else
	{
		PALMVEIN_PROTOCOL_LOG(C_LIGHT_RED C_COLOR "verity result error, %d" C_NONE,result);
	}


	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}


/**
  * @brief  删除单个用户reply回调
  * 
  * @note
  */
static void Palmvein_P_DelUser_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;

	DealParam.event = PALMVEIN_EVENT_DELETE_FINISH; //删除完成
	switch (result)
	{
	case MR_SUCCESS:
		DealParam.result = RESULT_SUCCESS_REPORT;	//删除成功
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT;		//失败
		break;
	}
	PALMVEIN_PROTOCOL_LOG("send deleting user result = %#2x", result);
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}


/**
  * @brief  删除全部用户reply回调
  * 
  * @note
  */
static void Palmvein_P_DelAll_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;

	DealParam.event = PALMVEIN_EVENT_DELETE_FINISH; //删除完成
	switch (result)
	{
	case MR_SUCCESS:
		
		DealParam.result = RESULT_SUCCESS_REPORT;	//删除成功
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT;		//失败
		break;
	}
	PALMVEIN_PROTOCOL_LOG("send deleting user result = %#2x", result);
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}

/**
  * @brief  终止当前操作回调
  * 
  * @note
  */
void Palmvein_P_Reset_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;

	switch (result)
	{
	case MR_SUCCESS:							  //成功
		DealParam.event = PALMVEIN_EVENT_ABORT_FINISH; //成功进入IDLE模式
		DealParam.result = RESULT_SUCCESS_REPORT;
		break;

	default:
		DealParam.event = PALMVEIN_EVENT_TIMEOUT; //当做超时
		DealParam.result = RESULT_FAIL_REPORT;
		break;
	}

	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}

/**
  * @brief  指令掉电操作回调
  * 
  * @note
  */
void Palmvein_P_PowerDowm_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;

	switch (result)
	{
	case MR_SUCCESS:							  //成功
		DealParam.event = PALMVEIN_EVENT_SLEEP_FINISH; 
		DealParam.result = RESULT_SUCCESS_REPORT;
		break;

	default:
		DealParam.event = PALMVEIN_EVENT_TIMEOUT; //当做超时
		DealParam.result = RESULT_FAIL_REPORT;
		break;
	}

	PALMVEIN_PROTOCOL_LOG("send powerDown result = %#2x", result);
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}


/**
  * @brief  0x52掌静脉设置量产加密秘钥序列回调
  * 
  * @note
  */
static void Palmvein_P_ReleaseEncrytionKeyNum_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;

	DealParam.event = PALMVEIN_EVENT_SET_FINISH;

	switch (result)
	{
		case MR_SUCCESS: //成功
			DealParam.result = RESULT_SUCCESS_REPORT;
			break;

		case MR_REJECTED:						   //模块拒接该命令
			DealParam.result = RESULT_ENCRYPTED; //已加密
			break;

		default:
			DealParam.result = RESULT_FAIL_REPORT;
			break;
	}
	
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}


/**
  * @brief  掌静脉设置debug模式回调
  * 
  * @note
  */
static void Palmvein_P_SetDebugMode_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;

	DealParam.event = PALMVEIN_EVENT_SET_FINISH; //设置结束
	switch (result)
	{
	case MR_SUCCESS: //成功
		DealParam.result = RESULT_SUCCESS_REPORT;
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT;
		break;
	}
	
	Palmvein_Event_Deal(&DealParam);		 //掌静脉模组事件处理
}


/**
  * @brief  掌静脉恢复出厂设置回调
  * 
  * @note
  */
static void Palmvein_P_Factory_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;

	DealParam.event = PALMVEIN_EVENT_DELETE_FINISH; //
	switch (result)
	{
	case MR_SUCCESS: //成功
		DealParam.result = RESULT_SUCCESS_REPORT;
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT;
		break;
	}
	
	Palmvein_Event_Deal(&DealParam);		 //掌静脉模组事件处理	
}


/**
  * @brief  掌静脉启动升级回调
  * @note
  *
  */
static void Palmvein_P_StartOta_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;
	DealParam.data = &data;

	switch (result)
	{
	case MR_SUCCESS: //成功
		DealParam.result = RESULT_SUCCESS_REPORT;
		DealParam.event = PALMVEIN_EVENT_OTA_READY; //OTA 前期准备
		break;

	default:
		DealParam.result = RESULT_FAIL_REPORT;
		DealParam.event = PALMVEIN_EVENT_OTA_FINISH; //OTA结束
		break;
	}
	
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}

/**
  * @brief  配置波特率回调
  * @note
  *
  */
static void Palmvein_P_OtaCfgBaud_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;
	DealParam.data = &data;
	PALMVEIN_PROTOCOL_LOG("OtaCfgBaud_Callback %d",result);

	switch (result)
	{
		case MR_SUCCESS: //成功
			DealParam.result = RESULT_SUCCESS_REPORT;
			DealParam.event = PALMVEIN_EVENT_OTA_CFG_BAUDRATE_FINISH; //波特率配置完成
			break;

		default:
			DealParam.result = RESULT_FAIL_REPORT;
			DealParam.event = PALMVEIN_EVENT_OTA_FINISH; //OTA结束
			break;
	}
	
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理

}

/**
  * @brief  ota分包回调
  * @note
  *
  */
void Palmvein_P_OtaPack_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;
	DealParam.data = &data;

	switch (result)
	{
		case MR_SUCCESS: //成功
			DealParam.result = RESULT_SUCCESS_REPORT;
			DealParam.event = PALMVEIN_EVENT_OTA_PACKET; //ota分包
			break;
			
		default:
			DealParam.result = RESULT_FAIL_REPORT;
			DealParam.event = PALMVEIN_EVENT_OTA_FINISH; //OTA结束
			break;
	}
	
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理

}


/**
  * @brief  0x42获取模块OTA状态回调
  * 
  * @note
  */
static void Palmvein_P_GetOtaStatus_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	s_msg_reply_getOtaStatus_data *pdatabuf = (s_msg_reply_getOtaStatus_data *)data;
	Palmvein_DealParam_stu_t DealParam;
	DealParam.data = pdatabuf;


	switch (result)
	{
		case MR_SUCCESS: //成功
			DealParam.result = RESULT_SUCCESS_REPORT;
			DealParam.event = PALMVEIN_EVENT_OTA_STATUS_FINISH; //OTA 前期准备
			break;

		default:
			DealParam.result = RESULT_FAIL_REPORT;
			DealParam.event = PALMVEIN_EVENT_OTA_FINISH; //OTA结束
			break;
	}
	
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}

/**
  * @brief  发送OTA固件信息回调
  * 
  * @note
  */
static void Palmvein_P_OtaFwInfo_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam;
	DealParam.data = &data;

	switch (result)
	{
		case MR_SUCCESS: //成功
			DealParam.result = RESULT_SUCCESS_REPORT;
			DealParam.event = PALMVEIN_EVENT_OTA_FWINFO_FINISH; //OTA 固件信息完成
			break;

		default:
			DealParam.result = RESULT_FAIL_REPORT;
			DealParam.event = PALMVEIN_EVENT_OTA_FINISH; //OTA结束
			break;
	}
	
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理

}




/**
  * @brief  发送结束升级的消息给掌静脉模组回调
  * 
  * @note
  */
static void Palmvein_P_StopOta_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	Palmvein_DealParam_stu_t DealParam ;

	DealParam.result = RESULT_FAIL_REPORT;
	DealParam.event = PALMVEIN_EVENT_OTA_FINISH; //OTA结束

	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}

/**
  * @brief  获取全部已注册的掌静脉id
  * 
  * @note
  */
static void Palmvein_P_GetAllUserid_Callback(uint8_t result, uint8_t *data, uint8_t len)
{
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_reply_all_userid_data *pdatabuf = (s_msg_reply_all_userid_data *)data;
	uint16_t palmvein_id;
	Palmvein_DealParam_stu_t DealParam;
	DealParam.data = pdatabuf;

	switch (result)
	{
		case MR_SUCCESS: //成功
			DealParam.result = RESULT_SUCCESS_REPORT;
			DealParam.event = PALMVEIN_EVENT_GET_ALL_USERID; //获取完成
			PALMVEIN_PROTOCOL_LOG("all user_counts: %d\r\n",pdatabuf->user_counts);
			Palmvein_Read_All_UserId();
			printf("user_num: user_id \r\n");
			for(uint8_t i = 0;i<pdatabuf->user_counts;i++)
			{
				// printf("0X%02X%02X ",pdatabuf->users_id[i*2],pdatabuf->users_id[i*2+1]);
				palmvein_id = (pdatabuf->users_id[i*2] << 8 ) | pdatabuf->users_id[i*2+1];

				Palmvein_Search_Compare((uint8_t *)&palmvein_id);

				/* 比对本地id库 */
				// if(Palmvein_Search_Compare((uint8_t *)&palmvein_id) != 1)
				// {
				// 	/* 删除无用id */
				// 	printf("can't find id: 0X%X, delete\r\n",palmvein_id);
				// 	Palmvein_P_Send_Start_DelUser_CMD(palmvein_id);
				// 	return;
				// }

			}
			printf("\r\n");

			break;

		default:
			DealParam.result = RESULT_FAIL_REPORT;
			DealParam.event = PALMVEIN_EVENT_TIMEOUT; //
			break;
	}
	
	Palmvein_Event_Deal(&DealParam); //掌静脉模组事件处理
}

/*******************************************************************************/
/********************       reply回调函数处理   END      ************************/
/*******************************************************************************/

/*******************************************************************************/
/********************       操作指令发送函数   START     ************************/
/*******************************************************************************/

/**
  * @brief  发送0x1D掌静脉录入
  * 
  * @param  num:掌静脉编号
  * 
  * @note
  */
void Palmvein_P_Send_StartEnrollSingle_CMD(uint8_t num,uint8_t type)
{
	uint8_t len = sizeof(s_msg_enroll_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_enroll_data *pdatabuf = (s_msg_enroll_data *)databuf;


	pdatabuf->user_name[0] = num;
	pdatabuf->admin = type;
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_EnrollSingle_Callback;
	pdatabuf->timeout = (PALMVEIN_ENROLLING_TIMEOUT - PALMVEIN_MODULE_DEAL_TIMEOUT_SMALL) / PALMVEIN_ONE_SEC_OF_MSEC;
	Palmvein_P_SendCmdToRingBuff(MID_ENROLL_SINGLE, databuf, len, cb, PALMVEIN_ENROLLING_TIMEOUT);

}

/**
  * @brief  发送0x13掌静脉（多次抓拍）录入
  * 
  * @param  num:掌静脉编号
  * 
  * @note
  */
void Palmvein_P_Send_StartEnroll_CMD(uint8_t num,uint8_t type)
{
	uint8_t len = sizeof(s_msg_enroll_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_enroll_data *pdatabuf = (s_msg_enroll_data *)databuf;

	pdatabuf->user_name[0] = num;
	pdatabuf->admin = type;

	PALMVEIN_PROTOCOL_LOG("user_name:%d",pdatabuf->user_name[0]);
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_Enroll_Callback;
	pdatabuf->timeout = (PALMVEIN_ENROLLING_TIMEOUT - PALMVEIN_MODULE_DEAL_TIMEOUT_SMALL) / PALMVEIN_ONE_SEC_OF_MSEC;
	Palmvein_P_SendCmdToRingBuff(MID_ENROLL, databuf, len, cb, PALMVEIN_ENROLLING_TIMEOUT);

}



/**
  * @brief  发送0x12掌静脉验证
  * 
  * @param  time ：超时时间等于time*(PALMVEIN_VERITIFYING_TIMEOUT-PALMVEIN_MODULE_DEAL_TIMEOUT_SMALL)
  * @note
  *
  */
void Palmvein_P_Send_StartVerity_CMD(uint8_t time)
{
	uint8_t len = sizeof(s_msg_verify_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_verify_data *pdatabuf = (s_msg_verify_data *)databuf;
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_Verity_Callback;
	pdatabuf->timeout = time * (PALMVEIN_VERITIFYING_TIMEOUT - PALMVEIN_MODULE_DEAL_TIMEOUT_SMALL) / PALMVEIN_ONE_SEC_OF_MSEC;
	// pdatabuf->timeout =0xe8;
	Palmvein_P_SendCmdToRingBuff(MID_VERIFY, databuf, len, cb, time * PALMVEIN_VERITIFYING_TIMEOUT);
}


/**
  * @brief  发送0x20删除单个掌静脉
  * 
  * @param  user_id 存储于掌静脉模组的用户ID
  * 
  * @note
  *
  */
void Palmvein_P_Send_Start_DelUser_CMD(uint16_t user_id)
{
	uint8_t len = sizeof(s_msg_deluser_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_deluser_data *pdatabuf = (s_msg_deluser_data *)databuf;

	pdatabuf->user_id_heb = (user_id >> 8) & 0xFF;
	pdatabuf->user_id_leb = user_id & 0xFF;
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_DelUser_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_DELUSER, databuf, len, cb, PALMVEIN_DELETING_TIMEOUT);
}


/**
  * @brief  发送0x21删除全部掌静脉
  * 
  * @note
  */
void Palmvein_P_Send_Start_DelAll_CMD(void)
{
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_DelAll_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_DELALL, NULL, 0, cb, PALMVEIN_DELETING_TIMEOUT);
}


/**
  * @brief  发送0x10终止当前操作指令（验证、注册）
  * 
  * @note
  */
void Palmvein_P_Send_Reset_CMD(void)
{
	uint32_t timeout = PALMVEIN_IDLE_TIMEOUT - PALMVEIN_MODULE_DEAL_TIMEOUT_SMALL;
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_Reset_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_RESET, NULL, 0, cb, timeout);
}

                                     
/**
  * @brief  发送0xED掉电的消息
  * 
  * @note
  */
void Palmvein_P_Send_PowerDowm_CMD(void)
{
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_PowerDowm_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_POWERDOWN, NULL, 0, cb, PALMVEIN_POWERDOWN_TIMEOUT);
}


/**
  * @brief  发送0x80特性设置
  * 
  * @note
  * @param flag
  * @return 
  */
void Palmvein_P_Send_SetAttribute_CMD(uint8_t flag)
{
	uint8_t len = sizeof(s_msg_attribute_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_attribute_data *pdatabuf = (s_msg_attribute_data *)databuf;
	PalmVeinMessageCallback_fun_t cb = NULL;
	pdatabuf->flag = flag;
	Palmvein_P_SendCmdToRingBuff(MID_ENABLE_ATTRIBUTE, databuf, len, cb, PALMVEIN_NORMAL_TIMEOUT);
}

/**
  * @brief  发送0x23清除录入记录
  * 
  * @note
  */
void Palmvein_P_Send_PalmveinReset_CMD(void)
{
	Palmvein_P_SendCmdToRingBuff(MID_PALMVEINRESET, NULL, 0, NULL, PALMVEIN_PALMVEIN_RESET_TIMEOUT);
}

/*******************************************************************************/
/********************       操作指令发送函数   END     ************************/
/*******************************************************************************/

/*******************************************************************************/
/******************       加密指令相关处理函数  START          *******************/
/*******************************************************************************/


/**
  * @brief  发送0x50初始化随机数（设置加密模式）给掌静脉模组
  * 
  * @param  seed :随机序列(4个字节)
  * @param  time :当前时间戳(4个字节)
  * @note
  *
  */
void Palmvein_P_Send_InitEncryption_CMD(uint8_t *seed, uint8_t seedlen, uint8_t *time, uint8_t timelen)
{
	uint8_t len = sizeof(s_msg_init_encryption_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_init_encryption_data *pdatabuf = (s_msg_init_encryption_data *)databuf;
	memcpy(pdatabuf->seed, seed, 4);
	memcpy(pdatabuf->crttime, time, 4);
	pdatabuf->mode = ST_ENCMODE_AES; //AES加密模式
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_InitEncryption_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_INIT_ENCRYPTION, databuf, len, cb, PALMVEIN_NORMAL_TIMEOUT);
}


/**
  * @brief  发送0x53临时加密key
  * 
  * @param  data :密钥的选择逻辑（16个字节）
  * @note
  */
void Palmvein_P_Send_DebugEncrytionKeyNum_Cmd(uint8_t *data, uint8_t datalen)
{
	uint8_t len = sizeof(s_msg_enc_key_number_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_enc_key_number_data *pdatabuf = (s_msg_enc_key_number_data *)databuf;
	memcpy(pdatabuf->enc_key_number, data, datalen);

	PalmVeinMessageCallback_fun_t cb = Palmvein_P_DebugEncrytionKeyNum_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_SET_DEBUG_ENC_KEY, databuf, len, cb, PALMVEIN_NORMAL_TIMEOUT);
}

/*******************************************************************************/
/******************       加密指令相关处理函数  END          *********************/
/*******************************************************************************/


/*******************************************************************************/
/******************       设置指令相关处理函数  START        *********************/
/*******************************************************************************/


/**
  * @brief  发送设置演示模式消息给掌静脉模组
  * 
  * @param  mode :0x01： demo       0x00：正常工作模式
  * @note
  *
  */
void Palmvein_P_Send_SetDemoMode_CMD(uint8_t mode)
{
	uint8_t len = sizeof(s_msg_set_debugmode);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_set_debugmode *pdatabuf = (s_msg_set_debugmode *)databuf;

	pdatabuf->mode = mode; //设置工作模式
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_SetDebugMode_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_DEMOMODE, databuf, len, cb, PALMVEIN_NORMAL_TIMEOUT);
}


/**
  * @brief  发送获取软件版本消息给掌静脉模组
  * 
  * @param  mode :0x01： debug       0x00：正常工作模式
  * @note
  *
  */
void Palmvein_P_Send_Get_Version_CMD(void)
{

	PalmVeinMessageCallback_fun_t cb = Palmvein_P_Get_Version_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_GET_VERSION, NULL, 0, cb, PALMVEIN_NORMAL_TIMEOUT);
}


/**
  * @brief  掌静脉0x52设置量产加密秘钥序列
  * 
  * @param  data :密钥的选择逻辑（16个字节）
  * @note
  *
  */
void Palmvein_P_Send_ReleaseEncrytionKeyNum_Cmd(uint8_t *data, uint8_t datalen)
{
	uint8_t len = sizeof(s_msg_enc_key_number_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_enc_key_number_data *pdatabuf = (s_msg_enc_key_number_data *)databuf;
	memcpy(pdatabuf->enc_key_number, data, datalen);

	PalmVeinMessageCallback_fun_t cb = Palmvein_P_ReleaseEncrytionKeyNum_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_SET_RELEASE_ENC_KEY, databuf, len, cb, PALMVEIN_NORMAL_TIMEOUT);
}


/**
  * @brief  掌静脉0xF8恢复出厂设置
  * 
  * @note
  *
  */
void Palmvein_P_Send_Factory_CMD(void)
{
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_Factory_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_FACTORY_RESET, NULL, 0, cb, PALMVEIN_NORMAL_TIMEOUT);
}

/*******************************************************************************/
/******************       设置指令相关处理函数  END        *********************/
/*******************************************************************************/


/*******************************************************************************/
/******************       OTA指令相关处理函数  START        *********************/
/*******************************************************************************/

/**
  * @brief  发送0x40启动升级的消息给掌静脉模组
  * 
  * @param  
  * @note
  *
  */
void Palmvein_P_Send_StartOta_CMD(void)
{
	uint8_t len = sizeof(s_msg_startota_data);
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_startota_data *pdatabuf = (s_msg_startota_data *)databuf;
	PalmVeinMessageCallback_fun_t cb = NULL;
	//pdatabuf->ota_type = type;
	pdatabuf->v_primary = 0x00;
	pdatabuf->v_secondary = 0x00;
	pdatabuf->v_revision = 0x00;
	cb = Palmvein_P_StartOta_Callback;

	Palmvein_P_SendCmdToRingBuff(MID_START_OTA, databuf, len, cb, PALMVEIN_MID_START_OTA_TIMEOUT);
}


/**
  * @brief  发送0x42获取模块OTA状态消息给掌静脉模组
  * 
  * @param  
  * @note
  *
  */
void Palmvein_P_Send_GetOtaStatus_CMD(void)
{
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_GetOtaStatus_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_GET_OTA_STATUS, NULL, 0, cb, PALMVEIN_MID_GET_OTA_STATUS_TIMEOUT);
}

/**
  * @brief  发送0x43固件信息消息给掌静脉模组
  * 
  * @param  
  * @note
  *
  */
void Palmvein_P_Send_OtaFwInfo_CMD(uint8_t *data)
{
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_otaheader_data *pdatabuf = (s_msg_otaheader_data *)databuf;
	memcpy(pdatabuf,data,sizeof(s_msg_otaheader_data));

	PalmVeinMessageCallback_fun_t cb = Palmvein_P_OtaFwInfo_Callback;
	Palmvein_P_SendCmd(MID_OTA_HEADER, databuf, sizeof(s_msg_otaheader_data), cb, PALMVEIN_NORMAL_TIMEOUT);
}

/**
  * @brief  发送0x51配置波特率消息给掌静脉模组
  * 
  * @param  
  * @note
  *
  */
void Palmvein_P_Send_OtaCfgBaud_CMD(uint8_t data)
{
	uint8_t databuf[SENSE_TIME_CMD_DATA_LEN] = {0};
	s_msg_config_baudrate *pdatabuf = (s_msg_config_baudrate *)databuf;
	pdatabuf->baudrate_index = data;

	PalmVeinMessageCallback_fun_t cb = Palmvein_P_OtaCfgBaud_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_CONFIG_BAUDRATE, databuf, sizeof(s_msg_config_baudrate), cb, PALMVEIN_NORMAL_TIMEOUT);
}



/*******************************************************************************/
/******************       OTA指令相关处理函数  END        ***********************/
/*******************************************************************************/

/**
  * @brief  发送获取已注册的所有用户id给掌静脉模组
  * 
  * @param  
  * @note
  *
  */
void Palmvein_P_Send_GetAllUserid_CMD(void)
{
	PalmVeinMessageCallback_fun_t cb = Palmvein_P_GetAllUserid_Callback;
	Palmvein_P_SendCmdToRingBuff(MID_GET_ALL_USERID, NULL, 0, cb, PALMVEIN_NORMAL_TIMEOUT);
}