#ifndef __PALMVEIN_ENCRYPT_H
#define __PALMVEIN_ENCRYPT_H

#include "palmvein.h"

// #define AES_DEBUG

#define RANDOM_SIZE 4
#define PALMVEIN_KEY_SIZE 16 //人脸AES密钥长度

#define PROTOCOL_MID_OFFSET 2
#define PROTOCOL_HEADER_SIZE 6
#define MAX_SEND_SIZE 4000

extern const uint8_t palmveinKeyIndex[PALMVEIN_KEY_SIZE];

void PalmveinEncrypt_GenerateRandomNumber(uint8_t *pRandom);
void PalmveinEncrypt_GenencKey(uint8_t *pRandom);
void PalmveinEncrypt_Set_EnFlag(uint8_t flag);
void PalmveinEncrypt_Decrypt(uint8_t EncryptDirMode, uint8_t *input, const uint16_t *inputLen, uint8_t *result, uint16_t *resultLen);
uint8_t PalmveinEncrypt_Get_EnFlag(void);
#endif