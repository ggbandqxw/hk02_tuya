#ifndef __PALMVEIN_COM_H
#define __PALMVEIN_COM_H

#include "device.h"
#include "palmvein.h"
#include "palmvein_encrypt.h"





#define PALMVEIN_TX_CMD_BUFF_NUM        3	   //发送命令环形队列深度
#define PALMVEIN_POWER_ON_TIMEOUT	    5000						//开机超时时间

//#define PALMVEIN_DEBUG //debug模式下可以打印的日志更全面

#define FIRMWARE_PACKAGE_LEN                0		//固件包长度
#define VERSION_INFO_BUFFER_SIZE            32      //掌静脉模组固件版本信息长度


/* 掌静脉事件*/
#define EVENT_PALMVEIN_RECV_MSG (0X00000001)		    //处理掌静脉协议事件
#define EVENT_PALMVEIN_SEND_MSG (0x00000002)		    //发送给掌静脉模组的事件
#define EVENT_PALMVEIN_TIME_OUT (0x00000004)		    //掌静脉任务超时事件
#define EVENT_PALMVEIN_OTA_TIMEOUT (0x00000008)		    //掌静脉ota超时事件
#define EVENT_PALMVEIN_POWER_OFF (0x00000010)		    //掌静脉断电事件
#define EVENT_PALMVEIN_GET_STATUS (0x00000020)		    //掌静脉获取状态事件


#define PALMVEIN_DELETE_ALL_NUN 0xFF            //全部删除编号
#define INVALID_NUM 0xFE		                //无效编号

#define PALMVEIN_NORMAL_TIMEOUT 2000							//一般超时时间
#define PALMVEIN_REMINDER_INTERVAL 2100							//掌静脉转头提醒间隔 2100
#define PALMVEIN_ONE_SEC_OF_MSEC 1000							//1s = 1000ms
#define PALMVEIN_MODULE_DEAL_TIMEOUT_SMALL 500					//掌静脉模组处理设置时间相对于配置时间减少500ms
#define FOREVER_TIME_OUT (0xFFFFU)							//永远不超时
#define IMMEDIATELY_TIME_OUT 0x00							//立即超时
#define PALMVEIN_POWERING_ON_TIMEOUT 2000						//掌静脉模组上电超时时间为2000ms
#define PALMVEIN_VERITIFYING_TIMEOUT 25500						//掌静脉验证超时时间为5000ms-500
#define PALMVEIN_POWERDOWN_TIMEOUT PALMVEIN_NORMAL_TIMEOUT			//休眠掉电超时时间
#define PALMVEIN_IDLE_SENDING_TIMEOUT 2000						//发送进入空闲状态
#define PALMVEIN_IDLE_TIMEOUT 1500								//空闲模式，1.5超时  1500
#ifdef DXTC_NO_MENU
#define PALMVEIN_ENROLLING_TIMEOUT 40000						//录入超时
#else
#define PALMVEIN_ENROLLING_TIMEOUT 15500						//录入超时
#endif
#define PALMVEIN_DELETING_TIMEOUT PALMVEIN_NORMAL_TIMEOUT			//删除超时
#define PALMVEIN_PALMVEIN_RESET_TIMEOUT PALMVEIN_NORMAL_TIMEOUT			//清除掌静脉录入
#define PALMVEIN_MID_START_OTA_TIMEOUT PALMVEIN_NORMAL_TIMEOUT		//启动OTA指令超时时间
#define PALMVEIN_MID_GET_OTA_STATUS_TIMEOUT PALMVEIN_NORMAL_TIMEOUT //获取模块OTA状态指令超时时间
#define PALMVEIN_SEND_FIRMWARE_TIMEOUT 10000					//发送升级固件超时
#define PALMVEIN_OTA_EXCUTING_TIMEOUT 120000					//掌静脉升级超时2分钟
#define PALMVEIN_SET_EXCUTING_TIMEOUT 5000						//掌静脉设置执行超时时间

//palmvein direction parameters
#define PALMVEIN_DIR_AMEND 1					 //修正参数
#define PALMVEIN_MIDDLE_MAX 10 - PALMVEIN_DIR_AMEND	 //正视
#define PALMVEIN_MIDDLE_MIN -10 + PALMVEIN_DIR_AMEND //正视
#define PALMVEIN_LEFT_MAX -10 - PALMVEIN_DIR_AMEND	 //左脸
#define PALMVEIN_LEFT_MIN -45 + PALMVEIN_DIR_AMEND
#define PALMVEIN_RIGHT_MAX 45 - PALMVEIN_DIR_AMEND //右脸
#define PALMVEIN_RIGHT_MIN 10 + PALMVEIN_DIR_AMEND
#define PALMVEIN_UP_MAX -10 - PALMVEIN_DIR_AMEND //抬头
#define PALMVEIN_UP_MIN -45 + PALMVEIN_DIR_AMEND
#define PALMVEIN_DOWN_MAX 45 - PALMVEIN_DIR_AMEND //低头
#define PALMVEIN_DOWN_MIN 10 + PALMVEIN_DIR_AMEND
#define PALMVEIN_ROLL_MAX 20 - PALMVEIN_DIR_AMEND //歪头
#define PALMVEIN_ROLL_MIN -20 + PALMVEIN_DIR_AMEND

typedef enum
{
	ALLOW = 0,
	DISALLOW = !ALLOW
} AllowStatus;


/*掌静脉状态枚举*/
typedef enum
{
	PALMVEIN_STATUS_SLEEPED,		   	//休眠状态：模块休眠
	PALMVEIN_STATUS_POWER_DOWN,			//断电状态
	PALMVEIN_STATUS_POWER_ON,		   	//上电状态
	PALMVEIN_STATUS_ENCRYPTION,			//加密状态
	PALMVEIN_STATUS_EXECUTE, 			//执行状态：验证，录入，升级，删除：执行命令
	PALMVEIN_STATUS_IDLE,				//空闲状态
	PALMVEIN_STATUS_OTA,				//ota状态
} Palmvein_status_enum_t;


/*掌静脉事件枚举*/
typedef enum
{

	PALMVEIN_EVENT_TIMEOUT,					//超时
	PALMVEIN_EVENT_POWER_ON_READY,	 		//掌静脉模组上电完成
	PALMVEIN_EVENT_POWER_ON_TIMEOUT,	 	//掌静脉模组上电超时
	PALMVEIN_EVENT_START_VERIFY,			//启动掌静脉验证
	PALMVEIN_EVENT_START_ENROLL,			//启动掌静脉录入
	PALMVEIN_EVENT_START_DELETE,		 	//启动掌静脉删除
	PALMVEIN_EVENT_START_VERIFY_DELETE,		//启动掌静脉验证删除
	PALMVEIN_EVENT_DEBUG_ENC_READY,			//临时秘钥完成
	PALMVEIN_EVENT_INIT_ENCRYPTION_READY,	//随机数初始化成功
	PALMVEIN_EVENT_VERIFY_SUCCESS,	 		//掌静脉验证成功
	PALMVEIN_EVENT_VERIFY_FAIL,	 			//掌静脉验证失败
	PALMVEIN_EVENT_ENROLL_SUCCESS,			//掌静脉录入成功
	PALMVEIN_EVENT_ENROLL_FAIL,				//掌静脉录入失败
	PALMVEIN_EVENT_DELETE_FINISH,			//掌静脉删除完成
	PALMVEIN_EVENT_NID_PALMVEIN_STATUS,			//掌静脉录入和验证掌静脉状态信息返回
	PALMVEIN_EVENT_ABORT_FINISH,				//掌静脉终止当前操作完成
	PALMVEIN_EVENT_SLEEP_FINISH,				//掌静脉睡眠
	PALMVEIN_EVENT_START_OTA,			 		//启动OTA
	PALMVEIN_EVENT_OTA_READY,			 		//OTA启动完成
	PALMVEIN_EVENT_OTA_CFG_BAUDRATE_FINISH,		//OTA配置波特率完成
	PALMVEIN_EVENT_OTA_STATUS_FINISH,			//OTA获取状态完成
	PALMVEIN_EVENT_OTA_START_FWINFO,			//启动发送固件信息
	PALMVEIN_EVENT_OTA_FWINFO_FINISH,			//OTA发送固件信息
	PALMVEIN_EVENT_OTA_PACKET,
	PALMVEIN_EVENT_OTA_FINISH,				//OTA结束
	PALMVEIN_EVENT_GET_ALL_USERID,			//获取全部注册id
	PALMVEIN_EVENT_GET_VERSION,				 //获取版本号
	PALMVEIN_EVENT_SET_DEMO_MODE,			//设置演示模式
	PALMVEIN_EVENT_SET_WORK_MODE,			//设置工作模式
	PALMVEIN_EVENT_SET_RELEASE_ENC_KEY,		//设置量产秘钥
	PALMVEIN_EVENT_SET_FINISH,				 //设置完成
	PALMVEIN_EVENT_FACTORY,				//恢复出厂设置
	PALMVEIN_MAX_EVENT,						 //事件最大值（无效）
} Palmvein_event_enum_t;


/*掌静脉模组ota过程状态枚举*/
typedef enum
{
	OTA_IDLE_STATUS,					   //OTA空闲状态
	OTA_SENDING_START_OTA_STATUS,		   //正在发送升级指令
	OTA_SENDING_GET_OTA_STATUS_STATUS,	   //正在发送获取OTA状态指令
	OTA_SENDING_CONFIG_BAUDRATE_STATUS,	   //发送设置波特率指令
	OTA_ZSENDING_OTA_READY_STATUS,		   //发送ota准备就绪
	OTA_ZSENDING_CONFIG_BAUDRATE_STATUS,   //设置zigbee的波特率
	OTA_ZSENDING_GET_FIRMWARE_INFO_STATUS, //获取固件信息
	OTA_ZSENDING_GET_CHECK_SUM_STATUS,	   //获取校验值
	OTA_SENDING_OTA_HEADER_INFO_STATUS,	   //发送固件包头信息状态
	OTA_ZSENDING_START_SENDING_FIRMWARE,   //开始发送固件包
	OTA_SENDING_OTA_PACKET_STATUS,		   //转发固件包
	OTA_MAX_STATUS,						   //无效状态
} PalmveinOtaStatus_enum_t;

/*掌静脉结果*/
typedef enum
{
	RESULT_SUCCESS_REPORT,		 //成功
	RESULT_FAIL_REPORT,			 //失败
	RESULT_VERIFY_FAIL_REPORT,	 //验证失败
	RESULT_CONTINULE,			 //继续下一步
	RESULT_PALMVEIN_ENROLLED,		 //掌静脉已存在
	RESULT_END,					 //结束
	RESULT_NO_PALMVEIN,				 //未检测到掌静脉
	RUSULT_MODULE_BREAKDOWN,	 //模组异常
	RUSULT_OTA_SENDING_FIRMWARE, //正在发送升级包
	RESULT_ENCRYPTED,			 //已加密
} Palmvein_Result_enum_t;

/*掌静脉OTA事件ID*/
typedef enum
{
	OTA_MSGID_RECEIVE_START_OTA_EVEN,			 //收到启动OTA
	OTA_MSGID_SEND_START_OTA_RESULT,			 //发送启动OTA结果
	OTA_MSGID_NID_OTA_DONE_RESULT,				 //note OTA状态信息
	OTA_MSGID_SEND_GET_OTA_STATUS_RESULT,		 //发送获取OTA状态结果
	OTA_MSGID_SENG_CONFIG_BAUDRATE_RESULT,		 //设置波特率状态结果
	OTA_MSGID_ZSENG_GET_FIRMWARE_INFO_RESULT,	 //获取固件信息
	OTA_MSGID_ZSENG_GET_CHECK_SUM_RESULT,		 //获取固件校验值
	OTA_MSGID_ZSENG_OTA_READY_RESULT,			 //OTA准备就绪
	OTA_MSGID_ZSEND_CONFIG_BAUDRATE_RESULT,		 //修改zigbee串口波特率
	OTA_MSGID_SEND_OTA_HEADER_RESULT,			 //发送固件包头信息结果
	OTA_MSGID_RECEIVE_START_SEND_PACKET_EVEN,	 //收到启动发送OTA升级包
	OTA_MSGID_ZSEND_GET_FIRMWARE_PACKAGE_RESULT, //获取固件包结果
	OTA_MSGID_SEND_FIRMWARE_PACKAGE_RESULT,		 //给掌静脉模组发送OTA升级包结果
	SET_MSGID_SET_DEMO_MODE,					 //设置为演示模式
	SET_MSGID_SET_WORK_MODE,					 //设置为工作模式
	SET_MSGID_GET_VERSION,						 //获取掌静脉模组软件版本号
	SET_MSGID_SET_RELEASE_ENC,					 //设定量产加密秘钥序列
	PALMVEIN_TASK_MSGID_MAX,						 //无效值
} Palmvein_Msg_Id_enum_t;


typedef enum
{
	OPERATE_ID_ENROLL,			//录入掌静脉
	OPERATE_ID_VERIFY,			//验证掌静脉
	OPERATE_ID_VERIFY_DELETE,	//验证删除掌静脉
	OPERATE_ID_DELETE,			//删除掌静脉
	OPERATE_ID_ABORT,			//终止掌静脉
	OPERATE_ID_SET_DEMO_MODE,	//设置演示模式		
	OPERATE_ID_SET_WORK_MODE,	//设置工作模式
	OPERATE_ID_SET_RELEASE_ENC,	//设置量产秘钥
	OPERATE_ID_GET_VERSION,		//获取版本号
	OPERATE_ID_START_OTA,		//启动OTA
	OPERATE_ID_MAX,

}Palmvein_operate_id_enum_t;


/* 掌静脉状态信息结构体*/
typedef struct
{
	Palmvein_status_enum_t      curStatus;      //掌静脉模组当前状态
	PalmVein_mode_enum_t        curMode;	  //掌静脉模组当前的工作模式
	PalmVein_ctrl_enum_t        ctrlId;		//掌静脉当前操作id
	AllowStatus                 allowBreakIn;	  //当前掌静脉执行流程    ALLOW:允许打断；    DISALLOW:不允许打断
	uint32_t                    start_time;		  //起始时间
	uint32_t                    timeout;			  //超时时间
	FlagStatus                  busy;			  //当前是否忙状态， SET：忙（只能被终止指令打断，其它事件无法打断）    RESET：可接受外部指令
								  
} Palmvein_Status_stu_t;
extern Palmvein_Status_stu_t palmveinModuleStatus;

// typedef void (*Master_Msg_Cb_fun_t)(uint8_t result, uint8_t *pData, uint8_t dataLen);

#pragma pack(1)

/* 主控发给掌静脉任务的消息结构体 */
typedef struct
{
	PalmVein_ctrl_enum_t    actionId; //动作id
	uint8_t     num;					//编号
	uint8_t     type;					//类型
} Palmvein_Param_stu_t;


/*掌静脉任务处理参数*/
typedef struct
{
	Palmvein_event_enum_t event;		//事件
	Palmvein_Result_enum_t result;	//结果
	uint16_t datalen;				//数据长度
	void *data;						//数据
} Palmvein_DealParam_stu_t;

typedef struct
{
	Palmvein_Msg_Id_enum_t resultId;
	void *pdata;
} Palmvein_OtaParam_stu_t;

/*掌静脉固件包传输结果数据值*/
typedef struct
{
	Palmvein_Result_enum_t result;
} Palmvein_OtaSendFirmware_Result_stu_t;

#pragma pack()


ErrorStatus PalmveinCom_OTASendPacket(uint8_t cmd, uint8_t *pData, uint16_t dataLen, FlagStatus waitAckFlag);
void palmvein_err_process(uint8_t err);
void PalmveinCom_SendMsgProcess(void);  //处理掌静脉发送和设置缓存数据，读取缓存队列的数据发送
void PalmveinCom_RecvMsgProcess(void);  //接收数据并解析处理
void PalmveinCom_SleepProcess(void);    //睡眠处理
void PalmveinCom_Init(void);
uint8_t PalmveinCom_Get_TxStatus(void);


/*ota函数*/
extern void PalmveinOta_Start(uint8_t *data);
extern ErrorStatus PalmveinOta_StartTest(void);
extern ErrorStatus Palmvein_Ota_SendUpgradePack(uint8_t *data, uint16_t len);
extern void Palmvein_Ota_DealFun(Palmvein_DealParam_stu_t *param);
#endif




