#ifndef __EXTEND_MIJIA_H__
#define __EXTEND_MIJIA_H__



typedef void (*ExtMi_send_cb_fun_t)(uint8_t cmd, void *, uint16_t); //发送回调类型

typedef enum
{
    EXT_RECV_DATA_PACKET,
    EXT_EXEC_CB_MI,
} ExtendMiJiaMsgType_enum_t;

/* ExtendKds组件消息类型 */
typedef struct
{
    ExtendMiJiaMsgType_enum_t msgType;
    ExtMi_send_cb_fun_t cb;
    uint8_t cmd;
    uint8_t times;
    uint16_t len;
    uint8_t data[];
} ExtendMiJiaMsg_t;

// static ErrorStatus ExtMi_Send(uint8_t cmd, void *data, uint16_t len, ExtMi_send_cb_fun_t cb);

#define ExtMi_Send(cmd_in, data_in, len_in, cb_in)                                             \
    ({                                                                                          \
        ExtendMiJiaMsg_t *msg = OSAL_Malloc(sizeof(ExtendMiJiaMsg_t) + len_in);                     \
        if (msg)                                                                                \
        {                                                                                       \
            msg->msgType = 0;                                                                   \
            msg->cb = cb_in;                                                                    \
            msg->cmd = cmd_in;                                                                  \
            msg->len = len_in;                                                                  \
            memcpy(msg->data, data_in, len_in);                                                 \
            OSAL_MboxPost(COMP_EXTEND_MIJIA, 0, (uint8_t *)msg, sizeof(ExtendMiJiaMsg_t) + len_in); \
            OSAL_Free(msg);                                                                     \
        }                                                                                       \
    })

#endif /* __EXTEND_MI_H__ */
