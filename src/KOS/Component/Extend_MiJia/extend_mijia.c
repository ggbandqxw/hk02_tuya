#include "device.h"
#include "component.h"

/* 调试打印接口 */
#define EXTEND_MIJIA_LOG(format, ...) __OSAL_LOG("[EXTEND_MIJIA.c] " C_YELLOW format C_NONE, ##__VA_ARGS__)

#define MAX_PACKAGE_LEN 256 //最大包长

/* 定义虚拟硬件接口 */
#define EXTEND_MIJIA_VHWL       vUART_4

#define EXTEND_INT_MCU_VHWL     vPIN_I20

/* UART事件处理周期 */
#define EXTEND_MIJIA_EVT_PROC_CYCLE (10)

/* UART事件 */
#define EVENT_EXTEND_MIJIA (1 << 0)
#define EVENT_EXTEND_MIJIA_SEND_ACK (1 << 1)

/* EXTEND_MIJIA环形接收缓存 */
#ifndef EXTEND_MIJIA_RB_RXBUF_LENGTH
#define EXTEND_MIJIA_RB_RXBUF_LENGTH (128) // EXTEND_MIJIA 接收环形缓存长度
#endif

///////////// 定义协议解析层数据结构 /////////////////////////////////////////////////////////////////////////////////////////
#define PROTOCOL_SEND_TIMES 3           //锁发送命令包的最大次数
#define PROTOCOL_RESEND_CMD_TIMEOUT 1000 //锁重发命令包间隔时间ms
#define PROTOCOL_SEND_ACK_TIME_GAP 20   //锁发出多包ACK的间隔时间ms
///////////// 串口通信协议：数据包结构 /////////////////////////////////////////////////////////////////////////////////////////
#pragma pack(1)
/* 协议头结构 */

/*米家协议结构*/
typedef struct
{
    uint8_t stx[3]; //协议头
    uint8_t cmd;    //命令字
    uint8_t len;    //数据长度
    uint8_t data[]; //数据 + 1位校验和
} ProtocolMi_stu_t;

#pragma pack()

// tx命令包结构
typedef struct
{
    uint8_t buffer[MAX_PACKAGE_LEN]; //发送缓存
    uint16_t len;                    //数据长度
    uint32_t acktimeout;             //等待ACK的超时时间（为0：不需要ACK）
    ExtMi_send_cb_fun_t cb;          //发送命令回调（为NULL：不需要回调）
} uart_txcmd_packet_stu_t;

/* 命令包发送状态信息 */
static struct
{
    uart_txcmd_packet_stu_t data; //数据
    uint32_t timeStamp;           //最后一次发送的时间戳
    uint8_t cnt;                  //发送次数（可能重发）
    enum
    {                    //当前发送的状态
        TX_STA_IDLE,     //空闲
        TX_STA_WAIT_ACK, //等待模块回复应答包
    } status;
    RingBufferHandle_t tbHandle;
} CmdPacketTxInfo;

/* 命令包接收状态信息：记录蓝牙和Uart两个设备的发送状态 */
static struct
{
    uint8_t protocolBuf[MAX_PACKAGE_LEN]; //协议解析缓存
    uint8_t protocolCount;                //协议解析计数变量
    RingBufferHandle_t rbHandle;
    ExtendKdsMsg_t *rx_msg;
    enum
    {
        RX_STA_IDLE,     //空闲
        RX_STA_SEND_ACK, //正在发送多包ACK
    } status;
} CmdPacketRxInfo;

/* 定义发送缓存 */
static uint8_t uart_tx_buffer[MAX_PACKAGE_LEN] = {0};
static uint16_t tx_ctrl_flg = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * @brief  打印HEX数据，最多只能打印32字节
 * @note
 *
 * @param  pData：数据指针
 * @param  len：数据长度
 */
static void ExtMi_PrintfHexData(char *Msg, uint8_t *pData, uint16_t len)
{
    char *str_buffer = OSAL_Malloc(len * 3 + 1);
    if (str_buffer != NULL)
    {
        memset(str_buffer, 0, len * 3 + 1);
        EXTEND_MIJIA_LOG("%s:%d(Bytes)\r\n", Msg, len);
        if (str_buffer != NULL)
        {
            for (int i = 0; i < len; i++)
            {
                sprintf(&str_buffer[i * 3], "%02X ", pData[i]);
            }
            EXTEND_MIJIA_LOG("%s\r\n\r\n", str_buffer);
        }
    }
    if (str_buffer)
        OSAL_Free(str_buffer);
}

/**
 * @brief  计算校验和
 * @note
 *
 * @param  pData：数据指针
 * @param  len：数据长度
 * @return 成功返回校验和
 */
static uint8_t ExtMi_CalcCheckSum(uint8_t *pData, uint16_t len)
{
    uint8_t sum = 0;

    for (; len; len--)
    {
        sum += *pData++;
    }
    return sum;
}

/**
 * @brief  获取发送缓存区的状态
 * @note
 *
 * @return 缓存区为空返回0
 */
static uint8_t ExtMi_GetTxBufferStatus(void)
{
    return tx_ctrl_flg;
}

/**
 * @brief  加载待发送的数据
 * @note   将要发出的数据，填入发送缓存区
 *
 * @param  pData：数据指针
 * @param  len：数据长度
 * @return 成功返回SUCCESS，失败返回ERROR
 */
static ErrorStatus ExtMi_LoadingDataToBeSent(uint8_t *pData, uint16_t len)
{
    uint8_t *pSendBuf = uart_tx_buffer;
    EXTEND_MIJIA_LOG("uart_tx_buffer[3]:%02X\r\n", pData[3]);
    if (tx_ctrl_flg == 0)
    {
        tx_ctrl_flg = len;
        memcpy(pSendBuf, pData, len);
        return SUCCESS;
    }
    EXTEND_MIJIA_LOG("Loading Tx data failed\r\n");
    return ERROR;
}

/**
 * @brief  处理发送缓存
 * @note   将发送缓存区的数据，通过串口发出，并处理Int控制逻辑
 *
 */
static void ExtMi_ProcessTxBuffer(void)
{
    static uint32_t timestamp=0;
    uint32_t currentTime = 0;
    uint8_t *pSendBuf = uart_tx_buffer;
   

    if (tx_ctrl_flg > 0)
    {
        if (tx_ctrl_flg & 0X8000)
        {
            /* 调用硬件层write接口，发出数据 */
            ExtMi_PrintfHexData("Uart send packet", pSendBuf, tx_ctrl_flg & 0X7FFF);
            Device_Write(EXTEND_MIJIA_VHWL, pSendBuf, tx_ctrl_flg & 0X7FFF, 0);

            Device_Write(EXTEND_INT_MCU_VHWL, NULL, 0, 1);
            Device_Disable(EXTEND_INT_MCU_VHWL);
           // 
            timestamp = 0;
            tx_ctrl_flg = 0;
        }
        else
        {
/* 开始发数据，通知底层先发出唤醒信号 */
            currentTime = OSAL_GetTickCount();
            if(timestamp == 0)
            {
                timestamp =  OSAL_GetTickCount();
                Device_Enable(EXTEND_INT_MCU_VHWL);
                Device_Write(EXTEND_INT_MCU_VHWL, NULL, 0, 0);
            }
            else if (OSAL_PastTime(currentTime, timestamp)  > 60)
            {
                tx_ctrl_flg |= 0X8000;
            }
        }
    }
}

static ErrorStatus ExtMi_SendAck(uint8_t cmd, void *data, uint16_t len)
{
    uint16_t checksum_len = 0;
    ErrorStatus res = ERROR;
    ProtocolMi_stu_t *sdata = (ProtocolMi_stu_t *)OSAL_Malloc(sizeof(ProtocolMi_stu_t) + len);

    sdata->stx[0] = 0X4E;
    sdata->stx[1] = 0X54;
    sdata->stx[2] = 0X46;

    sdata->len = len - 1;
    sdata->cmd = cmd;
    memcpy(sdata->data, &data[1], len - 1);
    sdata->data[sdata->len] = ExtMi_CalcCheckSum(&sdata->cmd, sdata->len + 2);

    EXTEND_MIJIA_LOG("start send ack...\r\n");
    if (*(uint8_t *)data == 0x00 || *(uint8_t *)data == 0x01) //需要在次发送ACK
    {
        if (CmdPacketRxInfo.rx_msg)
            CmdPacketRxInfo.rx_msg->times++;
        OSAL_EventSingleCreate(COMP_EXTEND_MIJIA, EVENT_EXTEND_MIJIA_SEND_ACK, PROTOCOL_SEND_ACK_TIME_GAP, EVT_PRIORITY_MEDIUM);
    }
    else
    {
        if (CmdPacketRxInfo.rx_msg)
        {
            OSAL_Free(CmdPacketRxInfo.rx_msg);
            CmdPacketRxInfo.rx_msg = NULL;
        }
        CmdPacketRxInfo.status = RX_STA_IDLE;
    }

    if (Device_Write(EXTEND_MIJIA_VHWL, sdata, sizeof(ProtocolMi_stu_t) + len, 0) == 0)
    {
        res = SUCCESS;
        ExtMi_PrintfHexData("ExtMi Send Ack", (uint8_t *)sdata, sizeof(ProtocolMi_stu_t) + len);
    }

    if (sdata)
        OSAL_Free(sdata);

    return res;
}

/**
 * @brief  处理接收的命令包
 * @note
 *
 * @param  pCmdPacket：数据包指针
 */
static void ExtMi_ProcessRxCmdPacket(ProtocolMi_stu_t *pCmdPacket)
{
    uint8_t len = pCmdPacket->len;
    static uint8_t last_len = 0; //用来保存上一次的长度，比如收到0xB0没有进行ACK，下次收到更长的指令就内存溢出了
    EXTEND_MIJIA_LOG("Recv packet, cmd: 0x%02X\r\n", pCmdPacket->cmd);
    ExtMi_PrintfHexData("rev data", (uint8_t *)pCmdPacket, len + 6);

    if (CmdPacketRxInfo.rx_msg == NULL)
    {
        last_len = len;
        CmdPacketRxInfo.rx_msg = (ExtendKdsMsg_t *)OSAL_Malloc(sizeof(ExtendKdsMsg_t) + len); //多包ACK结束时需要释放
    }
    else
    {
        if (last_len < len) //上次申请的长度不够
        {
            EXTEND_MIJIA_LOG("last_len: %d, len: %d\r\n", last_len, len);
            last_len = len;
            OSAL_Free(CmdPacketRxInfo.rx_msg);
            CmdPacketRxInfo.rx_msg = (ExtendKdsMsg_t *)OSAL_Malloc(sizeof(ExtendKdsMsg_t) + len);
        }
    }

    if (CmdPacketRxInfo.rx_msg != NULL)
    {
        if (CmdPacketTxInfo.status == TX_STA_WAIT_ACK &&
            (pCmdPacket->cmd == CmdPacketTxInfo.data.buffer[3] ||
             pCmdPacket->cmd == 0X0A && CmdPacketTxInfo.data.buffer[3] == 0X00 ||
             pCmdPacket->cmd == 0X02 && CmdPacketTxInfo.data.buffer[3] == 0X01 ))
        {
            CmdPacketTxInfo.status = TX_STA_IDLE;
            CmdPacketRxInfo.rx_msg->msgType = EXT_EXEC_CB_MI;
            CmdPacketRxInfo.rx_msg->cb = CmdPacketTxInfo.data.cb;
        }
        else
        {
            CmdPacketRxInfo.rx_msg->msgType = EXT_RECV_DATA_PACKET;
        }
        CmdPacketRxInfo.rx_msg->cmd = pCmdPacket->cmd;
        CmdPacketRxInfo.rx_msg->len = pCmdPacket->len; //减去CMD命令字节
        CmdPacketRxInfo.rx_msg->times = 0;
        memcpy(CmdPacketRxInfo.rx_msg->data, &pCmdPacket->data, CmdPacketRxInfo.rx_msg->len);

        EXTEND_MIJIA_LOG("publish data size=%d\r\n", sizeof(ExtendKdsMsg_t) + CmdPacketRxInfo.rx_msg->len);
        OSAL_MessagePublish(CmdPacketRxInfo.rx_msg, sizeof(ExtendKdsMsg_t) + CmdPacketRxInfo.rx_msg->len);
        
        // CmdPacketRxInfo.status = RX_STA_SEND_ACK;
    }
    else
    {
        return;
    }
}

/**
 * @brief  解析接收的数据包
 * @note
 *
 * @param  pData：数据包地址
 */
static void ExtMi_ParseRxPacket(uint8_t *pData)
{
    if (pData == NULL)
    {
        return;
    }

    ProtocolMi_stu_t *pPacke = (ProtocolMi_stu_t *)pData;
    if (pPacke->stx[0] == 0x43 && pPacke->stx[1] == 0x4D && pPacke->stx[2] == 0x44)
    {
        ExtMi_ProcessRxCmdPacket(pPacke);
    }
}

/**
 * @brief  从底层环形缓存获取一包数据并解析
 *
 * @note   底层驱动收到数据，会暂存在环形缓存区
 *
 * @return 返回解析后的数据包地址，解析失败返回NULL
 */
static uint8_t *ExtMi_GetOnePacket(void)
{
    static uint8_t getPacketTimeoutCnt = 0;
    static uint8_t protocolBuff[MAX_PACKAGE_LEN * 2];
    static uint16_t cnt = 0;
    ProtocolMi_stu_t *ProtocolMi;
    uint16_t index;
    uint8_t checksum,protocol_sum;
    uint8_t tmpData = 0;
    uint16_t tmpLen = 0;

    tmpLen = OSAL_RingBufferGetValidSize(CmdPacketRxInfo.rbHandle);
    if (tmpLen == 0)
    {
        /* 连续3次没有读取到数据（30ms） */
        if (++getPacketTimeoutCnt >= 3)
        {
            getPacketTimeoutCnt = 0;
            cnt = 0; //清0解析计数
        }
        return NULL;
    }
    getPacketTimeoutCnt = 0;

    for (; tmpLen; tmpLen--)
    {
        OSAL_RingBufferRead(CmdPacketRxInfo.rbHandle, &tmpData);
        protocolBuff[cnt++] = tmpData;
    }

    if (cnt < 6)
    {
        return NULL;
    }

    for (index = 0; index < cnt - 5; index++)
    {
        if (cnt - index < 6)
        {
            return NULL;
        }
        if (protocolBuff[index] == 0x43 && protocolBuff[index + 1] == 0x4D && protocolBuff[index + 2] == 0x44)
        {
            ProtocolMi = (ProtocolMi_stu_t *)&protocolBuff[index];
            if (cnt - index < ProtocolMi->len + 6)
            {
                return NULL;
            }

            // if( (ProtocolMi->cmd >> 4) == 0x06)//OTA类命令  len占两个字节
            // {
            //    checksum = ExtMi_CalcCheckSum(&protocolBuff[index + 3], ProtocolMi->len + 3);
            //    protocol_sum = ProtocolMi->data[ProtocolMi->len+1];
            // }
            // else
            {
                checksum = ExtMi_CalcCheckSum(&protocolBuff[index + 3], ProtocolMi->len + 2);
                protocol_sum = ProtocolMi->data[ProtocolMi->len];
                
            }
            
            EXTEND_MIJIA_LOG("cmd=[%x]checksum %x ,%x\r\n",ProtocolMi->cmd,checksum,protocol_sum);

            if (checksum == protocol_sum)
            {
                // if( (ProtocolMi->cmd >> 4) == 0x06)//OTA类命令  len占两个字节
                // {
                //     memcpy(CmdPacketRxInfo.protocolBuf, &protocolBuff[index], ProtocolMi->len + 7);
                //     memmove(&CmdPacketRxInfo.protocolBuf[5],&CmdPacketRxInfo.protocolBuf[6],ProtocolMi->len+1);
                //     CmdPacketRxInfo.protocolCount = ProtocolMi->len + 6;

                //     cnt -= (index + ProtocolMi->len + 7);
                //     memmove(protocolBuff, protocolBuff[index + ProtocolMi->len + 7], cnt);
                // }
                // else
                {
                    memcpy(CmdPacketRxInfo.protocolBuf, &protocolBuff[index], ProtocolMi->len + 6);
                    CmdPacketRxInfo.protocolCount = ProtocolMi->len + 6;

                    cnt -= (index + ProtocolMi->len + 6);
                    memmove(protocolBuff, protocolBuff[index + ProtocolMi->len + 6], cnt);
                }


                return CmdPacketRxInfo.protocolBuf;
            }
            else
            {
                EXTEND_MIJIA_LOG("checksum  ERR...\r\n");
            }
        }
    }
    cnt = 0;
    return NULL;
}


/**
  * @brief  重发命令包，并处理超时逻辑
  *
  * @note   MCU发出命令包后，超时没有收到确认包，就重发
  */
static void ExtKds_ResendCmdPacket(void)
{
	uint8_t send_timeout_msg = 0;
	uint32_t currentTime = OSAL_GetTickCount();

	if (CmdPacketTxInfo.status == TX_STA_WAIT_ACK)//等待应答包
	{
        if (OSAL_PastTime(currentTime, CmdPacketTxInfo.timeStamp) > CmdPacketTxInfo.data.acktimeout)//ERR:等待应答包超时
		{
            EXTEND_MIJIA_LOG("wait ack timeout\r\n");
			send_timeout_msg = 1;
			CmdPacketTxInfo.status = TX_STA_IDLE;

		}
	}

	/* exec timeout callback */
    if(send_timeout_msg)
    {
        if(CmdPacketTxInfo.data.cb != NULL)
        {
            
            //CmdPacketTxInfo.data.cb(((ProtocolMi_stu_t *)CmdPacketTxInfo.data.buffer)->cmd,NULL,0);
        }
        
    }
}
/**
 * @brief  Uart任务 通信协议处理
 *
 * @note   该函数会每间隔10ms执行一次
 */
static void ExtMi_ProtocolPro(void)
{
    if (ExtMi_GetTxBufferStatus() != 0)
    {
        ExtMi_ProcessTxBuffer(); //处理发送缓存中的数据
    }
    else
    {
        ExtMi_ParseRxPacket(ExtMi_GetOnePacket()); //解析数据
        ExtKds_ResendCmdPacket();
    }
}

/**
 * @brief  判断协议层是否为空闲状态
 * @note
 *
 * @return 空闲：返回SUCCESS， 正忙：返回ERROR
 */
static ErrorStatus ExtMi_IsProtocolIdle(void)
{
    /* 当前没有发出命令、发送缓存为空、接收的命令也处理完成了，才能发出新命令 */
    if (ExtMi_GetTxBufferStatus() == 0 && CmdPacketRxInfo.status == RX_STA_IDLE && CmdPacketTxInfo.status == RX_STA_IDLE)
    {
        return SUCCESS;
    }
    return ERROR;
}

/**
 * @brief  Uart任务 协议层初始化
 *
 * @note
 */
static void ExtMi_ProtocolInit(void)
{
    CmdPacketTxInfo.status = TX_STA_IDLE;
    CmdPacketTxInfo.cnt = 0;
    CmdPacketRxInfo.status = RX_STA_IDLE;
    CmdPacketRxInfo.rx_msg = NULL;
    memset(CmdPacketRxInfo.protocolBuf, 0, sizeof(CmdPacketRxInfo.protocolBuf));
    CmdPacketRxInfo.protocolCount = 0;
}

//处理TX队列
static void ExtMi_ProcessTxQueue(void)
{
    uart_txcmd_packet_stu_t txcmd_packet = {0};

    if (ExtMi_IsProtocolIdle() != SUCCESS)
    {
        return;
    }
    if (OSAL_RingBufferRead(CmdPacketTxInfo.tbHandle, &txcmd_packet) != SUCCESS)
    {
        return;
    }
    CmdPacketTxInfo.data = txcmd_packet;
    CmdPacketTxInfo.timeStamp = OSAL_GetTickCount();
    CmdPacketTxInfo.cnt = 1;
    CmdPacketTxInfo.status = TX_STA_WAIT_ACK;
    ExtMi_LoadingDataToBeSent(CmdPacketTxInfo.data.buffer, CmdPacketTxInfo.data.len);
}

/**
 * @brief  串口接收回调
 *
 * @return
 */
static void ExtMi_Receivecb(VirtualHardware_enum_t dev, void *data, uint32_t len)
{
    uint32_t i = 0;
    uint8_t *p = (uint8_t *)data;

    /* 收到的数据写入缓存 */
    for (i = 0; i < len; i++)
    {
        OSAL_RingBufferWrite(CmdPacketRxInfo.rbHandle, p + i);
    }
}

static ErrorStatus ExtendKds_ProcessMbox(uint8_t *msg)
{
    ExtendMiJiaMsg_t *temp = (ExtendMiJiaMsg_t *)msg;
    uint8_t cmd = temp->cmd;
    uint16_t len = temp->len;
    ExtMi_send_cb_fun_t cb = temp->cb;
    uint8_t *data = temp->data;
    ErrorStatus res = ERROR;

    uart_txcmd_packet_stu_t *send_packer = NULL;
    ProtocolMi_stu_t *sdata;

    {
        EXTEND_MIJIA_LOG("ExtendKds_ProcessMbox, cmd: %02x, len: %d, cb: %p\r\n", cmd, len, cb);

        if (cb == NULL) //发送ACK
        {
            res = ExtMi_SendAck(cmd, data, len);
            goto exit;
        }

        send_packer = (uart_txcmd_packet_stu_t *)OSAL_Malloc(sizeof(uart_txcmd_packet_stu_t));
        sdata = (ProtocolMi_stu_t *)send_packer->buffer;
        sdata->stx[0] = 0X4E;
        sdata->stx[1] = 0X54;
        sdata->stx[2] = 0X46;

        sdata->len = len;
        sdata->cmd = cmd;
        memcpy(sdata->data, data, len);
        sdata->data[sdata->len] = ExtMi_CalcCheckSum(&sdata->cmd, sdata->len + 2);

        send_packer->len = sizeof(ProtocolMi_stu_t) + len + 1;
        send_packer->cb = cb;
        send_packer->acktimeout = PROTOCOL_RESEND_CMD_TIMEOUT;

        if (CmdPacketTxInfo.tbHandle != NULL)
        {
            if (OSAL_RingBufferWrite(CmdPacketTxInfo.tbHandle, send_packer) == SUCCESS)
            {

                res = SUCCESS;
            }
        }
    }

exit:
    if (send_packer)
        OSAL_Free(send_packer);
    return res;
}



/**
 * @brief 
 * @note
 * @return
 */
static void ExtMi_Int_Irq_Handler(char *name, uint8_t status, uint8_t times)
{
    // if (status == INPUT_PORT_STA_PRESS && times != 0xFF)
    // {
    //     /* 创建ISR事件 */
    //     OSAL_EventCreateFromISR(COMP_FINGER);
    // }
    EXTEND_MIJIA_LOG("ExtMi_Int_Irq_Handler\r\n");
}

/**
 * @brief  UART任务函数
 *
 * @note   1.任务函数内不能写阻塞代码
 *         2.任务函数每次运行只处理一个事件
 *
 * @param  event：当前任务的所有事件
 *
 * @return 返回未处理的事件
 */
static uint32_t ExtMi_Task(uint32_t event)
{
    static uint8_t uart_init_flag;
    /* 系统启动事件 */
    if (event & EVENT_SYS_START)
    {
        if (uart_init_flag == 0)
        {
            /* 创建RX环形缓存 */
            CmdPacketRxInfo.rbHandle = OSAL_RingBufferCreate(EXTEND_MIJIA_RB_RXBUF_LENGTH, 1);
            /* 创建TX环形缓存 */
            CmdPacketTxInfo.tbHandle = OSAL_RingBufferCreate(10, sizeof(uart_txcmd_packet_stu_t));
            /* 注册串口接收回调 */
            Device_RegisteredCB(EXTEND_MIJIA_VHWL, ExtMi_Receivecb);
            /* 初始化UART协议层 */
            ExtMi_ProtocolInit();
            uart_init_flag = 1;
        }
        OSAL_EventRepeatCreate(COMP_EXTEND_MIJIA, EVENT_EXTEND_MIJIA, EXTEND_MIJIA_EVT_PROC_CYCLE, EVT_PRIORITY_MEDIUM);
        EXTEND_MIJIA_LOG("ExtMi_Task start\r\n");
        Device_Enable(EXTEND_MIJIA_VHWL);
        
        Device_RegisteredCB(EXTEND_INT_MCU_VHWL, ExtMi_Int_Irq_Handler);
        Device_ConfigCB(EXTEND_INT_MCU_VHWL, ENABLE);
        
        return (event ^ EVENT_SYS_START);
    }

    /* 串口处理事件 */
    if (event & EVENT_EXTEND_MIJIA)
    {
        ExtMi_ProtocolPro();
        ExtMi_ProcessTxQueue();
        return (event ^ EVENT_EXTEND_MIJIA);
    }

    if (event & EVENT_EXTEND_MIJIA_SEND_ACK) //多个ACK包用到的
    {
        if (CmdPacketRxInfo.rx_msg)
        {
            if (OSAL_MessagePublish(CmdPacketRxInfo.rx_msg, sizeof(ExtendKdsMsg_t) + CmdPacketRxInfo.rx_msg->len) != SUCCESS)
            {
                EXTEND_MIJIA_LOG("OSAL_MessagePublish error\r\n");
            }
        }
        return (event ^ EVENT_EXTEND_MIJIA_SEND_ACK);
    } 

    if (event & EVENT_SYS_MBOX)
    {
        uint8_t buffer[MAX_PACKAGE_LEN + 10] = {0};
        while (OSAL_MboxAccept(buffer))
        {
            ExtendKds_ProcessMbox(buffer);
        }
        return (event ^ EVENT_SYS_MBOX);
    }

    /* 系统休眠事件 */
    if (event & EVENT_SYS_SLEEP)
    {
        EXTEND_MIJIA_LOG("ExtMi_Task sleep\r\n");
        OSAL_EventDelete(COMP_EXTEND_MIJIA, EVENT_EXTEND_MIJIA);
        Device_Disable(EXTEND_MIJIA_VHWL);
         Device_Disable(EXTEND_INT_MCU_VHWL);

        return (event ^ EVENT_SYS_SLEEP);  
    }
    return 0;
}
COMPONENT_TASK_EXPORT(COMP_EXTEND_MIJIA, ExtMi_Task, 0);


/**
 * @brief  米家唤醒逻辑
 * @param
 * @return
 */
static int32_t ExtMi_WakeHandle(uint32_t dev)
{
    EXTEND_MIJIA_LOG("ExtMi_WakeHandle...\r\n");
    return 1;
}
COMPONENT_WAKEUP_EXPORT(COMP_EXTEND_MIJIA, ExtMi_WakeHandle, EXTEND_INT_MCU_VHWL);
