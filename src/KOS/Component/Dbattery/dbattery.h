#ifndef __DBATTERY_H__
#define __DBATTERY_H__
#include "osal.h"

typedef struct
{
    uint8_t battery_s;
    uint8_t battery_b;
}DbatteryMsg_t;

static ErrorStatus Dbattery_Get(DbatteryMsg_t *bat);

#endif
