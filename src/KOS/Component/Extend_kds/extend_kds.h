#ifndef __EXTEND_KDS_H__
#define __EXTEND_KDS_H__

#ifdef WIFI_TYPE_8266
    #define MAX_PACKAGE_LEN              1024     //最大包长
#else
    #define MAX_PACKAGE_LEN              128     //最大包长
#endif

typedef void (*extkds_send_cb_fun_t)(uint8_t cmd, void *, uint16_t); //发送回调类型

typedef enum
{
    EXT_RECV_CMD_PACKET,
    EXT_EXEC_CB,
    EXT_SET_BAUD,
} ExtendKdsMsgType_enum_t;

/* ExtendKds组件消息类型 */
typedef struct
{
    ExtendKdsMsgType_enum_t msgType;
    extkds_send_cb_fun_t cb;
    uint8_t cmd;
    uint8_t times;
    uint16_t len;
    uint8_t data[];
} ExtendKdsMsg_t;

// static ErrorStatus ExtKds_Send(uint8_t cmd, void *data, uint16_t len, extkds_send_cb_fun_t cb);

#define ExtKds_Send(cmd_in, data_in, len_in, cb_in)                                         \
({                                                                                          \
    ExtendKdsMsg_t *msg = OSAL_Malloc(sizeof(ExtendKdsMsg_t) + len_in);                     \
    if (msg)                                                                                \
    {                                                                                       \
        msg->msgType = EXT_RECV_CMD_PACKET;                                                 \
        msg->cb = cb_in;                                                                    \
        msg->cmd = cmd_in;                                                                  \
        msg->len = len_in;                                                                  \
        memcpy(msg->data, data_in, len_in);                                                 \
        OSAL_MboxPost(COMP_EXTEND_KDS, 0, (uint8_t *)msg, sizeof(ExtendKdsMsg_t) + len_in); \
        OSAL_Free(msg);                                                                     \
    }                                                                                       \
})

//_baudrate 为0xFFFFFFFF是为切换人脸商汤协议
#define ExtKds_Set_Baudrate(_baudrate)                                                 \
({                                                                                     \
    ExtendKdsMsg_t *msg = OSAL_Malloc(sizeof(ExtendKdsMsg_t) + 4);                     \
    if (msg)                                                                           \
    {                                                                                  \
        msg->msgType = EXT_SET_BAUD;                                                   \
        msg->data[0] = _baudrate & 0xFF;                                               \
        msg->data[1] = (_baudrate >> 8) & 0xFF;                                        \
        msg->data[2] = (_baudrate >> 16) & 0xFF;                                       \
        msg->data[3] = (_baudrate >> 24) & 0xFF;                                       \
        OSAL_MboxPost(COMP_EXTEND_KDS, 0, (uint8_t *)msg, sizeof(ExtendKdsMsg_t) + 4); \
        OSAL_Free(msg);                                                                \
    }                                                                                  \
})



#endif /* __EXTEND_KDS_H__ */
