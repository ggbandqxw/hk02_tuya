#ifndef __BEEP_H__
#define __BEEP_H__
#include "component.h"

#define  BEEP_MBOX_VOICE_CONTROL 0
#define  BEEP_MBOX_MUTE_CONTROL 1
/* NV BEEP 组件初始化标识*/
#define BEEP_NV_INIT_FLAG				0xFA
#pragma pack(1)
typedef struct
{
    uint8_t nvInitFlag;
    uint8_t mute;
}BeepNv_stu_t;
#pragma pack()

/* BEEP控制接口 */
#define Beep_Set(_times, _cycle)                      \
({                                                    \
    uint8_t temp[4] = {BEEP_MBOX_VOICE_CONTROL,0};    \
                                                      \
    *(uint16_t *)(&temp[1]) = _cycle;                 \
    temp[3] = _times;                                 \
    OSAL_MboxPost(COMP_BEEP, 0, &temp, sizeof(temp)); \
})

/* BEEP控制接口 */
#define Beep_SetMuteMode(mute)                       \
({                                                   \
    uint8_t temp[2] = {BEEP_MBOX_MUTE_CONTROL,0};    \
    temp[1] = mute;                                  \
    OSAL_MboxPost(COMP_BEEP, 0, &temp, sizeof(temp));\
})



/* 获取当前静音状态 */
#define Beep_GetMuteMode()                                       \
({                                                                \
    uint8_t temp;                                                 \
    (OSAL_NvReadGlobal(COMP_BEEP,                                \
                       AUDIO_ALIAS_FRONT,                         \
                       OSAL_OFFSET(BeepNv_stu_t, mute),          \
                       &temp, sizeof(temp)) != ERROR) ? temp : 0; \
})

#endif /* __BEEP_H__ */
