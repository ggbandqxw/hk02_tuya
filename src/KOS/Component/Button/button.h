#ifndef __BUTTON_H__
#define __BUTTON_H__


#define KEY_OPEN_CLOSE_PIN  0x02000000 
extern uint32_t wakup_pin_flag;


/* Button组件消息类型 */
typedef struct 
{
    uint8_t action;          //动作  UserAction_enum_t
    char name[20];           //按键名称
    uint8_t times;           //连按次数
}ButtonMsg_t;



#endif /* __BUTTON_H__ */
