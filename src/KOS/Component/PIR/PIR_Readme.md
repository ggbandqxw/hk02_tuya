# PIR组件

## 传感器触发原理
当人体靠近时，OUT脚产生波形，INT脚产生高电平，
距离越近，波形变化幅度越大，同时每个波形都是伴随持续的高电平，高电平的长度与检测到人体遮挡传感器的宽度有关

## 徘徊算法基本流程图
```mermaid
flowchart TD;
op1[定时50ms检测]
op2([读取adc和irq脚状态])
op3{{当前adc小于上次最大值,则刷新当前最大值}}
op4{{当前adc小于上次最小值,则刷新当前最小值}}
op5{{判断最大值和最小值的差是否满足阀值,且最小值是否满足阀值}}
op6[记录一次徘徊]
op7{{判断徘徊次数和时间是否满足当前条件}}
op8[触发徘徊报警]
op9[清空状态]

op1-->op2-->op3
op3-->op4-->op5
op5--yes-->op6-->op7
op5-->op7
op7--yes-->op8 
op7--no-->op9

```




## PIR ADC数据汇总表
|   |取值范围|平均值|差值|
|---|---|---|---|
|1M中心范围|ADC>=2200 |2316|1619|
|^|ADC<=800|696|^|
|1M徘徊范围|ADC>=2200 |2352|1674|
|^|ADC<=800|677|^|
|2M中心范围|ADC>=2000 |2122|944|
|^|ADC<=1300|1177|^|
|2M徘徊范围|ADC>=2000 |2146|1003|
|^|ADC<=1300|1143|^|
|3M中心范围|ADC>=1750 |1826|366|
|^|ADC<=1550|1459|^|
|3M徘徊范围|ADC>=1750 |1768|261|
|^|ADC<=1550|1506|^|


## API接口

```c
/**
 * @brief: PIR 设置灵敏度
 * @note:
 * @param level：灵敏度等级( 高，中，低)
 * @return 
 */
static uint32_t Pir_SetSentivity(PirSens_enum_t level) //设置灵敏度

/**
 * @brief: PIR 设置徘徊时间
 * @note:
 * @param time：时间(ms)
 * @return 
 */
static uint32_t Pir_SetWanderTime(uint32_t time) //设置徘徊时间

/**
 * @brief: PIR 开关
 * @note:
 * @param data:使能，0：关闭，1：打开
 * @return 
 */
static uint32_t Pir_Switch(uint8_t data) //pir开关
```



