#ifndef _PIR_SENSOR_H
#define _PIR_SENSOR_H
#include "component.h"

typedef enum
{
    PIR_SENS_H = 1,  //灵敏度 高
    PIR_SENS_M,     //中
    PIR_SENS_L,     //低
}PirSens_enum_t;


/* Pir组件消息类型 */
typedef struct
{
    uint8_t alarm;   //
    uint16_t voltage;   
    uint16_t irq;
}PirMsg_t;

#endif