/**
 * @file nd03b_data.h
 * @author tongsheng.tang
 * @brief ND03B get depth data functions
 * @version 1.x.x
 * @date 2021-11
 * 
 * @copyright Copyright (c) 2021, Shenzhen Nephotonics Inc.
 * 
 */
#ifndef __ND03B_DATA_H__
#define __ND03B_DATA_H__

#include "nd03b_def.h"
 
/** @defgroup ND03B_Data_Group ND03B Data Funtions
 *  @brief ND03B Data Funtions
 *  @{
 */

/** 等待ND03B测距完成 */
int32_t ND03B_WaitDepthAndAmpBufReady(ND03B_Dev_t *pNxDevice);

/** 发送开始测量信号 */
int32_t ND03B_StartMeasurement(ND03B_Dev_t *pNxDevice);

/** 发送结束测量信号，用于连续模式 */
int32_t ND03B_StopMeasurement(ND03B_Dev_t *pNxDevice);

/** 读取深度和幅度值 */
int32_t ND03B_ReadDepthAndAmpData(ND03B_Dev_t *pNxDevice, ND03B_RangingData_t *pData);

/** 清除数据有效位 */
int32_t ND03B_ClearDataValidFlag(ND03B_Dev_t *pNxDevice);

/** 获取一次测量深度和幅度值 */
int32_t ND03B_GetRangingData(ND03B_Dev_t *pNxDevice, ND03B_RangingData_t *pData);


/** @} ND03B_Data_Group */

#endif

