/**
 * @file nd03b_dev.h
 * @author tongsheng.tang
 * @brief ND03B device setting functions
 * @version 1.x.x
 * @date 2021-11
 * 
 * @copyright Copyright (c) 2021, Shenzhen Nephotonics Inc.
 * 
 */
#ifndef __ND03B_DEV_H__
#define __ND03B_DEV_H__

#include "nd03b_def.h"

/** @defgroup ND03B_Dev_Group ND03B Device Funtions
 *  @brief ND03B Device Funtions
 *  @{
 */

/** 获取SDK版本号 */
uint32_t ND03B_GetSdkVersion(void);
/** 获取当前ND03B的积分时间 */
uint16_t ND03B_GetCurrentExp(ND03B_Dev_t *pNxDevice);
/** 设置ND03B的测量间隔时间 */
int32_t ND03B_SetRangeInterval(ND03B_Dev_t *pNxDevice, uint32_t interval);
/** 获取ND03B的测量间隔时间 */
uint32_t ND03B_GetRangeInterval(ND03B_Dev_t *pNxDevice);
/** 发送保存自动检测触发阈值请求 */
int32_t ND03B_SaveLowPowerDataRequest(ND03B_Dev_t *pNxDevice);
/** 等待保存自动检测触发阈值完成 */
int32_t ND03B_WaitforSaveLowPowerData(ND03B_Dev_t *pNxDevice);
/** 保存自动检测触发阈值 */
int32_t ND03B_SaveLowPowerData(ND03B_Dev_t *pNxDevice);
/** 设置自动检测触发阈值灵敏度(1~100) */
int32_t ND03B_SetSensitivity(ND03B_Dev_t *pNxDevice, uint32_t Sensitivity);
/** ND03B待机 */
int32_t ND03B_Sleep(ND03B_Dev_t *pNxDevice);
/** ND03B唤醒 */
int32_t ND03B_Wakeup(ND03B_Dev_t *pNxDevice);
/** 初始化ND03B设备 */
int32_t ND03B_InitDevice(ND03B_Dev_t *pNxDevice);
/** 获取ND03B模组固件版本号 */
int32_t ND03B_GetFirmwareVersion(ND03B_Dev_t *pNxDevice, uint32_t* pFirmwareDataBuf);
/** 获取设备当前信息 */
int32_t ND03B_GetDevInfo(ND03B_Dev_t *pNxDevice);
/** ND03B低功耗模式触发 */
int32_t ND03B_LowPowerModeTrig(ND03B_Dev_t *pNxDevice);
/** ND03B高精度模式触发 */
int32_t ND03B_HighRangingModeTrig(ND03B_Dev_t *pNxDevice);
/** ND03B自动检测模式触发 */
int32_t ND03B_AutoDetectModeTrig(ND03B_Dev_t *pNxDevice);
/** 等待ND03B模组启动 */
int32_t ND03B_WaitDeviceBootUp(ND03B_Dev_t *pNxDevice);

/** @} ND03B_Dev_Group */

#endif

