/**
 * @file nd03b_calib.h
 * @author tongsheng.tang
 * @brief ND03B Calibration functions
 * @version 1.x.x
 * @date 2021-11
 * 
 * @copyright Copyright (c) 2021, Shenzhen Nephotonics Inc.
 * 
 */

#ifndef __ND03B_CALIB_H__
#define __ND03B_CALIB_H__

#include "nd03b_def.h"

/** @defgroup ND03B_Calibration_Group ND03B Calibration Funtions
 *  @brief ND03B Calibration Funtions
 *  @{
 */

/** 获取错误值 */
int32_t ND03B_ReadErrorStatus(ND03B_Dev_t *pNxDevice);

/** 发送Offset标定请求 */
int32_t ND03B_OffsetCalibrationRequest(ND03B_Dev_t *pNxDevice);

/** 发送串扰标定请求 */
int32_t ND03B_XTalkCalibrationRequest(ND03B_Dev_t *pNxDevice);

/** 等待Offset标定完成 */
int32_t ND03B_WaitforOffsetCalibration(ND03B_Dev_t *pNxDevice);

/** 等待串扰标定完成 */
int32_t ND03B_WaitforXTalkCalibration(ND03B_Dev_t *pNxDevice);

/** Offset标定标定函数*/
int32_t ND03B_OffsetCalibration(ND03B_Dev_t *pNxDevice);

/** Offset标定函数带有标定深度设置 */
int32_t ND03B_OffsetCalibrationAtDepth(ND03B_Dev_t *pNxDevice, uint16_t calib_depth_mm);

/** 串扰标定函数 */
int32_t ND03B_XTalkCalibration(ND03B_Dev_t *pNxDevice);

/** 串扰标定函数带有标定深度设置 */
int32_t ND03B_XTalkCalibrationAtDepth(ND03B_Dev_t *pNxDevice, uint16_t xtalk_calib_depth_mm);

/** 设置Offset标定深度 */
int32_t ND03B_SetOffsetCalibDistMM(ND03B_Dev_t *pNxDevice, uint16_t depth_mm);

/** 设置串扰标定深度 */
int32_t ND03B_SetXTalkCalibDistMM(ND03B_Dev_t *pNxDevice, uint16_t depth_mm);

/** @} ND03B_Calibration_Group */

#endif

