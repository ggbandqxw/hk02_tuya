/**
 * @file nd03b_comm.h
 * @author tongsheng.tang
 * @brief ND03B communication functions
 * @version 1.x.x
 * @date 2021-11
 * 
 * @copyright Copyright (c) 2021, Shenzhen Nephotonics Inc.
 * 
 */
#ifndef __ND03B_COMM_H__
#define __ND03B_COMM_H__

#include "nd03b_def.h"

/** @defgroup ND03B_Communication_Group ND03B Communication Funtions
 *  @brief ND03B Communication Funtions
 *  @{
 */


/** 
 * @struct ND03B_Func_Ptr_t
 * 
 * @brief 链接平台的API\n
 */
typedef struct{
	int32_t(*I2C_WriteNBytesFunc)(uint8_t, uint16_t, uint8_t *, uint8_t); ///< i2c写函数
	int32_t(*I2C_ReadNBytesFunc)(uint8_t, uint16_t, uint8_t *, uint8_t); ///< i2c读函数
	void(*Delay1msFunc)(uint32_t );   ///< 延时1ms函数
	void(*Delay10usFunc)(uint32_t );  ///< 延时10us函数
}ND03B_Func_Ptr_t;


/** 初始化ND03B库的API */
void ND03B_RegisteredPlatformApi(ND03B_Func_Ptr_t dev_op);

/** 延时时间（ms） */
void ND03B_Delay1ms(uint32_t ms);

/** 延时时间（10us） */
void ND03B_Delay10us(uint32_t us);

/** 设置xshut引脚的电平 */
void ND03B_SetXShutPinLevel(ND03B_Dev_t *pNxDevice, uint32_t level);

/** 获取Gpio0引脚的电平 */
int32_t ND03B_GetGpio0PinLevel(ND03B_Dev_t *pNxDevice);
    
/** 对ND03B寄存器写1个字节 */
int32_t ND03B_WriteByte(ND03B_Dev_t *pNxDevice, uint16_t addr, uint8_t wdata);

/** 对ND03B寄存器写1个字 */
int32_t ND03B_WriteWord(ND03B_Dev_t *pNxDevice, uint16_t addr, uint32_t wdata);

/** 对ND03B寄存器读1个字 */
int32_t ND03B_ReadWord(ND03B_Dev_t *pNxDevice, uint16_t addr, uint32_t *rdata);

/** 对ND03B寄存器写N个字 */
int32_t ND03B_WriteNWords(ND03B_Dev_t *pNxDevice, uint16_t addr, uint32_t *wdata, uint32_t len);

/** 对ND03B寄存器读N个字 */
int32_t ND03B_ReadNWords(ND03B_Dev_t *pNxDevice, uint16_t addr, uint32_t *rdata, uint32_t len);


/** @} ND03B_Communication_Group */

#endif

