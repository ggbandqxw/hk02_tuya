/**
 * @file nd03b_def.h
 * @author tongsheng.tang
 * @brief ND03B's Macro definition and data structure
 * @version 1.x.x
 * @date 2021-11
 * 
 * @copyright Copyright (c) 2021, Shenzhen Nephotonics Inc.
 * 
 */
#ifndef __ND03B__DEF__H__
#define __ND03B__DEF__H__

#include "nd03b_stdint.h"
#include <stdio.h>



#define DEBUG_INFO        0 /** 调试信息打印开关 */

#if __WORD_SIZE__ == 4
#define NX_PRINTF(fmt, ...)   do{if(DEBUG_INFO) printf(fmt, ##__VA_ARGS__); }while(0) /** 调试接口，默认使用printf */
#elif DEBUG_INFO
#define NX_PRINTF       printf
#else
#define NX_PRINTF       /##/
#endif

/** @defgroup ND03B_Global_Define_Group ND03B Defines
 *	@brief	  ND03B Defines
 *	@{
 */

/** @defgroup ND03B_Fw_Ver_Value_Group ND03B Version Value Defines
 * @brief	  ND03B Firmware Version Value
 *  @{
 */

#define ND03B_FW_VERSION_V123_VALUE              (10203)   /** V1.2.3版本数值 */

/** @} ND03B_Fw_Ver_Value_Group */


/** @defgroup ND03B_Reg_Group ND03B Register Defines
 * @brief	  ND03B Register Defines
 *  @{
 */

#define ND03B_REG_BOOT1                          (0x55AA)  /** ND03B模组第一段启动寄存器，需要对该寄存器写1byte，且值为0x38 */
#define ND03B_REG_BOOT2                          (0x3BFC)  /** ND03B模组第二段启动寄存器，需要对该寄存器写4bytes，且值为0xFFFFFFF2 */
#define ND03B_REG_BOOT3                          (0x0888)  /** ND03B模组第三段启动寄存器，需要对该寄存器写4bytes，且值为0x00000001 */
#define ND03B_REG_BOOT4                          (0x3A1C)  /** ND03B模组第四段启动寄存器，需要对该寄存器写4bytes，且值为0x0000000C */
#define ND03B_REG_BOOT5                          (0x3950)  /** ND03B模组第五段启动寄存器，需要对该寄存器写4bytes，且值为0x00000000 */
#define ND03B_REG_BOOT6                          (0x0808)  /** ND03B模组第六段启动寄存器，需要对该寄存器写4bytes，且值为0x00000001 */
#define ND03B_REG_DEV_ID                         (0x3B80)  /** ND03B模组设备ID */

#define ND03B_REG_LOW_POWER_TH                   (0xA720)  /** 低功耗阈值寄存器组 */
#define ND03B_REG_MODEL                          (0xA78C)  /** 模组型号寄存器 */

/* Read Only(RO) */
#define ND03B_REG_FW_VERSION                     (0xA7A0)  /** ND03B模组固件版本 */
#define ND03B_REG_STATE                          (0xA7A4)  /** ND03B模组运行状态 */
#define ND03B_REG_AMP_DEPTH                      (0xA7A8)  /** 幅度值和深度值寄存器，[15:0]->uint16_t:深度值(mm), [31:16]->uint16_t:幅度值 */
#define ND03B_REG_EXP_THERM                      (0xA7AC)  /** 积分时间和温度值寄存器, [15:0]->int16_t:温度, [31:16]->uint16_t:积分时间(us)*/
#define ND03B_REG_ERROR_FLAG                     (0xA7B0)  /** 错误返回寄存器 */
#define ND03B_REG_XTALK_AMP                      (0xA7B4)  /** 串扰幅度值寄存器 */
#define ND03B_REG_XTALK_DEPTH                    (0xA7B8)  /** 串扰深度值寄存器 */

/* Read and Write(RW) */
#define ND03B_REG_CALIB_XT_OFFSET                (0xA7BC)  /** 偏移量标定和串扰标定的实际深度寄存器， [15:0]->uint16_t:标定深度(mm), [31:16]->uint16_t:串扰标定深度(mm)*/
#define ND03B_REG_DATA_VAL_REQ                   (0xA7C8)  /** 数据请求和数据有效寄存器, [15:0]->uint16_t:数据请求, [31:16]->uint16_t:数据有效*/

#define ND03B_REG_V123_XTALK_AMP                 (0xA43C)  /** V1.2.3版本的串扰幅度值寄存器 */
#define ND03B_REG_V123_XTALK_DEPTH               (0xA43C)  /** V1.2.3版本的串扰深度值寄存器 */

/** @} ND03B_Reg_Group */

/** @defgroup ND03B_Depth_Value_Group ND03B Depth Value Defines
 * @brief	  ND03B Depth Value
 *  @{
 */

#define ND03B_DEPTH_OVERFLOW                     (65500)   /** 过曝时深度值 */                  
#define ND03B_DEPTH_LOW_AMP                      (65300)   /** 欠曝时深度值 */
#define ND03B_LOW_POWER_DEPTH                    (65200)   /** 低功耗检测模式深度值 */

/** @} ND03B_Depth_Value_Group */

/** @defgroup ND03B_Boot_Group ND03B Boot Value
 *	@brief	  ND03B Boot  (ND03B_REG_BOOT)
 *	@{
 */

#define ND03B_REG_BOOT1_VALUE                      0x38          /** ND03B模组第一段启动值，且值为0x38 */
#define ND03B_REG_BOOT2_VALUE                      0xFFFFFFF2    /** ND03B模组第二段启动值，且值为0xFFFFFFF2 */
#define ND03B_REG_BOOT3_VALUE                      0xFFFFFFFF    /** ND03B模组第三段启动值，且值为0xFFFFFFFF */
#define ND03B_REG_BOOT4_VALUE                      0xFFFFFF0C    /** ND03B模组第四段启动值，且值为0xFFFFFF0C */
#define ND03B_REG_BOOT5_VALUE                      0xFFFFFF00    /** ND03B模组第五段启动值，且值为0x00000000 */
#define ND03B_REG_BOOT6_VALUE                      0xFFFFFFFF    /** ND03B模组第六段启动值，且值为0xFFFFFFFF */
#define ND03B_REG_DEV_ID_VALUE                     0x002F0012    /** ND03B模组设备ID默认值 */

/** @} ND03B_Boot_Group */


/** @defgroup ND03B_State_Group ND03B Data Request Index
 *	@brief	  ND03B State  (ND03B_REG_STATE)
 *	@{
 */

#define ND03B_STATE_SOFTWARE_READY                 0x000000A5
#define ND03B_STATE_GOT_DEPTH                      0x000000A6

/** @} ND03B_State_Group */

/** @defgroup ND03B_Data_Val_Req_Idx_Group ND03B Data Request Index
 *	@brief	  ND03B Data Request Mask (ND03B_REG_DATA_VAL_REQ)
 *	@{
 */
// REG_DATA_REQ MASK
#define ND03B_DEPTH_DATA_REQ_MASK                  0x00000001  /** 深度数据读取请求掩码 */
#define ND03B_OFFSET_CALIBRATION_REQ_MASK          0x00000002  /** 偏移量标定请求掩码 */
#define ND03B_XTALK_CALIB_REQ_MASK                 0x00000004  /** 串扰标定请求掩码 */
#define ND03B_SAVE_LOW_POWER_DATA_REQ_MASK         0x00000030  /*!< 保存低功耗数据请求掩码 */
// REG_DATA_VAL MASK
#define ND03B_DEPTH_DATA_VAL_MASK                  0x00010000  /** 深度数据有效位掩码 */
#define ND03B_OFFSET_CALIBRATION_VAL_MASK          0x00020000  /** 偏移量标定完成有效位掩码 */
#define ND03B_XTALK_CALIB_VAL_MASK                 0x00040000  /** 串扰标定完成有效位掩码 */
#define ND03B_SAVE_LOW_POWER_DATA_VAL_MASK         0x00300000  /*!< 保存低功耗数据请求掩码 */
/** @} ND03B_Data_Val_Req_Idx_Group */


/** @defgroup ND03B_Error_Group ND03B Error Group
 *  @brief	  ND03B Error Group (ND03B_REG_ERROR_FLAG)
 *	@{
 */
#define ND03B_ERROR_NONE                            0  /** 成功 */
#define ND03B_ERROR_CALIBRATION                    -1  /** 标定失败错误 */
#define ND03B_ERROR_TIME_OUT                       -2  /** 超时错误 */
#define ND03B_ERROR_I2C			                      -3  /** IIC通讯错误 */
#define ND03B_ERROR_AMP			                      -4  /** 幅度值错误 */
#define ND03B_ERROR_FW                             -5  /** 固件版本兼容错误 */
#define ND03B_ERROR_BOOT                           -6  /** 模组启动错误 */
#define ND03B_ERROR_WORD_SIZE                      -7  /** 字的大小配置错误 */
#define ND03B_ERROR_RANGING                        -8  /** 测距出错 */

#define ND03B_ERROR_XTALK_AMP_OVERFLOW             33  /** 串扰幅度值过大 */
#define ND03B_ERROR_XTALK_CALIBRATION              34  /** 串扰标定失败 */
/** @} ND03B_Error_Group */


/** @defgroup ND03B_Device_Mode_Group ND03B Device Mode
 *  @brief  ND03B Device Mode (ND03B_REG_RANGE_MODE)
 *	@{
 */

#define ND03B_RANGE_MODE_MASK                      ((uint32_t)0x000000FF)              /** 测量模式掩码 */

#define ND03B_NORMAL_MODE                          ((uint32_t)0x00000000)              /** 正常模式   */
#define ND03B_LOW_POWER_MODE                       ((uint32_t)0x00000001)              /** 低功耗检测模式 */
#define ND03B_HIGH_RANGE_MODE                      ((uint32_t)0x00000002)              /** 高精度测距模式 */
#define ND03B_AUTO_DETECT_MODE                     ((uint32_t)0x00000003)              /** 自动检测模式 */

/** @} ND03B_Device_Mode_Group */


/** @defgroup ND03B_Slave_Address_Group ND03B Device Mode
 *  @brief  ND03B Slave Address (ND03B_REG_Slave_ADDR)
 *	@{
 */

#define ND03B_DEFAULT_SLAVE_ADDR                   0x5B  /** ND03B默认IIC地址(7bit) */

/** @} ND03B_Slave_Address_Group */


/** @enum  ND03B_Status_e
 *  @brief 定义ND03B状态宏
 */
typedef enum{
    ND03B_DISABLE = 0,   ///< 关闭状态
    ND03B_ENABLE  = 1    ///< 使能状态
  } ND03B_Status_e;


/** 
  * @struct ND03B_RangingData_t
  * 
  * @brief ND03B测量结果结构体 \n
  * 定义存储ND03B的深度、幅度信息
  */
typedef struct{
    uint16_t depth; ///< 测量距离/mm
    uint16_t amp;   ///< 测量幅度值，作为信号可信度
}ND03B_RangingData_t;


/** 
  * @struct ND03B_ChipInfo_t
  * 
  * @brief ND03B模组生产信息\n
  */
 typedef struct {
    uint32_t  nd03b_fw_version;        ///< ND03B固件版本
    uint32_t  nd03b_product_date;      ///< ND03B生产日期
    uint32_t  nd03b_product_time;      ///< ND03B生产时间
    uint32_t  nd03b_model_revision;    ///< ND03B设备类型及硬件版本
    uint8_t   nd03b_chip_id[32];       ///< ND03B唯一芯片ID,字符串
 } ND03B_ChipInfo_t;

/** 
  * @struct ND03B_DevConfig_t
  * 
  * @brief ND03B模组配置数据\n
  */
typedef struct {
    uint32_t        range_mode;             ///< 模组取图模式配置
    uint32_t        range_interval;         ///< 模组取图间隔时间配置
} ND03B_DevConfig_t;

/** 
  * @struct ND03B_Dev_t
  * 
  * @brief 设备类型结构体\n
  */
typedef struct {
    void(*SetXShutPinLevelFunc)(uint32_t );   ///< 如果设置XShut电平函数指针，参数值为0，则输出低电平，否则输出高电平
    int32_t(*GetGpio0PinLevelFunc)(void);     ///< 获取GPIO0电平状态函数指针，返回值为0，则输入低电平，否则输入高电平
    ND03B_DevConfig_t  config;                 ///< 模组配置信息
    ND03B_ChipInfo_t   chip_info;              ///< 模组生产信息
    uint8_t           i2c_dev_addr;           ///< i2c设备地址
    uint32_t          dev_pwr_state;          ///< 设备的当前状态, 就绪模式或者休眠模式
} ND03B_Dev_t;

/** @} ND03B_Dev_Group */

#endif

