/*
    This file list all the functions for user to implement.
*/

#include "nd03b_platform.h"
#include "device.h"

#define VHW_IIC_AMS vIIC_3
#define VHW_PIN_AMS_EN vPIN_C16

int32_t i2c_read_nbytes(uint8_t i2c_addr, uint16_t i2c_read_addr, uint8_t *i2c_read_data, uint8_t len)
{
    uint8_t reg[2] = {((i2c_read_addr & 0xff00) >> 8), i2c_read_addr & 0xff};
    if (Device_Write(VHW_IIC_AMS, &reg, 2, i2c_addr) == 0)
	{
		return (Device_Read(VHW_IIC_AMS, i2c_read_data, len, i2c_addr) == 0) ? 0 : 1;
	}
    return 1;
}

int32_t i2c_write_nbytes(uint8_t i2c_addr, uint16_t i2c_write_addr, uint8_t *i2c_write_data, uint8_t len)
{
    unsigned char buffer[130] = {0};
    buffer[1] = i2c_write_addr & 0xff;
    buffer[0] = (i2c_write_addr & 0xff00) >> 8;

	memcpy(&buffer[2], i2c_write_data, len);
	return (Device_Write(VHW_IIC_AMS, buffer, len + 2, i2c_addr) == 0) ? 0 : 1;
}

int32_t i2c_link_check(uint8_t i2c_addr)
{
    return Device_Write(VHW_IIC_AMS, NULL, 0, i2c_addr);
}


void delay_1ms(uint32_t count)
{
    Device_DelayMs(count);
}

void delay_10us(uint32_t count)
{
    Device_DelayUs(10 * count);
}


void set_xshut_pin_level(uint32_t level)
{
    Device_Write(VHW_PIN_AMS_EN, 0, 0, level);
}


