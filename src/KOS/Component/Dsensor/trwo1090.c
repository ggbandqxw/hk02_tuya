/**
  * @file       TRWO1090.c
  * @brief      物体靠近传感器组件
  * @copyright  Copyright (c) 2020~2030 ShenZhen Dxtc Tech. Co., Ltd.
  * All rights reserved.
  * @version    V1.0
  * @author     Qinxg
  * @date       20221-03-31
  * @note       鼎新同创·智能锁
  *				
  * 参见: TRWO1090-V1.1红外感应模块规格书.pdf
  * 硬件: 红外LED发射红外线, 经过物体反射后, 检测物体靠近
  * 设置: 长距离, 0.5S检测时间, 2秒超时保护, 接入阀值60, IO低电平模式, 指示灯关闭
  * 	  ( 即物体靠近时, 输出2秒的低电平脉冲; 如果一直不离开, 则输出不变,除非下次进入 )
  * 软件: 红外监测设置工具.exe
  * 逻辑: 中断在pin.c里设置, 低电平在input_port.c "BUT_D_SENSOR"里判定
  * 问题: 在开发使用过程中，如还有任何疑问之处，请联系开发者。电话或微信：18038061187
  *       价格便宜. 但是红外对人的衣物很敏感, 检测的距离不稳定, 不使用
  *****************************************************************************/
#include "osal.h"
#include "device.h"
#include "component.h"
#include "local_main.h"
#include "nvram.h"

#define DSENSOR_LOG(format, ...) 	OSAL_LOG(C_BLUE format C_NONE, ##__VA_ARGS__)


// 距离传感器中断使能/禁止标志. 菜单里设置"打开/关闭"距离感应
// 注意: 因为硬件上没有禁止本模块输出的电路, 只能使用 DSensor_enable 标志来禁止其中断
__NVRAM static FunctionalState DSensor_enable = 0;

/**
  * @brief  读取距离传感器使能/禁止标志
  *
  * @param  
  *
  * @return: 1使能; 0禁止
  */
FunctionalState DSensor_ReadEnableFlag( void )
{
	NVRAM_Restore( NVRAM_TYPE_NVRAM );
	return DSensor_enable;
}


/**
  * @brief  传感器进入测试模式
  * @note
  * @return SUCCESS/ERROR
  */
static ErrorStatus dsensor_set_demo_mode(uint8_t mode)
{
  	if( mode )
	{
		Device_Enable( vPIN_I13 );
		InputPort_EnableProt( "BUT_D_SENSOR" );
		DSENSOR_LOG( "dsensor open\r\n" );
	}
	return SUCCESS;
}


/**
  * @brief  距离传感器中断使能/禁止
  *
  * @param  event：当关闭/打开人脸识别功能的时候, 也关闭/打开距离传感器的IO中断
  *
  * @return 
  */
static ErrorStatus dsensor_power_ctrl( FunctionalState enable )
{
	DSensor_enable = enable;
	if( enable )
	{
		Device_Enable( vPIN_I13 );
		InputPort_EnableProt( "BUT_D_SENSOR" );
		DSENSOR_LOG( "dsensor open\r\n" );
	} else 
	{
		Device_Disable( vPIN_I13 );
		InputPort_DisableProt( "BUT_D_SENSOR" );
		DSENSOR_LOG( "dsensor close\r\n" );		
	}

	return SUCCESS;
}


/**
  * @brief  距离传感器任务函数
  *
  * @param  event：当前任务的所有事件
  *
  * @return 返回未处理的事件
  */
static uint32_t Dsensor_Task(uint32_t event)
{
	/* 系统启动事件 */
	if (event & EVENT_SYS_START)
	{
		DSENSOR_LOG( "dsensor start" );	
	
		dsensor_power_ctrl( DSensor_enable );
		SYS_API( DSensor_ReadEnableFlag );		
		SYS_API( dsensor_power_ctrl );
		SYS_API( dsensor_set_demo_mode );
		return (event ^ EVENT_SYS_START);
	}

	/* 系统休眠事件 */
	if (event & EVENT_SYS_SLEEP)
	{
		DSENSOR_LOG("dsensor sleep\r\n");
		return (event ^ EVENT_SYS_SLEEP);
	}

	if (event & EVENT_SYS_ISR)
	{
		return (event ^ EVENT_SYS_ISR);
	}

	return 0;
}
COMPONENT_TASK_EXPORT(COMP_DSENSOR, Dsensor_Task, 0);
