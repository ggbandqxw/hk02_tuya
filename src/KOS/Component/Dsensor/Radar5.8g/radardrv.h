#ifndef __RADARDRV_H__
#define __RADARDRV_H__

#include "device.h"

int32_t Radar_Sleep(void);
int32_t Radar_Wake(void);
int32_t Radar_SetThreshold(uint16_t value);
void Radar_Init(uint16_t threshold); //雷达模组初始化

#endif
