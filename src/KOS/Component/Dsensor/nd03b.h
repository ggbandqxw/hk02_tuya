/**
  * @file       nd03b.h
  * @brief      nd03b封装头文件
  * @copyright  Copyright (c) 2020~2030 ShenZhen Dxtc Tech. Co., Ltd.
  * All rights reserved.
  * @version    V1.0
  * @author     TianXubin
  * @date       2022-08-29 16:05:47
  * @note       鼎新同创·智能锁
  *
  *****************************************************************************/


#ifndef _ND03B_H
#define _ND03B_H

#include "device.h"

int32_t ND03B_IsValid(void);
int32_t ND03B_SetWorkMode(uint8_t mode);
int32_t ND03B_Calibration_XTalk(void);
int32_t ND03B_Calibration_Offset(void);
int32_t ND03B_StartCapture(void);
int32_t ND03B_ReadDistance(uint16_t *ps, uint8_t *cfi);
void ND03B_DrvInit(void);

#endif
