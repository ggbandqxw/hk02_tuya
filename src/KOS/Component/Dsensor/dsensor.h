#ifndef __DSENSOR_H__
#define __DSENSOR_H__

#include "osal.h"

typedef enum
{
    READ_DISTANCE, //读距离
    WANDER_TRIG,//徘徊触发
    CAL_RESULT,    //校准结果
} DsensorMsg_Type_t;

typedef struct
{
    uint8_t alarm;   //
    uint16_t voltage;   
    uint16_t irq;
}Wander_Msg_t;//徘徊消息

typedef struct
{
    UserAction_enum_t action; //动作
    uint8_t cfi;              //可信度
    uint16_t distance;        //距离
} Dsensor_DistanceMsg_t;

typedef struct
{
    uint8_t result; //校准结果
} Dsensor_CalMsg_t;

typedef struct
{
    DsensorMsg_Type_t type;
    union
    {
        Dsensor_DistanceMsg_t ps_msg;
        Dsensor_CalMsg_t cal_msg;
        Wander_Msg_t wander_msg;
    };
} DsensorMsg_t;

typedef enum
{
    DSENSOR_SENS_H = 1,
    DSENSOR_SENS_M,
    DSENSOR_SENS_L,
    DSENSOR_SENS_OFF,
}Dsensor_Sensitivity_enum_t;

/* 传感器校准，产测发送校准命令，校准成功后，会发送校准成功消息 */
static ErrorStatus dsensor_calibration(void);

static ErrorStatus dsensor_calibration_ex(uint8_t mode);

/* 传感器使能控制，默认为关闭 */
static ErrorStatus dsensor_power_ctrl(FunctionalState ctrl);

/* 设置传感器灵敏度 */
static ErrorStatus dsensor_set_sensitivity(Dsensor_Sensitivity_enum_t val);

/* 获取传感器灵敏度 */
static Dsensor_Sensitivity_enum_t dsensor_get_sensitivity(void);

/* 传感器设置测试模式 */
static ErrorStatus dsensor_set_demo_mode(uint8_t mode);

#endif
