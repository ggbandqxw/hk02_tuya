/**
  * @file 		TDKMode.h
  * @brief		基于TDKCH202的接近感应模组的驱动头文件
  *
  * @copyright  Copyright (c) 2017~2020 ShenZhen Dxtc Technology Co., Ltd. 
  * All rights reserved.
  *
  * @version	V0.00.002
  * @author		zhanggang
  * @date 		2022-08-26
  * 
  * @note		这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
  * 
  * @par 修改日志:
  * <1> 2022/08/26 V0.00.001 zhanggang 创建初始版本
  * <2> 2022/10/31 V0.00.002 Shenjiwei 增加注释
  *******************************************************************/
#ifndef __TDKMODE_DRIVER_H__
#define __TDKMODE_DRIVER_H__

#include <stdint.h>
#include <stdio.h>
#include "osal.h"
#include "component.h"

/** @brief IIC虚拟接口定义 */
#define VHW_IIC_TDKMODE vIIC_3


#define REGADDR_MODEL_TYPE_REGADDR   0x00                  ///< 模組类型寄存器地址
#define REGADDR_SOFT_VERSION_H  0x01;                      ///< 软件版本,例： V1.02 0x0102
#define REGADDR_SOFT_VERSION_L  0x02;                      ///< 软件版本,例： V1.02 0x0102
#define REGADDR_MAX_DISTANCE_WANDER_H  0x03                ///< 传感器徘徊检测最大距离，单位（mm）
#define REGADDR_MAX_DISTANCE_WANDER_L  0x04                ///< 传感器徘徊检测最大距离，单位（mm）
#define REGADDR_CYCLE_DETECTION_WANDER_H 0x05              ///< 传感器徘徊检测周期，单位（ms）
#define REGADDR_CYCLE_DETECTION_WANDER_L 0x06              ///< 传感器徘徊检测周期，单位（ms）
#define REGADDR_TRIGGER_THRESHOLD_WANDER_H   0x07          ///< 徘徊报警检测灵敏度阈值，高0，中1，低2，
#define REGADDR_TRIGGER_THRESHOLD_WANDER_L   0x08          ///< 徘徊报警检测触发阈值，小于此距离徘徊检测触发，
#define REGADDR_TIME_STAY_WANDER_H  0x09                   ///< 在徘徊检测区域停留多长时间触发报警，最大60s,单位（ms）
#define REGADDR_TIME_STAY_WANDER_L  0x0A                   ///< 在徘徊检测区域停留多长时间触发报警，最大60s,单位（ms）
#define REGADDR_MAX_DISTANCE_NEAR_H  0x0B                  ///< 接近感应有效探测距离，小于此距离属于接近感应有效范围，单位（mm）,最大1000mm
#define REGADDR_MAX_DISTANCE_NEAR_L  0x0C                  ///< 接近感应有效探测距离，小于此距离属于接近感应有效范围，单位（mm）,最大1000mm
#define REGADDR_TIME_STAY_NEAR_H  0x0D                     ///< 在接近感应检测区域停留多长时间触发，最大10s,
#define REGADDR_TIME_STAY_NEAR_L  0x0E                     ///< 在接近感应检测区域停留多长时间触发，最大10s,
#define REGADDR_CYCLE_DETECTION_NEAR_H  0x0F               ///< 传感器接近感应检测周期，单位（ms）
#define REGADDR_CYCLE_DETECTION_NEAR_L  0x10               ///< 传感器接近感应检测周期，单位（ms）
#define REGADDR_TRIGGER_THRESHOLD_NEAR_H  0x11             ///< 接近感应触发阈值，小于此距离进入接近感应停留判断，单位（mm）
#define REGADDR_TRIGGER_THRESHOLD_NEAR_L  0x12             ///< 接近感应触发阈值，小于此距离进入接近感应停留判断，单位（mm）
#define REGADDR_REALTRIGGER_DISTANCE_WANDER_H 0x13         ///< 徘徊触发的实际距离，单位（mm）
#define REGADDR_REALTRIGGER_DISTANCE_WANDER_L 0x14         ///< 徘徊触发的实际距离，单位（mm）
#define REGADDR_REALTRIGGER_DISTANCE_NEAR_H  0x15          ///< 接近感应触发的实际距离，单位（mm）
#define REGADDR_REALTRIGGER_DISTANCE_NEAR_L  0x16          ///< 接近感应触发的实际距离，单位（mm）
#define REGADDR_TRIGGER_FLAG  0x17                         ///< Bit0:0 徘徊触发  1 徘徊未触发； Bit1:0 接近感应触发 1  接近感应未触发（读数据后清除）
#define REGADDR_WORK_STATE    0x18                         ///< Bit0：0 徘徊检测关 1 徘徊检测开 ; Bit1：0 接近感应关 1 ;接近感应开 Bit2：0 超声波传感器正常 1 超声波传感器异常
#define REGADDR_TRIGGER_FREQUENCY_WANDER   0x19            ///< 徘徊检测探测到的触发次数，触发对应次数触发报警，单位（次）
#define REGADDR_TRIGGER_FREQUENCY_NEAR   0x1A              ///< 接近感应探测到的触发次数，触发对应次数触发事件，单位（次）
#define REGADDR_STRENGTH_TRIGGER_THRESHOLD_WANDER_H 0x1B   ///< 徘徊检测超声波强度阈值，强度变化超过这个值，有效触发，可设置来调节触发灵敏度
#define REGADDR_STRENGTH_TRIGGER_THRESHOLD_WANDER_L 0x1C   ///< 徘徊检测超声波强度阈值，强度变化超过这个值，有效触发，可设置来调节触发灵敏度
#define REGADDR_STRENGTH_TRIGGER_THRESHOLD_NEAR_H   0x1D   ///< 接近感应超声波强度阈值，强度变化超过这个值，有效触发，可设置来调节触发灵敏度
#define REGADDR_STRENGTH_TRIGGER_THRESHOLD_NEAR_L   0x1E   ///< 接近感应超声波强度阈值，强度变化超过这个值，有效触发，可设置来调节触发灵敏度
#define REGADDR_ON_OFF_STATE   0x1F                        ///< 超声波(TDK)传感器使能开关——总开关
//////////////////////////////////////////////////////////////

typedef struct
{
    uint8_t model_type;                         ///< 模组类型，默认值0xE2
    uint16_t soft_version;                      ///< 软件版本,例： V1.02 0x0102
    uint16_t max_distance_wander;               ///< 传感器徘徊检测最大距离，单位（mm）
    uint16_t cycle_detection_wander;            ///< 传感器徘徊检测周期，单位（ms）
    uint16_t trigger_threshold_wander;          ///< 徘徊报警检测触发阈值，小于此距离徘徊检测触发，
    uint16_t time_stay_wander;                  ///< 在徘徊检测区域停留多长时间触发报警，最大60s,单位（ms）
    uint16_t max_distance_near;                 ///< 接近感应有效探测距离，小于此距离属于接近感应有效范围，单位（mm）,最大1000mm
    uint16_t time_stay_near;                    ///< 在接近感应检测区域停留多长时间触发，最大10s,
    uint16_t cycle_detection_near;              ///< 传感器接近感应检测周期，单位（ms）
    uint16_t trigger_threshold_near;            ///< 接近感应触发阈值，小于此距离进入接近感应停留判断，单位（mm）
    uint16_t realtrigger_distance_wander;       ///< 徘徊触发的实际距离，单位（mm）
    uint16_t realtrigger_distance_near;         ///< 接近感应触发的实际距离，单位（mm）
    uint8_t  trigger_flag;                      ///< Bit0:0 徘徊未触发  1 徘徊触发； Bit1:0 接近感应未触发 1  接近感应触发（读数据后清除）
    uint8_t  work_state;                        ///< Bit0：0 徘徊检测关 1 徘徊检测开 ; Bit1：0 接近感应关 1 ;接近感应开 Bit2：0 超声波传感器正常 1 超声波传感器异常
    uint8_t  trigger_frequency_wander;          ///< 徘徊检测探测到的触发次数，触发对应次数触发报警，单位（次）
    uint8_t  trigger_frequency_near;            ///< 接近感应探测到的触发次数，触发对应次数触发事件，单位（次）
    uint16_t strength_trigger_threshold_wander; ///< 徘徊检测超声波强度阈值，强度变化超过这个值，有效触发，可设置来调节触发灵敏度
    uint16_t strength_trigger_threshold_near;   ///< 接近感应超声波强度阈值，强度变化超过这个值，有效触发，可设置来调节触发灵敏度

} tdkmodel_config_t;

/** @brief 徘徊传感器灵敏度 */
typedef enum
{
    TDKSENSOR_SENS_H = 1,   ///< 高灵敏度
    TDKSENSOR_SENS_M,       ///< 中灵敏度
    TDKSENSOR_SENS_L,       ///< 低灵敏度
}TDKsensor_Sensitivity_enum_t;
#endif
