#ifndef __COMPONENT_H__
#define __COMPONENT_H__

/* 组件名称（也是组件的唯一ID，也表示组件里面Task的优先级） */
typedef enum
{
    COMP_BUTTON,
    COMP_EXTEND_KDS,
    COMP_MC60_UART,
    COMP_TOUCH,
    COMP_LOCK,
    COMP_AUDIO,
    COMP_IRSENSOR,
    COMP_DBATTERY,
    COMP_LED,
    COMP_BEEP,
    COMP_EXTEND_IPC,//10
    COMP_XUNMEI,
    COMP_FACE,
    COMP_CARD,
    COMP_BATTERY,
    COMP_PSENSOR,
    COMP_GSENSOR,
    COMP_MSENSOR,
    COMP_DSENSOR,
    COMP_RECORD,
    COMP_FINGER,//20
    COMP_USERS,
    COMP_YMODEM,
    COMP_DFU,
    COMP_BLE,
    COMP_NB,
    COMP_WIFI,
    COMP_NETWORK,
    COMP_FACE_OTA,
    COMP_OLED,
    COMP_PIR,
    COMP_EXTEND_KDS_OTA,
    COMP_CAMERA,
    COMP_WIFI_CFG,
    COMP_FINGERVEIN,    //指静脉
    COMP_FINGERVEIN_OTA,
    COMP_LCD,
    COMP_EXTEND_MIJIA,
    COMP_EXTEND_MIJIA_OTA,
    COMP_PALMVEIN,
    COMP_BATTERY_ENCRY,
    COMP_RTC,
    COMP_MCUOTA,
    COMP_SYS_MSG = 0xF0,
} ComponentName_enum_t;

/* 用户动作类型 */
typedef enum
{
    USER_ACTION_INVALID,    //无效值
    USER_ACTION_RELEASE,    //松开了（按钮松开、触摸键松开、门开了……）
    USER_ACTION_PRESS,      //按下了
    USER_ACTION_LONG_PRESS, //长按
} UserAction_enum_t;

/* 工作模式 */
typedef enum
{
    WORK_MODE_VERIFY, //验证模式
    WORK_MODE_ADD,    //录入模式
    WORK_MODE_DISABLE //禁用模式
} WorkMode_enum_t;

/* 错误码类型 */
typedef enum
{
    ERRCODE_TYPE_FLASH,
    ERRCODE_TYPE_MSENSOR,
    ERRCODE_TYPE_PSENSOR,
    ERRCODE_TYPE_DSENSOR,
    ERRCODE_TYPE_NFC,
    ERRCODE_TYPE_BATTERY,
    ERRCODE_TYPE_FINGER,
    ERRCODE_TYPE_GSENSOR,
    ERRCODE_TYPE_IRSENSOR,
    ERRCODE_TYPE_OLED,
    ERRCODE_TYPE_FACE,
    ERRCODE_TYPE_EXTEND,
    ERRCODE_TYPE_XUNMEI,
    ERRCODE_TYPE_COM_FRONT_REAR,
    ERRCODE_TYPE_COM_FRONT_WIFI,
    ERRCODE_TYPE_COM_FRONT_TOUCH,
    ERRCODE_TYPE_RTC,
    ERRCODE_TYPE_FINGERVEIN,        //指静脉
    ERRCODE_TYPE_PIRSENSOR,         //PIR
    ERRCODE_TYPE_CAMERA,            //摄像头
    ERRCODE_TYPE_PALMVEIN,
} ErrorCode_enum_t;

#include "osal.h"

#if !(defined(HI3816_CHIP_PLATFORM) || defined(ESP32_CHIP_PLATFORM))
#include "Audio/audio.h"
#include "Led/led.h"
#include "Lock/lock.h"
#include "Gsensor/gsensor.h"
#include "Msensor/msensor.h"
#include "Button/button.h"
#include "Touch/touch.h"
#include "Beep/beep.h"
#include "Battery/battery.h"
#include "Bat_Encrypt/bat_encrypt.h"
#include "Extend_kds/extend_kds.h"
#include "Records/records.h"
#include "Oled/oled.h"
#include "Users/users.h"
#include "XunMei/xunmei.h"
#include "Face/face.h"
#include "FacePro/face.h"
#include "IRsensor/IRsensor.h"
#include "Card/card.h"
#include "Finger/finger.h"
#include "Psensor/psensor.h"
#include "Dsensor/dsensor.h"
#include "Dsensor/TDKMode.h"
#include "Dbattery/dbattery.h"
#include "Ymodem/ymodem.h"
#include "DFU/dfu.h"
#include "Wifi/wifi.h"
#include "Network/network.h"
#include "Face_OTA/face_ota.h"
#include "Extend_kds_OTA/Extend_kds_ota.h"
#include "Camera/comp_camera.h"
#include "Wifi_cfg/wifi_cfg.h"
#include "Fingervein/Fingervein.h"
#include "PIR/pirsensor.h"
#include "Ble/ble.h"
#include "Fingervein/fingervein.h"     //指静脉
#include "Fingervein_OTA/fingervein_ota.h"     //指静脉
#include "Lcd/lcd.h"//7789 lcd
#include "Extend_MiJia/extend_mijia.h" 
#include "Extend_MiJia_OTA/Extend_MiJia_ota.h" 
#include "Palmvein/palmvein.h"     //掌静脉
#include "RTC/rtc_comp.h"
#include "Mc60/mc60_uart.h" 
#include "Lock/LockDai_3opto/motor_dai_3opto.h"
#endif
#define KOS_VERSION "V142"

#endif /* __COMPONENT_H__ */
