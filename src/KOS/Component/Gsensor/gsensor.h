#ifndef __GSENSOR_H__
#define __GSENSOR_H__



/* Gsensor组件消息类型（单击：1， 双击：2） */
typedef uint8_t GsensorMsg_t;



/* 设置Gsensor触发级别(高、中、低) */
static ErrorStatus Gsensor_SetLevel(uint8_t level);
static uint8_t Gsensor_GetLevel(void);
/* 设置Gsensor使能/禁能  user id 代表谁使能禁能的*/
static ErrorStatus Gsensor_SetStatus(FunctionalState status, uint32_t time ,uint8_t userId);
/* 获取Gsensor在self-test模式的自测结果 */
static ErrorStatus Gsensor_self_test(void);
static uint8_t Gsensor_get_level(void);//获取灵敏度

#endif /* __GSENSOR_H__ */
