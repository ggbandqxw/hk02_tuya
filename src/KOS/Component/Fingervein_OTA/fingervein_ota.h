#ifndef _FINGERVEIN_OTA_H_
#define _FINGERVEIN_OTA_H_

#include "component.h"
#include "device.h"




typedef struct
{
	uint16_t psn;	// the packet id
	uint16_t size; // the size of this package
	uint8_t *data; // data 0 start
} Fingervein_OTA_DataPacket;

typedef enum
{
	FINGERVEIN_OTA_START,			//ota开始
	FINGERVEIN_OTA_READY,			//ota准备就绪
	FINGERVEIN_OTA_SUCCESS,			//ota成功
	FINGERVEIN_OTA_ERROR,			//ota失败
}FingerveinOTAResult_enum_t;

typedef struct
{
	FingerveinOTAResult_enum_t ota_Result;		//ota结果
}FingerveinOTAMsg_t;

static ErrorStatus Fingervein_OTA_Start(uint32_t fileLen);								//API,启动指静脉的OTA；
static ErrorStatus Fingervein_Recv_OTA_FileData(Fingervein_OTA_DataPacket *pdata);		//API,指静脉组件接收应用层发送过来的OTA数据



#endif