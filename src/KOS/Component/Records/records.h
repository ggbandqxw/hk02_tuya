#ifndef __RECORDS_H__
#define __RECORDS_H__


/* 事件记录eventSource（用户类型）（密钥类型） */
//! 详见users.h里面 USERS_TYPE开头的宏

/* 事件记录userID（密钥编号）（这里是特殊密钥编号） */
//! 详见users.h里面 USERS_ID开头的宏

/* 事件记录eventType（记录类型） */
#define EVENT_TYPE_OPEN_CLOSE       0X01 //开关锁记录
#define EVENT_TYPE_PROGRAM          0X02 //操作记录（增删改记录）
#define EVENT_TYPE_ALARM            0X03 //报警记录
#define EVENT_TYPE_SENEOR           0X04 //传感器类型
#define EVENT_TYPE_ALL              0XFF //所有记录

/* 事件记录（eventCode）（当eventType为OPEN_CLOSE时） WiFi也调用此处*/
#define EVENT_CODE_LOCK             0X01 //上锁 
#define EVENT_CODE_UNLOCK           0X02 //开锁
#define EVENT_CODE_ONE_TOUCH_LOCK   0X07 //长按触摸键盘一键上锁
#define EVENT_CODE_MECH_LOCK        0X08 //机械方式上锁
#define EVENT_CODE_MECH_UNLOCK      0X09 //机械方式开锁
#define EVENT_CODE_AUTO_LOCK        0X0A //自动上锁
#define EVENT_CODE_KNOCK_UNLOCK     0X10 //敲击开锁
#define EVENT_CODE_ONE_TOUCH_UNLOCK 0X11 //触摸唤醒开锁（启动敲击开锁时，未敲击直接触摸唤醒了就会开锁）
#define EVENT_CODE_ANTI             0X12 //反锁（隐私模式）

/* 事件记录（eventCode）（当eventType为传感器类型时） */
#define EVENT_CODE_MSENSOR_OPEN     0X01 //门磁检测开门 
#define EVENT_CODE_MSENSOR_CLOSE    0X02 //门磁检测关门
#define EVENT_CODE_MSENSOR_ERROR    0X03 //门磁异常

/* 事件记录（eventCode）（当eventType为Program时） WiFi也调用此处 */
#define EVENT_CODE_MODIFY           0X01 //修改密码
#define EVENT_CODE_ADD              0X02 //添加
#define EVENT_CODE_DELETE           0X03 //删除
#define EVENT_CODE_RESTORE          0X0F //恢复出厂
#define EVENT_CODE_OPEN_ANTI        0XF0 //开启反锁
#define EVENT_CODE_CLOSE_ANTI       0XF1 //关闭反锁
#define EVENT_CODE_OPEN_ARMING      0XF2 //开启布防
#define EVENT_CODE_CLOSE_ARMING     0XF3 //关闭反锁
#define EVENT_CODE_RESET            0XF4 //软件复位

//以下类型为了上传B1 B4指令，不保存记录
#define EVENT_CODE_DOORSEN_OPEN     0X29 //门磁状态开
#define EVENT_CODE_DOORSEN_CLOSE    0X2A //门磁状态关
#define EVENT_CODE_DOORSEN_ERROR    0X2B //门磁状态异常

/* 事件记录（eventCode）（当eventType为Alarm时） */
#define EVENT_CODE_SYS_LOCK         0X01 //系统锁定报警
#define EVENT_CODE_HIJACK           0X02 //劫持报警、胁迫
#define EVENT_CODE_ERR_THREE        0X03 //三次错误报警
#define EVENT_CODE_PRY_LOCK         0X04 //撬锁报警（锁被撬开）
#define EVENT_CODE_MECHANIC_KEY     0X08 //机械钥匙报警（使用机械钥匙开锁）
#define EVENT_CODE_LOW_POWER        0X10 //低电压报警（电池电量不足）
#define EVENT_CODE_ILLEGAL_BAT      0X11 //非法电池
#define EVENT_CODE_LOCK_ERR         0X20 //门锁异常报警
#define EVENT_CODE_ARMING           0X40 //门锁布防报警（门外布防后，从门内开锁了就会报警）
#define EVENT_CODE_ROTOR_ARMING     0x41 //堵转报警
#define EVENT_CODE_DOOR_BELL        0X60 //门铃报警
#define EVENT_CODE_PIR              0X70 //PIR报警
#define EVENT_CODE_HOVER            0X50 //（人脸试错）徘徊报警——勿删
#define EVENT_CODE_SYSLOCK          0X09 //凭证试开

/* 事件记录（appID） */
#define APP_ID_LOCAL                0X00 //本地操作产生的记录
#define APP_ID_BLE                  0X01 //蓝牙命令产生的记录
#define APP_ID_WIFI                 0X02 //WIFI命令产生的记录

/* 最大存储记录数量 */
#define RECORD_MAX_QTY              600 



/* 事件记录内容（9字节） */
#pragma pack(1)
typedef struct
{
    uint8_t  eventType;
    uint8_t  eventSource;
    uint8_t  eventCode;
    uint8_t  userID;
    uint8_t  appID;
    uint32_t timestamp;
}EventRecord_stu_t;
#pragma pack()


/* Records组件API函数类型 */
static ErrorStatus Record_Delete(void);
static ErrorStatus Record_Save(EventRecord_stu_t *record);
static ErrorStatus Record_Read(EventRecord_stu_t *record, uint16_t number, uint8_t eventType);
static uint16_t    Record_QuickGet(uint8_t *record, uint16_t startNum, uint16_t endNum);
static uint32_t    Record_GetLastTimestamp(void);
static uint16_t    Record_GetQty(uint16_t start, uint16_t end, uint8_t eventType);
static uint16_t    Record_GetByTime(uint32_t timestamp);
static uint16_t Record_GetRecodeListHeadPos(void);
static uint8_t Record_GetRecordRule(EventRecord_stu_t *pRecord, uint16_t *pNum, uint16_t cur_pos);
#endif /* __RECORDS_H__ */
