#ifndef __FACE_COMMON_H
#define __FACE_COMMON_H

#include "device.h"
#include "face.h"

/* 定义默认的指密卡人脸用户的数量 */
#if defined(DXTC_NO_MENU)
#define KOS_PARAM_USERS_QTY_FACE   50
#else
#ifndef KOS_PARAM_USERS_QTY_FACE
#define KOS_PARAM_USERS_QTY_FACE   20
#endif
#endif

//#define FACE_DEBUG //debug模式下可以打印的日志更全面

#define FIRMWARE_PACKAGE_LEN 0		//固件包长度
#define VERSION_INFO_BUFFER_SIZE 32 //人脸模组固件版本信息长度
#define FACE_MODEL_LEN 6            //人脸模组型号长度
#define FACE_VERSION_LEN 10			//人脸版本号长度
#define FACE_SN_LEN 20				//人脸sn长度
#define MAX_FACE_NUM   KOS_PARAM_USERS_QTY_FACE				//人脸最大数量

/* 人脸事件*/
#define EVENT_FACE_PROTOCOL (0X00000001)		//处理人脸协议事件
#define EVENT_MASTER_TO_FACS_MSG (0X00000002)	//主控发送给人脸任务的事件
#define EVENT_FACE_MODULE_MSG (0x00000004)		//发送给人脸模组的事件
#define EVENT_FACE_TIME_OUT (0x00000008)		//人脸任务超时事件
#define EVENT_ZIGBEE_PROTOCOL (0x00000010)		//人脸zigbee消息处理
#define EVENT_CHECK_OTA_DMA_STATUS (0x00000020) //检测人脸DMA状态

#define FACE_DELETE_ALL_NUN 0xFF //全部删除编号
#define INVALID_NUM 0xFE		 //无效编号

#define FACE_NORMAL_TIMEOUT 2000							//一般超时时间
#define FACE_REMINDER_INTERVAL 2100							//人脸转头提醒间隔 2100
#define FACE_ONE_SEC_OF_MSEC 1000							//1s = 1000ms
#define FACE_MODULE_DEAL_TIMEOUT_SMALL 500					//人脸模组处理设置时间相对于配置时间减少500ms
#define FOREVER_TIME_OUT (0xFFFFU)							//永远不超时
#define IMMEDIATELY_TIME_OUT 0x00							//立即超时
#define FACE_POWERING_ON_TIMEOUT 2000						//人脸模组上电超时时间为2000ms
#define FACE_VERITIFYING_TIMEOUT 5500						//人脸验证超时时间为5000ms-500
#define FACE_POWERDOWN_TIMEOUT FACE_NORMAL_TIMEOUT			//休眠掉电超时时间
#define FACE_IDLE_SENDING_TIMEOUT 2000						//发送进入空闲状态
#define FACE_IDLE_TIMEOUT 1500								//空闲模式，1.5超时  1500
#ifdef DXTC_NO_MENU
#define FACE_ENROLLING_TIMEOUT 40000						//录入超时
#else
#define FACE_ENROLLING_TIMEOUT 15500						//录入超时
#endif
#define FACE_DELETING_TIMEOUT FACE_NORMAL_TIMEOUT			//删除超时
#define FACE_FACE_RESET_TIMEOUT FACE_NORMAL_TIMEOUT			//清除人脸录入
#define FACE_MID_START_OTA_TIMEOUT FACE_NORMAL_TIMEOUT		//启动OTA指令超时时间
#define FACE_MID_GET_OTA_STATUS_TIMEOUT FACE_NORMAL_TIMEOUT //获取模块OTA状态指令超时时间
#define FACE_SEND_FIRMWARE_TIMEOUT 10000					//发送升级固件超时
#define FACE_OTA_EXCUTING_TIMEOUT 120000					//人脸升级超时2分钟
#define FACE_SET_EXCUTING_TIMEOUT 5000						//人脸设置执行超时时间

//face direction parameters
#define FACE_DIR_AMEND 1					 //修正参数
#define FACE_MIDDLE_MAX 10 - FACE_DIR_AMEND	 //正视
#define FACE_MIDDLE_MIN -10 + FACE_DIR_AMEND //正视
#define FACE_LEFT_MAX -10 - FACE_DIR_AMEND	 //左脸
#define FACE_LEFT_MIN -45 + FACE_DIR_AMEND
#define FACE_RIGHT_MAX 45 - FACE_DIR_AMEND //右脸
#define FACE_RIGHT_MIN 10 + FACE_DIR_AMEND
#define FACE_UP_MAX -10 - FACE_DIR_AMEND //抬头
#define FACE_UP_MIN -45 + FACE_DIR_AMEND
#define FACE_DOWN_MAX 45 - FACE_DIR_AMEND //低头
#define FACE_DOWN_MIN 10 + FACE_DIR_AMEND
#define FACE_ROLL_MAX 20 - FACE_DIR_AMEND //歪头
#define FACE_ROLL_MIN -20 + FACE_DIR_AMEND

typedef enum
{
	ALLOW = 0,
	DISALLOW = !ALLOW
} AllowStatus;


/*人脸状态枚举*/
typedef enum
{
	FACE_STATUS_SLEEPED,		   	//休眠状态：模块休眠
	FACE_STATUS_POWER_DOWN,			//断电状态
	FACE_STATUS_POWER_ON,		   	//上电状态
	FACE_STATUS_ENCRYPTION,			//加密状态
	FACE_STATUS_EXECUTE, 			//执行状态：验证，录入，升级，删除：执行命令
	FACE_STATUS_IDLE,				//空闲状态
	FACE_STATUS_OTA,				//ota状态
} Face_status_enum_t;


/*人脸事件枚举*/
typedef enum
{

	FACE_ENENT_TIMEOUT,					//超时
	FACE_EVENT_POWER_ON_READY,	 		//人脸模组上电完成
	FACE_EVENT_POWER_ON_TIMEOUT,	 	//人脸模组上电超时
	FACE_EVENT_START_VERIFY,			//启动人脸验证
	FACE_EVENT_START_ENROLL,			//启动人脸录入
	FACE_EVENT_START_DELETE,		 	//启动人脸删除
	FACE_EVENT_START_VERIFY_DELETE,		//启动人脸验证删除
	FACE_EVENT_DEBUG_ENC_READY,			//临时秘钥完成
	FACE_EVENT_INIT_ENCRYPTION_READY,	//随机数初始化成功
	FACE_EVENT_VERIFY_SUCCESS,	 		//人脸验证成功
	FACE_EVENT_VERIFY_FAIL,	 			//人脸验证失败
	FACE_EVENT_ENROLL_SUCCESS,			//人脸录入成功
	FACE_EVENT_ENROLL_FAIL,				//人脸录入失败
	FACE_EVENT_DELETE_FINISH,			//人脸删除完成
	FACE_EVENT_NID_FACE_STATUS,			//人脸录入和验证人脸状态信息返回
	FACE_EVENT_ABORT_FINISH,			//人脸终止当前操作完成
	FACE_EVENT_SLEEP_FINISH,			//人脸睡眠
	FACE_EVENT_START_OTA,			 	//启动OTA
	FACE_EVENT_OTA_READY,			 	//OTA启动完成
	FACE_EVENT_OTA_STATUS_FINISH,		//OTA状态完成
	FACE_EVENT_OTA_FINISH,				//OTA结束
	FACE_EVENT_GET_ALL_USERID,			//获取全部注册id
	FACE_EVENT_GET_VERSION,				 //获取版本号
	FACE_EVENT_SET_DEMO_MODE,			//设置演示模式
	FACE_EVENT_SET_WORK_MODE,			//设置工作模式
	FACE_EVENT_SET_RELEASE_ENC_KEY,		//设置量产秘钥
	FACE_EVENT_SET_FINISH,				 //设置完成
	FACE_MAX_EVENT,						 //事件最大值（无效）
} Face_event_enum_t;


/*人脸模组ota过程状态枚举*/
typedef enum
{
	OTA_IDLE_STATUS,					   //OTA空闲状态
	OTA_SENDING_START_OTA_STATUS,		   //正在发送升级指令
	OTA_SENDING_GET_OTA_STATUS_STATUS,	   //正在发送获取OTA状态指令
	OTA_SENDING_CONFIG_BAUDRATE_STATUS,	   //发送设置波特率指令
	OTA_ZSENDING_OTA_READY_STATUS,		   //发送ota准备就绪
	OTA_ZSENDING_CONFIG_BAUDRATE_STATUS,   //设置zigbee的波特率
	OTA_ZSENDING_GET_FIRMWARE_INFO_STATUS, //获取固件信息
	OTA_ZSENDING_GET_CHECK_SUM_STATUS,	   //获取校验值
	OTA_SENDING_OTA_HEADER_INFO_STATUS,	   //发送固件包头信息状态
	OTA_ZSENDING_START_SENDING_FIRMWARE,   //开始发送固件包
	OTA_SENDING_OTA_PACKET_STATUS,		   //转发固件包
	OTA_MAX_STATUS,						   //无效状态
} FaceOtaStatus_enum_t;

/*人脸结果*/
typedef enum
{
	RESULT_SUCCESS_REPORT,		 //成功
	RESULT_FAIL_REPORT,			 //失败
	RESULT_VERIFY_FAIL_REPORT,	 //验证失败
	RESULT_CONTINULE,			 //继续下一步
	RESULT_FACE_ENROLLED,		 //人脸已存在
	RESULT_END,					 //结束
	RESULT_NO_FACE,				 //未检测到人脸
	RUSULT_MODULE_BREAKDOWN,	 //模组异常
	RUSULT_OTA_SENDING_FIRMWARE, //正在发送升级包
	RESULT_ENCRYPTED,			 //已加密
} Face_Result_enum_t;

/*人脸OTA事件ID*/
typedef enum
{
	OTA_MSGID_RECEIVE_START_OTA_EVEN,			 //收到启动OTA
	OTA_MSGID_SEND_START_OTA_RESULT,			 //发送启动OTA结果
	OTA_MSGID_NID_OTA_DONE_RESULT,				 //note OTA状态信息
	OTA_MSGID_SEND_GET_OTA_STATUS_RESULT,		 //发送获取OTA状态结果
	OTA_MSGID_SENG_CONFIG_BAUDRATE_RESULT,		 //设置波特率状态结果
	OTA_MSGID_ZSENG_GET_FIRMWARE_INFO_RESULT,	 //获取固件信息
	OTA_MSGID_ZSENG_GET_CHECK_SUM_RESULT,		 //获取固件校验值
	OTA_MSGID_ZSENG_OTA_READY_RESULT,			 //OTA准备就绪
	OTA_MSGID_ZSEND_CONFIG_BAUDRATE_RESULT,		 //修改zigbee串口波特率
	OTA_MSGID_SEND_OTA_HEADER_RESULT,			 //发送固件包头信息结果
	OTA_MSGID_RECEIVE_START_SEND_PACKET_EVEN,	 //收到启动发送OTA升级包
	OTA_MSGID_ZSEND_GET_FIRMWARE_PACKAGE_RESULT, //获取固件包结果
	OTA_MSGID_SEND_FIRMWARE_PACKAGE_RESULT,		 //给人脸模组发送OTA升级包结果
	SET_MSGID_SET_DEMO_MODE,					 //设置为演示模式
	SET_MSGID_SET_WORK_MODE,					 //设置为工作模式
	SET_MSGID_GET_VERSION,						 //获取人脸模组软件版本号
	SET_MSGID_SET_RELEASE_ENC,					 //设定量产加密秘钥序列
	FACE_TASK_MSGID_MAX,						 //无效值
} Face_Msg_Id_enum_t;


typedef enum
{
	OPERATE_ID_ENROLL,			//录入人脸
	OPERATE_ID_VERIFY,			//验证人脸
	OPERATE_ID_VERIFY_DELETE,	//验证删除人脸
	OPERATE_ID_DELETE,			//删除人脸
	OPERATE_ID_ABORT,			//终止人脸
	OPERATE_ID_SET_DEMO_MODE,	//设置演示模式		
	OPERATE_ID_SET_WORK_MODE,	//设置工作模式
	OPERATE_ID_SET_RELEASE_ENC,	//设置量产秘钥
	OPERATE_ID_GET_VERSION,		//获取版本号
	OPERATE_ID_START_OTA,		//启动OTA
	OPERATE_ID_MAX,

}Face_operate_id_enum_t;


// typedef void (*Master_Msg_Cb_fun_t)(uint8_t result, uint8_t *pData, uint8_t dataLen);

#pragma pack(1)
/* 主控发给人脸任务的消息结构体 */
typedef struct
{
	Face_operate_id_enum_t actionId; //动作id
	uint8_t startNum;					//开始编号
	uint8_t endNum;						//结束编号
	uint8_t del_mode;
} Face_Param_stu_t;

/*人脸任务处理参数*/
typedef struct
{
	Face_event_enum_t event;		//事件
	Face_Result_enum_t result;	//结果
	uint16_t datalen;				//数据长度
	void *data;						//数据
} Face_DealParam_stu_t;

typedef struct
{
	Face_Msg_Id_enum_t resultId;
	void *pdata;
} Face_OtaParam_stu_t;

/*人脸固件包传输结果数据值*/
typedef struct
{
	Face_Result_enum_t result;
} Face_OtaSendFirmware_Result_stu_t;

#pragma pack()

#endif
