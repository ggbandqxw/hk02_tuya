#ifndef _FACE_BOX_STRUCT_H
#define _FACE_BOX_STRUCT_H

#include "face.h"

static void Face_SleepedIdle_StatusFun(Face_DealParam_stu_t *param);
static void Face_PoweringOn_StatusFun(Face_DealParam_stu_t *param);
static void Face_VerityingExcuteOrder_StatusFun(Face_DealParam_stu_t *param);
static void Face_SleepingSending_StatusFun(Face_DealParam_stu_t *param);
static void Face_VerityingFinished_StatusFun(Face_DealParam_stu_t *param);
static void Face_IdleSending_StatusFun(Face_DealParam_stu_t *param);
static void Face_EnrollingExcuteOrder_StatusFun(Face_DealParam_stu_t *param);
static void Face_EnrollingFinished_StatusFun(Face_DealParam_stu_t *param);
static void Face_DeletingExcuteOrder_StatusFun(Face_DealParam_stu_t *param);
static void Face_deletingFinished_StatusFun(Face_DealParam_stu_t *param);
static void Face_OTA_ExcuteOrder_StatusFun(Face_DealParam_stu_t *param);
static void Face_OtaFinished_StatusFun(Face_DealParam_stu_t *param);
static void Face_SetingFinished_StatusFun(Face_DealParam_stu_t *param);
static void Face_SetingExcuteOrder_StatusFun(Face_DealParam_stu_t *param);
static void Face_Encrypting_StatusFun(Face_DealParam_stu_t *param);
static void Face_EncrypFinish_StatusFun(Face_DealParam_stu_t *param);
static ErrorStatus Face_Sending_Start_OTA(Face_OtaParam_stu_t *param);

/*人脸各状态处理函数*/
typedef void (*FaceTackFunction_fun_t)(Face_DealParam_stu_t *param);

/*人脸升级子状态处理函数*/
typedef ErrorStatus (*pFace_FaceOtaStep_Fun)(Face_OtaParam_stu_t *param);

#pragma pack(1)

/*状态信息*/
typedef struct
{
    Face_status_enum_t status; //状态
    AllowStatus allowBreakIn;  //当前人脸执行流程    ALLOW:允许打断；    DISALLOW:不允许打断 ,允许终止当前操作
    FlagStatus busy;           //当前是否忙状态， SET：忙（只能被终止指令打断，其它事件无法打断）    RESET：可接受外部指令
    uint32_t timeout;          //超时时间
} FaceInform_stu_t;

/*状态处理函数*/
typedef struct
{
    Face_status_enum_t Status;       //当前状态
    FaceTackFunction_fun_t pFuntion; //事件处理函数
} FaceDeal_stu_t;

/*状态事件切换*/
typedef struct
{
    Face_status_enum_t curStatus;  //当前状态
    Face_event_enum_t event;       //发生的事件
    Face_mode_enum_t nextMode;     //下一模式
    Face_status_enum_t nextStatus; //下一状态
} FaceSwitch_stu_t;

typedef struct
{
    Face_mode_enum_t mode;
    uint8_t len;
    void *const pBoxData;
} FaceBoxData_stu_t;

/*OTA升级子状态处理和切换*/
typedef struct
{
    FaceOtaStatus_enum_t curOtaStatus;  //当前状态
    pFace_FaceOtaStep_Fun pFuntion;     //处理函数
    FaceOtaStatus_enum_t nextOtaStatus; //下一状态 (PFuntion 返回 SUCCESS：切换到下一状态   FAIL：保持当前状态)
} FaceOtaDealSwitch_stu_t;

typedef struct
{
    uint8_t len;
    void *const pOtaBoxData;
} FaceOtaBoxData_stu_t;

#pragma pack()

static FaceOtaDealSwitch_stu_t otaDealSwitchBox[] = {
    /*当前状态                              处理函数                                    下一状态（处理函数收到正确的信息）*/
    {OTA_IDLE_STATUS, Face_Sending_Start_OTA, OTA_ZSENDING_OTA_READY_STATUS},
};

FaceOtaBoxData_stu_t faceOtaBoxData = {
    .len = OSAL_LENGTH(otaDealSwitchBox),
    .pOtaBoxData = otaDealSwitchBox,
};
/******************************人脸模组休眠模式框************************************/
static FaceSwitch_stu_t switchBoxSleeped[] =
    {
/*当前状态，事件，模式，下一状态*/
#ifdef FACE_ENCRYPTION_ENABLE //人脸加密通信

        /*完成状态，超时，空闲模式，空闲状态*/
        {FINISHED_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},

        /*模组上电中，超时，睡眠模式，完成状态*/
        {EVENT_SWITCH_ANY_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_SLEEPED_MODE, FINISHED_STATUS},

        /*休眠状态，上电，睡眠模式，模块上电*/
        {SLEEPED_IDLE_STATUS, FACE_POWER_ON_EVEN, FACE_MODULE_SLEEPED_MODE, POWERING_ON_STATUS},

        /*模组上电中，模组上电完成，睡眠模式，加密中状态*/
        {POWERING_ON_STATUS, FACE_MODULE_POWER_ON_READY_EVENT, FACE_MODULE_SLEEPED_MODE, ENCRYPT_STATUS},

        /*模组加密中，上电初始化加密码，睡眠模式，模组加密中*/
        {ENCRYPT_STATUS, FACE_POWER_ON_INIT_ENCRYPT_EVEN, FACE_MODULE_SLEEPED_MODE, ENCRYPT_STATUS},

        /*模组加密中，模组加密成功完成，睡眠模式，完成状态*/
        {ENCRYPT_STATUS, FACE_POWER_ON_ENCRYPT_FINISH_EVEN, FACE_MODULE_SLEEPED_MODE, FINISHED_STATUS},

#else
        /*休眠状态，上电，睡眠模式，模块上电*/
        {SLEEPED_IDLE_STATUS, FACE_POWER_ON_EVEN, FACE_MODULE_SLEEPED_MODE, POWERING_ON_STATUS},

        /*模组上电中，模组上电完成，空闲模式，空闲状态*/
        {POWERING_ON_STATUS, FACE_MODULE_POWER_ON_READY_EVENT, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},

        /*模组上电中，超时，空闲模式，空闲状态*/
        {POWERING_ON_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},
#endif
};

FaceBoxData_stu_t sleepedSwitchBox =
    {
        .mode = FACE_MODULE_SLEEPED_MODE,
        .len = OSAL_LENGTH(switchBoxSleeped),
        .pBoxData = switchBoxSleeped,
};

static FaceInform_stu_t informBoxSleeped[] =
    {
        {SLEEPED_IDLE_STATUS, ALLOW, RESET, FOREVER_TIME_OUT},
        {POWERING_ON_STATUS, DISALLOW, SET, FACE_POWERING_ON_TIMEOUT}, //上电过程不允许打断
        {ENCRYPT_STATUS, DISALLOW, SET, FACE_NORMAL_TIMEOUT},
        {FINISHED_STATUS, DISALLOW, SET, IMMEDIATELY_TIME_OUT},
};

FaceBoxData_stu_t sleepedInformBox =
    {
        .mode = FACE_MODULE_SLEEPED_MODE,
        .len = OSAL_LENGTH(informBoxSleeped),
        .pBoxData = informBoxSleeped,
};

static FaceDeal_stu_t dealBoxSleeped[] =
    {
        {SLEEPED_IDLE_STATUS, Face_SleepedIdle_StatusFun},
        {POWERING_ON_STATUS, Face_PoweringOn_StatusFun},
        {ENCRYPT_STATUS, Face_Encrypting_StatusFun},
        {FINISHED_STATUS, Face_EncrypFinish_StatusFun},
};

FaceBoxData_stu_t sleepedDealBox =
    {
        .mode = FACE_MODULE_SLEEPED_MODE, //人脸模组已经休眠断电
        .len = OSAL_LENGTH(dealBoxSleeped),
        .pBoxData = dealBoxSleeped,
};
/******************************人脸模组休眠模式框结束************************************/

/********************************人脸模组验证模式框************************************/

static FaceSwitch_stu_t switchBoxVetifying[] =
    {
        /*当前状态，事件，模式，下一状态*/
        /*完成验证，超时，执行休眠，发送休眠指令*/
        // {FINISHED_STATUS,FACE_TIMEOUT_ENENT,FACE_MODULE_SLEEPING_MODE,SLEEPING_SENDING_STATUS},
        {FINISHED_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},

        /*任意状态，超时，验证模式，验证结束*/
        {EVENT_SWITCH_ANY_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_VERITYFYING_MODE, FINISHED_STATUS},

        /*任意状态，终止操作，空闲模式，空闲状态*/
        {EVENT_SWITCH_ANY_STATUS, FACE_ABORT_EVENT, FACE_MODULE_IDLE_MODE, IDLE_SENDING_STATUS},

        /*执行相应的命令，验证完成成功，验证模式，完成*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_VERITY_SUCCESS_FINISHED_EVENT, FACE_MODULE_VERITYFYING_MODE, FINISHED_STATUS},

        /*执行相应的命令，验证完成失败，验证模式，完成*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_VERITY_FAIL_FINISHED_EVENT, FACE_MODULE_VERITYFYING_MODE, FINISHED_STATUS},

        /*执行相应的命令，人脸状态信息返回，验证模式，执行相应的命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_NID_FACE_STATUS_EVENT, FACE_MODULE_VERITYFYING_MODE, FACE_EXECUTIVING_ORDER_STATUS},

};

FaceBoxData_stu_t vetifyingSwitchBox =
    {
        .mode = FACE_MODULE_VERITYFYING_MODE,
        .len = OSAL_LENGTH(switchBoxVetifying),
        .pBoxData = switchBoxVetifying,
};

static FaceInform_stu_t informBoxVetifying[] =
    {
        //   {POWERING_ON_STATUS,            DISALLOW,   FACE_POWERING_ON_TIMEOUT},     //上电过程不允许打断
        {FACE_EXECUTIVING_ORDER_STATUS, ALLOW, SET, FOREVER_TIME_OUT},
        {FINISHED_STATUS, DISALLOW, SET, IMMEDIATELY_TIME_OUT},
};

FaceBoxData_stu_t vetifyingInformBox =
    {
        .mode = FACE_MODULE_VERITYFYING_MODE,
        .len = OSAL_LENGTH(informBoxVetifying),
        .pBoxData = informBoxVetifying,
};

static FaceDeal_stu_t dealBoxVetifying[] =
    {
        {FACE_EXECUTIVING_ORDER_STATUS, Face_VerityingExcuteOrder_StatusFun},
        {FINISHED_STATUS, Face_VerityingFinished_StatusFun},
};

FaceBoxData_stu_t vetifyingDealBox =
    {
        .mode = FACE_MODULE_VERITYFYING_MODE, //人脸模组正在验证人脸
        .len = OSAL_LENGTH(dealBoxVetifying),
        .pBoxData = dealBoxVetifying,
};
/******************************人脸模组验证模式框结束************************************/

/******************************人脸模组录入模式框************************************/
static FaceSwitch_stu_t switchBoxEnrolling[] =
    {
        /*当前状态，事件，下一模式，下一状态*/
        /*完成验证，超时，执行休眠，发送休眠指令*/
        //  {FINISHED_STATUS,FACE_TIMEOUT_ENENT,FACE_MODULE_SLEEPING_MODE,SLEEPING_SENDING_STATUS},
        {FINISHED_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},

        /*任意状态，超时，录入模式，验证结束*/
        {EVENT_SWITCH_ANY_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_ENROLLING_MODE, FINISHED_STATUS},

        /*任意状态，终止操作，空闲模式，空闲状态*/
        {EVENT_SWITCH_ANY_STATUS, FACE_ABORT_EVENT, FACE_MODULE_IDLE_MODE, IDLE_SENDING_STATUS},

        /*模组上电中，模组上电完成，录入模式，执行相应的命令*/
        //  {POWERING_ON_STATUS,FACE_MODULE_POWER_ON_READY_EVENT,FACE_MODULE_ENROLLING_MODE,FACE_EXECUTIVING_ORDER_STATUS},

        /*执行相应的命令，验证完成，录入模式，完成*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_ENROLL_FINISHED_EVENT, FACE_MODULE_ENROLLING_MODE, FINISHED_STATUS},

        /*执行相应的命令，人脸状态信息返回，录入模式，执行相应的命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_NID_FACE_STATUS_EVENT, FACE_MODULE_ENROLLING_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*执行相应的命令，录入下一方向人脸，录入模式，执行相应的命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_ENROLL_ENROLL_NEXT_DIR_EVENT, FACE_MODULE_ENROLLING_MODE, FACE_EXECUTIVING_ORDER_STATUS},
};

FaceBoxData_stu_t EnrollingSwitchBox =
    {
        .mode = FACE_MODULE_ENROLLING_MODE, //人脸模组正在录入人脸:录入模式
        .len = OSAL_LENGTH(switchBoxEnrolling),
        .pBoxData = switchBoxEnrolling,
};

static FaceInform_stu_t informBoxEnrolling[] =
    {
        {FACE_EXECUTIVING_ORDER_STATUS, ALLOW, SET, FOREVER_TIME_OUT},
        {FINISHED_STATUS, DISALLOW, SET, IMMEDIATELY_TIME_OUT},

};

FaceBoxData_stu_t EnrollingInformBox =
    {
        .mode = FACE_MODULE_ENROLLING_MODE, //人脸模组正在录入人脸:录入模式
        .len = OSAL_LENGTH(informBoxEnrolling),
        .pBoxData = informBoxEnrolling,
};

static FaceDeal_stu_t dealBoxEnrolling[] =
    {
        {FACE_EXECUTIVING_ORDER_STATUS, Face_EnrollingExcuteOrder_StatusFun},
        {FINISHED_STATUS, Face_EnrollingFinished_StatusFun},
};

FaceBoxData_stu_t EnrollingDealBox =
    {
        .mode = FACE_MODULE_ENROLLING_MODE, //人脸模组正在录入人脸:录入模式
        .len = OSAL_LENGTH(dealBoxEnrolling),
        .pBoxData = dealBoxEnrolling,
};
/******************************人脸模组录入模式框结束************************************/

/******************************人脸模组删除模式框************************************/
static FaceSwitch_stu_t switchBoxDeleting[] =
    {
        /*当前状态，事件，模式，下一状态*/
        /*完成，超时，执行休眠，发送休眠指令*/
        //  {FINISHED_STATUS,FACE_TIMEOUT_ENENT,FACE_MODULE_SLEEPING_MODE,SLEEPING_SENDING_STATUS},

        {FINISHED_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},

        /*任意状态，超时，删除模式，结束*/
        {EVENT_SWITCH_ANY_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_DELETING_MODE, FINISHED_STATUS},

        /*任意状态，终止操作，空闲模式，空闲状态*/
        {EVENT_SWITCH_ANY_STATUS, FACE_ABORT_EVENT, FACE_MODULE_IDLE_MODE, IDLE_SENDING_STATUS},

        /*模组上电中，模组上电完成，删除模式，执行相应的命令*/
        //   {POWERING_ON_STATUS,FACE_MODULE_POWER_ON_READY_EVENT,FACE_MODULE_DELETING_MODE,FACE_EXECUTIVING_ORDER_STATUS},

        /*执行相应的命令，验证完成，删除模式，执行相应的命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_VERITY_SUCCESS_FINISHED_EVENT, FACE_MODULE_DELETING_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*执行相应的命令，验证完成，删除模式，执行相应的命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_VERITY_FAIL_FINISHED_EVENT, FACE_MODULE_DELETING_MODE, FINISHED_STATUS},

        /*执行相应的命令，删除完成，删除模式，完成*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_DELETE_FINISH_EVENT, FACE_MODULE_DELETING_MODE, FINISHED_STATUS},

        /*执行相应的命令，人脸状态信息返回，验证模式，执行相应的命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_NID_FACE_STATUS_EVENT, FACE_MODULE_DELETING_MODE, FACE_EXECUTIVING_ORDER_STATUS},
};

FaceBoxData_stu_t DeletingSwitchBox =
    {
        .mode = FACE_MODULE_DELETING_MODE, //人脸模组删除人脸：删除模式
        .len = OSAL_LENGTH(switchBoxDeleting),
        .pBoxData = switchBoxDeleting,
};

static FaceInform_stu_t informBoxDeleting[] =
    {
        //   {POWERING_ON_STATUS,            DISALLOW,   FACE_POWERING_ON_TIMEOUT},     //上电过程不允许打断
        {FACE_EXECUTIVING_ORDER_STATUS, ALLOW, SET, FOREVER_TIME_OUT},
        {FINISHED_STATUS, DISALLOW, SET, IMMEDIATELY_TIME_OUT},
};

FaceBoxData_stu_t DeletingInformBox =
    {
        .mode = FACE_MODULE_DELETING_MODE, //人脸模组删除人脸：删除模式
        .len = OSAL_LENGTH(informBoxDeleting),
        .pBoxData = informBoxDeleting,
};

static FaceDeal_stu_t dealBoxDeleting[] =
    {
        {FACE_EXECUTIVING_ORDER_STATUS, Face_DeletingExcuteOrder_StatusFun},
        {FINISHED_STATUS, Face_deletingFinished_StatusFun},
};

FaceBoxData_stu_t DeletingDealBox =
    {
        .mode = FACE_MODULE_DELETING_MODE, //人脸模组删除人脸：删除模式
        .len = OSAL_LENGTH(dealBoxDeleting),
        .pBoxData = dealBoxDeleting,
};
/******************************人脸模组删除模式框结束************************************/

/******************************人脸模组升级模式框************************************/
static FaceSwitch_stu_t switchBoxBooting[] =
    {
        /*当前状态，事件，模式，下一状态*/
        {FINISHED_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_SLEEPING_MODE, SLEEPING_SENDING_STATUS}, //ota状态下非加密通信，结束后直接休眠断电模组，重新上电后再次加密通信
        /*OTA结束，超时，IDLE模式，空闲*/
        {EVENT_SWITCH_ANY_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_OTA_MODE, FINISHED_STATUS},
        /*任意状态，终止操作，空闲模式，空闲状态*/
        {EVENT_SWITCH_ANY_STATUS, FACE_ABORT_EVENT, FACE_MODULE_IDLE_MODE, IDLE_SENDING_STATUS},

        /*执行命令，OTA前期准备，升级模式，执行命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_OTA_PREPAREING_EVEN, FACE_MODULE_OTA_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*执行命令，OTA开始发送固件，升级模式，执行命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_OTA_START_SEND_PACKET_EVEN, FACE_MODULE_OTA_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*执行命令，OTA持续发送升级包，升级模式，执行命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_OTA_KEEP_SENDING_PACKET_EVEN, FACE_MODULE_OTA_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*执行命令，OTA结束，升级模式，升级结束命令*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_OTA_FINISH_EVEN, FACE_MODULE_OTA_MODE, FINISHED_STATUS},
};

FaceBoxData_stu_t BootingSwitchBox =
    {
        .mode = FACE_MODULE_OTA_MODE, //人脸模组正在升级人脸：升级模式
        .len = OSAL_LENGTH(switchBoxBooting),
        .pBoxData = switchBoxBooting,
};

static FaceInform_stu_t informBoxBooting[] =
    {
        {FACE_EXECUTIVING_ORDER_STATUS, DISALLOW, SET, FACE_OTA_EXCUTING_TIMEOUT},
        {FINISHED_STATUS, DISALLOW, SET, FACE_NORMAL_TIMEOUT},
};

FaceBoxData_stu_t BootingInformBox =
    {
        .mode = FACE_MODULE_OTA_MODE, //人脸模组正在升级人脸：升级模式
        .len = OSAL_LENGTH(informBoxBooting),
        .pBoxData = informBoxBooting,
};

static FaceDeal_stu_t dealBoxBooting[] =
    {
        {FACE_EXECUTIVING_ORDER_STATUS, Face_OTA_ExcuteOrder_StatusFun},
        {FINISHED_STATUS, Face_OtaFinished_StatusFun},
};

FaceBoxData_stu_t BootingDealBox =
    {
        .mode = FACE_MODULE_OTA_MODE, //人脸模组正在升级人脸：升级模式
        .len = OSAL_LENGTH(dealBoxBooting),
        .pBoxData = dealBoxBooting,
};
/******************************人脸模组升级模式框结束************************************/

/******************************人脸模组准备休眠模式框************************************/
static FaceSwitch_stu_t switchBoxSleeping[] =
    {
        /*当前状态，事件，模式，下一状态*/

        /*发送休眠指令，超时，断电休眠模式，断电休眠*/
        {SLEEPING_SENDING_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_SLEEPED_MODE, SLEEPED_IDLE_STATUS},

        /*任意状态，终止操作，空闲模式，空闲状态*/
        // {EVENT_SWITCH_ANY_STATUS,FACE_ABORT_EVENT,FACE_MODULE_IDLE_MODE,IDLE_SENDING_STATUS},

        /*发送休眠指令，允许休眠，断电休眠模式，断电休眠*/
        {SLEEPING_SENDING_STATUS, FACE_SLEEPING_ALLOW_EVENT, FACE_MODULE_SLEEPED_MODE, SLEEPED_IDLE_STATUS},

        /*发送休眠状态，不允许休眠，执行休眠模式，发送休眠状态*/
        {SLEEPING_SENDING_STATUS, FACE_SLEEPING_DISALLOW_EVENT, FACE_MODULE_SLEEPED_MODE, SLEEPED_IDLE_STATUS},
};

FaceBoxData_stu_t sleepingSwitchBox =
    {
        .mode = FACE_MODULE_SLEEPING_MODE,
        .len = OSAL_LENGTH(switchBoxSleeping),
        .pBoxData = switchBoxSleeping,
};

static FaceInform_stu_t informBoxSleeping[] =
    {
        {SLEEPING_SENDING_STATUS, DISALLOW, SET, FOREVER_TIME_OUT},
};

FaceBoxData_stu_t sleepingInformBox =
    {
        .mode = FACE_MODULE_SLEEPING_MODE,
        .len = OSAL_LENGTH(informBoxSleeping),
        .pBoxData = informBoxSleeping,
};

static FaceDeal_stu_t dealBoxSleeping[] =
    {
        {SLEEPING_SENDING_STATUS, Face_SleepingSending_StatusFun},
};

FaceBoxData_stu_t sleepingDealBox =
    {
        .mode = FACE_MODULE_SLEEPING_MODE, //人脸模组已经休眠断电
        .len = OSAL_LENGTH(dealBoxSleeping),
        .pBoxData = dealBoxSleeping,
};
/******************************人脸模组准备休眠模式框结束************************************/

/******************************人脸模组空闲模式框************************************/
static FaceSwitch_stu_t switchBoxIdle[] =
    {
        /*当前状态，事件，模式，下一状态*/

        /*发送进入空闲，成功进入IDLE模式，空闲模式，空闲状态*/
        {IDLE_SENDING_STATUS, FACE_IDLE_SUCCESS_EVEN, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},

        /*发送进入空闲，超时，执行休眠模式，发送休眠状态*/
        {IDLE_SENDING_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_SLEEPING_MODE, SLEEPING_SENDING_STATUS},

        /*空闲状态，上电事件，空闲模式，空闲状态（用于再延时等待1.5s，防止在上电和录入指令之间出现超时导致录入指令无法执行）*/
        {POWER_ON_IDLE_STATUS, FACE_POWER_ON_EVEN, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},

        /*空闲状态，启动验证，验证模式，执行命令*/
        {POWER_ON_IDLE_STATUS, FACE_MASTER_START_VERITY_EVENT, FACE_MODULE_VERITYFYING_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*空闲状态，启动录入，录入模式，执行命令*/
        {POWER_ON_IDLE_STATUS, FACE_MASTER_START_ENROLL_EVENT, FACE_MODULE_ENROLLING_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*空闲状态，启动删除，删除模式，执行命令*/
        {POWER_ON_IDLE_STATUS, FACE_MASTER_START_DELETE_EVENT, FACE_MODULE_DELETING_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*空闲状态，启动OTA，OTA模式，执行命令*/
        {POWER_ON_IDLE_STATUS, FACE_MASTER_START_OTA_EVEN, FACE_MODULE_OTA_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*空闲状态，启动SET，SET模式，执行命令*/
        {POWER_ON_IDLE_STATUS, FACE_START_SET_EVENT, FACE_MODULE_SET_MODE, FACE_EXECUTIVING_ORDER_STATUS},

        /*空闲状态，超时，执行休眠模式，发送休眠状态*/
        {POWER_ON_IDLE_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_SLEEPING_MODE, SLEEPING_SENDING_STATUS},
};

FaceBoxData_stu_t idleSwitchBox =
    {
        .mode = FACE_MODULE_IDLE_MODE,
        .len = OSAL_LENGTH(switchBoxIdle),
        .pBoxData = switchBoxIdle,
};

static FaceInform_stu_t informBoxIdle[] =
    {
        {IDLE_SENDING_STATUS, DISALLOW, SET, FOREVER_TIME_OUT},
        {POWER_ON_IDLE_STATUS, ALLOW, RESET, FACE_IDLE_TIMEOUT},
};

FaceBoxData_stu_t idleInformBox =
    {
        .mode = FACE_MODULE_IDLE_MODE,
        .len = OSAL_LENGTH(informBoxIdle),
        .pBoxData = informBoxIdle,
};

static FaceDeal_stu_t dealBoxIdle[] =
    {
        {IDLE_SENDING_STATUS, Face_IdleSending_StatusFun},
};

FaceBoxData_stu_t idleDealBox =
    {
        .mode = FACE_MODULE_IDLE_MODE, //人脸模组已经休眠断电
        .len = OSAL_LENGTH(dealBoxIdle),
        .pBoxData = dealBoxIdle,
};
/******************************人脸模组空闲模式框结束************************************/

/******************************人脸模组设置模式框************************************/
static FaceSwitch_stu_t switchBoxSeting[] =
    {
        /*当前状态，事件，模式，下一状态*/

        /*完成验证，超时，空闲模式，空闲状态*/
        {FINISHED_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_IDLE_MODE, POWER_ON_IDLE_STATUS},

        // /*任意状态，超时，验证模式，验证结束*/
        {EVENT_SWITCH_ANY_STATUS, FACE_TIMEOUT_ENENT, FACE_MODULE_SET_MODE, FINISHED_STATUS},

        // /*任意状态，终止操作，空闲模式，空闲状态*/
        {EVENT_SWITCH_ANY_STATUS, FACE_ABORT_EVENT, FACE_MODULE_IDLE_MODE, IDLE_SENDING_STATUS},

        /*执行相应的命令，设置完成，设置模式，完成*/
        {FACE_EXECUTIVING_ORDER_STATUS, FACE_SET_FINISH_EVENT, FACE_MODULE_SET_MODE, FINISHED_STATUS},

        // /*执行相应的命令，验证完成失败，验证模式，完成*/
        // {FACE_EXECUTIVING_ORDER_STATUS,FACE_VERITY_FAIL_FINISHED_EVENT,FACE_MODULE_VERITYFYING_MODE,FINISHED_STATUS},

        // /*执行相应的命令，人脸状态信息返回，验证模式，执行相应的命令*/
        // {FACE_EXECUTIVING_ORDER_STATUS,FACE_NID_FACE_STATUS_EVENT,FACE_MODULE_VERITYFYING_MODE,FACE_EXECUTIVING_ORDER_STATUS},

};

FaceBoxData_stu_t setingSwitchBox =
    {
        .mode = FACE_MODULE_SET_MODE, //人脸设置模式，用于人脸参数设置
        .len = OSAL_LENGTH(switchBoxSeting),
        .pBoxData = switchBoxSeting,
};

static FaceInform_stu_t informBoxSeting[] =
    {
        //   {POWERING_ON_STATUS,            DISALLOW,   FACE_POWERING_ON_TIMEOUT},     //上电过程不允许打断
        {FACE_EXECUTIVING_ORDER_STATUS, DISALLOW, SET, FACE_SET_EXCUTING_TIMEOUT},
        {FINISHED_STATUS, DISALLOW, SET, IMMEDIATELY_TIME_OUT},
};

FaceBoxData_stu_t setingInformBox =
    {
        .mode = FACE_MODULE_SET_MODE, //人脸设置模式，用于人脸参数设置
        .len = OSAL_LENGTH(informBoxSeting),
        .pBoxData = informBoxSeting,
};

static FaceDeal_stu_t dealBoxSeting[] =
    {
        {FACE_EXECUTIVING_ORDER_STATUS, Face_SetingExcuteOrder_StatusFun},
        {FINISHED_STATUS, Face_SetingFinished_StatusFun},
};

FaceBoxData_stu_t setingDealBox =
    {
        .mode = FACE_MODULE_SET_MODE, //人脸设置模式，用于人脸参数设置
        .len = OSAL_LENGTH(dealBoxSeting),
        .pBoxData = dealBoxSeting,
};
/******************************人脸模组准备休眠模式框结束************************************/

#endif
