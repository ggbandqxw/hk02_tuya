#ifndef _FACE_TASK_H_
#define _FACE_TASK_H_

#include "component.h"
#include "device.h"

#define FACE_ENCRYPTION_ENABLE
#define FACE_ENROLL_SINGLE

/* 人脸动作类型 */
typedef enum
{
	FACE_ACTION_VERIFY_OK,				//验证成功
	FACE_ACTION_ADD_OK,					//添加成功
	FACE_ACTION_VERIFY_ERR,				//验证失败
	FACE_ACTION_NOFACE,					//未发现人脸
	FACE_ACTION_VERIFY_END,				//验证结束
	FACE_ACTION_ADD_ERR,				//添加失败
	FACE_ACTION_ADD_REPEAT,				//添加失败，人脸已存在
	FACE_ACTION_DEL_OK,					//删除成功
	FACE_ACTION_DEL_FAIL,				//删除失败
	FACE_ACTION_ENROLL_MIDDLE_REMINDER, //注册正向人脸提醒
	FACE_ACTION_ENROLL_RIGHT_REMINDER,	//注册右向人脸提醒
	FACE_ACTION_ENROLL_LEFT_REMINDER,	//注册左向人脸提醒
	FACE_ACTION_ENROLL_DOWN_REMINDER,	//注册低头人脸提醒
	FACE_ACTION_ENROLL_UP_REMINDER,		//注册抬头人脸提醒
	FACE_ACTION_TOOUP,					//人脸太靠近图片上边沿，未能录入
	FACE_ACTION_TOODOWN,				//人脸太靠近图片下边沿，未能录入
	FACE_ACTION_TOOLEFT,				//人脸太靠近图片下边沿，未能录入
	FACE_ACTION_TOOLRIGHT,				//人脸太考经图片右边沿，未能录入
	FACE_ACTION_FAR,					//人脸距离太远，未能录入
	FACE_ACTION_CLOSE,					//人脸距离太近，未能录入
	FACE_ACTION_EYEBROW_OCCLUSION,		//眉毛遮挡
	FACE_ACTION_EYE_OCCLUSION,			//眼睛遮挡
	FACE_ACTION_FACE_OCCLUSION,			//脸部遮挡
	FACE_ACTION_DIRECTION_ERROR,		//录入人脸方向错误
	FACE_ACTION_MODULE_BREAKDOWN,		//人脸模组异常
	FACE_ACTION_ENTER_OTA_MODE,			//人脸模组进入ota模式
	FACE_ACTION_OTA_SENDING_FIRMWARE,	//人脸OTA正在发送升级包
	FACE_ACTION_OTA_OK,					//OTA成功
	FACE_ACTION_OTA_ERR,				//OTA失败
	FACE_ACTION_ENC_SUCCESS,			//设置人脸加密秘钥序列成功
	FACE_ACTION_ENC_FAIL,				//设置人脸加密秘钥序列失败
	FACE_ACTION_ENCRYPTED,				//人脸加密密钥系列已经存在
	FACE_ACTION_TIME_OUT,				//超时
	FACE_ACTION_OTAING,					//人脸正在ota
	FACE_ACTION_MIDDLE_REMINDER,		//正视提醒
	FACE_ACTION_RIGHT_REMINDER,			//向右转头提醒
	FACE_ACTION_LEFT_REMINDER,			//向左转头提醒
	FACE_ACTION_DOWN_REMINDER,			//向下转头提醒
	FACE_ACTION_UP_REMINDER,			//向上转头提醒
	FACE_ACTION_POWER_ON,				//人脸上电
	FACE_ACTION_POWER_OFF,				//人脸掉电
	FACE_ACTION_MAX,					//无效值
} FaceActionType_enum_t;

/*人脸模式枚举*/
typedef enum
{
	FACE_MODULE_SLEEPED_MODE,	  //人脸模组已经休眠断电 ：断电休眠模式
	FACE_MODULE_VERITYFYING_MODE, //人脸模组正在验证人脸 ：验证模式
	FACE_MODULE_ENROLLING_MODE,	  //人脸模组正在录入人脸:录入模式
	FACE_MODULE_DELETING_MODE,	  //人脸模组删除人脸：删除模式
	FACE_MODULE_OTA_MODE,		  //人脸模组正在升级人脸:升级模式
	FACE_MODULE_SLEEPING_MODE,	  //人脸模组正在执行休眠动作 ：执行休眠模式
	FACE_MODULE_IDLE_MODE,		  //人脸模组空闲模式：上电状态受到结束指令转换到这个模式：空闲模式
	FACE_MODULE_SET_MODE,		  //人脸设置模式，用于人脸参数设置
	FACE_MAX_MODE,
} Face_mode_enum_t;

/* Face组件消息类型 */
typedef struct
{
	FaceActionType_enum_t action; //动作
	uint8_t id;					  //用户id
} FaceMsg_t;

typedef void (*Master_Msg_Cb_fun_t)(uint8_t result, uint8_t *pData, uint8_t dataLen);

static ErrorStatus Face_Power_On(void);
static ErrorStatus Face_Abort_Current_Operation(void);
static ErrorStatus Face_Verity_Start(void);
static ErrorStatus Face_Delete_Verity_Start(void);
static ErrorStatus Face_Enroll_Start(uint8_t enrollNum);
static ErrorStatus Face_Delete_Start(uint8_t startNum, uint8_t endNum);
static ErrorStatus Face_Read_Version(uint8_t *pData, uint8_t *pLen);
static ErrorStatus Face_Read_Model(uint8_t *pData);
static ErrorStatus Face_Set_Enc_Key(void);
static ErrorStatus Face_Factory(void);
static ErrorStatus Face_Set_Demo_Mode(uint8_t mode);
static ErrorStatus Face_OTA_Start(void);
static Face_mode_enum_t Face_Get_FaceModule_CurMode(void);

#endif
