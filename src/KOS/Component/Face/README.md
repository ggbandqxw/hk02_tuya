# 一、接口API

## 1、应用层调用API函数说明

### 1.1 人脸模组上电
    void Face_Power_On(void)
### 1.2 人脸终止当前操作
    void Face_Abort_Current_Operation(void)
### 1.3 验证人脸
    ErrorStatus Face_Verity_Start(void)
### 1.4 注册人脸
    void Face_Enroll_Start(uint8_t enrollNum)
### 1.5 删除人脸
    void Face_Delete_Start(uint8_t startNum, uint8_t endNum)
### 1.6 获取人脸版本
    void Face_Read_Version(uint8_t *pData, uint8_t *pLen)
### 1.7 产测设置加密密钥
    void Face_Set_Enc_Key(void)
### 1.8 人脸恢复出厂设置
    void Face_Factory(void)


## 2、应用层订阅消息说明

- 消息结构体

```
typedef struct
{
	FaceActionType_enum_t action; //动作
	uint8_t id;					  //用户id
} FaceMsg_t;
```

