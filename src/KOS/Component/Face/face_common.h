#ifndef __FACE_COMMON_H
#define __FACE_COMMON_H

#include "device.h"
#include "face.h"

#define FACE_DEBUG //debug模式下可以打印的日志更全面

#define FIRMWARE_PACKAGE_LEN 0		//固件包长度
#define VERSION_INFO_BUFFER_SIZE 32 //人脸模组固件版本信息长度
#define FACE_VERSION_LEN 10			//人脸版本号长度
#define FACE_MODEL_LEN 6            //人脸模组型号长度
#define FACE_SN_LEN 20				//人脸sn长度

#ifndef KOS_PARAM_USERS_QTY_FACE
#define KOS_PARAM_USERS_QTY_FACE   20
#endif

#if (KOS_PARAM_USERS_QTY_FACE <= 20) //兼容已出货的锁
#define MAX_FACE_NUM 20
#else
#define MAX_FACE_NUM 50             //人脸最大数量
#endif

/* 人脸事件*/
#define EVENT_FACE_PROTOCOL (0X00000001)		//处理人脸协议事件
#define EVENT_MASTER_TO_FACS_MSG (0X00000002)	//主控发送给人脸任务的事件
#define EVENT_FACE_MODULE_MSG (0x00000004)		//发送给人脸模组的事件
#define EVENT_FACE_TIME_OUT (0x00000008)		//人脸任务超时事件
#define EVENT_ZIGBEE_PROTOCOL (0x00000010)		//人脸zigbee消息处理
#define EVENT_CHECK_OTA_DMA_STATUS (0x00000020) //检测人脸DMA状态

#define FACE_DELETE_ALL_NUN 0xFF //全部删除编号
#define INVALID_NUM 0xFE		 //无效编号

#define FACE_NORMAL_TIMEOUT 2000							//一般超时时间
#define FACE_REMINDER_INTERVAL 2100							//人脸转头提醒间隔 2100
#define FACE_ONE_SEC_OF_MSEC 1000							//1s = 1000ms
#define FACE_MODULE_DEAL_TIMEOUT_SMALL 500					//人脸模组处理设置时间相对于配置时间减少500ms
#define FOREVER_TIME_OUT (0xFFFFU)							//永远不超时
#define IMMEDIATELY_TIME_OUT 0x00							//立即超时
#define FACE_POWERING_ON_TIMEOUT 2000						//人脸模组上电超时时间为2000ms
#define FACE_VERITIFYING_TIMEOUT 5500						//人脸验证超时时间为5000ms-500
#define FACE_POWERDOWN_TIMEOUT 1000							//休眠掉电超时时间
#define FACE_IDLE_SENDING_TIMEOUT 2000						//发送进入空闲状态
#define FACE_IDLE_TIMEOUT 1500								//空闲模式，1.5超时  1500
#define FACE_ENROLLING_TIMEOUT 15500						//录入超时
#define FACE_DELETING_TIMEOUT FACE_NORMAL_TIMEOUT			//删除超时
#define FACE_FACE_RESET_TIMEOUT FACE_NORMAL_TIMEOUT			//清除人脸录入
#define FACE_MID_START_OTA_TIMEOUT FACE_NORMAL_TIMEOUT		//启动OTA指令超时时间
#define FACE_MID_GET_OTA_STATUS_TIMEOUT FACE_NORMAL_TIMEOUT //获取模块OTA状态指令超时时间
#define FACE_SEND_FIRMWARE_TIMEOUT 10000					//发送升级固件超时
#define FACE_OTA_EXCUTING_TIMEOUT 120000					//人脸升级超时2分钟
#define FACE_SET_EXCUTING_TIMEOUT 5000						//人脸设置执行超时时间

//face direction parameters
#define FACE_DIR_AMEND 1					 //修正参数
#define FACE_MIDDLE_MAX 10 - FACE_DIR_AMEND	 //正视
#define FACE_MIDDLE_MIN -10 + FACE_DIR_AMEND //正视
#define FACE_LEFT_MAX -10 - FACE_DIR_AMEND	 //左脸
#define FACE_LEFT_MIN -45 + FACE_DIR_AMEND
#define FACE_RIGHT_MAX 45 - FACE_DIR_AMEND //右脸
#define FACE_RIGHT_MIN 10 + FACE_DIR_AMEND
#define FACE_UP_MAX -10 - FACE_DIR_AMEND //抬头
#define FACE_UP_MIN -45 + FACE_DIR_AMEND
#define FACE_DOWN_MAX 45 - FACE_DIR_AMEND //低头
#define FACE_DOWN_MIN 10 + FACE_DIR_AMEND
#define FACE_ROLL_MAX 20 - FACE_DIR_AMEND //歪头
#define FACE_ROLL_MIN -20 + FACE_DIR_AMEND

typedef enum
{
	ALLOW = 0,
	DISALLOW = !ALLOW
} AllowStatus;

typedef enum
{
	MASTER_FACE_MSG_VERITY,	  //人脸验证
	MASTER_FACE_MSG_ENROLL,	  //人脸录入
	MASTER_FACE_MSG_DELETE,	  //人脸删除
	MASTER_FACE_MSG_POWER,	  //电量通知
	MASTER_FACE_MSG_ABORT,	  //终止当前操作
	MASTER_FACE_MSG_POWER_ON, //给人脸模组上电
	MASTER_FACE_MSG_OTA,	  //启动OTA
	MASTER_FACE_MSG_DEMO,	  //设置为DEBUG模式
	MASTER_FACE_MSG_DISDEMO, //非DEBUG模式
	MASTER_FACE_GET_VERSION,  //获取人脸软件版本号
	MASTER_FACE_SET_ENC_KEY,  //设定量产加密的秘钥
	MASTER_FACE_MSG_DELETE_VERITY, //删除时验证人脸
} Master_To_FaceMsg_enum_t;

/*LIST 的排列顺序要和上面的枚举数据一致*/
#define MODE_LIST                                                                                                                                                 \
	{                                                                                                                                                             \
		{FACE_MODULE_SLEEPED_MODE, "SLEEPED MODE"},				/*人脸模组已经休眠断电 ：断电休眠模式 */                                         \
			{FACE_MODULE_VERITYFYING_MODE, "VERITYFYING MODE"}, /*人脸模组正在验证人脸 ：验证模式 */                                               \
			{FACE_MODULE_ENROLLING_MODE, "ENROLLING MODE"},		/*人脸模组正在录入人脸*/                                                                \
			{FACE_MODULE_DELETING_MODE, "DELETING MODE"},		/*人脸模组删除人脸 */                                                                     \
			{FACE_MODULE_OTA_MODE, "OTA MODE"},					/*人脸模组正在升级人脸 */                                                               \
			{FACE_MODULE_SLEEPING_MODE, "SLEEPING MODE"},		/*人脸模组正在执行休眠动作 ：执行休眠模式*/                                    \
			{FACE_MODULE_IDLE_MODE, "IDLE MODE"},				/*人脸模组空闲模式：上电状态受到结束指令转换到这个模式：空闲模式*/ \
			{FACE_MODULE_SET_MODE, "SET MODE"},					/*人脸设置模式*/                                                                            \
	}

/*人脸状态枚举*/
typedef enum
{
	SLEEPED_IDLE_STATUS,		   //空闲休眠状态：模块断电休眠
	POWER_ON_IDLE_STATUS,		   //模组上电状态且空闲：空闲状态
	POWERING_ON_STATUS,			   //人脸模组上电中
	ENCRYPT_STATUS,				   //加密状态
	FACE_EXECUTIVING_ORDER_STATUS, //人脸模组正在执行相应的命令：验证，录入，升级，删除：执行命令
	FINISHED_STATUS,			   //完成
	SLEEPING_SENDING_STATUS,	   //正在发送休眠指令给模块：发送休眠状态
	SLEEPING_SLEEPFAIL_STATUS,	   //休眠失败
	IDLE_SENDING_STATUS,		   //发送进入空闲状态
	EVENT_SWITCH_ANY_STATUS,	   //在列表中，无视当前状态，只要事件发生,立即切换状态和模式
	FACE_MAX_STATUS,			   //无效人脸状态
} Face_status_enum_t;

/*LIST 的排列顺序要和上面的枚举数据一致*/
#define STATUS_LIST                                                                                                                                                          \
	{                                                                                                                                                                        \
		{SLEEPED_IDLE_STATUS, "SLEEPED_IDLE_STATUS"},						  /*空闲休眠状态：模块断电休眠 */                                                   \
			{POWER_ON_IDLE_STATUS, "POWER_ON_IDLE_STATUS"},					  /*模组上电状态且空闲：空闲状态*/                                                 \
			{POWERING_ON_STATUS, "POWERING_ON_STATUS"},						  /*人脸模组上电中*/                                                                      \
			{ENCRYPT_STATUS, "ENCRYPT_STATUS"},								  /*加密状态*/                                                                               \
			{FACE_EXECUTIVING_ORDER_STATUS, "FACE_EXECUTIVING_ORDER_STATUS"}, /*人脸模组正在执行相应的命令：验证，录入，升级，删除：执行命令*/ \
			{FINISHED_STATUS, "FINISHED_STATUS"},							  /*完成*/                                                                                     \
			{SLEEPING_SENDING_STATUS, "SLEEPING_SENDING_STATUS"},			  /*正在发送休眠指令给模块：发送休眠状态*/                                     \
			{SLEEPING_SLEEPFAIL_STATUS, "SLEEPING_SLEEPFAIL_STATUS"},		  /*休眠失败*/                                                                               \
			{IDLE_SENDING_STATUS, "IDLE_SENDING_STATUS"},					  /*发送进入空闲状态*/                                                                   \
			{EVENT_SWITCH_ANY_STATUS, "EVENT_SWITCH_ANY_STATUS"},			  /*在列表中，无视当前状态，只要事件发生,立即切换状态和模式*/         \
			{FACE_MAX_STATUS, "FACE_MAX_STATUS"},							  /*无效人脸状态*/                                                                         \
	}

/*人脸事件枚举*/
typedef enum
{
	FACE_TIMEOUT_ENENT,					 //超时
	FACE_ABORT_EVENT,					 //终止当前操作: 终止操作
	FACE_MASTER_START_VERITY_EVENT,		 //主任务启动人脸验证 ：启动验证
	FACE_MODULE_POWER_ON_READY_EVENT,	 //人脸模组上电完成
	FACE_VERITY_SUCCESS_FINISHED_EVENT,	 //人脸验证成功结束
	FACE_VERITY_FAIL_FINISHED_EVENT,	 //人脸验证失败结束
	FACE_SLEEPING_ALLOW_EVENT,			 //人脸允许休眠
	FACE_SLEEPING_DISALLOW_EVENT,		 //人脸不允许休眠
	FACE_NID_FACE_STATUS_EVENT,			 //录入和验证人脸状态信息返回:人脸状态信息返回
	FACE_MASTER_START_ENROLL_EVENT,		 //主任务启动人脸录入：启动录入
	FACE_ENROLL_FINISHED_EVENT,			 //人脸录入结束：录入结束
	FACE_ENROLL_ENROLL_NEXT_DIR_EVENT,	 //人脸录入下一方向人脸 ：录入下一方向人脸
	FACE_MASTER_START_DELETE_EVENT,		 //主任务启动人脸删除：启动删除
	FACE_DELETE_FINISH_EVENT,			 //删除完成
	FACE_IDLE_SUCCESS_EVEN,				 //成功进入IDLE模式
	FACE_POWER_ON_EVEN,					 //上电事件
	FACE_MASTER_START_OTA_EVEN,			 //启动OTA
	FACE_OTA_PREPAREING_EVEN,			 //OTA 前期准备
	FACE_OTA_START_SEND_PACKET_EVEN,	 //OTA开始发送固件升级包
	FACE_OTA_KEEP_SENDING_PACKET_EVEN,	 //OTA正在发送升级包
	FACE_OTA_FINISH_EVEN,				 //OTA结束
	FACE_START_SET_EVENT,				 //人脸设置模式开始
	FACE_SET_FINISH_EVENT,				 //设置完成
	FACE_POWER_ON_ENCRYPT_SUCCESS_EVENT, //人脸上电和加密成功
	FACE_POWER_ON_ENCRYPT_FAIL_EVENT,	 //人脸上电和加密失败
	FACE_POWER_ON_INIT_ENCRYPT_EVEN,	 //上电初始化加密码
	FACE_POWER_ON_ENCRYPT_FINISH_EVEN,	 //上电加密完成
	FACE_MAX_EVENT,						 //事件最大值（无效）
} Face_event_enum_t;

/*LIST 的排列顺序要和上面的枚举数据一致*/
#define EVENT_LIST                                                                                                                        \
	{                                                                                                                                     \
		{FACE_TIMEOUT_ENENT, "FACE_TIMEOUT_ENENT"},										  /*超时 */                                     \
			{FACE_ABORT_EVENT, "FACE_ABORT_EVENT"},										  /*终止当前操作: 终止操作 */           \
			{FACE_MASTER_START_VERITY_EVENT, "FACE_MASTER_START_VERITY_EVENT"},			  /*主任务启动人脸验证 ：启动验证*/ \
			{FACE_MODULE_POWER_ON_READY_EVENT, "FACE_MODULE_POWER_ON_READY_EVENT"},		  /*人脸模组上电完成*/                    \
			{FACE_VERITY_SUCCESS_FINISHED_EVENT, "FACE_VERITY_SUCCESS_FINISHED_EVENT"},	  /*人脸验证结束*/                          \
			{FACE_VERITY_FAIL_FINISHED_EVENT, "FACE_VERITY_FAIL_FINISHED_EVENT"},		  /*人脸验证结束*/                          \
			{FACE_SLEEPING_ALLOW_EVENT, "FACE_SLEEPING_ALLOW_EVENT"},					  /*人脸允许休眠*/                          \
			{FACE_SLEEPING_DISALLOW_EVENT, "FACE_SLEEPING_DISALLOW_EVENT"},				  /*人脸不允许休眠*/                       \
			{FACE_NID_FACE_STATUS_EVENT, "FACE_NID_FACE_STATUS_EVENT"},					  /*录入和验证人脸状态信息返回*/     \
			{FACE_MASTER_START_ENROLL_EVENT, "FACE_MASTER_START_ENROLL_EVENT"},			  /*主任务启动人脸录入：启动录入 */ \
			{FACE_ENROLL_FINISHED_EVENT, "FACE_ENROLL_FINISHED_EVENT"},					  /*人脸录入结束：录入结束  */         \
			{FACE_ENROLL_ENROLL_NEXT_DIR_EVENT, "FACE_ENROLL_ENROLL_NEXT_DIR_EVENT"},	  /*人脸录入下一方向人脸  */            \
			{FACE_MASTER_START_DELETE_EVENT, "FACE_MASTER_START_DELETE_EVENT"},			  /*主任务启动人脸删除：启动删除*/  \
			{FACE_DELETE_FINISH_EVENT, "FACE_DELETE_FINISH_EVENT"},						  /*删除完成  */                              \
			{FACE_IDLE_SUCCESS_EVEN, "FACE_IDLE_SUCCESS_EVEN"},							  /*成功进入IDLE模式 */                     \
			{FACE_POWER_ON_EVEN, "FACE_POWER_ON_EVEN"},									  /*成功进入IDLE模式 */                     \
			{FACE_MASTER_START_OTA_EVEN, "FACE_MASTER_START_OTA_EVEN"},					  /*成功进入IDLE模式 */                     \
			{FACE_OTA_PREPAREING_EVEN, "FACE_OTA_PREPAREING_EVEN"},						  /*OTA 前期准备*/                            \
			{FACE_OTA_START_SEND_PACKET_EVEN, "FACE_OTA_START_SEND_PACKET_EVEN"},		  /*OTA 开始发送固件升级包*/             \
			{FACE_OTA_KEEP_SENDING_PACKET_EVEN, "FACE_OTA_KEEP_SENDING_PACKET_EVEN"},	  /*OTA正在发送升级包*/                    \
			{FACE_OTA_FINISH_EVEN, "FACE_OTA_FINISH_EVEN"},								  /*OTA结束*/                                   \
			{FACE_START_SET_EVENT, "FACE_START_SET_EVENT"},								  /*人脸设置模式开始*/                    \
			{FACE_SET_FINISH_EVENT, "FACE_SET_FINISH_EVENT"},							  /*设置完成*/                                \
			{FACE_POWER_ON_ENCRYPT_SUCCESS_EVENT, "FACE_POWER_ON_ENCRYPT_SUCCESS_EVENT"}, /*人脸上电和加密成功*/                 \
			{FACE_POWER_ON_ENCRYPT_FAIL_EVENT, "FACE_POWER_ON_ENCRYPT_FAIL_EVENT"},		  /*人脸上电和加密失败*/                 \
			{FACE_POWER_ON_INIT_ENCRYPT_EVEN, "FACE_POWER_ON_INIT_ENCRYPT_EVEN"},		  /*上电初始化加密码*/                    \
			{FACE_POWER_ON_ENCRYPT_FINISH_EVEN, "FACE_POWER_ON_ENCRYPT_FINISH_EVEN"},	  /*上电加密完成*/                          \
			{FACE_MAX_EVENT, "FACE_MAX_EVENT"},											  /*事件最大值（无效）*/                 \
	}

/*人脸模组ota过程状态枚举*/
typedef enum
{
	OTA_IDLE_STATUS,					   //OTA空闲状态
	OTA_SENDING_START_OTA_STATUS,		   //正在发送升级指令
	OTA_SENDING_GET_OTA_STATUS_STATUS,	   //正在发送获取OTA状态指令
	OTA_SENDING_CONFIG_BAUDRATE_STATUS,	   //发送设置波特率指令
	OTA_ZSENDING_OTA_READY_STATUS,		   //发送ota准备就绪
	OTA_ZSENDING_CONFIG_BAUDRATE_STATUS,   //设置zigbee的波特率
	OTA_ZSENDING_GET_FIRMWARE_INFO_STATUS, //获取固件信息
	OTA_ZSENDING_GET_CHECK_SUM_STATUS,	   //获取校验值
	OTA_SENDING_OTA_HEADER_INFO_STATUS,	   //发送固件包头信息状态
	OTA_ZSENDING_START_SENDING_FIRMWARE,   //开始发送固件包
	OTA_SENDING_OTA_PACKET_STATUS,		   //转发固件包
	OTA_MAX_STATUS,						   //无效状态
} FaceOtaStatus_enum_t;

/*人脸结果*/
typedef enum
{
	RESULT_SUCCESS_REPORT,		 //成功
	RESULT_FAIL_REPORT,			 //失败
	RESULT_VERIFY_FAIL_REPORT,	 //验证失败
	RESULT_CONTINULE,			 //继续下一步
	RESULT_FACE_ENROLLED,		 //人脸已存在
	RESULT_END,					 //结束
	RESULT_NO_FACE,				 //未检测到人脸
	RUSULT_MODULE_BREAKDOWN,	 //模组异常
	RUSULT_OTA_SENDING_FIRMWARE, //正在发送升级包
	RESULT_ENCRYPTED,			 //已加密
} Face_Result_enum_t;

/*人脸OTA事件ID*/
typedef enum
{
	OTA_MSGID_RECEIVE_START_OTA_EVEN,			 //收到启动OTA
	OTA_MSGID_SEND_START_OTA_RESULT,			 //发送启动OTA结果
	OTA_MSGID_NID_OTA_DONE_RESULT,				 //note OTA状态信息
	OTA_MSGID_SEND_GET_OTA_STATUS_RESULT,		 //发送获取OTA状态结果
	OTA_MSGID_SENG_CONFIG_BAUDRATE_RESULT,		 //设置波特率状态结果
	OTA_MSGID_ZSENG_GET_FIRMWARE_INFO_RESULT,	 //获取固件信息
	OTA_MSGID_ZSENG_GET_CHECK_SUM_RESULT,		 //获取固件校验值
	OTA_MSGID_ZSENG_OTA_READY_RESULT,			 //OTA准备就绪
	OTA_MSGID_ZSEND_CONFIG_BAUDRATE_RESULT,		 //修改zigbee串口波特率
	OTA_MSGID_SEND_OTA_HEADER_RESULT,			 //发送固件包头信息结果
	OTA_MSGID_RECEIVE_START_SEND_PACKET_EVEN,	 //收到启动发送OTA升级包
	OTA_MSGID_ZSEND_GET_FIRMWARE_PACKAGE_RESULT, //获取固件包结果
	OTA_MSGID_SEND_FIRMWARE_PACKAGE_RESULT,		 //给人脸模组发送OTA升级包结果
	SET_MSGID_SET_DEMO_MODE,					 //设置为演示模式
	SET_MSGID_SET_WORK_MODE,					 //设置为工作模式
	SET_MSGID_GET_VERSION,						 //获取人脸模组软件版本号
	SET_MSGID_SET_RELEASE_ENC,					 //设定量产加密秘钥序列
	FACE_TASK_MSGID_MAX,						 //无效值
} Face_Msg_Id_enum_t;

// typedef void (*Master_Msg_Cb_fun_t)(uint8_t result, uint8_t *pData, uint8_t dataLen);

#pragma pack(1)
/* 主控发给人脸任务的消息结构体 */
typedef struct
{
	Master_To_FaceMsg_enum_t actionNum; //动作id
	uint8_t startNum;					//开始编号
	uint8_t endNum;						//结束编号
	uint8_t del_mode;
	Master_Msg_Cb_fun_t cb;
} Master_Face_Msg_stu_t;

/*人脸任务处理参数*/
typedef struct
{
	Face_event_enum_t event;
	Face_Msg_Id_enum_t resultId;
	uint16_t datalen;
	Master_Msg_Cb_fun_t cb;
	void *pinputdata;
} Face_DealParam_stu_t;

typedef struct
{
	Face_Msg_Id_enum_t resultId;
	void *pdata;
} Face_OtaParam_stu_t;

/*人脸固件包传输结果数据值*/
typedef struct
{
	Face_Result_enum_t result;
} Face_OtaSendFirmware_Result_stu_t;

#pragma pack()

#endif
