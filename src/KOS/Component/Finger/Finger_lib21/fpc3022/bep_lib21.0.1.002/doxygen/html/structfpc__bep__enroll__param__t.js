var structfpc__bep__enroll__param__t =
[
    [ "max_nbr_of_immobile_touches", "structfpc__bep__enroll__param__t.html#a30b739e367567bf6a947401cdb420570", null ],
    [ "min_image_quality", "structfpc__bep__enroll__param__t.html#ac5211844ca9e681a124b33ed56d2fe2a", null ],
    [ "min_sensor_coverage", "structfpc__bep__enroll__param__t.html#a6bc032db468322c94b62b92fc4db30c4", null ],
    [ "nbr_of_images", "structfpc__bep__enroll__param__t.html#a84aed4a42d84bf3a34c0217135d7afa9", null ],
    [ "scheme", "structfpc__bep__enroll__param__t.html#aaccae16088d5779f70c0de14b01920d9", null ]
];