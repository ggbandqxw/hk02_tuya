var fpc__log_8h =
[
    [ "fpc_log_printf", "fpc__log_8h.html#a74c002a00ca396ca0ac0112469d429a7", null ],
    [ "fpc_log_printfe", "fpc__log_8h.html#a6fb7155f1201ef1aa4f31eca33d125cf", null ],
    [ "fpc_log_level_t", "fpc__log_8h.html#a02649358843828935cdb6842c4786cf8", [
      [ "FPC_LOG_LEVEL_ALERT", "fpc__log_8h.html#a02649358843828935cdb6842c4786cf8a250c6c23e107e9d063c2e438bac5ec9e", null ],
      [ "FPC_LOG_LEVEL_ERROR", "fpc__log_8h.html#a02649358843828935cdb6842c4786cf8a187a52023d07343422fb64758928c56e", null ],
      [ "FPC_LOG_LEVEL_WARNING", "fpc__log_8h.html#a02649358843828935cdb6842c4786cf8a2e5ca065e41a05d4f361e59693b8b012", null ],
      [ "FPC_LOG_LEVEL_INFO", "fpc__log_8h.html#a02649358843828935cdb6842c4786cf8aa25106f13a54ad5cac7e3f8047d2e031", null ],
      [ "FPC_LOG_LEVEL_DEBUG", "fpc__log_8h.html#a02649358843828935cdb6842c4786cf8ad2d790c3a327b4d74dfe7b76e626b5ee", null ],
      [ "FPC_LOG_LEVEL_LAST", "fpc__log_8h.html#a02649358843828935cdb6842c4786cf8a8e339017fa054b234ece4402015099a4", null ]
    ] ],
    [ "fpc_log", "fpc__log_8h.html#af62f5a2df26ad9118d4ecfe0e4f95e74", null ],
    [ "fpc_log_diag", "fpc__log_8h.html#a0b1d0fba8956f574a4a8ec29e170ab05", null ],
    [ "fpc_log_flush", "fpc__log_8h.html#a4734cb198515843ba77e053a24fa1085", null ],
    [ "fpc_log_init", "fpc__log_8h.html#af49f45adc9877c0befef6b3af1b13ccf", null ],
    [ "fpc_log_level_get", "fpc__log_8h.html#a519b8eaa6b25c56e188fdf73a0518baa", null ],
    [ "fpc_log_level_set", "fpc__log_8h.html#ab823ceec83eea8b3190cb76f6fd4c3d6", null ],
    [ "fpc_log_mem", "fpc__log_8h.html#a02d3f57f5c1189fc6127a9da9520f795", null ],
    [ "fpc_log_var", "fpc__log_8h.html#ada35d6414ace89b3bfc03b6239a5310f", null ]
];