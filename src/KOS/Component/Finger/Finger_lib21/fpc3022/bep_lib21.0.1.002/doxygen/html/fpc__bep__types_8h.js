var fpc__bep__types_8h =
[
    [ "fpc_bep_image_t", "fpc__bep__types_8h.html#aac8124c8124fff6434f4442baa231459", null ],
    [ "fpc_bep_sensor_t", "fpc__bep__types_8h.html#aa5f53a6c77b54a8f592f9c81480e79a5", null ],
    [ "fpc_bep_config_t", "fpc__bep__types_8h.html#a37ee8052603322afdec6397af116d968", [
      [ "FPC_BEP_CONFIG_DISABLE", "fpc__bep__types_8h.html#a37ee8052603322afdec6397af116d968aaba90a2070f25aeff79264b22375491c", null ],
      [ "FPC_BEP_CONFIG_ENABLE", "fpc__bep__types_8h.html#a37ee8052603322afdec6397af116d968a7e332aa8d0678f80889eefd59d97eda2", null ]
    ] ],
    [ "fpc_bep_finger_status_t", "fpc__bep__types_8h.html#a1c3740269395cfccd0aef5ac41559a6b", [
      [ "FPC_BEP_FINGER_STATUS_UNDEFINED", "fpc__bep__types_8h.html#a1c3740269395cfccd0aef5ac41559a6ba5c83dce54a2bbf41492348220fe408bc", null ],
      [ "FPC_BEP_FINGER_STATUS_PRESENT", "fpc__bep__types_8h.html#a1c3740269395cfccd0aef5ac41559a6ba07df95998e03f48f2393fe595011cd61", null ],
      [ "FPC_BEP_FINGER_STATUS_NOT_PRESENT", "fpc__bep__types_8h.html#a1c3740269395cfccd0aef5ac41559a6ba6eec487445b2ef2dbe5c2501911803ef", null ],
      [ "FPC_BEP_FINGER_STATUS_PARTIAL", "fpc__bep__types_8h.html#a1c3740269395cfccd0aef5ac41559a6ba8908f945b53b633b6e60ba0e562c89d7", null ]
    ] ],
    [ "fpc_bep_result_t", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4", [
      [ "FPC_BEP_RESULT_OK", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4aee87626898fbfd622671a03b35f12ddc", null ],
      [ "FPC_BEP_RESULT_GENERAL_ERROR", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a853d1e690e912e7f7738ca82717dee4b", null ],
      [ "FPC_BEP_RESULT_INTERNAL_ERROR", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a7c79faff4a13f087af5d1ad247577570", null ],
      [ "FPC_BEP_RESULT_INVALID_ARGUMENT", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4ab73ef6ea129b50e5555e66759cfb1aed", null ],
      [ "FPC_BEP_RESULT_NOT_IMPLEMENTED", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4ab20c608bef4feb912be5fb6083619461", null ],
      [ "FPC_BEP_RESULT_CANCELLED", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a7d84a83ac06904dc5d613c19057c0bf6", null ],
      [ "FPC_BEP_RESULT_NO_MEMORY", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4ad4ffd1ec3731d32b970a669bb77fdd34", null ],
      [ "FPC_BEP_RESULT_NO_RESOURCE", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a4a73f08108b40087f6345ef5e57bbe81", null ],
      [ "FPC_BEP_RESULT_IO_ERROR", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a52b25fd6bf8b1a591740ee82d61f3d12", null ],
      [ "FPC_BEP_RESULT_BROKEN_SENSOR", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a172d674af7c82c8e349dae6fd4db050f", null ],
      [ "FPC_BEP_RESULT_WRONG_STATE", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4af766cc66419c840a162f0db60a6416e6", null ],
      [ "FPC_BEP_RESULT_TIMEOUT", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a35581ee1aec698a56bc68b26e800cd60", null ],
      [ "FPC_BEP_RESULT_ID_NOT_UNIQUE", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4ac3231d3bcd1952a8d8b2f20f633d4440", null ],
      [ "FPC_BEP_RESULT_ID_NOT_FOUND", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a3a035acb45193e42d5ec024a939fe3f0", null ],
      [ "FPC_BEP_RESULT_INVALID_FORMAT", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a3aea22028db593f12a297804f7a3c683", null ],
      [ "FPC_BEP_RESULT_IMAGE_CAPTURE_ERROR", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4af0d4b2ad8dd8a8c9dbf00cab630ffeb3", null ],
      [ "FPC_BEP_RESULT_SENSOR_MISMATCH", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4abe553e9b2a3b26b92db3a13e11c972e6", null ],
      [ "FPC_BEP_RESULT_MISSING_TEMPLATE", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4acf7cf146e115ca85f64c849fe0fb4506", null ],
      [ "FPC_BEP_RESULT_INVALID_CALIBRATION", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4aeabf36b68b69ef24d090a5ec1ca10d9d", null ],
      [ "FPC_BEP_RESULT_STORAGE_NOT_FORMATTED", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a5985c8ccfe631ebb081743d6d961172e", null ],
      [ "FPC_BEP_RESULT_SENSOR_NOT_INITIALIZED", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4aff307a27dfb2d6e183bdd01f7afb3492", null ],
      [ "FPC_BEP_RESULT_TOO_MANY_BAD_IMAGES", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4aca133611ed039051a066d85e42d9b813", null ],
      [ "FPC_BEP_RESULT_NOT_SUPPORTED", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a6718006bc4a66c7955ac0001c99c3176", null ],
      [ "FPC_BEP_RESULT_FINGER_NOT_STABLE", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a5b0ae903522b35ef2b4f1ab8ab4b6cae", null ],
      [ "FPC_BEP_RESULT_NOT_INITIALIZED", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4ae9dcba53630d44054e74dc5ea0c2ff0b", null ],
      [ "FPC_BEP_RESULT_TOO_MANY_DEAD_PIXELS", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a499b7fbf94bff0696d3487712938efb7", null ],
      [ "FPC_BEP_RESULT_SECURITY_VIOLATION", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a638c02f96cd1d266b778c0ed26f97ddc", null ],
      [ "FPC_BEP_RESULT_SENSOR_FIFO_UNDERFLOW", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4ad2b4d6ff72495c0bfdbf15153565fa93", null ],
      [ "FPC_BEP_RESULT_OTP_NO_DATA", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4aba7a352250e47b51e42e62123ef2a970", null ],
      [ "FPC_BEP_RESULT_SECURITY_CAPTURE_ERROR", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a1ee116ad7e8d7c5c8ffac9601069d2cf", null ],
      [ "FPC_BEP_RESULT_SENSOR_IRQ_NOT_HIGH", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4a42edcae58da6d02b7935f4434b580446", null ],
      [ "FPC_BEP_RESULT_SENSOR_IRQ_NOT_LOW", "fpc__bep__types_8h.html#adf46f9c3966202d9cf52a82981adaea4ada84c3d206b52e8981913831a90a1588", null ]
    ] ],
    [ "fpc_bep_sensor_driver_mechanism_t", "fpc__bep__types_8h.html#a58f034417a4e1bc9cef9137aa3aa031f", [
      [ "FPC_BEP_INTERRUPT_DRIVEN", "fpc__bep__types_8h.html#a58f034417a4e1bc9cef9137aa3aa031fa3e90c53c3d1bd719b55d76ba61b9cadc", null ],
      [ "FPC_BEP_POLLING", "fpc__bep__types_8h.html#a58f034417a4e1bc9cef9137aa3aa031fa66e4a6dd9f2298b114c777a8275817a0", null ]
    ] ],
    [ "fpc_bep_sensor_reset_t", "fpc__bep__types_8h.html#ac4da354010b9012ce2767da249de35b9", [
      [ "FPC_BEP_HARD_RESET", "fpc__bep__types_8h.html#ac4da354010b9012ce2767da249de35b9a6ef0de25d7da74f58a3861c55453da3d", null ],
      [ "FPC_BEP_SOFT_RESET", "fpc__bep__types_8h.html#ac4da354010b9012ce2767da249de35b9a0f5403529d3469496a55aeb2cffe640f", null ]
    ] ],
    [ "fpc_bep_sensor_state_after_release_t", "fpc__bep__types_8h.html#aa6e74077b38c2f61ddf287bc03c09579", [
      [ "FPC_BEP_SENSOR_DEEP_SLEEP", "fpc__bep__types_8h.html#aa6e74077b38c2f61ddf287bc03c09579aed3aaff12a171843c6c8764d4ce7718f", null ],
      [ "FPC_BEP_SENSOR_SLEEP", "fpc__bep__types_8h.html#aa6e74077b38c2f61ddf287bc03c09579a951113e4e4d7d91308aa134fdb8e6806", null ],
      [ "FPC_BEP_SENSOR_IDLE", "fpc__bep__types_8h.html#aa6e74077b38c2f61ddf287bc03c09579ae8d27b2d96c9ea3db354b9f3066d5dda", null ]
    ] ],
    [ "fpc_bep_sensor_type_t", "fpc__bep__types_8h.html#a2c9d883c6aa5a2e7f59227c6ecaaacc6", [
      [ "FPC_BEP_SENSOR_TYPE_UNDEFINED", "fpc__bep__types_8h.html#a2c9d883c6aa5a2e7f59227c6ecaaacc6a5f99f5de00564a10d43436554bcc94f5", null ],
      [ "FPC_BEP_SENSOR_TYPE_FPC1261", "fpc__bep__types_8h.html#a2c9d883c6aa5a2e7f59227c6ecaaacc6a6eb419f257f9480db18f0496e036108a", null ]
    ] ]
];