var structfpc__bep__contrast__result__t =
[
    [ "avg_std_dev", "structfpc__bep__contrast__result__t.html#a7ad48469a1dc531c426aec561b352a6f", null ],
    [ "max_std_dev", "structfpc__bep__contrast__result__t.html#a68f7d7c358619107a93fc04887624390", null ],
    [ "min_std_dev", "structfpc__bep__contrast__result__t.html#a29bfa0828859f15d1166b1d19abc2d53", null ],
    [ "total_subareas", "structfpc__bep__contrast__result__t.html#a7bb135578b33ddd27bceabaaadf0261d", null ]
];