var fpc__bep__bio_8h =
[
    [ "fpc_bep_enrollment_status_t", "structfpc__bep__enrollment__status__t.html", "structfpc__bep__enrollment__status__t" ],
    [ "fpc_bep_identify_result_t", "structfpc__bep__identify__result__t.html", "structfpc__bep__identify__result__t" ],
    [ "fpc_bep_template_t", "fpc__bep__bio_8h.html#a3528eaebdeaa50e05c860f61a3dceaea", null ],
    [ "fpc_bep_enrollment_feedback_t", "fpc__bep__bio_8h.html#a3d61f8ce6100b7a0fa811b998d8080db", [
      [ "FPC_BEP_ENROLLMENT_DONE", "fpc__bep__bio_8h.html#a3d61f8ce6100b7a0fa811b998d8080dbaec09acba691f41d7877348c11a28bb74", null ],
      [ "FPC_BEP_ENROLLMENT_PROGRESS", "fpc__bep__bio_8h.html#a3d61f8ce6100b7a0fa811b998d8080dbac4e7f4642cfef66f1b65b6d4110554aa", null ],
      [ "FPC_BEP_ENROLLMENT_REJECT_REASON_LOW_QUALITY", "fpc__bep__bio_8h.html#a3d61f8ce6100b7a0fa811b998d8080dbae7ca46810ab892b4aceae72190533dc1", null ],
      [ "FPC_BEP_ENROLLMENT_REJECT_REASON_LOW_SENSOR_COVERAGE", "fpc__bep__bio_8h.html#a3d61f8ce6100b7a0fa811b998d8080dba86c408b4cb4730439265d7e8237fe130", null ],
      [ "FPC_BEP_ENROLLMENT_REJECT_REASON_LOW_MOBILITY", "fpc__bep__bio_8h.html#a3d61f8ce6100b7a0fa811b998d8080dbac8a7db21b00d17ff7636918f4a453635", null ],
      [ "FPC_BEP_ENROLLMENT_REJECT_REASON_OTHER", "fpc__bep__bio_8h.html#a3d61f8ce6100b7a0fa811b998d8080dba10284a07633b6ba2ddf5b9fc369f7115", null ],
      [ "FPC_BEP_ENROLLMENT_PROGRESS_BUT_IMMOBILE", "fpc__bep__bio_8h.html#a3d61f8ce6100b7a0fa811b998d8080dba4790bed45091199143fb2865816b7bcc", null ]
    ] ],
    [ "fpc_bep_mem_release_t", "fpc__bep__bio_8h.html#af6bbdc6247cf0d53adf1e549f5f8e68e", [
      [ "FPC_BEP_DISABLE_MEM_RELEASE", "fpc__bep__bio_8h.html#af6bbdc6247cf0d53adf1e549f5f8e68ea0f13efc1c920538c346db3c3753deb01", null ],
      [ "FPC_BEP_ENABLE_MEM_RELEASE", "fpc__bep__bio_8h.html#af6bbdc6247cf0d53adf1e549f5f8e68ead49a911686811409fa8e168cb3c34ab9", null ]
    ] ],
    [ "fpc_bep_bio_get_recommended_param", "fpc__bep__bio_8h.html#abd2f8aad57e2b85d93851524c316ae18", null ],
    [ "fpc_bep_bio_init", "fpc__bep__bio_8h.html#a4940247601046998b3ebb85cfa7db167", null ],
    [ "fpc_bep_bio_release", "fpc__bep__bio_8h.html#a94b9e8f82bb3f7b1368796f9656943ef", null ],
    [ "fpc_bep_enroll", "fpc__bep__bio_8h.html#ac92dd04aad06f61d2478f6b2da2a11d1", null ],
    [ "fpc_bep_enroll_finish", "fpc__bep__bio_8h.html#a5053960d3306454772ee17e5ebf56a0b", null ],
    [ "fpc_bep_enroll_start", "fpc__bep__bio_8h.html#a1ca572e95088c6424ddffa2aadb26268", null ],
    [ "fpc_bep_identify", "fpc__bep__bio_8h.html#af93435c7808c792e117b56522fd8b267", null ],
    [ "fpc_bep_identify_release", "fpc__bep__bio_8h.html#a0ce8582ecd16b4b6c6f3d63ec1f788de", null ],
    [ "fpc_bep_image_extract", "fpc__bep__bio_8h.html#a1352282c1351b5229aa5025a1e3356d3", null ],
    [ "fpc_bep_template_delete", "fpc__bep__bio_8h.html#ab948ff41b9cfb34b42efa09482e628d0", null ],
    [ "fpc_bep_template_deserialize", "fpc__bep__bio_8h.html#a4eb8697483cca70a232a1d953c3c273c", null ],
    [ "fpc_bep_template_get_size", "fpc__bep__bio_8h.html#aab2a5b3d349d935782216c0068ae6bc3", null ],
    [ "fpc_bep_template_serialize", "fpc__bep__bio_8h.html#ad513de07e7353dbe8182ed8fbe817056", null ]
];