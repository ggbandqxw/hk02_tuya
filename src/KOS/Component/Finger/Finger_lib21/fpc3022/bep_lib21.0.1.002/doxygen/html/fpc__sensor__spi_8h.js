var fpc__sensor__spi_8h =
[
    [ "fpc_bep_wfi_check_t", "fpc__sensor__spi_8h.html#a6f07e0f8a500105af828f6574f282b92", null ],
    [ "fpc_bep_spi_duplex_mode_t", "fpc__sensor__spi_8h.html#aebf41e8e5f8e4af635136283c1863651", [
      [ "FPC_BEP_SPI_FULL_DUPLEX", "fpc__sensor__spi_8h.html#aebf41e8e5f8e4af635136283c1863651a0fa32003d55beac7e3173399d241815f", null ],
      [ "FPC_BEP_SPI_HALF_DUPLEX", "fpc__sensor__spi_8h.html#aebf41e8e5f8e4af635136283c1863651a3e48b4a782d1557049c3c073c546ef00", null ]
    ] ],
    [ "fpc_sensor_spi_check_irq", "fpc__sensor__spi_8h.html#a0330b0b1f58c340016e6bd403a9237c5", null ],
    [ "fpc_sensor_spi_get_duplex_mode", "fpc__sensor__spi_8h.html#a88ab296f9e86448906262deed2063aa8", null ],
    [ "fpc_sensor_spi_init", "fpc__sensor__spi_8h.html#aa5ebacf0a7cefecf27b08a47ba511760", null ],
    [ "fpc_sensor_spi_read_irq", "fpc__sensor__spi_8h.html#af00eb26e4dfd2a354d89fc281c383106", null ],
    [ "fpc_sensor_spi_reset", "fpc__sensor__spi_8h.html#a1d3250f59a2e50929ac1c10310dbdab0", null ],
    [ "fpc_sensor_spi_write_read", "fpc__sensor__spi_8h.html#a8863f62e7531ba270ae8fcae30ad747a", null ],
    [ "fpc_sensor_wfi", "fpc__sensor__spi_8h.html#a38db7e8faa7af2006efee2f91ff0b9f7", null ]
];