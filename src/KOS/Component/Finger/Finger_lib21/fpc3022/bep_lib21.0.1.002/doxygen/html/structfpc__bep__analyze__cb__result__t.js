var structfpc__bep__analyze__cb__result__t =
[
    [ "median_type1_max", "structfpc__bep__analyze__cb__result__t.html#adbde36e25ba4d8f8e8627f393dd997dd", null ],
    [ "median_type1_min", "structfpc__bep__analyze__cb__result__t.html#a3a5466c7b7dcafdc2f02ad2223ad48ca", null ],
    [ "median_type2_max", "structfpc__bep__analyze__cb__result__t.html#a4b2e92c79dcc8c90da3ab6f590f35641", null ],
    [ "median_type2_min", "structfpc__bep__analyze__cb__result__t.html#ac155dbf07f58150f9dec0494805c2fea", null ],
    [ "num_of_defect_pixels", "structfpc__bep__analyze__cb__result__t.html#a6a29aaef572499bdbbaa59bebe3a0cfd", null ],
    [ "num_of_defect_pixels_in_detect_zones", "structfpc__bep__analyze__cb__result__t.html#ade0f323dcb9703cf26cd16849a74c1cd", null ]
];