var searchData=
[
  ['name',['name',['../structfpc__bep__sensor__prop__t.html#ac99cecd6ed09b4a779c685a30184b846',1,'fpc_bep_sensor_prop_t']]],
  ['nbr_5fof_5ffinger_5fpresent_5fzones',['nbr_of_finger_present_zones',['../structfpc__bep__sensor__param__t.html#a48c1fdae6a1d0dd1916732dfbadc6661',1,'fpc_bep_sensor_param_t']]],
  ['nbr_5fof_5fimages',['nbr_of_images',['../structfpc__bep__enroll__param__t.html#a84aed4a42d84bf3a34c0217135d7afa9',1,'fpc_bep_enroll_param_t']]],
  ['num_5fof_5fdefect_5fpixels',['num_of_defect_pixels',['../structfpc__bep__analyze__reset__result__t.html#a026d91e0e9d59df7d5413e6fc062f5fa',1,'fpc_bep_analyze_reset_result_t::num_of_defect_pixels()'],['../structfpc__bep__analyze__cb__result__t.html#a6a29aaef572499bdbbaa59bebe3a0cfd',1,'fpc_bep_analyze_cb_result_t::num_of_defect_pixels()']]],
  ['num_5fof_5fdefect_5fpixels_5fin_5fdetect_5fzones',['num_of_defect_pixels_in_detect_zones',['../structfpc__bep__analyze__cb__result__t.html#ade0f323dcb9703cf26cd16849a74c1cd',1,'fpc_bep_analyze_cb_result_t']]],
  ['num_5fsub_5fareas_5fheight',['num_sub_areas_height',['../structfpc__bep__sensor__prop__t.html#a3330302acf537f05f18aa31847962dcf',1,'fpc_bep_sensor_prop_t']]],
  ['num_5fsub_5fareas_5fwidth',['num_sub_areas_width',['../structfpc__bep__sensor__prop__t.html#a41d075bafc066cdd86bfca185cc6ce62',1,'fpc_bep_sensor_prop_t']]]
];
