var fpc__bep__image_8h =
[
    [ "fpc_bep_image_dimensions_t", "structfpc__bep__image__dimensions__t.html", "structfpc__bep__image__dimensions__t" ],
    [ "fpc_bep_image_delete", "fpc__bep__image_8h.html#a1b516d4d673c231f8441f348ef39f3b9", null ],
    [ "fpc_bep_image_get_dimensions", "fpc__bep__image_8h.html#aaf44e4aa57fb7e23addf238098015d6b", null ],
    [ "fpc_bep_image_get_pixels", "fpc__bep__image_8h.html#a33ccf8d2b78ebe7b57a02c4110632984", null ],
    [ "fpc_bep_image_get_size", "fpc__bep__image_8h.html#ac67c0a74b41329c8386b2ee4a7d1f34c", null ],
    [ "fpc_bep_image_new", "fpc__bep__image_8h.html#aacb10dffb536ff553815d53558a77bf2", null ]
];