var fpc__bep__bio__param_8h =
[
    [ "fpc_bep_general_param_t", "structfpc__bep__general__param__t.html", "structfpc__bep__general__param__t" ],
    [ "fpc_bep_enroll_param_t", "structfpc__bep__enroll__param__t.html", "structfpc__bep__enroll__param__t" ],
    [ "fpc_bep_identify_param_t", "structfpc__bep__identify__param__t.html", "structfpc__bep__identify__param__t" ],
    [ "fpc_bep_bio_param_t", "structfpc__bep__bio__param__t.html", "structfpc__bep__bio__param__t" ],
    [ "fpc_bep_enroll_scheme_t", "fpc__bep__bio__param_8h.html#a7b2c65bc4f81f00b74ef1cd11c9c7e01", [
      [ "FPC_BEP_ENROLL_SCHEME_TOUCH", "fpc__bep__bio__param_8h.html#a7b2c65bc4f81f00b74ef1cd11c9c7e01a6668f3221c57cb37c6809f4cedbef8f6", null ],
      [ "FPC_BEP_ENROLL_SCHEME_STRICT", "fpc__bep__bio__param_8h.html#a7b2c65bc4f81f00b74ef1cd11c9c7e01a319b4962caf4e63f4fcc41e177fd677c", null ],
      [ "FPC_BEP_ENROLL_SCHEME_CUSTOM", "fpc__bep__bio__param_8h.html#a7b2c65bc4f81f00b74ef1cd11c9c7e01a7362f75dd340dfe7da374c3929e18ca4", null ]
    ] ],
    [ "fpc_bep_latency_scheme_t", "fpc__bep__bio__param_8h.html#a65019d87918cf7c2e3fbe40c5e75e0f4", [
      [ "FPC_BEP_LATENCY_SCHEME_STANDARD", "fpc__bep__bio__param_8h.html#a65019d87918cf7c2e3fbe40c5e75e0f4ad40b69cb955655a0fb95baeff0aa9118", null ]
    ] ],
    [ "fpc_bep_security_level_t", "fpc__bep__bio__param_8h.html#a80e4c156c4cb37fb3abb656fe4d1527d", [
      [ "FPC_BEP_SECURITY_LEVEL_FAR_20K", "fpc__bep__bio__param_8h.html#a80e4c156c4cb37fb3abb656fe4d1527da0238967f8e6cea6a73c0c87432ce6a35", null ],
      [ "FPC_BEP_SECURITY_LEVEL_FAR_50K", "fpc__bep__bio__param_8h.html#a80e4c156c4cb37fb3abb656fe4d1527da67b8b285cb1c9e0dbbc7d6baebf2db27", null ],
      [ "FPC_BEP_SECURITY_LEVEL_FAR_100K", "fpc__bep__bio__param_8h.html#a80e4c156c4cb37fb3abb656fe4d1527da47ab055ea5c68a2ba303d09c7d2d6c02", null ],
      [ "FPC_BEP_SECURITY_LEVEL_FAR_500K", "fpc__bep__bio__param_8h.html#a80e4c156c4cb37fb3abb656fe4d1527da073a2394e86507da875a79e81f148fb7", null ]
    ] ]
];