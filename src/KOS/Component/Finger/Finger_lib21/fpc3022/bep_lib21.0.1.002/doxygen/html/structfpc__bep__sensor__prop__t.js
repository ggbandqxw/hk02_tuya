var structfpc__bep__sensor__prop__t =
[
    [ "dpi", "structfpc__bep__sensor__prop__t.html#a85ab2fb3bda9f1c60ef5dbec8cce0390", null ],
    [ "height", "structfpc__bep__sensor__prop__t.html#a0dd19cbe97e48d05756886c57265e979", null ],
    [ "max_spi_clock", "structfpc__bep__sensor__prop__t.html#a338821461c454697d8dca8132589ed9a", null ],
    [ "name", "structfpc__bep__sensor__prop__t.html#ac99cecd6ed09b4a779c685a30184b846", null ],
    [ "num_sub_areas_height", "structfpc__bep__sensor__prop__t.html#a3330302acf537f05f18aa31847962dcf", null ],
    [ "num_sub_areas_width", "structfpc__bep__sensor__prop__t.html#a41d075bafc066cdd86bfca185cc6ce62", null ],
    [ "sensor_type", "structfpc__bep__sensor__prop__t.html#ae94c13d34ab7f678e6e991ed1a6f3339", null ],
    [ "width", "structfpc__bep__sensor__prop__t.html#a5741102024b51a8e11dbcf0e8776eb43", null ]
];