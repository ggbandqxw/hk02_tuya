<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>fpc_bep_algorithm.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__algorithm_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <member kind="typedef">
      <type>struct fpc_bep_algorithm</type>
      <name>fpc_bep_algorithm_t</name>
      <anchorfile>fpc__bep__algorithm_8h.html</anchorfile>
      <anchor>a62308e92cbea62c87cc7a19afafffe78</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>fpc_bep_algorithm_get_info</name>
      <anchorfile>fpc__bep__algorithm_8h.html</anchorfile>
      <anchor>a02f20801b51c58f12785271047160c97</anchor>
      <arglist>(const fpc_bep_algorithm_t *algorithm)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_algorithm_get_max_template_size</name>
      <anchorfile>fpc__bep__algorithm_8h.html</anchorfile>
      <anchor>a7eadb9b6c57c9bd530f5b915ee6b9ce1</anchor>
      <arglist>(const fpc_bep_algorithm_t *algorithm, size_t *size)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_sensor_type_t</type>
      <name>fpc_bep_algorithm_get_sensor_type</name>
      <anchorfile>fpc__bep__algorithm_8h.html</anchorfile>
      <anchor>aecb52efea0dbb5edac4dea96e78a5fb3</anchor>
      <arglist>(const fpc_bep_algorithm_t *algorithm)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_algorithms.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__algorithms_8h</filename>
    <includes id="fpc__bep__algorithm_8h" name="fpc_bep_algorithm.h" local="yes" imported="no">fpc_bep_algorithm.h</includes>
    <member kind="variable">
      <type>const fpc_bep_algorithm_t</type>
      <name>fpc_bep_algorithm_pfe_1261</name>
      <anchorfile>fpc__bep__algorithms_8h.html</anchorfile>
      <anchor>ae7da79791d64444257993c67d380bf7d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_bio.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__bio_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <includes id="fpc__bep__algorithm_8h" name="fpc_bep_algorithm.h" local="yes" imported="no">fpc_bep_algorithm.h</includes>
    <includes id="fpc__bep__bio__param_8h" name="fpc_bep_bio_param.h" local="yes" imported="no">fpc_bep_bio_param.h</includes>
    <class kind="struct">fpc_bep_enrollment_status_t</class>
    <class kind="struct">fpc_bep_identify_result_t</class>
    <member kind="typedef">
      <type>struct fpc_bep_template</type>
      <name>fpc_bep_template_t</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3528eaebdeaa50e05c860f61a3dceaea</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_mem_release_t</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>af6bbdc6247cf0d53adf1e549f5f8e68e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_DISABLE_MEM_RELEASE</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>af6bbdc6247cf0d53adf1e549f5f8e68ea0f13efc1c920538c346db3c3753deb01</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENABLE_MEM_RELEASE</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>af6bbdc6247cf0d53adf1e549f5f8e68ead49a911686811409fa8e168cb3c34ab9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_enrollment_feedback_t</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3d61f8ce6100b7a0fa811b998d8080db</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLLMENT_DONE</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3d61f8ce6100b7a0fa811b998d8080dbaec09acba691f41d7877348c11a28bb74</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLLMENT_PROGRESS</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3d61f8ce6100b7a0fa811b998d8080dbac4e7f4642cfef66f1b65b6d4110554aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLLMENT_REJECT_REASON_LOW_QUALITY</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3d61f8ce6100b7a0fa811b998d8080dbae7ca46810ab892b4aceae72190533dc1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLLMENT_REJECT_REASON_LOW_SENSOR_COVERAGE</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3d61f8ce6100b7a0fa811b998d8080dba86c408b4cb4730439265d7e8237fe130</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLLMENT_REJECT_REASON_LOW_MOBILITY</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3d61f8ce6100b7a0fa811b998d8080dbac8a7db21b00d17ff7636918f4a453635</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLLMENT_REJECT_REASON_OTHER</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3d61f8ce6100b7a0fa811b998d8080dba10284a07633b6ba2ddf5b9fc369f7115</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLLMENT_PROGRESS_BUT_IMMOBILE</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a3d61f8ce6100b7a0fa811b998d8080dba4790bed45091199143fb2865816b7bcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_bio_get_recommended_param</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>abd2f8aad57e2b85d93851524c316ae18</anchor>
      <arglist>(const fpc_bep_algorithm_t *algorithm, fpc_bep_bio_param_t *param)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_bio_init</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a4940247601046998b3ebb85cfa7db167</anchor>
      <arglist>(fpc_bep_bio_param_t *param, const fpc_bep_algorithm_t *algorithm)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_bio_release</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a94b9e8f82bb3f7b1368796f9656943ef</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_image_extract</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a1352282c1351b5229aa5025a1e3356d3</anchor>
      <arglist>(fpc_bep_image_t **image, fpc_bep_template_t **template)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_template_get_size</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>aab2a5b3d349d935782216c0068ae6bc3</anchor>
      <arglist>(const fpc_bep_template_t *template, size_t *size)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_template_serialize</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>ad513de07e7353dbe8182ed8fbe817056</anchor>
      <arglist>(const fpc_bep_template_t *template, uint8_t *data, size_t data_size)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_template_deserialize</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a4eb8697483cca70a232a1d953c3c273c</anchor>
      <arglist>(fpc_bep_template_t **template, uint8_t *data, size_t data_size, fpc_bep_mem_release_t data_release)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_template_delete</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>ab948ff41b9cfb34b42efa09482e628d0</anchor>
      <arglist>(fpc_bep_template_t **template)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_identify</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>af93435c7808c792e117b56522fd8b267</anchor>
      <arglist>(const fpc_bep_template_t **candidates, const size_t candidates_count, fpc_bep_identify_result_t *result)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_identify_release</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a0ce8582ecd16b4b6c6f3d63ec1f788de</anchor>
      <arglist>(bool *update_template)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_enroll_start</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a1ca572e95088c6424ddffa2aadb26268</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_enroll_finish</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>a5053960d3306454772ee17e5ebf56a0b</anchor>
      <arglist>(fpc_bep_template_t **template)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_enroll</name>
      <anchorfile>fpc__bep__bio_8h.html</anchorfile>
      <anchor>ac92dd04aad06f61d2478f6b2da2a11d1</anchor>
      <arglist>(fpc_bep_image_t *image, fpc_bep_enrollment_status_t *status)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_bio_param.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__bio__param_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <class kind="struct">fpc_bep_general_param_t</class>
    <class kind="struct">fpc_bep_enroll_param_t</class>
    <class kind="struct">fpc_bep_identify_param_t</class>
    <class kind="struct">fpc_bep_bio_param_t</class>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_enroll_scheme_t</name>
      <anchorfile>fpc__bep__bio__param_8h.html</anchorfile>
      <anchor>a7b2c65bc4f81f00b74ef1cd11c9c7e01</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLL_SCHEME_TOUCH</name>
      <anchorfile>fpc__bep__bio__param_8h.html</anchorfile>
      <anchor>a7b2c65bc4f81f00b74ef1cd11c9c7e01a6668f3221c57cb37c6809f4cedbef8f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLL_SCHEME_STRICT</name>
      <anchorfile>fpc__bep__bio__param_8h.html</anchorfile>
      <anchor>a7b2c65bc4f81f00b74ef1cd11c9c7e01a319b4962caf4e63f4fcc41e177fd677c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_ENROLL_SCHEME_CUSTOM</name>
      <anchorfile>fpc__bep__bio__param_8h.html</anchorfile>
      <anchor>a7b2c65bc4f81f00b74ef1cd11c9c7e01a7362f75dd340dfe7da374c3929e18ca4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_latency_scheme_t</name>
      <anchorfile>fpc__bep__bio__param_8h.html</anchorfile>
      <anchor>a65019d87918cf7c2e3fbe40c5e75e0f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_LATENCY_SCHEME_STANDARD</name>
      <anchorfile>fpc__bep__bio__param_8h.html</anchorfile>
      <anchor>a65019d87918cf7c2e3fbe40c5e75e0f4ad40b69cb955655a0fb95baeff0aa9118</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_security_level_t</name>
      <anchorfile>fpc__bep__bio__param_8h.html</anchorfile>
      <anchor>a80e4c156c4cb37fb3abb656fe4d1527d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_calibration.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__calibration_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_cal_get</name>
      <anchorfile>fpc__bep__calibration_8h.html</anchorfile>
      <anchor>a845544bf5963fc762c3b5f54f26f9ab4</anchor>
      <arglist>(uint8_t **calibration, size_t *size)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_diag.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__diag_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <member kind="function">
      <type>void</type>
      <name>fpc_bep_diag_trace_enable</name>
      <anchorfile>fpc__bep__diag_8h.html</anchorfile>
      <anchor>af9ffb989abc5dfd45dde8c6fcbc54d5a</anchor>
      <arglist>(bool enable)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_bep_diag_enable</name>
      <anchorfile>fpc__bep__diag_8h.html</anchorfile>
      <anchor>aa3f9b147be9840ee504c3d2809bd4fd8</anchor>
      <arglist>(bool enable)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_diag_reg_dump_enable</name>
      <anchorfile>fpc__bep__diag_8h.html</anchorfile>
      <anchor>a4904a8db8b09977baee6a8e4e049efab</anchor>
      <arglist>(bool enable)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_bep_diag_dump</name>
      <anchorfile>fpc__bep__diag_8h.html</anchorfile>
      <anchor>af082ccb9204c088c40d5e2cae0cf29b0</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_image.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__image_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <class kind="struct">fpc_bep_image_dimensions_t</class>
    <member kind="function">
      <type>fpc_bep_image_t *</type>
      <name>fpc_bep_image_new</name>
      <anchorfile>fpc__bep__image_8h.html</anchorfile>
      <anchor>aacb10dffb536ff553815d53558a77bf2</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_bep_image_delete</name>
      <anchorfile>fpc__bep__image_8h.html</anchorfile>
      <anchor>a1b516d4d673c231f8441f348ef39f3b9</anchor>
      <arglist>(fpc_bep_image_t **image)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>fpc_bep_image_get_size</name>
      <anchorfile>fpc__bep__image_8h.html</anchorfile>
      <anchor>ac67c0a74b41329c8386b2ee4a7d1f34c</anchor>
      <arglist>(const fpc_bep_image_t *image)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t *</type>
      <name>fpc_bep_image_get_pixels</name>
      <anchorfile>fpc__bep__image_8h.html</anchorfile>
      <anchor>a33ccf8d2b78ebe7b57a02c4110632984</anchor>
      <arglist>(const fpc_bep_image_t *image)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_image_get_dimensions</name>
      <anchorfile>fpc__bep__image_8h.html</anchorfile>
      <anchor>aaf44e4aa57fb7e23addf238098015d6b</anchor>
      <arglist>(const fpc_bep_image_t *image, fpc_bep_image_dimensions_t *dimensions)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_sensor.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__sensor_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <class kind="struct">fpc_bep_sensor_prop_t</class>
    <class kind="struct">fpc_bep_sensor_param_t</class>
    <member kind="function">
      <type>fpc_bep_sensor_type_t</type>
      <name>fpc_bep_sensor_get_type</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>afe89d73d3f6317bc17f96d40cfc2fd5c</anchor>
      <arglist>(const fpc_bep_sensor_t *sensor)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_get_recommended_param</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>a9f7816865c3cbb3e528b72a10e51f760</anchor>
      <arglist>(const fpc_bep_sensor_t *sensor, fpc_bep_sensor_param_t *param)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_init</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>a7d7ca04903a7b5f993ac7a4019f97b3e</anchor>
      <arglist>(const fpc_bep_sensor_t *sensor, const uint8_t *calibration, const fpc_bep_sensor_param_t *param)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_release</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>ad079e331d8f2db958daedb175de7df16</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_sleep</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>ae3dde0b3a7ee4b46f3edb3a5e57e527a</anchor>
      <arglist>(uint16_t sleep_time)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fpc_bep_finger_detect</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>a23bc3ffb7309cffebf1242d6a77d6798</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_keep_in_idle</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>adc02283e359669904ea35544b81d8782</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_deep_sleep</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>a6d9cdb24d60dbdfa50eed9c87ec366f3</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_check_finger_present</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>abff59351f56fca4d0ac5de8e0027bd39</anchor>
      <arglist>(fpc_bep_finger_status_t *finger_status)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_get_finger_detect_zones</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>a70d88fd94696259b75f110477b79db1b</anchor>
      <arglist>(uint16_t *finger_detect_zones)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_capture</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>ab5648089bf85fd31b4adde578783c33d</anchor>
      <arglist>(fpc_bep_image_t *image)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_reset</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>a46d95bd3e7999db7029cee1c7c8d37e1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_properties</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>a1efdd9b9237ee66570e9adfdb806ccce</anchor>
      <arglist>(fpc_bep_sensor_prop_t *properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_bep_signal_image_capture_start</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>a6c6fd4640fe4d05846a9d8f0268ecd7e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_read_otp</name>
      <anchorfile>fpc__bep__sensor_8h.html</anchorfile>
      <anchor>ac802aa6f23460af1a9c0c695a4e01c6a</anchor>
      <arglist>(uint8_t addr, uint8_t *data, size_t size)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_sensor_test.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__sensor__test_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <class kind="struct">fpc_bep_contrast_result_t</class>
    <class kind="struct">fpc_bep_analyze_reset_result_t</class>
    <class kind="struct">fpc_bep_analyze_cb_result_t</class>
    <class kind="union">fpc_bep_analyze_result_t</class>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_test_image_size_t</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>acd671b97dee6f3432ae36a1fcb7e18b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_TEST_IMAGE_LARGE</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>acd671b97dee6f3432ae36a1fcb7e18b5a3f61668be219796639d85e7b3aa06550</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_TEST_IMAGE_MEDIUM</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>acd671b97dee6f3432ae36a1fcb7e18b5a626f43a7b6299ba2214d683c58dc4dfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_TEST_IMAGE_SMALL</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>acd671b97dee6f3432ae36a1fcb7e18b5a27f15ce0f0e98e795bb0e0a245039e02</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_capture_test_mode_t</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>ad262a0e939dafdfa29231f48766425b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_CAPTURE_TEST_MODE_RESET</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>ad262a0e939dafdfa29231f48766425b8a7e4642a3c36a4cc7f72cca93dd264ba4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_CAPTURE_TEST_MODE_CHECKERBOARD</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>ad262a0e939dafdfa29231f48766425b8a0586b1d43e0f88a180dcd38b8b831205</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_CAPTURE_TEST_MODE_CHECKERBOARD_INVERTED</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>ad262a0e939dafdfa29231f48766425b8ac6333f2b03cac46cc68d9115751b9301</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_prod_test</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>ad1367d79d9f4921065301991fa2bab37</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_prod_test_capture</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>af217aac418ce35227bcefe9000b1db12</anchor>
      <arglist>(fpc_bep_test_image_size_t image_size)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_prod_test_mode_capture</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>a4960b94e48307abfc393fe0c57e44163</anchor>
      <arglist>(fpc_bep_image_t *image, fpc_bep_capture_test_mode_t mode)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_prod_test_mode_analyze</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>a94dade3f80636b4d283aac2128a77e21</anchor>
      <arglist>(fpc_bep_image_t *image, fpc_bep_capture_test_mode_t mode, fpc_bep_analyze_result_t *test_result)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_prod_test_contrast</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>aa8bc82869c11bd0d8b509b6050a0cd0b</anchor>
      <arglist>(fpc_bep_image_t *image, fpc_bep_contrast_result_t *test_result)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_bep_sensor_otp_test</name>
      <anchorfile>fpc__bep__sensor__test_8h.html</anchorfile>
      <anchor>a5f6b1338c5109b998359db9cd9ca573d</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_types.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__types_8h</filename>
    <member kind="typedef">
      <type>struct fpc_bep_image</type>
      <name>fpc_bep_image_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>aac8124c8124fff6434f4442baa231459</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>struct fpc_bep_sensor</type>
      <name>fpc_bep_sensor_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>aa5f53a6c77b54a8f592f9c81480e79a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_result_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_OK</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4aee87626898fbfd622671a03b35f12ddc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_GENERAL_ERROR</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a853d1e690e912e7f7738ca82717dee4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_INTERNAL_ERROR</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a7c79faff4a13f087af5d1ad247577570</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_INVALID_ARGUMENT</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4ab73ef6ea129b50e5555e66759cfb1aed</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_NOT_IMPLEMENTED</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4ab20c608bef4feb912be5fb6083619461</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_CANCELLED</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a7d84a83ac06904dc5d613c19057c0bf6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_NO_MEMORY</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4ad4ffd1ec3731d32b970a669bb77fdd34</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_NO_RESOURCE</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a4a73f08108b40087f6345ef5e57bbe81</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_IO_ERROR</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a52b25fd6bf8b1a591740ee82d61f3d12</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_BROKEN_SENSOR</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a172d674af7c82c8e349dae6fd4db050f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_WRONG_STATE</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4af766cc66419c840a162f0db60a6416e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_TIMEOUT</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a35581ee1aec698a56bc68b26e800cd60</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_ID_NOT_UNIQUE</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4ac3231d3bcd1952a8d8b2f20f633d4440</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_ID_NOT_FOUND</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a3a035acb45193e42d5ec024a939fe3f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_INVALID_FORMAT</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a3aea22028db593f12a297804f7a3c683</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_IMAGE_CAPTURE_ERROR</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4af0d4b2ad8dd8a8c9dbf00cab630ffeb3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_SENSOR_MISMATCH</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4abe553e9b2a3b26b92db3a13e11c972e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_MISSING_TEMPLATE</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4acf7cf146e115ca85f64c849fe0fb4506</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_INVALID_CALIBRATION</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4aeabf36b68b69ef24d090a5ec1ca10d9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_STORAGE_NOT_FORMATTED</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a5985c8ccfe631ebb081743d6d961172e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_SENSOR_NOT_INITIALIZED</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4aff307a27dfb2d6e183bdd01f7afb3492</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_TOO_MANY_BAD_IMAGES</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4aca133611ed039051a066d85e42d9b813</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_NOT_SUPPORTED</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a6718006bc4a66c7955ac0001c99c3176</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_FINGER_NOT_STABLE</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a5b0ae903522b35ef2b4f1ab8ab4b6cae</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_NOT_INITIALIZED</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4ae9dcba53630d44054e74dc5ea0c2ff0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_TOO_MANY_DEAD_PIXELS</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a499b7fbf94bff0696d3487712938efb7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_SECURITY_VIOLATION</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a638c02f96cd1d266b778c0ed26f97ddc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_SENSOR_FIFO_UNDERFLOW</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4ad2b4d6ff72495c0bfdbf15153565fa93</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_OTP_NO_DATA</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4aba7a352250e47b51e42e62123ef2a970</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_SECURITY_CAPTURE_ERROR</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a1ee116ad7e8d7c5c8ffac9601069d2cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_SENSOR_IRQ_NOT_HIGH</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4a42edcae58da6d02b7935f4434b580446</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_RESULT_SENSOR_IRQ_NOT_LOW</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>adf46f9c3966202d9cf52a82981adaea4ada84c3d206b52e8981913831a90a1588</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_sensor_type_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a2c9d883c6aa5a2e7f59227c6ecaaacc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_finger_status_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a1c3740269395cfccd0aef5ac41559a6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_FINGER_STATUS_UNDEFINED</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a1c3740269395cfccd0aef5ac41559a6ba5c83dce54a2bbf41492348220fe408bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_FINGER_STATUS_PRESENT</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a1c3740269395cfccd0aef5ac41559a6ba07df95998e03f48f2393fe595011cd61</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_FINGER_STATUS_NOT_PRESENT</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a1c3740269395cfccd0aef5ac41559a6ba6eec487445b2ef2dbe5c2501911803ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_FINGER_STATUS_PARTIAL</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a1c3740269395cfccd0aef5ac41559a6ba8908f945b53b633b6e60ba0e562c89d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_sensor_driver_mechanism_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a58f034417a4e1bc9cef9137aa3aa031f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_INTERRUPT_DRIVEN</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a58f034417a4e1bc9cef9137aa3aa031fa3e90c53c3d1bd719b55d76ba61b9cadc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_POLLING</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a58f034417a4e1bc9cef9137aa3aa031fa66e4a6dd9f2298b114c777a8275817a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_sensor_reset_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>ac4da354010b9012ce2767da249de35b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_HARD_RESET</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>ac4da354010b9012ce2767da249de35b9a6ef0de25d7da74f58a3861c55453da3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_SOFT_RESET</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>ac4da354010b9012ce2767da249de35b9a0f5403529d3469496a55aeb2cffe640f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_sensor_state_after_release_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>aa6e74077b38c2f61ddf287bc03c09579</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_SENSOR_DEEP_SLEEP</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>aa6e74077b38c2f61ddf287bc03c09579aed3aaff12a171843c6c8764d4ce7718f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_SENSOR_SLEEP</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>aa6e74077b38c2f61ddf287bc03c09579a951113e4e4d7d91308aa134fdb8e6806</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_BEP_SENSOR_IDLE</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>aa6e74077b38c2f61ddf287bc03c09579ae8d27b2d96c9ea3db354b9f3066d5dda</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_config_t</name>
      <anchorfile>fpc__bep__types_8h.html</anchorfile>
      <anchor>a37ee8052603322afdec6397af116d968</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_bep_version.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/api/inc/</path>
    <filename>fpc__bep__version_8h</filename>
    <member kind="function">
      <type>const char *</type>
      <name>fpc_bep_get_version</name>
      <anchorfile>fpc__bep__version_8h.html</anchorfile>
      <anchor>a8ce35c0e2796273fe1c49caacf9794fb</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_malloc.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/req_interface/inc/</path>
    <filename>fpc__malloc_8h</filename>
    <member kind="function">
      <type>void *</type>
      <name>fpc_malloc</name>
      <anchorfile>fpc__malloc_8h.html</anchorfile>
      <anchor>a06205b99a6dbc7715a784f8a08b0a2c9</anchor>
      <arglist>(size_t size)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_free</name>
      <anchorfile>fpc__malloc_8h.html</anchorfile>
      <anchor>ae60c0733d33adb5dd5d32be7b7f150d0</anchor>
      <arglist>(void *data)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_sensor_spi.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/req_interface/inc/</path>
    <filename>fpc__sensor__spi_8h</filename>
    <includes id="fpc__bep__types_8h" name="fpc_bep_types.h" local="yes" imported="no">fpc_bep_types.h</includes>
    <member kind="typedef">
      <type>bool(*</type>
      <name>fpc_bep_wfi_check_t</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>a6f07e0f8a500105af828f6574f282b92</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_bep_spi_duplex_mode_t</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>aebf41e8e5f8e4af635136283c1863651</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_sensor_spi_write_read</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>a8863f62e7531ba270ae8fcae30ad747a</anchor>
      <arglist>(uint8_t *data, size_t write_size, size_t read_size, bool leave_cs_asserted)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fpc_sensor_spi_check_irq</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>a0330b0b1f58c340016e6bd403a9237c5</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fpc_sensor_spi_read_irq</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>af00eb26e4dfd2a354d89fc281c383106</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_sensor_spi_reset</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>a1d3250f59a2e50929ac1c10310dbdab0</anchor>
      <arglist>(bool state)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_sensor_spi_init</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>aa5ebacf0a7cefecf27b08a47ba511760</anchor>
      <arglist>(uint32_t speed_hz)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_spi_duplex_mode_t</type>
      <name>fpc_sensor_spi_get_duplex_mode</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>a88ab296f9e86448906262deed2063aa8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>fpc_bep_result_t</type>
      <name>fpc_sensor_wfi</name>
      <anchorfile>fpc__sensor__spi_8h.html</anchorfile>
      <anchor>a38db7e8faa7af2006efee2f91ff0b9f7</anchor>
      <arglist>(uint16_t timeout_ms, fpc_bep_wfi_check_t enter_wfi, bool enter_wfi_mode)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_timebase.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/req_interface/inc/</path>
    <filename>fpc__timebase_8h</filename>
    <member kind="function">
      <type>uint32_t</type>
      <name>fpc_timebase_get_tick</name>
      <anchorfile>fpc__timebase_8h.html</anchorfile>
      <anchor>a9a06fbb5fce95e153a82d1d6084e31eb</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_timebase_delay_ms</name>
      <anchorfile>fpc__timebase_8h.html</anchorfile>
      <anchor>ab4344ced36900ee2a274f90fae673b8d</anchor>
      <arglist>(uint32_t ms)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_timebase_delay_us</name>
      <anchorfile>fpc__timebase_8h.html</anchorfile>
      <anchor>a7fc231a5570eade1f6e819cfe02fa9a2</anchor>
      <arglist>(uint32_t us)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_timebase_init</name>
      <anchorfile>fpc__timebase_8h.html</anchorfile>
      <anchor>ac460a539a284d1628a13046259fe18d1</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_assert.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/util_interface/inc/</path>
    <filename>fpc__assert_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>fpc_assert</name>
      <anchorfile>fpc__assert_8h.html</anchorfile>
      <anchor>a76ea31132ea7525dc8cfdf7ddaedb067</anchor>
      <arglist>(_e)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_assert_fail</name>
      <anchorfile>fpc__assert_8h.html</anchorfile>
      <anchor>af2dc8f4ecb5c18066deacb634bc6dbfb</anchor>
      <arglist>(const char *file, uint32_t line, const char *func, const char *expr)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_log.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/util_interface/inc/</path>
    <filename>fpc__log_8h</filename>
    <includes id="fpc__log__config_8h" name="fpc_log_config.h" local="yes" imported="no">fpc_log_config.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>fpc_log_printf</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a74c002a00ca396ca0ac0112469d429a7</anchor>
      <arglist>(format,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>fpc_log_printfe</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a6fb7155f1201ef1aa4f31eca33d125cf</anchor>
      <arglist>(format,...)</arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>fpc_log_level_t</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a02649358843828935cdb6842c4786cf8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_LOG_LEVEL_ALERT</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a02649358843828935cdb6842c4786cf8a250c6c23e107e9d063c2e438bac5ec9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_LOG_LEVEL_ERROR</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a02649358843828935cdb6842c4786cf8a187a52023d07343422fb64758928c56e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_LOG_LEVEL_WARNING</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a02649358843828935cdb6842c4786cf8a2e5ca065e41a05d4f361e59693b8b012</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_LOG_LEVEL_INFO</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a02649358843828935cdb6842c4786cf8aa25106f13a54ad5cac7e3f8047d2e031</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FPC_LOG_LEVEL_DEBUG</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a02649358843828935cdb6842c4786cf8ad2d790c3a327b4d74dfe7b76e626b5ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_log</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>af62f5a2df26ad9118d4ecfe0e4f95e74</anchor>
      <arglist>(const char *source, uint8_t level, const char *message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_log_var</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>ada35d6414ace89b3bfc03b6239a5310f</anchor>
      <arglist>(const char *source, uint8_t level, const char *format,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_log_mem</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a02d3f57f5c1189fc6127a9da9520f795</anchor>
      <arglist>(const char *source, uint8_t level, const char *message, const uint8_t *data, size_t len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_log_flush</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a4734cb198515843ba77e053a24fa1085</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>fpc_log_level_get</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a519b8eaa6b25c56e188fdf73a0518baa</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_log_level_set</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>ab823ceec83eea8b3190cb76f6fd4c3d6</anchor>
      <arglist>(uint8_t level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_log_init</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>af49f45adc9877c0befef6b3af1b13ccf</anchor>
      <arglist>(uint8_t level)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fpc_log_diag</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a0b1d0fba8956f574a4a8ec29e170ab05</anchor>
      <arglist>(const char *str)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>fpc_log_printf</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a74c002a00ca396ca0ac0112469d429a7</anchor>
      <arglist>(format,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>fpc_log_printfe</name>
      <anchorfile>fpc__log_8h.html</anchorfile>
      <anchor>a6fb7155f1201ef1aa4f31eca33d125cf</anchor>
      <arglist>(format,...)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fpc_log_config.h</name>
    <path>/home/jenkins/jenkins-builds-noandroid/workspace/BEP/SW21/BEP-SW21-PARAM_delivery_package/FPC-BEP-SW-DELIVERY_EMB_FPC1261_fpc_bep_kaadas_21.0.1.002/bep_lib/util_interface/inc/</path>
    <filename>fpc__log__config_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>FPC_LOG_QUEUE_SIZE</name>
      <anchorfile>fpc__log__config_8h.html</anchorfile>
      <anchor>afb365db156da548c8b3d8655ddbb3dec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FPC_LOG_MAX_SOURCE_SIZE</name>
      <anchorfile>fpc__log__config_8h.html</anchorfile>
      <anchor>a93d2fd621e3f76d5ca159a3e193b5c53</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FPC_LOG_MAX_MESSAGE_SIZE</name>
      <anchorfile>fpc__log__config_8h.html</anchorfile>
      <anchor>ac63c6a2dd2a7699037e7e26a6a17ebfe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FPC_LOG_MAX_DATA_SIZE</name>
      <anchorfile>fpc__log__config_8h.html</anchorfile>
      <anchor>a661a0daa340b6cd8795055e3e4435710</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FPC_LOG_OUTPUT_ITM</name>
      <anchorfile>fpc__log__config_8h.html</anchorfile>
      <anchor>a11710e6fe11f2ea58eec7947a40bcbd0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_analyze_cb_result_t</name>
    <filename>structfpc__bep__analyze__cb__result__t.html</filename>
    <member kind="variable">
      <type>uint16_t</type>
      <name>num_of_defect_pixels</name>
      <anchorfile>structfpc__bep__analyze__cb__result__t.html</anchorfile>
      <anchor>a6a29aaef572499bdbbaa59bebe3a0cfd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>num_of_defect_pixels_in_detect_zones</name>
      <anchorfile>structfpc__bep__analyze__cb__result__t.html</anchorfile>
      <anchor>ade0f323dcb9703cf26cd16849a74c1cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>median_type1_min</name>
      <anchorfile>structfpc__bep__analyze__cb__result__t.html</anchorfile>
      <anchor>a3a5466c7b7dcafdc2f02ad2223ad48ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>median_type1_max</name>
      <anchorfile>structfpc__bep__analyze__cb__result__t.html</anchorfile>
      <anchor>adbde36e25ba4d8f8e8627f393dd997dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>median_type2_min</name>
      <anchorfile>structfpc__bep__analyze__cb__result__t.html</anchorfile>
      <anchor>ac155dbf07f58150f9dec0494805c2fea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>median_type2_max</name>
      <anchorfile>structfpc__bep__analyze__cb__result__t.html</anchorfile>
      <anchor>a4b2e92c79dcc8c90da3ab6f590f35641</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_analyze_reset_result_t</name>
    <filename>structfpc__bep__analyze__reset__result__t.html</filename>
    <member kind="variable">
      <type>uint16_t</type>
      <name>num_of_defect_pixels</name>
      <anchorfile>structfpc__bep__analyze__reset__result__t.html</anchorfile>
      <anchor>a026d91e0e9d59df7d5413e6fc062f5fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>median</name>
      <anchorfile>structfpc__bep__analyze__reset__result__t.html</anchorfile>
      <anchor>a7ab753b13c64909fd94a373ccd89e7ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>dummy</name>
      <anchorfile>structfpc__bep__analyze__reset__result__t.html</anchorfile>
      <anchor>a4ffd459e76941a61624187f508757a8e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="union">
    <name>fpc_bep_analyze_result_t</name>
    <filename>unionfpc__bep__analyze__result__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_bio_param_t</name>
    <filename>structfpc__bep__bio__param__t.html</filename>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_contrast_result_t</name>
    <filename>structfpc__bep__contrast__result__t.html</filename>
    <member kind="variable">
      <type>float</type>
      <name>max_std_dev</name>
      <anchorfile>structfpc__bep__contrast__result__t.html</anchorfile>
      <anchor>a68f7d7c358619107a93fc04887624390</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>min_std_dev</name>
      <anchorfile>structfpc__bep__contrast__result__t.html</anchorfile>
      <anchor>a29bfa0828859f15d1166b1d19abc2d53</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>avg_std_dev</name>
      <anchorfile>structfpc__bep__contrast__result__t.html</anchorfile>
      <anchor>a7ad48469a1dc531c426aec561b352a6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>total_subareas</name>
      <anchorfile>structfpc__bep__contrast__result__t.html</anchorfile>
      <anchor>a7bb135578b33ddd27bceabaaadf0261d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_enroll_param_t</name>
    <filename>structfpc__bep__enroll__param__t.html</filename>
    <member kind="variable">
      <type>int32_t</type>
      <name>min_sensor_coverage</name>
      <anchorfile>structfpc__bep__enroll__param__t.html</anchorfile>
      <anchor>a6bc032db468322c94b62b92fc4db30c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int32_t</type>
      <name>min_image_quality</name>
      <anchorfile>structfpc__bep__enroll__param__t.html</anchorfile>
      <anchor>ac5211844ca9e681a124b33ed56d2fe2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int32_t</type>
      <name>nbr_of_images</name>
      <anchorfile>structfpc__bep__enroll__param__t.html</anchorfile>
      <anchor>a84aed4a42d84bf3a34c0217135d7afa9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>fpc_bep_enroll_scheme_t</type>
      <name>scheme</name>
      <anchorfile>structfpc__bep__enroll__param__t.html</anchorfile>
      <anchor>aaccae16088d5779f70c0de14b01920d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int32_t</type>
      <name>max_nbr_of_immobile_touches</name>
      <anchorfile>structfpc__bep__enroll__param__t.html</anchorfile>
      <anchor>a30b739e367567bf6a947401cdb420570</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_enrollment_status_t</name>
    <filename>structfpc__bep__enrollment__status__t.html</filename>
    <member kind="variable">
      <type>uint32_t</type>
      <name>samples_remaining</name>
      <anchorfile>structfpc__bep__enrollment__status__t.html</anchorfile>
      <anchor>a050d8e859beec1257c5409981112039c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>touch_overlap</name>
      <anchorfile>structfpc__bep__enrollment__status__t.html</anchorfile>
      <anchor>a86c117fc69e49bc8f03aecf89dbd98ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>fpc_bep_enrollment_feedback_t</type>
      <name>feedback</name>
      <anchorfile>structfpc__bep__enrollment__status__t.html</anchorfile>
      <anchor>a8f0e83bcd405c43341e0cced2531ebc5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_general_param_t</name>
    <filename>structfpc__bep__general__param__t.html</filename>
    <member kind="variable">
      <type>fpc_bep_latency_scheme_t</type>
      <name>latency_scheme</name>
      <anchorfile>structfpc__bep__general__param__t.html</anchorfile>
      <anchor>aac5e389272ab077f1920c0c9377d4c92</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_identify_param_t</name>
    <filename>structfpc__bep__identify__param__t.html</filename>
    <member kind="variable">
      <type>fpc_bep_config_t</type>
      <name>template_update</name>
      <anchorfile>structfpc__bep__identify__param__t.html</anchorfile>
      <anchor>a3dd7029c1ff7ae44169247d3681cbde6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_identify_result_t</name>
    <filename>structfpc__bep__identify__result__t.html</filename>
    <member kind="variable">
      <type>bool</type>
      <name>match</name>
      <anchorfile>structfpc__bep__identify__result__t.html</anchorfile>
      <anchor>aea99b9d3d7c1a5ad26ee21539b259b3e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>size_t</type>
      <name>index</name>
      <anchorfile>structfpc__bep__identify__result__t.html</anchorfile>
      <anchor>ae6961cfcaf1229ff409320673df89926</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_image_dimensions_t</name>
    <filename>structfpc__bep__image__dimensions__t.html</filename>
    <member kind="variable">
      <type>uint16_t</type>
      <name>width</name>
      <anchorfile>structfpc__bep__image__dimensions__t.html</anchorfile>
      <anchor>a4fa97cb60eec86bb21e07bff578e8433</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>height</name>
      <anchorfile>structfpc__bep__image__dimensions__t.html</anchorfile>
      <anchor>a8c5342ebdc5c5de8e564fb5f536e6d7b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_sensor_param_t</name>
    <filename>structfpc__bep__sensor__param__t.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>nbr_of_finger_present_zones</name>
      <anchorfile>structfpc__bep__sensor__param__t.html</anchorfile>
      <anchor>a48c1fdae6a1d0dd1916732dfbadc6661</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>fpc_bep_sensor_driver_mechanism_t</type>
      <name>driver_mechanism</name>
      <anchorfile>structfpc__bep__sensor__param__t.html</anchorfile>
      <anchor>a2a2cfb834251eb061c4a58738221cf05</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>fpc_bep_sensor_reset_t</type>
      <name>reset</name>
      <anchorfile>structfpc__bep__sensor__param__t.html</anchorfile>
      <anchor>a38d631c84c21da901a3dbbd526bcdc64</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>fpc_bep_sensor_state_after_release_t</type>
      <name>release_state</name>
      <anchorfile>structfpc__bep__sensor__param__t.html</anchorfile>
      <anchor>ac49305025c4fa59a9804c66f78843d85</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>fpc_bep_sensor_prop_t</name>
    <filename>structfpc__bep__sensor__prop__t.html</filename>
    <member kind="variable">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>structfpc__bep__sensor__prop__t.html</anchorfile>
      <anchor>ac99cecd6ed09b4a779c685a30184b846</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>fpc_bep_sensor_type_t</type>
      <name>sensor_type</name>
      <anchorfile>structfpc__bep__sensor__prop__t.html</anchorfile>
      <anchor>ae94c13d34ab7f678e6e991ed1a6f3339</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>width</name>
      <anchorfile>structfpc__bep__sensor__prop__t.html</anchorfile>
      <anchor>a5741102024b51a8e11dbcf0e8776eb43</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>height</name>
      <anchorfile>structfpc__bep__sensor__prop__t.html</anchorfile>
      <anchor>a0dd19cbe97e48d05756886c57265e979</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint16_t</type>
      <name>dpi</name>
      <anchorfile>structfpc__bep__sensor__prop__t.html</anchorfile>
      <anchor>a85ab2fb3bda9f1c60ef5dbec8cce0390</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint32_t</type>
      <name>max_spi_clock</name>
      <anchorfile>structfpc__bep__sensor__prop__t.html</anchorfile>
      <anchor>a338821461c454697d8dca8132589ed9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>num_sub_areas_width</name>
      <anchorfile>structfpc__bep__sensor__prop__t.html</anchorfile>
      <anchor>a41d075bafc066cdd86bfca185cc6ce62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>num_sub_areas_height</name>
      <anchorfile>structfpc__bep__sensor__prop__t.html</anchorfile>
      <anchor>a3330302acf537f05f18aa31847962dcf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>Main BEP</title>
    <filename>index</filename>
  </compound>
</tagfile>
