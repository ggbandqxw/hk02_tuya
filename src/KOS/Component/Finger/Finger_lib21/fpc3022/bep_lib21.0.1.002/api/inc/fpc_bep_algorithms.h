/*
 * Copyright (c) 2018-2021 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_BEP_ALGORITHMS_H
#define FPC_BEP_ALGORITHMS_H

/**
 * @file    fpc_bep_algorithms.h
 * @brief   Biometric Embedded Platform algorithms.
 *
 * Algorithm configurations for supported algorithm variants and sensor combinations.
 * The algorithm configurations are optimized for a specific sensor and declared
 * according following pattern:
 * fpc_bep_algorithm_<algorithm type>_<sensor type>
 *
 * The selected algorithm configuration needs to be specified during initialization of
 * the biometric functions in BEP library.
 *
 */

#include "fpc_bep_algorithm.h"





/** FPC PFE algorithm, FPC1261-S configuration */
extern const fpc_bep_algorithm_t fpc_bep_algorithm_pfe_1261;

#endif /* FPC_BEP_ALGORITHMS_H */
