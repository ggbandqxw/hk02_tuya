/*
 * @Author: wang 297161893@qq.com
 * @Date: 2022-01-21 13:25:31
 * @LastEditors: wang 297161893@qq.com
 * @LastEditTime: 2022-06-29 14:47:19
 * @FilePath: \5011\KOS\Component\Finger\Finger_lib21\fpc3022\fpc_sensor_spi.c
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
#include "fpc_sensor_spi.h"
#include "fpc_bep_types.h"
#include "fpc_timebase.h"
#include "device.h"

/* 定义虚拟硬件接口 */
#define SPI_VHW_FINGER                  vSPI_1
#define CS_VHW_FINGER                   vPIN_C13
#define RST_VHW_FINGER                  vPIN_C14
#define IRQ_VHW_FINGER                  vPIN_I5

fpc_bep_result_t fpc_sensor_spi_write_read(uint8_t *data, size_t write_size, size_t read_size,
        bool leave_cs_asserted)
{
    fpc_bep_result_t status = FPC_BEP_RESULT_OK;
    Device_Write(CS_VHW_FINGER, NULL, 0, 0);//低电平
	Device_Write(SPI_VHW_FINGER, data, write_size + read_size, (uint32_t)data);
	
    if (!leave_cs_asserted) 
	{
        Device_Write(CS_VHW_FINGER, NULL, 0, 1);//高电平
	}
    return status;   	
}

bool fpc_sensor_spi_check_irq(void)
{
    return (bool)Device_Read(IRQ_VHW_FINGER, NULL, 0, 0);
}

bool fpc_sensor_spi_read_irq(void)
{
    bool active = false;
    int32_t ret = Device_Read(IRQ_VHW_FINGER, NULL, 0, 0);
    if (ret)
    {
        active = true;
    }
    return active;
}

void fpc_sensor_spi_reset(bool state)
{
    if (state)
    {
        Device_Write(RST_VHW_FINGER, NULL, 0, 1);//高电平
    }
    else
    {
        Device_Write(RST_VHW_FINGER, NULL, 0, 0);//低电平 
    }
    Device_DelayMs(5);
}

void fpc_sensor_spi_init(uint32_t speed_hz)
{
    if (speed_hz == 0)
        return;
}

fpc_bep_result_t fpc_sensor_wfi(uint16_t timeout_ms, fpc_bep_wfi_check_t enter_wfi,
        bool enter_wfi_mode)
{
    return FPC_BEP_RESULT_OK;
}

fpc_bep_spi_duplex_mode_t fpc_sensor_spi_get_duplex_mode(void)
{
    return FPC_BEP_SPI_FULL_DUPLEX;    
}
