/*
 * Copyright (c) 2018 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_BEP_BIO_PARAM_H
#define FPC_BEP_BIO_PARAM_H

/**
 * @file    fpc_bep_bio_param.h
 * @brief   Biometric API runtime parameters.
 *
 * Definitions of the runtime configuration parameters used in the biometric API of
 * Biometric Embedded Platform (BEP) library.
 * The library contains functionality for use with biometric hardware from
 * Fingerprint Cards. It is targeting embedded systems with tight
 * restrictions on available CPU, memory and storage resources.
 *
 * The library is by definition executing in the same security domain as the
 * caller, therefore the API does not define any security mechanisms and it is
 * the responsibility of the caller to securely deliver and protect any
 * sensitive data being delivered to other parts of the system.
 *
 * @note This is a work-in-progress specification. Implementers are informed
 * that this API may change without providing any backward compatibility.
 * However it is FPC's ambition that the API shall remain compatible between
 * releases.
 */

#include <stddef.h>
#include <stdint.h>

#include "fpc_bep_types.h"

/**
 * @brief Fingerprint algorithm enroll scheme.
 */
typedef enum {
    /** Algorithm enroll scheme only depending on number of touches. */
    FPC_BEP_ENROLL_SCHEME_TOUCH = 0,
    /**
     *  Algorithm enroll scheme for touch with overlap requirement.
     *  A new enrollment image is compared with previous images
     *  from same enrollment session, to secure it is the same finger
     *  that is always enrolled. */
     FPC_BEP_ENROLL_SCHEME_STRICT = 1,
    /**
    * Algorithm enroll scheme with flexibility where the enrollment can be finished
    * prior to the enroll progress reaching 100%.
    * This scheme can be used if a customized stop-criteria is preferred. */
    FPC_BEP_ENROLL_SCHEME_CUSTOM = 2,
} fpc_bep_enroll_scheme_t;

/**
 * @brief Fingerprint algorithm latency scheme.
 */
typedef enum {
    /** Algorithm scheme for optimized biometric performance. */
    FPC_BEP_LATENCY_SCHEME_STANDARD = 1
} fpc_bep_latency_scheme_t;

/**
 * @brief BEP library general configuration parameters.
 */
typedef struct {
    /** Algorithm latency scheme */
    fpc_bep_latency_scheme_t latency_scheme;
} fpc_bep_general_param_t;

/**
 * @brief BEP library enroll configuration parameters.
 */
typedef struct {
    /**
     * Minimum sensor coverage required for an image to be accepted during enrollment.
     * The coverage is specified in % of total sensor area.
     */
    int32_t min_sensor_coverage;
    /**
     * Minimum image quality required for an image to be accepted during enrollment.
     * The image quality is specified as a value in the range between 0 and 100,
     * where a higher value corresponds to a better image quality.
     */
    int32_t min_image_quality;
    /** Number of accepted enroll samples, after which enrollment is completed. */
    int32_t nbr_of_images;
    /** Algorithm enroll scheme. */
    fpc_bep_enroll_scheme_t scheme;
    /**
     * The maximum number of immobile touches that are taken into account
     * for calculating the remaining touches. Once this limit is reached, subsequent
     * remaining touches are decreased even though they might be immobile.
     * This number shall not be larger than nbr_of_images. */
    int32_t max_nbr_of_immobile_touches;

} fpc_bep_enroll_param_t;

/**
 * @brief Fingerprint identification security levels.
 */
typedef enum {
    FPC_BEP_SECURITY_LEVEL_FAR_20K  = 1,
    FPC_BEP_SECURITY_LEVEL_FAR_50K  = 2,
    FPC_BEP_SECURITY_LEVEL_FAR_100K = 3,
    FPC_BEP_SECURITY_LEVEL_FAR_500K = 4
} fpc_bep_security_level_t;

/**
 * @brief BEP library identify configuration parameters.
 */
typedef struct {
    fpc_bep_security_level_t security_level;
    /** Template update configuration. */
    fpc_bep_config_t template_update;
} fpc_bep_identify_param_t;

/**
 * @brief BEP library biometric runtime configuration parameters.
 */
typedef struct {
    fpc_bep_general_param_t general;
    fpc_bep_enroll_param_t enroll;
    fpc_bep_identify_param_t identify;
} fpc_bep_bio_param_t;

#endif /* FPC_BEP_BIO_PARAM_H */
