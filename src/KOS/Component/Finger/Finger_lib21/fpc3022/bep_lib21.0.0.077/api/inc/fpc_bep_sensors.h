/*
 * Copyright (c) 2018-2021 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_BEP_SENSORS_H
#define FPC_BEP_SENSORS_H

#include "fpc_bep_types.h"

/**
 * @brief BEP library supported sensors.
 */

/** FPC 1020 sensor */
extern const fpc_bep_sensor_t fpc_bep_sensor_1020;
/** FPC 1021 sensor */
extern const fpc_bep_sensor_t fpc_bep_sensor_1021;
/** FPC 1024 sensor */
extern const fpc_bep_sensor_t fpc_bep_sensor_1024;
/** FPC 1025 sensor */
extern const fpc_bep_sensor_t fpc_bep_sensor_1025;
/** FPC 1035 sensor */
extern const fpc_bep_sensor_t fpc_bep_sensor_1035;
/** FPC 1260 sensor */
extern const fpc_bep_sensor_t fpc_bep_sensor_1260;
/** FPC 1261 sensor */
extern const fpc_bep_sensor_t fpc_bep_sensor_1261;

#endif /* FPC_BEP_SENSORS_H */
