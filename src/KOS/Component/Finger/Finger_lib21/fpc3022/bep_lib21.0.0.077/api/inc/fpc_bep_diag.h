/*
 * Copyright (c) 2017 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_BEP_DIAG_H
#define FPC_BEP_DIAG_H

/**
 * @file    fpc_bep_diag.h
 * @brief   FPC BEP Diagnostics functionality.
 *
 * Note! These functions are only available in the diagnostics lib (libbep_diag.a).
 */

#include <stdint.h>

#include "fpc_bep_types.h"

/**
 * @brief Enable/Disable Trace prints
 *
 * @param enable enable or disable
 */
void fpc_bep_diag_trace_enable(bool enable);

/**
 * @brief Enable/Disable Diagnostics functionality
 *
 * @param enable enable or disable
 */
void fpc_bep_diag_enable(bool enable);

/**
 * @brief Enable/Disable Diagnostics register dump subscription
 *
 * @param enable enable or disable
 */
fpc_bep_result_t fpc_bep_diag_reg_dump_enable(bool enable);

/**
 * @brief Dump diagnostics information
 *
 * Diagnostics are printed out using the fpc_log_diag interface.
 */
void fpc_bep_diag_dump(void);


#endif /* FPC_BEP_DIAG_H */
