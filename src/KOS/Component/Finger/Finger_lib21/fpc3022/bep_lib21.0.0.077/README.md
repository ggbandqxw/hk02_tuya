# Main BEP {#mainpage}

Welcome to the documentation for the Biometric Embedded Platform (BEP).

The following sections will introduce you to the main concepts of BEP.

## Components

[Fingerprint sensor API](@ref fpc_bep_sensor.h)

- [Fingerprint supported sensors](@ref fpc_bep_sensors.h)

[Fingerprint sensor calibration API](@ref fpc_bep_calibration.h)

[Fingerprint image API](@ref fpc_bep_image.h)

[Biometric API](@ref fpc_bep_bio.h)

- [Biometric configuration options](@ref fpc_bep_bio_param.h)

[Algorithm configuration API](@ref fpc_bep_algorithm.h)

- [Sensor algorithm configuration](@ref fpc_bep_algorithms.h)

[Common types](@ref fpc_bep_types.h)

[Sensor production test API](@ref fpc_bep_sensor_test.h)

[Diagnostic API](@ref fpc_bep_diag.h)

[BEP version API](@ref fpc_bep_version.h)

## Interfaces

### Required interfaces

[Timebase](@ref fpc_timebase.h)

[Sensor SPI](@ref fpc_sensor_spi.h)

[Malloc](@ref fpc_malloc.h)

### Utility Interfaces

[Assert](@ref fpc_assert.h)

[Logging](@ref fpc_log.h)

- [logging configuration](@ref fpc_log_config.h)
