/*
 * Copyright (c) 2016 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_TIMEBASE_H
#define FPC_TIMEBASE_H

/**
 * @file    fpc_timebase.h
 * @brief   Timebase based on a system tick.
 *
 * Supplies tick counter and wait operation(s).
 */

#include <stdint.h>

/**
 * @brief Reads the system tick counter.
 *
 * @return Tick count since fpc_timebase_init() call. [ms]
 */
uint32_t fpc_timebase_get_tick(void);


/**
 * @brief Delay in milliseconds.
 *
 * @param[in] ms  Time to wait [ms].
 * 0 => return immediately
 * 1 => wait at least 1ms etc.
 */
void fpc_timebase_delay_ms(uint32_t ms);


/**
 * @brief Delay in microseconds.
 *
 * This function is mainly used to minimize the time spent waiting for the sensor
 * to complete certain operations when running in polling mode.
 * The time accuracy for this function is up to the user to determine depending on
 * latency needs, but it should be at least 100us.
 * If not implemented elsewhere a weak implementation of this function is available
 * in the BEP library that will fall back to using fpc_timebase_delay_ms.
 *
 * @param[in] us  Time to wait [us].
 * 0 => return immediately
 * 1 => wait at least 1us etc.
 */
void fpc_timebase_delay_us(uint32_t us);

/**
 * @brief Initializes timebase. Starts system tick counter.
 */
void fpc_timebase_init(void);

#endif /* FPC_TIMEBASE_H */
