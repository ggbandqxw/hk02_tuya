#include "fpc_timebase.h"
#include "osal.h"
#include "device.h"

uint32_t fpc_timebase_get_tick(void)
{
    uint32_t tmp = OSAL_GetTickCount();
    return tmp;
}

void fpc_timebase_init(void)
{  
}

void fpc_timebase_busy_wait(uint32_t ms)
{
    Device_DelayMs(ms);
}

void fpc_timebase_delay_ms(uint32_t ms)
{
    Device_DelayMs(ms);
}
