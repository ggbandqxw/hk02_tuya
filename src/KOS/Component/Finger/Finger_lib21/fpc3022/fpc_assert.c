#include "fpc_assert.h"
#include "osal.h"
#include "device.h"

#define UNUSED(x) (void)(x)


#define ASSERT_LOG(format, ...)     printf(""format"\r\n",##__VA_ARGS__)


void fpc_assert_fail(const char *file,
                     uint32_t    line,
                     const char *func,
                     const char *expr)
{
    ASSERT_LOG("Assert error, File: %s  Line: %05d  Func: %s  Expr: %s \r\n",file,line,func,expr);
    Device_EnterCritical();
    while(1);
}
