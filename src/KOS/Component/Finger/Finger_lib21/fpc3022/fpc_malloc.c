#include "fpc_malloc.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "stdbool.h"
#include "osal.h"

//void *fpc_malloc(size_t size)
//{
//	return OSAL_Malloc(size);
//}

//void fpc_free(void *data)
//{
//	OSAL_Free(data);
//}


void *malloc(size_t size)
{
	return OSAL_Malloc(size);
}

void free(void *data)
{
	OSAL_Free(data);
}
