/*
 * Copyright (c) 2018 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

/**
 * @file    fpc_bep_algorithm.h
 * @brief   Biometric algorithm API.
 */
#ifndef FPC_BEP_ALGORITHM_H
#define FPC_BEP_ALGORITHM_H

#include <stdlib.h>

#include "fpc_bep_types.h"

/**
 * @brief Biometric algorithm configuration.
 */
typedef struct fpc_bep_algorithm fpc_bep_algorithm_t;

/**
 * @brief Gets algorithm information.
 *
 * This function could be called before the BEP library is initialized.
 *
 * @param[in] algorithm Algorithm configuration.
 *
 * @return ::char*  String with algorithm information.
 */
const char *fpc_bep_algorithm_get_info(const fpc_bep_algorithm_t *algorithm);

/**
 * @brief Determines the maximum template size for specified algorithm configuration.
 *
 * This function could be called before the BEP library is initialized.
 *
 * @param[in] algorithm Algorithm configuration.
 * @param[out] size The maximum template size.
 *
 * @return ::fpc_bep_result_t
 */
fpc_bep_result_t fpc_bep_algorithm_get_max_template_size(const fpc_bep_algorithm_t *algorithm,
        size_t *size);

/**
 * @brief Gets sensor type for the specified algorithm configuration.
 *
 * This function could be called before the BEP library is initialized.
 *
 * @param[in] algorithm Algorithm configuration.
 *
 * @return ::fpc_bep_sensor_type_t Sensor type, FPC_BEP_SENSOR_TYPE_UNDEFINED returned on error.
 */
fpc_bep_sensor_type_t fpc_bep_algorithm_get_sensor_type(const fpc_bep_algorithm_t *algorithm);

#endif /* FPC_BEP_ALGORITHM_H */
