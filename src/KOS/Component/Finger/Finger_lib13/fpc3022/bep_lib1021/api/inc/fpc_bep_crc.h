/*
 * Copyright (c) 2016 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_BEP_CRC_H
#define FPC_BEP_CRC_H

/**
 * @file    fpc_bep_crc.h
 * @brief   Functionality for calculating CRC-32.
 */

#include <stdint.h>

/**
 * @brief Calculates CRC-32 value for the data in the buffer.
 *
 * @param crc Accumulated CRC-32 value, must be 0 on first call.
 * @param buf Buffer with data to calculate CRC-32 for.
 * @param size Size of buffer in number of bytes.
 * @return CRC-32 value for the data in buffer.
 */
uint32_t fpc_bep_crc(uint32_t crc, const void *buf, uint32_t size);

#endif /* FPC_BEP_CRC_H */
