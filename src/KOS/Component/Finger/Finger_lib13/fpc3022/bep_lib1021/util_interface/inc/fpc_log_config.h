/*
 * Copyright (c) 2016 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_LOG_CONFIG_H
#define FPC_LOG_CONFIG_H

/**
 * @file       fpc_log_config.h
 * @brief      Compile time configuration for BEP lib and BEP reference platform.
 *
 * This file contains compile time configuration "#define" for the FPC debug log component.
 */


/**
 * FPC_LOG_INCLUDE 1 includes the complete FPC debug log component.
 * FPC_LOG_INCLUDE 0 includes the FPC debug log interface only - every call to
 * the component will be empty.
 */
#ifndef FPC_LOG_INCLUDE
#define FPC_LOG_INCLUDE  0
#endif

/**
 * Capacity of log queue in number of log records. The log queue is used awaiting
 * write of log data to external interface (e.g. UART).
 * Log at full queue will cause loss of a log record (sequence number skip in output).
 */
#ifndef FPC_LOG_QUEUE_SIZE
#define FPC_LOG_QUEUE_SIZE  4
#endif

/**
 * Maximum number of characters in log source field. Including terminating null.
 * Value must be >= 1.
 */
#ifndef FPC_LOG_MAX_SOURCE_SIZE
#define FPC_LOG_MAX_SOURCE_SIZE  20
#endif

/**
 * Maximum number of characters in log message field. After formatting, including terminating null.
 * Static messages (from log functions logging static messages) are not limited by this parameter.
 * Value must be >= 1.
 */
#ifndef FPC_LOG_MAX_MESSAGE_SIZE
#define FPC_LOG_MAX_MESSAGE_SIZE  80
#endif

/**
 * Maximum number of bytes in log data field. For log of memory data.
 */
#ifndef FPC_LOG_MAX_DATA_SIZE
#define FPC_LOG_MAX_DATA_SIZE  16
#endif

/**
 * FPC_LOG_OUTPUT_ITM 0 directs log data output to UART.
 * FPC_LOG_OUTPUT_ITM 1 directs log data output to ITM.
 * FPC_LOG_OUTPUT_ITM 2 directs log data to buffer, need to manualy be fetched
 */
#ifndef FPC_LOG_OUTPUT_ITM
#define FPC_LOG_OUTPUT_ITM  0
#endif

#endif /* FPC_LOG_CONFIG_H */
