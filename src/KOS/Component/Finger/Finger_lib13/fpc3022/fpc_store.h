#ifndef _FPC_STORE_H
#define _FPC_STORE_H
#include "fpc_bep_algorithm.h"  //生物识别算法API
#include "fpc_bep_algorithms.h" //嵌入式生物识别算法API
#include "fpc_bep_bio.h"        //生物识别API
#include "fpc_bep_bio_param.h"  //生物识别API运行时参数
#include "fpc_bep_calibration.h"//嵌入式平台传感器校准
#include "fpc_bep_crc.h"        //计算CRC-32的功能
#include "fpc_bep_diag.h"       //FPC BEP诊断功能
#include "fpc_bep_image.h"      //指纹图像API   
#include "fpc_bep_sensor.h"     //指纹传感器API
#include "fpc_bep_sensor_test.h"//BEP传感器生产测试功能
#include "fpc_bep_types.h"      //类型声明
#include "fpc_bep_version.h"    //版本信息

#include "fpc_timebase.h"       //时基基于系统滴答
#include "fpc_sensor_spi.h"     //SPI主驱动程序

#include "fpc_assert.h"         //断言
#include "fpc_log.h"            //调试嵌入式应用程序的文本和数据日志
#include "fpc_log_config.h"

#define FPC_MAX_NUM                     (100 + 1)

#ifdef RUN_COUNTER_HW
    #define GET_RUN_COUNTER()               Cy_TCPWM_Counter_GetCounter(RUN_COUNTER_HW, RUN_COUNTER_NUM)    //运行计数器毫秒
    #define SET_RUN_COUNTER(ms)             Cy_TCPWM_Counter_SetCounter(RUN_COUNTER_HW, RUN_COUNTER_NUM,ms)    //运行计数器毫秒
    #define RUN_COUNTER_START()             RUN_COUNTER_Start()
#else
    #define GET_RUN_COUNTER()               0
    #define SET_RUN_COUNTER(...)
    #define RUN_COUNTER_START()
#endif

/* 排序表 */
typedef struct {
    uint32_t time_stamp;    //时间戳
    uint16_t fpc_num;       //指纹编号
    uint16_t index_id;      //索引编号
}SortList_t;

typedef struct {
    SortList_t list[FPC_MAX_NUM];
    uint16_t size;
}VerifList_t;

bool fpc_store_init(size_t max_len,VerifList_t *verif_list);

bool fpc_save_temp(const fpc_bep_template_t **temp,uint16_t fpc_num,bool save_type);

bool fpc_del_num(uint16_t start_num,uint16_t nums);

bool fpc_store_empty(void);

bool fpc_read_temp(fpc_bep_template_t **temp,uint16_t index_id);

uint16_t fpc_read_verify_num(VerifList_t *verif_list);

#endif
