/*
 * Copyright (c) 2016 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_BEP_VERSION_H
#define FPC_BEP_VERSION_H

/**
 * @file    fpc_bep_version.h
 * @brief   Biometric Embedded Platform version functionality.
 *
 * This is the version API of the Biometric Embedded Platform (BEP) library.
 * @note    This is a work-in-progress specification. Implementers are informed
 * that this API may change without providing any backward compatibility.
 * However it is FPC's ambition that the API shall remain compatible between
 * releases.
 */

/**
 * @brief Retrieve the BEP lib version string
 *
 * @return The version string.
 */
const char *fpc_bep_get_version(void);

#endif /* FPC_BEP_VERSION_H */
