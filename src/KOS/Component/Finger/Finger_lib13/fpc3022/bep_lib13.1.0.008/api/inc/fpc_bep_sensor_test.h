/*
 * Copyright (c) 2016 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_BEP_SENSOR_TEST_H
#define FPC_BEP_SENSOR_TEST_H

/**
 * @file    fpc_bep_sensor_test.h
 * @brief   BEP sensor production test functions.
 *
 * Check for sensor damage during production test.
 *
 * @note Before using this API the sensor must be initialized, see
 * fpc_bep_sensor_init().
 *
 * @note This is a work-in-progress specification. Implementers are informed
 * that this API may change without providing any backward compatibility.
 * However it is FPC's ambition that the API shall remain compatible between
 * releases.
 */

#include "fpc_bep_types.h"

/** @brief Result returned by BEP sensor test functions. */
typedef enum {
    /** No errors occurred. */
    FPC_BEP_SENSOR_TEST_OK = 0,
    /** Out of memory. */
    FPC_BEP_SENSOR_TEST_NO_MEMORY = -1,
    /** Number of dead pixels is over limit */
    FPC_BEP_SENSOR_TEST_TOO_MANY_DEAD_PIXELS = -2,
    /** Failed to capture image */
    FPC_BEP_SENSOR_TEST_CAPTURE_FAILED = -3,
    /** Failed to communicate with sensor */
    FPC_BEP_SENSOR_TEST_COMMUNICATION_ERROR = -4,
    /** Sensor hasn't been initialized. */
    FPC_BEP_SENSOR_TEST_NOT_INITIALIZED = -5,
    /** The functionality is not supported. */
    FPC_BEP_SENSOR_TEST_NOT_SUPPORTED = -6,
} fpc_bep_sensor_test_result_t;

/**
 * @brief Test image size.
 */
typedef enum {
    /** Capture test image using all sensor pixels. */
    FPC_BEP_TEST_IMAGE_LARGE  = 0,
    /** Capture test image using 1/4 of all the sensor pixels */
    FPC_BEP_TEST_IMAGE_MEDIUM = 1,
    /** Capture test image using 1/16 of all the sensor pixels */
    FPC_BEP_TEST_IMAGE_SMALL  = 2,
} fpc_bep_test_image_size_t;

/**
 * @brief Capture test modes.
 */
typedef enum {
    /** Capture test image using reset test mode. */
    FPC_BEP_CAPTURE_TEST_MODE_RESET = 0,
    /** Capture test image using checkerboard test mode. */
    FPC_BEP_CAPTURE_TEST_MODE_CHECKERBOARD = 1,
    /** Capture test image using checkerboard inverted test mode. */
    FPC_BEP_CAPTURE_TEST_MODE_CHECKERBOARD_INVERTED = 2,
} fpc_bep_capture_test_mode_t;

/**
 * @brief Sensor production test
 *
 * The test checks that the module meets the requirements.
 * The following tests are performed:
 *   - Checkerboard test
 *   - Reset pixel test
 *
 * To find any dead pixels, checkerboard and reset pixels images are captured and processed by BEP.
 * If too many dead pixels are found the test will fail. The number of dead pixels available as a
 * log output only while using libbep_diag.a
 *
 * @note    The function requires that the sensor is powered with 1.8 volt.
 *
 * @return ::fpc_bep_sensor_test_result_t
 */
fpc_bep_sensor_test_result_t fpc_bep_sensor_prod_test(void);

/**
 * @brief Captures an image during production test
 *
 * This function will put the sensor in idle mode and then capture an image regardless of the
 * sensor coverage.
 *
 * This is mainly used for testing connectivity of the bezel and is only supported for older
 * sensors without companion chip.
 *
 * @param[in] image_size Image capture size. The smaller size, the shorter time for image capture.
 * @return ::fpc_bep_sensor_test_result_t
 */
fpc_bep_sensor_test_result_t fpc_bep_sensor_prod_test_capture(fpc_bep_test_image_size_t image_size);

/**
 * @brief Captures an image using one of the capture test modes
 *
 * This function will put the sensor in idle mode and then capture an image using the selected
 * mode ::fpc_bep_capture_test_mode_t.
 *
 * This can be useful to evaluate the images outside the lib.
 *
 * @param[in,out] image The captured image.
 * @param[in] mode Image capture test mode.
 * @return ::fpc_bep_sensor_test_result_t
 */
fpc_bep_sensor_test_result_t fpc_bep_sensor_prod_test_mode_capture(fpc_bep_image_t *image,
    fpc_bep_capture_test_mode_t mode);

#endif /* FPC_BEP_SENSOR_TEST_H */
