/*
 * Copyright (c) 2016 Fingerprint Cards AB <tech@fingerprints.com>
 *
 * All rights are reserved.
 * Proprietary and confidential.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Any use is subject to an appropriate license granted by Fingerprint Cards AB.
 */

#ifndef FPC_ASSERT_H
#define FPC_ASSERT_H

/**
 * @file   fpc_assert.h
 * @brief  Header for FPC variant of assert.
 */

#include <stdint.h>


/**
 * @brief Assert macro.
 *
 * Will do nothing when assertion is fulfilled.
 * When assertion fails, the macro will extract location info and call the fpc_assert_fail()
 * function.
 *
 * @param[in] _e Expression to assert on.
 */
#ifdef NDEBUG // Mimic ANSI standard for assert.
#define fpc_assert(_e) ((void)0)
#else
#define fpc_assert(_e) ((_e) ? fpc_assert_fail(__FILE__, __LINE__, __func__, #_e) : (void)0)
#endif


/**
 * @brief Function called by fpc_assert() macro if assertion fails.
 *
 * To be accessed only via fpc_assert() macro.
 *
 * Function measures are implementation dependent.
 * Candidate measures may include log of info to UART and program restart.
 *
 * Note. The noreturn attribute is needed to use assert with scan-build.
 *
 * @param[in] file  Source file from which assert macro is called.
 * @param[in] line  Source line from which assert macro is called.
 * @param[in] func  Source function from which assert macro is called.
 * @param[in] expr  Assert expression in string form.
 */
void fpc_assert_fail(const char *file,
                     uint32_t    line,
                     const char *func,
                     const char *expr) __attribute__((__noreturn__));

#endif /* FPC_ASSERT_H */
