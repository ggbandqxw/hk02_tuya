#ifndef __FINGER_H__
#define __FINGER_H__
#include "component.h"

/* 指纹动作类型 */
typedef enum 
{
	FINGER_ACTION_GET_IMAGE_OK,			//获取图像成功
	FINGER_ACTION_GET_IMAGE_ERR,		//获取图像失败
	FINGER_ACTION_VERIFY_OK,			//校验成功
	FINGER_ACTION_VERIFY_ERR,			//校验失败
	FINGER_ACTION_ADD_ERR,				//添加失败
	FINGER_ACTION_ADD_OK,				//添加成功
	FINGER_ACTION_ENTER_AGAIN,			//再次输入
}FingerAction_enum_t;

/* Finger组件消息类型 */
typedef struct 
{
    FingerAction_enum_t action;			//指纹动作
	uint8_t fingerNumber;				//指纹编号
    uint8_t finger_add_cnt;		        //指纹录入次数
}FingerMsg_t;

/* TASK工作模式*/
typedef enum
{
	FINGER_WORK_MODE_VERIFY, //验证模式
	FINGER_WORK_MODE_ADD,    //录入模式
	FINGER_WORK_MODE_DISABLE //禁用模式
} Finger_WorkMode_enum_t;



typedef struct
{
	uint8_t msg_type;
	uint8_t msg_data[2];
} FingerMsgBuff_t;
///* FINGER任务函数 */
//static ErrorStatus Finger_WorkModeSet(Finger_WorkMode_enum_t mode, uint8_t fpc_num);

/* 删除指纹图像 */
// static ErrorStatus Finger_DeleteImage(uint8_t startNum, uint8_t endNum);


/* 指纹组件邮箱主题（APP->COMP） */
#define FINGER_SETWORKMODE          0
#define FINGER_DELETEIMAGE       	1

#define UART_FINGER_LED_CONTROL     2

/* UART FINGER LED*/
/**********************LED模式*******************************/
#define FINGER_BREATHING_LED__MODE      0x1   // 普通呼吸灯
#define FINGER_FLASHING_LED_MODE        0x2   // 闪烁灯
#define FINGER_LED_ON_MODE              0x3   // 常开灯
#define FINGER_LED_OFF_MODE             0x4   // 常关灯
#define FINGER_LED_GRADUALLY_ON_MODE    0x5   // 渐开灯
#define FINGER_LED_GRADUALLY_OFF_MODE   0x6   // 渐关灯
#define FINGER_LED_SEVEN_COLORS_MODE    0x7   // 七彩编程呼吸灯


/**********************起始/结束LED选择*******************************/
#define FINGER_LED_BLUE         (1 << 0)  // 蓝灯标志位
#define FINGER_LED_GREEN        (1 << 1)  // 绿灯标志位
#define FINGER_LED_RED          (1 << 2)  // 红灯标志位
#define FINGER_LED_ALL_OFF      (0)       // 所有灯灭
/********************串口定义**************************/

/* 设置FINGER工作模式 */
#define Finger_WorkModeSet(mode, fpc_num)                  \
({                                                         \
    uint8_t temp[3] = {FINGER_SETWORKMODE, mode, fpc_num}; \
                                                           \
    OSAL_MboxPost(COMP_FINGER, 0, temp, sizeof(temp));     \
})

/* 删除指纹图像 */
#define Finger_DeleteImage(startNum, endNum)                  \
({                                                            \
    uint8_t temp[3] = {FINGER_DELETEIMAGE, startNum, endNum}; \
                                                              \
    OSAL_MboxPost(COMP_FINGER, 0, temp, sizeof(temp));        \
})

#ifdef FINGER_MODULE
/* 获取指纹模组版本号 */
#define Finger_GetVersion(str)                          \
    ({                                                  \
        OSAL_NvReadGlobal(COMP_FINGER, 0, 0, str, 18);  \
    })
#endif // #ifdef FINGER_MODULE


/* 设置串口指纹LED */
#define Finger_LedControl(mode, ledStart, ledEnd, times)                  \
({                                                            \
    uint8_t temp[5] = {UART_FINGER_LED_CONTROL, mode, ledStart, ledEnd, times}; \
                                                              \
    OSAL_MboxPost(COMP_FINGER, 0, temp, sizeof(temp));        \
})


#endif /* __FINGER_H__ */
