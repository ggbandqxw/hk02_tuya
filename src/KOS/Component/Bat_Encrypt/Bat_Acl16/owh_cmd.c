/***********************************************************************
 * Copyright (c)  2008 - 2021, dxtc Co.,Ltd .
 * All rights reserved.
 * Filename    : owh_cmd.c
 * Description : owh command demo
 * Author(s)   : liujia
 * version     : V1.0
 * Modify date : 2023-02-14
 ***********************************************************************/
#include "owh_cmd.h"

#include "sha_sdg.h"
#include "aes_sdg.h"
#include "uecc_sdg.h"

/* 调试打印接口 */
#define ACL16_LOG(format, ...)    		printf(format, ##__VA_ARGS__)

uint8_t u8_v_romid[8] = {0};
uint8_t u8_v_manid[2] = {0};
uint8_t v_DEV_PubKey[64] = {0};
uint8_t v_DEV_PubKey_Sign[64] = {0};
uint8_t v_memory_data_index[32] = {0};

/* 指纹组件状态 */
static ACL6Status_enum_t acl16_status = ACL16_INIT;

static void reverse_memory(uint8_t *buff, uint32_t length)
{
	uint8_t temp;
	uint8_t *buff_start = buff;
	uint8_t *buff_end = buff + length -1;

	while(buff_end > buff_start)
	{
		temp = *buff_start;
		*buff_start++ = *buff_end;
		*buff_end-- = temp;
	}
}
//crc验证的数组
static const uint8_t w1_crc8_table[] = {
	0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
	157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
	35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
	190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
	70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
	219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
	101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
	248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
	140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
	17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
	175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
	50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
	202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
	87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
	233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
	116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
};

static uint8_t w1_calc_crc8(uint8_t * data, int len)
{
	uint8_t crc = 0;

	while (len--)
		crc = w1_crc8_table[crc ^ *data++];

	return crc;
}

uint8_t Cmd_Owh_ChkCrc_RomdID(uint8_t *rom_id)
{
	uint8_t id[8];
	
	memcpy(id, rom_id, 8);
	reverse_memory(id, 8);
	
	if(w1_calc_crc8(id, 7) != id[7])
	{
		return 1;
	}
	return 0;
}

void OWH_Chip_PowerOn(void)
{
	Device_Enable(VHW_IIC_ACL16);
	owh_systick_delay_ms(10);
}

void OWH_Chip_PowerOff(void)
{
	Device_Disable(VHW_IIC_ACL16);
}

//Read 64bit ROM_ID & 16bit MANU_ID
//8 字节ROMMID+2 字节MANUID
uint8_t Cmd_Owh_Get_PCM_ID(uint8_t *pcm_id)
{
	OWH_COMMAND	 owh_cmd;
	OWH_RESPONSE owh_resp;
	uint8_t ret;

	memset(pcm_id, 0x00, 8+2);
	// ret = OWH_ResetDevice(CMD_OWH_SKIPROM, 0);
	// if(ret != 0x00)
	// {
	// 	return ret;
	// }

	owh_cmd.command = 0xB2;
	owh_cmd.para_len = 0;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 5);
	if(ret != 0x00)
	{
		return ret;
	}

	memcpy(pcm_id, owh_resp.resp_data, 8+2);

	return 0;
}

//Set 17bit Dec-Counter
uint8_t Cmd_Owh_Set_Counter_Value(void)
{
	OWH_COMMAND	 owh_cmd;
	OWH_RESPONSE owh_resp;
	uint8_t ret;

	// ret = OWH_ResetDevice(CMD_OWH_SKIPROM, 0);
	// if(ret != 0x00)
	// {
	// 	return ret;
	// }

	owh_cmd.command = 0x24;
	owh_cmd.para_len = 0;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 10);
	if(ret != 0x00)
	{
		return ret;
	}

	return 0;
}

//Read 17bit Dec-Counter
uint8_t Cmd_Owh_Get_Counter_Value(uint32_t *u32_counter_value)
{
	OWH_COMMAND	 owh_cmd;
	OWH_RESPONSE owh_resp;
	uint8_t ret;

	*u32_counter_value = 0;
	// ret = OWH_ResetDevice(CMD_OWH_SKIPROM, 0);
	// if(ret != 0x00)
	// {
	// 	return ret;
	// }

	owh_cmd.command = 0xC0;
	owh_cmd.para_len = 0;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 5);
	if(ret != 0x00)
	{
		return ret;
	}

	*u32_counter_value = owh_resp.resp_data[0];
	*u32_counter_value <<= 8;
	*u32_counter_value += owh_resp.resp_data[1];
	*u32_counter_value <<= 8;
	*u32_counter_value += owh_resp.resp_data[2];
	*u32_counter_value <<= 8;
	*u32_counter_value += owh_resp.resp_data[3];

	return 0;
}
//获取memory的index页的数据
uint8_t Cmd_Owh_Get_Memory_Page(uint32_t num_index,uint8_t *data)
{
	OWH_COMMAND	 owh_cmd;
	OWH_RESPONSE owh_resp;
	uint8_t ret;

	if((num_index < 0)||(num_index > 16))
	{
		return 1;
	}
	memset(data,0,32);

	owh_cmd.command = 0xCA;
	owh_cmd.para_len = 1;
	owh_cmd.para_data[0] = num_index;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
	if(ret != 0x00)
	{
		return ret;
	}
	memcpy(data, owh_resp.resp_data, 32);
	return 0;
}

//设置memory的index页的数据
uint8_t Cmd_Owh_Set_Memory_Page(uint32_t num_index,uint8_t *data)
{
	OWH_COMMAND	 owh_cmd;
	OWH_RESPONSE owh_resp;
	uint8_t ret;
	uint8_t memory_data[32] = {0};
	uint8_t memory_data_check[32] = {0};

	memcpy(memory_data, data, 32);

	if((num_index < 0)||(num_index > 16))
	{
		return 1;
	}

	owh_cmd.command = 0xDA;
	owh_cmd.para_len = 0x21;
	owh_cmd.para_data[0] = num_index;
	memcpy(&owh_cmd.para_data[1],memory_data, 32);
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
	if(ret != 0x00)
	{
		return ret;
	}

	Cmd_Owh_Get_Memory_Page(num_index,memory_data_check);
	if (memcmp(memory_data, memory_data_check, 32) == 0)
	{
		return 0;
	}
	else
	{
		printf("write fail\r\n");
		return 1;
	}
}

//验证公钥和签名的合法性
uint8_t Cmd_Owh_Verify_Digital_Signature_Initial(uint8_t u8_page_index)
{
//	uint8_t u8_romid[8] = {0};
//	uint8_t u8_manid[2] = {0};

	OWH_COMMAND	 owh_cmd;
	OWH_RESPONSE owh_resp;
	uint8_t ret;

	// ret = OWH_ResetDevice(CMD_OWH_SKIPROM, 0);
	// if(ret != 0x00)
	// {
	// 	return ret;
	// }

	//**********1.read romid & manid**********//
	//执行获取ROMID/MANUID 指令，读取ROM ID 和MANU ID
	owh_cmd.command = 0xB2;
	owh_cmd.para_len = 0;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 1);
	if(ret != 0x00)
	{
		return ret;
	}
	memcpy(u8_v_romid, &owh_resp.resp_data[0], sizeof(u8_v_romid));
	memcpy(u8_v_manid, &owh_resp.resp_data[sizeof(u8_v_romid)], sizeof(u8_v_manid));

	//**********2.read page data**********//
	//读Memory 页
//	uint8_t memory_data_index[32] = {0};
	owh_cmd.command = 0xCA;
	owh_cmd.para_len = 1;
	owh_cmd.para_data[0] = u8_page_index;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
	if(ret != 0x00)
	{
		return ret;
	}
	memcpy(v_memory_data_index, owh_resp.resp_data, 32);

	//**********3.read device pubkey_X**********//
	//执行读Memory 页指令，读取芯片公钥对
//	uint8_t DEV_PubKey[64] = {0};

	owh_cmd.command = 0xCA;
	owh_cmd.para_len = 1;
	owh_cmd.para_data[0] = 0x3D;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
	if(ret != 0x00)
	{
		return ret;
	}
	memcpy(&v_DEV_PubKey[0x00], owh_resp.resp_data, 32);

	//**********4.read device pubkey_Y**********//
	owh_cmd.command = 0xCA;
	owh_cmd.para_len = 1;
	owh_cmd.para_data[0] = 0x3E;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
	if(ret != 0x00)
	{
		return ret;
	}
	memcpy(&v_DEV_PubKey[0x20], owh_resp.resp_data, 32);

	//**********5.read device pubkey sign_R**********//
	//执行读Memory 页指令，读取芯片公钥对证书
//	uint8_t DEV_PubKey_Sign[64] = {0};

	owh_cmd.command = 0xCA;
	owh_cmd.para_len = 1;
	owh_cmd.para_data[0] = 0x3B;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
	if(ret != 0x00)
	{
		return ret;
	}
	memcpy(&v_DEV_PubKey_Sign[0x00], owh_resp.resp_data, 32);

	//**********6.read device pubkey sign_S**********//
	owh_cmd.command = 0xCA;
	owh_cmd.para_len = 1;
	owh_cmd.para_data[0] = 0x3C;
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
	if(ret != 0x00)
	{
		return ret;
	}
	memcpy(&v_DEV_PubKey_Sign[0x20], owh_resp.resp_data, 32);

	//**********7.verify device pubkey sign**********//
	//主机使用CA 公钥对ROM ID||MANU ID||Chip_PubKey 和芯片公钥对证书进行验签，以验证芯片公钥对证书的合法性
	SDG_SHA_CTX context;
	const struct SDG_uECC_Curve_t * curve_para;
	curve_para = SDG_uECC_secp256r1();
	uint8_t u8_shal256_result[32] = {0};

	SDG_SHA_init(&context);
	SDG_SHA_update(&context,u8_v_romid,sizeof(u8_v_romid));//romid
	SDG_SHA_update(&context,u8_v_manid,sizeof(u8_v_manid));//manid
	SDG_SHA_update(&context,&v_DEV_PubKey[0x00],32);//pubkey_X
	SDG_SHA_update(&context,&v_DEV_PubKey[0x20],32);//pubkey_Y
	SDG_SHA_final(u8_shal256_result,&context);

	uint8_t m_e_temp[64] = {0xA2,0x36,0x96,0xB0,0x55,0x0C,0xA4,0x87,0x9F,0x86,0x04,0x3E,0x40,0xDE,0x83,0x04,\
							0xDB,0x59,0x9E,0x8F,0x8A,0x53,0x12,0x85,0xD1,0x3C,0x3E,0x96,0x86,0x1B,0x09,0xA5,\
							0x2F,0x72,0x68,0x40,0x54,0xCB,0x77,0x73,0x13,0x01,0x6B,0xA3,0x8F,0x40,0x7E,0x9B,\
							0x73,0x57,0xF7,0x4B,0xAF,0x6C,0x80,0x08,0xBA,0xD1,0x8B,0x34,0x29,0x90,0x49,0x9F};
	uint8_t m_temp[64] = {0};
	uint8_t temp_s[23] = "sdg.aisinochip.com.test";
	uint8_t temp_k[32] = {0};
	SDG_AES_KEY aesalg_key;

	SDG_SHA_hash(temp_s, sizeof(temp_s), temp_k);
	SDG_AES_set_decrypt_key(&temp_k[0], 128, &aesalg_key);
	SDG_AES_cbc_encrypt(m_e_temp, m_temp, sizeof(m_e_temp), &aesalg_key, &temp_k[16], SDG_AES_DECRYPT);

	ret = SDG_uECC_verify(m_temp, u8_shal256_result, sizeof(u8_shal256_result), v_DEV_PubKey_Sign, curve_para);//验证公钥和签名的合法性
	if (!ret) {
		return 1;
	}

	return 0;
}
//验签随机数，用私钥加密随机，用公钥验证
uint8_t Cmd_Owh_Verify_Digital_Signature_Test(uint8_t u8_page_index, uint8_t *u8_challenge_data)
{
	uint8_t u8_sign_value[64] = {0};

	OWH_COMMAND	 owh_cmd;
	OWH_RESPONSE owh_resp;
	uint8_t ret;

	OWH_Chip_PowerOn();

	// ret = OWH_ResetDevice(CMD_OWH_SKIPROM, 0);
	// if(ret != 0x00)
	// {
	// 	return ret;
	// }

	//**********8.ECDSA sign**********//
	//执行ECDSA 签名命令（包含PAGE#||CHALLENGE），SDG 芯片使用芯片私钥对ROMID||MANU ID|| PAGE DATA||PAGE#||CHALLENGE 签名，并返回签名值
	owh_cmd.command = 0x2A;
	owh_cmd.para_len = 0x21;
	owh_cmd.para_data[0] = u8_page_index;
	memcpy(&owh_cmd.para_data[1], u8_challenge_data, 32);
	ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 40);
	if(ret != 0x00)
	{
		return ret;
	}
	memcpy(u8_sign_value, owh_resp.resp_data, sizeof(u8_sign_value));

	OWH_Chip_PowerOff();

	//**********9.verify ECDSA sign**********//
	//主机使用芯片公钥对ROM ID||MANU ID|| PAGE DATA||PAGE#||CHALLENGE 的签名值进行验签，以验证SDG 芯片的合法性
	//使用的PAGE DATA 可通过读Memory 页指令获取,在第2步
	SDG_SHA_CTX context;
	const struct SDG_uECC_Curve_t * curve_para;
	curve_para = SDG_uECC_secp256r1();
	uint8_t u8_shal256_result[32] = {0};

	SDG_SHA_init(&context);
	SDG_SHA_update(&context, u8_v_romid, sizeof(u8_v_romid));
	SDG_SHA_update(&context, v_memory_data_index, sizeof(v_memory_data_index));
	SDG_SHA_update(&context, &u8_page_index, sizeof(u8_page_index));
	SDG_SHA_update(&context, u8_v_manid, sizeof(u8_v_manid));
	SDG_SHA_update(&context, u8_challenge_data, 32);
	SDG_SHA_final(u8_shal256_result, &context);

	ret = SDG_uECC_verify(v_DEV_PubKey, u8_shal256_result, sizeof(u8_shal256_result), u8_sign_value, curve_para);
	if (!ret) {
		return 1;
	}

	return 0;
}

/**
  * @brief
  * @note
  * @param
  * @return
  */
uint8_t u8_sign_value[64] = {0};
uint8_t Acl16_Processor(void)
{
	OWH_COMMAND	 owh_cmd;
	OWH_RESPONSE owh_resp;
	uint8_t ret;
	SDG_SHA_CTX context;
	uint8_t u8_page_index = 0;
	uint8_t u8_challenge[32] = {0x12,0xE6,0x37,0x4B,0xA8,0xB2,0x62,0x56,0x1B,0xD9,0xAB,0x84,0xBE,0xB3,0x18,0x8D,\
								0xF1,0x6D,0x31,0xAC,0x97,0xAF,0x11,0x5E,0xCA,0x7B,0xBC,0x68,0xCD,0x59,0xB3,0x17};
	const struct SDG_uECC_Curve_t * curve_para;
	uint8_t u8_shal256_result[32] = {0};

	uint8_t m_e_temp[64] = {0xA2,0x36,0x96,0xB0,0x55,0x0C,0xA4,0x87,0x9F,0x86,0x04,0x3E,0x40,0xDE,0x83,0x04,\
							0xDB,0x59,0x9E,0x8F,0x8A,0x53,0x12,0x85,0xD1,0x3C,0x3E,0x96,0x86,0x1B,0x09,0xA5,\
							0x2F,0x72,0x68,0x40,0x54,0xCB,0x77,0x73,0x13,0x01,0x6B,0xA3,0x8F,0x40,0x7E,0x9B,\
							0x73,0x57,0xF7,0x4B,0xAF,0x6C,0x80,0x08,0xBA,0xD1,0x8B,0x34,0x29,0x90,0x49,0x9F};
	uint8_t m_temp[64] = {0};
	uint8_t temp_s[23] = "sdg.aisinochip.com.test";
	uint8_t temp_k[32] = {0};
	SDG_AES_KEY aesalg_key;

  	ACL16_LOG("Acl16_ProcessorTimer acl16_status = %d\r\n", acl16_status);
	switch (acl16_status)
	{
		case ACL16_INIT:
			acl16_status = ACL16_READID;
			break;
		case ACL16_READID:
			//**********1.read romid & manid**********//
			//执行获取ROMID/MANUID 指令，读取ROM ID 和MANU ID
			owh_cmd.command = 0xB2;
			owh_cmd.para_len = 0;
			ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 1);
			if(ret != 0x00)
			{
				acl16_status = ACL16_ERROR;
			}
			else
			{
			memcpy(u8_v_romid, &owh_resp.resp_data[0], sizeof(u8_v_romid));
			memcpy(u8_v_manid, &owh_resp.resp_data[sizeof(u8_v_romid)], sizeof(u8_v_manid));
			acl16_status = ACL16_READPAGE;
			}
			break;
		case ACL16_READPAGE:
			//**********2.read page data**********//
			//读Memory 页
			owh_cmd.command = 0xCA;
			owh_cmd.para_len = 1;
			owh_cmd.para_data[0] = u8_page_index;
			ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
			if(ret != 0x00)
			{
				acl16_status = ACL16_ERROR;
			}
			else
			{
				memcpy(v_memory_data_index, owh_resp.resp_data, 32);
				acl16_status = ACL16_READ_PUBKEY_X;
			}
			break;
		case ACL16_READ_PUBKEY_X:
			//**********3.read device pubkey_X**********//
			//执行读Memory 页指令，读取芯片公钥对
			owh_cmd.command = 0xCA;
			owh_cmd.para_len = 1;
			owh_cmd.para_data[0] = 0x3D;
			ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
			if(ret != 0x00)
			{
				acl16_status = ACL16_ERROR;
			}
			else
			{
				memcpy(&v_DEV_PubKey[0x00], owh_resp.resp_data, 32);
				acl16_status = ACL16_READ_PUBKEY_Y;
			}
			break;
		case ACL16_READ_PUBKEY_Y:
			//**********4.read device pubkey_Y**********//
			owh_cmd.command = 0xCA;
			owh_cmd.para_len = 1;
			owh_cmd.para_data[0] = 0x3E;
			ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
			if(ret != 0x00)
			{
				acl16_status = ACL16_ERROR;
			}
			else
			{
				memcpy(&v_DEV_PubKey[0x20], owh_resp.resp_data, 32);
				acl16_status = ACL16_READ_PUBKEY_SIGN_R;
			}
			break;
		case ACL16_READ_PUBKEY_SIGN_R:
			//**********5.read device pubkey sign_R**********//
			//执行读Memory 页指令，读取芯片公钥对证书
			owh_cmd.command = 0xCA;
			owh_cmd.para_len = 1;
			owh_cmd.para_data[0] = 0x3B;
			ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
			if(ret != 0x00)
			{
				acl16_status = ACL16_ERROR;
			}
			else
			{
				memcpy(&v_DEV_PubKey_Sign[0x00], owh_resp.resp_data, 32);
				acl16_status = ACL16_READ_PUBKEY_SIGN_S;
			}
			break;
		case ACL16_READ_PUBKEY_SIGN_S:
			//**********6.read device pubkey sign_S**********//
			owh_cmd.command = 0xCA;
			owh_cmd.para_len = 1;
			owh_cmd.para_data[0] = 0x3C;
			ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 3);
			if(ret != 0x00)
			{
				acl16_status = ACL16_ERROR;
			}
			else
			{
				memcpy(&v_DEV_PubKey_Sign[0x20], owh_resp.resp_data, 32);
				acl16_status = ACL16_VERIFY_PUBKEY_SIGN;
			}
			break;
		case ACL16_VERIFY_PUBKEY_SIGN:
			//**********7.verify device pubkey sign**********//
			//主机使用CA 公钥对ROM ID||MANU ID||Chip_PubKey 和芯片公钥对证书进行验签，以验证芯片公钥对证书的合法性
			curve_para = SDG_uECC_secp256r1();

			SDG_SHA_init(&context);
			SDG_SHA_update(&context,u8_v_romid,sizeof(u8_v_romid));//romid
			SDG_SHA_update(&context,u8_v_manid,sizeof(u8_v_manid));//manid
			SDG_SHA_update(&context,&v_DEV_PubKey[0x00],32);//pubkey_X
			SDG_SHA_update(&context,&v_DEV_PubKey[0x20],32);//pubkey_Y
			SDG_SHA_final(u8_shal256_result,&context);

			SDG_SHA_hash(temp_s, sizeof(temp_s), temp_k);
			SDG_AES_set_decrypt_key(&temp_k[0], 128, &aesalg_key);
			SDG_AES_cbc_encrypt(m_e_temp, m_temp, sizeof(m_e_temp), &aesalg_key, &temp_k[16], SDG_AES_DECRYPT);

			ret = SDG_uECC_verify(m_temp, u8_shal256_result, sizeof(u8_shal256_result), v_DEV_PubKey_Sign, curve_para);//验证公钥和签名的合法性
			if (!ret) {
				acl16_status = ACL16_ERROR;
			}
			else
			{
				acl16_status = ACL16_PERFORM_ECDSA_SIGN;
			}
			break;
		case ACL16_PERFORM_ECDSA_SIGN:
			OWH_Chip_PowerOn();
			//**********8.ECDSA sign**********//
			//执行ECDSA 签名命令（包含PAGE#||CHALLENGE），SDG 芯片使用芯片私钥对ROMID||MANU ID|| PAGE DATA||PAGE#||CHALLENGE 签名，并返回签名值
			owh_cmd.command = 0x2A;
			owh_cmd.para_len = 0x21;
			owh_cmd.para_data[0] = u8_page_index;
			memcpy(&owh_cmd.para_data[1], u8_challenge, 32);
			ret = OWH_Command_Response((OWH_COMMAND *)&owh_cmd, (OWH_RESPONSE *)&owh_resp, 40);
			if(ret != 0x00)
			{
				acl16_status = ACL16_ERROR;
			}
			else
			{
				memcpy(u8_sign_value, owh_resp.resp_data, sizeof(u8_sign_value));
				OWH_Chip_PowerOff();
				acl16_status = ACL16_VERIFY_ECDSA_SIGN;
			}
			break;
		case ACL16_VERIFY_ECDSA_SIGN:
			//**********9.verify ECDSA sign**********//
			//主机使用芯片公钥对ROM ID||MANU ID|| PAGE DATA||PAGE#||CHALLENGE 的签名值进行验签，以验证SDG 芯片的合法性
			//使用的PAGE DATA 可通过读Memory 页指令获取,在第2步
			curve_para = SDG_uECC_secp256r1();

			SDG_SHA_init(&context);
			SDG_SHA_update(&context, u8_v_romid, sizeof(u8_v_romid));
			SDG_SHA_update(&context, v_memory_data_index, sizeof(v_memory_data_index));
			SDG_SHA_update(&context, &u8_page_index, sizeof(u8_page_index));
			SDG_SHA_update(&context, u8_v_manid, sizeof(u8_v_manid));
			SDG_SHA_update(&context, u8_challenge, 32);
			SDG_SHA_final(u8_shal256_result, &context);

			ret = SDG_uECC_verify(v_DEV_PubKey, u8_shal256_result, sizeof(u8_shal256_result), u8_sign_value, curve_para);
			if (!ret) {
				acl16_status = ACL16_ERROR;
			}
			else
			{
				acl16_status = ACL16_END;
			}
			break;
		case ACL16_END:break;
		case ACL16_ERROR:break;
		default:break;
	}
	ACL16_LOG("Acl16_ProcessorTimer finish\r\n");
	return acl16_status;
}
