/***********************************************************************
 * Copyright (c)  2008 - 2021, Shanghai AisinoChip Co.,Ltd .
 * All rights reserved.
 * Filename    : owh.h
 * Description : one-wire host example header file
 * Author(s)   : liujuia
 * version     : V1.0
 * Modify date : 2023-02-08
 ***********************************************************************/
#ifndef __OWH_H__
#define __OWH_H__

#include  "stdio.h"	   //printf .....
#include  "string.h"   //strlen ,memset,strcmp,memcmp,strcpy .....  
#include <stdlib.h>  // standard lib functions
#include <stddef.h>  // standard definitions
#include <stdint.h>  // standard integer definition
#include <stdbool.h> // boolean definition

#include  "owh.h"
#include  "owh_cmd.h"
#include  "peripheral.h"
#include "device.h"
#include "component.h"

//cmd_code definition
//bits[7:4] means operation, bits[3:0] means mode.
#define CMD_OWH_READROM				0x33
#define CMD_OWH_SKIPROM				0xCC

#define VHW_IIC_ACL16		vIIC_5
typedef struct _owh_command_t
{
	uint8_t dummy[2];
	uint8_t command;
	uint8_t para_len;
	uint8_t para_data[260];
}OWH_COMMAND;

typedef struct _owh_response_t
{
	uint8_t dummy[2];
	uint8_t resp_len;
	uint8_t resp_code;
	uint8_t resp_data[260];
}OWH_RESPONSE;

//search romid
//uint8_t OWH_Search(uint8_t search_type, uint8_t *romid);
//skip romid or read romid

uint8_t OWH_ResetDevice(uint8_t type, uint8_t *romid);

uint8_t OWH_Command_Response(OWH_COMMAND *cmd, OWH_RESPONSE *resp, uint32_t wait_ms);

#if 1
#define OwhprintfS     printf
#define OwhprintfB8    owh_printf_buff_byte
#define OwhprintfB32   owh_printf_buff_word	 
#else
#define	OwhprintfS(format, ...)	     		((void)0)
#define	OwhprintfB8(buff, byte_len)	 		((void)0)
#define	OwhprintfB32(buff, word_len)	 	((void)0)
#endif

#define SWAP16(x)             ((((x) & 0xFF) << 8) | (((x) >> 8) & 0xFF))
#define SWAP32(x)             ((((x) & 0xFF) << 24) | (((x) & 0xFF00) << 8) | (((x) >> 8) & 0xFF00) | (((x) >> 24) & 0xFF))

void owh_printf_buff_byte(const uint8_t* buff, uint32_t length);
void owh_printf_buff_word(const uint32_t* buff, uint32_t length);
void owh_systick_delay_us(uint32_t us);
void owh_systick_delay_ms(uint32_t ms);

uint8_t Hal_Owh_IIC_Init(void);
void Hal_Owh_IIC_Deinit(void);
uint8_t Hal_Owh_ChkCrc_Rom_ID(uint8_t *rom_id);
uint8_t Hal_Owh_Read_Unique_ID(uint8_t *rom_id);
uint8_t Hal_Owh_Read_PCM_ID(uint8_t *pcm_id);
uint8_t Hal_Owh_Write_Counter_Value(void);
uint8_t Hal_Owh_Read_Counter_Value(uint32_t *counter_value);
uint8_t Hal_Owh_Verify_Digital_Signature(void);

#endif

