/*
 * Copyright 2002-2016 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the OpenSSL license (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */
#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>

#ifndef HEADER_SDG_AES_H
# define HEADER_SDG_AES_H

# include <stddef.h>
# ifdef  __cplusplus
extern "C" {
# endif

# define SDG_AES_ENCRYPT     1
# define SDG_AES_DECRYPT     0

/*
 * Because array size can't be a const in C, the following two are macros.
 * Both sizes are in bytes.
 */
# define SDG_AES_MAXNR 14
# define SDG_AES_BLOCK_SIZE 16

/* This should be a hidden type, but EVP requires that the size be known */
struct sdg_aes_key_st {
    uint32_t rd_key[4 * (SDG_AES_MAXNR + 1)];
    int rounds;
};
typedef struct sdg_aes_key_st SDG_AES_KEY;

const char *SDG_AES_options(void);

int SDG_AES_set_encrypt_key(const unsigned char *userKey, const int bits,
                        SDG_AES_KEY *key);
int SDG_AES_set_decrypt_key(const unsigned char *userKey, const int bits,
                        SDG_AES_KEY *key);

void SDG_AES_encrypt(const unsigned char *in, unsigned char *out,
                 const SDG_AES_KEY *key);
void SDG_AES_decrypt(const unsigned char *in, unsigned char *out,
                 const SDG_AES_KEY *key);

void SDG_AES_ecb_encrypt(const unsigned char *in, unsigned char *out,
                     unsigned int len, const SDG_AES_KEY *key,
					 const int enc);
void SDG_AES_cbc_encrypt(const unsigned char *in, unsigned char *out,
                     unsigned int len, const SDG_AES_KEY *key,
                     unsigned char *ivec, const int enc);


# ifdef  __cplusplus
}
# endif

#endif
