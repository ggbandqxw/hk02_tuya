/**************************************************************************
Filename	: sha_sdg.h
Language	: head file
Description	: define some function and variable in sha_sdg.c;
Author(s)	: lhm  
Company		: AisinoChip Ltd.
version 	: 1.0
Change Log	: 2011-07-05 14:08, first edition
***************************************************************************/
#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>

#ifndef __SHA_SDG_H__
#define __SHA_SDG_H__

/**********************************************************
*	definitions
**********************************************************/
#define SDG_SHA_ROTR(bits,word) (((word) >> (bits)) | ((word) << (32-(bits))))
#define SDG_SHA_SHR(bits,word)  ((word) >> (bits))
#define SDG_SHA_CH(x,y,z) ((x&y)^(~x&z))
#define SDG_SHA_MAJ(x,y,z) ((x&y)^(x&z)^(y&z)) 
#define SDG_SHA_E0(x) (SDG_SHA_ROTR(2,x)^SDG_SHA_ROTR(13,x)^SDG_SHA_ROTR(22,x))
#define SDG_SHA_E1(x) (SDG_SHA_ROTR(6,x)^SDG_SHA_ROTR(11,x)^SDG_SHA_ROTR(25,x))
#define SDG_SHA_A0(x) (SDG_SHA_ROTR(7,x)^SDG_SHA_ROTR(18,x)^SDG_SHA_SHR(3,x))
#define SDG_SHA_A1(x) (SDG_SHA_ROTR(17,x)^SDG_SHA_ROTR(19,x)^SDG_SHA_SHR(10,x))



/**********************************************************
*	include files
**********************************************************/

//#include "common.h"
/**********************************************************
*	structure
**********************************************************/
//SHA1 context
typedef struct {
  uint32_t state[8];                                   //state (ABCD)
  uint32_t count[2];        // number of bits, modulo 2^64 (msb first) 
  uint8_t  buffer[64];                         // input buffer
} SDG_SHA_CTX;
/**********************************************************
*	extern variable
***********************************************************/

/**********************************************************
*	extern functions
***********************************************************/


void SDG_SHA_init(SDG_SHA_CTX *context);

void SDG_SHA_transform (uint32_t *state, uint8_t *block);

void SDG_SHA_update (SDG_SHA_CTX *context, uint8_t *input,uint32_t inputLen);

void SDG_SHA_final (uint8_t *digest, SDG_SHA_CTX *context);

void SDG_SHA_hash(uint8_t *pDataIn,uint32_t DataLen,uint8_t *pDigest);

#endif
