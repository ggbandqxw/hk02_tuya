/* Copyright 2015, Kenneth MacKay. Licensed under the BSD 2-clause license. */

#ifndef _UECC_VLI_SDG_H_
#define _UECC_VLI_SDG_H_

#include "uecc_sdg.h"
#include "types_sdg.h"

/* Functions for raw large-integer manipulation. These are only available
   if SDG_uECC.c is compiled with SDG_uECC_ENABLE_VLI_API defined to 1. */
#ifndef SDG_uECC_ENABLE_VLI_API
    #define SDG_uECC_ENABLE_VLI_API 0
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#if SDG_uECC_ENABLE_VLI_API

void SDG_uECC_vli_clear(SDG_uECC_word_t *vli, wordcount_t num_words);

/* Constant-time comparison to zero - secure way to compare long integers */
/* Returns 1 if vli == 0, 0 otherwise. */
SDG_uECC_word_t SDG_uECC_vli_isZero(const SDG_uECC_word_t *vli, wordcount_t num_words);

/* Returns nonzero if bit 'bit' of vli is set. */
SDG_uECC_word_t SDG_uECC_vli_testBit(const SDG_uECC_word_t *vli, bitcount_t bit);

/* Counts the number of bits required to represent vli. */
bitcount_t SDG_uECC_vli_numBits(const SDG_uECC_word_t *vli, const wordcount_t max_words);

/* Sets dest = src. */
void SDG_uECC_vli_set(SDG_uECC_word_t *dest, const SDG_uECC_word_t *src, wordcount_t num_words);

/* Constant-time comparison function - secure way to compare long integers */
/* Returns one if left == right, zero otherwise */
SDG_uECC_word_t SDG_uECC_vli_equal(const SDG_uECC_word_t *left,
                           const SDG_uECC_word_t *right,
                           wordcount_t num_words);

/* Constant-time comparison function - secure way to compare long integers */
/* Returns sign of left - right, in constant time. */
cmpresult_t SDG_uECC_vli_cmp(const SDG_uECC_word_t *left, const SDG_uECC_word_t *right, wordcount_t num_words);

/* Computes vli = vli >> 1. */
void SDG_uECC_vli_rshift1(SDG_uECC_word_t *vli, wordcount_t num_words);

/* Computes result = left + right, returning carry. Can modify in place. */
SDG_uECC_word_t SDG_uECC_vli_add(SDG_uECC_word_t *result,
							const SDG_uECC_word_t *left,
							const SDG_uECC_word_t *right,
							wordcount_t num_words);

/* Computes result = left - right, returning borrow. Can modify in place. */
SDG_uECC_word_t SDG_uECC_vli_sub(SDG_uECC_word_t *result,
                         const SDG_uECC_word_t *left,
                         const SDG_uECC_word_t *right,
                         wordcount_t num_words);

/* Computes result = left * right. Result must be 2 * num_words long. */
void SDG_uECC_vli_mult(SDG_uECC_word_t *result,
                   const SDG_uECC_word_t *left,
                   const SDG_uECC_word_t *right,
                   wordcount_t num_words);

/* Computes result = left^2. Result must be 2 * num_words long. */
void SDG_uECC_vli_square(SDG_uECC_word_t *result, const SDG_uECC_word_t *left, wordcount_t num_words);

/* Computes result = (left + right) % mod.
   Assumes that left < mod and right < mod, and that result does not overlap mod. */
void SDG_uECC_vli_modAdd(SDG_uECC_word_t *result,
                     const SDG_uECC_word_t *left,
                     const SDG_uECC_word_t *right,
                     const SDG_uECC_word_t *mod,
                     wordcount_t num_words);

/* Computes result = (left - right) % mod.
   Assumes that left < mod and right < mod, and that result does not overlap mod. */
void SDG_uECC_vli_modSub(SDG_uECC_word_t *result,
                     const SDG_uECC_word_t *left,
                     const SDG_uECC_word_t *right,
                     const SDG_uECC_word_t *mod,
                     wordcount_t num_words);

/* Computes result = product % mod, where product is 2N words long.
   Currently only designed to work for mod == curve->p or curve_n. */
void SDG_uECC_vli_mmod(SDG_uECC_word_t *result,
                   SDG_uECC_word_t *product,
                   const SDG_uECC_word_t *mod,
                   wordcount_t num_words);

/* Calculates result = product (mod curve->p), where product is up to
   2 * curve->num_words long. */
void SDG_uECC_vli_mmod_fast(SDG_uECC_word_t *result, SDG_uECC_word_t *product, SDG_uECC_Curve curve);

/* Computes result = (left * right) % mod.
   Currently only designed to work for mod == curve->p or curve_n. */
void SDG_uECC_vli_modMult(SDG_uECC_word_t *result,
                      const SDG_uECC_word_t *left,
                      const SDG_uECC_word_t *right,
                      const SDG_uECC_word_t *mod,
                      wordcount_t num_words);

/* Computes result = (left * right) % curve->p. */
void SDG_uECC_vli_modMult_fast(SDG_uECC_word_t *result,
                           const SDG_uECC_word_t *left,
                           const SDG_uECC_word_t *right,
                           SDG_uECC_Curve curve);

/* Computes result = left^2 % mod.
   Currently only designed to work for mod == curve->p or curve_n. */
void SDG_uECC_vli_modSquare(SDG_uECC_word_t *result,
                        const SDG_uECC_word_t *left,
                        const SDG_uECC_word_t *mod,
                        wordcount_t num_words);

/* Computes result = left^2 % curve->p. */
void SDG_uECC_vli_modSquare_fast(SDG_uECC_word_t *result, const SDG_uECC_word_t *left, SDG_uECC_Curve curve);

/* Computes result = (1 / input) % mod.*/
void SDG_uECC_vli_modInv(SDG_uECC_word_t *result,
                     const SDG_uECC_word_t *input,
                     const SDG_uECC_word_t *mod,
                     wordcount_t num_words);

#if SDG_uECC_SUPPORT_COMPRESSED_POINT
/* Calculates a = sqrt(a) (mod curve->p) */
void SDG_uECC_vli_mod_sqrt(SDG_uECC_word_t *a, SDG_uECC_Curve curve);
#endif

/* Converts an integer in SDG_uECC native format to big-endian bytes. */
void SDG_uECC_vli_nativeToBytes(uint8_t *bytes, int num_bytes, const SDG_uECC_word_t *native);
/* Converts big-endian bytes to an integer in SDG_uECC native format. */
void SDG_uECC_vli_bytesToNative(SDG_uECC_word_t *native, const uint8_t *bytes, int num_bytes);

unsigned SDG_uECC_curve_num_words(SDG_uECC_Curve curve);
unsigned SDG_uECC_curve_num_bytes(SDG_uECC_Curve curve);
unsigned SDG_uECC_curve_num_bits(SDG_uECC_Curve curve);
unsigned SDG_uECC_curve_num_n_words(SDG_uECC_Curve curve);
unsigned SDG_uECC_curve_num_n_bytes(SDG_uECC_Curve curve);
unsigned SDG_uECC_curve_num_n_bits(SDG_uECC_Curve curve);

const SDG_uECC_word_t *SDG_uECC_curve_p(SDG_uECC_Curve curve);
const SDG_uECC_word_t *SDG_uECC_curve_n(SDG_uECC_Curve curve);
const SDG_uECC_word_t *SDG_uECC_curve_G(SDG_uECC_Curve curve);
const SDG_uECC_word_t *SDG_uECC_curve_b(SDG_uECC_Curve curve);

//int SDG_uECC_valid_point(const SDG_uECC_word_t *point, SDG_uECC_Curve curve);

/* Multiplies a point by a scalar. Points are represented by the X coordinate followed by
   the Y coordinate in the same array, both coordinates are curve->num_words long. Note
   that scalar must be curve->num_n_words long (NOT curve->num_words). */
void SDG_uECC_point_mult(SDG_uECC_word_t *result,
                     const SDG_uECC_word_t *point,
                     const SDG_uECC_word_t *scalar,
                     SDG_uECC_Curve curve);

/* Generates a random integer in the range 0 < random < top.
   Both random and top have num_words words. */
int SDG_uECC_generate_random_int(SDG_uECC_word_t *random,
                             const SDG_uECC_word_t *top,
                             wordcount_t num_words);

#endif /* SDG_uECC_ENABLE_VLI_API */

#ifdef __cplusplus
} /* end of extern "C" */
#endif

#endif /* _UECC_VLI_H_ */
