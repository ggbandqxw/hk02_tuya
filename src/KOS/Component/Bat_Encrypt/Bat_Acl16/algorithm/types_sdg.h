/* Copyright 2015, Kenneth MacKay. Licensed under the BSD 2-clause license. */

#ifndef _UECC_TYPES_H_
#define _UECC_TYPES_H_

#define SDG_uECC_PLATFORM SDG_uECC_arm
#ifndef SDG_uECC_PLATFORM
    #if __AVR__
        #define SDG_uECC_PLATFORM SDG_uECC_avr
    #elif defined(__thumb2__) || defined(_M_ARMT) /* I think MSVC only supports Thumb-2 targets */
        #define SDG_uECC_PLATFORM SDG_uECC_arm_thumb2
    #elif defined(__thumb__)
        #define SDG_uECC_PLATFORM SDG_uECC_arm_thumb
    #elif defined(__arm__) || defined(_M_ARM)
        #define SDG_uECC_PLATFORM SDG_uECC_arm
    #elif defined(__aarch64__)
        #define SDG_uECC_PLATFORM SDG_uECC_arm64
    #elif defined(__i386__) || defined(_M_IX86) || defined(_X86_) || defined(__I86__)
        #define SDG_uECC_PLATFORM SDG_uECC_x86
    #elif defined(__amd64__) || defined(_M_X64)
        #define SDG_uECC_PLATFORM SDG_uECC_x86_64
    #else
        #define SDG_uECC_PLATFORM SDG_uECC_arch_other
    #endif
#endif

#ifndef SDG_uECC_ARM_USE_UMAAL
    #if (SDG_uECC_PLATFORM == SDG_uECC_arm) && (__ARM_ARCH >= 6)
        #define SDG_uECC_ARM_USE_UMAAL 1
    #elif (SDG_uECC_PLATFORM == SDG_uECC_arm_thumb2) && (__ARM_ARCH >= 6) && !__ARM_ARCH_7M__
        #define SDG_uECC_ARM_USE_UMAAL 1
    #else
        #define SDG_uECC_ARM_USE_UMAAL 0
    #endif
#endif

#ifndef SDG_uECC_WORD_SIZE
    #if SDG_uECC_PLATFORM == SDG_uECC_avr
        #define SDG_uECC_WORD_SIZE 1
    #elif (SDG_uECC_PLATFORM == SDG_uECC_x86_64 || SDG_uECC_PLATFORM == SDG_uECC_arm64)
        #define SDG_uECC_WORD_SIZE 8
    #else
        #define SDG_uECC_WORD_SIZE 4
    #endif
#endif

#if (SDG_uECC_WORD_SIZE != 1) && (SDG_uECC_WORD_SIZE != 4) && (SDG_uECC_WORD_SIZE != 8)
    #error "Unsupported value for SDG_uECC_WORD_SIZE"
#endif

#if ((SDG_uECC_PLATFORM == SDG_uECC_avr) && (SDG_uECC_WORD_SIZE != 1))
    #pragma message ("SDG_uECC_WORD_SIZE must be 1 for AVR")
    #undef SDG_uECC_WORD_SIZE
    #define SDG_uECC_WORD_SIZE 1
#endif

#if ((SDG_uECC_PLATFORM == SDG_uECC_arm || SDG_uECC_PLATFORM == SDG_uECC_arm_thumb || \
        SDG_uECC_PLATFORM ==  SDG_uECC_arm_thumb2) && \
     (SDG_uECC_WORD_SIZE != 4))
    #pragma message ("SDG_uECC_WORD_SIZE must be 4 for ARM")
    #undef SDG_uECC_WORD_SIZE
    #define SDG_uECC_WORD_SIZE 4
#endif

#if 0//defined(__SIZEOF_INT128__) || ((__clang_major__ * 100 + __clang_minor__) >= 302)
    #define SUPPORTS_INT128 1
#else
    #define SUPPORTS_INT128 0
#endif

typedef int8_t wordcount_t;
typedef int16_t bitcount_t;
typedef int8_t cmpresult_t;

#if (SDG_uECC_WORD_SIZE == 1)

typedef uint8_t SDG_uECC_word_t;
typedef uint16_t SDG_uECC_dword_t;

#define HIGH_BIT_SET 0x80
#define SDG_uECC_WORD_BITS 8
#define SDG_uECC_WORD_BITS_SHIFT 3
#define SDG_uECC_WORD_BITS_MASK 0x07

#elif (SDG_uECC_WORD_SIZE == 4)

typedef uint32_t SDG_uECC_word_t;
typedef uint64_t SDG_uECC_dword_t;

#define HIGH_BIT_SET 0x80000000
#define SDG_uECC_WORD_BITS 32
#define SDG_uECC_WORD_BITS_SHIFT 5
#define SDG_uECC_WORD_BITS_MASK 0x01F

#elif (SDG_uECC_WORD_SIZE == 8)

typedef uint64_t SDG_uECC_word_t;
#if SUPPORTS_INT128
typedef unsigned __int128 SDG_uECC_dword_t;
#endif

#define HIGH_BIT_SET 0x8000000000000000ull
#define SDG_uECC_WORD_BITS 64
#define SDG_uECC_WORD_BITS_SHIFT 6
#define SDG_uECC_WORD_BITS_MASK 0x03F

#endif /* SDG_uECC_WORD_SIZE */

#endif /* _UECC_TYPES_H_ */
