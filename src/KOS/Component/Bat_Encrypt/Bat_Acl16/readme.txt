/************设备认证芯片测试示例***************/
IDE版本：KEIL5

功能：ACL16_KDS_I2C测试设备认证芯片功能

文件说明：所有相关文件及函数库存放路径：..\algorithm和..\owh
..\algorith：存放加密算法库文件
owh.c/owh.h：单总线-I2C版本命令封装
owh_cmd.c/owh_cmd.h：设备认证指令函数库调用接口声明文件
owh_app.c/owh_app.h：封装用户测试指令
owh_iic.c/owh_iic.h：硬件模拟的iic
aes_sdg.c/aes_sdg.h：aes加密
sha_sdg.c/sha_sdg.h：sha加密
uecc_sdg.c/uecc_sdg.h：ecc加密

时间：2023/02/12
作者：liujia