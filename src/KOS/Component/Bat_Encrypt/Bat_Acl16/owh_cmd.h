#ifndef __OWH_CMD_H__
#define __OWH_CMD_H__

#include "owh.h"

typedef enum 
{
	ACL16_INIT,			             //初始化
	ACL16_READID,			         //读取ROMid和manid
    ACL16_READPAGE,                  //读取page x
	ACL16_READ_PUBKEY_X,			 //读取芯片公钥x
    ACL16_READ_PUBKEY_Y,             //读取芯片公钥y
    ACL16_READ_PUBKEY_SIGN_R,        //读取证书R
    ACL16_READ_PUBKEY_SIGN_S,        //读取证书S
    ACL16_VERIFY_PUBKEY_SIGN,        //验证公钥和证书的合法性
    ACL16_PERFORM_ECDSA_SIGN,        //执行ECDSA 签名命令
    ACL16_VERIFY_ECDSA_SIGN,         //签名值进行验签
    ACL16_END,                       //结束状态
    ACL16_ERROR,                     //错误状态
}ACL6Status_enum_t;

uint8_t Cmd_Owh_ChkCrc_RomdID(uint8_t *rom_id);
uint8_t Cmd_Owh_Get_PCM_ID(uint8_t *pcm_id);
uint8_t Cmd_Owh_Set_Counter_Value(void);
uint8_t Cmd_Owh_Get_Counter_Value(uint32_t *u32_counter_value);
uint8_t Cmd_Owh_Get_Memory_Page(uint32_t num_index,uint8_t *data);
uint8_t Cmd_Owh_Set_Memory_Page(uint32_t num_index,uint8_t *data);
uint8_t Cmd_Owh_Verify_Digital_Signature_Initial(uint8_t u8_page_index);
uint8_t Cmd_Owh_Verify_Digital_Signature_Test(uint8_t u8_page_index, uint8_t *u8_challenge_data);
uint8_t Acl16_Processor(void);
#endif
