#ifndef __BAT_ENCRYPT_H__
#define __BAT_ENCRYPT_H__

#include "osal.h"

typedef struct 
{
    uint16_t percentage;	    //电量百分比
    uint16_t addr;              //加密芯片地址
    uint32_t count;             //当前计数值
    uint16_t status;            //状态值 0:正常  1:异常
}Bat_Encrypt_Msg_t;

/*
 * 发送数据包（应用层->COMP）
 * param：该参数是个结构体指针：LockSendBleMsg_t*
 */
#define BAT_ENCRY_SendDataToBAT(str, len)                                       \
    ({                                                                          \
        uint8_t *temp = (uint8_t *)OSAL_Malloc(sizeof(Bat_Encrypt_Msg_t));        \
        memcpy((char *)&temp[0], str, len);                                     \
        OSAL_MboxPost(COMP_BATTERY_ENCRY, 0, temp, sizeof(Bat_Encrypt_Msg_t));    \
        OSAL_Free(temp);                                                        \
    })



#endif
