#ifndef USECASE_USECASE_H_
#define USECASE_USECASE_H_

/***************************************************************************************
 * Copyright 2020 Infineon Technologies AG ( www.infineon.com ).                       *
 * All rights reserved.                                                                *
 *                                                                                     *
 * Licensed  Material-Property of Infineon Technologies AG.                            *
 * This software is made available solely pursuant to the terms of Infineon            *
 * Technologies AG agreement which governs its use. This code and the information      *
 * contained in it are proprietary and confidential to Infineon Technologies AG.       *
 * No person is allowed to copy, reprint, reproduce or publish any part of this code,  *
 * nor disclose its contents to others, nor make any use of it, nor allow or assist    *
 * others to make any use of it - unless by prior Written express authorization of     *
 * Infineon Technologies AG and then only to the extent authorized.                    *
 *                                                                                     *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,            *
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,           *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO       *
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,                 *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;         *
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY             *
 * WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR            *
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF              *
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                          *
 *                                                                                     *
 ***************************************************************************************/
/**
 * @file   usecase.h
 * @date   June, 2020
 * @brief  Definition of usecase functions
 */
#include "auths_config.h"

//display all SFR
void read_all_sfr(void);

// NVM test
void test_nvm(void);
void nvm_lock_status(void);
void nvmtest_lock(void);
void nvmtest_resetlock(void);
void nvmtest_changeDevAddr(uint16_t nw_newDevAddr);
void nvmtest_WritePg(uint16_t nvm_page,uint8_t bytetowrite,uint8_t * data);

// LSC test
void test_LscDec(uint8_t lsc_number);
void test_Lsc1AutoDec(void);
void test_Lsc2AutoDec(void);
void test_LscWrite(uint8_t lsc_number);
void test_LscLock(uint8_t lsc_number);
void test_LscSetZero(uint8_t lsc_number);
void test_LscReset(uint8_t lsc_number);

// ECC test
void test_kill_ecc_auto(void);

//NVM test
void nvmtest_resetlock();
void test_LscReset(uint8_t lsc_numb);

//GPO test
void test_gpo(void);
void ha_set_gpo_high(void);
void lsc2_set_gpo_high(void);

#endif /* USECASE_USECASE_H_ */
