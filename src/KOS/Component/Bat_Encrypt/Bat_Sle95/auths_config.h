/***************************************************************************************
 * Copyright 2020 Infineon Technologies AG ( www.infineon.com ).                       *
 * All rights reserved.                                                                *
 *                                                                                     *
 * Licensed  Material-Property of Infineon Technologies AG.                            *
 * This software is made available solely pursuant to the terms of Infineon            *
 * Technologies AG agreement which governs its use. This code and the information      *
 * contained in it are proprietary and confidential to Infineon Technologies AG.       *
 * No person is allowed to copy, reprint, reproduce or publish any part of this code,  *
 * nor disclose its contents to others, nor make any use of it, nor allow or assist    *
 * others to make any use of it - unless by prior Written express authorization of     *
 * Infineon Technologies AG and then only to the extent authorized.                    *
 *                                                                                     *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,            *
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,           *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO       *
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,                 *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;         *
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY             *
 * WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR            *
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF              *
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                          *
 *                                                                                     *
 ***************************************************************************************/
/**
 * @file   auths_config.h
 * @date   May, 2021
 * @brief  SDK configuration
 *
 */
#ifndef AUTHS_CONFIG_H_
#define AUTHS_CONFIG_H_

#include "stdint.h"
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

/**
 @brief Interface configuration
 */
/* SDK Version */
#define SHOW_SDK_VERSION           (1) //Set "1" to display SDK version. Set "0" to hide.
#define SHOW_CRYPTO_LIB_VERSION    (1) //Set "1" to display Crypto library version. Set "0" to hide.

/**
 @brief SDK version
 */
#define SDK_VERSION  (0x100Bu)

/**
 @brief SDK release date
 */
#define SDK_DATE (0x20220211u)

/**
* @brief Authenticate S default device address
*/
#define AUTHS_DEFAULT_ADDRESS     (0x30)

/**
* @brief Authenticate S personalized identification
*/
#define AUTHS_VID     (0x0000) /*!  Set a 16-bit personalized value for vendor identification. */
#define AUTHS_PID1    (0x0017) /*!  Set a 16-bit personalized value for product identification */
#define AUTHS_PID2    (0x0014) /*!  Set a 16-bit personalized value for product identification */

/**
* @brief Number of expected devices on SWI/I2C bus for multiple enumeration
*/
#define DEVICES_ON_BUS                (8)     /*!  Set "8" for maximum number of devices connected bus. */

/**
* @brief Size of UID in bytes
*/
#define UID_SIZE_BYTE                 (12)

/**
* @brief Number of expected devices on I2C bus
*/
#define	CONFIG_I2C_MAX_COUNT		 (2)     /*!  Set "1" or "2" for maximum number of devices connected I2C bus. */
#if ((CONFIG_I2C_MAX_COUNT== 0))
#pragma message("Error: No device expected on I2C bus") 
#endif

/**
* @brief NVM Configuration
*/
#define	CONFIG_NVM_SIZE		          (5)     /*!  Set "1", "2" or "5" for different NVM size. */

#if (CONFIG_NVM_SIZE==1)
#define CONFIG_NVM_PAGE_COUNT  (32)   //32 pages of User NVM for 1Kbits NVM
#elif (CONFIG_NVM_SIZE==2)
#define CONFIG_NVM_PAGE_COUNT  (64)   //64 pages of User NVM for 2Kbits NVM
#elif (CONFIG_NVM_SIZE==5)
#define CONFIG_NVM_PAGE_COUNT  (164)   //164 pages of User NVM for 5.125Kbits NVM
#endif

#define	NVM_PAGE_SIZE_BYTE		       (4)     /*!  Each page in Authenticate S is 4 bytes. */

#if ((CONFIG_NVM_SIZE!=1) && (CONFIG_NVM_SIZE!=2) && (CONFIG_NVM_SIZE!=5))
#pragma message("Error: Invalid NVM size") 
#endif

/**
* @brief Authenticate S MAC Keys
*/
#define ENGINEERING_SAMPLE_MAC_KEY     (1) /*!  Set "1" to denote engineering sample MAC key. Set "0" to disable this key. */
#define PERSONALIZED_MAC_KEY           (0) /*!  Set "1" to denote personalized MAC key. Set "0" to disable this key. */

#if ((ENGINEERING_SAMPLE_MAC_KEY==1) && (PERSONALIZED_MAC_KEY==1))
#pragma message("Error: More than 1 MAC key defined") 
#endif

#if ((ENGINEERING_SAMPLE_MAC_KEY==0) && (PERSONALIZED_MAC_KEY==0))
#pragma message("Error: No MAC key defined") 
#endif

/**
* @brief Authenticate S ODC Public key
*/
#define ENGINEERING_SAMPLE_ODC         (1) /*!  Set "1" to use IFX engineering sample ODC public key verification. Set "0" to disable. */
#define PRODUCTIVE_ODC                 (0) /*!  Set "1" to use productive ODC public key verification. Set "0" to disable. */
#define PRODUCTIVE_ODC_OR_ES_ODC       (0) /*!  Set "1" to use first Productive then ES ODC to check for verification. Set "0" to allow use productive ODC only. */

#if ((ENGINEERING_SAMPLE_ODC==0) && (PRODUCTIVE_ODC==0) && (PRODUCTIVE_ODC_OR_ES_ODC==0))
#pragma message("Error: No ODC format selected") 
#endif

/**
* @brief Authenticate S feature passwords
*/
#define ENGINEERING_SAMPLE_PASSWORD    (1)  /*!  Set "1" to denote engineering sample password. Set "0" to disable. */
#define PRODUCTIVE_PASSWORD            (0) /*!  Set "1" to denote productive password. Set "0" to disable. */

#if ((ENGINEERING_SAMPLE_PASSWORD==1) && (PRODUCTIVE_PASSWORD==1))
#pragma message("Error: More than 1 password type selected") 
#endif

#if ((ENGINEERING_SAMPLE_PASSWORD==0) && (PRODUCTIVE_PASSWORD==0))
#pragma message("Error: No password selected") 
#endif

/**
* @brief Advance ODC verify configuration features
*/
#define USE_LD_COORDS                            (1) /*! Use Lopez Dahab Coordinates for ECC calculations */

/**
* @brief Authenticate S features
*/
#define HOST_AUTHENTICATION_FEATURE_SUPPORTED    (1)  /*!  Set "1" to denote host authentication is supported. Set "0" to disable. */
/**
* @brief Authenticate S host support
*/
#define ENABLE_HOST_SUPPORT         (0) /*!  Set "1" to enable host support mode. Set "0" disable host support mode. */


/**
* @brief System configuration
*/
#define ENABLE_DEBUG_PRINT          (1) /*! Set "1" to enable debug printing. Set "0" disable debug print. */
#define ENABLE_CRYPTO_DEBUG_PRINT   (0) /*! Set "1" to enable crypto printing. Set "0" disable print. */
#define ENABLE_INTERRUPT_MODE       (1) /*! Set "1" to enable SWI interrupt mode of ECC result. Set "0" disable SWI interrupt. */
#define ENABLE_POLLING_MODE         (0) /*! Set "1" to enable SWI polling mode of ECC result. Set "0" disable polling. */
#define ENABLE_USE_CASE             (1) /*! Set "1" to enable use case. Set "0" disable all use cases. */

#if ((ENABLE_INTERRUPT_MODE==0 ) && (ENABLE_POLLING_MODE==0))
#pragma message("Error: No SWI ECC response check")
#endif
#if ((ENABLE_INTERRUPT_MODE==1 ) && (ENABLE_POLLING_MODE==1))
#pragma message("Error: Both interrupt and polling mode enabled. Only enable one.")
#endif

#endif /* AUTHS_CONFIG_H_ */
