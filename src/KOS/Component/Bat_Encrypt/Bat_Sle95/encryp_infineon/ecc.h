#ifndef ECC_H_
#define ECC_H_
/***************************************************************************************
 * Copyright 2020 Infineon Technologies AG ( www.infineon.com ).                       *
 * All rights reserved.                                                                *
 *                                                                                     *
 * Licensed  Material-Property of Infineon Technologies AG.                            *
 * This software is made available solely pursuant to the terms of Infineon            *
 * Technologies AG agreement which governs its use. This code and the information      *
 * contained in it are proprietary and confidential to Infineon Technologies AG.       *
 * No person is allowed to copy, reprint, reproduce or publish any part of this code,  *
 * nor disclose its contents to others, nor make any use of it, nor allow or assist    *
 * others to make any use of it - unless by prior Written express authorization of     *
 * Infineon Technologies AG and then only to the extent authorized.                    *
 *                                                                                     *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,            *
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,           *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO       *
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,                 *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;         *
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY             *
 * WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR            *
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF              *
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                          *
 *                                                                                     *
 ***************************************************************************************/
/**
 * @file   ecc.h
 * @date   June, 2020
 * @brief  Implementation of ECC routine
 */
#include "auths_api.h"

#define ODC_BYTE_LEN			(25*2)
#define HASH_BYTE_LEN			(32)
#if ((HASH_BYTE_LEN!=32 ))
#pragma message("Error: Authenticate S only supports SHA256")
#endif
#define PUBLICKEY_BYTE_LEN		(22)
#define ECC_CHALLENGE_LEN		(21)
#define ECC_RESPONSE_LEN		(44)

typedef enum 
{
	SLE95_INIT,			             //初始化
	SLE95_READID,			         //读取ROMid和manid
	SLE95_GET_ODC,			         //获得ODC
    SLE95_GET_PUBKEY_KEY,            //获得公钥,得到hash，读取nvm存储的值
    SLE95_VERIFY_ODC,                //验证ODC
    SLE95_GENERATE_CHALLENGE,        //本地ECC计算 
    SLE95_CHALLENGE_RESPONSE,        //执行ECDSA 签名命令,比较ECC验签结果
    SLE95_END=10,                       //结束状态
    SLE95_ERROR,                     //错误状态
}SLE95_Status_enum_t;

uint16_t Sle95_Processor(void);
#endif /* ECC_H_ */
