#ifndef SLE95_H_
#define SLE95_H_
/***********************************************************************
 * Copyright (c)  2008 - 2021, dxtc Co.,Ltd .
 * All rights reserved.
 * Filename    : sle95_app.c
 * Description : 
 * Author(s)   : liujia
 * version     : V1.0
 * Modify date : 2023-02-23
 ***********************************************************************/
#include "auths_api.h"
#include "ecc.h"

void Read_UVP_Id(void);
void common_usecase(void);
void Read_UVP_Id(void);
void common_usecase(void);
void sample_demo(void);
uint8_t auths_init(void);
void auths_deinit(void);

#endif /* SLE95_H_ */
					

