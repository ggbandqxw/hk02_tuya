#ifndef PLATFORM_HELPER_H_
#define PLATFORM_HELPER_H_

/***************************************************************************************
 * Copyright 2020 Infineon Technologies AG ( www.infineon.com ).                       *
 * All rights reserved.                                                                *
 *                                                                                     *
 * Licensed  Material-Property of Infineon Technologies AG.                            *
 * This software is made available solely pursuant to the terms of Infineon            *
 * Technologies AG agreement which governs its use. This code and the information      *
 * contained in it are proprietary and confidential to Infineon Technologies AG.       *
 * No person is allowed to copy, reprint, reproduce or publish any part of this code,  *
 * nor disclose its contents to others, nor make any use of it, nor allow or assist    *
 * others to make any use of it - unless by prior Written express authorization of     *
 * Infineon Technologies AG and then only to the extent authorized.                    *
 *                                                                                     *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,            *
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,           *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO       *
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,                 *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;         *
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY             *
 * WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR            *
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF              *
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                          *
 *                                                                                     *
 ***************************************************************************************/
/**
 * @file   helper.h
 * @date   June, 2020
 * @brief  Definition of platform functions
 */
#include <stddef.h>
#include <stdint.h>
#include "auths_config.h"

/**
* @brief check specific conditions at runtime and cause a breakpoint to facilitate a debug.
* @param expr expression is false will cause an assert.
*/
#define ASSERT(expr) do {} while(0)

#define GPIO_LEVEL_HIGH (1u) /*!< GPIO logic high */
#define GPIO_LEVEL_LOW (0u)  /*!< GPIO logic low */

typedef enum
{
	RED = 1,
	BLUE,
	GREEN,
	YELLOW,
	MAGENTA,
	CYAN
}PRINT_COLOR;

#define HIGH_BYTE_ADDR(Addr) ((Addr >> 8u) & 0xFFu)
#define LOW_BYTE_ADDR(Addr) (Addr & 0xFFu)

#define POLY				(0x8005)
#define CRC_BYTE_LEN		(2)

/* CRC functions */
void crc16_init();
void crc16_free(void);
uint16_t crc16_gen(uint8_t *data_p, uint16_t len);

/* Hash functions */
void sha256(uint8_t *hash_value, const uint8_t *input_data, const uint32_t input_length);

/* RNG functions */
int gf2n_rand(uint32_t* erg);
void rand_byte(uint8_t *ub_rand, uint8_t ub_rand_len);

void delay_ms(uint32_t uw_time_ms);
void delay_us(volatile uint32_t ul_Loops);

/* print functions */
void print_info(void);
void handle_error(void);
char *errorstring(uint16_t error_code);
void PRINT_C(PRINT_COLOR COLOR, char *string,...);
void PRINT(const char *string,...);
void cls_screen(void);
void print_row_UID_table(uint8_t *UID);
void print_odc(uint32_t *ODC);
void print_puk(uint32_t *PUK);
void print_nvm_userpage(uint8_t *page, uint8_t count);
void print_nvm_TMpage(uint8_t *page, uint16_t count);
void print_nvm_page(uint8_t *page, uint8_t pagenumber);
void print_nvm_page_with_offset(uint8_t *data, uint8_t pagenumber, uint8_t offset);
void print_array(uint8_t* prgbBuf, uint16_t wLen);
void print_msg_digest(uint8_t* data, uint8_t len);
void PrintByteArrayHex(uint8_t *data,  uint16_t ArrayLen);

void PrintGf2nHex(uint32_t * data);

/* memory functions */
void *memset(void *s, int c, size_t n);
void *memcpy(void *dest, const void *src, size_t count);
uint8_t memchk(const void *mem1, const void *mem2, size_t n);
void convert8_to_32(uint32_t *dest, uint8_t *src, uint8_t n);
void convert32_to_8(uint8_t *dest, uint32_t *src, uint8_t n);

/* MCU related features */
void hw_sha256(uint8_t *in, uint8_t *out, const uint32_t len);
void hw_rng (uint8_t* random_byte, uint8_t len);

/* CRC-8 functions */
uint8_t CRC8(const uint8_t *data, uint32_t length);

#endif /* PLATFORM_HELPER_H_ */
