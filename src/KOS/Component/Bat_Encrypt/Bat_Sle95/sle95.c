/***********************************************************************
 * Copyright (c)  2008 - 2021, dxtc Co.,Ltd .
 * All rights reserved.
 * Filename    : sle95_app.c
 * Description : 
 * Author(s)   : liujia
 * version     : V1.0
 * Modify date : 2023-02-23
 ***********************************************************************/
#include "auths_config.h"
#include "auths_api.h"
#include "auths_status.h"
#include "ecc.h"
#include "host_auth.h"
#include "helper.h"
#include "platform.h"
#include "usecase.h"
#include "usecase_nvm.h"

#define LOOP_COUNT                 (3) //Set number of loop test

extern AuthS_Capability auths_capability;
AuthS_Capability *auths_capability_handle = &auths_capability;

AuthS_Interface auths_device;
AuthS_Interface *auths_device_handle = &auths_device;
extern AuthS_Enumeration auths_enumeration;

/**
 * @brief Initialization the interface
 * @param interface Provide the required interface parameter
 * */
static uint16_t init_interface(INTERFACE_TYPE interface){

	uint16_t ret;
	switch(interface){
		case I2C:{
			auths_device.active_interface = (INTERFACE_TYPE) I2C;
			auths_device.interface.i2c.frequency = I2C_FREQ;
			// auths_device.interface.i2c.SDA = mI2C_SDA;
			// auths_device.interface.i2c.SCL = mI2C_SCL;
			// auths_device.gpo = GPIO_GPO_I2C;
			ret = platform_i2c_init();
			if(ret!= SDK_SUCCESS){
				return ret;
			}
			break;
		}
		case SWI:{
			break;
		}
		case UNDEFINED_INTERFACE:
			return EXE_E_INTERFACE;
	}
	auths_capability_handle->auths_alt_rng = NULL;
	auths_capability_handle->auths_alt_sha = NULL;

	//setup device attributes
	auths_capability_handle->device_attribute.gpo_trigger_authentication=unsupported;
	auths_capability_handle->device_attribute.gpo_trigger_lsc2=unsupported;

	return SDK_SUCCESS;
}

/** @brief I2C example code */
void Read_UVP_Id(void){

	uint16_t ret;
	uint8_t uid[12];
	uint8_t vid[2];
	uint8_t pid[2];
	printf("read id\r\n");
	ret = auths_exe_read_uid(uid, vid, pid);
	if (EXE_SUCCESS != ret) {
		PRINT("Error: Read UID from I2C Device register, ret=0x%x\r\n", ret);
	} else {
		print_row_UID_table(uid);
	}
	memcpy(auths_capability_handle->uid, uid, 12);
}


//Common use case routines
void common_usecase(void){
	uint16_t ret;
	HOST_AUTH_STATUS status;

	auths_is_host_auth_done(&status);
	if(host_auth_not_done==status){
		ret = auths_exe_host_authentication();
		if(ret != APP_HA_SUCCESS){
			PRINT("Error: Failed Host Auth, ret=0x%X\r\n", ret);
		}else{
			PRINT("Result: Host Auth Passed\r\n");			
		}
	}

	//Get UID and rearrange for ECC
	Read_UVP_Id();

	//Verify ECC Key 1
	ret = auths_exe_ecc(0, MAC_GROUP_0);
	if(EXE_SUCCESS!= ret){
		PRINT("Error: Failed ECC1, ret=0x%X\r\n", ret);
	}else{
		PRINT("Result: ECC1 Passed.\r\n");
	}

	//Verify ECC Key 2
	ret = auths_exe_ecc(1, MAC_GROUP_0);
	if(EXE_SUCCESS!= ret){
		PRINT("Error: Failed ECC2, ret=0x%X\r\n", ret);
	}else{
		PRINT("Result: ECC2 Passed.\r\n");
	}
}

void sample_demo(void)
{
	uint16_t ret;
	uint16_t loop=LOOP_COUNT;
	uint16_t loop_count=1;

	print_info();

	while(loop){
		memset(auths_device_handle, 0, sizeof(AuthS_Interface));
		memset(auths_capability_handle, 0, sizeof(AuthS_Capability));

		PRINT("+++++ I2C CONFIGURATION +++++\r\n\r\n");
		ret = init_interface(I2C);
		if(SDK_SUCCESS != ret){
			PRINT("Error: I2C interface, ret=0x%X\r\n", ret);
			ASSERT(0);
			break;
		}
		delay_ms(10);
		ret = auths_init_sdk(auths_device, auths_capability_handle);
		if(SDK_SUCCESS != ret){
			PRINT("Error: I2C sdk failed, ret=0x%X\r\n", ret);
		}


		common_usecase();		//These uses cases are independent of the device interface
		ret = auths_deinit_sdk();
		if (SDK_SUCCESS != ret) {
			PRINT("Error: SDK, ret=0x%X\r\n", ret);
		}
		auths_exe_power_down();
		platform_i2c_free();
		loop--;
		PRINT("Loop count=%d\r\n\n", loop_count++);
	}

    PRINT("==The end== \r\n");
}
//返回值为0表示成功，非0表示为失败
uint8_t auths_init(void)
{
	uint16_t ret;
	HOST_AUTH_STATUS status;

	memset(auths_device_handle, 0, sizeof(AuthS_Interface));
	memset(auths_capability_handle, 0, sizeof(AuthS_Capability));

	PRINT("+++++ I2C CONFIGURATION +++++\r\n\r\n");
	ret = init_interface(I2C);
	if(SDK_SUCCESS != ret){
		PRINT("Error: I2C interface, ret=0x%X\r\n", ret);
		ASSERT(0);
	}
	delay_ms(10);
	ret = auths_init_sdk(auths_device, auths_capability_handle);
	if(SDK_SUCCESS != ret){
		PRINT("Error: I2C sdk failed, ret=0x%X\r\n", ret);
	}
	auths_is_host_auth_done(&status);
	if(host_auth_not_done==status){
		ret = auths_exe_host_authentication();
		if(ret != APP_HA_SUCCESS){
			PRINT("Error: Failed Host Auth, ret=0x%X\r\n", ret);
		}else{
			PRINT("Result: Host Auth Passed\r\n");			
		}
	}
	return ret;
}

void auths_deinit(void)
{
	uint16_t ret;
	ret = auths_deinit_sdk();
	if (SDK_SUCCESS != ret) {
		PRINT("Error: SDK, ret=0x%X\r\n", ret);
	}
	auths_exe_power_down();
	platform_i2c_free();
}


					




	

	
