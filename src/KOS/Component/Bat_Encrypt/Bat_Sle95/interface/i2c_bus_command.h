/***************************************************************************************
 * Copyright 2020 Infineon Technologies AG ( www.infineon.com ).                       *
 * All rights reserved.                                                                *
 *                                                                                     *
 * Licensed  Material-Property of Infineon Technologies AG.                            *
 * This software is made available solely pursuant to the terms of Infineon            *
 * Technologies AG agreement which governs its use. This code and the information      *
 * contained in it are proprietary and confidential to Infineon Technologies AG.       *
 * No person is allowed to copy, reprint, reproduce or publish any part of this code,  *
 * nor disclose its contents to others, nor make any use of it, nor allow or assist    *
 * others to make any use of it - unless by prior Written express authorization of     *
 * Infineon Technologies AG and then only to the extent authorized.                    *
 *                                                                                     *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,            *
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,           *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO       *
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,                 *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;         *
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY             *
 * WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR            *
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF              *
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                          *
 *                                                                                     *
 ***************************************************************************************/
/**
 * @file   i2c_bus_command.h
 * @date   June, 2020
 * @brief  Implementation of I2C bus command
 */
#ifndef BUS_COMMAND_I2C_H_
#define BUS_COMMAND_I2C_H_

#include <stdint.h>
#include "auths_config.h"

typedef enum I2C_CMD_{
BRES    =   0x06,
PDWN    =   0x12,
WD	    =   0x15,
RD      =   0x16,
WDA	    =	0x18,
AVREQ   =	0xB2, //Apply to ENABLE_HOST_SUPPORT 
AREQ1   =   0xB3, //Apply to ENABLE_HOST_SUPPORT 
ARES1   =   0xB4, //Apply to ENABLE_HOST_SUPPORT 
ECCS1   =	0xC1,
ECCS2   =	0xC2,
ECCC	=	0xC3,
ECCR	=	0xC4,
MACS	=	0xD2,
MACR	=	0xD4,
MACK	=	0xD8,
MACCR1	=	0xD9,
MACCR2	=	0xDA,
MACCR3	=	0xDB,
MACCR4	=	0xDC,
MACCR5  =   0xDD,
HRREQ	=	0xE0,
HRRES	=	0xE1,
DRRES	=	0xE2,
HREQ1	=	0xE3,
DRES1	=	0xE4,
}I2C_CMD;

uint16_t i2c_bus_command_BRES(uint8_t i2c_address);
uint16_t i2c_bus_command_PDWN(uint8_t device_address);
uint16_t i2c_bus_command_WD(uint8_t device_address, uint16_t uwAddr,uint8_t data_len, uint8_t *ub_data);
uint16_t i2c_bus_command_RD(uint8_t device_address, uint16_t uwAddr, uint8_t rd_len, uint8_t *ub_data);
uint16_t i2c_bus_command_WDA(uint8_t device_address, uint16_t ubAddr);

uint16_t i2c_bus_command_AVREQ(uint8_t device_address, uint8_t *ub_data);
uint16_t i2c_bus_command_AREQ1(uint8_t device_address, uint8_t *ub_data);
uint16_t i2c_bus_command_ARES1(uint8_t device_address, uint8_t *ub_data);

uint16_t i2c_bus_command_ECCS1(uint8_t device_address);
uint16_t i2c_bus_command_ECCS2(uint8_t device_address);
uint16_t i2c_bus_command_ECCC(uint8_t device_address, uint8_t *ub_data);
uint16_t i2c_bus_command_ECCR(uint8_t device_address, uint8_t *ub_data);

uint16_t i2c_bus_command_MACS(uint8_t device_address, uint16_t uw_mac_addr);
uint16_t i2c_bus_command_MACR(uint8_t device_address, uint8_t* ub_data);
uint16_t i2c_bus_command_MACK(uint8_t device_address, uint8_t* ub_data);

uint16_t i2c_bus_command_MACCR1(uint8_t device_address, uint8_t* ub_data);
uint16_t i2c_bus_command_MACCR2(uint8_t device_address, uint8_t* ub_data);
uint16_t i2c_bus_command_MACCR3(uint8_t device_address, uint8_t* ub_data);
uint16_t i2c_bus_command_MACCR4(uint8_t device_address, uint8_t* ub_data);
uint16_t i2c_bus_command_MACCR5(uint8_t device_address, uint8_t* ub_data);

uint16_t i2c_bus_command_HRREQ(uint8_t device_address);
uint16_t i2c_bus_command_HRRES(uint8_t device_address, uint8_t *ub_data);

uint16_t i2c_bus_command_DRRES(uint8_t device_address, uint8_t *ub_data);
uint16_t i2c_bus_command_HREQ1(uint8_t device_address, uint8_t *ub_data);
uint16_t i2c_bus_command_DRES1(uint8_t device_address, uint8_t *ub_data);

#endif //BUS_COMMAND_I2C_H_
