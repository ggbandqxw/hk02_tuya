/***************************************************************************************
 * Copyright 2020 Infineon Technologies AG ( www.infineon.com ).                       *
 * All rights reserved.                                                                *
 *                                                                                     *
 * Licensed  Material-Property of Infineon Technologies AG.                            *
 * This software is made available solely pursuant to the terms of Infineon            *
 * Technologies AG agreement which governs its use. This code and the information      *
 * contained in it are proprietary and confidential to Infineon Technologies AG.       *
 * No person is allowed to copy, reprint, reproduce or publish any part of this code,  *
 * nor disclose its contents to others, nor make any use of it, nor allow or assist    *
 * others to make any use of it - unless by prior Written express authorization of     *
 * Infineon Technologies AG and then only to the extent authorized.                    *
 *                                                                                     *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,            *
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,           *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO       *
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,                 *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;         *
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY             *
 * WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR            *
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF              *
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                          *
 *                                                                                     *
 ***************************************************************************************/
/**
 * @file   gpo.c
 * @date   June, 2020
 * @brief  Implementation of GPO routine
 */
#include "gpo.h"
#include "auths_api.h"
#include "auths_config.h"
#include "auths_status.h"
#include "helper.h"

/**
* @brief Set GPO config
* @param gpo_value GPO value
*/
uint16_t auths_set_gpo_config(GPO_status gpo_value){
	uint8_t ub_data;
    uint16_t ret;

    if(gpo_value == strongHigh){
	    ub_data = (1<<BIT_GPO_HDEN)|(1<<BIT_GPO_OUT);
    }else if(gpo_value == strongLow){
        ub_data = (1<<BIT_GPO_LDEN);
    }else if(gpo_value == weakHigh){
        ub_data = (1<<BIT_GPO_PUEN);
    }else if(gpo_value == weakLow){
        ub_data = (1<<BIT_GPO_PDEN);
    }else if(gpo_value == tristate){
        ub_data = 0;
    }else{
        ret = APP_GPO_E_INPUT;
        return ret;
    }

    ret = Intf_WriteRegister(AUTHS_SFR_GPO_CTRL, &ub_data, 1);
    if(SDK_INTERFACE_SUCCESS!=ret){
        return ret;
    }

    delay_ms(1);
    return APP_GPO_SUCCESS;
}

/**
* @brief Get GPO value
* @param gpo_config GPO configuration
*/
uint16_t auths_get_gpo_config(GPO_status *gpo_config){

	uint16_t ret;
	uint8_t ub_data;

    ret = Intf_ReadRegister(AUTHS_SFR_GPO_CTRL_STS, &ub_data, 1);
    if(SDK_INTERFACE_SUCCESS != ret){
        return ret;
    }

    if(((ub_data >> BIT_GPO_HDEN) & 0x01) == 0x01){
     	 *gpo_config = strongHigh;
    }
    else if(((ub_data >> BIT_GPO_LDEN) & 0x01) == 0x01){
		 *gpo_config = strongLow;
	}
    else if(((ub_data >> BIT_GPO_PUEN) & 0x01) == 0x01){
		 *gpo_config = weakHigh;
	}
    else if(((ub_data >> BIT_GPO_PDEN) & 0x01) == 0x01){
		 *gpo_config = weakLow;
	}
    else if(ub_data == 0){
 		 *gpo_config = tristate;
 	}
	else{
        return APP_GPO_E_CTRL_STS;
    }

    return APP_GPO_SUCCESS;

}

/**
* @brief Get GPO value
* @param ub_gpo_value GPO value
*/
uint16_t auths_get_gpo_level(uint8_t *ub_gpo_value){

	uint16_t ret;
	uint8_t ub_data;

    ret = Intf_ReadRegister(AUTHS_SFR_GPO_CTRL_STS, &ub_data, 1);
    if(SDK_INTERFACE_SUCCESS != ret){
        return ret;
    }    

	// high
	if(((ub_data & 0x2f) == 0x22) || (ub_data == 0x80) || (ub_data == 0x82)){
        *ub_gpo_value = 1;
    }

	// low
	else if(((ub_data & 0x1f) == 0x10) || (ub_data == 0x40) || (ub_data == 0x42)){
        *ub_gpo_value = 0;
    }
	else{
        return APP_GPO_E_CTRL_STS;
    }

    return APP_GPO_SUCCESS;
}
