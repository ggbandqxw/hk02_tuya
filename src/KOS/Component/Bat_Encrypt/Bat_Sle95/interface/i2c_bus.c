/***************************************************************************************
 * Copyright 2020 Infineon Technologies AG ( www.infineon.com ).                       *
 * All rights reserved.                                                                *
 *                                                                                     *
 * Licensed  Material-Property of Infineon Technologies AG.                            *
 * This software is made available solely pursuant to the terms of Infineon            *
 * Technologies AG agreement which governs its use. This code and the information      *
 * contained in it are proprietary and confidential to Infineon Technologies AG.       *
 * No person is allowed to copy, reprint, reproduce or publish any part of this code,  *
 * nor disclose its contents to others, nor make any use of it, nor allow or assist    *
 * others to make any use of it - unless by prior Written express authorization of     *
 * Infineon Technologies AG and then only to the extent authorized.                    *
 *                                                                                     *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,            *
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,           *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO       *
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,                 *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;         *
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY             *
 * WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR            *
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF              *
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                          *
 *                                                                                     *
 ***************************************************************************************/
/**
 * @file   i2c.c
 * @date   June, 2020
 * @brief  Implementation of i2c bus commands protocol
 */

#include "helper.h"
#include "auths_status.h"
#include "auths_config.h"
#include "i2c_bus.h"

 //todo System porting required: implements platform I2C, if required.

/**
* @brief I2C Send and receive data
* @param inst_i2c i2c parameters
*/
uint16_t I2C_SendAndReceive(IIC_Struct *inst_i2c) {
	uint16_t ret=INF_I2C_E_INPUT;
 //todo System porting required: implements platform I2C, if required.	
 	if (0 != Device_Write(VHW_IIC_SLE95, inst_i2c->BufferPtr, inst_i2c->SendByteCnt, inst_i2c->AddrOfSlave)) {
		ret = INF_I2C_E_NACK;
	}

	if (0 != Device_Read(VHW_IIC_SLE95, inst_i2c->BufferPtr, inst_i2c->RecvByteCnt, inst_i2c->AddrOfSlave)) {
		ret =  INF_I2C_E_NACK;
	}

	// check CRC
	uint16_t len = inst_i2c->RecvByteCnt-2;

	volatile uint16_t uw_crc = *(inst_i2c->BufferPtr+len) | *((inst_i2c->BufferPtr+(len+1)))<< 8;
	if(uw_crc != crc16_gen(inst_i2c->BufferPtr, (inst_i2c->RecvByteCnt)-CRC_BYTE_LEN))
	{
		ret = INF_I2C_E_CRC;
	}
	else{
		ret = INF_I2C_SUCCESS;
	}
	return ret;
}

/**
* @brief I2C Send data only
* @param inst_i2c i2c parameters
*/
uint16_t I2C_SendOnly(IIC_Struct *inst_i2c) {
	uint16_t ret=INF_I2C_E_INPUT;
 //todo System porting required: implements platform I2C, if required.
	uint16_t timeout_ms=50;
	if (0 != Device_Write(VHW_IIC_SLE95, inst_i2c->BufferPtr, inst_i2c->SendByteCnt, inst_i2c->AddrOfSlave)) {
		ret = INF_I2C_E_NACK;
	} else {
		ret = INF_I2C_SUCCESS;
	}
	return ret;
}

