/***************************************************************************************
 * Copyright 2020 Infineon Technologies AG ( www.infineon.com ).                       *
 * All rights reserved.                                                                *
 *                                                                                     *
 * Licensed  Material-Property of Infineon Technologies AG.                            *
 * This software is made available solely pursuant to the terms of Infineon            *
 * Technologies AG agreement which governs its use. This code and the information      *
 * contained in it are proprietary and confidential to Infineon Technologies AG.       *
 * No person is allowed to copy, reprint, reproduce or publish any part of this code,  *
 * nor disclose its contents to others, nor make any use of it, nor allow or assist    *
 * others to make any use of it - unless by prior Written express authorization of     *
 * Infineon Technologies AG and then only to the extent authorized.                    *
 *                                                                                     *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,            *
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,           *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO       *
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,                 *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;         *
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY             *
 * WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR            *
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF              *
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                          *
 *                                                                                     *
 ***************************************************************************************/
/**
 * @file   register.h
 * @date   June, 2020
 * @brief  List ofAuthenticate S register
 */
#ifndef INTERFACE_REGISTER_H_
#define INTERFACE_REGISTER_H_

#include "auths_config.h"

#define  UID_BYTE_LEN			(12u)

/* Device  Register */    
// NVM User Address Space (0x0000-0x001F)
#define AUTHS_USR_NVM_BASE				(uint16_t)0x0000u	// Base address of User NVM


// NVM IFX Address Space (0x00A4-0x00DB)
#define AUTHS_IFX_NVM_ADDR				(uint16_t)(AUTHS_USR_NVM_BASE + 0xA4u)	// Base address of IFX NVM
#define AUTHS_IFX_NVM_END				(uint16_t)(AUTHS_USR_NVM_BASE + 0xDBu)	// End address of IFX NVM
//#define AUTHS_IFX_NVM_PAGE_SIZE		    (uint16_t)(AUTHS_IFX_NVM_END-AUTHS_IFX_NVM_BASE+1u) // Page size of IFX NVM

// ODC 
#define AUTHS_ODC1_ADDR				(uint16_t)(AUTHS_USR_NVM_BASE + 0xA4u) // ODC1 base address
#define AUTHS_ODC1_END				(uint16_t)(AUTHS_USR_NVM_BASE + 0xB0u) // ODC1 end address,first two bytes
#define AUTHS_ODC2_ADDR				(uint16_t)(AUTHS_USR_NVM_BASE + 0xBEu) // ODC2 base address
#define AUTHS_ODC2_END				(uint16_t)(AUTHS_USR_NVM_BASE + 0xCAu) // ODC2 end address,first two bytes

// AUTH PKey
#define AUTHS_AUTH_PK1_ADDR			(uint16_t)(AUTHS_USR_NVM_BASE + 0xB0u) // Auth PKey1 base address,start from byte2
#define AUTHS_AUTH_PK1_END			(uint16_t)(AUTHS_USR_NVM_BASE + 0xB5u) // Auth Pkey1 End address
#define AUTHS_AUTH_PK2_ADDR			(uint16_t)(AUTHS_USR_NVM_BASE + 0xCAu) // Auth PKey2 base address,start from byte2
#define AUTHS_AUTH_PK2_END			(uint16_t)(AUTHS_USR_NVM_BASE + 0xCFu) // Auth Pkey2 End address

// Hash ODC PKey
#define AUTHS_HASH1_ADDR	(uint16_t)(AUTHS_USR_NVM_BASE + 0xB6u) // Hash1 base address
#define AUTHS_HASH1_END		(uint16_t)(AUTHS_USR_NVM_BASE + 0xBDu) // Hash1 End address
#define AUTHS_HASH2_ADDR	(uint16_t)(AUTHS_USR_NVM_BASE + 0xD0u) // Hash2 base address
#define AUTHS_HASH2_END		(uint16_t)(AUTHS_USR_NVM_BASE + 0xD7u) // Hash2 End address

// UID
#define AUTHS_UID_ADDR		(uint16_t)(AUTHS_USR_NVM_BASE + 0xD8u) // UID base address
#define AUTHS_UID_END		(uint16_t)(AUTHS_USR_NVM_BASE + 0xDBu) // UID End address

#define AUTHS_UID_SIZE		(uint16_t)((AUTHS_UID_END-AUTHS_UID_ADDR + 1u)*4u) //
#define AUTHS_UID_PAGE_SIZE	(uint16_t)(AUTHS_UID_END-AUTHS_UID_ADDR + 1u) //

// Life Span Counter
#define AUTHS_LSC_BUF_1		(uint16_t)(AUTHS_USR_NVM_BASE + 0xE0u) // Life Span Counter Buffer1
#define AUTHS_LSC_BUF_2		(uint16_t)(AUTHS_USR_NVM_BASE + 0xE1u) // Life Span Counter Buffer2
#define AUTHS_LSC_BUF_3		(uint16_t)(AUTHS_USR_NVM_BASE + 0xE8u) // Life Span Counter Buffer3
#define AUTHS_LSC_BUF_4		(uint16_t)(AUTHS_USR_NVM_BASE + 0xE9u) // Life Span Counter Buffer4
#define AUTHS_LSC_1			(uint16_t)(AUTHS_USR_NVM_BASE + 0xE4u) // Life Span Counter1
#define AUTHS_LSC_2			(uint16_t)(AUTHS_USR_NVM_BASE + 0xE5u) // Life Span Counter2
#define AUTHS_LSC_3		    (uint16_t)(AUTHS_USR_NVM_BASE + 0xECu) // Life Span Counter3
#define AUTHS_LSC_4		    (uint16_t)(AUTHS_USR_NVM_BASE + 0xEDu) // Life Span Counter4

// Specific Function Register (SFR)(0x4000-0x40FF)
#define AUTHS_SFR_BASE					(uint16_t)0x4000u	// Base address of Authenticate S SFR
#define AUTHS_SFR_END					(uint16_t)(AUTHS_SFR_BASE + 0xFFu)	// End address of Authenticate S SFR
#define AUTHS_SFR_USER_NVM_LOCK_1		(uint16_t)(AUTHS_SFR_BASE + 0x00u)	// User NVM Lock Control Reg 1 (Write-Only)
#define AUTHS_SFR_USER_NVM_LOCK_2		(uint16_t)(AUTHS_SFR_BASE + 0x01u)	// User NVM Lock Control Reg 2 (Write-Only)
#define AUTHS_SFR_USER_NVM_LOCK_3		(uint16_t)(AUTHS_SFR_BASE + 0x02u)	// User NVM Lock Control Reg 3 (Write-Only)
#define AUTHS_SFR_USER_NVM_LOCK_4		(uint16_t)(AUTHS_SFR_BASE + 0x03u)	// User NVM Lock Control Reg 4 (Write-Only)
#define AUTHS_SFR_USER_NVM_LOCK_5		(uint16_t)(AUTHS_SFR_BASE + 0x04u)	// User NVM Lock Control Reg 5 (Write-Only)
#define AUTHS_SFR_USER_NVM_LOCK_6		(uint16_t)(AUTHS_SFR_BASE + 0x05u)	// User NVM Lock Control Reg 6 (Write-Only)
#define AUTHS_SFR_USER_NVM_LOCK_STS_1	(uint16_t)(AUTHS_SFR_BASE + 0x06u)	// User NVM Lock Status Reg 1 (Read-Only)
#define AUTHS_SFR_USER_NVM_LOCK_STS_2	(uint16_t)(AUTHS_SFR_BASE + 0x07u)	// User NVM Lock Status Reg 2 (Read-Only)
#define AUTHS_SFR_USER_NVM_LOCK_STS_3	(uint16_t)(AUTHS_SFR_BASE + 0x08u)	// User NVM Lock Status Reg 3 (Read-Only)
#define AUTHS_SFR_USER_NVM_LOCK_STS_4	(uint16_t)(AUTHS_SFR_BASE + 0x09u)	// User NVM Lock Status Reg 4 (Read-Only)
#define AUTHS_SFR_USER_NVM_LOCK_STS_5	(uint16_t)(AUTHS_SFR_BASE + 0x0Au)	// User NVM Lock Status Reg 5 (Read-Only)
#define AUTHS_SFR_USER_NVM_LOCK_STS_6	(uint16_t)(AUTHS_SFR_BASE + 0x0Bu)	// User NVM Lock Status Reg 6 (Read-Only)
#define AUTHS_SFR_LSC_KILL_CTRL		    (uint16_t)(AUTHS_SFR_BASE + 0x0Cu)	// LifeSpanCounter Kill Control (Write-Only)
#define AUTHS_SFR_LSC_KILL_CONF_1	    (uint16_t)(AUTHS_SFR_BASE + 0x0Du)	// LifeSpanCounter Kill Configure1 (Write-Only)
#define AUTHS_SFR_LSC_KILL_CONF_2	    (uint16_t)(AUTHS_SFR_BASE + 0x0Eu)	// LifeSpanCounter Kill Configure2 (Write-Only)
#define AUTHS_SFR_LSC_KILL_FEAT_STS_1	(uint16_t)(AUTHS_SFR_BASE + 0x0Fu)	// LifeSpanCounter Kill Feature Status1 (Read-Only)
#define AUTHS_SFR_LSC_KILL_FEAT_STS_2	(uint16_t)(AUTHS_SFR_BASE + 0x10u)	// LifeSpanCounter Kill Feature Status2 (Read-Only)
#define AUTHS_SFR_LSC_KILL_ACT_STS_1	(uint16_t)(AUTHS_SFR_BASE + 0x11u)	// LifeSpanCounter Kill Act Status1 (Read-Only)
#define AUTHS_SFR_LSC_KILL_ACT_STS_2	(uint16_t)(AUTHS_SFR_BASE + 0x12u)	// LifeSpanCounter Kill Act Status2 (Read-Only)
#define AUTHS_SFR_BUSY_STS				(uint16_t)(AUTHS_SFR_BASE + 0x14u)	// Busy Status (Read-Only)
#define	AUTHS_SFR_DEVICE_ADDR			(uint16_t)(AUTHS_SFR_BASE + 0x15u)	// Device Address (Read/Write)
#define AUTHS_SFR_LOCK_STS				(uint16_t)(AUTHS_SFR_BASE + 0x16u)	// Lock Status Reg (Read-Only)
#define AUTHS_SFR_LSC_DEC_VAL_L			(uint16_t)(AUTHS_SFR_BASE + 0x17u)	// LifeSpanCounter Decrement Low (Read/Write)
#define AUTHS_SFR_LSC_DEC_VAL_H		    (uint16_t)(AUTHS_SFR_BASE + 0x18u)	// LifeSpanCounter Decrement High (Read/Write)
#define	AUTHS_SFR_SWI_CONF_0			(uint16_t)(AUTHS_SFR_BASE + 0x20u)	// SWI Configure0 (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_CONF_1			(uint16_t)(AUTHS_SFR_BASE + 0x21u)	// SWI Configure1 (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_STS_0		        (uint16_t)(AUTHS_SFR_BASE + 0x23u)	// SWI Status0 Word Count (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_STS_1		        (uint16_t)(AUTHS_SFR_BASE + 0x24u)	// SWI Status1 Error Count (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_STS_2		        (uint16_t)(AUTHS_SFR_BASE + 0x25u)	// SWI Status2 Last Error Code (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_INT_0		        (uint16_t)(AUTHS_SFR_BASE + 0x26u)	// SWI Interrupt Register 0 (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_INT_1		        (uint16_t)(AUTHS_SFR_BASE + 0x27u)	// SWI Interrupt Register 1 (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_DADR0_L		    (uint16_t)(AUTHS_SFR_BASE + 0x28u)	// SWI Device Address Register0_L (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_DADR0_H		    (uint16_t)(AUTHS_SFR_BASE + 0x29u)	// SWI Device Address Register0_H (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_RD_LEN		    (uint16_t)(AUTHS_SFR_BASE + 0x2Au)	// SWI Read Data Length (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_SWI_WD_LEN		    (uint16_t)(AUTHS_SFR_BASE + 0x2Bu)	// SWI Write Data Length (Read-Only for I2C, Read/Write for SWI)
#define AUTHS_SFR_GPO_CTRL		        (uint16_t)(AUTHS_SFR_BASE + 0x40u)	// GPO Control Register (Write-Only)
#define AUTHS_SFR_GPO_CTRL_STS		    (uint16_t)(AUTHS_SFR_BASE + 0x41u)	// GPO Control Status Register (Read-Only)


#define HiByte(din_16) ((din_16 >> 8) & 0xff)
#define LoByte(din_16) ((din_16) & 0xff)

// Flag
#define BUSY	1u
#define	NOTBUSY	0u

// AUTHS_SFR_USER_NVM_LOCK_1 bit mapping
#define BIT_NVM_LOCK_PAGE1_3			 0
#define BIT_NVM_LOCK_PAGE4_7			 1
#define BIT_NVM_LOCK_PAGE8_11			 2
#define BIT_NVM_LOCK_PAGE12_15			 3
#define BIT_NVM_LOCK_PAGE16_19			 4
#define BIT_NVM_LOCK_PAGE20_23			 5
#define BIT_NVM_LOCK_PAGE24_27			 6
#define BIT_NVM_LOCK_PAGE28_31			 7

// AUTHS_SFR_LSC_KILL_CTRL bit mapping
#define BIT_SET_LSC_1_ZERO			 0
#define BIT_DEC_LSC_1				 1
#define BIT_SET_LSC_2_ZERO			 2
#define BIT_DEC_LSC_2				 3
#define BIT_SET_LSC_3_ZERO			 4
#define BIT_DEC_LSC_3				 5
#define BIT_SET_LSC_4_ZERO			 6
#define BIT_DEC_LSC_4				 7

// AUTHS_SFR_LSC_KILL_CONF_1 bit mapping
#define BIT_LSC_1_ZERO_EN			 0
#define BIT_LSC_1_EN				 1
#define BIT_LSC_2_ZERO_EN			 2
#define BIT_LSC_2_EN				 3
#define BIT_LSC_3_ZERO_EN			 4
#define BIT_LSC_3_EN				 5
#define BIT_LSC_4_ZERO_EN			 6
#define BIT_LSC_4_EN				 7

// AUTHS_SFR_LSC_KILL_CONF_2 bit mapping
#define BIT_LSC_1_AU_EN				 2
#define BIT_KILL_EN					 6

// AUTHS_SFR_LSC_KILL_FEAT_STS_1 bit mapping
#define BIT_LSC_1_ZERO_EN_STS		0
#define BIT_LSC_1_EN_STS			1
#define BIT_LSC_2_ZERO_EN_STS		2
#define BIT_LSC_2_EN_STS			3
#define BIT_LSC_3_ZERO_EN_STS		4
#define BIT_LSC_3_EN_STS			5
#define BIT_LSC_4_ZERO_EN_STS		6
#define BIT_LSC_4_EN_STS			7

// AUTHS_SFR_LSC_KILL_FEAT_STS_2 bit mapping
#define BIT_LSC_1_AU_EN_STS			2
#define BIT_KILL_FEATURE_EN_STS		6

// AUTHS_SFR_LSC_KILL_ACT_STS_1 bit mapping
#define BIT_I2C_RD_CRC_ERR_STS		0
#define BIT_KILL_IND				6
#define BIT_NVM_ERR_STS				7

// AUTHS_SFR_LSC_KILL_ACT_STS_2 bit mapping
#define BIT_LSC_DEC_1_STS			0
#define BIT_LSC_DEC_2_STS			1
#define BIT_LSC_DEC_3_STS			2
#define BIT_LSC_DEC_4_STS			3
#define BIT_LSC1_RESET_STS			4
#define BIT_LSC2_RESET_STS			5
#define BIT_LSC3_RESET_STS			6
#define BIT_LSC4_RESET_STS			7

// AUTHS_SFR_BUSY_STS bit mapping
#define BIT_NVM_BUSY					(0)
#if (ENABLE_HOST_SUPPORT ==1)
#define BIT_HSM_TAGB_STS				(5)
#endif
#define BIT_RANDOM_NUM_BUSY				(6)
#define BIT_AUTH_MAC_BUSY				(7)


// AUTHS_SFR_SWI_CONFIG_1
#define BIT_DEV_INT_EN				0


// AUTHS_SFR_SWI_INT_0
#define BIT_ECC_DONE_INT_EN			0
#define BIT_DEC_LFSP_END_INT_EN		7


// AUTHS_SFR_SWI_INT_1
#define BIT_ECC_DONE_INT_STS		0
#define BIT_DEC_LFSP_END_INT_STS	7

// AUTHS_SFR_LOCK_STS
#define BIT_IFX_KILL_CONF			 0
#define BIT_IFX_AUTO_KILL_CONF		 1
#define BIT_HOSTAUTH_CONF			 2
#define BIT_LSC_RESET_CONF			 3
#define BIT_CHIP_LOCK				 4
#define BIT_HOST_SUP_MODE			 5
#define BIT_IFX_USERNVM_RESET_CONFIG 6

// AUTHS_SFR_GPO_CNTRL
#define BIT_GPO_OUT					 1
#define BIT_GPO_LDEN				 4
#define BIT_GPO_HDEN				 5
#define BIT_GPO_PDEN				 6
#define BIT_GPO_PUEN				 7

typedef enum REGISTER_ACCESS_{
    ACCESS_AFTER_HOST_AUTH, 
    ACCESS_ALWAYS 
} Register_Access;


// RegAddr_LSC_KILL_CTRL bit mapping
#define BIT_SET_LSC_1_ZERO			 0
#define BIT_DEC_LSC_1				 1
#define BIT_SET_LSC_2_ZERO			 2
#define BIT_DEC_LSC_2				 3
#define BIT_SET_LSC_3_ZERO			 4
#define BIT_DEC_LSC_3				 5
#define BIT_SET_LSC_4_ZERO			 6
#define BIT_DEC_LSC_4				 7

// RegAddr_LSC_KILL_CONF_1
#define BIT_LSC_1_ZERO_EN			 0
#define BIT_LSC_1_EN				 1
#define BIT_LSC_2_ZERO_EN			 2
#define BIT_LSC_2_EN				 3
#define BIT_LSC_3_ZERO_EN			 4
#define BIT_LSC_3_EN				 5
#define BIT_LSC_4_ZERO_EN			 6
#define BIT_LSC_4_EN				 7

// RegAddr_LSC_KILL_CONF_2 bit mapping
#define BIT_LSC_1_AU_EN				 2
#define BIT_KILL_EN					 6

// RegAddr_LSC_KILL_FEAT_STATUS_1
#define BIT_LSC_1_ZERO_EN_STS		0
#define BIT_LSC_1_EN_STS			1
#define BIT_LSC_2_ZERO_EN_STS		2
#define BIT_LSC_2_EN_STS			3
#define BIT_LSC_3_ZERO_EN_STS		4
#define BIT_LSC_3_EN_STS			5
#define BIT_LSC_4_ZERO_EN_STS		6
#define BIT_LSC_4_EN_STS			7

// RegAddr_LSC_KILL_FEAT_STATUS_2
#define BIT_LSC_1_AU_EN_STS			2
#define BIT_KILL_FEATURE_EN_STS		6

// RegAddr_LSC_KILL_ACT_STATUS_1 bit mapping
#define BIT_I2C_RD_CRC_ERR_STS		0
#define BIT_KILL_IND				6
#define BIT_NVM_ERR_STS				7

// RegAddr_LSC_KILL_ACT_STATUS_2 bit mapping
#define BIT_LSC_DEC_1_STS			0
#define BIT_LSC_DEC_2_STS			1
#define BIT_LSC_DEC_3_STS			2
#define BIT_LSC_DEC_4_STS			3
#define BIT_LSC1_RESET_STS			4
#define BIT_LSC2_RESET_STS			5
#define BIT_LSC3_RESET_STS			6
#define BIT_LSC4_RESET_STS			7

// RegAddr_BUSY_STATUS bit mapping
#define BIT_NVM_BUSY					(0)
#if (ENABLE_HOST_SUPPORT ==1)
#define BIT_HSM_TAGB_STS				(5)
#endif
#define BIT_RANDOM_NUM_BUSY				(6)
#define BIT_AUTH_MAC_BUSY				(7)


// RegAddr_SWI_CONFIG_1
#define BIT_DEV_INT_EN				0


// RegAddr_SWI_INT_0
#define BIT_ECC_DONE_INT_EN			0
#define BIT_DEC_LFSP_END_INT_EN		7


// RegAddr_SWI_INT_1
#define BIT_ECC_DONE_INT_STS		0
#define BIT_DEC_LFSP_END_INT_STS	7

// RegAddr_LOCK_STS
#define BIT_IFX_KILL_CONF			 0
#define BIT_IFX_AUTO_KILL_CONF		 1
#define BIT_HOSTAUTH_CONF			 2
#define BIT_LSC_RESET_CONF			 3
#define BIT_CHIP_LOCK				 4
#define BIT_HOST_SUP_MODE			 5
#define BIT_IFX_USERNVM_RESET_CONFIG 6

// RegAddr_GPO_CNTRL
#define BIT_GPO_OUT					 1
#define BIT_GPO_LDEN				 4
#define BIT_GPO_HDEN				 5
#define BIT_GPO_PDEN				 6
#define BIT_GPO_PUEN				 7


#endif /* INTERFACE_REGISTER_H_ */
