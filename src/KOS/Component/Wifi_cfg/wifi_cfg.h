/**
  * @file       wifi.h
  * @brief      
  * @copyright  Copyright (c) 2020~2030 ShenZhen Dxtc Tech. Co., Ltd.
  * All rights reserved.
  * @version    V1.0
  * @author     Xiagaohui
  * @date       2022-07-19 16:43:59
  * @note       鼎新同创·智能锁
  *
  *****************************************************************************/

#ifndef __WIFI_CFG_H__
#define __WIFI_CFG_H__

#include "osal.h"

#pragma pack(1)
/* wifi cfg 组件消息类型（publish） */
typedef struct
{
    uint8_t msg_type;  //消息类型
    uint8_t data[128]; //消息数据
} WifiCfgMsg_t;
#pragma pack()

/**
  * @brief  开启AP（配网时才调用）
  * @note
  * @param
  */
#define Wifi_Cfg_Start_AP(cfg_info)                                          \
    ({                                                                       \
        uint8_t *temp = OSAL_Malloc(sizeof(Ap_Cfg_Lock_Info) + 1);           \
        temp[0]       = (uint8_t)WIFI_CFG_CTRL_AP_START;                     \
        memcpy(temp + 1, cfg_info, sizeof(Ap_Cfg_Lock_Info));                \
        OSAL_MboxPost(COMP_WIFI_CFG, 0, temp, sizeof(Ap_Cfg_Lock_Info) + 1); \
        OSAL_Free(temp);                                                     \
    })

/**
  * @brief  关闭AP（配网退出）
  * @note
  * @param
  */
#define Wifi_Cfg_Stop_AP()                              \
    ({                                                  \
        uint8_t *temp = OSAL_Malloc(+1);                \
        temp[0]       = (uint8_t)WIFI_CFG_CTRL_AP_STOP; \
        OSAL_MboxPost(COMP_WIFI_CFG, 0, temp, +1);      \
        OSAL_Free(temp);                                \
    })

/**
  * @brief  同步配网信息(重复按4按1进入配网时要同步密码因子)
  * @note
  * @param
  */
#define Wifi_Cfg_Sync_Info(cfg_info)                                         \
    ({                                                                       \
        uint8_t *temp = OSAL_Malloc(sizeof(Ap_Cfg_Lock_Info) + 1);           \
        temp[0]       = (uint8_t)WIFI_CFG_CTRL_SYNC_INFO;                    \
        memcpy(temp + 1, cfg_info, sizeof(Ap_Cfg_Lock_Info));                \
        OSAL_MboxPost(COMP_WIFI_CFG, 0, temp, sizeof(Ap_Cfg_Lock_Info) + 1); \
        OSAL_Free(temp);                                                     \
    })
#endif
