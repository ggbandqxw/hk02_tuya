#include "lcm.h"

#define LCD_MAX_LEN 20 // 屏驱动长度

void lcd_config_init(void)
{
    LcdDevice_Msg_t lcd_dev;
    uint8_t lcd_data[LCD_MAX_LEN] = {0};

    lcd_dev.cmd = 0xfe;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    lcd_dev.cmd = 0xef;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);


    lcd_dev.cmd = 0x36;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x68;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x3a;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x05;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xa4;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x44;
    *(lcd_dev.pData+1) = 0x44;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xa5;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x42;
    *(lcd_dev.pData+1) = 0x42;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xaa;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x88;
    *(lcd_dev.pData+1) = 0x88;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xae;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x2b;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xe8;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x11;
    *(lcd_dev.pData+1) = 0x0b;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xe3;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x01;
    *(lcd_dev.pData+1) = 0x10;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xff;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x61;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xAC;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x00;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xad;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x33;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xAf;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x55;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xa6;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x15;
    *(lcd_dev.pData+1) = 0x15;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xa7;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x06;
    *(lcd_dev.pData+1) = 0x06;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xa8;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x0f;
    *(lcd_dev.pData+1) = 0x0f;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xa9;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = 0x02;
    *(lcd_dev.pData+1) = 0x02;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf0;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x02;
	*(lcd_dev.pData+1) = 0x01;
    *(lcd_dev.pData+2) = 0x00;
    *(lcd_dev.pData+3) = 0x00;
    *(lcd_dev.pData+4) = 0x00;
    *(lcd_dev.pData+5) = 0x07;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf1;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x01;
	*(lcd_dev.pData+1) = 0x02;
    *(lcd_dev.pData+2) = 0x1b;
    *(lcd_dev.pData+3) = 0x16;
    *(lcd_dev.pData+4) = 0x15;
    *(lcd_dev.pData+5) = 0x06;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf2;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x06;
	*(lcd_dev.pData+1) = 0x05;
    *(lcd_dev.pData+2) = 0x1b;
    *(lcd_dev.pData+3) = 0x04;
    *(lcd_dev.pData+4) = 0x03;
    *(lcd_dev.pData+5) = 0x3b;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf3;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x05;
	*(lcd_dev.pData+1) = 0x05;
    *(lcd_dev.pData+2) = 0x2d;
    *(lcd_dev.pData+3) = 0x04;
    *(lcd_dev.pData+4) = 0x03;
    *(lcd_dev.pData+5) = 0x3f;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf4;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x0c;
	*(lcd_dev.pData+1) = 0x1d;
    *(lcd_dev.pData+2) = 0x1f;
    *(lcd_dev.pData+3) = 0x26;
    *(lcd_dev.pData+4) = 0x26;
    *(lcd_dev.pData+5) = 0x0F;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf5;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x05;
	*(lcd_dev.pData+1) = 0x0b;
    *(lcd_dev.pData+2) = 0x08;
    *(lcd_dev.pData+3) = 0x26;
    *(lcd_dev.pData+4) = 0x26;
    *(lcd_dev.pData+5) = 0x0F;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xe9;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x08;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x2a;
    lcd_dev.len = 4;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x00;
	*(lcd_dev.pData+1) = 0x00;
    *(lcd_dev.pData+2) = 0x00;
    *(lcd_dev.pData+3) = 0xef;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x2b;
    lcd_dev.len = 4;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x00;
	*(lcd_dev.pData+1) = 0x00;
    *(lcd_dev.pData+2) = 0x00;
    *(lcd_dev.pData+3) = 0xef;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x2c;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    lcd_dev.cmd = 0x21;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    lcd_dev.cmd = 0x11;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);


    // OSAL_DelayUs(120 * 1000);

    lcd_dev.cmd = 0x29;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

}

void lcd_enter_sleep(void)
{
    uint8_t data[3];
    data[0] = 0x28;
    data[1] = 0;
    Device_Write(vSPI_3, data, 2, LCD_SET_DEVICE);

    OSAL_DelayUs(120 * 1000);

    data[0] = 0x10;
    data[1] = 0;
    Device_Write(vSPI_3, data, 2, LCD_SET_DEVICE);

    OSAL_DelayUs(50 * 1000);

}

void lcd_exit_sleep(void)
{
    uint8_t data[3];
    data[0] = 0x11;
    data[1] = 0;
    Device_Write(vSPI_3, data, 2, LCD_SET_DEVICE);

    // OSAL_DelayUs(120 * 1000);

    data[0] = 0x29;
    data[1] = 0;
    Device_Write(vSPI_3, data, 2, LCD_SET_DEVICE);
}

/**
 * lcd_set_window
 *
 */
void lcd_set_window(uint16_t x_start, uint16_t y_start, uint16_t x_end, uint16_t y_end)
{
    LcdDevice_Msg_t lcd_dev;
    uint8_t lcd_data[LCD_MAX_LEN] = {0};

    lcd_dev.cmd = 0x2a;
    lcd_dev.len = 4;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = (x_start >> 8) & 0xFF;
    *(lcd_dev.pData+1) = (x_start >> 0) & 0xFF;
    *(lcd_dev.pData+2) = (x_end >> 8) & 0xFF;
    *(lcd_dev.pData+3) = (x_end >> 0) & 0xFF;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x2b;
    lcd_dev.len = 4;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = (y_start >> 8) & 0xFF;
    *(lcd_dev.pData+1) = (y_start >> 0) & 0xFF;
    *(lcd_dev.pData+2) = (y_end >> 8) & 0xFF;
    *(lcd_dev.pData+3) = (y_end >> 0) & 0xFF;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);
}
