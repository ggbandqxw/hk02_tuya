#ifndef __LCM_H__
#define __LCM_H__


#include "device.h"
#include "component.h"

#ifndef LCD_WIDTH
#define LCD_WIDTH 240 // 屏宽
#endif

#ifndef LCD_HEIGHT
#define LCD_HEIGHT 240 // 屏高
#endif

#pragma pack(1)
/* 屏驱动下发底层设置 */
typedef struct
{
    uint8_t cmd;    // 发过来的命令
    uint8_t len;    // 数据长度
    uint8_t *pData;  // 数据
} LcdDevice_Msg_t;
#pragma pack()

void lcd_config_init(void);
void lcd_enter_sleep(void);
void lcd_exit_sleep(void);
void lcd_set_window(uint16_t x_start, uint16_t y_start, uint16_t x_end, uint16_t y_end);

#endif /* __LCM_H__ */