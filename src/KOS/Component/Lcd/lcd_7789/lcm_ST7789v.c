#include "lcm.h"

#define LCD_MAX_LEN 20 // 屏驱动长度

#if 1
void lcd_config_init(void)
{
    LcdDevice_Msg_t lcd_dev;
    uint8_t lcd_data[LCD_MAX_LEN] = {0};

    lcd_dev.cmd = 0x11; // internal reg enable
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    // OSAL_DelayUs(120 * 1000);

    lcd_dev.cmd = 0x3a;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x05;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x36;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x00;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xE7;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x10;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xB2;
    lcd_dev.len = 5;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x0C;
	*(lcd_dev.pData+1) = 0x0C;
    *(lcd_dev.pData+2) = 0x00;
    *(lcd_dev.pData+3) = 0x33;
    *(lcd_dev.pData+4) = 0x33;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xB7;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x02;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xBB;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x2F;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xC2;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x01;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xC3;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x19;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);
    
    lcd_dev.cmd = 0xC4;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x20;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xC6;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x0F;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xD0;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData = 0xA4;
    *(lcd_dev.pData+1) = 0xA1;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xD6;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0xA1;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xE0;
    lcd_dev.len = 14;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x70;
	*(lcd_dev.pData+1) = 0x0D;
	*(lcd_dev.pData+2) = 0x12;
	*(lcd_dev.pData+3) = 0x0B;
	*(lcd_dev.pData+4) = 0x0A;
    *(lcd_dev.pData+5) = 0x06;
    *(lcd_dev.pData+6) = 0x33;
    *(lcd_dev.pData+7) = 0x40;
    *(lcd_dev.pData+8) = 0x48;
    *(lcd_dev.pData+9) = 0x08;
    *(lcd_dev.pData+10) = 0x15;
    *(lcd_dev.pData+11) = 0x15;
    *(lcd_dev.pData+12) = 0x2C;
    *(lcd_dev.pData+13) = 0x2F;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xE1;
    lcd_dev.len = 14;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData      = 0x70;	
    *(lcd_dev.pData+1)  = 0x0D;
    *(lcd_dev.pData+2)  = 0x12;
    *(lcd_dev.pData+3)  = 0x0A;
    *(lcd_dev.pData+4)  = 0x0A;
    *(lcd_dev.pData+5)  = 0x06;
    *(lcd_dev.pData+6)  = 0x33;
    *(lcd_dev.pData+7)  = 0x37;
    *(lcd_dev.pData+8)  = 0x49;
    *(lcd_dev.pData+9)  = 0x08;
    *(lcd_dev.pData+10) = 0x15;
    *(lcd_dev.pData+11) = 0x16;
    *(lcd_dev.pData+12) = 0x2C;
    *(lcd_dev.pData+13) = 0x2F;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xE4;
    lcd_dev.len = 3;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x27; // 0x1D;--240
	*(lcd_dev.pData+1) = 0x0A;
	*(lcd_dev.pData+2) = 0x11;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x21;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    // OSAL_DelayUs(120 * 1000);

    lcd_dev.cmd = 0x29;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

}
#else
void lcd_config_init(void)
{
    LcdDevice_Msg_t lcd_dev;
    uint8_t lcd_data[LCD_MAX_LEN] = {0};

    lcd_dev.cmd = 0xfe; // internal reg enable
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    lcd_dev.cmd = 0xef; // internal reg enable
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    lcd_dev.cmd = 0x36;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x48;
    printf("lcd_dev.pData:%x\r\n", lcd_dev.pData[0]);
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x3a;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x05;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x86;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x98;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x89;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x33;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x8b;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x80;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x8d;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x22;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x8e;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x0f;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xe8;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x12;
    *(lcd_dev.pData+1) = 0x00;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xe9;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x08;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xec;
    lcd_dev.len = 3;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x13;
    *(lcd_dev.pData+1) = 0x02;
    *(lcd_dev.pData+2) = 0x88;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xed;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x18;
    *(lcd_dev.pData+1) = 0x09;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xff;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x62;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x99;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x3e;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x9d;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x4b;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xc3;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x3a;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xc4;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x30;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xc9;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x0a;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf0;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x45;
	*(lcd_dev.pData+1) = 0x0A;
	*(lcd_dev.pData+2) = 0x14;
	*(lcd_dev.pData+3) = 0x06;
	*(lcd_dev.pData+4) = 0x05;
	*(lcd_dev.pData+5) = 0x2E;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf2;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x45;
	*(lcd_dev.pData+1) = 0x09;
	*(lcd_dev.pData+2) = 0x14;
	*(lcd_dev.pData+3) = 0x0b;
	*(lcd_dev.pData+4) = 0x05;
	*(lcd_dev.pData+5) = 0x2E;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf1;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x45;
	*(lcd_dev.pData+1) = 0x8F;
	*(lcd_dev.pData+2) = 0x8f;
	*(lcd_dev.pData+3) = 0x3B;
	*(lcd_dev.pData+4) = 0x3F;
	*(lcd_dev.pData+5) = 0x7f;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0xf3;
    lcd_dev.len = 6;
    lcd_dev.pData = lcd_data;
	*lcd_dev.pData     = 0x45;
	*(lcd_dev.pData+1) = 0x8f;
	*(lcd_dev.pData+2) = 0x8f;
	*(lcd_dev.pData+3) = 0x3B;
	*(lcd_dev.pData+4) = 0x3F;
	*(lcd_dev.pData+5) = 0x7f;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x35;
    lcd_dev.len = 1;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x00;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x44;
    lcd_dev.len = 2;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData = 0x00;
    *(lcd_dev.pData+1) = 0x0a;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    lcd_dev.cmd = 0x11;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    // OSAL_DelayUs(120 * 1000);

    lcd_dev.cmd = 0x29;
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

    lcd_dev.cmd = 0x2c; // internal reg enable
    lcd_dev.len = 0;
    Device_Write(vSPI_3, &lcd_dev, 2, LCD_SET_DEVICE);

}
#endif


void lcd_enter_sleep(void)
{
    uint8_t data[3];
    data[0] = 0x28;
    data[1] = 0;
    Device_Write(vSPI_3, data, 2, LCD_SET_DEVICE);

    OSAL_DelayUs(120 * 1000);

    data[0] = 0x10;
    data[1] = 0;
    Device_Write(vSPI_3, data, 2, LCD_SET_DEVICE);

    OSAL_DelayUs(50 * 1000);

}

void lcd_exit_sleep(void)
{
    uint8_t data[3];
    data[0] = 0x11;
    data[1] = 0;
    Device_Write(vSPI_3, data, 2, LCD_SET_DEVICE);

    // OSAL_DelayUs(120 * 1000);

    data[0] = 0x29;
    data[1] = 0;
    Device_Write(vSPI_3, data, 2, LCD_SET_DEVICE);
}

/**
 * lcd_set_window
 *
 */
void lcd_set_window(uint16_t x_start, uint16_t y_start, uint16_t x_end, uint16_t y_end)
{
    LcdDevice_Msg_t lcd_dev;
    uint8_t lcd_data[LCD_MAX_LEN] = {0};

    lcd_dev.cmd = 0x2a;
    lcd_dev.len = 4;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = (x_start >> 8) & 0xFF;
    *(lcd_dev.pData+1) = (x_start >> 0) & 0xFF;
    *(lcd_dev.pData+2) = (x_end >> 8) & 0xFF;
    *(lcd_dev.pData+3) = (x_end >> 0) & 0xFF;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);

    uint16_t y_start1, y_end1;
    y_start1 = LCD_HEIGHT - y_end;
    y_end1 = LCD_HEIGHT - y_start;
    printf("y_start: %d, y_start1: %d, y_end: %d, y_end1: %d\r\n", y_start, y_start1, y_end, y_end1);

    lcd_dev.cmd = 0x2b;
    lcd_dev.len = 4;
    lcd_dev.pData = lcd_data;
    *lcd_dev.pData     = (y_start1 >> 8) & 0xFF;
    *(lcd_dev.pData+1) = (y_start1 >> 0) & 0xFF;
    *(lcd_dev.pData+2) = (y_end1 >> 8) & 0xFF;
    *(lcd_dev.pData+3) = (y_end1 >> 0) & 0xFF;
    Device_Write(vSPI_3, &lcd_dev, (lcd_dev.len+2), LCD_SET_DEVICE);
}
