#ifndef __BMP_H__
#define __BMP_H__

#define BMP_FACE_START              10001
#define BMP_FACE_END                10012

#define BMP_FACE_WHITE              10013
#define BMP_FACE_RED                10014
#define BMP_FACE_GREEN              10015

#define BMP_SUCCESS_GREEN           10016
#define BMP_SUCCESS_WHITE           10017

#define BMP_FAIL_RED                10018
#define BMP_FAIL_WHITE              10019

#define BMP_FINGER_WHITE            10020
#define BMP_FINGER_RED              10021
#define BMP_FINGER_GREEN            10022

#define BMP_KEY_WHITE               10023
#define BMP_KEY_RED                 10024
#define BMP_KEY_GREEN               10025

#define BMP_CHANGE_WHITE            10026
#define BMP_CHANGE_RED              10027
#define BMP_CHANGE_GREEN            10028

#define BMP_CARD_WHITE              10029
#define BMP_CARD_RED                10030
#define BMP_CARD_GREEN              10031

#define BMP_BATTERY_LOW             10032
#define BMP_BATTERY_ILLEGAL         10033

#define BMP_VERIFY_DUAL             10034 // 双重验证
#define BMP_VERIFY_DUAL_OK          10035
#define BMP_VERIFY_DUAL_FAIL1       10036
#define BMP_VERIFY_DUAL_FAIL2       10037

#define BMP_DXTC_LOGO             10038

#define BMP_ARMING_MODE             10039 // 布防
#define BMP_ANTI_LOCK_MODE          10040 // 反锁

#define BMP_VIOCE_MUTE              10041 // 静音
#define BMP_VIOCE_LOW               10042
#define BMP_VIOCE_HEIGHT            10043

#define BMP_DOORBELL_START          10044
#define BMP_DOORBELL_END            10046

#define BMP_ALARM_RED               10047
#define BMP_LEAVE_MESSAGE           10048

#define BMP_PALM_WHITE              10049
#define BMP_PALM_RED                10050
#define BMP_PALM_GREEN              10051

#define BMP_UWB_WHITE               10052
#define BMP_UWB_RED                 10053
#define BMP_UWB_GREEN               10054


#define BMP_CLOSE_DOOR_INSIDE       10055//门内上锁
#define BMP_OPEN_DOOR_INSIDE        10056//门内开锁

#define BMP_NO_ACTIVATE             10057//未激活
#endif