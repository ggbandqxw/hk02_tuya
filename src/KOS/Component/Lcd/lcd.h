#ifndef __LCD_H__
#define __LCD_H__
#include "osal.h"
#include "bmp.h"


#define PICTURE_CENTER          UINT16_MAX//图片居中
#define PICTURE_NOBLINK         UINT16_MAX//图片不闪烁
#define PICTURE_DISPLAY_TIME    2500      //2s

#pragma pack(1)
/* 动画显示 11 */ 
typedef struct
{
    uint16_t start_sn; // 起始编号
    uint16_t end_sn;   // 结束编号
    uint16_t start_x;  // 起始x值(居中：0xFFFF)
    uint16_t start_y;  // 起始y值(居中：0xFFFF)
    uint16_t interval; // 每张间隔（ms）
    uint8_t times;     // 显示次数(永久：0xFF)
} DisplayGif_stu_t;
#pragma pack()

/* lcd组件邮箱主题（APP->COMP） */
#define LCD_MBOX_GIF  1
#define LCD_MBOX_STOP 2

/**
 * @brief 显示动画
 * 
 */
#define Lcd_DisplayGif(_start_sn, _end_sn, _x, _y, _interval, _times)     \
({                                                                        \
    uint8_t *temp = (uint8_t *)OSAL_Malloc(sizeof(DisplayGif_stu_t) + 1); \
    DisplayGif_stu_t* pData = (DisplayGif_stu_t*)&temp[1];                \
    temp[0] = LCD_MBOX_GIF;                                               \
    pData->start_sn = (uint16_t)_start_sn;                                \
    pData->end_sn = (uint16_t)_end_sn;                                    \
    pData->start_x =(uint16_t) _x;                                        \
    pData->start_y =(uint16_t) _y;                                        \
    pData->interval =(uint16_t) _interval;                                \
    pData->times = (uint8_t)_times;                                       \
    OSAL_MboxPost(COMP_LCD, 0, temp, sizeof(DisplayGif_stu_t) + 1);       \
    OSAL_Free(temp);                                                      \
})

/**
 * @brief 显示图片
 * 
 */
#define Lcd_DisplayPic(_sn, _x, _y, _interval, _times)   Lcd_DisplayGif(_sn, _sn, _x, _y, _interval, _times)

/*
 * 关闭显示
 */
#define Lcd_StopDisplay()                            \
({                                                   \
    uint8_t temp = LCD_MBOX_STOP;                    \
    OSAL_MboxPost(COMP_LCD, 0, &temp, sizeof(temp)); \
})



#endif
