/**
 * @file ble_comp.h
 * @brief
 *
 * @version 0.1
 * @date 2023-09-05
 *
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 *
 * @note          鼎新同创・智能锁
 *
 * @par 修改日志:
 * <1> 2023-09-05 v0.1 tianshidong 创建初始版本
 * *************************************************************************
 */
#ifndef __BLE_COMP_H__
#define __BLE_COMP_H__
#include "osal.h"

/* *******************************宏定义****************************************** */
#define BLE_COMP_BUFFER_SIZE (200)   //全局缓冲区存储数据的长度
#define BLE_RECV_ISR_DATA_SIZE (100) //存储蓝牙底层数据的缓冲长度// 20
#define BLE_RB_RXBUF_LENGTH (10)    // 环形缓存的缓存长度

/* *******************************结构类型定义****************************************** */
#pragma pack(1)

typedef struct
{
    uint8_t len;                           // 数据长度
    uint8_t type;                          // 类型
    uint8_t pData[BLE_RECV_ISR_DATA_SIZE]; // 数据
} BleIRQMsg_t;

typedef struct
{
    uint8_t bind_flag;
    uint8_t ble_encryptionflag;
} BleNvData_stu_t;

#pragma pack()

void Tuya_Ble_Int_Irq_Handler(void *pBuf, uint32_t len);



#endif /*__BLE_COMP_H__*/