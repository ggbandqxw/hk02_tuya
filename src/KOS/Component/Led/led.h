#ifndef __LED_H__
#define __LED_H__
#include "component.h"


/* LED灯控制类型 */
typedef enum
{
    LED_ON,        //常亮
    LED_OFF,       //常灭
    LED_FLASH,     //闪烁
    LED_ANIMATION, //上电动画
}LedCtrlType_enum_t;

typedef struct
{
    uint8_t times;
    LedCtrlType_enum_t ctrl;
    uint16_t cycle;
    char name[20];
}LedSet_stu_t;

/* LED组件昵称枚举 */
typedef enum
{
    LED_ALIAS_FRONT = 1, //前板LED组件
    LED_ALIAS_REAR  = 2, //后板LED组件
}LedAlias_enum_t;

/* LED控制接口 */
#define Led_Set(_name, _ctrl, _times, _cycle)        \
({                                                   \
    LedSet_stu_t temp = {0};                         \
                                                     \
    strcpy(temp.name, (char*)_name);                 \
    temp.ctrl  = _ctrl;                              \
    temp.times = _times;                             \
    temp.cycle = _cycle;                             \
    OSAL_MboxPost(COMP_LED, LED_ALIAS_FRONT, &temp, sizeof(temp)); \
})

/* 后板LED控制接口 */
#define Led_Set_Rear(_name, _ctrl, _times, _cycle)   \
({                                                   \
    LedSet_stu_t temp = {0};                         \
                                                     \
    strcpy(temp.name, (char*)_name);                 \
    temp.ctrl  = _ctrl;                              \
    temp.times = _times;                             \
    temp.cycle = _cycle;                             \
    OSAL_MboxPost(COMP_LED, LED_ALIAS_REAR, &temp, sizeof(temp)); \
})




#endif /* __LED_H__ */

