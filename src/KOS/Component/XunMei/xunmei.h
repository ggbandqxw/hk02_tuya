/**
  * @file       xunmei.h
  * @brief      讯美通讯组件
  * @copyright  Copyright (c) 2020~2030 ShenZhen Dxtc Tech. Co., Ltd.
  * All rights reserved.
  * @version    V1.0
  * @author     TianXubin
  * @date       2022-02-12 15:18:09
  * @note       鼎新同创·智能锁
  *
  *****************************************************************************/

#ifndef _XUNMEI_H
#define _XUNMEI_H
#include "stdbool.h"
#include "stdint.h"
#include "component.h"

#define TX_RX_CMD_WAKE_NOTIFI 0X20 //唤醒通知
#define RX_CMD_MULTI_PACKET 0x11 //分包接收
#define RX_CMD_MULTI_PACKET_1 0x1C //分包接收
#define TX_CMD_MULTI_PACKET 0x01 //分包发送

/*命令执行结果*/
#define XUNMEI_EXE_ERR 0 //命令执行失败
#define XUNMEI_EXE_OK 1  //命令执行成功

//发送结果
typedef enum
{
    UART_SEND_SUCCEED,          //成功
    UART_SEND_ERROR,            //失败
    UART_SEND_WAIT_ACK_TIMEOUT, //等待ACK包超时
    UART_SEND_MAX,
} uart_send_res_enum_t;

//发送包类型
typedef enum
{
    JSON_PACKET_RECODE_LIST,
    JSON_PACKET_NOMARL,
    COMMAND,
    COMMAND_ACK,
} cmd_packet_type;

typedef enum
{
    XM_RECV_CMD_PACKET,
    XM_EXEC_CB,
} XunMeiMsgType_enum_t;

/* 串口发送回调类型 */
typedef void (*uart_send_cb_fun_t)(uart_send_res_enum_t res, uint8_t cmd, uint8_t *out_data, uint16_t out_len);

#pragma pack(1)
typedef struct
{
    XunMeiMsgType_enum_t msgType;
    uart_send_cb_fun_t cb;
    uart_send_res_enum_t res;
    uint8_t cmd;
    uint16_t len;
    uint8_t data[];
} XunMeiMsg_t;
#pragma pack()

static int XunmeiSend_Cmd(uint8_t cmd, uint8_t *pdata, uint16_t len, uart_send_cb_fun_t cb, cmd_packet_type pack_type);
static int XunMei_SendWakeSignal(void);
static ErrorStatus XunMei_IsProtocolIdle(void);
static int XunMei_Power(FlagStatus onoff);

#endif
