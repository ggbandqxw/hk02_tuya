/**
  ******************************************************************************
  * @file    qmcX983.c
  * @author  STMicroelectronics
  * @version V1.0
  * @date    2013-xx-xx
  * @brief    qmcX983驱动
  ******************************************************************************
  * @attention
  *
  * 实验平台:秉火 指南者 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 

#include "qmc6308.h"

#define QMC_LOG(format, ...) __OSAL_LOG("[qmc6308.c] " C_BLUE format C_NONE, ##__VA_ARGS__)

static unsigned char qmc6308_chipid = 0;
static qmc6308_map g_map;

static VirtualHardware_enum_t iic_vhw = IIC_VHW_MSENSOR_LEFT;
static FlagStatus init_flag = RESET;


static ErrorStatus qmc6308_iic_read(uint8_t reg_addr, uint8_t *data, uint8_t len)
{
    if (Device_Write(iic_vhw, &reg_addr, 1, QMC6308_IIC_ADDR) == 0)
    {
        return (Device_Read(iic_vhw, data, len, QMC6308_IIC_ADDR) == 0) ? SUCCESS : ERROR;
    }
    return ERROR;
}

static ErrorStatus qmc6308_iic_write(uint8_t reg_addr, uint8_t *data, uint8_t len)
{
    uint8_t buffer[10] = {0};

    buffer[0] = reg_addr;
    memcpy(&buffer[1], data, len);
    return (Device_Write(iic_vhw, buffer, len+1, QMC6308_IIC_ADDR) == 0) ? SUCCESS : ERROR;
}

static uint8_t qmc6308_read_block(uint8_t addr, uint8_t *data, uint8_t len)
{
	uint8_t ret = 0;
	uint32_t retry = 0;

	while((!ret) && (retry++ < 5))
	{
        ret = qmc6308_iic_read(addr, data, len);
	}
	return ret;
}

static uint8_t qmc6308_write_reg(uint8_t addr, uint8_t data)
{
	uint8_t ret = 0;
	uint32_t retry = 0;

	while((!ret) && (retry++ < 5))
	{
        ret = qmc6308_iic_write(addr, &data, 1);
	}
	return ret;
}

static void qmc6308_set_layout(int layout)
{
	if(layout == 0)
	{
		g_map.sign[AXIS_X] = 1;
		g_map.sign[AXIS_Y] = 1;
		g_map.sign[AXIS_Z] = 1;
		g_map.map[AXIS_X] = AXIS_X;
		g_map.map[AXIS_Y] = AXIS_Y;
		g_map.map[AXIS_Z] = AXIS_Z;
	}
	else if(layout == 1)
	{
		g_map.sign[AXIS_X] = -1;
		g_map.sign[AXIS_Y] = 1;
		g_map.sign[AXIS_Z] = 1;
		g_map.map[AXIS_X] = AXIS_Y;
		g_map.map[AXIS_Y] = AXIS_X;
		g_map.map[AXIS_Z] = AXIS_Z;
	}
	else if(layout == 2)
	{
		g_map.sign[AXIS_X] = -1;
		g_map.sign[AXIS_Y] = -1;
		g_map.sign[AXIS_Z] = 1;
		g_map.map[AXIS_X] = AXIS_X;
		g_map.map[AXIS_Y] = AXIS_Y;
		g_map.map[AXIS_Z] = AXIS_Z;
	}
	else if(layout == 3)
	{
		g_map.sign[AXIS_X] = 1;
		g_map.sign[AXIS_Y] = -1;
		g_map.sign[AXIS_Z] = 1;
		g_map.map[AXIS_X] = AXIS_Y;
		g_map.map[AXIS_Y] = AXIS_X;
		g_map.map[AXIS_Z] = AXIS_Z;
	}	
	else if(layout == 4)
	{
		g_map.sign[AXIS_X] = -1;
		g_map.sign[AXIS_Y] = 1;
		g_map.sign[AXIS_Z] = -1;
		g_map.map[AXIS_X] = AXIS_X;
		g_map.map[AXIS_Y] = AXIS_Y;
		g_map.map[AXIS_Z] = AXIS_Z;
	}
	else if(layout == 5)
	{
		g_map.sign[AXIS_X] = 1;
		g_map.sign[AXIS_Y] = 1;
		g_map.sign[AXIS_Z] = -1;
		g_map.map[AXIS_X] = AXIS_Y;
		g_map.map[AXIS_Y] = AXIS_X;
		g_map.map[AXIS_Z] = AXIS_Z;
	}
	else if(layout == 6)
	{
		g_map.sign[AXIS_X] = 1;
		g_map.sign[AXIS_Y] = -1;
		g_map.sign[AXIS_Z] = -1;
		g_map.map[AXIS_X] = AXIS_X;
		g_map.map[AXIS_Y] = AXIS_Y;
		g_map.map[AXIS_Z] = AXIS_Z;
	}
	else if(layout == 7)
	{
		g_map.sign[AXIS_X] = -1;
		g_map.sign[AXIS_Y] = -1;
		g_map.sign[AXIS_Z] = -1;
		g_map.map[AXIS_X] = AXIS_Y;
		g_map.map[AXIS_Y] = AXIS_X;
		g_map.map[AXIS_Z] = AXIS_Z;
	}
	else		
	{
		g_map.sign[AXIS_X] = 1;
		g_map.sign[AXIS_Y] = 1;
		g_map.sign[AXIS_Z] = 1;
		g_map.map[AXIS_X] = AXIS_X;
		g_map.map[AXIS_Y] = AXIS_Y;
		g_map.map[AXIS_Z] = AXIS_Z;
	}
}

static int qmc6308_get_chipid(void)
{
	int ret = 0;
	int i;

	for(i=0; i<10; i++)
	{
		ret = qmc6308_read_block(QMC6308_CHIP_ID_REG, &qmc6308_chipid , 1);
		QMC_LOG("qmc6308_get_chipid chipid = 0x%x\r\n", qmc6308_chipid);
		if(ret)
		{
			break;
		}
	}
	if(i>=10)
	{
		return 0;
	}
	// if((qmc6308_chipid & 0xf0)==0)
	if (qmc6308_chipid != 0x80)
	{
		return 0;
	}

	return 1;
}

int qmc6308_enable(FlagStatus opt)
{
	int ret;

	// if(qmc6308_chipid == 0x80)
	// {
	// 	ret = qmc6308_write_reg(0x0d, 0x40);
	// 	ret = qmc6308_write_reg(QMC6308_CTL_REG_TWO, 0x00);
	// 	ret = qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0xc3);
	// }
	// else
	// {
	// 	ret = qmc6308_write_reg(0x0d, 0x40);
	// 	ret = qmc6308_write_reg(QMC6308_CTL_REG_TWO, 0x08);
	// 	ret = qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0x63);
	// }
	
    //进入单次测量模式，测量结束会自动进入低功耗模式
    // if (opt == SET)
    // {
    //     ret = qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0XC2);//过采样率配置为8，功耗高些
    // }
    // else
    {
        // ret = qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0X32);//过采样率配置为1，功耗低些
        ret = qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0XC2);//过采样率配置为8，提高采样稳定性
    }
	return ret;
}

int qmc6308_disable(void)
{
	int ret;
	ret = qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0x00);
	return ret;
}

int qmc6308_read_mag_xyz(float *data)
{
	int res;
	unsigned char mag_data[6];
	short hw_d[3] = {0};
	float hw_f[3];
	int t1 = 0;
	unsigned char rdy = 0;
    
	/* Check status register for data availability */
	while(!(rdy & 0x01) && (t1 < 5)){
		rdy = QMC6308_STATUS_REG;
		res = qmc6308_read_block(QMC6308_STATUS_REG, &rdy, 1);
		t1++;
	}

	mag_data[0] = QMC6308_DATA_OUT_X_LSB_REG;

	res = qmc6308_read_block(QMC6308_DATA_OUT_X_LSB_REG, mag_data, 6);
	if(res == 0)
  	{
		return 0;
	}

	hw_d[0] = (short)(((mag_data[1]) << 8) | mag_data[0]);
	hw_d[1] = (short)(((mag_data[3]) << 8) | mag_data[2]);
	hw_d[2] = (short)(((mag_data[5]) << 8) | mag_data[4]);

	hw_f[0] = (float)((float)hw_d[0] / 10.0f);		// ut
	hw_f[1] = (float)((float)hw_d[1] / 10.0f);		// ut
	hw_f[2] = (float)((float)hw_d[2] / 10.0f);		// ut

	data[AXIS_X] = (float)(g_map.sign[AXIS_X]*hw_f[g_map.map[AXIS_X]]);
	data[AXIS_Y] = (float)(g_map.sign[AXIS_Y]*hw_f[g_map.map[AXIS_Y]]);
	data[AXIS_Z] = (float)(g_map.sign[AXIS_Z]*hw_f[g_map.map[AXIS_Z]]);
	
	return (rdy>>1) & 1;
}

int qmc6308_read_xyz(int16_t *p_xyz, FlagStatus opt)
{
	int res, t1 = 0;
	uint8_t mag_data[6] = {0};
	uint8_t rdy = 0;
    
    if (init_flag == RESET)
    {
        QMC_LOG("m sensor not init\r\n");
        return 0;
    }

    if (opt == RESET)
    {
        //使能单次测量
        qmc6308_enable(opt);
    }

	//读状态寄存器
	while(!(rdy & 0x01) && (t1 < 5))
    {
		rdy = 0;
		qmc6308_read_block(QMC6308_STATUS_REG, &rdy, 1);
		t1++;
	}

    //读XYZ数据
	res = qmc6308_read_block(QMC6308_DATA_OUT_X_LSB_REG, mag_data, 6);
	if (res == 0 || (rdy & 0x02))
  	{
        QMC_LOG("m sensor bad\r\n");
		return 0;
	}

	p_xyz[0] = (short)(((mag_data[1]) << 8) | mag_data[0]);// / 10; //1uT = 10mGs
	p_xyz[1] = (short)(((mag_data[3]) << 8) | mag_data[2]);// / 10;
	p_xyz[2] = (short)(((mag_data[5]) << 8) | mag_data[4]);// / 10;
    // QMC_LOG("x : %d, y : %d, z : %d\r\n", p_xyz[0], p_xyz[1], p_xyz[2]);
	return 1;
}

/* Set the sensor mode */
int qmc6308_set_mode(unsigned char mode)
{
	int err = 0;
	unsigned char ctrl1_value = 0;
	
	err = qmc6308_read_block(QMC6308_CTL_REG_ONE, &ctrl1_value, 1);
	ctrl1_value = (ctrl1_value&(~0x03))|mode;
	QMC_LOG("qmc6308_set_mode, 0x%x = 0x%x \n", QMC6308_CTL_REG_ONE,ctrl1_value);
	err = qmc6308_write_reg(QMC6308_CTL_REG_ONE, ctrl1_value);

	return err;
}

int qmc6308_set_output_data_rate(unsigned char rate){
	
	int err = 0;
	unsigned char ctrl1_value = 0;
	
	err = qmc6308_read_block(QMC6308_CTL_REG_ONE, &ctrl1_value, 1);
	ctrl1_value = (ctrl1_value& (~0xE8)) | (rate << 5);
	QMC_LOG("qmc6308_set_output_data_rate, 0x%x = 0x%2x \n", QMC6308_CTL_REG_ONE,ctrl1_value);
	err = qmc6308_write_reg(QMC6308_CTL_REG_ONE, ctrl1_value);

	return err;	
}

void qmc6308_enable_continue(void)
{
    if (init_flag == SET)
    {
        // qmc6308_write_reg(0x0d, 0x40);
        qmc6308_write_reg(QMC6308_CTL_REG_TWO, 0x80); // 软复位
        qmc6308_write_reg(QMC6308_CTL_REG_TWO, 0x00);
        qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0XCF);
    }
}

int qmc6308_self_test(void)
{
    if (init_flag == SET)
    {
        uint8_t rdy = 0, t1 = 0, res = 0;
        int16_t data1[3] = {0};
        int16_t data2[3] = {0};

        //设置连续转换模式
        qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0X03);

        //读状态寄存器
        while(!(rdy & 0x01) && (t1 < 5))
        {
            rdy = 0;
            qmc6308_read_block(QMC6308_STATUS_REG, &rdy, 1);
            t1++;
        }
        Device_DelayMs(5);

        //读XYZ数据
        res = qmc6308_read_block(QMC6308_DATA_OUT_X_LSB_REG, (uint8_t*)data1, 6);
        if (res == 0 || (rdy & 0x02))
        {
            return 0;
        }

        //设置self-test模式
        qmc6308_write_reg(QMC6308_CTL_REG_TWO, 0X40);
        Device_DelayMs(5);
        
        //读XYZ数据
        res = qmc6308_read_block(QMC6308_DATA_OUT_X_LSB_REG, (uint8_t*)data2, 6);
        if (res == 0)
        {
            return 0;
        }

        int16_t delta_x = data1[0] - data2[0];
        int16_t delta_y = data1[1] - data2[1];
        int16_t delta_z = data1[2] - data2[2];

        QMC_LOG("data1_x:%-6d data2_x:%-6d delta_X:%d", data1[0], data2[0], delta_x);
        QMC_LOG("data1_y:%-6d data2_y:%-6d delta_Y:%d", data1[1], data2[1], delta_y);
        QMC_LOG("data1_z:%-6d data2_z:%-6d delta_Z:%d", data1[2], data2[2], delta_z);
        if (delta_x < 800 || delta_x > 1200 || delta_y < 800 || delta_y > 1200 || delta_z < 120 || delta_z > 1200)
        {
            QMC_LOG("msensor self test error\r\n");
            return 0;
        }
        else
        {
            QMC_LOG("msensor self test succeed\r\n");
            return 1;
        }
    }
    return 0;
}

int qmc6308_init(VirtualHardware_enum_t dev)
{
	int ret = 0;

    /* 关闭不用的那个sensor */
    if (dev == IIC_VHW_MSENSOR_LEFT)
    {
        iic_vhw = IIC_VHW_MSENSOR_RIGHT;
    }
    else if (dev == IIC_VHW_MSENSOR_RIGHT)
    {
        iic_vhw = IIC_VHW_MSENSOR_LEFT;
    }
    else
    {
        return 0;
    }
    qmc6308_disable();


    /* 初始化要用的sensor */
    iic_vhw = dev;
	ret = qmc6308_get_chipid();
	if(ret==0)
	{
        init_flag = RESET;
		return 0;
	}
	qmc6308_set_layout(1);

    ret = qmc6308_write_reg(0x0d, 0x40);
    ret = qmc6308_write_reg(QMC6308_CTL_REG_TWO, 0x00);//Full Range：±30 Guass
    ret = qmc6308_write_reg(QMC6308_CTL_REG_ONE, 0XCF);//0xC3);

#if 0
	{
		unsigned char ctrl_value;
		qmc6308_read_block(QMC6308_CTL_REG_ONE, &ctrl_value, 1);
		QMC_LOG("qmc6308  0x%x=0x%x \n", QMC6308_CTL_REG_ONE, ctrl_value);
		qmc6308_read_block(QMC6308_CTL_REG_TWO, &ctrl_value, 1);
		QMC_LOG("qmc6308  0x%x=0x%x \n", QMC6308_CTL_REG_TWO, ctrl_value);
		qmc6308_read_block(0x0d, &ctrl_value, 1);
		QMC_LOG("qmc6308  0x%x=0x%x \n", 0x0d, ctrl_value);
	}
#endif

    init_flag = SET;
	return 1;
}

