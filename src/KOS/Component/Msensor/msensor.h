#ifndef __MSENSOR_H__
#define __MSENSOR_H__

/* Msensor门的动作类型 */
typedef enum
{
    MSENSOR_STA_INIT  = 0, //最开始没状态时
    MSENSOR_STA_OPEN  = 1, //传感器离开设定范围
    MSENSOR_STA_CLOSE = 2, //传感器进入设定范围
    MSENSOR_STA_ERROR = 3, //1、强磁干扰导致传感器产生误差 --- 需要用户唤醒锁做开关门动作（门锁会重新校准修正误差）
}Msensor_Action_t;

/* Msensor设置参数 */
typedef enum
{
    MSENSOR_OPEN = 1,     
    MSENSOR_CLOSE,    //校准关门状态
    MSENSOR_XUYAN,    //校准虚掩状态
    MSENSOR_ENABLE,   //使能门磁
    MSENSOR_DISABLE,  //关闭门磁

    MSENSOR_INIT_LEFT, //初始化左边sensor
    MSENSOR_INIT_RIGHT,//初始化右边sensor

    MSENSOR_SELF_TEST, //测试sensor
    MSENSOR_FUNC_STATE,//门磁功能开关变化上报
    MSENSOR_DOOR_STATE,//门当前状态， Msensor_Action_t
    MSENSOR_CAL_STATE, //当前是否在门磁校准，0：不在 1在校准中
}Msensor_enum_t;

/* 门磁组件上报类型 */
typedef struct 
{
    Msensor_enum_t action;			//门磁动作
	uint8_t res;        		    //回复结果
}MsensorMsg_t;

/* 门磁组件邮箱主题（APP->COMP） */
#define MSENSOR_MBOX_SETTING        0
#define MSENSOR_MBOX_SETFLAG      1

#define Msensor_Setting(action, flag)                           \
({                                                              \
    uint8_t temp[3] = {MSENSOR_MBOX_SETTING, action, flag};     \
    OSAL_MboxPost(COMP_MSENSOR, 0, temp, sizeof(temp));         \
})

/* 设置门磁标志位（APP->COMP）RESET锁舌伸出不检测 SET开锁成功检测门磁 */
#define Set_MsdpFlag(flag)                                      \
({                                                              \
    uint8_t temp[2] = {MSENSOR_MBOX_SETFLAG, flag};             \
    OSAL_MboxPost(COMP_MSENSOR, 0, temp, sizeof(temp));         \
})
/*
#define Msensor_GetStatus()                                \
({                                                         \
    OSAL_MboxPost(COMP_MSENSOR, 0, MSENSOR_MBOX_GETSTATUS, \
        sizeof(MSENSOR_MBOX_GETSTATUS));                   \
})
*/
#endif /* __MSENSOR_H__ */
