#ifndef __MSDP_H__
#define __MSDP_H__

#include "osal.h"


typedef enum
{
    MSDP_SET_OPEN = 1,  //开门
    MSDP_SET_CLOSE,     //关门
    MSDP_SET_HALF_CLOSE,//虚掩
    MSDP_SET_ENABLE,    //使能
    MSDP_SET_DISABLE,   //禁能
}MsdpSetting_enum_t;

typedef enum
{
    MSDP_EVT_NULL,   //无事件
    MSDP_EVT_EXIT,   //传感器离开设定范围
	MSDP_EVT_ENTER,  //传感器进入设定范围
    MSDP_EVT_ERROR,  //1、强磁干扰导致传感器产生误差 --- 需要用户唤醒锁做开关门动作（门锁会重新校准修正误差）
                     //2、磁铁更换了位置或角度 --- 需要用户关闭门磁功能，然后重新开启门磁完成校准
}MsdpEvent_enum_t;

void Msdp_SendStatus(int first_flag);
int Msdp_Setting(MsdpSetting_enum_t param);
void Msensor_SendFuncStatus(FunctionalState res);
void Msensor_SendCalStatus(uint8_t res);
uint8_t Get_MsdpFlag(void);
void Msdp_Init(void);
MsdpEvent_enum_t Msdp_Process(int16_t x, int16_t y, int16_t z);

#endif /* __MSDP_H__ */
