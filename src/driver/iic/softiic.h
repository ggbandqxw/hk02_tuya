#ifndef __SOFT_IIC_H__
#define __SOFT_IIC_H__

#include <stdint.h>
typedef struct
{
    uint8_t sclPin;
    uint8_t sdaPin;
    uint16_t delyCounter;
}softiic_config_t;
/* 软IIC读写接口 */
uint8_t Softiic_Write(uint8_t channel,uint8_t slave_add, uint8_t *Buf, uint8_t Size);
uint8_t Softiic_Read(uint8_t channel,uint8_t slave_add, uint8_t *_pReadBuf, uint8_t _usSize);
void Softiic_init(uint8_t channel);
void Softiic_deinit(uint8_t channel);
#endif /* __SOFT_IIC_H__ */
