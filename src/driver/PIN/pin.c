#include "device.h"
#include <string.h>

//昂瑞微的驱动库API
#include "rwip_config.h" // RW SW configuration
#include "peripheral.h"
#include "debug_config.h"
#include "pin.h"

static uint8_t Intflag = 0;
static uint8_t num = 0;
static uint8_t version_table[5];

/* 虚拟IO与硬件端口映射表 */
typedef struct
{
    VirtualHardware_enum_t pin;
    uint32_t port;
} GpioMap_stu_t;

/****************************子设备 PIN**********************************************/
static const GpioMap_stu_t gpio_map[] = PIN_MAP;

//设置中断脚为输出-推挽-输出
void SetGpio_Input_To_Output(uint32_t pin_num)
{
    // printf("out:%d\r\n",pin_num);
    pinmux_config(pin_num, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(pin_num), PMU_PIN_MODE_PP);
    // gpio_write(BITMASK(pin_num), GPIO_HIGH);
    gpio_set_direction(BITMASK(pin_num), GPIO_OUTPUT);
}

//设置输出脚恢复中断功能-上拉-输入-下降沿
void SetGpio_Output_To_Input(uint32_t pin_num)
{
    // printf("int:%d\r\n",pin_num);
    pinmux_config(pin_num, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(pin_num), PMU_PIN_MODE_PU);
    gpio_set_direction(BITMASK(pin_num), GPIO_INPUT);
    gpio_set_interrupt(BITMASK(pin_num), GPIO_FALLING_EDGE);
}

//设置输出脚恢复中断功能-下拉-输入-上升沿
void SetGpio_Output_To_Input_Down(uint32_t pin_num)
{
    // printf("int:%d\r\n",pin_num);
    pinmux_config(pin_num, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(pin_num), PMU_PIN_MODE_PD);
    gpio_set_direction(BITMASK(pin_num), GPIO_INPUT);
    gpio_set_interrupt(BITMASK(pin_num), GPIO_RISING_EDGE);
}
//设置io的电平
void set_gpio_write(uint32_t pin_num, gpio_level_t gpio_level)
{
    if (pin_num >= 32)
    {
#ifdef CONFIG_HS6621P
        gpio_write_ex(BITMASK(pin_num - 32), gpio_level);
#else
        printf("mcu is not pg\r\n");
#endif
    }
    else
    {
        gpio_write(BITMASK(pin_num), gpio_level);
    }
}
//设置io的模式，方向，中断
void set_io_port_to_gpio(uint32_t pin_num,
                         pinmux_t pin_pinmux,
                         pmu_pin_mode_t pin_mode,
                         gpio_direction_t pin_direction,
                         gpio_trigger_type_t pin_interrupt_type,
                         gpio_callback_t pin_interrupt_callback,
                         pmu_pin_wakeup_type_t wakeup_type,
                         gpio_callback_t sleep_callback)
{
    if (pin_num >= 32)
    {
#ifdef CONFIG_HS6621P
        pinmux_config(pin_num, pin_pinmux);
        pmu_pin_mode_set_ex(BITMASK(pin_num - 32), pin_mode);
        gpio_set_direction_ex(BITMASK(pin_num - 32), pin_direction);
        gpio_set_interrupt_ex(BITMASK(pin_num - 32), pin_interrupt_type);
        if (pin_interrupt_callback)
            gpio_set_interrupt_callback_ex(pin_interrupt_callback);

        pmu_wakeup_pin_set_ex(BITMASK(pin_num - 32), wakeup_type);
        if (sleep_callback)
            pmu_wakeup_pin_register_callback_ex(sleep_callback, sleep_callback);
#else
        printf("mcu is not pg\r\n");
#endif
    }
    else
    {
        pinmux_config(pin_num, pin_pinmux);
        pmu_pin_mode_set(BITMASK(pin_num), pin_mode);
        gpio_set_direction(BITMASK(pin_num), pin_direction);
        gpio_set_interrupt(BITMASK(pin_num), pin_interrupt_type);
        if (pin_interrupt_callback)
            gpio_set_interrupt_callback(pin_interrupt_callback);

        pmu_wakeup_pin_set(BITMASK(pin_num), wakeup_type);
        if (sleep_callback)
            pmu_wakeup_pin_register_callback(sleep_callback, sleep_callback);
    }
}
#ifdef CONFIG_HS6621P
/**
 * @brief  GPIO拓展中断服务函数
 *
 * @note   只处理gpio32开始的IO
 */
void KOS_GPIO_ex_IRQHandler(uint32_t pin_mask)
{
    log_debug("GPIO_ex button: %08X\n", pin_mask);

    for (int i = 0; i < OSAL_LENGTH(gpio_map); i++)
    {
        if (gpio_map[i].port >= 32)
        {
            if (pin_mask & (BITMASK(gpio_map[i].port - 32)))
            {
                Device_IRQHandler(gpio_map[i].pin, NULL, 0);
                // break;//一次可能多个bit,所以要一次扫描完所有的
                log_debug("button22: %08d,port:%08d\n", gpio_map[i].pin, gpio_map[i].port);
            }
        }
    }
}
#endif

/**
 * @brief  GPIO中断服务函数
 *
 * @note
 */
void KOS_GPIO0_IRQHandler(uint32_t pin_mask)
{
    // log_debug("G:%u\n", pin_mask);//不要打开中断打印不然会导致读取感应把手版本号读取不到

    for (int i = 0; i < OSAL_LENGTH(gpio_map); i++)
    {
        if (gpio_map[i].port < 32)
        {
            if (pin_mask & (BITMASK(gpio_map[i].port)))
            {
                Device_IRQHandler(gpio_map[i].pin, NULL, 0);
                // break;//一次可能多个bit,所以要一次扫描完所有的
                // log_debug("b:%d,p:%d", gpio_map[i].pin,gpio_map[i].port);
            }
        }
    }
}

/**
 * @brief  查找GPIO映射表
 *
 * @note
 * @param  dev：虚拟设备
 */
static GpioMap_stu_t *Gpio_FindMap(VirtualHardware_enum_t dev)
{
    for (int i = 0; i < sizeof(gpio_map) / sizeof(gpio_map[0]); i++)
    {
        if (dev == gpio_map[i].pin)
        {
            return (GpioMap_stu_t *)&gpio_map[i];
        }
    }
    return NULL;
}

void test_1(void)
{
    #if defined(B2B_INT_PIN)	
    log_debug("GPIO_Disable 1\n");
    pinmux_config(B2B_INT_PIN, PINMUX_GPIO_MODE_CFG); // B2B_INT_PIN
    pmu_pin_mode_set(BITMASK(B2B_INT_PIN), PMU_PIN_MODE_PU);
    gpio_set_direction(BITMASK(B2B_INT_PIN), GPIO_INPUT);
    gpio_set_interrupt(BITMASK(B2B_INT_PIN), GPIO_FALLING_EDGE);
    pmu_wakeup_pin_set(BITMASK(B2B_INT_PIN), PMU_PIN_WAKEUP_LOW_LEVEL);

    log_debug("GPIO_Disable 2\n");
    #endif
}

/**
 * @brief  唤醒检测IO读取电平操作，节省在休眠下检测的时间开销
 *
 * @note
 * @param  dev：虚拟设备
 */
int32_t GPIO_WakeupCheckReadPinIo(uint32_t pin_num)
{
    return (((int32_t)gpio_read(BITMASK(pin_num))) >> (pin_num)) & 0x1;
}

/**
 * @brief  唤醒检测IO使能光耦操作，节省在休眠下系统调度的时间开销
 *
 * @note
 * @param  
 */
int32_t GPIO_WakeupCheckPowerOn(uint32_t pin_num, uint8_t power)
{
    if (power == RESET)
    {
        gpio_write(BITMASK(pin_num), GPIO_LOW);
    }
    else
    {
        gpio_write(BITMASK(pin_num), GPIO_HIGH);
    }
    
    return (((int32_t)gpio_read(BITMASK(pin_num))) >> (pin_num)) & 0x1;
}

/**
 * @brief  失能GPIO接口
 *
 * @note
 * @param  dev：虚拟设备
 */
static int8_t GPIO_Disable(VirtualHardware_enum_t dev)
{
    GpioMap_stu_t *p = Gpio_FindMap(dev);

    if (p == NULL)
    {
        return -1;
    }
    if (dev == vPIN_B13)
    {
        if (gpio_read(BITMASK(p->port)))
        {
            gpio_set_interrupt(BITMASK(p->port), GPIO_FALLING_EDGE);
            pmu_wakeup_pin_set(BITMASK(p->port), PMU_PIN_WAKEUP_LOW_LEVEL);
        }
        else
        {
            gpio_set_interrupt(BITMASK(p->port), GPIO_RISING_EDGE);
            pmu_wakeup_pin_set(BITMASK(p->port), PMU_PIN_WAKEUP_HIGH_LEVEL);
        }
    }
    return 0;
}

/**
 * @brief  使能GPIO接口
 *
 * @note
 * @param  dev：虚拟设备
 */
static int8_t GPIO_Enable(VirtualHardware_enum_t dev)
{
    GpioMap_stu_t *p = Gpio_FindMap(dev);

    if (p == NULL)
    {
        return -1;
    }

    return 0;
}

/**
 * @brief  读取GPIO接口
 *
 * @note
 * @param  dev：虚拟设备
 * @param  data：GPIO设备没用到，传NULL即可
 * @param  len：GPIO设备没用到，传0即可
 * @param  param：GPIO设备没用到，传0即可
 */
static int32_t GPIO_Read(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param)
{
    GpioMap_stu_t *p = Gpio_FindMap(dev);

    if (p == NULL)
    {
        return -1;
    }

#ifdef CONFIG_HS6621C

    if (dev == vPIN_C29)
    {
        return !((((int32_t)gpio_read(BITMASK(p->port))) >> (p->port)) & 0x1);
    }
    else
    {
        return (((int32_t)gpio_read(BITMASK(p->port))) >> (p->port)) & 0x1;
    }

#elif CONFIG_HS6621P
    if (p->port >= 32)
    {
        return (((int32_t)gpio_read_ex(BITMASK(p->port - 32))) >> (p->port - 32)) & 0x1;
    }
    else
    {
        return (((int32_t)gpio_read(BITMASK(p->port))) >> (p->port)) & 0x1;
    }
#endif
}

/**
 * @brief  写GPIO接口
 *
 * @note
 * @param  dev：虚拟设备
 * @param  data：GPIO设备没用到，传NULL即可
 * @param  len：GPIO设备没用到，传0即可
 * @param  param：0:置低电平 1:置高电平 其他:取反电平
 */
static int32_t GPIO_Write(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param)
{
    GpioMap_stu_t *p = Gpio_FindMap(dev);

    if (p == NULL)
    {
        return -1;
    }

    if (param == 0)
    {
        if (p->port >= 32)
        {
            #ifdef CONFIG_HS6621P
            gpio_write_ex(BITMASK(p->port - 32), GPIO_LOW);
            #else
            printf("mcu is not pg\r\n");
            #endif
        }
        else
        {
            if (dev == vPIN_L18 || dev == vPIN_C29)
            {
                gpio_write(BITMASK(p->port), GPIO_HIGH);
                log_debug("write disable  %d",dev);

            }
            else
            {
                gpio_write(BITMASK(p->port), GPIO_LOW);
                if (dev == vPIN_C46)
                {
                    log_debug("GPIO_Write 0\r\n");
                }
            }
        }
    }
    else if (param == 1)
    {
        if (p->port >= 32)
        {
            #ifdef CONFIG_HS6621P
            gpio_write_ex(BITMASK(p->port - 32), GPIO_HIGH);
            #else
            printf("mcu is not pg\r\n");
            #endif
        }
        else
        {
            if (dev == vPIN_L18 || dev == vPIN_C29 )
            {
                gpio_write(BITMASK(p->port), GPIO_LOW);
                log_debug("write enable  %d",dev);
            }
            else
            {
                gpio_write(BITMASK(p->port), GPIO_HIGH);
                if (dev == vPIN_C46)
                {
                    log_debug("GPIO_Write 1\r\n");
                }
                
            }
        }
    }
    else if (param == 2)
    {
        gpio_toggle(BITMASK(p->port));
    }
    else if(param ==  PIN_MODE_OD_DRIVE) //osport传入参数是开漏输出，实际配置是推挽输出
    {
        pinmux_config(BITMASK(p->port), PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(p->port), PMU_PIN_MODE_PP);
        gpio_set_direction(BITMASK(p->port), GPIO_OUTPUT);
        // printf("PIN_MODE_OD_DRIVE p->port:%d\n",p->port);
    }
    else if (param == PIN_MODE_INPUT_PULLUP)
    {
        pinmux_config(BITMASK(p->port), PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(p->port), PMU_PIN_MODE_PU);
        gpio_set_direction(BITMASK(p->port), GPIO_INPUT);
        gpio_set_interrupt(BITMASK(p->port), GPIO_FALLING_EDGE);
        //  printf("PIN_MODE_INPUT_PULLUP p->port:%d\n",p->port);
    }
    /*******************************************************************************/

    return (int32_t)param;
}



/**
 * @brief  GPIO唤醒中断唤醒
 *
 * @note
 * @param  pin_mask：pin号
 */
void wakeup_gpio_handler(uint32_t pin_mask)
{
    static int counter = 0;
    log_debug("wake_gpio: %08X [%d]\n", pin_mask, counter++);
    for (int i = 0; i < OSAL_LENGTH(gpio_map); i++)
    {
        if (gpio_map[i].port < 32)
        {
            if (pin_mask & (BITMASK(gpio_map[i].port)))
            {
                #ifdef HALL2_PIN              
                        bool hall2_level = 0;
                        hall2_level = HS_GPIO_READ(HALL2_PIN);
                        HS_WAKEUP_PIN_SET(HALL2_PIN, (pmu_pin_wakeup_type_t)(!hall2_level));                                                                           
                #endif
                Device_IRQHandler(gpio_map[i].pin, NULL, 0);
                // break;//一次可能多个bit,所以要一次扫描完所有的
                log_debug("wake pin: %d,  port:%d\n", gpio_map[i].pin, gpio_map[i].port);
            }
        }
    }
}

#ifdef CONFIG_HS6621P
/**
 * @brief  GPIO拓展唤醒中断唤醒
 *
 * @note
 * @param  pin_mask：pin号
 */
void wakeup_gpio_ex_handler(uint32_t pin_mask)
{
    static int counter1 = 0;
    log_debug("wake_gpio_ex: %08X [%d]\n", pin_mask, counter1++);

    for (int i = 0; i < OSAL_LENGTH(gpio_map); i++)
    {
        if (gpio_map[i].port >= 32)
        {
            if (pin_mask & (BITMASK(gpio_map[i].port - 32)))
            {
                Device_IRQHandler(gpio_map[i].pin, NULL, 0);
                // break;//一次可能多个bit,所以要一次扫描完所有的
                log_debug("wake pin: %d,  port:%d\n", gpio_map[i].pin, gpio_map[i].port);
            }
        }
    }
}
#endif

/**
 * @brief OS2OS 设置唤醒
 * 
 * @param 
 */
void Gpio_SetB2BWakeup(void)
{
#if (B2B_INT_UART_RX)
#if defined(OS2OS_UART_TX_PIN)
    // 用TX唤醒RX
    pinmux_config(OS2OS_UART_TX_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(OS2OS_UART_TX_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(OS2OS_UART_TX_PIN), GPIO_LOW);
    gpio_set_direction(BITMASK(OS2OS_UART_TX_PIN), GPIO_OUTPUT);
#endif
#else
#if defined(B2B_INT_PIN)
    // 直接拉IO唤醒
    pinmux_config(B2B_INT_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(B2B_INT_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(B2B_INT_PIN), GPIO_LOW);
    gpio_set_direction(BITMASK(B2B_INT_PIN), GPIO_OUTPUT);
#endif
#endif
}

/**
 * @brief  GPIO引脚初始化
 *
 * @note
 */
static void GpioHal_init(void)
{
    /******************************子设备 pin***********************************************/
    //输入设置
#if defined(FINGER_INT_PIN)
    pinmux_config(FINGER_INT_PIN, PINMUX_GPIO_MODE_CFG); // UART FINGER
    pmu_pin_mode_set(BITMASK(FINGER_INT_PIN), PMU_PIN_MODE_PD);
	gpio_set_direction(BITMASK(FINGER_INT_PIN), GPIO_INPUT); 
    gpio_set_interrupt(BITMASK(FINGER_INT_PIN), GPIO_RISING_EDGE);
    pmu_wakeup_pin_set(BITMASK(FINGER_INT_PIN), PMU_PIN_WAKEUP_HIGH_LEVEL);
#endif

#if defined(FINGER_POWER_PIN)	
    pinmux_config(FINGER_POWER_PIN, PINMUX_GPIO_MODE_CFG); // UART FING-POWER
    pmu_pin_mode_set(BITMASK(FINGER_POWER_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(FINGER_POWER_PIN), GPIO_LOW); // UART FING-POWER
    gpio_set_direction(BITMASK(FINGER_POWER_PIN), GPIO_OUTPUT);
#endif

#if defined(TOUCH1_RST_PIN)	
    pinmux_config(TOUCH1_RST_PIN, PINMUX_GPIO_MODE_CFG); // TOUCH-RST
    pmu_pin_mode_set(BITMASK(TOUCH1_RST_PIN), PMU_PIN_MODE_PP);
	  gpio_write(BITMASK(TOUCH1_RST_PIN), GPIO_HIGH);
    gpio_set_direction(BITMASK(TOUCH1_RST_PIN), GPIO_OUTPUT);
#endif

#if defined(TOUCH1_INT_PIN)	
    pinmux_config(TOUCH1_INT_PIN, PINMUX_GPIO_MODE_CFG); // TOUCH-INT1
    pmu_pin_mode_set(BITMASK(TOUCH1_INT_PIN), PMU_PIN_MODE_PU);
	gpio_set_direction(BITMASK(TOUCH1_INT_PIN), GPIO_INPUT); 
    gpio_set_interrupt(BITMASK(TOUCH1_INT_PIN), GPIO_FALLING_EDGE);
    pmu_wakeup_pin_set(BITMASK(TOUCH1_INT_PIN), PMU_PIN_WAKEUP_LOW_LEVEL);
#endif 

#if defined(B2B_INT_PIN)	
    pinmux_config(B2B_INT_PIN, PINMUX_GPIO_MODE_CFG); // B2B_INT_PIN
    pmu_pin_mode_set(BITMASK(B2B_INT_PIN), PMU_PIN_MODE_PU);
	gpio_set_direction(BITMASK(B2B_INT_PIN), GPIO_INPUT);
    gpio_set_interrupt(BITMASK(B2B_INT_PIN), GPIO_FALLING_EDGE);
    pmu_wakeup_pin_set(BITMASK(B2B_INT_PIN), PMU_PIN_WAKEUP_LOW_LEVEL);
#endif

/* 电机使能引脚 */
#if defined(MOTOR_EN_PIN)
     pinmux_config(MOTOR_EN_PIN, PINMUX_GPIO_MODE_CFG); //电机SLEEP
     pmu_pin_mode_set(BITMASK(MOTOR_EN_PIN), PMU_PIN_MODE_PP);
	 gpio_write(BITMASK(MOTOR_EN_PIN), GPIO_LOW);
     gpio_set_direction(BITMASK(MOTOR_EN_PIN), GPIO_OUTPUT);
#endif

#if defined(LED_WHIT_PIN)
    pinmux_config(LED_WHIT_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(LED_WHIT_PIN), PMU_PIN_MODE_PP);
	gpio_write(BITMASK(LED_WHIT_PIN), GPIO_LOW);
    gpio_set_direction(BITMASK(LED_WHIT_PIN), GPIO_OUTPUT);
#endif

#if defined(LED_GREEN_PIN)
    pinmux_config(LED_GREEN_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(LED_GREEN_PIN), PMU_PIN_MODE_PP);
	gpio_write(BITMASK(LED_GREEN_PIN), GPIO_HIGH);
    gpio_set_direction(BITMASK(LED_GREEN_PIN), GPIO_OUTPUT);
#endif

#if defined(LED_RED_PIN)
    pinmux_config(LED_RED_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(LED_RED_PIN), PMU_PIN_MODE_PP);
	gpio_write(BITMASK(LED_RED_PIN), GPIO_HIGH); 
    gpio_set_direction(BITMASK(LED_RED_PIN), GPIO_OUTPUT);
#endif

#if defined(PWR_AUDIO_LED_PIN)
    pinmux_config(PWR_AUDIO_LED_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(PWR_AUDIO_LED_PIN), PMU_PIN_MODE_PP);
	gpio_write(BITMASK(PWR_AUDIO_LED_PIN), GPIO_HIGH);//  GPIO_LOW
    gpio_set_direction(BITMASK(PWR_AUDIO_LED_PIN), GPIO_OUTPUT);
#endif

#if defined(LED_G_PIN)
    pinmux_config(LED_G_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(LED_G_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(LED_G_PIN), GPIO_HIGH); 
    gpio_set_direction(BITMASK(LED_G_PIN), GPIO_OUTPUT);
#endif

#if defined(LED_R_PIN)
    pinmux_config(LED_R_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(LED_R_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(LED_R_PIN), GPIO_HIGH); 
    gpio_set_direction(BITMASK(LED_R_PIN), GPIO_OUTPUT);
#endif
#if defined(AUDIO_ONELINE_DATA_PIN)
    pinmux_config(AUDIO_ONELINE_DATA_PIN, PINMUX_GPIO_MODE_CFG); 
    pmu_pin_mode_set(BITMASK(AUDIO_ONELINE_DATA_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(AUDIO_ONELINE_DATA_PIN), GPIO_HIGH); 
    gpio_set_direction(BITMASK(AUDIO_ONELINE_DATA_PIN), GPIO_OUTPUT);
#endif
#if defined(AUDIO_ONELINE_BUSY_PIN)
    pinmux_config(AUDIO_ONELINE_BUSY_PIN, PINMUX_GPIO_MODE_CFG); 
    pmu_pin_mode_set(BITMASK(AUDIO_ONELINE_BUSY_PIN), PMU_PIN_MODE_FLOAT);
    gpio_set_direction(BITMASK(AUDIO_ONELINE_BUSY_PIN), GPIO_INPUT); 
#endif

#if defined(PIN_BLE_WAKE_MCU)
    // HS_PIN_FUNC_SET(PIN_BLE_WAKE_MCU, PINMUX_GPIO_MODE_CFG); // BLE WAKE MCU 
    // HS_PIN_MODE_SET(PIN_BLE_WAKE_MCU, PMU_PIN_MODE_PD);
    // HS_GPIO_DIR_SET(PIN_BLE_WAKE_MCU, GPIO_INPUT);
    // HS_GPIO_SET_INTERRUPT(PIN_BLE_WAKE_MCU, GPIO_RISING_EDGE);
    // HS_WAKEUP_PIN_SET(PIN_BLE_WAKE_MCU, PMU_PIN_WAKEUP_HIGH_LEVEL);


    pinmux_config(PIN_BLE_WAKE_MCU, PINMUX_GPIO_MODE_CFG); // B2B_INT_PIN
    pmu_pin_mode_set(BITMASK(PIN_BLE_WAKE_MCU), PMU_PIN_MODE_PD);
	gpio_set_direction(BITMASK(PIN_BLE_WAKE_MCU), GPIO_INPUT);
    gpio_set_interrupt(BITMASK(PIN_BLE_WAKE_MCU), GPIO_RISING_EDGE);
    pmu_wakeup_pin_set(BITMASK(PIN_BLE_WAKE_MCU), PMU_PIN_WAKEUP_HIGH_LEVEL);

#endif

#if defined(PIN_MCU_WAKE_BLE)
    // HS_PIN_FUNC_SET(PIN_MCU_WAKE_BLE, PINMUX_GPIO_MODE_CFG); // MCU WAKE BLE 
    // HS_PIN_MODE_SET(PIN_MCU_WAKE_BLE, PMU_PIN_MODE_PP);
    // // HS_GPIO_WRITE(PIN_MCU_WAKE_BLE, GPIO_HIGH);//待定 GPIO_LOW
    // HS_GPIO_WRITE(PIN_MCU_WAKE_BLE, GPIO_LOW); 
    // HS_GPIO_DIR_SET(PIN_MCU_WAKE_BLE, GPIO_OUTPUT);


    pinmux_config(PIN_MCU_WAKE_BLE, PINMUX_GPIO_MODE_CFG); //电机SLEEP
     pmu_pin_mode_set(BITMASK(PIN_MCU_WAKE_BLE), PMU_PIN_MODE_PP);
	 gpio_write(BITMASK(PIN_MCU_WAKE_BLE), GPIO_LOW);
     gpio_set_direction(BITMASK(PIN_MCU_WAKE_BLE), GPIO_OUTPUT);
#endif

#if defined(BUTTON_ONOFF_PIN)
    // HS_PIN_FUNC_SET(BUTTON_ONOFF_PIN, PINMUX_GPIO_MODE_CFG);
    // HS_PIN_MODE_SET(BUTTON_ONOFF_PIN, PMU_PIN_MODE_PU);
    // HS_GPIO_DIR_SET(BUTTON_ONOFF_PIN, GPIO_INPUT);
    // HS_GPIO_SET_INTERRUPT(BUTTON_ONOFF_PIN, GPIO_FALLING_EDGE);
    // HS_WAKEUP_PIN_SET(BUTTON_ONOFF_PIN, PMU_PIN_WAKEUP_LOW_LEVEL);


    pinmux_config(BUTTON_ONOFF_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(BUTTON_ONOFF_PIN), PMU_PIN_MODE_PU);
	gpio_set_direction(BITMASK(BUTTON_ONOFF_PIN), GPIO_INPUT);
    gpio_set_interrupt(BITMASK(BUTTON_ONOFF_PIN), GPIO_FALLING_EDGE);
    pmu_wakeup_pin_set(BITMASK(BUTTON_ONOFF_PIN), PMU_PIN_WAKEUP_LOW_LEVEL);

#endif

#if defined(HOMEKIT_NRST_PIN)
    pinmux_config(HOMEKIT_NRST_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(HOMEKIT_NRST_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(HOMEKIT_NRST_PIN), GPIO_HIGH); //  GPIO_LOW
    gpio_set_direction(BITMASK(HOMEKIT_NRST_PIN), GPIO_OUTPUT);
#endif

#if defined(BUTTON_RESET_PIN)
    //中断设置
     pinmux_config(BUTTON_RESET_PIN, PINMUX_GPIO_MODE_CFG);     //恢复出厂键
     gpio_set_direction(BITMASK(BUTTON_RESET_PIN), GPIO_INPUT); 
     pmu_pin_mode_set(BITMASK(BUTTON_RESET_PIN), PMU_PIN_MODE_PU);
     gpio_set_interrupt(BITMASK(BUTTON_RESET_PIN), GPIO_FALLING_EDGE);
     pmu_wakeup_pin_set(BITMASK(BUTTON_RESET_PIN), PMU_PIN_WAKEUP_LOW_LEVEL);
#endif

#if defined(HOMEKIT_PIN)
    pinmux_config(HOMEKIT_PIN, PINMUX_GPIO_MODE_CFG);     
    gpio_set_direction(BITMASK(HOMEKIT_PIN), GPIO_INPUT); 
    pmu_pin_mode_set(BITMASK(HOMEKIT_PIN), PMU_PIN_MODE_PU);
    gpio_set_interrupt(BITMASK(HOMEKIT_PIN), GPIO_FALLING_EDGE);
    pmu_wakeup_pin_set(BITMASK(HOMEKIT_PIN), PMU_PIN_WAKEUP_LOW_LEVEL);
#endif

	  /* 光耦 */
#ifdef OPTO1_PIN 
    // HS_PIN_FUNC_SET(OPTO1_PIN, PINMUX_GPIO_MODE_CFG);
    // HS_PIN_MODE_SET(OPTO1_PIN, PMU_PIN_MODE_FLOAT);
    // HS_GPIO_DIR_SET(OPTO1_PIN, GPIO_INPUT);

    pinmux_config(OPTO1_PIN, PINMUX_GPIO_MODE_CFG);     
    gpio_set_direction(BITMASK(OPTO1_PIN), GPIO_INPUT); 
    pmu_pin_mode_set(BITMASK(OPTO1_PIN), PMU_PIN_MODE_FLOAT);
#endif
#ifdef OPTO2_PIN
    // HS_PIN_FUNC_SET(OPTO2_PIN, PINMUX_GPIO_MODE_CFG);
    // HS_PIN_MODE_SET(OPTO2_PIN, PMU_PIN_MODE_FLOAT);
    // HS_GPIO_DIR_SET(OPTO2_PIN, GPIO_INPUT);

    pinmux_config(OPTO2_PIN, PINMUX_GPIO_MODE_CFG);     
    gpio_set_direction(BITMASK(OPTO2_PIN), GPIO_INPUT); 
    pmu_pin_mode_set(BITMASK(OPTO2_PIN), PMU_PIN_MODE_FLOAT);
#endif

#ifdef OPTO3_PIN
    // HS_PIN_FUNC_SET(OPTO3_PIN, PINMUX_GPIO_MODE_CFG);
    // HS_PIN_MODE_SET(OPTO3_PIN, PMU_PIN_MODE_FLOAT);
    // HS_GPIO_DIR_SET(OPTO3_PIN, GPIO_INPUT);

    pinmux_config(OPTO3_PIN, PINMUX_GPIO_MODE_CFG);     
    gpio_set_direction(BITMASK(OPTO3_PIN), GPIO_INPUT); 
    pmu_pin_mode_set(BITMASK(OPTO3_PIN), PMU_PIN_MODE_FLOAT);
#endif

#ifdef HALL2_PIN
    // HS_PIN_FUNC_SET(HALL2_PIN, PINMUX_GPIO_MODE_CFG);
    // HS_PIN_MODE_SET(HALL2_PIN, PMU_PIN_MODE_FLOAT);
    // HS_GPIO_DIR_SET(HALL2_PIN, GPIO_INPUT);
    // HS_GPIO_SET_INTERRUPT(HALL2_PIN, GPIO_BOTH_EDGE);
    // HS_WAKEUP_PIN_SET(HALL2_PIN, PMU_PIN_WAKEUP_LOW_LEVEL);

    pinmux_config(HALL2_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(HALL2_PIN), PMU_PIN_MODE_FLOAT);
	gpio_set_direction(BITMASK(HALL2_PIN), GPIO_INPUT);
    gpio_set_interrupt(BITMASK(HALL2_PIN), GPIO_BOTH_EDGE);
    pmu_wakeup_pin_set(BITMASK(HALL2_PIN), PMU_PIN_WAKEUP_LOW_LEVEL);
#endif

	/* 电机供电电压控制脚（电压高时进行降压） */
#if defined(MOTOR_EN_VCC_PIN)
    // HS_PIN_FUNC_SET(MOTOR_EN_VCC_PIN, PINMUX_GPIO_MODE_CFG);
    // HS_PIN_MODE_SET(MOTOR_EN_VCC_PIN, PMU_PIN_MODE_PP);
	// HS_GPIO_WRITE(MOTOR_EN_VCC_PIN, GPIO_LOW);
    // HS_GPIO_DIR_SET(MOTOR_EN_VCC_PIN, GPIO_OUTPUT);   


    pinmux_config(MOTOR_EN_VCC_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(MOTOR_EN_VCC_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(MOTOR_EN_VCC_PIN), GPIO_LOW); 
    gpio_set_direction(BITMASK(MOTOR_EN_VCC_PIN), GPIO_OUTPUT);

#endif
    /* 光耦使能引脚 */
#if defined(OPTO_EN_PIN)
    // HS_PIN_FUNC_SET(OPTO_EN_PIN, PINMUX_GPIO_MODE_CFG);
    // HS_PIN_MODE_SET(OPTO_EN_PIN, PMU_PIN_MODE_PP);
    // HS_GPIO_WRITE(OPTO_EN_PIN, GPIO_HIGH);//
	// HS_GPIO_DIR_SET(OPTO_EN_PIN, GPIO_OUTPUT);

    pinmux_config(OPTO_EN_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(OPTO_EN_PIN), PMU_PIN_MODE_PP);
    gpio_write(BITMASK(OPTO_EN_PIN), GPIO_HIGH); 
    gpio_set_direction(BITMASK(OPTO_EN_PIN), GPIO_OUTPUT);
#endif

#if defined(USB_OK_PIN)
    // HS_PIN_FUNC_SET(USB_OK_PIN, PINMUX_GPIO_MODE_CFG); 
    // HS_PIN_MODE_SET(USB_OK_PIN, PMU_PIN_MODE_PD);
    // HS_GPIO_DIR_SET(USB_OK_PIN, GPIO_INPUT);
    // HS_GPIO_SET_INTERRUPT(USB_OK_PIN, GPIO_RISING_EDGE);
    // HS_WAKEUP_PIN_SET(USB_OK_PIN, PMU_PIN_WAKEUP_HIGH_LEVEL);  

    pinmux_config(USB_OK_PIN, PINMUX_GPIO_MODE_CFG);     
    gpio_set_direction(BITMASK(USB_OK_PIN), GPIO_INPUT); 
    pmu_pin_mode_set(BITMASK(USB_OK_PIN), PMU_PIN_MODE_PD);
    gpio_set_interrupt(BITMASK(USB_OK_PIN), GPIO_RISING_EDGE);
    pmu_wakeup_pin_set(BITMASK(USB_OK_PIN), PMU_PIN_WAKEUP_HIGH_LEVEL);
#endif

    gpio_set_interrupt_callback(KOS_GPIO0_IRQHandler);                          //唤醒时的中断
    pmu_wakeup_pin_register_callback(wakeup_gpio_handler, wakeup_gpio_handler); //休眠时的中断唤醒

	//输出设置
}

/**
 * @brief tim_timer_handler()
 *
 * @return
 **/

/************************************************************************************************/
static void Gpio_Init(void)
{
    printf("gpio init\r\n");
    /* 1、初始化硬件 */
    GpioHal_init();
    printf("gpio init1111........\r\n");

    /* 2、将底层设备具体操作接口，挂到设备控制块里面 */
    Device_stu_t *dcb = Device_GetDeviceCtrlBlock(vPIN);
    dcb->enable = GPIO_Enable;
    dcb->disable = GPIO_Disable;
    dcb->write = GPIO_Write;
    dcb->read = GPIO_Read;
    printf("gpio init........\r\n");
}
INIT_PREV_EXPORT(Gpio_Init); //读取感应把手版本需要将次任务提前
