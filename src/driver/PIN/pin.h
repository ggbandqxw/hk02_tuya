/**
 * @file pin.h
 * @brief 
 * 
 * @version 0.1
 * @date 2024-01-13
 * 
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 * 
 * @note          鼎新同创・智能锁
 *  
 * @par 修改日志: 
 * <1> 2024-01-15 v0.1 huangliwu 创建初始版本
 * *************************************************************************
 */
#ifndef _PIN_H_
#define _PIN_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*******************************************************************************
 * INCLUDES
 */
#include "peripheral.h"
/*******************************************************************************
 * MACROS
 */

/* 昂瑞微IO操作宏 */
#define PIN_HIGH 1
#define PIN_LOW 0

#define PIN_INVALID 0xFFFF
#if defined (CONFIG_HS6621P)
#define HS6621P_BIT_MASK(bitNum) ((bitNum != PIN_INVALID) ? ((bitNum >= 32) ? (1u << (bitNum - 32)) : (1u << (bitNum))) : 0)

/* IO功能设置 */
#define HS_PIN_FUNC_SET(pin, func) ((pin != PIN_INVALID) ? (pinmux_config(pin, func)) : 0)

#define HS_PIN_DRIVEN_CURRENT_SET(pin_mask, pin_driven_current) (pmu_pin_driven_current_set(pin_mask, pin_driven_current))

/* IO模式设置 */
#define HS_PIN_MODE_SET(pin, mode) ((pin != PIN_INVALID) ? ((pin >= 32) ? (pmu_pin_mode_set_ex(HS6621P_BIT_MASK(pin), mode)) : (pmu_pin_mode_set(HS6621P_BIT_MASK(pin), mode))) : 0)
/* IO方向设置 */
#define HS_GPIO_DIR_SET(pin, dir) ((pin != PIN_INVALID) ? ((pin >= 32) ? (gpio_set_direction_ex(HS6621P_BIT_MASK(pin), dir)) : (gpio_set_direction(HS6621P_BIT_MASK(pin), dir))) : 0)
/* 读IO */
#define HS_GPIO_READ(pin) ((pin != PIN_INVALID) ? ((pin >= 32) ? (gpio_read_ex(HS6621P_BIT_MASK(pin))) : (gpio_read(HS6621P_BIT_MASK(pin)))) : 0)

/* 写IO */
#define HS_GPIO_WRITE(pin, level) ((pin != PIN_INVALID) ? ((pin >= 32) ? (gpio_write_ex(HS6621P_BIT_MASK(pin), level)) : (gpio_write(HS6621P_BIT_MASK(pin), level))) : 0)

/* 写IO */
#define HS_GPIO_WRITE_MASK(pin_mask, level) (gpio_write(pin_mask, level))

/* 唤醒引脚设置 */
#define HS_WAKEUP_PIN_SET(pin, trigger_type) ((pin != PIN_INVALID) ? ((pin >= 32) ? (pmu_wakeup_pin_set_ex(HS6621P_BIT_MASK(pin), trigger_type)) : (pmu_wakeup_pin_set(HS6621P_BIT_MASK(pin), trigger_type))) : 0)

/* 中断引脚配置 */
#define HS_GPIO_SET_INTERRUPT(pin, trigger_type) ((pin != PIN_INVALID) ? ((pin >= 32) ? (gpio_set_interrupt_ex(HS6621P_BIT_MASK(pin), trigger_type)) : (gpio_set_interrupt(HS6621P_BIT_MASK(pin), trigger_type))) : 0)

#elif defined (CONFIG_HS6621C)

#define HS6621P_BIT_MASK(bitNum) ((bitNum != PIN_INVALID) ? 1u << (bitNum) : 0)

/* IO功能设置 */
#define HS_PIN_FUNC_SET(pin, func) ((pin != PIN_INVALID) ? (pinmux_config(pin, func)) : 0)

#define HS_PIN_DRIVEN_CURRENT_SET(pin_mask, pin_driven_current) pmu_pin_driven_current_set(pin_mask, pin_driven_current)

/* IO模式设置 */
#define HS_PIN_MODE_SET(pin, mode) (pin != PIN_INVALID) ? pmu_pin_mode_set(HS6621P_BIT_MASK(pin), (mode)) : 0
/* IO方向设置 */
#define HS_GPIO_DIR_SET(pin, dir) (pin != PIN_INVALID) ? gpio_set_direction(HS6621P_BIT_MASK(pin), (dir)) : 0
/* 读IO */
#define HS_GPIO_READ(pin) ((pin != PIN_INVALID) ? gpio_read(HS6621P_BIT_MASK(pin)) : 0)

/* 写IO */
#define HS_GPIO_WRITE(pin, level) ((pin != PIN_INVALID) ? gpio_write(HS6621P_BIT_MASK(pin), level) : 0)

/* 写IO */
#define HS_GPIO_WRITE_MASK(pin_mask, level) (gpio_write(pin_mask, level))

/* 唤醒引脚设置 */
#define HS_WAKEUP_PIN_SET(pin, trigger_type) ((pin != PIN_INVALID) ? pmu_wakeup_pin_set(HS6621P_BIT_MASK(pin), trigger_type) : 0)

/* 中断引脚配置 */
#define HS_GPIO_SET_INTERRUPT(pin, trigger_type) ((pin != PIN_INVALID) ? gpio_set_interrupt(HS6621P_BIT_MASK(pin), trigger_type) : 0)

#else
#error "not support mcu"
#endif
    /*******************************************************************************
     * CONSTANTS
     */

    /*******************************************************************************
     * TYPEDEFS
     */

    /*******************************************************************************
     * EXTERNAL VARIABLES
     */

    /*******************************************************************************
     * EXTERNAL FUNCTIONS
     */

#ifdef __cplusplus
}
#endif

#endif /* _PIN_H_ */