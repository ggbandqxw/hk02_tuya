/**
 * @file nb.c
 * @brief
 *
 * @version 0.1
 * @date 2023-09-03
 *
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 *
 * @note          鼎新同创・智能锁
 *
 * @par 修改日志:
 * <1> 2023-09-03 v0.1 tianshidong 创建初始版本
 * *************************************************************************
 */
#include "rwip.h" // RW SW initialization
#include "co_debug.h"
#include "app.h"
#include "peripheral.h"
#include "co_debug.h"

#include "device.h"
#include "component.h"
#include "debug_config.h"

#define NB_UART_TX_PIN 0
#define NB_UART_RX_PIN 1
#define PIN_UART0_TX   5
#define PIN_UART0_RX   6

#define NB_HARD_UART HS_UART1 //昂瑞威串口
#define NB_UART_BAUDRATE 115200
#define ATE_HARD_UART HS_UART0 //昂瑞威串口

#define NB_VIRTUAL_UART vUART_1 // nb模组对应的虚拟串口
__IO static uint8_t usartATERxData;

/**
 * @brief  NB串口中断函数
 *
 * @note
 */
static void Nb_uart_read_handler(uint8_t data)
{
    Device_IRQHandler(NB_VIRTUAL_UART, (void *)&data, sizeof(data));
}

/**
 * @brief  UART0中断函数
 *
 * @note   产测串口
 */
static void uart0_receive_handler(uint8_t data)
{
    usartATERxData = data;

    /* 收到的数据写入缓存 */
    Device_IRQHandler(vUART_2, (void *)&usartATERxData, sizeof(usartATERxData));
}

/**
 * @brief NB串口初始化
 *
 *
 * @note
 */
static void Nb_uart_init(void)
{
    // Pinmux
    pinmux_config(NB_UART_TX_PIN, PINMUX_UART1_SDA_O_CFG); // tx
    pinmux_config(NB_UART_RX_PIN, PINMUX_UART1_SDA_I_CFG); // rx

    pmu_pin_mode_set(BITMASK(NB_UART_TX_PIN), PMU_PIN_MODE_PP);
    pmu_pin_mode_set(BITMASK(NB_UART_RX_PIN), PMU_PIN_MODE_PU);

    // UART Init
    uart_open(NB_HARD_UART, NB_UART_BAUDRATE, UART_FLOW_CTRL_DISABLED, Nb_uart_read_handler);
}

/**
 * @brief  产测串口初始化
 *
 * @param  baudrate：波特率
 */
static void Uart0_Start(uint32_t baudrate)
{
    uart_open(ATE_HARD_UART, baudrate, UART_FLOW_CTRL_DISABLED, uart0_receive_handler);
}

//产测串口，IO配置
static void Uart0_PortInit(void)
{
    pinmux_config(PIN_UART0_TX, PINMUX_UART0_SDA_O_CFG);
    pinmux_config(PIN_UART0_RX, PINMUX_UART0_SDA_I_CFG);
    pmu_pin_mode_set(BITMASK(PIN_UART0_TX), PMU_PIN_MODE_PP);
    pmu_pin_mode_set(BITMASK(PIN_UART0_RX), PMU_PIN_MODE_PU);
}

/**
 * @brief
 *
 * @param [in] dev
 *
 * @note
 */
static void NB_Enable(VirtualHardware_enum_t dev)
{
    if (NB_VIRTUAL_UART == dev)
    {
        pinmux_config(NB_UART_TX_PIN,  PINMUX_UART1_SDA_O_CFG);  //tx
        // pinmux_config(NB_UART_TX_PIN, PINMUX_GPIO_MODE_CFG);
        // gpio_set_direction(BITMASK(NB_UART_TX_PIN), GPIO_INPUT);
        // pmu_pin_mode_set(BITMASK(NB_UART_TX_PIN), PMU_PIN_MODE_PU);

        pinmux_config(NB_UART_RX_PIN, PINMUX_UART1_SDA_I_CFG); // rx
        uart_open(NB_HARD_UART, NB_UART_BAUDRATE, UART_FLOW_CTRL_DISABLED, Nb_uart_read_handler);
    }
    else if (vUART_2 == dev)
    {
        Uart0_PortInit();
    }
}
/**
 * @brief
 *
 *
 * @note
 */
static void NB_Disable(VirtualHardware_enum_t dev)
{
    if (NB_VIRTUAL_UART == dev)
    {
        uart_close(NB_HARD_UART);
        pinmux_config(NB_UART_RX_PIN, PINMUX_GPIO_MODE_CFG); // rx
        pmu_pin_mode_set(BITMASK(NB_UART_RX_PIN), PMU_PIN_MODE_PU);
        gpio_write(BITMASK(NB_UART_RX_PIN), GPIO_HIGH); // rx
        gpio_set_direction(BITMASK(NB_UART_RX_PIN), GPIO_OUTPUT);

        pinmux_config(NB_UART_TX_PIN, PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(NB_UART_TX_PIN), PMU_PIN_MODE_PU);
        gpio_write(BITMASK(NB_UART_TX_PIN), GPIO_HIGH);
        gpio_set_direction(BITMASK(NB_UART_TX_PIN), GPIO_OUTPUT);
    }
    else if (vUART_2 == dev) //ATE
    {
        uart_close(ATE_HARD_UART);
    }
}

/**
 * @brief 蓝牙写操作
 *
 * @param [in] cmd
 * @param [in] param
 * @return ErrorStatus
 *
 * @note
 */
static ErrorStatus NB_Write(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param)
{
    uint8_t *pData = (uint8_t *)data;
    if (data == NULL && len == 0)
    {
        return -1;
    }

    if (NB_VIRTUAL_UART == dev)
    {
        // InputPort_DisableProt("BUT_RESET");
        // pinmux_config(NB_UART_TX_PIN, PINMUX_UART1_SDA_O_CFG);
        uart_send_block(NB_HARD_UART, pData, len);
        // pinmux_config(NB_UART_TX_PIN, PINMUX_GPIO_MODE_CFG);
        // gpio_set_direction(BITMASK(NB_UART_TX_PIN), GPIO_INPUT);
        // pmu_pin_mode_set(BITMASK(NB_UART_TX_PIN), PMU_PIN_MODE_PU);
        // InputPort_EnableProt("BUT_RESET");
    }
    else if (vUART_2 == dev)
    {
        uart_send_block(ATE_HARD_UART, pData, len);
    }
   return 0;
}

/**
 * @brief  设置串口波特率
 * @note
 * @param  dev：虚拟设备
 * @param  baudrate：波特率
 * @return int8_t
 */
static int8_t NB_SetBaudrate(VirtualHardware_enum_t dev, uint32_t baudrate)
{
    if (NB_VIRTUAL_UART == dev)
    {
        uart_open(NB_HARD_UART, baudrate, UART_FLOW_CTRL_DISABLED, Nb_uart_read_handler);
    }
}
/**
 * @brief 蓝牙设备初始化
 *
 *
 * @note
 */
void Nb_Init(void)
{
    NB_LOG_D("Nb init");
    Nb_uart_init();
     /* 产测串口初始化 */
    Uart0_PortInit();//0
    Uart0_Start(115200);

    /* 将底层设备具体操作接口，挂到设备控制块里面 */
    Device_stu_t *dcb = Device_GetDeviceCtrlBlock(vUART);
    dcb->enable = NB_Enable;
    dcb->disable = NB_Disable;
    dcb->write = NB_Write;
    dcb->set_uart_baudrate = NB_SetBaudrate;
}
INIT_PREV_EXPORT(Nb_Init);