#include "rwip.h" // RW SW initialization
#include "co_debug.h"
#include "peripheral.h"
#include "device.h"
#include "component.h"
#include "debug_config.h"
#include "pmu.h"
#define ONELINE_LOG(format, ...) __OSAL_LOG("[audi_oneline_drv.c] " C_GREEN format C_NONE, ##__VA_ARGS__)

#define AUDIO_CHIP_NYQUEST 0x00    // 九齐语音芯片，单线串口
#define AUDIO_CHIP_WAYTRONIC 0x01 // 维创语音，单线串口

#define AUDIO_CHIP_TYPE AUDIO_CHIP_WAYTRONIC
// Occupancy
#if (AUDIO_CHIP_TYPE == AUDIO_CHIP_NYQUEST)
#define AUDIO_DATA_LINE_HIGH_OCCUPANCY_TIME 1500 // 高电平保持时长  单位us
#define AUDIO_DATA_LINE_LOW_OCCUPANCY_TIME 500 // 低电平保持时长 单位us
#else
#define AUDIO_DATA_LINE_HIGH_OCCUPANCY_TIME 600 // 高电平保持时长 单位us
#define AUDIO_DATA_LINE_LOW_OCCUPANCY_TIME 200 // 低电平保持时长 单位us
#endif

#define ONELONE_DATA_VIRTUALHARDWARE vPIN_C39 ///< 九齐DATA
#define AUDIO_DATA_H Device_Write(ONELONE_DATA_VIRTUALHARDWARE, NULL, 0, 1)
#define AUDIO_DATA_L Device_Write(ONELONE_DATA_VIRTUALHARDWARE, NULL, 0, 0)

void audio_waytronic_play_num(uint8_t num)
{
    for (uint8_t i = 0; i < 8; i++)
    {
        if (num & 0x01)
        {
            AUDIO_DATA_H;
            Device_DelayUs(AUDIO_DATA_LINE_HIGH_OCCUPANCY_TIME);
            AUDIO_DATA_L;
            Device_DelayUs(AUDIO_DATA_LINE_LOW_OCCUPANCY_TIME);
        }
        else
        {
            AUDIO_DATA_H;
            Device_DelayUs(AUDIO_DATA_LINE_LOW_OCCUPANCY_TIME);
            AUDIO_DATA_L;
            Device_DelayUs(AUDIO_DATA_LINE_HIGH_OCCUPANCY_TIME);
        }
        num >>= 1;
    }
}

void audio_jiuqi_play(uint16_t num)
{
    uint8_t low_byte = (uint8_t)(num & 0xFF);
    uint8_t high_byte = (uint8_t)((num >> 8) & 0xFF);

    // AUDIO_DATA_H;
    // Device_DelayMs(5);//TODO: 休眠拉低，开始发码前要先拉高5ms在拉低5ms发码; 测试使能 AUDIO_DATA_H后，到播放语音的时间
    AUDIO_DATA_L;
    Device_DelayMs(5);

    Device_EnterCritical();
#if (AUDIO_CHIP_TYPE == AUDIO_CHIP_NYQUEST)
    for (uint8_t i = 0; i < 16; i++)
    {
        if (num & 0x8000)
        {
            AUDIO_DATA_H;
            Device_DelayUs(1500);
            AUDIO_DATA_L;
            Device_DelayUs(500);
        }
        else
        {
            AUDIO_DATA_H;
            Device_DelayUs(500);
            AUDIO_DATA_L;
            Device_DelayUs(1500);
        }
        num <<= 1;
    }
#else
    // 高8位先发 低位在前 字节切换之间要增加高电平保持2ms后再重复发字节操作
    audio_waytronic_play_num(high_byte);
    // 低字节开始传输
    AUDIO_DATA_H;
    Device_DelayMs(2);
    AUDIO_DATA_L;
    Device_DelayMs(5);
    audio_waytronic_play_num(low_byte);
#endif
    AUDIO_DATA_H;
    Device_ExitCritical();
}

static int32_t OneLineAudio_Read(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param)
{
    if (vAUDIO_1 == dev)
    {
        return 0;
    }
    return -1;
}
static int32_t OneLineAudio_Write(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param)
{
    if (vAUDIO_1 == dev)
    {
        if (strcmp(data, "PLAY") == 0 && param > 0)
        {
            audio_jiuqi_play(param - 1);
        }
        else if (strcmp(data, "VOLUME") == 0)
        {
            uint8_t vol = (param * 15) / 100;
            ONELINE_LOG("VOLUME,%d%% vol%d", param, vol);
            audio_jiuqi_play(0xFFE0 + vol);
        }
        return 0;
    }
    return -1;
}
static int8_t OneLineAudio_Disable(VirtualHardware_enum_t dev)
{
    if (vAUDIO_1 == dev)
    {  
        ONELINE_LOG("OneLineAudio_Disable");
        gpio_set_interrupt(BITMASK(AUDIO_ONELINE_BUSY_PIN), GPIO_TRIGGER_DISABLE);
        AUDIO_DATA_L;
    }
    return 0;
}

static int8_t OneLineAudio_Enable(VirtualHardware_enum_t dev)
{
    if (vAUDIO_1 == dev)
    {
        gpio_set_interrupt(BITMASK(AUDIO_ONELINE_BUSY_PIN), GPIO_RISING_EDGE);
        AUDIO_DATA_H;
    }

    return 0;
}

/**
 * @brief TSM12设备初始化
 *
 *
 * @note
 */
static void OneLineAudio_Init(void)
{
    ONELINE_LOG("OneLine Audio init\r\n");
    Device_stu_t *dcb = Device_GetDeviceCtrlBlock(vAUDIO);
    dcb->read = OneLineAudio_Read;
    dcb->write = OneLineAudio_Write;
    dcb->enable = OneLineAudio_Enable;
    dcb->disable = OneLineAudio_Disable;
}
INIT_DEVICE_EXPORT(OneLineAudio_Init);

//__attribute__((section("RAMCODE")))
//void audio_jiuqi_play(uint16_t num)
//{
//    AUDIO_DATA_L;
//    Device_DelayMs(5);

//    Device_EnterCritical();
//    for (uint8_t i = 0; i < 16; i++)
//    {
//        if (num & 0x8000)
//        {
//            AUDIO_DATA_H;
//            Device_DelayUs(1500);
//            AUDIO_DATA_L;
//            Device_DelayUs(500);
//        }
//        else
//        {
//            AUDIO_DATA_H;
//            Device_DelayUs(500);
//            AUDIO_DATA_L;
//            Device_DelayUs(1500);
//        }
//        num <<= 1;
//    }
//    Device_ExitCritical();
//    
//    AUDIO_DATA_H;
//}
