/**
 * @file gpio_uart.h
 * @brief 
 * 
 * @version 0.1
 * @date 2023-11-21
 * 
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 * 
 * @note          鼎新同创・智能锁
 *  
 * @par 修改日志: 
 * <1> 2023-11-21 v0.1 huangliwu 创建初始版本
 * *************************************************************************
 */
#ifndef __GPIO_UART_H__
#define __GPIO_UART_H__

#define USE_GPIO_UART
#define GPIO_UART_PIN 4


void gpio_uart_init(void);
void gpio_uart_send_byte(char ch);
#endif /* __GPIO_UART_H__ */