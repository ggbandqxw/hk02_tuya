/**
 * @file gpio_uart.c
 * @brief 
 * 
 * @version 0.1
 * @date 2023-11-21
 * 
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 * 
 * @note          鼎新同创・智能锁
 * @note          gpio模拟串口打印
 * @par 修改日志: 
 * <1> 2023-11-21 v0.1 huangliwu 创建初始版本
 * *************************************************************************
 */
#include "device.h"
#include "peripheral.h"
#include "gpio_uart.h"

/**
 * @brief  GPIO模拟串口初始化
 */
void gpio_uart_init(void)
{
    // IO配置成GPIO输出模式
    pinmux_config(GPIO_UART_PIN, PINMUX_GPIO_MODE_CFG);
    pmu_pin_mode_set(BITMASK(GPIO_UART_PIN), PMU_PIN_MODE_PP);
    gpio_set_direction(BITMASK(GPIO_UART_PIN), GPIO_OUTPUT);
    gpio_write(BITMASK(GPIO_UART_PIN),GPIO_HIGH);  
}

/**
 * @brief  延迟函数
 */
static inline void gpio_uart_deley(void)
{
#ifdef CONFIG_HS6621P
    // 128M主频
    volatile uint16_t i = 4; // 波特率根据延时在设置 921600波特率
    while (i--)
        ;
    __nop();
    __nop();
    __nop();
    __nop();
    __nop();
    __nop();
#elif CONFIG_HS6621C

#ifdef MCU_FREQ_32M
    // 32M主频
    // volatile uint16_t i = 10;//波特率根据延时在设置 600000波特率
    // while(i--);
    __nop();
    __nop();
#else
    // 64M主频
    volatile uint16_t i = 1; // 波特率根据延时在设置 921600波特率
    while (i--)
        ;

#endif

#endif
}

/**
 * @brief  发送字符
 * @param  ch: 字符
 */
void gpio_uart_send_byte(char ch)
{
    uint16_t value = ch & 0xFF;
    value |= 0X100; // stop  bit
    value <<= 2;    // start bit
    value |= 1;     // start bit
    //Device_EnterCritical();
    CO_DISABLE_IRQ();
    for (int i = 0; i < 11; i++)
    {
        if (value & 0x1)
        {
            gpio_write(BITMASK(GPIO_UART_PIN),GPIO_HIGH);  
        }
        else
        {
            gpio_write(BITMASK(GPIO_UART_PIN),GPIO_LOW);
        }
        value >>= 1;
        gpio_uart_deley();
    }
    //Device_ExitCritical();
    CO_RESTORE_IRQ();
}
