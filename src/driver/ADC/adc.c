#include "device.h"
//#include <string.h>

//昂瑞微的驱动库API
//#include "rwip_config.h" // RW SW configuration
#include "peripheral.h"
#include "pin.h"

#define ADC_LOG(format, ...)    OSAL_LOG(format, ##__VA_ARGS__)
/**
 * @brief  使能ADC
 * @note
 * @param  dev：虚拟设备
 * @return 成功/失败
 */
static int8_t ADC_Enable(VirtualHardware_enum_t dev)
{

    if (dev == vADC_0)
    {
		#ifdef BATTERY_ADC_PIN
        pmu_pin_mode_set(BIT_MASK(BATTERY_ADC_PIN), PMU_PIN_MODE_FLOAT);
		#endif
    }
    else if (dev == vADC_1)
    {
        #ifdef MOTO_OVP_ADC_PIN
            HS_GPIO_DIR_SET(MOTO_OVP_ADC_PIN, GPIO_INPUT);
            HS_PIN_MODE_SET(MOTO_OVP_ADC_PIN, PMU_PIN_MODE_FLOAT);
            #if (MOTO_OVP_ADC_PIN == 12)
            HS_PIN_FUNC_SET(MOTO_OVP_ADC_PIN, PINMUX_ANALOG_CH6_PIN12_CFG);
            #endif
        #endif

        adc_start();
    }
    return 0;
}

/**
 * @brief  失能ADC
 * @note
 * @param  dev：虚拟设备
 * @return 成功/失败
 */
static int8_t ADC_Disable(VirtualHardware_enum_t dev)
{
    if (dev == vADC_0)
    {
		#ifdef BATTERY_ADC_PIN
        // TODO adc脚需要切回普通io，否则会有几个uA漏电
        pinmux_config(BATTERY_ADC_PIN, PINMUX_GPIO_MODE_CFG);
		#endif
    }
     else if (dev == vADC_1)
    {
        adc_stop();//ADC_Rea没有nostop导致功耗2mA
    #ifdef MOTO_OVP_ADC_PIN
        HS_PIN_FUNC_SET(MOTO_OVP_ADC_PIN, PINMUX_GPIO_MODE_CFG);
        HS_PIN_MODE_SET(MOTO_OVP_ADC_PIN, PMU_PIN_MODE_PP);
        HS_GPIO_DIR_SET(MOTO_OVP_ADC_PIN, GPIO_OUTPUT);
        HS_GPIO_WRITE(MOTO_OVP_ADC_PIN, GPIO_LOW);
        ADC_LOG("ADC_Disable vADC_1\n");
    #endif
    }
    return 0;
}

/**
 * @brief  读取ADC
 * @note
 * @param  data：此处没用到,传NULL即可
 * @param  len：此处没用到,传0即可
 * @param  param：此处没用到,传0即可
 * @return ADC转换值
 */
static int32_t ADC_Read(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param)
{
    int16_t batteryAdcVal = 0;
    int16_t motoAdcValue  = 0;

    if (dev == vADC_0) //电池电量采集  
    {
		#ifdef BATTERY_ADC_PIN
        batteryAdcVal = adc_battery_voltage_read_by_single_pin_750mv(ADC_CHANNEL_EXTERN_CH0, ADC_PGA_GAIN_0P25, NULL, 20);
		ADC_LOG("bat adc = %d",batteryAdcVal);
		return (int32_t)(batteryAdcVal * BATTERY_VOLTAGE_SCALE);
		#endif
    }
    else if (dev == vADC_1)  //电机检测
    {
        #ifdef MOTO_OVP_ADC_PIN
            #if (MOTO_OVP_ADC_PIN == 12)
            // motoAdcValue = adc_voltage_read_single_begin_nostop(ADC_CHANNEL_EXTERN_CH6, ADC_CHANNEL_CHIP_VCM, ADC_PGA_GAIN_1,
            //                                                 ADC_VCM_SEL_000mV, 20);

            motoAdcValue = adc_battery_voltage_read_by_single_pin_750mv(ADC_CHANNEL_EXTERN_CH6, ADC_PGA_GAIN_0P25, NULL, 20);
		    //ADC_LOG("motot adc = %d",motoAdcValue);

            #endif
            if (motoAdcValue <= 0)
            {
                motoAdcValue = 0;
            }

        return motoAdcValue;

        #endif
    }
	return 0;
}

static void ADC_IO_Init(void)
{
	#ifdef BATTERY_ADC_PIN
	gpio_set_direction(BIT_MASK(BATTERY_ADC_PIN), GPIO_INPUT);	
    pmu_pin_mode_set(BIT_MASK(BATTERY_ADC_PIN), PMU_PIN_MODE_FLOAT);
    pinmux_config(BATTERY_ADC_PIN, PINMUX_ANALOG_CH0_PIN8_CFG);
	#endif
	
    #ifdef MOTO_OVP_ADC_PIN
        HS_GPIO_DIR_SET(MOTO_OVP_ADC_PIN, GPIO_INPUT); 
        HS_PIN_MODE_SET(MOTO_OVP_ADC_PIN, PMU_PIN_MODE_FLOAT);
        #if (MOTO_OVP_ADC_PIN == 12)
        HS_PIN_FUNC_SET(MOTO_OVP_ADC_PIN, PINMUX_ANALOG_CH6_PIN12_CFG);
        #endif
    #endif
}

/**
 * @brief  ADC初始化
 * @note
 */
static void ADC_Init(void)
{
    /* 1、初始化硬件 */
    ADC_IO_Init();

    /* 2、将底层设备具体操作接口，挂到设备控制块里面 */
    Device_stu_t *dcb = Device_GetDeviceCtrlBlock(vADC);
    dcb->enable = ADC_Enable;
    dcb->disable = ADC_Disable;
    dcb->read = ADC_Read;
	ADC_LOG("adc init\r\n");
}
INIT_DEVICE_EXPORT(ADC_Init);
