#ifndef __EEPROM_H__
#define __EEPROM_H__

#include "stdint.h"

#define FLASH_BASE          0x400000

#define FLASH_SUM_SIZE    1024*1024   //flash总大小1MB
#define FLASH_CFG_INFO_SIZE     20 * 1024 //flash 最后20KB为系统配置和信息区不能使用 

/* FLASH各个区的SIZE */
#define FLASH_SIZE_EEPROM   (40 << 10)    //EEPROM分配40K     实际使用：36K


/* FLASH各个区的起始地址 */
#define FLASH_ADDR_EEPROM   (FLASH_SUM_SIZE - FLASH_CFG_INFO_SIZE - FLASH_SIZE_EEPROM) //EEPROM在964KB从开始



void ee_restore(void);

int eeprom_init(void);
int eeprom_read(uint32_t addr, uint8_t *data, uint32_t len);
int eeprom_write(uint32_t addr, uint8_t *data, uint32_t len);



#endif /* __EEPROM_H__ */
