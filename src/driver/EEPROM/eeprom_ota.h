#ifndef __EEPROM_OTA_H__
#define __EEPROM_OTA_H__

#include "bootloader.h"


int eeprom_ota_init(void);
int eeprom_ota_read(uint32_t addr, uint8_t *data, uint32_t len);
int eeprom_ota_write(uint32_t addr, uint8_t *data, uint32_t len);

#endif /* __EEPROM_H__ */
