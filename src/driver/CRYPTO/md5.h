#ifndef __MD5_H_
#define __MD5_H_
#include "stdint.h"

uint8_t MD5_Calc_String(uint8_t *dest_str, uint32_t dest_len, char *md5_str);
uint8_t MD5_Calc(uint8_t *dest_str, uint32_t dest_len, uint8_t *md5_str);
uint8_t MD5_Calc_Subsection(uint32_t addr,uint8_t *dest_str ,uint32_t opt_len,uint32_t dest_len,char *md5_str);
#endif

