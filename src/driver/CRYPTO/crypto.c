#include "device.h"
#include <string.h>

//昂瑞微的底层API
#include "peripheral.h"
#include "aes.h"
#include "md5.h"
#include<string.h>
#include "AES_PKCS7.h"

static unsigned long crc32_tab[] = {
    0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
    0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L,
    0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L,
    0x90bf1d91L, 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
    0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L, 0x136c9856L,
    0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L,
    0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
    0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
    0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L,
    0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L, 0x26d930acL, 0x51de003aL,
    0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L,
    0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
    0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
    0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL,
    0x9fbfe4a5L, 0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL,
    0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
    0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL,
    0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L, 0x12b7e950L,
    0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
    0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
    0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL,
    0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L,
    0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL, 0x270241aaL, 0xbe0b1010L,
    0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
    0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
    0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L,
    0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L,
    0x73dc1683L, 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
    0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L, 0xf00f9344L,
    0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL,
    0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
    0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
    0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L,
    0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL,
    0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL,
    0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
    0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
    0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L,
    0x2cd99e8bL, 0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL,
    0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
    0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL,
    0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L, 0xf1d4e242L,
    0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
    0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
    0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L,
    0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L,
    0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L,
    0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
    0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
    0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L,
    0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL,
    0x2d02ef8dL};

uint32_t CRC32_Subsection(const unsigned char *s, uint32_t value, uint32_t len)
{
	unsigned int i;
	uint32_t crc32val;

	crc32val = value;

	for (i = 0; i < len; i++)
	{
		crc32val = crc32_tab[(crc32val ^ s[i]) & 0xff] ^ (crc32val >> 8);
	}

	return crc32val;
}
/**
  * @brief  计算flash中CRC校验值
  * @note   
  * @param  addr：flash的起始地址，dest_len：要计算的长度
  *         dest_str：单次读取的数据存储的位置，opt_len：单次读取的长度
  * @return 成功/失败
  */
uint32_t CalcSPICRC32(uint32_t address,uint8_t *dest_str ,uint32_t opt_len,uint32_t dest_len,char *md5_str)
{
    uint8_t md5_value[4];
	uint32_t crc32val = 0xFFFFFFFF;
	uint32_t calc_len = 0;

	while(dest_len > opt_len)
	{
        sf_read(HS_SF, 0, address, dest_str, opt_len);
		crc32val = CRC32_Subsection(dest_str, crc32val, opt_len);
		address = address + opt_len;
		dest_len = dest_len - opt_len;
	}

	if(dest_len > 0)//最后一包数据
	{
		sf_read(HS_SF, 0, address, dest_str, dest_len);
		crc32val = CRC32_Subsection(dest_str, crc32val, dest_len);
	}
    crc32val = ~crc32val;       //最后一包取反

    memcpy(md5_str, &crc32val, 4);
    
    //打印crc值
    memcpy(md5_value, &crc32val, 4);
	printf("\r\nMD5: ");
	for(uint8_t i=0;i<4;i++)
	{
		printf("%02X",md5_value[i]);
	}
	printf("\r\n");

	return 0;
}

static uint32_t get_rand(void)
{
    uint32_t rand_data = HS_RANDOM->RANDOM;
    uint32_t rnd_data;
    rnd_data = pmu_random_seed_fetch(rand_data);
    return rnd_data;
}

/**
  * @brief  计算CRC校验
  * @note
  * @param  *puchMsg：数据指针
  * @param  usDataLen：数据长度
  * @return uint16_t
  */
uint16_t crc16_ccitt_update( uint16_t crc, uint8_t data ) 
{
    uint16_t ret_val;

    data ^= ( uint8_t )(crc) & (uint8_t )(0xFF);
    data ^= data << 4;

    ret_val = ((((uint16_t)data << 8) | ((crc & 0xFF00) >> 8))
                ^ (uint8_t)(data >> 4)
                ^ ((uint16_t)data << 3));

    return ret_val;
}

uint16_t crc16_ccitt(uint8_t *data, uint32_t len) 
{
    uint16_t crc16_data = 0;
    while (len--)
    {
        crc16_data = crc16_ccitt_update(crc16_data, data[0]);
        data++;
    }
    return crc16_data;
}

/**
 * @brief  加解密接口
 * @note
 * @param  cmd：命令字，具体参考DevCryptoCmd_enum_t枚举
 * @param  pSrc：源
 * @param  len：源的字节数
 * @param  pDst：目的地
 * @param  key：密钥
 * @return 失败返回-1。成功返回0
 */
static int8_t Cypto_EnDecryption(Crypto_enum_t param, void *src, uint32_t len, void *dst, void *k)
{
    int8_t crypto_status = 0xff;
    uint8_t *key = (uint8_t *)k;

    if (param == AES128_ENCRYPTION || param == AES128_DECRYPTION) // AES128加解密
    {
        uint8_t *pSrc = (uint8_t*)src;
        uint8_t *pDst = (uint8_t*)dst;
        uint16_t loop = len / 16;
        
        //< 长度检查（必须16的整数倍）
        if (len % 16)
        {
            return -1;
        }

        //< 加解密运算
        for (int i = 0; i < loop; i++)
        {
            if (param == AES128_ENCRYPTION) // 加密
            {
                AES128_ECB_encrypt(pSrc, key, pDst);
            }
            else
            {
                AES128_ECB_decrypt(pSrc, key, pDst);
            }
            pSrc += 16;
            pDst += 16;
        }
    }
    else  if (param == AES128_ENCRYPTION_PKCS7 || param == AES128_DECRYPTION_PKCS7) // AES128_PKCS7加解密  
    {
        uint8_t *pSrc = (uint8_t*)src;
        uint8_t *pDst = (uint8_t*)dst;

        if (param == AES128_ENCRYPTION_PKCS7) // 加密
        {
            AES_Init(key);
		    AES_Encrypt_PKCS7 (pSrc, pDst, len, key);
        }
        else
        {
            AES_Init(key);
		    AES_Decrypt (pSrc, pDst, len, key);
        }
        
    }
    else if (param == CALC_RANDOM_NUMBER) //计算随机数
    {
        if (len == 0)
        {
            len = 4;
        }
        co_generate_random((uint8_t *)dst, len);
        crypto_status = 0;
    }
    else if (param == CALC_MD5)
    {
        if (key != NULL) // 分段计算MD5
        {
            uint32_t md5_addr = (uint32_t)src;
            uint8_t flash_type = (uint8_t)k; // 1:外部 2：内部
            if (MD5_Calc_Section(md5_addr, len, dst, flash_type) == 0)
            {
                crypto_status = 0;
            }
        }
        else
        {
            if (MD5_Calc(src, len, dst) == 0)
            {
                crypto_status = 0;
            }
        }
    }
    else if (param == CALC_CRC16)
    {
        uint8_t *pcrc = (uint8_t *)dst;
        uint16_t crc_cal;
        crc_cal = crc16_ccitt(src, len);
        memcpy(pcrc,&crc_cal,2);    
    }

    if (crypto_status == 0)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

/**
 * @brief  加密模块初始化
 * @note
 */
static void Cypto_Init(void)
{
    // uint8_t table[32];
    // uint8_t num[6] = {0x02,0x06,0x01,0x07,0x00,0x01};
    // uint16_t rc = 0;
    printf("cypto init\r\n");
    Device_stu_t *dcb = Device_GetDeviceCtrlBlock(vCRYPTO);
    dcb->crypto = Cypto_EnDecryption;

    // printf("cypto:");
    // Cypto_EnDecryption(CALC_RANDOM_NUMBER,NULL,29,table,NULL);
    // for(uint8_t i=0;i<32;i++)
    // {
    //     printf("%02x ",table[i]);
    // }
    // printf("\r\n");
    // rc = crc16_ccitt(num,sizeof(num));

    // printf("rc = %04x\r\n",rc);
    
    // while(1)
    // {
    //     rwip_schedule();
    //     wdt_keepalive();
    // }
}
INIT_DEVICE_EXPORT(Cypto_Init);
