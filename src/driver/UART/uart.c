 /**
 * @file uart.c
 * @brief
 *
 * @version 0.1
 * @date 2023-09-03
 *
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 *
 * @note          鼎新同创・智能锁
 *
 * @par 修改日志:
 * <1> 2023-09-03 v0.1 tianshidong 创建初始版本
 * *************************************************************************
 */
#include "rwip.h" // RW SW initialization
#include "co_debug.h"
#include "app.h"
#include "peripheral.h"
#include "co_debug.h"

#include "device.h"
#include "component.h"
#include "debug_config.h"
#include "pin.h"

#define UART_DEV_LOG(format, ...) OSAL_LOG(C_LIGHT_GREEN format C_NONE "\r\n", ##__VA_ARGS__)
#define UART_DEV_HEX(note, array, len)   log_debug_array_ex(note, array, len)

#define OS2OS_HARD_UART HS_UART1 //昂瑞微串口
#define OS2OS_UART_BAUDRATE 115200  //921600 
#define OS2OS_VIRTUAL_UART vUART_1 // os2os对应的虚拟串口

#define FG_HARD_UART HS_UART0
#define FG_UART_BAUDRATE 57600
#define FG_VIRTUAL_UART vUART_10 // 指纹模组对应的虚拟串口

#ifdef MC60_UART
#define MC60_HARD_UART HS_UART0 
#define MC60_UART_BAUDRATE 115200 
#endif   
#define MC60_VIRTUAL_UART vUART_12 // MC60 Homekit模组对应的虚拟串口


/**
 * @brief  OS2OS串口中断函数
 *
 * @note
 */
static void os2os_uart_read_handler(uint8_t data)
{
	   /* os2os接口 */
    Device_IRQHandler(OS2OS_VIRTUAL_UART, (void *)&data, sizeof(data));
}

/**
 * @brief  UART0中断函数
 *
 * @note   产测串口
 */
static void Fg_uart_read_handler(uint8_t data)
{
    Device_IRQHandler(FG_VIRTUAL_UART, (void *)&data, sizeof(data));
}

/**
 * @brief  MC60串口中断函数
 *
 * @note
 */
static void mc60_uart_read_handler(uint8_t data)
{
    Device_IRQHandler(MC60_VIRTUAL_UART, (void *)&data, sizeof(data));
}

/**
 * @brief OS2OS串口初始化
 *
 *
 * @note
 */
static void os2os_uart_init(void)
{
    // Pinmux
    pinmux_config(OS2OS_UART_TX_PIN, PINMUX_UART1_SDA_O_CFG); // tx
    pinmux_config(OS2OS_UART_RX_PIN, PINMUX_UART1_SDA_I_CFG); // rx

    pmu_pin_mode_set(BITMASK(OS2OS_UART_TX_PIN), PMU_PIN_MODE_PP);
    pmu_pin_mode_set(BITMASK(OS2OS_UART_RX_PIN), PMU_PIN_MODE_PU);

    // UART Init
    uart_open(OS2OS_HARD_UART, OS2OS_UART_BAUDRATE, UART_FLOW_CTRL_DISABLED, os2os_uart_read_handler);
}

// 指纹串口初始化
static void fg_uart_init(void)
{
    uart_close(FG_HARD_UART);
	#if defined(FG_UART_TX_PIN)  
    pinmux_config(FG_UART_TX_PIN, PINMUX_UART0_SDA_O_CFG);
    pinmux_config(FG_UART_RX_PIN, PINMUX_UART0_SDA_I_CFG);
    pmu_pin_mode_set(BITMASK(FG_UART_TX_PIN), PMU_PIN_MODE_PP);
    pmu_pin_mode_set(BITMASK(FG_UART_RX_PIN), PMU_PIN_MODE_PU);
	#endif
    uart_open(FG_HARD_UART, FG_UART_BAUDRATE, UART_FLOW_CTRL_DISABLED, Fg_uart_read_handler);
}

/**
 * @brief MC60串口初始化
 *
 *
 * @note
 */
static void mc60_uart_init(void)
{
    #ifdef MC60_UART
    // Pinmux
//    HS_PIN_FUNC_SET(PIN_MC60_UART0_TX, PINMUX_UART0_SDA_O_CFG); // tx
//    HS_PIN_FUNC_SET(PIN_MC60_UART0_RX, PINMUX_UART0_SDA_I_CFG); // rx

//    HS_PIN_MODE_SET(BITMASK(PIN_MC60_UART0_TX), PMU_PIN_MODE_PP);
//    HS_PIN_MODE_SET(BITMASK(PIN_MC60_UART0_RX), PMU_PIN_MODE_PU);

	pinmux_config(PIN_MC60_UART0_TX, PINMUX_UART0_SDA_O_CFG); // tx
    pinmux_config(PIN_MC60_UART0_RX, PINMUX_UART0_SDA_I_CFG); // rx
    pmu_pin_mode_set(BITMASK(PIN_MC60_UART0_TX), PMU_PIN_MODE_PP);
    pmu_pin_mode_set(BITMASK(PIN_MC60_UART0_RX), PMU_PIN_MODE_PU);
	
    // UART Init
    uart_open(MC60_HARD_UART, MC60_UART_BAUDRATE, UART_FLOW_CTRL_DISABLED, mc60_uart_read_handler);
    #endif 
}

/**
 * @brief
 *
 * @param [in] dev
 *
 * @note
 */
static void UartDev_Enable(VirtualHardware_enum_t dev)
{
    if (OS2OS_VIRTUAL_UART == dev)
    {
        pinmux_config(OS2OS_UART_TX_PIN, PINMUX_UART1_SDA_O_CFG); // tx
        pinmux_config(OS2OS_UART_RX_PIN, PINMUX_UART1_SDA_I_CFG); // rx

        pmu_pin_mode_set(BITMASK(OS2OS_UART_TX_PIN), PMU_PIN_MODE_PP);
        pmu_pin_mode_set(BITMASK(OS2OS_UART_RX_PIN), PMU_PIN_MODE_PU);
        
        uart_open(OS2OS_HARD_UART, OS2OS_UART_BAUDRATE, UART_FLOW_CTRL_DISABLED, os2os_uart_read_handler);
    }
    else if (FG_VIRTUAL_UART == dev)
    {
        UART_DEV_LOG("fg uart init");
        fg_uart_init();
    }
    else if (MC60_VIRTUAL_UART == dev)
    {
        #ifdef MC60_UART
//        HS_PIN_FUNC_SET(PIN_MC60_UART0_TX, PINMUX_UART0_SDA_O_CFG);
//        HS_PIN_FUNC_SET(PIN_MC60_UART0_RX, PINMUX_UART0_SDA_I_CFG);
//        uart_open(MC60_HARD_UART, MC60_UART_BAUDRATE, UART_FLOW_CTRL_DISABLED, mc60_uart_read_handler);
			
        pinmux_config(PIN_MC60_UART0_TX, PINMUX_UART0_SDA_O_CFG); // tx
        pinmux_config(PIN_MC60_UART0_RX, PINMUX_UART0_SDA_I_CFG); // rx
        pmu_pin_mode_set(BITMASK(PIN_MC60_UART0_TX), PMU_PIN_MODE_PP);
        pmu_pin_mode_set(BITMASK(PIN_MC60_UART0_RX), PMU_PIN_MODE_PU); //   PMU_PIN_MODE_FLOAT
        uart_open(MC60_HARD_UART, MC60_UART_BAUDRATE, UART_FLOW_CTRL_DISABLED, mc60_uart_read_handler);
       // UART_DEV_LOG("mc60 uart init");
        #endif
    }
}
/**
 * @brief
 *
 *
 * @note
 */
static void UartDev_Disable(VirtualHardware_enum_t dev)
{
    if (OS2OS_VIRTUAL_UART == dev)
    {
        uart_close(OS2OS_HARD_UART);

#if (B2B_INT_UART_RX)
        // OS2OS RX BE INT
        pinmux_config(OS2OS_UART_RX_PIN, PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(OS2OS_UART_RX_PIN), PMU_PIN_MODE_PU);
        gpio_set_direction(BITMASK(OS2OS_UART_RX_PIN), GPIO_INPUT);
        gpio_set_interrupt(BITMASK(OS2OS_UART_RX_PIN), GPIO_FALLING_EDGE);
        pmu_wakeup_pin_set(BITMASK(OS2OS_UART_RX_PIN), PMU_PIN_WAKEUP_LOW_LEVEL);
#else
        // pinmux_config(OS2OS_UART_RX_PIN, PINMUX_GPIO_MODE_CFG);        // rx
        // pmu_pin_mode_set(BITMASK(OS2OS_UART_RX_PIN), PMU_PIN_MODE_PP); //PMU_PIN_MODE_PU
        // gpio_write(BITMASK(OS2OS_UART_RX_PIN), GPIO_HIGH);             // rx //
        // gpio_set_direction(BITMASK(OS2OS_UART_RX_PIN), GPIO_OUTPUT);   //
        pinmux_config(OS2OS_UART_RX_PIN, PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(OS2OS_UART_RX_PIN), PMU_PIN_MODE_PU);
        gpio_set_direction(BITMASK(OS2OS_UART_RX_PIN), GPIO_INPUT);

#endif

        pinmux_config(OS2OS_UART_TX_PIN, PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(OS2OS_UART_TX_PIN), PMU_PIN_MODE_PP);//PMU_PIN_MODE_PU
        gpio_write(BITMASK(OS2OS_UART_TX_PIN), GPIO_HIGH);//
        gpio_set_direction(BITMASK(OS2OS_UART_TX_PIN), GPIO_OUTPUT);//
        UART_DEV_LOG("deinit os2os\r\n");//
    }
    else if (FG_VIRTUAL_UART == dev) //ATE
    {
        uart_close(FG_HARD_UART);
        #if defined(FG_UART_TX_PIN)
        // pinmux_config(FG_UART_RX_PIN, PINMUX_GPIO_MODE_CFG); // rx
        // pmu_pin_mode_set(BITMASK(FG_UART_RX_PIN), PMU_PIN_MODE_PD);
        // gpio_write(BITMASK(FG_UART_RX_PIN), GPIO_LOW); // rx
        // gpio_set_direction(BITMASK(FG_UART_RX_PIN), GPIO_OUTPUT);

        pinmux_config(FG_UART_RX_PIN, PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(FG_UART_RX_PIN), PMU_PIN_MODE_PD);
        gpio_set_direction(BITMASK(FG_UART_RX_PIN), GPIO_INPUT);

        pinmux_config(FG_UART_TX_PIN, PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(FG_UART_TX_PIN), PMU_PIN_MODE_PP);//  PMU_PIN_MODE_PD
        gpio_write(BITMASK(FG_UART_TX_PIN), GPIO_LOW);
        gpio_set_direction(BITMASK(FG_UART_TX_PIN), GPIO_OUTPUT);
	    #endif

        UART_DEV_LOG("UartDev deinit finger\r\n");
    }
    else if (MC60_VIRTUAL_UART == dev) //TODO:待确认休眠时tx,rx的电平设置
    {
        #ifdef MC60_UART
        uart_close(MC60_HARD_UART);
//        HS_PIN_FUNC_SET(PIN_MC60_UART0_TX, PINMUX_GPIO_MODE_CFG);
//        HS_PIN_MODE_SET(PIN_MC60_UART0_TX, PMU_PIN_MODE_PP);
//        HS_GPIO_DIR_SET(PIN_MC60_UART0_TX, GPIO_OUTPUT);
//        HS_GPIO_WRITE(PIN_MC60_UART0_TX, GPIO_HIGH);
//        HS_PIN_FUNC_SET(PIN_MC60_UART0_RX, PINMUX_GPIO_MODE_CFG);
//        HS_PIN_MODE_SET(PIN_MC60_UART0_RX, PMU_PIN_MODE_PP);
//        HS_GPIO_DIR_SET(PIN_MC60_UART0_RX, GPIO_OUTPUT);
//        HS_GPIO_WRITE(PIN_MC60_UART0_RX, GPIO_HIGH);
			
		pinmux_config(PIN_MC60_UART0_TX, PINMUX_GPIO_MODE_CFG); // rx
        pmu_pin_mode_set(BITMASK(PIN_MC60_UART0_TX), PMU_PIN_MODE_PP); //PMU_PIN_MODE_PU
        gpio_write(BITMASK(PIN_MC60_UART0_TX), GPIO_HIGH); // rx //
        gpio_set_direction(BITMASK(PIN_MC60_UART0_TX), GPIO_OUTPUT);//

        // pinmux_config(PIN_MC60_UART0_RX, PINMUX_GPIO_MODE_CFG);
        // pmu_pin_mode_set(BITMASK(PIN_MC60_UART0_RX), PMU_PIN_MODE_PP);//PMU_PIN_MODE_PU
        // gpio_write(BITMASK(PIN_MC60_UART0_RX), GPIO_HIGH);//
        // gpio_set_direction(BITMASK(PIN_MC60_UART0_RX), GPIO_OUTPUT);//

        pinmux_config(PIN_MC60_UART0_RX, PINMUX_GPIO_MODE_CFG);
        pmu_pin_mode_set(BITMASK(PIN_MC60_UART0_RX), PMU_PIN_MODE_PU);
        gpio_set_direction(BITMASK(PIN_MC60_UART0_RX), GPIO_INPUT);
        #endif
    } 
}

/**
 * @brief 写操作
 *
 * @param [in] cmd
 * @param [in] param
 * @return ErrorStatus
 *
 * @note
 */
static ErrorStatus UartDev_Write(VirtualHardware_enum_t dev, void *data, uint32_t len, uint32_t param)
{
    uint8_t *pData = (uint8_t *)data;
    if (data == NULL && len == 0)
    {
        return -1;
    }

    if (OS2OS_VIRTUAL_UART == dev)
    {
        // InputPort_DisableProt("BUT_RESET");
        // pinmux_config(OS2OS_UART_TX_PIN, PINMUX_UART1_SDA_O_CFG);
        uart_send_block(OS2OS_HARD_UART, pData, len);
        // pinmux_config(OS2OS_UART_TX_PIN, PINMUX_GPIO_MODE_CFG);
        // gpio_set_direction(BITMASK(OS2OS_UART_TX_PIN), GPIO_INPUT);
        // pmu_pin_mode_set(BITMASK(OS2OS_UART_TX_PIN), PMU_PIN_MODE_PU);
        // InputPort_EnableProt("BUT_RESET");
    }
    else if (FG_VIRTUAL_UART == dev)
    {
        uart_send_block(FG_HARD_UART, pData, len);
        // UART_DEV_LOG("FG_VIRTUAL_UART:%02X\r\n", pData[9]);
    }
    else if (MC60_VIRTUAL_UART == dev)
    {
        #ifdef MC60_UART
        uart_send_block(MC60_HARD_UART, pData, len);
        // UART_DEV_HEX("mc60_uart", pData, len);
        #endif
    }
   return 0;
}

/**
 * @brief  设置串口波特率
 * @note
 * @param  dev：虚拟设备
 * @param  baudrate：波特率
 * @return int8_t
 */
static int8_t UartDev_SetBaudrate(VirtualHardware_enum_t dev, uint32_t baudrate)
{
    if (OS2OS_VIRTUAL_UART == dev)
    {
        uart_open(OS2OS_HARD_UART, baudrate, UART_FLOW_CTRL_DISABLED, os2os_uart_read_handler);
    }
    else if (FG_VIRTUAL_UART == dev)
    {
        uart_open(FG_HARD_UART, baudrate, UART_FLOW_CTRL_DISABLED, Fg_uart_read_handler);
    }
    else if (MC60_VIRTUAL_UART == dev)
    {
        #ifdef MC60_UART
        uart_open(MC60_HARD_UART, baudrate, UART_FLOW_CTRL_DISABLED, mc60_uart_read_handler);
        #endif
    }
}

// static void BootUart1_PortReInit(void)
// {
//     uart_close(HS_UART0);
//     uart_close(HS_UART1);

//     pinmux_config(PIN_BOOT_UART1_TX, PINMUX_GPIO_MODE_CFG); // rx
//     pmu_pin_mode_set(BITMASK(PIN_BOOT_UART1_TX), PMU_PIN_MODE_PD);
//     gpio_write(BITMASK(PIN_BOOT_UART1_TX), GPIO_LOW); // rx
//     gpio_set_direction(BITMASK(PIN_BOOT_UART1_TX), GPIO_OUTPUT);

//     pinmux_config(PIN_BOOT_UART1_RX, PINMUX_GPIO_MODE_CFG);
//     pmu_pin_mode_set(BITMASK(PIN_BOOT_UART1_RX), PMU_PIN_MODE_PD);
//     gpio_write(BITMASK(PIN_BOOT_UART1_RX), GPIO_LOW);
//     gpio_set_direction(BITMASK(PIN_BOOT_UART1_RX), GPIO_OUTPUT);
// }

/**
 * @brief 串口设备初始化
 *
 *
 * @note
 */
void UartDev_Init(void)
{
    UART_DEV_LOG("UartDev init");
    //BootUart1_PortReInit();
    os2os_uart_init();
    // fg_uart_init();
    mc60_uart_init();

    /* 将底层设备具体操作接口，挂到设备控制块里面 */
    Device_stu_t *dcb = Device_GetDeviceCtrlBlock(vUART);
    dcb->enable = UartDev_Enable;
    dcb->disable = UartDev_Disable;
    dcb->write = UartDev_Write;
    dcb->set_uart_baudrate = UartDev_SetBaudrate;
    //UartDev_Disable(vUART_1);//
}
INIT_PREV_EXPORT(UartDev_Init);
