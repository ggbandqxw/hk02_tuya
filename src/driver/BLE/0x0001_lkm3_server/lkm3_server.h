/**
 ****************************************************************************************
 *
 * @file lkm3_server.h
 *
 * @brief Header file - Service Server Role
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 *
 ****************************************************************************************
 */

#ifndef _LKM3_SERVER_H_
#define _LKM3_SERVER_H_

/**
 ****************************************************************************************
 * @addtogroup LKM3_SERVER 'Profile' Server
 * @ingroup LKM3_SERVER
 * @brief 'Profile' Server
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include "rwip_config.h"
#include "lkm3_server_task.h"
#include "prf_types.h"
#include "prf.h"

/*
 * DEFINES
 ****************************************************************************************
 */

#define ATT_SVC_LKM3_SERVER_SERVICE ATT_UUID_16(0x0001) //
#define ATT_SVC_LKM3_SERVER_CHAC1 ATT_UUID_16(0x0002)   // write,write no response
#define ATT_SVC_LKM3_SERVER_CHAC2 ATT_UUID_16(0x0003)   // notify

/// Maximum number of Server task instances
#define LKM3_SERVER_IDX_MAX 0x01

/*
 * ENUMERATIONS
 ****************************************************************************************
 */

/// Possible states of the LKM3_SERVER task
enum lkm3_server_state
{
    /// Idle state
    LKM3_SERVER_IDLE,
    /// busy state
    LKM3_SERVER_BUSY,
    /// Number of defined states.
    LKM3_SERVER_STATE_MAX
};

/// LKM3_SERVER Service Attributes Indexes
enum
{
    LKM3_SERVER_IDX_SVC,

    LKM3_SERVER_IDX_DEMO_CHAR1,
    LKM3_SERVER_IDX_DEMO_VAL1,

    LKM3_SERVER_IDX_DEMO_CHAR2,
    LKM3_SERVER_IDX_DEMO_VAL2,
    LKM3_SERVER_IDX_DEMO_NTF_CFG,

    LKM3_SERVER_IDX_NB,
};

/*
 * TYPE DEFINITIONS
 ****************************************************************************************
 */

/// 'Profile' Server environment variable
struct lkm3_server_env_tag
{
    /// profile environment
    prf_env_t prf_env;
    /// On-going operation
    struct ke_msg *operation;
    /// Services Start Handle
    uint16_t start_hdl;
    /// LKM3_SERVER task state
    ke_state_t state[LKM3_SERVER_IDX_MAX];
    /// Notification configuration of peer devices.
    uint8_t ntf_cfg[BLE_CONNECTION_MAX];
};

/*
 * GLOBAL VARIABLE DECLARATIONS
 ****************************************************************************************
 */

/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Retrieve service profile interface
 *
 * @return service profile interface
 ****************************************************************************************
 */
const struct prf_task_cbs *lkm3_server_prf_itf_get(void);

/**
 ****************************************************************************************
 * @brief Retrieve Attribute handle from service and attribute index
 *
 * @param[in] svc_idx LKM3_SERVER Service index
 * @param[in] att_idx Attribute index
 *
 * @return LKM3_SERVER attribute handle or INVALID HANDLE if nothing found
 ****************************************************************************************
 */
uint16_t lkm3_server_get_att_handle(uint8_t att_idx);

/**
 ****************************************************************************************
 * @brief Retrieve Service and attribute index form attribute handle
 *
 * @param[out] handle  Attribute handle
 * @param[out] svc_idx Service index
 * @param[out] att_idx Attribute index
 *
 * @return Success if attribute and service index found, else Application error
 ****************************************************************************************
 */
uint8_t lkm3_server_get_att_idx(uint16_t handle, uint8_t *att_idx);

/*
 * TASK DESCRIPTOR DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * Initialize task handler
 *
 * @param task_desc Task descriptor to fill
 ****************************************************************************************
 */
void lkm3_server_task_init(struct ke_task_desc *task_desc);

/// @} LKM3_SERVER

#endif /* _LKM3_SERVER_H_ */
