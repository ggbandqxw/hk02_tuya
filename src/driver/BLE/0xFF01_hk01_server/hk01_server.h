/**
 ****************************************************************************************
 *
 * @file hk01_server.h
 *
 * @brief Header file - Service Server Role
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 *
 ****************************************************************************************
 */

#ifndef _HK01_SERVER_H_
#define _HK01_SERVER_H_

/**
 ****************************************************************************************
 * @addtogroup HK01_SERVER 'Profile' Server
 * @ingroup HK01_SERVER
 * @brief 'Profile' Server
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include "rwip_config.h"
#include "hk01_server_task.h"
#include "prf_types.h"
#include "prf.h"

/*
 * DEFINES
 ****************************************************************************************
 */

#define ATT_SVC_HK01_SERVER_SERVICE 0x1910 
#define ATT_SVC_HK01_SERVER_CHAC1 0x2B11  // write,write no response
#define ATT_SVC_HK01_SERVER_CHAC2 0x2B10  // notify

/// Maximum number of Server task instances
#define HK01_SERVER_IDX_MAX 0x01

/*
 * ENUMERATIONS
 ****************************************************************************************
 */

/// Possible states of the HK01_SERVER task
enum hk01_server_state
{
    /// Idle state
    HK01_SERVER_IDLE,
    /// busy state
    HK01_SERVER_BUSY,
    /// Number of defined states.
    HK01_SERVER_STATE_MAX
};

/// HK01_SERVER Service Attributes Indexes
enum
{
    HK01_SERVER_IDX_SVC,

    HK01_SERVER_IDX_DEMO_CHAR1,
    HK01_SERVER_IDX_DEMO_VAL1,

    HK01_SERVER_IDX_DEMO_CHAR2,
    HK01_SERVER_IDX_DEMO_VAL2,
    HK01_SERVER_IDX_DEMO_NTF_CFG,

    HK01_SERVER_IDX_NB,
};

/*
 * TYPE DEFINITIONS
 ****************************************************************************************
 */

/// 'Profile' Server environment variable
struct hk01_server_env_tag
{
    /// profile environment
    prf_env_t prf_env;
    /// On-going operation
    struct ke_msg *operation;
    /// Services Start Handle
    uint16_t start_hdl;
    /// HK01_SERVER task state
    ke_state_t state[HK01_SERVER_IDX_MAX];
    /// Notification configuration of peer devices.
    uint8_t ntf_cfg[BLE_CONNECTION_MAX];
};

/*
 * GLOBAL VARIABLE DECLARATIONS
 ****************************************************************************************
 */

/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Retrieve service profile interface
 *
 * @return service profile interface
 ****************************************************************************************
 */
const struct prf_task_cbs *hk01_server_prf_itf_get(void);

/**
 ****************************************************************************************
 * @brief Retrieve Attribute handle from service and attribute index
 *
 * @param[in] svc_idx HK01_SERVER Service index
 * @param[in] att_idx Attribute index
 *
 * @return HK01_SERVER attribute handle or INVALID HANDLE if nothing found
 ****************************************************************************************
 */
uint16_t hk01_server_get_att_handle(uint8_t att_idx);

/**
 ****************************************************************************************
 * @brief Retrieve Service and attribute index form attribute handle
 *
 * @param[out] handle  Attribute handle
 * @param[out] svc_idx Service index
 * @param[out] att_idx Attribute index
 *
 * @return Success if attribute and service index found, else Application error
 ****************************************************************************************
 */
uint8_t hk01_server_get_att_idx(uint16_t handle, uint8_t *att_idx);

/*
 * TASK DESCRIPTOR DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * Initialize task handler
 *
 * @param task_desc Task descriptor to fill
 ****************************************************************************************
 */
void hk01_server_task_init(struct ke_task_desc *task_desc);

/// @} HK01_SERVER

#endif /* _HK01_SERVER_H_ */
