/**
 ****************************************************************************************
 *
 * @file hk01_server_task.h
 *
 * @brief Header file - Service Server Role Task.
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 *
 ****************************************************************************************
 */

#ifndef _HK01_SERVER_TASK_H_
#define _HK01_SERVER_TASK_H_

/**
 ****************************************************************************************
 * @addtogroup HK01_SERVERSTASK Task
 * @ingroup HK01_SERVER
 * @brief 'Profile' Task.
 *
 * The HK01_SERVERSTASK is responsible for handling the messages coming in and out of the
 * @ref BAPS block of the BLE Host.
 *
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include "prf_types.h"
#include "rwip_task.h" // Task definitions
#include "co_utils.h"
#include "co_debug.h"
#include "rwapp_config.h"
/*
 * DEFINES
 ****************************************************************************************
 */

/// Maximal number of HK01_SERVER that can be added in the DB
#define HK01_SERVER_NB_INSTANCES_MAX (1)

#define HK01_SERVER_MAX_CHAC_LEN BLE_MAX_OCTETS

/*
 * TYPE DEFINITIONS
 ****************************************************************************************
 */

/// Messages for HK01_SERVER
/*@TRACE*/
enum hk01_server_msg_id
{
    /// Start the Server - at connection used to restore bond data
    HK01_SERVER_ENABLE_REQ = TASK_FIRST_MSG(TASK_ID_HK01_SERVER), //!< HK01_SERVER_ENABLE_REQ @ref struct hk01_server_enable_req
                                                                  /// Confirmation of the Server start
    HK01_SERVER_ENABLE_RSP, //!< HK01_SERVER_ENABLE_RSP @ref struct hk01_server_enable_req
                            /// Inform APP that Notification Configuration has been changed
    HK01_SERVER_NTF_CFG_IND, //!< HK01_SERVER_NTF_CFG_IND @ref struct hk01_server_demo_ntf_cfg_ind
                             /// Send notification to gatt client
    HK01_SERVER_SEND_NTF_CMD, //!< HK01_SERVER_SEND_NTF_CMD @ref struct hk01_server_send_ntf_cmd
    HK01_SERVER_TIMEOUT_TIMER,
};

/*
 * APIs Structures
 ****************************************************************************************
 */

/// Parameters for the database creation
struct hk01_server_db_cfg
{
    void *param;
};

/// Parameters of the @ref HK01_SERVER_ENABLE_REQ message
struct hk01_server_enable_req
{
    /// connection index
    uint8_t conidx;
    /// Notification Configuration
    uint8_t ntf_cfg;
};

/// Parameters of the @ref HK01_SERVER_ENABLE_RSP message
struct hk01_server_enable_rsp
{
    /// connection index
    uint8_t conidx;
    /// status
    uint8_t status;
};

/// Parameters of the @ref HK01_SERVER_DEMO_NTF_CFG_IND message
struct hk01_server_demo_ntf_cfg_ind
{
    /// connection index
    uint8_t conidx;
    /// Notification Configuration
    uint8_t ntf_cfg;
};

/// Parameters of the @ref HK01_SERVER_SEND_NTF_CMD message
struct hk01_server_send_ntf_cmd
{
    /// connection index
    uint8_t conidx;
    uint16_t length;
    uint8_t value[__ARRAY_EMPTY];
};

/// @} HK01_SERVERSTASK

#endif /* _HK01_SERVER_TASK_H_ */
