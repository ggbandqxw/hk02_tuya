/**
 ****************************************************************************************
 *
 * @file app_hk01_server.h
 *
 * @brief Application hk01 server profile entry point
 *
 * Copyright (C) RivieraWaves 2009-2015
 *
 *
 ****************************************************************************************
 */

#ifndef APP_HK01_SERVER_H_
#define APP_HK01_SERVER_H_

/**
 ****************************************************************************************
 * @addtogroup APP_HK01_SERVER_PROFILE_H app_hk01_server.h
 * @ingroup APP_HK01_SERVER
 *
 * @brief Application hk01 server profile entry point
 *
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */

#include "rwip_config.h" // SW configuration
#include <stdint.h>      // Standard Integer Definition
#include "ke_task.h"     // Kernel Task Definition
#include "co_debug.h"    // Kernel Task Definition

/*
 * STRUCTURES DEFINITION
 ****************************************************************************************
 */

/// struct app_hk01_server_env_tag
///  Application Module Environment Structure
struct app_hk01_server_env_tag
{
    /// Connection handle
    uint8_t conidx;
    /// Some other parameters
};

/*
 * GLOBAL VARIABLES DECLARATIONS
 ****************************************************************************************
 */

extern struct app_hk01_server_env_tag app_hk01_server_env; /// Application environment

extern const struct app_subtask_handlers app_hk01_server_handlers; /// Table of message handlers

/*
 * FUNCTIONS DECLARATION
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 *
 * Health Thermometer Application Functions
 *
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Initialize Application Module
 * @return void.
 ****************************************************************************************
 */
void app_hk01_server_init(void);

/**
 ****************************************************************************************
 * @brief Add a Service instance in the DB
 * @return void.
 ****************************************************************************************
 */
void app_hk01_server_add_service(void);

/**
 ****************************************************************************************
 * @brief Enable the Service
 * @param[in] conidx: connect index.
 * @return void.
 ****************************************************************************************
 */
void app_hk01_server_enable_prf(uint8_t conidx);

/**
 ****************************************************************************************
 * @brief Disable the Service
 * @param[in] conidx: connect index.
 * @return void.
 ****************************************************************************************
 */
void app_hk01_server_disable_prf(uint8_t conidx);

void app_hk01_server_send_data(uint8_t *pdata, uint16_t len);
// Some other functions

/// @} APP

#endif // APP_HK01_SERVER_H_
