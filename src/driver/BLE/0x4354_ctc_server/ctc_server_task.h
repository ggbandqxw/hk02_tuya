/**
 ****************************************************************************************
 *
 * @file ctc_server_task.h
 *
 * @brief Header file - Service Server Role Task.
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 *
 ****************************************************************************************
 */


#ifndef _CTC_SERVER_TASK_H_
#define _CTC_SERVER_TASK_H_

/**
 ****************************************************************************************
 * @addtogroup CTC_SERVERSTASK Task
 * @ingroup CTC_SERVER
 * @brief 'Profile' Task.
 *
 * The CTC_SERVERSTASK is responsible for handling the messages coming in and out of the
 * @ref BAPS block of the BLE Host.
 *
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include "prf_types.h"
#include "rwip_task.h" // Task definitions
#include "co_utils.h"
#include "co_debug.h"
#include "rwapp_config.h"
/*
 * DEFINES
 ****************************************************************************************
 */

///Maximal number of CTC_SERVER that can be added in the DB
#define CTC_SERVER_NB_INSTANCES_MAX         (1)

#define CTC_SERVER_MAX_CHAC_LEN    BLE_MAX_OCTETS


/*
 * TYPE DEFINITIONS
 ****************************************************************************************
 */


/// Messages for CTC_SERVER
/*@TRACE*/
enum ctc_server_msg_id
{
    /// Start the Server - at connection used to restore bond data
    CTC_SERVER_ENABLE_REQ = TASK_FIRST_MSG(TASK_ID_CTC_SERVER),//!< CTC_SERVER_ENABLE_REQ @ref struct ctc_server_enable_req
    /// Confirmation of the Server start
    CTC_SERVER_ENABLE_RSP,                               //!< CTC_SERVER_ENABLE_RSP @ref struct ctc_server_enable_req
    /// Inform APP that Notification Configuration has been changed
    CTC_SERVER_NTF_CFG_IND,                         //!< CTC_SERVER_NTF_CFG_IND @ref struct ctc_server_demo_ntf_cfg_ind
    /// Send notification to gatt client
    CTC_SERVER_SEND_NTF_CMD,                         //!< CTC_SERVER_SEND_NTF_CMD @ref struct ctc_server_send_ntf_cmd
    CTC_SERVER_TIMEOUT_TIMER,
};


/*
 * APIs Structures
 ****************************************************************************************
 */

/// Parameters for the database creation
struct ctc_server_db_cfg
{
    void *param;
};

/// Parameters of the @ref CTC_SERVER_ENABLE_REQ message
struct ctc_server_enable_req
{
    /// connection index
    uint8_t  conidx;
    /// Notification Configuration
    uint8_t  ntf_cfg;
};

/// Parameters of the @ref CTC_SERVER_ENABLE_RSP message
struct ctc_server_enable_rsp
{
    /// connection index
    uint8_t conidx;
    ///status
    uint8_t status;
};

///Parameters of the @ref CTC_SERVER_DEMO_NTF_CFG_IND message
struct ctc_server_demo_ntf_cfg_ind
{
    /// connection index
    uint8_t  conidx;
    ///Notification Configuration
    uint8_t  ntf_cfg;
};

///Parameters of the @ref CTC_SERVER_SEND_NTF_CMD message
struct ctc_server_send_ntf_cmd
{
    /// connection index
    uint8_t  conidx;
    uint16_t length;
    uint8_t value[__ARRAY_EMPTY];
};


/// @} CTC_SERVERSTASK

#endif /* _CTC_SERVER_TASK_H_ */
