/**
 ****************************************************************************************
 *
 * @file ctc_server.h
 *
 * @brief Header file - Service Server Role
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 *
 ****************************************************************************************
 */

#ifndef _CTC_SERVER_H_
#define _CTC_SERVER_H_

/**
 ****************************************************************************************
 * @addtogroup CTC_SERVER 'Profile' Server
 * @ingroup CTC_SERVER
 * @brief 'Profile' Server
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include "rwip_config.h"
#include "ctc_server_task.h"
#include "prf_types.h"
#include "prf.h"

/*
 * DEFINES
 ****************************************************************************************
 */

#define ATT_SVC_CTC_SERVER_SERVICE ATT_UUID_16(0x0001)//
#define ATT_SVC_CTC_SERVER_CHAC1   ATT_UUID_16(0x0002)//write,write no response
#define ATT_SVC_CTC_SERVER_CHAC2   ATT_UUID_16(0x0003)//notify


///Maximum number of Server task instances
#define CTC_SERVER_IDX_MAX     0x01


/*
 * ENUMERATIONS
 ****************************************************************************************
 */

/// Possible states of the CTC_SERVER task
enum ctc_server_state
{
    /// Idle state
    CTC_SERVER_IDLE,
    /// busy state
    CTC_SERVER_BUSY,
    /// Number of defined states.
    CTC_SERVER_STATE_MAX
};

/// CTC_SERVER Service Attributes Indexes
enum
{
    CTC_SERVER_IDX_SVC,

    CTC_SERVER_IDX_DEMO_CHAR1,
    CTC_SERVER_IDX_DEMO_VAL1,
    
    CTC_SERVER_IDX_DEMO_CHAR2,
    CTC_SERVER_IDX_DEMO_VAL2,
    CTC_SERVER_IDX_DEMO_NTF_CFG,

    CTC_SERVER_IDX_NB,
};

/*
 * TYPE DEFINITIONS
 ****************************************************************************************
 */

/// 'Profile' Server environment variable
struct ctc_server_env_tag
{
    /// profile environment
    prf_env_t prf_env;
    /// On-going operation
    struct ke_msg * operation;
    /// Services Start Handle
    uint16_t start_hdl;
    /// CTC_SERVER task state
    ke_state_t state[CTC_SERVER_IDX_MAX];
    /// Notification configuration of peer devices.
    uint8_t ntf_cfg[BLE_CONNECTION_MAX];
};

/*
 * GLOBAL VARIABLE DECLARATIONS
 ****************************************************************************************
 */


/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Retrieve service profile interface
 *
 * @return service profile interface
 ****************************************************************************************
 */
const struct prf_task_cbs* ctc_server_prf_itf_get(void);

/**
 ****************************************************************************************
 * @brief Retrieve Attribute handle from service and attribute index
 *
 * @param[in] svc_idx CTC_SERVER Service index
 * @param[in] att_idx Attribute index
 *
 * @return CTC_SERVER attribute handle or INVALID HANDLE if nothing found
 ****************************************************************************************
 */
uint16_t ctc_server_get_att_handle(uint8_t att_idx);

/**
 ****************************************************************************************
 * @brief Retrieve Service and attribute index form attribute handle
 *
 * @param[out] handle  Attribute handle
 * @param[out] svc_idx Service index
 * @param[out] att_idx Attribute index
 *
 * @return Success if attribute and service index found, else Application error
 ****************************************************************************************
 */
uint8_t ctc_server_get_att_idx(uint16_t handle, uint8_t *att_idx);

/*
 * TASK DESCRIPTOR DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * Initialize task handler
 *
 * @param task_desc Task descriptor to fill
 ****************************************************************************************
 */
void ctc_server_task_init(struct ke_task_desc *task_desc);

/// @} CTC_SERVER

#endif /* _CTC_SERVER_H_ */
