/**
 ****************************************************************************************
 *
 * @file bleapp_server.h
 *
 * @brief Header file - Service Server Role
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 *
 ****************************************************************************************
 */

#ifndef _BLEAPP_SERVER_H_
#define _BLEAPP_SERVER_H_

/**
 ****************************************************************************************
 * @addtogroup BLEAPP_SERVER 'Profile' Server
 * @ingroup BLEAPP_SERVER
 * @brief 'Profile' Server
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include "rwip_config.h"
#include "bleapp_server_task.h"
#include "prf_types.h"
#include "prf.h"

/*
 * DEFINES
 ****************************************************************************************
 */

#define ATT_SVC_BLEAPP_SERVER_SERVICE ATT_UUID_16(0xFFE0) //
#define ATT_SVC_BLEAPP_SERVER_CHAC1 ATT_UUID_16(0xFFE1)   // RO
#define ATT_SVC_BLEAPP_SERVER_CHAC2 ATT_UUID_16(0xFFE4)   // notify               

/// Maximum number of Server task instances
#define BLEAPP_SERVER_IDX_MAX 0x01

/*
 * ENUMERATIONS
 ****************************************************************************************
 */

/// Possible states of the BLEAPP_SERVER task
enum bleapp_server_state
{
    /// Idle state
    BLEAPP_SERVER_IDLE,
    /// busy state
    BLEAPP_SERVER_BUSY,
    /// Number of defined states.
    BLEAPP_SERVER_STATE_MAX
};

/// BLEAPP_SERVER Service Attributes Indexes
enum
{
    BLEAPP_SERVER_IDX_SVC,

    BLEAPP_SERVER_IDX_DEMO_CHAR1,
    BLEAPP_SERVER_IDX_DEMO_VAL1,
    //BLEAPP_SERVER_IDX_DEMO_CHAR1_DESC,
		
	
    BLEAPP_SERVER_IDX_DEMO_CHAR2,
    BLEAPP_SERVER_IDX_DEMO_VAL2,
    BLEAPP_SERVER_IDX_DEMO_NTF_CFG,
    
    //BLE_APP_SERVER_IDX_DEMO_CHAR2_DESC,
	
    BLEAPP_SERVER_IDX_NB,
};

/*
 * TYPE DEFINITIONS
 ****************************************************************************************
 */

/// 'Profile' Server environment variable
struct bleapp_server_env_tag
{
    /// profile environment
    prf_env_t prf_env;
    /// On-going operation
    struct ke_msg *operation;
    /// Services Start Handle
    uint16_t start_hdl;
    /// BLEAPP_SERVER task state
    ke_state_t state[BLEAPP_SERVER_IDX_MAX];
    /// Notification configuration of peer devices.
    uint8_t ntf_cfg[BLE_CONNECTION_MAX];
};

/*
 * GLOBAL VARIABLE DECLARATIONS
 ****************************************************************************************
 */

/*
 * FUNCTION DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * @brief Retrieve service profile interface
 *
 * @return service profile interface
 ****************************************************************************************
 */
const struct prf_task_cbs *bleapp_server_prf_itf_get(void);

/**
 ****************************************************************************************
 * @brief Retrieve Attribute handle from service and attribute index
 *
 * @param[in] svc_idx BLEAPP_SERVER Service index
 * @param[in] att_idx Attribute index
 *
 * @return BLEAPP_SERVER attribute handle or INVALID HANDLE if nothing found
 ****************************************************************************************
 */
uint16_t bleapp_server_get_att_handle(uint8_t att_idx);

/**
 ****************************************************************************************
 * @brief Retrieve Service and attribute index form attribute handle
 *
 * @param[out] handle  Attribute handle
 * @param[out] svc_idx Service index
 * @param[out] att_idx Attribute index
 *
 * @return Success if attribute and service index found, else Application error
 ****************************************************************************************
 */
uint8_t bleapp_server_get_att_idx(uint16_t handle, uint8_t *att_idx);

/*
 * TASK DESCRIPTOR DECLARATIONS
 ****************************************************************************************
 */

/**
 ****************************************************************************************
 * Initialize task handler
 *
 * @param task_desc Task descriptor to fill
 ****************************************************************************************
 */
void bleapp_server_task_init(struct ke_task_desc *task_desc);

/// @} BLEAPP_SERVER

#endif /* _BLEAPP_SERVER_H_ */
