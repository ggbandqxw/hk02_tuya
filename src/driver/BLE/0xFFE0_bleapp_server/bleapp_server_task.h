/**
 ****************************************************************************************
 *
 * @file bleapp_server_task.h
 *
 * @brief Header file - Service Server Role Task.
 *
 * Copyright (C) RivieraWaves 2009-2016
 *
 *
 ****************************************************************************************
 */

#ifndef _BLEAPP_SERVER_TASK_H_
#define _BLEAPP_SERVER_TASK_H_

/**
 ****************************************************************************************
 * @addtogroup BLEAPP_SERVERSTASK Task
 * @ingroup BLEAPP_SERVER
 * @brief 'Profile' Task.
 *
 * The BLEAPP_SERVERSTASK is responsible for handling the messages coming in and out of the
 * @ref BAPS block of the BLE Host.
 *
 * @{
 ****************************************************************************************
 */

/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include "prf_types.h"
#include "rwip_task.h" // Task definitions
#include "co_utils.h"
#include "co_debug.h"
#include "rwapp_config.h"
/*
 * DEFINES
 ****************************************************************************************
 */

/// Maximal number of BLEAPP_SERVER that can be added in the DB
#define BLEAPP_SERVER_NB_INSTANCES_MAX (1)

#define BLEAPP_SERVER_MAX_CHAC_LEN BLE_MAX_OCTETS

/*
 * TYPE DEFINITIONS
 ****************************************************************************************
 */

/// Messages for BLEAPP_SERVER
/*@TRACE*/
enum bleapp_server_msg_id
{
    /// Start the Server - at connection used to restore bond data
    BLEAPP_SERVER_ENABLE_REQ = TASK_FIRST_MSG(TASK_ID_BLEAPP_SERVER), //!< BLEAPP_SERVER_ENABLE_REQ @ref struct bleapp_server_enable_req
                                                                  /// Confirmation of the Server start
    BLEAPP_SERVER_ENABLE_RSP, //!< BLEAPP_SERVER_ENABLE_RSP @ref struct bleapp_server_enable_req
                            /// Inform APP that Notification Configuration has been changed
    BLEAPP_SERVER_NTF_CFG_IND, //!< BLEAPP_SERVER_NTF_CFG_IND @ref struct bleapp_server_demo_ntf_cfg_ind
                             /// Send notification to gatt client
    BLEAPP_SERVER_SEND_NTF_CMD, //!< BLEAPP_SERVER_SEND_NTF_CMD @ref struct bleapp_server_send_ntf_cmd
    BLEAPP_SERVER_TIMEOUT_TIMER,
};

/*
 * APIs Structures
 ****************************************************************************************
 */

/// Parameters for the database creation
struct bleapp_server_db_cfg
{
    void *param;
};

/// Parameters of the @ref BLEAPP_SERVER_ENABLE_REQ message
struct bleapp_server_enable_req
{
    /// connection index
    uint8_t conidx;
    /// Notification Configuration
    uint8_t ntf_cfg;
};

/// Parameters of the @ref BLEAPP_SERVER_ENABLE_RSP message
struct bleapp_server_enable_rsp
{
    /// connection index
    uint8_t conidx;
    /// status
    uint8_t status;
};

/// Parameters of the @ref BLEAPP_SERVER_DEMO_NTF_CFG_IND message
struct bleapp_server_demo_ntf_cfg_ind
{
    /// connection index
    uint8_t conidx;
    /// Notification Configuration
    uint8_t ntf_cfg;
};

/// Parameters of the @ref BLEAPP_SERVER_SEND_NTF_CMD message
struct bleapp_server_send_ntf_cmd
{
    /// connection index
    uint8_t conidx;
    uint16_t length;
    uint8_t value[__ARRAY_EMPTY];
};

/// @} BLEAPP_SERVERSTASK

#endif /* _BLEAPP_SERVER_TASK_H_ */
