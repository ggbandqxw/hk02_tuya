/**
 * @file ble_rev_main.h
 * @brief
 *
 * @version 0.1
 * @date 2023-09-05
 *
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 *
 * @note          鼎新同创・智能锁
 *
 * @par 修改日志:
 * <1> 2023-09-05 v0.1 tianshidong 创建初始版本
 * *************************************************************************
 */

#ifndef _NB_MAIN_H_
#define _NB_MAIN_H_
#include "component.h"

//=============================================================================
//设备信息宏定义
//=============================================================================
#define PRODUCE_ID 0xdd80
#define SOUTH_ADDRESS "221.229.214.202"
#define SOUTH_PORT "5683"
//=============================================================================
//设备信息宏定义
//=============================================================================
// 0x01 配置鉴权信息
#define NB_LKM3_RESULT_SUCCESS 0X00     //添加成功
#define NB_LKM3_RESULT_PARAM_ERROR 0X03 //参数不合法
#define NB_LKM3_RESULT_AUTH_ERROR 0X04  //蓝牙鉴权码验证失败

//凭证状态
#define EVIDENCE_STATUS_FREEZE 0x01 // 冻结状态
#define EVIDENCE_STATUS_ACTIVE 0x02 // 启用状态

// 0x03 0x04 0x1f 添加/删除/网络开门  凭证 回复信息
#define EVIDENCE_ACK_SUCCESS 0x00                    //添加成功          //删除成功
#define EVIDENCE_ACK_ERROR 0x01                      //表示失败
#define EVIDENCE_ACK_FULL 0x02                       //凭证已满
#define EVIDENCE_ACK_AUTH_ERROR 0x03                 //指令鉴权码错误    //删除 标识鉴权码失败
#define EVIDENCE_ACK_ID_EXIST 0x05                   //凭证ID已存在
#define EVIDENCE_ACK_ID_NOT_EXIST 0x06               //凭证ID不存在
#define EVIDENCE_ACK_DATE_ERROR 0x07                 //日期错误
#define EVIDENCE_ACK_AUTH_NOT_CFG 0x0A               //鉴权码未配置
#define EVIDENCE_ACK_TIME_NOT_ARRIVED 0X0E           //时间未到
#define EVIDENCE_ACK_OUT_OF_DATA 0X0F                //凭证过期
#define EVIDENCE_ACK_FREEZE 0X10                     //凭证被冻结
#define EVIDENCE_ACK_OPEN_LOCK_TIMES_DEFICIENCY 0X11 //凭证开门次数不足

//删除凭证
#define DEL_SINGLE_EVIDENCE 0x01 //删除单个凭证
#define DEL_ALL_EVIDENCE 0x02    //删除全部凭证

//更新凭证
#define UPDATE_SINGLE_EVIDENCE 0x01 //更新单个凭证
#define UPDATE_ALL_EVIDENCE 0x02    //更新全部凭证，此时凭证ID为0

//清空数据
#define CLEAR_EVIDENCE_INFO 0x01 //清空凭证信息
#define CLEAR_EVIDENCE_LOG 0x02  //清空凭证操作日志

//=============================================================================
//设备信息宏定义
//=============================================================================
#define NB_LKM3_CMD_SET_AES_KEY 0X01              //设置门锁密钥
#define NB_LKM3_CMD_SET_AUTHENTICATION 0X02       //设置配置信息
#define NB_LKM3_CMD_ADD_EVIDENCE 0X03             //添加凭证信息
#define NB_LKM3_CMD_DEL_EVIDENCE 0X04             //删除凭证信息
#define NB_LKM3_CMD_MODIFY_EVIDENCE 0X05          //修改凭证信息
#define NB_LKM3_CMD_UPDATE_EVIDENCE 0X06          //更新凭证信息
#define NB_LKM3_CMD_RESET_DATA 0X07               //清空门锁上存储的所有凭证信息或者凭证日志
#define NB_LKM3_CMD_RESET_FACTORY 0X08            //恢复出厂设置，清空凭证信息、凭证操作日志、指令鉴权码
#define NB_LKM3_CMD_RESET_DEVICE 0X09             //门锁系统重启
#define NB_LKM3_CMD_SET_TIME 0X0A                 //设置时间，将服务端的时间同步至门锁，用于门锁时间的校准
#define NB_LKM3_CMD_SET_VOLUME 0X0B               //设置音量，调节门锁音量大小。
#define NB_LKM3_CMD_SET_SOUTH_ADDRESS 0X0C        //设置云平台南向地址
#define NB_LKM3_CMD_SET_HEART_BRET_PERIOD 0X0D    //设置心跳上报周期，可设置门锁NB通道定时上报数据的频率，比如24小时1次，8小时3次，5分钟一次等。
#define NB_LKM3_CMD_NET_OPEN_LOCK 0X1F            //网络开门， 使用凭证ID和凭证值操作蓝牙开门
#define NB_LKM3_CMD_INQUIRY_VERSION_INFO 0X0F     //查询版本信息，包含了软件版本，硬件版本，NB模组IMEI号和蓝牙版本。软件版本中为代码版本，硬件版本为主板版本和产品系列，蓝牙版本包含了蓝牙MAC地址和蓝牙固件版本。
#define NB_LKM3_CMD_REPORT_VERSION 0X10           //上报版本信息，NB入网后会主动上报版本信息，其中包含了软件版本，硬件版本，NB模组IMEI号和蓝牙版本。软件版本中为代码版本硬件版本为主板版本和产品系列，蓝牙版本包含了蓝牙MAC地址和蓝牙固件版本
#define NB_LKM3_CMD_REPORT_HEARTBEAT_INFO 0X11    //上报心跳信息
#define NB_LKM3_CMD_REPORT_OPEN_LOCK_RECORD 0X12  //上报开锁记录信息
#define NB_LKM3_CMD_REPORT_ALARM_INFO 0X13        //上报告警信息，告警信息包含：撬锁、低电量、胁迫开门、开门失败信息。
#define NB_LKM3_CMD_REPORT_RESET_FACTORY 0X14     //上报恢复出厂设置，门锁被恢复出厂设置后会上报指令0x14的空包数据。
#define NB_LKM3_CMD_SET_FREQUENCY_OF_RESTART 0X1E //设置重启频次，设置门锁定期自动重启频次
#define NB_LKM3_CMD_INQUIRY_CONFIG_INFO 0X20      //查询配置信息，可查询门锁NB南向地址信息、门锁本地时间、门锁运行模式
#define NB_LKM3_CMD_INQUIRY_EVIDENCE_LIST 0X22    //查询凭证列表，其中包含了冻结状态凭证、启用状态凭证、有效期期内凭证、过期凭证。
#define NB_LKM3_CMD_INSTALL_LOCK_INFO 0X23        //安装门锁初始化，其中包含了恢复出厂设置、设置密钥、设置配置信息、添加临时凭证
#define NB_LKM3_CMD_INSTALL_LOCK_COMPLETE 0X24    //安装门锁完成，门锁安装结束，删除所有凭证、重启设备。

/* OTA相关命令 */
#define OTA_MAX_SIZE (256 + 16)                // 网关能支持的下发每个分片包的最大长度，从包头开始计算
#define NB_LKM3_OTA_CMD_UPDATE_REQUEST 0X17    // 升级请求
#define NB_LKM3_OTA_CMD_FILE_TRANSMIT 0X19     // 传输文件分包片
#define NB_LKM3_OTA_CMD_FILE_TRANSMIT_END 0X1A // 传输结束确认
#define OTA_ALLOWED_UPGTADE 0x12               // 允许升级
#define OTA_HARDWARE_VER_ERR 0x13              // 硬件版本错误
#define OTA_CODE_LARGE 0x14                    // bin 文件过大
#define OTA_SOFT_VER_ERR 0x15                  // 软件版本错误
#define OTA_SOFT_VER_SAME 0x16                 // 要升级的版本已在本地升级成功
#define OTA_WIRTE_SUCC 0x01                    // 表示写入成功
#define OTA_WIRTE_ERR 0x02                     // 表示写入失败
#define OTA_PACKNUM_ERR 0x03                   // 表示分片序号不正确
#define OTA_UPGTADE_SUCC 0x01                  // 下载成功确认
#define OTA_SOFT_VER_ERR 0x02                  // 版本错误
#define OTA_SOFT_PACKNUMS_ERR 0x03             // 分片总数错误
#define OTA_CRC_ERR 0x04                       // HASH 校验错误
//=============================================================================
//命令数据结构类型
//=============================================================================
#pragma pack(1)

/* NB 关键参数状态 */

/* 测试用：临时存储凭证ID */
typedef struct
{
    uint8_t storeEvidenceID;       //凭证ID;
    uint8_t storeEvidenceValue[8]; //凭证值;
    uint32_t storefailureTime;     //指令失效时间;
    uint32_t storestartTimeStamp;  //生效时间时间戳（本地时间戳）
    uint32_t storeendTimeStamp;    //失效时间时间戳（本地时间戳）
    uint8_t storeEvidenceStatus;   //凭证状态
} NB_delEvidence_store_stu_t;

typedef struct
{
    uint8_t Evidence[EVIDENCE_LEN_MAX]; //凭证值

    uint8_t EvidenceId;     //凭证ID
    uint8_t EvidenceStatus; //凭证状态
    uint8_t attr;           //凭证属性：永久、一次性、策略……
    uint8_t week;           //钥匙策略：周几生效（为0表示当前策略类型为年月日策略）
    uint32_t stime;         //生效时间
    uint32_t etime;         //失效时间
    uint32_t ctime;         //钥匙创建时间
} NB_NvEvidence_stu_t;

/* 0x01门锁设置密钥回复 */
typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_Set_key_ack_stu_t;

/* 0x02设置配置信息 */
typedef struct
{
    uint8_t cmd;
    uint32_t failureTime; //指令失效时间;
    uint8_t Auth[8];      //指令鉴权码;
} NB_Set_Auth_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_Set_Auth_ack_stu_t;

/* 0x03添加凭证信息 */
typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;     //指令失效时间;
    uint8_t Auth[8];          //指令鉴权码;
    uint8_t addEvidenceID;    //凭证ID;
    uint8_t evidenceValue[8]; //凭证值;
    uint32_t startTimeStamp;  //生效时间时间戳（本地时间戳）
    uint32_t endTimeStamp;    //失效时间时间戳（本地时间戳）
} NB_addEvidence_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_addEvidence_ack_stu_t;

/* 0x04删除凭证信息 */
typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;  //指令失效时间;
    uint8_t Auth[8];       //指令鉴权码;
    uint8_t operationType; //操作类型
    uint8_t delEvidenceID; //要删除的凭证ID
} NB_delEvidence_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_delEvidence_ack_stu_t;

/* 0x1f网络开门指令 */
typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;     //指令失效时间;
    uint8_t Auth[8];          //指令鉴权码;
    uint8_t evidenceID;       //凭证ID;
    uint8_t evidenceValue[8]; //凭证值;
    uint32_t serverTimeStamp; //服务器当前时间
} NB_netOpenLock_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
    uint8_t evidenceID;         //凭证ID;
    uint8_t batteryLevel;       //电池电量
    uint8_t openLockType;       //开锁类型
    uint32_t openLockTimeStamp; //开锁时间
    uint16_t recordId;          //记录编号，开锁记录编号
} NB_netOpenLock_ack_stu_t;

/* 0x05修改凭证信息 */

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;    //指令失效时间;
    uint8_t Auth[8];         //指令鉴权码;
    uint8_t evidenceID;      //凭证ID
    uint32_t startTimeStamp; //生效时间时间戳（本地时间戳）
    uint32_t endTimeStamp;   //失效时间时间戳（本地时间戳）
} NB_modifyEvidence_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_modifyEvidence_ack_stu_t;

/* 0x06更新凭证状态 */

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;   //指令失效时间;
    uint8_t Auth[8];        //指令鉴权码;
    uint8_t operationType;  //操作类型
    uint8_t evidenceID;     //被更新的凭证ID
    uint8_t evidenceStusus; //凭证状态
} NB_updateEvidence_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_updateEvidence_ack_stu_t;

/* 0x07清空数据 */

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;  //指令失效时间;
    uint8_t Auth[8];       //指令鉴权码;
    uint8_t operationType; //清除数据类型
} NB_resetEvidenceInfo_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_resetEvidenceInfo_ack_stu_t;

/* 0x08 恢复出厂设置 */

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;  //指令失效时间;
    uint8_t Auth[8];       //指令鉴权码;
    uint8_t operationType; //操作类型
} NB_resetFactore_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_resetFactore_ack_stu_t;

/* 0x09 重启设备 */

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;  //指令失效时间;
    uint8_t Auth[8];       //指令鉴权码;
    uint8_t operationType; //操作类型
} NB_restartDevice_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_restartDevice_ack_stu_t;

/* 0x0b 设置音量 */
typedef enum
{
    SET_VOLUME_HIGH = 0X01,
    SET_VOLUME_MIDDLE = 0X02,
    SET_VOLUME_LOW = 0X03,
} NB_setVolume_enum_t;

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime; //指令失效时间;
    uint8_t Auth[8];      //指令鉴权码;
    uint8_t volume;       //音量
} NB_setVolume_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_setVolume_ack_stu_t;

/* 0x0A 设置时间 */

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime; //指令失效时间;
    uint8_t Auth[8];      //指令鉴权码;
    uint32_t timeStamps;  //服务器端时间
} NB_setTime_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_setTime_ack_stu_t;

/* 0x0c 云平台南向地址 */

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;     //指令失效时间;
    uint8_t Auth[8];          //指令鉴权码;
    uint32_t southIpAddr[16]; //南向IP地址
    uint32_t port;            //端口
} NB_setSouthAddr_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_setSouthAddr_ack_stu_t;

/* 0x0D 设置心跳上报周期 */
#define HEART_BEAT_NAME "HEART_BEAT"
typedef enum
{
    HEARTBEAT_1_TIMES = 0, //表示一天1次
    HEARTBEAT_2_TIMES,     //表示一天2次
    HEARTBEAT_3_TIMES,     //表示一天3次
    HEARTBEAT_12_TIMES,    //表示一天12次
    HEARTBEAT_24_TIMES,    //表示一天24次
    HEARTBEAT_48_TIMES,    //表示一天48次
    HEARTBEAT_288_TIMES,   //表示一天288次
    HEARTBEAT_MAX_TIMES,
} NB_setHeartbeat_enum_t;

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime; //指令失效时间;
    uint8_t Auth[8];      //指令鉴权码;
    uint8_t period;       //心跳上报周期
} NB_setHeartBeatPeriod_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_setHeartBeatPeriod_ack_stu_t;

/* 0x0F 查询版本信息 */

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime; //指令失效时间;
    uint8_t Auth[8];      //指令鉴权码;
} NB_inquiryVersioin_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
    uint16_t softVersion;   //软件版本
    uint8_t hardVersion[4]; //硬件版本
    uint8_t moduleId[8];    //通讯模组ID
    uint8_t bleModuleId[8]; //蓝牙ID
} NB_inquiryVersion_ack_stu_t;

/* 0x10 上报版本信息 */
typedef enum
{
    JOIN_NETWORK_POWER_ON = 0X01,     //开机自动组网
    JOIN_NETWORK_MANUAL,              //手动组网
    JOIN_NETWORK_FAILED_RETRY,        //组网不成功重新组网
    JOIN_NETWORK_PING_FAILED_RETRY,   // ping不同重新组网
    JOIN_NETWORK_SET_SOUTHADDR_RETRY, //设置了南向地址重新组网
} NB_reportVersioin_enum_stu_t;

typedef struct
{
    uint8_t cmd;
    uint16_t softVersion;   //软件版本
    uint8_t hardVersion[4]; //硬件版本
    uint8_t moduleId[8];    //通讯模组ID
    uint8_t bleModuleId[8]; //蓝牙ID
    uint8_t netTrigger;     //入网出发
    uint8_t simIccid[15];   // SIM卡ICCID
} NB_reportVersioin_cmd_stu_t;

/* 0x11 上报心跳信息 */
typedef enum //普通心跳类型
{
    HEART_BEAT_TYPE_NATURAL = 0x01,     //自然心跳
    HEART_BEAT_TYPE_TOUCH = 0x02,       //触摸屏幕唤醒上报
    HEART_BEAT_TYPE_RCC_RELEASE = 0X03, //表示RRC连接释放心跳
} NB_reportHeartBeatNormal_enum_stu_t;

typedef enum //启动心跳类型
{
    HEART_BEAT_START_TYPE_NATURAL = 0x00,      //自然定时心跳
    HEART_BEAT_START_TYPE_POWER_ON = 0x01,     //上电启动第一次心跳
    HEART_BEAT_START_TYPE_SOFT_RESTART = 0X02, //软重启第一次心跳及网络重连心跳
} NB_reportHeartBeatStart_enum_stu_t;
typedef enum //设备当前状态
{
    DEVICE_CUR_STATUS_NORMAL = 0x00,       //正常状态
    DEVICE_CUR_STATUS_POWER_SAVING = 0x01, //省电状态
} NB_reportDeviceCurStatus_enum_stu_t;

typedef enum //设备运行状态
{
    DEVICE_RUN_STATUS_MANUAL_POWER_SAVING = 0x01, //手动强制进入省电模式
    DEVICE_RUN_STATUS_NORMAL = 0x02,              //正常模式
    DEVICE_RUN_STATUS_ALWAYS_OPEN = 0x03,         //常开模式下，压把手直接开门无需蓝牙验证身份，用于整租的卧室开门或者空置房屋频繁带看场景
} NB_reportDeviceRunStatus_enum_stu_t;
typedef struct // 2B代表正，2D代表负
{
    uint8_t signalPower[3]; //信号强度 2D0846 = -84.6
    uint8_t snr[3];         // snr信噪比 2B0273 = 27.3
    uint8_t txPower[3];     // TX power终端发射功率 2B0230 = 23.0
    uint8_t cellId[6];      // Cell ID网络中小区的编号 2B0245014354
    uint8_t pci[3];         // PCI物理小区标识，区分不同小区的无线信号
    uint8_t earfcn[3];      // EARFCN基站频点号
} NB_reportNbinfo_stu_t;
typedef struct
{
    uint8_t cmd;
    uint8_t batteryLevel;    //电量信息
    uint8_t heartBeatType;   //心跳类型
    uint8_t heartBeatPeriod; //自然心跳类型
    uint8_t startHeartBeat;  //启动类型心跳p
    uint8_t deviceCurStutas; //设备当前状态
    uint8_t deviceRunMode;   //设备运行模式
    uint8_t destroyStatus;   //防撬开关状态
    uint8_t NbInfo[22];      // NB信息
} NB_reportHeartBeatInfo_cmd_stu_t;

/* 0x12 上报开锁记录信息 */
typedef struct
{
    uint8_t cmd;
    uint8_t EvidenceID;     //凭证ID
    uint8_t batteryLevel;   //电量信息
    uint8_t openLockType;   //开门类型
    uint32_t openTimeStamp; //开门时间
    uint8_t openLockInfo;   //开门信息
    uint16_t recordId;      //记录编号
} NB_reportOpenLockInfo_cmd_stu_t;

/* 0x13 上报告警信息 */
typedef enum
{
    ALARM_TYPE_PRY_LOCK = 0x02,               //撬锁
    ALARM_TYPE_BATTERY_LOW = 0x04,            //电量不足
    ALARM_TYPE_HIJACK = 0x05,                 //表示胁迫报警
    ALARM_TYPE_UNLOCK_FAIlED = 0x06,          //表示开门失败，或凭证时间不符
    ALARM_TYPE_UNLOCK_TIMES_ERROR = 0X07,     //表示开门失败，开门次数不足
    ALARM_TYPE_UNLOCK_EVIDENCE_FAILED = 0X08, //表示开门失败，凭证被冻结
    ALARM_TYPE_UNLOCK_EVIDENCE_TRY = 0x09,    //表示凭证试开报警
} NB_alarmType_enum_stu_t;

typedef struct
{
    uint8_t cmd;
    uint8_t batteryLevel;       //电量信息
    uint32_t openTimeStamp;     //开门时间
    uint8_t alarmType;          //告警类型
    uint8_t EvidenceID;         //凭证ID
    uint32_t EvidenceStartTime; //有效时间
    uint32_t EvidenceEndTime;   //有效时间
} NB_reportAlarmInfo_cmd_stu_t;

/* 0x14 上报恢复出厂设置 */
typedef struct
{
    uint8_t cmd;
} NB_reportResetFactory_cmd_stu_t;

/* 0x1E 设置门锁定期自动重启频次*/
#define RESTART_NAME "RESTART"
typedef enum
{
    RESTART_TIMES_3_TIMES,  // 表示一天3次
    RESTART_TIMES_5_TIMES,  // 表示一天5次
    RESTART_TIMES_10_TIMES, // 表示一天10次
    RESTART_TIMES_20_TIMES, // 表示一天20次
    RESTART_TIMES_30_TIMES, // 表示一天30次
    RESTART_TIMES_MAX_TIMES
} NB_frequencyRestart_enum_stu_t;

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;    //指令失效时间;
    uint8_t Auth[8];         //指令鉴权码;
    uint8_t softRestartFreq; //软件重启频次
} NB_frequencyRestart_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_frequencyRestart_ack_stu_t;

/* 0x20 查询配置信息 */
typedef enum
{
    MODE_FORCE_POWER_SAVE = 0X01, //手动强制进入省电
    MODE_NORMAL = 0X02,           //正常模式
    MODE_ALWAYS_OPEN = 0X03,      //常开模式
} NB_inquiryConfigInfo_enum_stu_t;

typedef struct
{
    uint8_t cmd;
    uint32_t failureTime; //指令失效时间;
    uint8_t Auth[8];      //指令鉴权码;
} NB_inquiryConfigInfo_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
    uint8_t southIpAddr[16]; //南向IP地址
    uint8_t port[4];         //端口
    uint8_t lockRunMode;     //门锁运行模式
} NB_inquiryConfigInfo_ack_stu_t;

/* 0x22 查询凭证列表 */
typedef enum
{
    INQUIRY_ALL_EVIDENCE = 0, //查询所有凭证
    INQUIRY_FREEZE_EVIDENCE,  //查询所有冻结状态凭证
    INQUIRY_ACTIVE_EVIDENCE,  //查询所有启动状态凭证
    INQUIRY_VALID_EVIDENCE,   //查询所有有效期内凭证
    INQUIRY_INVALID_EVIDENCE, //查询所有过期的凭证
} NB_inquiryEvidenceList_enum_stu_t;

typedef struct
{
    uint8_t evidenceID;       //凭证ID
    uint8_t evidenceValue[8]; //凭证值
    uint8_t openLockTimes;    //有效开门次数
    uint32_t startTimeStamp;  //生效时间时间戳（本地时间戳）
    uint32_t endTimeStamp;    //失效时间时间戳（本地时间戳）
    uint8_t EvidenceStusus;   //凭证状态
} NB_EvidenceList_type_stu_t;
typedef struct
{
    uint8_t cmd;
    uint32_t failureTime; //指令失效时间;
    uint8_t Auth[8];      //指令鉴权码;
    uint8_t evidenceType; //凭证类别
} NB_inquiryEvidenceList_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
    uint8_t evidenceNum;                       //凭证个数
    NB_EvidenceList_type_stu_t evidenceList[]; //凭证列表
} NB_inquiryEvidenceList_ack_stu_t;

/* 0x23 门锁安装初始化 */
typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;     //指令失效时间;
    uint8_t AuthVerify[8];    //指令鉴权码校验老的;
    uint8_t key[16];          //密钥
    uint8_t AuthSet[8];       //指令鉴权码，设置新的;
    uint8_t EvidenceID;       //凭证ID
    uint8_t evidenceValue[8]; //凭证值
    uint32_t startTimeStamp;  //生效时间时间戳（本地时间戳）
    uint32_t endTimeStamp;    //失效时间时间戳（本地时间戳）
} NB_lockInstall_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_lockInstall_ack_stu_t;

/* 0x23 门锁安装完成 */
typedef struct
{
    uint8_t cmd;
    uint32_t failureTime;  //指令失效时间;
    uint8_t AuthVerify[8]; //指令鉴权码校验老的;
} NB_lockInstallComplete_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;
} NB_lockInstallComplete_ack_stu_t;

typedef struct
{
    uint8_t cmd;
    uint8_t Auth[8];        // 指令鉴权码
    uint8_t hardVersion[4]; // 硬件版本
    uint16_t softVersion;   // 软件版本
    uint8_t fileSize[3];    // 文件大小
} NB_otaReq_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;       // 鉴权结果
    uint16_t updResuit;   // 是否允许升级
    uint16_t softVersion; // 软件版本
    uint16_t breakpoint;  // 断点位置
    uint16_t MaxSize;     // 每包最大长度
} NB_otaReq_ack_stu_t;

typedef struct
{
    uint8_t cmd;
    uint16_t packNum;  // 分片包序号
    uint16_t packSize; // 分片包大小
    uint8_t data[];    // 固件内容
} NB_otaFile_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result;   // 结果
    uint16_t packNum; // 分片包序号
} NB_otaFile_ack_stu_t;

typedef struct
{
    uint8_t cmd;
    uint16_t softVersion; // 软件版本
    uint16_t packNums;    // 分片总数
} NB_otaEnd_cmd_stu_t;

typedef struct
{
    uint8_t ackCmd;
    uint8_t result; // 结果
} NB_otaEnd_ack_stu_t;

#pragma pack()
/* 消息回调函数类型 */
typedef void (*MessageCallback_fun_t)(uint8_t result);

static uint8_t NbMain_send_readData_cmd(uint8_t operation);
static uint8_t NbMain_get_CSQ(MessageCallback_fun_t cb);

#define TEST_AEP_OTA 0

#define AEP_OTA_REQ_ONEPACK_MAX_TIME 5 //同一包数据请求最大次数
#define AEP_OTA_QUER_DEVICE_VER 0x13   // 19 查询设备版本
#define AEP_OTA_NEW_VER_NOTIFY 0x14    // 20 新版本通知
#define AEP_OTA_REQ_UPD 0x15           // 21 请求升级包
#define AEP_OTA_REP_UPD_PACK_STA 0x16  // 22 上报升级包下载状态
#define AEP_OTA_PERFROM_UPD 0x17       // 23 执行升级
#define AEP_OTA_REP_UPD_RES 0x18       // 24 上报升级结果
#define AEP_OTA_SUCCESS 0X00           //处理成功
#define AEP_OTA_DEV_BUSY 0X01          //设备使用中
#define AEP_OTA_DEV_SIGNAL_LOW 0X02    //信号质量差
#define AEP_OTA_VER_IS_NEV 0X03        //已经是最新版本
#define AEP_OTA_LOW_BAT 0X04           //电量不足
#define AEP_OTA_NO_FLASH 0X05          //剩余空间不足
#define AEP_OTA_DOWNLOAD_TIMEOUT 0X06  //下载超时
#define AEP_OTA_CRC_ERR 0X07           //升级包校验失败
#define AEP_OTA_TPE_ERR 0X08           //升级包类型不支持
#define AEP_OTA_NO_RAM 0X09            //内存不足
#define AEP_OTA_PROGRAM_FLASH_ERR 0X0A //安装升级包失败
#define AEP_OTA_IDENTIFY_ERR 0X7F      //内部异常	表示无法识别的异常
#define AEP_OTA_TASK_ERR 0X80          //升级任务不存在
#define AEP_OTA_PACK_ERR 0X81          //指定的分片不存在

typedef enum
{
    AEP_OTA_STA_NONE,
    AEP_OTA_STA_QUER_CODE_INFOR, // 请求固件信息
    AEP_OTA_STA_DOWNLOADING,     // 固件下载中
    AEP_OTA_STA_DOWNLOAD_OK,     // 固件下载完成

    // AEP_OTA_STA_QUER_DEVICE_VER, //查询设备版本
    // AEP_OTA_STA_REDAY_UPD,       // 准备刷机
    // AEP_OTA_STA_REP_UPD_RES,     // 上报升级结果
} AepOta_sta_enum_stu_t;
#pragma pack(1)
typedef struct
{
    uint8_t result;
    uint8_t ver[16];
} AepOta_QuerDeviceVer_stu_t, AepOta_ReqUpdRes_stu_t;

typedef struct
{
    uint8_t ver[16];
    uint16_t singPackSize;
    uint16_t PackNums;
    uint16_t crc;
} AepOta_NewVerNotify_stu_t;

typedef struct
{
    uint8_t result;
    uint16_t PackNum;
    uint8_t data[];
} AepOta_ReqUpdPackInfor_stu_t;

typedef struct
{
    uint8_t ver[16];
    uint16_t PackNum;
} AepOta_ReqUpd_stu_t;

typedef struct
{
    AepOta_NewVerNotify_stu_t NewVerInfor; //正在升级的信息
    uint16_t recvDataCount;                //断点处
    uint8_t status;                        //升级状态
    uint8_t errCnt;                        //失败次数
    uint16_t ValidPackNums;                //有效包数量
} AepOta_NewVerBackups_stu_t;

#pragma pack()
#endif
