
#include "ble_rev_main.h"
#include "debug_config.h"
#include "nb_main.h"
#include "local_main.h"
#include "application.h"

static TimerHandle_stu_t bleActiveTimer = NULL;       //激活时，60s不休眠
static TimerHandle_stu_t bleSendToNbDataTimer = NULL; //转发NB命令时，先唤醒NB后，等待500ms再转发
static uint8_t bleActiveFlag = RESET;
uint8_t bleSendToNbData[512] = {0};
uint8_t bleSendToNbDataLen = 0;
static uint8_t bleConnectSta = 0;
static TimerHandle_stu_t bledisconnect_timer = NULL; //开门成功后500ms断开蓝牙连接
extern TestMode_enum_t ATE_GetProductionTestMode(void);
/**
 * @brief 发送命令字
 *
 * @param [in] len
 * @param [in] pData
 * @return uint8_t
 *
 * @note
 */
static uint8_t BleRevMain_send_Cmd(uint8_t *pData, uint8_t len)
{
    BleCallbackMsg_t msg = {
        .source = BLE_MSG_CMD,
        .cmd = 0,
        .ret = 0,
        .len = len,
        .pData = {0},
    };
    if (len <= sizeof(msg.pData))
    {
        memcpy(msg.pData, pData, len);
    }
    else
    {
        FUNC_BLE_LOG_I("Send cmd len error");
        return 0;
    }
    FUNC_BLE_LOG_HEX("Ble Send cmd data:", msg.pData, len);

    Ble_SendDataToBle(&msg, len);
}

/**
 * @brief 获取当前是否在激活状态
 *
 * @return uint8_t
 *
 * @note
 */
static uint8_t BleMain_get_activeStatus(void)
{

    return bleActiveFlag;
}
/**
 * @brief 转发NB命令时，先唤醒NB后，等待500ms再转发
 *
 * @param [in] xTimer
 *
 * @note
 */
static void bleSendToNbDataCallback(TimerHandle_stu_t xTimer)
{
    FUNC_BLE_LOG_I("bleSendToNbDataCallback");
    SYS_CALL(NB_TASK_receive_ble_convert_data_handle, bleSendToNbData, bleSendToNbDataLen);
    bleSendToNbDataTimer = NULL;
    memset(bleSendToNbData, 0, sizeof(bleSendToNbData));
    bleSendToNbDataLen = 0;
}
/**
 * @brief 激活时，60s超时函数
 *
 * @param [in] xTimer
 *
 * @note
 */
static void bleUpdateAdvTimerCallback(TimerHandle_stu_t xTimer)
{
    bleActiveTimer = NULL;
    bleActiveFlag = RESET;
    FUNC_BLE_LOG_I("Active device timer time out");
}

/**
 * @brief 开锁成功上报500ms后，主动断开蓝牙
 *
 * @param [in] xTimer
 *
 * @note
 */
static void bleDisconnectTimerCallback(TimerHandle_stu_t xTimer)
{
    Ble_Setconnect(0);//断开蓝牙连接
    if (bledisconnect_timer != NULL)
    {
        OSAL_TimerDelete(bledisconnect_timer);
        bledisconnect_timer = NULL;
    }
    FUNC_BLE_LOG_I("bleDisconnectTimerCallback");
}

/**
 * @brief 上报开锁状态
 *
 *
 * @note
 */
static void FuncBle_lkm3_unLockSta_Handle(uint8_t result, uint8_t unLockId, uint32_t timeStamp)
{
    Ble_lockStatus_ack_t ack = {0};
    bleActiveFlag = RESET; //厂测激活后，开锁测试成功后，此激活状态复位

    if (result == USERS_ID_RFEEZE) //冻结状态
    {
        ack.result = OSAL_SWAP16(CMD_EVIDENCE_RFEEZE);
    }
    else if (result == 0x06) // 凭证不存在
    {
        ack.result = OSAL_SWAP16(CMD_EVIDENCE_NOEXiST);
    }
    else if (result == USERS_ID_SYSLOCK) // 系统锁定
    {
        ack.result = OSAL_SWAP16(CMD_EVIDENCE_ERROR);
    }
    else
    {
        (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));
    }
    ack.massageType = BLE_LKM3_ACK_LOCK_STATUS;
    ack.evidenceId = unLockId;
    ack.batLev = SYS_CALL(Battery_Get);
    ack.time = OSAL_SWAP32(timeStamp);
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));

        if (bledisconnect_timer != NULL)
        {
            OSAL_TimerDelete(bledisconnect_timer);
            bledisconnect_timer = NULL;
        }
        bledisconnect_timer = OSAL_TimerCreate(bleDisconnectTimerCallback, 500, RESET);
}

/**
 * @brief 通用应答
 *
 *
 * @note
 */
static void FuncBle_lkm3_general_ack_Handle(uint32_t cmd, uint8_t result)
{
    Ble_genaral_ack_t ack = {0};
    uint16_t massageType = (uint16_t)cmd;

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(massageType);
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 获取凭证列表
 *
 *
 * @note
 */
static void FuncBle_lkm3_getEvidenceList_Handle(uint8_t result, uint8_t type, uint8_t evidNum, uint8_t *evidId)
{
    uint8_t tmp[32] = {0};

    Ble_getEvidenceList_ack_t *ack = (Ble_getEvidenceList_ack_t *)tmp;

    (result == EVIDENCE_ACK_SUCCESS) ? (ack->result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack->result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    FUNC_BLE_LOG_D("result:%x, type:%x, evidNum:%d, totalLen:%d", result, type, evidNum, sizeof(Ble_getEvidenceList_ack_t) + evidNum);

    ack->massageType = OSAL_SWAP16(BLE_LKM3_ACK_GET_EVIDENCE_LIST);
    ack->evidenceType = type;
    ack->evidenceNum = evidNum;
    memcpy(ack->idList, evidId, evidNum);
    BleRevMain_send_Cmd((uint8_t *)ack, sizeof(Ble_getEvidenceList_ack_t) + evidNum);
}

/**
 * @brief 设置门锁运行模式
 *
 *
 * @note
 */
static void FuncBle_lkm3_setLockMode_Handle(Ble_setLockMode_cmd_t *pData)
{
    Ble_setLockMode_ack_t ack = {0};
    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_SET_LOCK_MODE);
    ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS);
    ack.lockMode = pData->lockMode;
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 查询配置信息
 *
 *
 * @note
 */
static void FuncBle_lkm3_inquiryConfig_Handle(uint8_t result, uint8_t *southAddr, uint8_t *port, uint8_t lockMode)
{
    Ble_inquiryConfig_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_INQUIRY_CONFIG);
    memcpy(ack.southAddress, southAddr, sizeof(ack.southAddress));
    memcpy(ack.port, port, sizeof(ack.port));

    ack.southAddress[15] = 0x2e;
    for (uint8_t i = 0; i < sizeof(ack.southAddress); i++)
    {
        if (ack.southAddress[i] != 0x2e)
        {
            ack.southAddress[i] -= '0';
        }
    }

    for (uint8_t i = 0; i < sizeof(ack.port); i++)
    {
        if (ack.port[i] != 0x2e)
        {
            ack.port[i] -= '0';
        }
    }

    ack.lockMode = lockMode;
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 设置重启频次
 *
 *
 * @note
 */
static void FuncBle_lkm3_restertFre_Handle(uint8_t result, uint8_t restartFre)
{
    Ble_restertFre_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_SET_RESTERT_FRE);
    ack.fre = restartFre;
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 查询版本信息
 *
 *
 * @note
 */
static void FuncBle_lkm3_version_Handle(uint8_t result, uint16_t softVer, uint8_t *hardVer, uint8_t *nbId, uint8_t *bleId)
{
    Ble_version_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_INQUIRY_VERSION);
    ack.sw = softVer;
    memcpy(ack.hw, hardVer, sizeof(ack.hw));
    memcpy(ack.NbId, nbId, sizeof(ack.NbId));
    memcpy(ack.BleId, bleId, sizeof(ack.BleId));
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 上报心跳周期
 *
 *
 * @note
 */
static void FuncBle_lkm3_heardBeatSet_Handle(uint8_t result, uint8_t beat)
{
    Ble_heardBeatSet_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_HEART_BEAT_SET);
    ack.beat = beat;
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 设置时间
 *
 *
 * @note
 */
static void FuncBle_lkm3_timeSet_Handle(uint8_t result, uint32_t timeStamps)
{
    Ble_timeSet_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_TIME_SET);
    ack.time = OSAL_SWAP32(timeStamps);
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 更新凭证信息
 *
 * @param [in] result NB处理结果
 * @param [in] opeType 更新类型，全部或者单个
 * @param [in] evidId 凭证ID
 * @param [in] evidStu 凭证状态
 *
 * @note
 */
static void FuncBle_lkm3_updateEvidence_Handle(uint8_t result, uint8_t opeType, uint8_t evidId, uint32_t evidStu)
{
    Ble_updateEvidence_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_UPDATE_EVIDENCE);
    ack.operationType = opeType;
    ack.evidenceId = evidId;
    ack.status = evidStu;
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 修改凭证状态
 *
 * @param [in] result NB处理结果
 * @param [in] evidId 凭证ID
 * @param [in] startTime 凭证有效期起始时间
 * @param [in] endTime 凭证有效期结束时间
 *
 * @note
 */
static void FuncBle_lkm3_modifyEvidence_Handle(uint8_t result, uint8_t evidId, uint32_t startTime, uint32_t endTime)
{
    Ble_addEvidence_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));
    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_MODIFY_EVIDENCE);
    ack.evidenceId = evidId;
    ack.effectiveTime = startTime;
    ack.failureTime = endTime;
    FUNC_BLE_LOG_D("Ble modify evidence ack:%x", ack.massageType);
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 删除凭证信息
 *
 *
 * @note
 */
static void FuncBle_lkm3_delEvidence_Handle(uint8_t result, uint8_t opeType, uint8_t evidId)
{
    Ble_delEvidence_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_DEL_EVIDENCE);
    ack.operationType = opeType;
    ack.evidenceId = evidId;
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}
/**
 * @brief 添加凭证信息
 *
 *
 * @note
 */
static void FuncBle_lkm3_addEvidence_Handle(uint8_t result, uint8_t evidId, uint32_t startTime, uint32_t endTime)
{
    Ble_addEvidence_ack_t ack = {0};

    (result == EVIDENCE_ACK_SUCCESS) ? (ack.result = OSAL_SWAP16(CMD_GENERAL_SUCCESS)) : (ack.result = OSAL_SWAP16(CMD_GENERAL_ERROR));

    ack.massageType = OSAL_SWAP16(BLE_LKM3_ACK_ADD_EVIDENCE);
    ack.evidenceId = evidId;
    ack.effectiveTime = startTime;
    ack.failureTime = endTime;
    BleRevMain_send_Cmd((uint8_t *)&ack, sizeof(ack));
}

/**
 * @brief 门锁激活
 *
 *
 * @note
 */
static uint8_t FuncBle_lkm3_activate_Handle(uint16_t cmd)
{
    FUNC_BLE_LOG_I("Ble send activation SUCCESS:%x", cmd);
    Ble_active_ack_t activeAck = {0};

    if (cmd == 0xee18)
    {
        //在激活过程中，门锁蓝牙需保持一分钟不休眠，不断开蓝牙连接
        bleActiveFlag = RESET;
        OSAL_TimerDelete(bleActiveTimer);
        bleActiveTimer = NULL;
    }
    activeAck.massageType = 0x8a;
    activeAck.result = OSAL_SWAP16(cmd);

    BleRevMain_send_Cmd(&activeAck, sizeof(activeAck));
    return 0;
}

/**
 * @brief 自检处理
 *
 *
 * @note
 */
static void FuncBle_lkm3_selfCheck_Handle(uint8_t NbRssi)
{
    Ble_selfCheck_ack_t selfCheckAck = {0};
    uint32_t firmwareVersion = BLE_FIRMWARE_VER;
    uint8_t i = 0;
    selfCheckAck.massageType = 0x7A;
    selfCheckAck.result = 0x00;
    selfCheckAck.firmwareVersion[0] = (firmwareVersion >> 16) & 0xff;
    selfCheckAck.firmwareVersion[1] = (firmwareVersion >> 8) & 0xff;
    selfCheckAck.firmwareVersion[2] = firmwareVersion & 0xff;

    OSAL_NvRead(SYS_OFFSET(nbIemi), selfCheckAck.IMEI, sizeof(selfCheckAck.IMEI));
    FUNC_BLE_LOG_I("Ble GET iemi:%.*s", sizeof(selfCheckAck.IMEI), selfCheckAck.IMEI);

    for (i = 0; i < sizeof(selfCheckAck.IMEI); i++)
    {
        selfCheckAck.IMEI[i] -= '0';
    }
    selfCheckAck.deviceType = OSAL_SWAP16(0x0001);
    selfCheckAck.NbRssi = NbRssi;
    selfCheckAck.battery = SYS_CALL(Battery_Get);
    selfCheckAck.reserve = 0;

    BleRevMain_send_Cmd(&selfCheckAck, sizeof(selfCheckAck));
}
/**
 * @brief 接收安居维棠APP发送的数据处理
 *
 * @param [in] len
 * @param [in] pdata
 *
 * @note
 */
static uint8_t BleRevMain_ReceiveBleCmd(uint8_t len, uint8_t *pData)
{
    uint8_t res = 0;
    uint32_t cmd = 0;

    cmd = pData[0];
    cmd = (cmd << 8) | pData[1];
    cmd = (cmd << 8) | pData[2];
    FUNC_BLE_LOG_I("cmd:0x%x , len:%d", cmd, len);
    FUNC_BLE_LOG_HEX("BleRevMain data:", pData, len);

    if (cmd == BLE_LKM3_CMD_ACTIVATE)
    {
        SYS_CALL(NbMain_send_readData_cmd, 1);
    }
    else
    {
        SYS_CALL(NbMain_send_readData_cmd, 0);
    }

    switch (cmd)
    {
    case BLE_LKM3_CMD_SELFCHECK:
    {
        SYS_CALL(NbMain_get_CSQ, FuncBle_lkm3_selfCheck_Handle);
    }
    break;

    case BLE_LKM3_CMD_ACTIVATE:
    {
        bleActiveFlag = SET;
        FuncBle_lkm3_activate_Handle(CMD_AVTICE_RET_CONNECT_NET_SUCCESS);
        if (bleActiveTimer == NULL)
        {
            bleActiveTimer = OSAL_TimerCreate(bleUpdateAdvTimerCallback, ACTIVATE_TIMER_TIMEOUT_MS, RESET);
        }
    }
    break;

    case BLE_LKM3_CMD_ADD_EVIDENCE:
    {
        // FuncBle_lkm3_addEvidence_Handle(pData);
    }
    break;

    case BLE_LKM3_CMD_DEL_EVIDENCE:
    {
        // FuncBle_lkm3_delEvidence_Handle(pData);
    }
    break;

    case BLE_LKM3_CMD_MODIFY_EVIDENCE:
    {
        // FuncBle_lkm3_modifyEvidence_Handle(pData);
    }
    break;

    case BLE_LKM3_CMD_UPDATE_EVIDENCE:
    {
        // FuncBle_lkm3_updateEvidence_Handle(pData);
    }
    break;

    case BLE_LKM3_CMD_TIME_SET:
    {
        // FuncBle_lkm3_timeSet_Handle(pData);
    }
    break;

    case BLE_LKM3_CMD_HEART_BEAT_SET:
    {
        // FuncBle_lkm3_heardBeatSet_Handle(pData);
    }
    break;

    case BLE_LKM3_CMD_INQUIRY_VERSION:
    {
        // FuncBle_lkm3_version_Handle();
    }
    break;

    case BLE_LKM3_CMD_SET_RESTERT_FRE:
    {
        // FuncBle_lkm3_restertFre_Handle(pData);
    }
    break;

    case BLE_LKM3_CMD_CLEAR_EVIDENCE:
    case BLE_LKM3_CMD_RESET_FACTORY:
    case BLE_LKM3_CMD_RESTART_DEVICE:
    case BLE_LKM3_CMD_INSTALL_INIT:
    case BLE_LKM3_CMD_INSTALL_OK:

    {
        // FuncBle_lkm3_general_ack_Handle(cmd);
    }
    break;

    case BLE_LKM3_CMD_INQUIRY_CONFIG:
    {
        // FuncBle_lkm3_inquiryConfig_Handle();
    }
    break;

    case BLE_LKM3_CMD_SET_LOCK_MODE:
    {
        FuncBle_lkm3_setLockMode_Handle(pData);
    }
    break;

    case BLE_LKM3_CMD_GET_EVIDENCE_LIST:
    {
        // FuncBle_lkm3_getEvidenceList_Handle(pData);
    }
    break;

    default: //转发NB的包
    {
        memcpy(bleSendToNbData, pData, len);
        bleSendToNbDataLen = len;
        if (bleSendToNbDataTimer == NULL)
        {
            bleSendToNbDataTimer = OSAL_TimerCreate(bleSendToNbDataCallback, 500, RESET);
        }
    }
    break;
    }

    return res;
}

/**
 * @brief 获取MAC地址
 *
 * @param [out] msg
 * @param [in] len
 *
 * @note
 */
static uint8_t RevMain_get_mac_address(uint8_t *msg, uint8_t len)
{
    if (msg == NULL || len != 6)
    {
        FUNC_BLE_LOG_W("Get mac address ERROR");
        return 0;
    }

    OSAL_NvRead(SYS_OFFSET(bleMacAddr), msg, len);

    return 0;
}
/**
 * @brief  BLE组件消息回调
 * @note
 * @param  *msg 消息指针
 * @param  len 消息长度
 * @return void
 */
static void RevMainBleCb(void *msg, uint16_t len)
{
    BlePublishMsg_t *ble = (BlePublishMsg_t *)msg;
    uint8_t bleMac[6];
    FUNC_BLE_LOG_D("RevMainBleCb type:%d \r\n", ble->type);
    switch (ble->type)
    {
    case PUBLISH_APP_CMD:
        SYS_CALL(Set_BleConnectStatust, 1);
        BleRevMain_ReceiveBleCmd(ble->len, ble->pData);
        break;

    case PUBLISH_BLE_MAC:
        OSAL_NvRead(SYS_OFFSET(bleMacAddr), bleMac, 6);
        if (memcmp(bleMac, ble->pData, 6) != 0) //不同才写入
        {
            OSAL_NvWrite(SYS_OFFSET(bleMacAddr), ble->pData, 6);
        }
        FUNC_BLE_LOG_HEX("mac address:", ble->pData, 6);

        break;

    case PUBLISH_BLE_STATUS:
        if (ble->pData[0])
        {
            // SYS_CALL(Set_BleConnectStatust, 1);
            FUNC_BLE_LOG_D("ble status is connected\r\n");
            bleConnectSta = 1;
            Led_Set("LED_LOGO_W", LED_ON, 0, 0);
        }
        else
        {
            SYS_CALL(Set_BleConnectStatust, 0);
            bleConnectSta = 0;
            FUNC_BLE_LOG_D("ble status is disconnect\r\n");
            Led_Set("LED_LOGO_W", LED_OFF, 0, 0);
        }
        break;

    case PUBLISH_HEART_BEAT:
        // 刷新定时器
        FUNC_BLE_LOG_D("Heartbeat event");
        break;

    default:
        break;
    }

    OSAL_UpdateSleepTime(15000, 1);
}
/**
 * @brief BLE休眠回调函数
 * @note
 * @param
 * @retval ErrorStatus
 */
static ErrorStatus Ble_sleep_cb(void)
{
    FUNC_BLE_LOG_I("ble_main sleep\r\n");
    if (bleActiveTimer != NULL) //激活模式1分钟不休眠
    {
        return ERROR;
    }
    // else if (bleConnectSta == 1) //还在连接模式先断开连接
    // {
    //     Ble_Setconnect(0);
    //     return ERROR;
    // }
    else
    {
        return SUCCESS;
    }
}
/**
 * @brief  BLE APP启动函数
 * @note
 *
 * @param  wake_id：唤醒ID（组件ID），0XFF表示上电唤醒
 * @param  wake_param：唤醒参数
 */
static void BLE_Main(uint32_t wake_id, uint32_t wake_param)
{
    FUNC_BLE_LOG_I("ble_main wake_id:%d, wake_param:%d\r\n", wake_id, wake_param);
    //上电唤醒：创建APP，订阅消息
    AppHandle_t app_handle = OSAL_AppCreate("BLE_Main", Ble_sleep_cb);
    OSAL_MessageSubscribe(app_handle, COMP_BLE, RevMainBleCb);
    // SYS_API(RevMain_get_mac_address);
    SYS_API(FuncBle_lkm3_activate_Handle);
    SYS_API(FuncBle_lkm3_unLockSta_Handle);
    SYS_API(FuncBle_lkm3_modifyEvidence_Handle);
    SYS_API(FuncBle_lkm3_updateEvidence_Handle);
    SYS_API(FuncBle_lkm3_addEvidence_Handle);
    SYS_API(FuncBle_lkm3_delEvidence_Handle);
    SYS_API(BleMain_get_activeStatus);
    SYS_API(FuncBle_lkm3_general_ack_Handle);
    SYS_API(FuncBle_lkm3_timeSet_Handle);
    SYS_API(FuncBle_lkm3_heardBeatSet_Handle);
    SYS_API(FuncBle_lkm3_version_Handle);
    SYS_API(FuncBle_lkm3_restertFre_Handle);
    SYS_API(FuncBle_lkm3_inquiryConfig_Handle);
    SYS_API(FuncBle_lkm3_getEvidenceList_Handle);
}
APP_INIT_EXPORT(BLE_Main);
