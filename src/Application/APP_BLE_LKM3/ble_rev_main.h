/**
 * @file ble_rev_main.h
 * @brief
 *
 * @version 0.1
 * @date 2023-09-05
 *
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 *
 * @note          鼎新同创・智能锁
 *
 * @par 修改日志:
 * <1> 2023-09-05 v0.1 tianshidong 创建初始版本
 * *************************************************************************
 */

#ifndef _BLE_REV_MAIN_H_
#define _BLE_REV_MAIN_H_
#include "component.h"

//=============================================================================
//设备信息宏定义
//=============================================================================
#define BLE_MAX_PAYLOAD_LEN 32 // payload最大长度


#define ACTIVATE_TIMER_TIMEOUT_MS (60000) //激活时，60s不休眠
//=============================================================================
//设备信息宏定义
//=============================================================================
#define BLE_LKM3_CMD_SELFCHECK 0x8A7A8A         //门锁自检
#define BLE_LKM3_CMD_ACTIVATE 0x7A8A7A          //门锁激活
#define BLE_LKM3_CMD_ADD_EVIDENCE 0x7ABA03      //添加凭证信息
#define BLE_LKM3_CMD_DEL_EVIDENCE 0x7ABA04      //删除凭证信息
#define BLE_LKM3_CMD_MODIFY_EVIDENCE 0x7ABA05   //修改凭证信息
#define BLE_LKM3_CMD_UPDATE_EVIDENCE 0x7ABA06   //更新凭证状态
#define BLE_LKM3_CMD_CLEAR_EVIDENCE 0x7ABA07    //清空数据
#define BLE_LKM3_CMD_RESET_FACTORY 0x7ABA08     //恢复出厂设置
#define BLE_LKM3_CMD_RESTART_DEVICE 0x7ABA09    //重启设备
#define BLE_LKM3_CMD_TIME_SET 0x7ABA0A          //设置时间
#define BLE_LKM3_CMD_HEART_BEAT_SET 0x7ABA0D    //上报心跳周期
#define BLE_LKM3_CMD_INQUIRY_VERSION 0x7ABA0F   //查询版本信息
#define BLE_LKM3_CMD_SET_RESTERT_FRE 0x7ABA1E   //设置重启频次
#define BLE_LKM3_CMD_INQUIRY_CONFIG 0x7ABA20    //查询配置信息
#define BLE_LKM3_CMD_SET_LOCK_MODE 0x7ABA21     //设置门锁运行模式
#define BLE_LKM3_CMD_GET_EVIDENCE_LIST 0x7ABA22 //获取凭证列表
#define BLE_LKM3_CMD_INSTALL_INIT 0x7ABA23      //安装门锁初始化
#define BLE_LKM3_CMD_INSTALL_OK 0x7ABA24        //安装门锁完成

#define BLE_LKM3_ACK_ADD_EVIDENCE 0xBA03      //添加凭证信息
#define BLE_LKM3_ACK_DEL_EVIDENCE 0xBA04      //删除凭证信息
#define BLE_LKM3_ACK_MODIFY_EVIDENCE 0xBA05   //修改凭证信息
#define BLE_LKM3_ACK_UPDATE_EVIDENCE 0xBA06   //更新凭证状态
#define BLE_LKM3_ACK_CLEAR_EVIDENCE 0xBA07    //清空数据
#define BLE_LKM3_ACK_RESET_FACTORY 0xBA08     //恢复出厂设置
#define BLE_LKM3_ACK_RESTART_DEVICE 0xBA09    //重启设备
#define BLE_LKM3_ACK_TIME_SET 0xBA0A          //设置时间
#define BLE_LKM3_ACK_HEART_BEAT_SET 0xBA0D    //上报心跳周期
#define BLE_LKM3_ACK_INQUIRY_VERSION 0xBA0F   //查询版本信息
#define BLE_LKM3_ACK_SET_RESTERT_FRE 0xBA1E   //设置重启频次
#define BLE_LKM3_ACK_LOCK_STATUS 0x9A         //上报开门结果信息
#define BLE_LKM3_ACK_INQUIRY_CONFIG 0xBA20    //查询配置信息
#define BLE_LKM3_ACK_SET_LOCK_MODE 0xBA21     //设置门锁运行模式
#define BLE_LKM3_ACK_GET_EVIDENCE_LIST 0xBA22 //获取凭证列表
#define BLE_LKM3_ACK_INSTALL_INIT 0xBA23      //安装门锁初始化
#define BLE_LKM3_ACK_INSTALL_OK 0xBA24        //安装门锁完成

/**************激活命令返回值**************/
#define CMD_GENERAL_SUCCESS 0xEE08 //成功
#define CMD_GENERAL_ERROR 0xEE06   //失败
#define CMD_EVIDENCE_RFEEZE 0xEE0A   //凭证被冻结     
#define CMD_EVIDENCE_EXPIRE 0xEE0B   //凭证已过期
#define CMD_EVIDENCE_NOTYET 0xEE0C   //凭证有效期起始时间未到
#define CMD_CMD_FAILURE     0xEE0D   //指令失效时间
#define CMD_EVIDENCE_ERROR       0xEE0E   //凭证错误
#define CMD_EVIDENCE_NOEXiST     0xEE0F   //凭证不存在

#define CMD_AVTICE_RET_CONNECT_NET_SUCCESS 0xEE08  //连接天翼云成功
#define CMD_AVTICE_RET_FAILED_SELFCHECK 0xEE07     //未通过自检
#define CMD_AVTICE_RET_TIME_ERROR 0xEE09           //时间错误
#define CMD_AVTICE_RET_ADD_EVIDENCE_SUCCESS 0xEE18 //添加凭证成功
#define CMD_AVTICE_RET_ADD_EVIDENCE_ERROR 0xEE19   //添加凭证错误
//=============================================================================
//命令数据结构类型
//=============================================================================
#pragma pack(1)

typedef struct
{
    uint8_t massageType;        //消息类型
    uint16_t result;            //结果
    uint8_t firmwareVersion[3]; //固件版本号
    uint8_t IMEI[15];           //设备IMEI码
    uint16_t deviceType;        //设备型号
    uint8_t NbRssi;             // NB信号
    uint8_t battery;            //门锁电量
    uint8_t reserve;            //保留
} Ble_selfCheck_ack_t;

typedef struct
{
    uint8_t massageType; //消息类型
    uint16_t result;     //结果
} Ble_active_ack_t;

typedef struct
{
    uint8_t cmd[3];         //命令
    uint8_t evidenceId;     //凭证id
    uint32_t effectiveTime; //生效时间
    uint32_t failureTime;   //失效时间
} Ble_addEvidence_cmd_t, Ble_modifyEvidence_cmd_t;

typedef struct
{
    uint16_t massageType;   //消息类型
    uint16_t result;        //结果
    uint8_t evidenceId;     //凭证id
    uint32_t effectiveTime; //生效时间
    uint32_t failureTime;   //失效时间
} Ble_addEvidence_ack_t, Ble_modifyEvidence_ack_t;

typedef struct
{
    uint8_t cmd[3];        //命令
    uint8_t operationType; //操作类型
    uint8_t evidenceId;    //凭证id
} Ble_delEvidence_cmd_t;

typedef struct
{
    uint16_t massageType;  //消息类型
    uint16_t result;       //结果
    uint8_t operationType; //操作类型
    uint8_t evidenceId;    //凭证id
} Ble_delEvidence_ack_t;

typedef struct
{
    uint8_t cmd[3];        //命令
    uint8_t operationType; //操作类型
    uint8_t evidenceId;    //凭证id
    uint8_t status;        //状态
} Ble_updateEvidence_cmd_t;

typedef struct
{
    uint16_t massageType;  //消息类型
    uint16_t result;       //结果
    uint8_t operationType; //操作类型
    uint8_t evidenceId;    //凭证id
    uint8_t status;        //状态
} Ble_updateEvidence_ack_t;

typedef struct
{
    uint16_t massageType; //消息类型
    uint16_t result;      //结果
} Ble_general_ack_t;

typedef struct
{
    uint8_t cmd[3]; //命令
    uint32_t time;  //时间
} Ble_timeSet_cmd_t;

typedef struct
{
    uint16_t massageType; //消息类型
    uint16_t result;      //结果
    uint32_t time;        //时间
} Ble_timeSet_ack_t;

typedef struct
{
    uint8_t cmd[3]; //命令
    uint8_t beat;   //心跳周期
} Ble_heardBeatSet_cmd_t;

typedef struct
{
    uint16_t massageType; //消息类型
    uint16_t result;      //结果
    uint8_t beat;         //心跳周期
} Ble_heardBeatSet_ack_t;

typedef struct
{
    uint16_t massageType; //消息类型
    uint16_t result;      //结果
    uint16_t sw;          //软件版本
    uint8_t hw[4];        //硬件版本
    uint8_t NbId[8];      //通讯模组 ID
    uint8_t BleId[8];     //蓝牙模块 ID
} Ble_version_ack_t;

typedef struct
{
    uint8_t cmd[3]; //命令
    uint8_t fre;    //重启频次
} Ble_restertFre_cmd_t;

typedef struct
{
    uint16_t massageType; //消息类型
    uint16_t result;      //结果
    uint8_t fre;          //重启频次
} Ble_restertFre_ack_t;

typedef struct
{
    uint8_t massageType; //消息类型
    uint16_t result;     //结果
    uint8_t evidenceId;  //凭证id
    uint8_t batLev;      //电量
    uint32_t time;       //时间
} Ble_lockStatus_ack_t;

typedef struct
{
    uint16_t massageType;     //消息类型
    uint16_t result;          //结果
    uint8_t southAddress[16]; //南向地址
    uint8_t port[4];          //端口号
    uint8_t lockMode;         //门锁运行模式
} Ble_inquiryConfig_ack_t;

typedef struct
{
    uint8_t cmd[3];   //命令
    uint8_t lockMode; //门锁运行模式
} Ble_setLockMode_cmd_t;

typedef struct
{
    uint16_t massageType; //消息类型
    uint16_t result;      //结果
    uint8_t lockMode;     //门锁运行模式
} Ble_setLockMode_ack_t;

typedef struct
{
    uint8_t cmd[3];       //命令
    uint8_t evidenceType; //凭证类型
} Ble_getEvidenceList_cmd_t;

typedef struct
{
    uint16_t massageType; //消息类型
    uint16_t result;      //结果
    uint8_t evidenceType; //凭证类型
    uint8_t evidenceNum;  //凭证数量
    uint8_t idList[];
} Ble_getEvidenceList_ack_t;

typedef struct
{
    uint16_t massageType; //消息类型
    uint16_t result;      //结果
} Ble_genaral_ack_t;

#pragma pack()

static uint8_t RevMain_get_mac_address(uint8_t *msg, uint8_t len);
static uint8_t FuncBle_lkm3_activate_Handle(uint16_t cmd);
static void FuncBle_lkm3_unLockSta_Handle(uint8_t result, uint8_t unLockId, uint32_t timeStamp);
static void FuncBle_lkm3_modifyEvidence_Handle(uint8_t result, uint8_t evidId, uint32_t startTime, uint32_t endTime);
static uint8_t BleMain_get_activeStatus(void);
static void FuncBle_lkm3_updateEvidence_Handle(uint8_t result, uint8_t opeType, uint8_t evidId, uint32_t evidStu);
static void FuncBle_lkm3_addEvidence_Handle(uint8_t result, uint8_t evidId, uint32_t startTime, uint32_t endTime);
static void FuncBle_lkm3_delEvidence_Handle(uint8_t result, uint8_t opeType, uint8_t evidId);
static void FuncBle_lkm3_general_ack_Handle(uint32_t cmd, uint8_t result);
static void FuncBle_lkm3_timeSet_Handle(uint8_t result, uint32_t timeStamps);
static void FuncBle_lkm3_heardBeatSet_Handle(uint8_t result, uint8_t beat);
static void FuncBle_lkm3_version_Handle(uint8_t result, uint16_t softVer, uint8_t *hardVer, uint8_t *nbId, uint8_t *bleId);
static void FuncBle_lkm3_restertFre_Handle(uint8_t result, uint8_t restartFre);
static void FuncBle_lkm3_inquiryConfig_Handle(uint8_t result, uint8_t *southAddr, uint8_t *port, uint8_t lockMode);
static void FuncBle_lkm3_getEvidenceList_Handle(uint8_t result, uint8_t type, uint8_t evidNum, uint8_t *evidId);
#endif
