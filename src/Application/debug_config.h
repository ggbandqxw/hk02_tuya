#ifndef __DEBUG_CONFIG_H__
#define __DEBUG_CONFIG_H__

#include "co_debug.h"
#include "osal.h"
/* 日志打印开关设置 */
#define ALL_DEBUG_EN                1   //日志打印总开关

#define BLE_LOG_DEBUG               1
#define BLE_TASK_LOG_DEBUG          1
#define BLE_FUNC_LOG_DEBUG          1

#define NB_LOG_DEBUG               1
#define NB_TASK_LOG_DEBUG          1
#define NB_FUNC_LOG_DEBUG          1

#define FINGER_TASK_LOG_DEBUG          1

#define LOCKDAI_LOG_DEBUG          1

/* osal任务打印 */
#define TASK_LOG(format, ...)       log_debug(format"\r\n",##__VA_ARGS__)

#if ALL_DEBUG_EN
/* BLE驱动打印 */
#if BLE_LOG_DEBUG
#define BLE_TAG "[BLE_DRV] "
#define BLE_LOG_D(format, ...)  log_debug(C_BLUE BLE_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define BLE_LOG_I(format, ...)  log_info(C_GREEN BLE_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define BLE_LOG_W(format, ...)  log_warn(C_YELLOW BLE_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define BLE_LOG_E(format, ...)  log_error(C_RED BLE_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define BLE_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)
#else
#define BLE_LOG_D(format, ...)
#define BLE_LOG_I(format, ...)
#define BLE_LOG_W(format, ...)
#define BLE_LOG_E(format, ...)
#define BLE_LOG_HEX(note, array, len)
#endif

/* BLE task打印 */
#if BLE_TASK_LOG_DEBUG
#define BLE_TASK_TAG "[BLE_TASK] "
#define BLE_TASK_LOG_D(format, ...)  log_debug(C_BLUE BLE_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define BLE_TASK_LOG_I(format, ...)  log_info(C_GREEN BLE_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define BLE_TASK_LOG_W(format, ...)  log_warn(C_YELLOW BLE_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define BLE_TASK_LOG_E(format, ...)  log_error(C_RED BLE_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define BLE_TASK_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)
#else
#define BLE_TASK_LOG_D(format, ...)
#define BLE_TASK_LOG_I(format, ...)
#define BLE_TASK_LOG_W(format, ...)
#define BLE_TASK_LOG_E(format, ...)
#define BLE_TASK_LOG_HEX(note, array, len)
#endif

/* BLE 应用层打印 */
#if BLE_FUNC_LOG_DEBUG 
#define FUNC_BLE_TAG "[BLE_MAIN]"
#define FUNC_BLE_LOG_D(format, ...)  log_debug(C_BLUE FUNC_BLE_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FUNC_BLE_LOG_I(format, ...)  log_info(C_GREEN FUNC_BLE_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FUNC_BLE_LOG_W(format, ...)  log_warn(C_YELLOW FUNC_BLE_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FUNC_BLE_LOG_E(format, ...)  log_error(C_RED FUNC_BLE_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FUNC_BLE_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)
#else
#define FUNC_BLE_LOG_D(format, ...)
#define FUNC_BLE_LOG_I(format, ...)
#define FUNC_BLE_LOG_W(format, ...)
#define FUNC_BLE_LOG_E(format, ...)
#define FUNC_BLE_LOG_HEX(note, array, len)
#endif

/* BLE驱动打印 */
#if NB_LOG_DEBUG
#define NB_TAG "[NB_DRV] "
#define NB_LOG_D(format, ...)  log_debug(C_BLUE NB_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define NB_LOG_I(format, ...)  log_info(C_GREEN NB_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define NB_LOG_W(format, ...)  log_warn(C_YELLOW NB_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define NB_LOG_E(format, ...)  log_error(C_RED NB_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define NB_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)
#else
#define NB_LOG_D(format, ...)
#define NB_LOG_I(format, ...)
#define NB_LOG_W(format, ...)
#define NB_LOG_E(format, ...)
#define NB_LOG_HEX(note, array, len)
#endif

/* NB task打印 */
#if NB_TASK_LOG_DEBUG
#define NB_TASK_TAG "[NB_TASK] "
#define NB_TASK_LOG_D(format, ...)  log_debug(C_BLUE NB_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define NB_TASK_LOG_I(format, ...)  log_info(C_GREEN NB_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define NB_TASK_LOG_W(format, ...)  log_warn(C_YELLOW NB_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define NB_TASK_LOG_E(format, ...)  log_error(C_RED NB_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define NB_TASK_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)
#else
#define NB_TASK_LOG_D(format, ...)
#define NB_TASK_LOG_I(format, ...)
#define NB_TASK_LOG_W(format, ...)
#define NB_TASK_LOG_E(format, ...)
#define NB_TASK_LOG_HEX(note, array, len)
#endif

/* NB 应用层打印 */
#if NB_FUNC_LOG_DEBUG 
#define FUNC_NB_TAG "[NB_MAIN]"
#define FUNC_NB_LOG_D(format, ...)  log_debug(C_BLUE FUNC_NB_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FUNC_NB_LOG_I(format, ...)  log_info(C_GREEN FUNC_NB_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FUNC_NB_LOG_W(format, ...)  log_warn(C_YELLOW FUNC_NB_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FUNC_NB_LOG_E(format, ...)  log_error(C_RED FUNC_NB_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FUNC_NB_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)
#else
#define FUNC_NB_LOG_D(format, ...)
#define FUNC_NB_LOG_I(format, ...)
#define FUNC_NB_LOG_W(format, ...)
#define FUNC_NB_LOG_E(format, ...)
#define FUNC_NB_LOG_HEX(note, array, len)
#endif

/* BLE task打印 */
#if FINGER_TASK_LOG_DEBUG
#define FINGER_TASK_TAG "[FINGER_TASK] "
#define FINGER_TASK_LOG_D(format, ...)  log_debug(C_BLUE FINGER_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FINGER_TASK_LOG_I(format, ...)  log_info(C_GREEN FINGER_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FINGER_TASK_LOG_W(format, ...)  log_warn(C_YELLOW FINGER_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FINGER_TASK_LOG_E(format, ...)  log_error(C_RED FINGER_TASK_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define FINGER_TASK_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)
#else
#define FINGER_TASK_LOG_D(format, ...)
#define FINGER_TASK_LOG_I(format, ...)
#define FINGER_TASK_LOG_W(format, ...)
#define FINGER_TASK_LOG_E(format, ...)
#define FINGER_TASK_LOG_HEX(note, array, len)
#endif

/* LOCKDAI 打印 */
#if LOCKDAI_LOG_DEBUG
#define LOCKDAI_TAG "[LOCKDAI] "
#define LOCKDAI_LOG_V(format, ...)  //log_debug(C_LIGHT_GRAY LOCKDAI_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define LOCKDAI_LOG_D(format, ...)  log_debug(C_BLUE LOCKDAI_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define LOCKDAI_LOG_I(format, ...)  log_info(C_GREEN LOCKDAI_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define LOCKDAI_LOG_W(format, ...)  log_warn(C_YELLOW LOCKDAI_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define LOCKDAI_LOG_E(format, ...)  log_error(C_RED LOCKDAI_TAG  format C_NONE"\r\n", ##__VA_ARGS__)
#define LOCKDAI_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)
#else
#define LOCKDAI_LOG_D(format, ...)
#define LOCKDAI_LOG_I(format, ...)
#define LOCKDAI_LOG_W(format, ...)
#define LOCKDAI_LOG_E(format, ...)
#define LOCKDAI_LOG_HEX(note, array, len)
#endif

#endif/* ALL_DEBUG_EN */
#endif/*__DEBUG_CONFIG_H__*/
