#ifndef _LOCAL_KDS_MAIN_H
#define _LOCAL_KDS_MAIN_H
#include "component.h"

/* 锁体 */
#define LOCK_TYPE_524                  1 //524锁体
#define LOCK_TYPE_824                  2 //824锁体
#define LOCK_TYPE_BW                   3 //霸王锁体
#define LOCK_TYPE_DAI                  4 //呆锁

/* 手自动模式 */
#define AUTO_MODE                      0
#define MANUAL_MODE                    1

#ifndef DEFAULT_AUTO_LOCK_TIME
#define DEFAULT_AUTO_LOCK_TIME         10 //默认自动上锁时间 单位：s
#endif

#if defined(LOCK_524)
	#define APP_LOCK_TYPE LOCK_TYPE_524
	#define DEFAULT_OPEN_LOCK_MODE AUTO_MODE
#elif defined(LOCK_824)
	#define APP_LOCK_TYPE LOCK_TYPE_824
	#define DEFAULT_OPEN_LOCK_MODE AUTO_MODE
#elif defined(LOCK_BW)
	#define APP_LOCK_TYPE LOCK_TYPE_BW
	#define DEFAULT_OPEN_LOCK_MODE MANUAL_MODE
#elif defined(LOCK_DAI)
	#define APP_LOCK_TYPE LOCK_TYPE_DAI
	#define DEFAULT_OPEN_LOCK_MODE AUTO_MODE
#else
	#error Please define lock type first.
#endif

/* 默认语言 */
#if defined(DEFAULT_LANG_EN)
	#define DEFAULT_LANGUAGE LANGUAGE_ENGLISH
#elif defined(DEFAULT_LANG_KK)
	#define DEFAULT_LANGUAGE LANGUAGE_KAZAK
#else
	#define DEFAULT_LANGUAGE LANGUAGE_ENGLISH 
#endif

#define DEFAULT_DSENSOR_SENSITIVE      1                    //距离传感器默认灵敏度高  1高 2中 3低 4关闭  
#define DEFAULT_TOUCH_HANDLE_FUNC      1                    //默认感应把手功能打开  0关闭   
#define DEFAULT_OPEN_DOOR_POWER        1                    //默认开锁力量 1低扭力（无天地钩） 2高扭力（带天地钩）
#define DEFAULT_FACE_FUNC              1                    //默认人脸识别功能 0关闭    1打开
#define DEFAULT_VIDEO_FUNC             1                    //默认视频功能开关  1 打开视频功能   0关闭视频功能
#define DEFAULT_HOVER_ALARM_FUNC       0                    //默认徘徊报警功能  0关闭   1打开
#define DEFAULT_HOVER_ALARM_LEVEL      1                    //默认徘徊报警灵敏度  0低 1中 2高


/* 存储在EE的系统标志 */
#define SYS_FLAG_TAMPER_ALARM      (1<<0)//防撬报警标志
#define SYS_FLAG_ARMING_ALARM      (1<<1)//布防报警标志
#define SYS_FLAG_ARMING_STATUS     (1<<2)//布防状态
#define SYS_FLAG_ANTI_LOCK_STATUS  (1<<3)//反锁状态
#define SYS_FLAG_AGING_STATUS      (1<<4)//老化状态
#define SYS_FLAG_MECHKEY_ALARM     (1<<5)//机械钥匙报警标志
#define SYS_FLAG_AUTO_MODE         (1<<6)//自动模式

#pragma pack(1)

typedef struct
{
    uint8_t cmd;
    uint16_t len;
    uint8_t ack;
    uint8_t data[];
} ProductAckType_stu_t;

typedef struct
{
    uint8_t cmd;
    uint16_t len;
    uint8_t data[];
} ProductCmdType_stu_t;


#if defined(WIFI_SUB_DEVICE) || defined(EXTEND_ITIC)
typedef enum
{
	OTA_STATUS_NONE,
	OTA_STATUS_DOWNLOADING,
	OTA_STATUS_UPGRADING,
	OTA_STATUS_FINISHED,
} OTAStatus_enum_t;

typedef struct
{
	OTAStatus_enum_t status;      //OTA状态
	int otaResultCode;            //OTA结果码
	int32_t otaTimestamp;         //OTA时间
	uint8_t devNum;               //设备编号
	int32_t fileExpiesTimestamp;  //文件过期时间
	int fileLen;                  //文件长度
	char url[200];                //固件下载链接
	char md5[33];                 //固件md5
	char oldVersion[20];          //老固件版本
} OTAInfo_stu_t;
#endif

/* 系统参数 */
typedef struct
{
	uint8_t  nvInitFlag;                // 初始化标志
	uint8_t  sys_flag;                  // 状态标志（bit0:防撬报警 bit1:布防报警 bit2:布防状态 bit3:反锁状态…… ）
	uint8_t blekey2[4];					// 蓝牙key2
	uint8_t auto_lock_time;		    	// 自动上锁时间
    uint8_t powerSavingMode;			// 节能模式
    uint8_t amMode;                 	//自动模式/手动模式（自动上锁/关闭自动上锁）
    uint8_t videoIsOpen;                //视频功能是否开启 默认关闭
    uint8_t faceIsOpen;                 //人脸功能是否开启 默认关闭
    uint8_t reserved0;            		//传感器灵敏度设置 默认高
    uint8_t en_handle;           		//感应把手使能
    uint8_t hoverAlarmIsEable;          //徘徊报警使能
    uint8_t hoverAlarmLevel;            //徘徊报警等级
    uint8_t touchKeyLockCnt;            //独立上锁键按下次数 //触摸上锁键按下计数值
    uint8_t  verifyErrCount;            // 验证失败的次数
    uint32_t verifyErrTime;             // 验证失败的时间
    uint32_t systemLockedTime;          // 系统锁定的时间
	uint32_t openLockTimes;             // 开锁次数
	uint32_t networkOffpwdTime;			// 智能屏的远程开锁功能, 这里保存时间戳, 防止重放攻击
	//NB和BLE所需要存储的变量
	uint8_t nbCSQ; 						//nb 信号强度
	uint8_t heartBeatReportPeriod; 		//nb 心跳上报周期
	uint8_t restartDevicePeriod;		//nb 心跳重启频次
	uint8_t nbAuthenticationCode[8];	//nb 鉴权码


    //注意新增的要回复出厂设置的变量要插到 reserve之前

	uint8_t reserve[56];				// 保留
	uint8_t lockActiveFlag;				// 激活标志位
	uint8_t nbIemi[15];					//nb IEMI
	uint8_t nbIccid[20];				//nb sim卡iccid
	uint8_t nbSouthIpAddr[16];       	//nb 南向IP地址
	uint8_t nbport[4];                  //nb 端口
	uint8_t bleMacAddr[6];				//ble 地址 

	//AEP OTA所需要存储的变量
	uint8_t aepOtaInfor[32];			//AEP OTA 信息
	uint8_t resetSilence;				//静默复位
	
    uint8_t openDirection;              //左开门 1 右开门 2   恢复出厂设置方向不变
    uint8_t torsion;                    //低扭力 1 高扭力 2   恢复出厂设置扭力不变

	/*产测数据*/
	uint8_t systemCode[32];      		// 防伪码
	uint8_t eSN[13];             		// eSN（bleSN）
	uint8_t bleKey[12];		     		// 蓝牙key
	uint8_t pcbaSn[32];			 		// 产测使用的32字节SN
	uint8_t model[20];           		// 产品型号
    uint8_t lock_type;           		// 锁类型
	uint8_t func_code;           		// 功能集
	uint8_t default_language;    		// 默认语言（恢复出厂设置后的语言）
	uint8_t en_wifi;             		// 网络功能使能
	uint8_t en_card_crypto;      		// 卡片加密使能

	/* 服务器参数 */
#if defined(WIFI_SUB_DEVICE) || defined(EXTEND_ITIC)
	char mqtt_domain[30];               // mqtt域名
	uint16_t mqtt_port;                 // mqtt端口号
	OTAInfo_stu_t ota_info;             // OTA信息
#endif
}NvSystemParam_stu_t;
#pragma pack()

#define SYS_OFFSET(member)        OSAL_OFFSET(NvSystemParam_stu_t,member)

/* 节能模式 */
typedef enum
{
    POWER_SAVING_OPEN,//开启节能模式  视频功能关闭
	POWER_SAVING_CLOSE,//关闭节能模式 视频功能打开
	POWER_SAVING_AUTO_OPEN,//低电自动开启节能模式  视频功能广播
}PowerSaving_enum_t;

/* 产测模式类型 */
typedef enum
{
	TESTMODE_NORMAL, //正常模式
	TESTMODE_TESTING,//测试模式
	TESTMODE_AGING,  //老化模式
    TESTMODE_UNKNOW  = 0XFFFF,
}TestMode_enum_t;

typedef enum
{
    LED_NAME_KEY = 1,           //键盘灯
	LED_NAME_WHITE_BAT,			//白色电量灯
    LED_NAME_RED,               //红灯
    LED_NAME_GREEN,             //绿灯
	LED_NAME_BAT,               //低电量灯
}LedNameType_enum_t;

typedef enum
{
    HOVER_LEVEL_LOW ,//徘徊等级低
    HOVER_LEVEL_MEDIUM,//徘徊等级中
    HOVER_LEVEL_HIGH,//徘徊等级高
}HoverAlarmLevel_enum_t;

#define CARD_LEN_MAX        10 //卡片ID最大长度

/* 上锁类型 */
#define LOCKED_TYPE_SLOT       0XFA // 长按插舌上锁 
#define LOCKED_TYPE_CLOSE      0XFB // 长按CLOSE上锁
#define LOCKED_TYPE_AUTO_MODE  0XFC // 切换到自动模式上锁
#define LOCKED_TYPE_AUTO       0XFD // 自动上锁


/* 验证类型 */                     
#define DUALVER_VERIFY_CLOSE       0 //关闭双重验证
#define DUALVER_VERIFY_OPEN        1 //开启双重验证
                                   
/* 手/自动模式 */                  
#define AUTO_MODE_OPEN             0 //开启自动模式
#define AUTO_MODE_CLOSE            1 //关闭自动模式



#define VERIFY_FAIL_NUM 			5	//验证失败次数

#define NV_INIT_FLAG                0xFA//NV初始化标识



/*REMOTE APP 开放的API接口*/
static int Remote_RecordReport(EventRecord_stu_t *record);//记录上报
static int Remote_DoorBellNotify(void);//门铃通知
static int Remote_FactoryReset(void);//恢复出厂设置
static int Remote_ConnectNetwork(void);//启动配网
static int Remote_SetEcoMode(PowerSaving_enum_t mode);//设置节能模式
static int Remote_SetLanguage(uint8_t setVal);//设置语言
static int Remote_SettingChangeReport(void);//设置状态上报
static int Remote_SendOtaFaceRequest(void);//发送OTA人脸请求
static int Remote_SendOtaFaceResult(uint8_t result);//OTA结果上报
static bool Local_GetSysFlag(uint8_t flag);
#endif /* _LOCAL_KDS_MAIN_H */

