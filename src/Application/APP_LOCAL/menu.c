#include "component.h"
#include "menu_data.h"
#include "local_main.h"
#include "local_lock.h"
#include "application.h"
#include "co_debug.h"

#ifndef KOS_PARAM_USERS_QTY_FINGER
#define KOS_PARAM_USERS_QTY_FINGER 10
#endif

/* 调试打印接口 */
#define MENU_LOG(format, ...) OSAL_LOG(C_LIGHT_RED format C_NONE, ##__VA_ARGS__)
#define MENU_LOG_HEX(note, array, len)   log_debug_array_ex(note, array, len)

// __EFRAM static uint8_t autoKeysNumPwd = 0;       //密码自动编号
// __EFRAM static uint8_t autoKeysNumFpt = 0;       //指纹自动编号
// __EFRAM static uint8_t autoKeysNumCard = 0;      //卡片自动编号
#define AUDIO_PLAY_PWD_NUM_START 1
#define AUDIO_PLAY_FPT_NUM_START 51
#define AUDIO_PLAY_CARD_NUM_START 101
#define AUDIO_PLAY_CARD_NUM_END 150


#define VERIFY_FAIL_NUM 5 //验证失败次数
#define LED_COUNTS 1
typedef MenuType_enum_t (*MenuFunction_fun_t)(uint8_t *, uint8_t, InputDataType_enum_t);

static MenuType_enum_t Menu_VerifyKeys(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType);
static MenuType_enum_t Menu_AddKeys(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType);
static MenuType_enum_t Menu_DelKeys(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType);
static MenuType_enum_t Menu_AddKeysInputNumber(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType);
static MenuType_enum_t Menu_OpenAutoLockSetTime(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType);
#if defined(OLED_FUNCTION)
static MenuType_enum_t Menu_UpdateSysTime(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType);
static MenuType_enum_t Menu_UpdateSysDate(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType);
static MenuType_enum_t Menu_InputThreeNumber(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType);
#endif
static void Menu_ListBoxProcess(BoxData_stu_t *const pBox, char key);

extern uint8_t get_access_code_err_flag(void);
extern void set_access_code_err_flag(uint8_t flag);
/* 菜单数据表 */
static struct
{
	MenuFunction_fun_t pFunction;
	MenuType_enum_t menu_father;
	BoxData_stu_t *box[6];
	uint16_t audio[2];
} menuDataList[MENU_MAX] =
	{
		/*             菜单功能函数                  父菜单                      菜单box表：box表第0个元素为主box（交互型box）        进入菜单提示音 */
		/* 主菜单   */ [MENU_MASTER] = {Menu_VerifyKeys, MENU_MAX, {&masterMenuBox1, &masterMenuBox2, &masterMenuBox3, &masterMenuBox4, &masterMenuBox5}, {0}},
		/* 管理模式 */[MENU_MANAGE_MODE] = {NULL, MENU_MASTER, {&manageModeBox1}, {0}},
		/* 修改密码 */[MENU_MODIFY_ADMIN] = {Menu_AddKeys, MENU_USER_SET, {&modifyAdminBox1, &modifyAdminBox2}, {QING_XIU_GAI_GUAN_LI_MI_MA, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},

		/* 用户设置 */[MENU_USER_SET] = {NULL, MENU_MANAGE_MODE, {&userSetBox1}, {0}},
		/* 普通用户设置 */[MENU_ADDUSER_SET] = {NULL, MENU_USER_SET, {&normaluserSetBox1}, {0}},//
		/* 编号输入 */[MENU_INPUT_NUMBER_ADD_KEYS] = {Menu_AddKeysInputNumber, MENU_USER_SET, {&addKeysInputNumBox1, &addKeysInputNumBox2}, {QING_SHU_RU_LIANG_WEI_BIAN_HAO, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
	
		// /* 添加密码 */[MENU_ADD_PWD] = {Menu_AddKeys, MENU_INPUT_NUMBER_ADD_KEYS, {&addPwdBox1, &addPwdBox2}, {QING_SHU_RU_XIN_MI_MA, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
		// /* 添加指纹 */[MENU_ADD_FPT] = {Menu_AddKeys, MENU_INPUT_NUMBER_ADD_KEYS, {&addFingerBox1}, {QING_AN_ZHI_WEN, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
		//   /* 添加卡片 */[MENU_ADD_CARD] = {Menu_AddKeys, MENU_INPUT_NUMBER_ADD_KEYS, {&addCardBox1}, {QING_SHUA_KA, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
			 /* 添加密码 */[MENU_ADD_PWD] = {Menu_AddKeys, MENU_ADDUSER_SET, {&addPwdBox1, &addPwdBox2}, {QING_SHU_RU_XIN_MI_MA, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
		    /* 添加指纹 */[MENU_ADD_FPT] = {Menu_AddKeys, MENU_ADDUSER_SET, {&addFingerBox1}, {QING_AN_ZHI_WEN, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}}, // addPwdBox1
			/* 添加卡片 */[MENU_ADD_CARD] = {Menu_AddKeys, MENU_ADDUSER_SET, {&addCardBox1}, {QING_SHUA_KA, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},

		/* 添加管理密码 */[MENU_ADD_ADMIN_PWD] = {Menu_AddKeys, MENU_INPUT_NUMBER_ADD_KEYS, {&addPwdBox1, &addPwdBox2}, {QING_SHU_RU_XIN_MI_MA, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
		/* 添加管理指纹 */[MENU_ADD_ADMIN_FPT] = {Menu_AddKeys, MENU_INPUT_NUMBER_ADD_KEYS, {&addFingerBox1}, {QING_AN_ZHI_WEN, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
		
		/* 删除选择 */[MENU_DEL_SELECT] = {NULL, MENU_MANAGE_MODE, {&delSelectBox1}, {0}},
		/* 删除密码 */[MENU_DEL_PWD] = {Menu_DelKeys, MENU_DEL_SELECT, {&delPwdBox1, &delPwdBox2}, {QING_SHU_RU_ZHI_WEN_HUO_MI_MA_HUO_KA_PIAN, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
		/* 删除指纹 */[MENU_DEL_FPT] = {Menu_DelKeys, MENU_DEL_SELECT, {&delFingerBox1, &delFingerBox2}, {QING_AN_ZHI_WEN, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
		/* 编号输入 */[MENU_INPUT_NUMBER_DEL_KEYS] = {Menu_DelKeys, MENU_DEL_SELECT, {&addKeysInputNumBox1, &addKeysInputNumBox2}, {QING_SHU_RU_BIAN_HAO, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},
		/* 自动上锁模式设置 */[MENU_AUTO_LOCK_SET] = {NULL, MENU_MANAGE_MODE, {&autoLockSetBox1}, {0}},
		
		/* 语音设置 */[MENU_AUDIO_SET] = {NULL, MENU_MANAGE_MODE, {&audioSetBox1}, {0}},
		///* 假期模式设置 */[MENU_VACATION_SET] = {NULL, MENU_MANAGE_MODE, {&vacationModeBox1}, {0}},
		/* 自动上锁时间设置 */[MENU_AUTO_LOCK_TIME_SET] = {Menu_OpenAutoLockSetTime, 	MENU_AUTO_LOCK_SET, {&setAutoLockTimeBox1, &setAutoLockTimeBox2}, {QING_SHU_RU_LIANG_WEI_ZI_DONG_SHANG_SUO_SHI_JIAN_YI_KAI_SUO_JIAN_JIE_SHU, FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN}},


	};
static MenuType_enum_t menuCurrent;			//当前菜单
static MenuType_enum_t menuNext;			//下一个要跳转的菜单
static MenuType_enum_t menuDelAll;			//清除全部密钥菜单
static MenuType_enum_t backupMenu;			// app添加密钥时，把当前菜单位置备份到这里
static FlagStatus appAddKeysFlag = RESET;	// app添加密钥标志
static FlagStatus appAddKeysResult = RESET; // app添加密钥的结果
static FlagStatus inputDualFlag = RESET;	//两次输入标志
//static uint8_t asteriskKeyTimes = 0;		//星键输入次数
uint8_t asteriskKeyTimes = 0;		//星键输入次数

static EventRecord_stu_t verifyRecord;		//密钥验证记录
static uint8_t inputKeysNumber;				//暂存输入的密钥编号
// static uint8_t appAddKeysAttr;              //暂存APP添加的钥匙的属性
// static NvPloy_stu_t appAddKeysPloy;         //暂存APP添加的钥匙的策略

// static uint8_t inputErrorPwdTimes;          //密码、指纹、卡片错误计数
// static uint8_t inputErrorFptTimes;
// static uint8_t inputErrorCardTimes;
static uint8_t verifyFailCount;				   //验证失败计数
__EFRAM static uint32_t verifyFailTimestamp;   //验证失败时间戳
__EFRAM static uint32_t systemLockedTimestamp; //系统锁定时间戳
extern uint8_t ble_off;
static uint8_t g_SysLanguage = DEFAULT_LANGUAGE; //全局变量系统语言   OLED任务使用
#if defined(OLED_FUNCTION)
static uint16_t g_inputThreeNumber = 0; //记录查询 暂存输入的三位记录编号
#endif
// static FlagStatus g_AddOrDeleteNetFlag =RESET;//进入添加或者删除网络菜单标志
uint8_t isMuteflag = 0; //进入管理模式前是否是静音状态

//static uint8_t shortKeyLockStudy = RESET; // 验证快捷键启动自学习 

#define PWD_ERR_TIMER_TIMEOUT_MS (300)  //时间待定
static TimerHandle_stu_t PWD_ERR_TIMEOUTTimer = NULL;

/**
 * @brief  输入数据转换成密钥编号
 *
 * @param  pData：数据指针
 * @param  len：长度
 *
 * @return 返回密钥编号，错误返回0XFF
 */
static uint8_t Menu_InputData2KeysNumber(uint8_t *pData, uint8_t len)
{
	uint16_t temp = 0;

	if (len == 2 || len == 3)
	{
		for (; len; len--)
		{
			temp = temp * 10;
			temp = temp + *pData++;
		}
		return (temp < 0XFF) ? temp : 0XFF;
	}
	return 0XFF;
}

/**
 * @brief  判断密钥库是否满了
 *
 * @param  menu：当前菜单
 *
 * @return 满了返回SUCCESS，没满返回ERROR
 */
static ErrorStatus Menu_IsKeysLibFull(MenuType_enum_t menu)
{
	if (menu == MENU_ADD_PWD)
	{
		uint8_t pwdQty = SYS_CALL(Users_GetQty, USERS_TYPE_PWD, 1); //读取密码数量
		if (pwdQty == SYS_CALL(Users_GetKeysQty, USERS_TYPE_PWD))	//密钥库已满
		{
			Audio_Play(MI_MA_KU_YI_MAN, RESET);
			Oled_PopUpDisplay(POPUPS_PwdLibFull, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return SUCCESS;
		}
	}
	else if (menu == MENU_ADD_FPT)
	{
		uint8_t fptQty = SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 1); //读取指纹数量
		MENU_LOG("\r\nfptQty:%d \r\n", fptQty);
		if (fptQty == SYS_CALL(Users_GetKeysQty, USERS_TYPE_FPT)) //指纹库已满
		{
			Audio_Play(ZHI_WEN_KU_YI_MAN, RESET);
			Oled_PopUpDisplay(POPUPS_FPLibFull, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return SUCCESS;
		}
	}
	else if (menu == MENU_ADD_CARD)
	{
		uint8_t cardQty = SYS_CALL(Users_GetQty, USERS_TYPE_CARD, 1); //读取卡片数量
		if (cardQty == SYS_CALL(Users_GetKeysQty, USERS_TYPE_CARD))	  //卡片库已满
		{
			Audio_Play(KA_PIAN_KU_YI_MAN, RESET);
			Oled_PopUpDisplay(POPUPS_CardsFull, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return SUCCESS;
		}
	}
	return ERROR;
}

/**
 * @brief  判断密钥库是否为空
 *
 * @param  menu：当前菜单
 *
 * @return 为空返回SUCCESS，非空返回ERROR
 */
static ErrorStatus Menu_IsKeysLibEmpty(MenuType_enum_t menu)
{
	if (menu == MENU_DEL_PWD || menu == MENU_DEL_ALL_PWD)
	{
		if (SYS_CALL(Users_GetQty, USERS_TYPE_PWD, 1) == 0 && SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 1) == 0 && SYS_CALL(Users_GetQty, USERS_TYPE_CARD, 1) == 0 ) //密码库为空 //
		{
			Audio_Play(MI_MA_KU_WEI_KONG, RESET);
			Oled_PopUpDisplay(POPUPS_PwdLibEmpty, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return SUCCESS;
		}
	}
	else if (menu == MENU_DEL_FPT || menu == MENU_DEL_ALL_FPT)
	{
		if (SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 1) == 0) //指纹库为空
		{
			Audio_Play(ZHI_WEN_KU_WEI_KONG, RESET);
			Oled_PopUpDisplay(POPUPS_FPLibEmpty, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return SUCCESS;
		}
	}
	else if (menu == MENU_DEL_CARD || menu == MENU_DEL_ALL_CARD)
	{
		if (SYS_CALL(Users_GetQty, USERS_TYPE_CARD, 1) == 0) //卡片库为空
		{
			Audio_Play(KA_PIAN_KU_WEI_KONG, RESET);
			Oled_PopUpDisplay(POPUPS_CardsEmpty, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return SUCCESS;
		}
	}
	return ERROR;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//   * @brief  配网回调
//   * @note
//   *
//   * @param  result：配网结果
//   */
// static void Menu_ConfigNetworkCallback(uint32_t result)
// {
// 	const MenuType_enum_t menu = menuCurrent;
// 	uint8_t *res = (uint8_t *)result;

// 	if (menu == MENU_CONNECT_NETWORK || menu == MENU_DISCONNECT_NETWORK)
// 	{
// 		// if (*res != ACK_RES_SUCCESS)
// 		// {
// 		// 	Audio_Play(SHE_ZHI_SHI_BAI, RESET);
// 		// 	if(g_AddOrDeleteNetFlag ==SET)//因为添加或者删除网络弹窗太快了，所以不让它立即弹窗成功或者失败、先弹个空窗
// 		// 	{
// 		// 		Oled_PopUpDisplay(POPUPS_NULL,POPUPS_NULL,POP_UP_WINDOW_TIME+1000);
// 		// 		g_AddOrDeleteNetFlag =RESET;
// 		// 	}
// 		// 	Oled_PopUpDisplay(POPUPS_SettingFailed,POPUPS_NULL,POP_UP_WINDOW_TIME);
// 		// }
// 		// else
// 		// {
// 		// 	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
// 		// 	if(g_AddOrDeleteNetFlag ==SET)//因为添加或者删除网络弹窗太快了，所以不让它立即弹窗成功或者失败、先弹个空窗
// 		// 	{
// 		// 		Oled_PopUpDisplay(POPUPS_NULL,POPUPS_NULL,POP_UP_WINDOW_TIME+1000);
// 		// 		g_AddOrDeleteNetFlag =RESET;
// 		// 	}
// 		// 	Oled_PopUpDisplay(POPUPS_SettingSucceed,POPUPS_NULL,POP_UP_WINDOW_TIME);
// 		// }

// 		MENU_LOG("Config network callback, res: %02X\r\n", *res);
// 		Menu_SwitchMenu( Menu_GetFatherMenu(menu) ); //返回上级菜单
// 		// OSAL_UpdateSleepTime(AUTO_SLEEP_TIME);
// 	}
// }

/**
 * @brief  加入网络
 *
 * @note   WIFI模块
 */
static void Menu_AddNetwork(void)
{
	MENU_LOG("Add Network");
	SYS_CALL(Remote_ConnectNetwork); //配网语音由讯美模块播报
									 // g_AddOrDeleteNetFlag =SET;
									 // FuncNetwork_Connect(NET_DEV_ZGB, Menu_ConfigNetworkCallback);
}

/**
 * @brief  开启假期模式
 *
 * @note   
 */
// static void Menu_OpenVacationMode(void)
// {
// 	Audio_Play(JIA_QI_MO_SHI_YI_KAI_QI, RESET);
// 	// TODO:假期模式逻辑
// }

/**
 * @brief  关闭假期模式
 *
 * @note   
 */
// static void Menu_CloseVacationMode(void)
// {
// 	Audio_Play(JIA_QI_MO_SHI_YI_GUAN_BI, RESET);
// }

/**
 * @brief  开启自动上锁
 *
 * @note   lock主键
 */
static void Menu_OpenAutoLock(void)
{
	uint8_t mode = 0;
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Audio_Play(ZI_DONG_SHANG_SUO_YI_KAI_QI, RESET);
	// TODO:自动上锁逻辑发给后板锁主键

	mode = AUTO_MODE;
	if (OSAL_NvWrite(SYS_OFFSET(amMode), &mode, sizeof(mode)) != SUCCESS)
	{
		MENU_LOG("Menu_OpenAutoLock failed\n");
	}
	// 设置同步上锁模式
	Lock_ModeSync(mode);
}


/**
 * @brief  自动上锁时间输入逻辑处理（开启自动上锁时）
 *
 * @note
 */
static MenuType_enum_t Menu_OpenAutoLockSetTime(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType)
{
	uint8_t inputTime = 10;
	uint8_t autoLockTime = DEFAULT_AUTO_LOCK_TIME;
	if (inputType == INPUT_TYPE_RETURN)
	{
		return Menu_GetFatherMenu(menuCurrent);
	}

	inputTime = Menu_InputData2KeysNumber(pDataBuffer, dataLen); //将输入的数据转换成密钥编号
	MENU_LOG("inputTime:%d\r\n", inputTime);

	if (inputTime >= 100 || inputTime < 10) //输入错误
	{
		Audio_Play(SHU_RU_CUO_WU, RESET);
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		MENU_LOG("input_number 000 or more than 150 to this:%d\r\n", inputTime);
		return menuCurrent;
	}
	else
	{
		autoLockTime = inputTime;
		if (OSAL_NvWrite(SYS_OFFSET(auto_lock_time), &autoLockTime, sizeof(autoLockTime)) != SUCCESS)
		{
			MENU_LOG("AutoLockTime save failed\n");
			Audio_Play(SHE_ZHI_SHI_BAI, RESET);
			return Menu_GetFatherMenu(menuCurrent);
		}
		SYS_CALL(Local_LockUpdateAutoLockTime, autoLockTime);
		Menu_OpenAutoLock();
	}
	return Menu_GetFatherMenu(menuCurrent);
}


/**
 * @brief  关闭自动上锁
 *
 * @note   lock主键
 */
static void Menu_CloseAutoLock(void)
{
	uint8_t mode = 0;
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Audio_Play(ZI_DONG_SHANG_SUO_YI_GUAN_BI, RESET);
	mode = MANUAL_MODE;
	if (OSAL_NvWrite(SYS_OFFSET(amMode), &mode, sizeof(mode)) != SUCCESS)
	{
		MENU_LOG("Menu_CloseAutoLock failed\n");
	}

	Lock_ModeSync(mode);
}

// /**
//   * @brief  退出网络
//   *
//   * @note   zigbee模块
//   */
// static void Menu_DelNetwork(void)
// {
// 	// g_AddOrDeleteNetFlag =SET;
// 	// FuncNetwork_Disconnect(NET_DEV_ZGB, Menu_ConfigNetworkCallback);
// }

/**
 * @brief  连接蓝牙
 *
 * @note   BLE模组
 */
static void Menu_ConnectBLE(void)
{
	// FuncNetwork_Connect(NET_DEV_BLE, Menu_ConfigNetworkCallback);
}

/**
 * @brief  断开蓝牙
 *
 * @note   BLE模组
 */
static void Menu_DisconnectBLE(void)
{
	// FuncNetwork_Disconnect(NET_DEV_BLE, Menu_ConfigNetworkCallback);
}

/**
 * @brief  开启蓝牙
 *
 * @note   BLE模组
 */
static void Menu_OpenBLE(void)
{
	// NvSystemParam_stu_t sysParam;

	// // FuncNetwork_SetNetworkDeviceStatus(NET_DEV_BLE, NETWORK_DEV_STA_OPEN);

	// FuncNvMan_ReadSystemParam(&sysParam);
	// sysParam.bluetoothStatus = BLE_POWER_ON;
	// FuncNvMan_SetSystemParam(&sysParam);
	// Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	// Oled_PopUpDisplay(POPUPS_SettingSucceed,POPUPS_NULL,POP_UP_WINDOW_TIME);
	// FuncNetwork_SettingChangeReprot(0);
}

/**
 * @brief  关闭蓝牙
 *
 * @note   BLE模组
 */
static void Menu_CloseBLE(void)
{
	// NvSystemParam_stu_t sysParam;

	// FuncNetwork_SetNetworkDeviceStatus(NET_DEV_BLE, NETWORK_DEV_STA_CLOSE);

	// FuncNvMan_ReadSystemParam(&sysParam);
	// sysParam.bluetoothStatus = BLE_POWER_OFF;
	// FuncNvMan_SetSystemParam(&sysParam);
	// Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	// Oled_PopUpDisplay(POPUPS_SettingSucceed,POPUPS_NULL,POP_UP_WINDOW_TIME);
	// FuncNetwork_SettingChangeReprot(0);
}

/**
 * @brief  语音模式中音量
 *
 * @note
 */
static void Menu_SetAudioOn(void)
{
	MENU_LOG("Menu_SetVoice on\r\n");
	isMuteflag = RESET;
	uint8_t voice = 0;
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	voice = 80;
	Audio_SetVolume(voice);
	Audio_SetMuteMode(RESET);
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  语音模式高音量
 *
 * @note
 */
static void Menu_SetAudioOnHighVolume(void)
{
	MENU_LOG("Menu_SetVoice on\r\n");
	isMuteflag = RESET;
	uint8_t voice = 0;
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	voice = 100;
	Audio_SetVolume(voice);
	Audio_SetMuteMode(RESET);
	SYS_CALL(Remote_SettingChangeReport);
}
/**
 * @brief  语音模式低音量
 *
 * @note
 */
static void Menu_SetAudioOnLowVolume(void)
{
	MENU_LOG("Menu_SetVoice on\r\n");
	isMuteflag = RESET;
	uint8_t voice = 0;
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	voice = 60;
	Audio_SetVolume(voice);
	Audio_SetMuteMode(RESET);
	SYS_CALL(Remote_SettingChangeReport);
}


/**
 * @brief  静音模式
 *
 * @note
 */
static void Menu_SetAudioOff(void)
{
	MENU_LOG("Menu_SetVoice off\r\n");
	isMuteflag = SET;
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  设置中文模式
 *
 * @note
 */
static void Menu_SetLanguageChinese(void)
{
	MENU_LOG("Menu_SetLanguage:Chinese\r\n");
	uint8_t language = LANGUAGE_CHINESE;
	Menu_UpdateSysLanguage(language);
	Audio_SetLanguage(language);
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	SYS_CALL(Remote_SetLanguage, language);
	SYS_CALL(Remote_SettingChangeReport);

	// TODO 网络上报锁状态信息
}

/**
 * @brief  设置英文模式
 *
 * @note
 */
static void Menu_SetLanguageEnglish(void)
{
	MENU_LOG("Menu_SetLanguage:English\r\n");
	uint8_t language = LANGUAGE_ENGLISH;
	Audio_SetLanguage(language);
	Menu_UpdateSysLanguage(language);
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);

	SYS_CALL(Remote_SetLanguage, language);
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  快捷键切换语言
 *
 * @note
 */
static void Menu_SetLanguageByShortKey(void)
{
	uint8_t language = LANGUAGE_ENGLISH;
	if (g_SysLanguage == LANGUAGE_ENGLISH)
	{
		language = LANGUAGE_CHINESE;
	}
	else
	{
		language = LANGUAGE_ENGLISH;
	}
	MENU_LOG("ShortKey set g_SysLanguage:%d\r\n", g_SysLanguage);
	Audio_SetLanguage(language);
	Menu_UpdateSysLanguage(language);
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);

	SYS_CALL(Remote_SetLanguage, language);
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  设置西班牙语模式
 *
 * @note
 */
static void Menu_SetLanguageSpanish(void)
{
	MENU_LOG("Menu_SetLanguage:Spanish\r\n");
	uint8_t language = LANGUAGE_SPANISH;
	Audio_SetLanguage(language);
	Menu_UpdateSysLanguage(language);
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);

	SYS_CALL(Remote_SetLanguage, language);
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  设置葡萄牙语模式
 *
 * @note
 */
static void Menu_SetLanguagePortuguese(void)
{
	MENU_LOG("Menu_SetLanguage:Portuguese\r\n");
	uint8_t language = LANGUAGE_PORTUGUESE;
	Audio_SetLanguage(language);
	Menu_UpdateSysLanguage(language);
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);

	SYS_CALL(Remote_SetLanguage, language);
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  设置俄语模式
 *
 * @note
 */
static void Menu_SetLanguageRussian(void)
{
	MENU_LOG("Menu_SetLanguage:Russian\r\n");
	uint8_t language = LANGUAGE_RUSSIAN;
	Audio_SetLanguage(language);
	Menu_UpdateSysLanguage(language);
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);

	SYS_CALL(Remote_SetLanguage, language);
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  设置哈萨克语模式
 *
 * @note
 */
static void Menu_SetLanguageKazak(void)
{
	MENU_LOG("Menu_SetLanguage:Russian\r\n");
	uint8_t language = LANGUAGE_KAZAK;
	Audio_SetLanguage(language);
	Menu_UpdateSysLanguage(language);
	Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);

	SYS_CALL(Remote_SetLanguage, language);
	SYS_CALL(Remote_SettingChangeReport);
}

void Menu_PlayKeyNum(uint8_t keyType, uint8_t keyNum)
{
	uint8_t aduioPlayNum = 0;
	// TODO: 编号删除减去累增
	if (keyType == USERS_TYPE_PWD)
	{
		aduioPlayNum = keyNum  + AUDIO_PLAY_PWD_NUM_START;
	}
	else if (keyType == USERS_TYPE_FPT)
	{
		aduioPlayNum = keyNum  + AUDIO_PLAY_FPT_NUM_START;
	}
	else if (keyType == USERS_TYPE_CARD)
	{
		aduioPlayNum = keyNum  + AUDIO_PLAY_CARD_NUM_START;
	}

	uint8_t playNum[5] = {0};
	sprintf(playNum, "%03d", aduioPlayNum);
	Audio_PlayString(playNum);
}

/**
 * @brief  设置常用模式
 *
 * @note
 */
static void Menu_SetOpenModeNormal(void)
{
	if (SYS_CALL(Users_SetSafeMode, RESET) == SUCCESS)
	{
		Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
		// TODO 网络上报状态信息
	}
	Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  设置安全模式
 *
 * @note
 */
static void Menu_SetOpenModeSafe(void)
{
	uint8_t fptQty = SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 0);	  //读取永久指纹数量
	uint8_t cardQty = SYS_CALL(Users_GetQty, USERS_TYPE_CARD, 0); //读取永久卡片数量
	uint8_t faceQty = SYS_CALL(Users_GetQty, USERS_TYPE_FACE, 0); //读取永久人脸数量
	if (SYS_CALL(Users_CheckAdmin, USERS_TYPE_FPT) == SUCCESS)
	{
		fptQty++; //已添加管理指纹
	}
	if (fptQty == 0 && cardQty == 0 && faceQty == 0)
	{
		Audio_Play(SHE_ZHI_SHI_BAI, RESET);
		Oled_PopUpDisplay(POPUPS_SettingFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
#if !defined(FPT_DISABLE)
		if (SYS_CALL(Users_GetKeysQty, USERS_TYPE_FPT))
		{
			Audio_Play(ZHI_WEN_KU_WEI_KONG, RESET);
			Oled_PopUpDisplay(POPUPS_NO_ADD_FPT, POPUPS_NULL, POP_UP_WINDOW_TIME);
		}
#endif
#if defined(CARD_FUNCTION)
		if (SYS_CALL(Users_GetKeysQty, USERS_TYPE_CARD))
		{
			Audio_Play(KA_PIAN_KU_WEI_KONG, RESET);
			Oled_PopUpDisplay(POPUPS_NO_ADD_USER_CARD, POPUPS_NULL, POP_UP_WINDOW_TIME);
		}
#endif
		return;
	}

	if (SYS_CALL(Users_SetSafeMode, SET) == SUCCESS)
	{
		Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
		Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
		// TODO 网络上报状态信息
	}
	else
	{
		Audio_Play(SHE_ZHI_SHI_BAI, RESET);
	}
	SYS_CALL(Remote_SettingChangeReport);
}

/**
 * @brief  删除所有密钥
 *
 * @note
 */
static void Menu_DeleteAllKeys(void)
{
	uint8_t keysType;
	MENU_LOG("Menu_DeleteAllKeys:%d\n", menuDelAll);
	//发删除所有卡片的指令
	MENU_LOG("MC60_Del_All_Nfc002\n");
	SYS_CALL(MC60_Del_All_Nfc);
	switch (menuDelAll)
	{
	case MENU_DEL_ALL_PWD:
		keysType = USERS_TYPE_PWD;
		break;
	case MENU_DEL_ALL_FPT:
		keysType = USERS_TYPE_FPT;
		break;
	case MENU_DEL_ALL_CARD:
		keysType = USERS_TYPE_CARD;
		break;
	default:
		return;
	}
	if (keysType == USERS_TYPE_PWD)
	{
		if (SYS_CALL(Users_GetQty, USERS_TYPE_PWD, 1) == 0 && SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 1) == 0 && SYS_CALL(Users_GetQty, USERS_TYPE_CARD, 1) == 0) //密码库为空
		{
			Audio_Play(SHAN_CHU_SHI_BAI, RESET);
			Oled_PopUpDisplay(POPUPS_DelFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			//Audio_Play(MI_MA_KU_WEI_KONG, RESET);
			// Audio_Play(ZHI_WEN_KU_WEI_KONG, RESET);
			// Audio_Play(KA_PIAN_KU_WEI_KONG, RESET);
			Oled_PopUpDisplay(POPUPS_PwdLibEmpty, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return;
		}
		if (SYS_CALL(Users_Del, USERS_TYPE_PWD, 0xFF, NULL, 0) > 0)
		{
			//Audio_Play(SHAN_CHU_CHENG_GONG, RESET);
			Oled_PopUpDisplay(POPUPS_DelSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			// Audio_Play(MI_MA_KU_WEI_KONG, RESET);
			// Oled_PopUpDisplay(POPUPS_PwdLibEmpty,POPUPS_NULL,POP_UP_WINDOW_TIME);
			Local_LockSaveRecord(EVENT_TYPE_PROGRAM, USERS_TYPE_PWD, EVENT_CODE_DELETE, 0XFF);
		}

		// if (SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 1) == 0) //指纹库为空
		// {
		// 	// Audio_Play(SHAN_CHU_SHI_BAI, RESET);
		// 	// Oled_PopUpDisplay(POPUPS_DelFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
		// 	// Audio_Play(ZHI_WEN_KU_WEI_KONG, RESET);
		// 	// Oled_PopUpDisplay(POPUPS_FPLibEmpty, POPUPS_NULL, POP_UP_WINDOW_TIME);

		// 	Audio_Play(SHAN_CHU_SHI_BAI, RESET);
		// 	 return;
		// }
		 if (SYS_CALL(Users_Del, USERS_TYPE_FPT, 0xFF, NULL, 0) > 0)
		{
				Finger_DeleteImage(0, KOS_PARAM_USERS_QTY_FINGER);//50
				//Audio_Play(SHAN_CHU_CHENG_GONG, RESET);
				Oled_PopUpDisplay(POPUPS_DelSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
				// Audio_Play(ZHI_WEN_KU_WEI_KONG, RESET);
				// Oled_PopUpDisplay(POPUPS_FPLibEmpty,POPUPS_NULL,POP_UP_WINDOW_TIME);
				Local_LockSaveRecord(EVENT_TYPE_PROGRAM, USERS_TYPE_FPT, EVENT_CODE_DELETE, 0XFF);
		}
		if (SYS_CALL(Users_Del, USERS_TYPE_CARD, 0xFF, NULL, 0) > 0)
		{
			//Oled_PopUpDisplay(POPUPS_DelSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			// Audio_Play(KA_PIAN_KU_WEI_KONG, RESET);
			// Oled_PopUpDisplay(POPUPS_CardsEmpty,POPUPS_NULL,POP_UP_WINDOW_TIME);
			Local_LockSaveRecord(EVENT_TYPE_PROGRAM, USERS_TYPE_CARD, EVENT_CODE_DELETE, 0XFF);
		}
		Audio_Play(SHAN_CHU_CHENG_GONG, RESET);

	}
	if (keysType == USERS_TYPE_FPT)
	{
		if (SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 1) == 0) //指纹库为空
		{
			Audio_Play(SHAN_CHU_SHI_BAI, RESET);
			Oled_PopUpDisplay(POPUPS_DelFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			Audio_Play(ZHI_WEN_KU_WEI_KONG, RESET);
			Oled_PopUpDisplay(POPUPS_FPLibEmpty, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return;
		}
		uint8_t isDel = 0;
		if (SYS_CALL(Users_GetSafeMode) == SET)
		{
			uint8_t cardQty = 0, faceQty = 0, admin_finger = 0, cnt = 0;
			cardQty = SYS_CALL(Users_GetQty, USERS_TYPE_CARD, 0);
			faceQty = SYS_CALL(Users_GetQty, USERS_TYPE_FACE, 0);
			admin_finger = SYS_CALL(Users_CheckAdmin, USERS_TYPE_FPT); //检查管理指纹
			cnt = cardQty + faceQty;
			MENU_LOG("cardQty + faceQty total num:%d, admin_finger:%d\r\n", cnt, admin_finger);
			if (cnt >= 1)
				isDel = 1;
		}
		else
		{
			isDel = 1;
		}
		if (isDel)
		{
			if (SYS_CALL(Users_Del, USERS_TYPE_FPT, 0xFF, NULL, 0) > 0)
			{
				Finger_DeleteImage(0, 100);
				Audio_Play(SHAN_CHU_CHENG_GONG, RESET);
				Oled_PopUpDisplay(POPUPS_DelSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
				// Audio_Play(ZHI_WEN_KU_WEI_KONG, RESET);
				// Oled_PopUpDisplay(POPUPS_FPLibEmpty,POPUPS_NULL,POP_UP_WINDOW_TIME);
				Local_LockSaveRecord(EVENT_TYPE_PROGRAM, USERS_TYPE_FPT, EVENT_CODE_DELETE, 0XFF);
			}
		}
		else
		{
			Audio_Play(SHAN_CHU_SHI_BAI_WEI_AN_QUAN_MO_SHI, RESET);

			Oled_PopUpDisplay(POPUPS_DelFailed, POPUPS_ForSafeMode, POP_UP_WINDOW_TIME);
		}
	}
	if (keysType == USERS_TYPE_CARD)
	{
		// if (SYS_CALL(Users_GetQty, USERS_TYPE_CARD, 1) == 0) //卡片库为空
		// {
		// 	Audio_Play(SHAN_CHU_SHI_BAI, RESET);
		// 	Oled_PopUpDisplay(POPUPS_DelFailed, POPUPS_DelFailed, POP_UP_WINDOW_TIME);
		// 	Audio_Play(KA_PIAN_KU_WEI_KONG, RESET);
		// 	Oled_PopUpDisplay(POPUPS_CardsEmpty, POPUPS_NULL, POP_UP_WINDOW_TIME);
		// 	return;
		// }

		uint8_t isDel = 0;
		if (SYS_CALL(Users_GetSafeMode) == SET)
		{
			uint8_t fptQty = 0, faceQty = 0, cnt = 0;
			fptQty = SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 0);
			// if (SYS_CALL(Users_CheckAdmin, USERS_TYPE_FPT) != SUCCESS)
			// {
			// 	fptQty++;
			// }
			faceQty = SYS_CALL(Users_GetQty, USERS_TYPE_FACE, 0);
			cnt = fptQty + faceQty;
			MENU_LOG("fptQty + faceQty total num:%d\r\n", cnt);
			if (cnt >= 1)
				isDel = 1;
		}
		else
		{
			isDel = 1;
		}
		if (isDel)
		{
			if (SYS_CALL(Users_Del, USERS_TYPE_CARD, 0xFF, NULL, 0) > 0)
			{
				Audio_Play(SHAN_CHU_CHENG_GONG, RESET);
				Oled_PopUpDisplay(POPUPS_DelSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
				// Audio_Play(KA_PIAN_KU_WEI_KONG, RESET);
				// Oled_PopUpDisplay(POPUPS_CardsEmpty,POPUPS_NULL,POP_UP_WINDOW_TIME);
				Local_LockSaveRecord(EVENT_TYPE_PROGRAM, USERS_TYPE_CARD, EVENT_CODE_DELETE, 0XFF);
			}
			//发删除所有卡片的指令
			//SYS_CALL(MC60_Del_All_Nfc);
		}
		else
		{
			Audio_Play(SHAN_CHU_SHI_BAI_WEI_AN_QUAN_MO_SHI, RESET);
			Oled_PopUpDisplay(POPUPS_DelFailed, POPUPS_ForSafeMode, POP_UP_WINDOW_TIME);
		}
	}
	// Menu_IsKeysLibEmpty(menuDelAll);
	menuDelAll = 0;
}

/**
 * @brief  进入系统锁定状态
 *
 * @note   记录锁定时的时间戳
 */
static void Menu_EnterSystemLocked(void)
{
	OSAL_TimeGet((uint8_t *)&systemLockedTimestamp, T_UTC);
	systemLockedTimestamp += 60; //锁定60s
	char led_string[] = "LED2";
	Led_Set("LED", LED_OFF, 0, 0);
	Led_Set(led_string, LED_FLASH, 0XFF, 60); // LED闪烁显示锁定时间 300
	Audio_Play(XI_TONG_YI_SUO_DING_QING_SHAO_HOU_ZAI_SHI, RESET);
	Oled_PopUpDisplay(POPUPS_SystemLocked, POPUPS_NULL, POP_UP_WINDOW_TIME);
	Oled_PopUpDisplay(POPUPS_TryAgainLater, POPUPS_NULL, POP_UP_WINDOW_TIME);
	masterMenuBox1.state = DISABLE;
	masterMenuBox2.state = DISABLE;
	masterMenuBox3.state = ENABLE;
	/* 发消息给网络任务 */
	Local_LockSaveRecord(EVENT_TYPE_ALARM, 0XFF, EVENT_CODE_SYS_LOCK, 0XFF);
	OSAL_NvWrite(SYS_OFFSET(systemLockedTime), &systemLockedTimestamp, sizeof(systemLockedTimestamp));
}

// /**
//   * @brief  获取蓝牙MAC地址回调
//   * @note
//   *
//   * @param  macAddr：蓝牙MAC存储地址
//   */
// static void Menu_GetBleMacCallback(uint32_t macAddr)
// {
// 	// if (macAddr != 0)
// 	// {
// 	// 	uint8_t macStr[5] = {0};
// 	// 	MENU_LOG("Get ble mac succeed.\r\n");
// 	// 	sprintf((char*)macStr, "%04X", *(uint16_t*)macAddr);
// 	// 	Oled_PopUpVaribaleText(POPUPS_BLE_MAC,macStr,NULL,POP_UP_WINDOW_TIME+1000);
// 	// 	FuncAudio_PlayString(macStr);
// 	// }
// }

/**
 * @brief  验证快捷键
 * @note   114：查询固件版本号
 *         115：查询蓝牙MAC地址
 *         400：查询防伪码
 *
 * @param  pInputData：输入的数据
 * @param  len：数据长度
 */
static ErrorStatus Menu_VerifyShortcutKeys(uint8_t *pInputData, uint8_t len)
{
	if (len > 3 || len == 0)
	{
		return ERROR;
	}
	uint8_t length = 0;
	if (pInputData[0] == 1 && pInputData[1] == 1 && pInputData[2] == 4)
	{
		Audio_PlayString(FIRMWARE_VER); //播放固件版本
		uint8_t *pNULL = NULL;
		Oled_PopUpVaribaleText(POPUPS_FIRMARE_VERSION, (uint8_t *)FIRMWARE_VER, (uint8_t *)pNULL, POP_UP_WINDOW_TIME + 1500);
		return SUCCESS;
	}
	else if (pInputData[0] == 5 && pInputData[1] == 0 && pInputData[2] == 4)
	{
		static uint8_t sn[3] = {0};
		OSAL_NvRead(SYS_OFFSET(eSN), sn, 3);
		length = strlen((char *)sn);
		if (length == 3)
		{
			Audio_PlayString((char *)sn);
			uint8_t *pNULL = NULL;
			Oled_PopUpVaribaleText(POPUPS_PID, sn, pNULL, POP_UP_WINDOW_TIME);
		}
		else
		{
			Audio_Play(QING_SHUA_FANG_WEI_MA, RESET);
			Oled_PopUpDisplay(POPUPS_ScanCode, POPUPS_NULL, POP_UP_WINDOW_TIME);
		}
		return SUCCESS;
	}
	else if (pInputData[0] == 4 && pInputData[1] == 0 && pInputData[2] == 0)
	{
		static uint8_t code[32] = {0};
		OSAL_NvRead(SYS_OFFSET(systemCode), code, 32);
		length = strlen((char *)code);
		if (length > 0)
		{
			Audio_PlayString((char *)code);
			if (length > 16)
			{
				static uint8_t partB[16] = {0};
				memcpy(partB, &code[16], sizeof(partB));
				Oled_PopUpVaribaleText(POPUPS_Code, code, partB, POP_UP_WINDOW_TIME + 15000);
			}
			else
			{
				uint8_t *pNULL = NULL;
				Oled_PopUpVaribaleText(POPUPS_Code, code, pNULL, POP_UP_WINDOW_TIME + 10000);
			}
		}
		else
		{
			Audio_Play(QING_SHUA_FANG_WEI_MA, RESET);
			Oled_PopUpDisplay(POPUPS_ScanCode, POPUPS_NULL, POP_UP_WINDOW_TIME);
		}
		return SUCCESS;
	}
	else if (pInputData[0] == 2 && pInputData[1] == 2 && pInputData[2] == 2)
	{
		// 设置前先读取一下语音主键NV保存的数据，确保数据已经同步过来
		Menu_UpdateSysLanguage(Audio_GetLanguage());
		MENU_LOG("get audio language %d\r\n", g_SysLanguage);
		Menu_SetLanguageByShortKey();
		return SUCCESS;
	}
	else if (pInputData[0] == 1 && len == 1)
	{
		uint8_t version[13] = {0};
		Audio_Play(BAN_BEN_HAO, RESET);
		Audio_PlayString(FIRMWARE_VER); //播放固件版本 
        Lock_GetVersion(version);
		//MENU_LOG("lock version:%s\r\n", version);
		Audio_PlayString(version); 
		uint8_t *pNULL = NULL;
		Oled_PopUpVaribaleText(POPUPS_FIRMARE_VERSION, (uint8_t *)FIRMWARE_VER, (uint8_t *)pNULL, POP_UP_WINDOW_TIME + 1500);
		return SUCCESS;
	}
	else if (pInputData[0] == 7 && pInputData[1] == 8 && len == 2)
	{
		// TODO:常开模式，关闭自动上锁
		Audio_Play(CHANG_KAI_MO_SHI_QING_ZHU_YI_AN_QUAN, RESET);
		return SUCCESS;
	}
	// else if (pInputData[0] == 3 && pInputData[1] == 3 && pInputData[2] == 3)
	// {
	// 	if (SYS_CALL(Users_AdminPwdIsEasy) == SUCCESS)
	// 	{
	// 		return ERROR;
	// 	}
	// 	// TODO：验证管理员密码后启动自学习
	// 	Audio_Play(QING_YAN_ZHENG_GUAN_LI_YUAN_MI_MA, RESET);
	// 	shortKeyLockStudy = SET;

	// 	return SUCCESS;
	// }
	// TODO: LED ON OFF
	// else if (pInputData[0] == 9 && pInputData[1] == 9 && pInputData[2] == 9)
	// {
	// 	Finger_LedControl(FINGER_LED_ON_MODE, FINGER_LED_GREEN, FINGER_LED_GREEN, 0);
	// 	return SUCCESS;
	// }
	// else if (pInputData[0] == 9 && pInputData[1] == 9 && pInputData[2] == 8)
	// {
	// 	Finger_LedControl(FINGER_BREATHING_LED__MODE, FINGER_LED_BLUE, FINGER_LED_BLUE, 3);
	// 	return SUCCESS;
	// }
	// else if (pInputData[0] == 9 && pInputData[1] == 9 && pInputData[2] == 7)
	// {
	// 	Finger_LedControl(FINGER_FLASHING_LED_MODE, FINGER_LED_RED, FINGER_LED_RED, 5);
	// 	return SUCCESS;
	// }
	// else if (pInputData[0] == 9 && pInputData[1] == 9 && pInputData[2] == 0)
	// {
	// 	Finger_LedControl(FINGER_LED_OFF_MODE, FINGER_LED_ALL_OFF, FINGER_LED_ALL_OFF, 0);
	// 	return SUCCESS;
	// }

	return ERROR;
}

/**
 * @brief  读取系统标志
 * @note
 *
 * @param  flag：SYS_FLAG_TAMPER_ALARM 防撬报警标志
 *               SYS_FLAG_ARMING_ALARM 布防报警标志
 *               SYS_FLAG_ARMING_STATUS 布防状态
 *               SYS_FLAG_ANTI_LOCK_STATUS 反锁状态
 *
 * @return state：标志状态 true 和 false
 */
static bool Menu_GetSysFlag(uint8_t flag)
{
	uint8_t sys_flag = 0;
	OSAL_NvRead(SYS_OFFSET(sys_flag), &sys_flag, 1);
	uint8_t test = 0;
	test = sys_flag & flag;
	MENU_LOG("sys_flag :%02X,test:%02X\r\n", sys_flag, test);
	return (sys_flag & flag) ? true : false;
}

/**
 * @brief  播报双重验证模式下的各种组合语音
 *@param   type 第一次验证的密钥类型
 * @note   搞个全局变量保存 验证记录
 */
static void Menu_SafeModeAudioPlay(uint8_t type)
{
	// uint8_t fptNum = 0, cardNum = 0, faceNum = 0;
	// fptNum = SYS_CALL(Users_GetKeysQty, USERS_TYPE_FPT);
	// cardNum = SYS_CALL(Users_GetKeysQty, USERS_TYPE_CARD);
	if (USERS_TYPE_PWD == type)
	{
#if defined(CARD_FUNCTION)
#if defined(FPT_DISABLE)
		Oled_PopUpDisplay(POPUPS_SwipingCard, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Audio_Play(QING_SHUA_KA, RESET);
#else
		Oled_PopUpDisplay(POPUPS_CardOrFp, POPUPS_NULL, POP_UP_WINDOW_TIME);
		//Audio_Play(QING_SHUA_KA_HUO_AN_ZHI_WEN, RESET);
#endif
#else
		Oled_PopUpDisplay(POPUPS_PressFP, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Audio_Play(QING_AN_ZHI_WEN, RESET);
#endif
	}
	else if (USERS_TYPE_FPT == type)
	{
#if defined(CARD_FUNCTION)
		Oled_PopUpDisplay(POPUPS_PwdOrCard, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Audio_Play(QING_SHU_RU_MI_MA_HUO_KA_PIAN, RESET);
#else
		Oled_PopUpDisplay(POPUPS_PlsEnterPwd, POPUPS_NULL, POP_UP_WINDOW_TIME);
		// Audio_Play(QING_SHU_RU_YONG_HU_MI_MA_YI_JING_HAO_JIAN_JIE_SHU, RESET);
#endif
	}
	else if (USERS_TYPE_CARD == type)
	{
#if defined(FPT_DISABLE)
		Oled_PopUpDisplay(POPUPS_PlsEnterPwd, POPUPS_NULL, POP_UP_WINDOW_TIME);
		// Audio_Play(QING_SHU_RU_YONG_HU_MI_MA_YI_JING_HAO_JIAN_JIE_SHU, RESET);
#else
		Audio_Play(QING_SHU_RU_MI_MA_HUO_ZHI_WEN, RESET);
		Oled_PopUpDisplay(POPUPS_PwdOrFP, POPUPS_NULL, POP_UP_WINDOW_TIME);
#endif
	}
}

/**
 * @brief 错误密码超时定时器
 *
 * @param [in] xTimer
 *
 * @note
 */
static void PWD_TimerCallback(TimerHandle_stu_t xTimer)
{
	uint8_t flag = 0;
	PWD_ERR_TIMEOUTTimer = NULL;

	flag = get_access_code_err_flag();

	if(flag == 0)
	{
		Menu_ProcessVerifyFailed();
		//Finger_LedControl(FINGER_FLASHING_LED_MODE, FINGER_LED_RED, FINGER_LED_RED, LED_COUNTS); // 失败： 闪红灯1次
	}
	else
	{
		set_access_code_err_flag(0);
	}
}
/**
 * @brief  密钥验证功能逻辑处理
 *
 * @note   搞个全局变量保存 验证记录
 */
static MenuType_enum_t Menu_VerifyKeys(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType)
{
	MenuType_enum_t menu = MENU_MASTER;
	static uint8_t first_user_id;
	static uint8_t first_user_type;
	//uint8_t temporary_pwd_ret = ERROR;
	if (inputType == INPUT_TYPE_CHAR)
	{
		/* 处理快捷键逻辑 */
		if (Menu_VerifyShortcutKeys(pDataBuffer, dataLen) != ERROR)
		{
			return menu;
		}
	}

	/* 处理系统锁定逻辑 */
	if (inputType == INPUT_TYPE_CHAR || inputType == INPUT_TYPE_FP || inputType == INPUT_TYPE_CARD)
	{
		uint8_t time = Menu_GetSystemLockedStatus();
		if (time > 0)
		{
			char led_string[] = "LED2";
			led_string[3] = time + '0';
			Led_Set("LED", LED_OFF, 0, 0);
			Led_Set(led_string, LED_FLASH, 0XFF, 60); // LED闪烁显示锁定时间 300
			Audio_Play(XI_TONG_YI_SUO_DING_QING_SHAO_HOU_ZAI_SHI, RESET);
			Oled_PopUpDisplay(POPUPS_SystemLocked, POPUPS_NULL, POP_UP_WINDOW_TIME);
			Oled_PopUpDisplay(POPUPS_TryAgainLater, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return menu;
		}
	}

	if (Menu_GetSysFlag(SYS_FLAG_ANTI_LOCK_STATUS) == SET) //已反锁
	{
		asteriskKeyTimes = 2;
	}
	int user_id = VERIFY_ERROR;
	uint8_t user_type = 0XFF;
	switch (inputType)
	{
	case INPUT_TYPE_RETURN:
		// TODO:临时需求，只需要一个开门密码 123456，不需要管理员菜单
		//(asteriskKeyTimes < 2) ? asteriskKeyTimes++ : asteriskKeyTimes;
		inputDualFlag = RESET;
		return menu;
	case INPUT_TYPE_CHAR:
		user_type = USERS_TYPE_PWD;
		user_id = SYS_CALL(Users_Verify, pDataBuffer, dataLen, inputDualFlag, USERS_TYPE_PWD, asteriskKeyTimes, 0);
		break;
	case INPUT_TYPE_FP:
		user_type = USERS_TYPE_FPT;
		user_id = SYS_CALL(Users_Verify, pDataBuffer, dataLen, inputDualFlag, USERS_TYPE_FPT, asteriskKeyTimes, 0);
		break;
	case INPUT_TYPE_CARD:
		user_type = USERS_TYPE_CARD;
		user_id = SYS_CALL(Users_Verify, pDataBuffer, dataLen, inputDualFlag, USERS_TYPE_CARD, asteriskKeyTimes, 0);
		break;
	default:
		return menu;
	}

	if (Menu_GetSysFlag(SYS_FLAG_ANTI_LOCK_STATUS) == SET) //已反锁
	{
		if (user_id >= VERIFY_DUAL_OFFSET)
		{
			user_id -= VERIFY_DUAL_OFFSET;
		}
		if (user_id != VERIFY_ADMIN_OK && !(user_id == 0 && user_type == USERS_TYPE_FPT))
		{
			Led_Set("LED", LED_OFF, 0, 0);
			Led_Set("LED13579", LED_FLASH, 2, 100);
			Led_Set("LED_PWM_R", LED_FLASH, 2, 100);
			Led_Set("LED_RED", LED_FLASH, 1, 500);
			Audio_Play(YI_FAN_SUO, RESET);
			Oled_PopUpDisplay(POPUPS_AGAINST, POPUPS_NULL, POP_UP_WINDOW_TIME);
			MENU_LOG("door is anit\r\n");
			return menu;
		}
	}
	// if (user_id == VERIFY_ADMIN_OK && shortKeyLockStudy)
	// {
	// 	// TODO:启动自学习
	// 	shortKeyLockStudy = RESET;
	// 	return menu;
	// }

	if (user_id == VERIFY_ADMIN_OK && asteriskKeyTimes >= 2)
	{
		Led_Set("LED_CLOSE", LED_OFF, 0, 0);
		Led_Set("LED_OPEN", LED_FLASH, 1, 2000);
		isMuteflag = Audio_GetMuteMode();
		Audio_SetMuteMode(RESET);
		Audio_Play(YI_JIN_RU_GUAN_LI_MO_SHI, RESET);
		Oled_PopUpDisplay(POPUPS_MasterMode, POPUPS_NULL, POP_UP_WINDOW_TIME);
		menu = (SYS_CALL(Users_AdminPwdIsEasy) == SUCCESS) ? MENU_MODIFY_ADMIN : MENU_MANAGE_MODE;
		Menu_ExitSystemLocked();
		inputDualFlag = RESET;
	}
	else if (user_id == NOT_ACTIVATION)
	{
		Audio_Play(MEN_SUO_WEI_JI_HUO_QING_SHU_RU_JI_HUO_MA, RESET);
		Oled_PopUpDisplay(POPUPS_ScanCode, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Menu_ExitSystemLocked();
		inputDualFlag = RESET;
		MENU_LOG("NOT_ACTIVATION\r\n");
		Led_Set("LED_OPEN", LED_FLASH, 1, 2000);
		Led_Set("LED_PWM_G", LED_ON, 0, 0);
		Led_Set("LED", LED_OFF, 0, 0); //关闭按键板灯光
		Led_Set("LED_LOCK", LED_ON, 0, 0);
		Local_LockControl(user_type, 0, LOCK_CTRL_VERIFY_OK);
	}
	else if (user_id == ACTIVATION_OK)
	{
		Audio_Play(JI_HUO_CHENG_GONG, RESET);
		Oled_PopUpDisplay(POPUPS_ActivateSuccess, POPUPS_NULL, POP_UP_WINDOW_TIME);
	}
	else if (user_id == VERIFY_ERROR)
	{
		MENU_LOG(">>>>>>>>user_type:%d, dataLen:%d,\r\n",user_type, dataLen);
		// if (SYS_CALL(Users_AdminPwdIsEasy) == SUCCESS)
		// {
		// 	Audio_Play(MI_MA_GUO_YU_JIAN_DAN, RESET);
		// 	Oled_PopUpDisplay(POPUPS_PwdTooSimple, POPUPS_NULL, POP_UP_WINDOW_TIME);
		// 	Audio_Play(QING_XIU_GAI_GUAN_LI_MI_MA, RESET);
		// }
		// else
		//{
			//Menu_ProcessVerifyFailed();
			//Access code
			//Finger_LedControl(FINGER_FLASHING_LED_MODE, FINGER_LED_RED, FINGER_LED_RED, LED_COUNTS); // 失败： 闪红灯1次
			//Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET); // YAN_ZHENG_SHI_BAI
			 if (user_type == USERS_TYPE_PWD)
			 {
				//临时密码
				// uint8_t temporary_verify_keys(uint8_t *inputKey, uint8_t keyLen);
				// if (dataLen == 8)
				// {
				// 	for (uint8_t i = 0; i < 8; i++)
				// 	{
				// 		pDataBuffer[i] = pDataBuffer[i] + 0x30;
				// 	}

				// 	temporary_pwd_ret = temporary_verify_keys(pDataBuffer, 8);
				// }
				// if (temporary_pwd_ret = ERROR)
				{
					// 4--8
					if (dataLen >= 4 && dataLen <= 8)
					{
						if(asteriskKeyTimes == 2)
						{
							Menu_ProcessVerifyFailed();
							Audio_SetNotMute(ENTER_MANAGE_NOMUTE_FLAG,RESET);
							//MENU_LOG("Audio_SetNotMute 1\n");
						}
						else
						{
							MENU_LOG_HEX("pDataBuffer", pDataBuffer, dataLen);
							SYS_CALL(MC60_Send_Access_Code, pDataBuffer, dataLen);
							//超时300ms定时器 
							if (PWD_ERR_TIMEOUTTimer == NULL)
							{
								PWD_ERR_TIMEOUTTimer = OSAL_TimerCreate(PWD_TimerCallback, PWD_ERR_TIMER_TIMEOUT_MS, RESET);
								MENU_LOG("Create PWD_ERR_TIMEOUTTimer");
							}
						}
					}
					else
					{
						Menu_ProcessVerifyFailed();
						if(asteriskKeyTimes == 2)
						{
							Audio_SetNotMute(ENTER_MANAGE_NOMUTE_FLAG,RESET);
							//MENU_LOG("Audio_SetNotMute 2\n");
						}
						Finger_LedControl(FINGER_FLASHING_LED_MODE, FINGER_LED_RED, FINGER_LED_RED, LED_COUNTS); // 失败： 闪红灯1次
					}
				}
				// else
				// {
				// 	user_id = 253;
				// }
			 }
			 else
			 {
					Menu_ProcessVerifyFailed();
					Finger_LedControl(FINGER_FLASHING_LED_MODE, FINGER_LED_RED, FINGER_LED_RED, LED_COUNTS); // 失败： 闪红灯1次
			 }		 
		//}

	}
	else if (user_id >= VERIFY_DUAL_OFFSET) //双重验证模式下，首次次验证成功
	{
		Led_Set("LED", LED_ON, 0, 0); //关闭按键板灯光
		Led_Set("LED_LOCK", LED_ON, 0, 0);
		first_user_id = user_id % VERIFY_DUAL_OFFSET;
		first_user_type = user_type;
		Menu_SafeModeAudioPlay(user_type);
		inputDualFlag = SET;
	}
	else if (user_id >= 0 && asteriskKeyTimes != 2) //验证成功，直接开锁
	{
		Led_Set("LED_CLOSE", LED_OFF, 0, 0);
		Led_Set("LED_OPEN", LED_FLASH, 1, 2000);
		Led_Set("LED_PWM_G", LED_ON, 0, 0);
		Led_Set("LED", LED_OFF, 0, 0); //关闭按键板灯光
		Led_Set("LED_LOCK", LED_ON, 0, 0);
		Finger_LedControl(FINGER_FLASHING_LED_MODE, FINGER_LED_GREEN, FINGER_LED_GREEN, LED_COUNTS); // 成功： 闪绿灯1次

		Menu_ExitSystemLocked();
		Audio_Play(YAN_ZHENG_CHENG_GONG, RESET);

		if (Local_LockGetAmMode() == MANUAL_MODE)
		{
			// 手动模式下，验证成功后开启自动模式
			uint8_t mode = 0;
			mode = AUTO_MODE;

			if (OSAL_NvWrite(SYS_OFFSET(amMode), &mode, sizeof(mode)) != SUCCESS)
			{
				MENU_LOG("Menu_OpenAutoLock failed\n");
			}
			// 设置同步上锁模式
			Lock_ModeSync(mode);
			MENU_LOG("Menu verify ok Open Auto Mode \r\n");

		}

		Oled_PopUpDisplay(POPUPS_VerifySuccess, POPUPS_NULL, POP_UP_WINDOW_TIME);
		if (inputDualFlag == SET)
		{
			// 双重验证模式下，存第一个钥匙的开锁记录
			Local_LockControl(first_user_type, first_user_id, LOCK_CTRL_VERIFY_OK);
		}
		else
		{
			if(user_id == VERIFY_ADMIN_OK)
			{
				user_type = USERS_TYPE_ADMIN_PWD;
			}
			Local_LockControl(user_type, user_id, LOCK_CTRL_VERIFY_OK);
		}
		inputDualFlag = RESET;
		MENU_LOG("VERIFY_OPEN_LOCK_OK\r\n");
	}
	else if (user_id >= 0 && asteriskKeyTimes == 2)
	{
		//Finger_LedControl(FINGER_FLASHING_LED_MODE, FINGER_LED_RED, FINGER_LED_RED, LED_COUNTS); // 失败： 闪红灯1次
		Led_Set("LED_OPEN", LED_OFF, 0, 0);
		Led_Set("LED_CLOSE", LED_FLASH, 1, 2000);
		Audio_Play(YAN_ZHENG_SHI_BAI_2, RESET);
		Audio_SetNotMute(ENTER_MANAGE_NOMUTE_FLAG,RESET);
		//MENU_LOG("Audio_SetNotMute 3\n");
	}
	else
	{
		(void)0;
	}
	asteriskKeyTimes = 0;
	return menu;
}

/**
 * @brief  添加密钥功能逻辑处理
 *
 * @note
 */
static MenuType_enum_t Menu_AddKeys(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType)
{
	MenuType_enum_t menu = menuCurrent;
	UsersAddRes_enum_t res = ADD_ERROR;
	ErrorStatus adminPwdEasyFlag = SYS_CALL(Users_AdminPwdIsEasy);
	uint8_t attr = USERS_ATTR_FOREVER;
	uint8_t keyType = 0;
	uint16_t audioNum = 0;

	switch (inputType)
	{
		case INPUT_TYPE_CHAR:
			keyType = USERS_TYPE_PWD;
			audioNum = MI_MA_KU_YI_MAN;
			break;

		case INPUT_TYPE_FP:
			keyType = USERS_TYPE_FPT;
			audioNum = ZHI_WEN_KU_YI_MAN;
			break;

		case INPUT_TYPE_CARD:
			keyType = USERS_TYPE_CARD;
			audioNum = KA_PIAN_KU_YI_MAN;
			break;

		// case INPUT_TYPE_FACE:
		// 	keyType = USERS_TYPE_FACE;
		// 	audioNum = REN_LIAN_KU_YI_MAN;
		// 	break;

		// case INPUT_TYPE_PVN:
		// 	keyType = USERS_TYPE_PVN;
		// 	audioNum = ZHANG_WEN_KU_YI_MAN;
		// 	break;

		default:
			audioNum = TIAN_JIA_SHI_BAI;
			break;
	}

	MENU_LOG("Menu_AddKeys %d inputType %d  keytype", menuCurrent, inputType, keyType);
	if (appAddKeysFlag == RESET)
	{
		inputKeysNumber = SYS_CALL(Users_GetValidId, keyType);
		if (inputKeysNumber == 0xFF)
		{
			/* 没有空闲的存储，该类型密钥已满 */
			// 切换菜单时会判断密钥是否满
			Audio_Play(audioNum, RESET);

			return Menu_GetFatherMenu(menuCurrent);
		}
	}

	MENU_LOG("add normal pwd menu %d attr %d num %d ", menuCurrent, attr, inputKeysNumber);

	switch (inputType)
	{
	case INPUT_TYPE_CHAR:
		// if (inputKeysNumber <= 4 || inputKeysNumber >= 10) //未指定，按默认属性
		// {
		// 	attr = USERS_ATTR_FOREVER;
		// }
		// else if (inputKeysNumber <= 8)
		// {
		// 	attr = USERS_ATTR_ONCE;
		// }
		// else
		// {
		// 	attr = USERS_ATTR_URGENT;
		// }
		attr = USERS_ATTR_FOREVER;
		if (menuCurrent == MENU_ADD_ADMIN_PWD || menuCurrent == MENU_ADD_ADMIN_FPT || menuCurrent == MENU_MODIFY_ADMIN) // 添加管理用户
		{
			// MENU_LOG("add admin pwd attr %d num %d", attr, inputKeysNumber);
			inputKeysNumber = USERS_ID_ADMIN;//TODO：改成编号0, 1 ？
			attr = USERS_ATTR_ADMIN;
			MENU_LOG("add admin pwd attr %d num %d", attr, inputKeysNumber);
		} 
		res = SYS_CALL(Users_Add, USERS_TYPE_PWD, inputKeysNumber, pDataBuffer, dataLen, attr, inputDualFlag);////inputKeysNumber
		keyType = USERS_TYPE_PWD;
		break;
	case INPUT_TYPE_FP:
		res = SYS_CALL(Users_Add, USERS_TYPE_FPT, inputKeysNumber, pDataBuffer, dataLen, attr, inputDualFlag);//  inputKeysNumber
		keyType = USERS_TYPE_FPT;
		break;
	case INPUT_TYPE_CARD:
		res = SYS_CALL(Users_Add, USERS_TYPE_CARD, inputKeysNumber, pDataBuffer, dataLen, attr, inputDualFlag);//
		keyType = USERS_TYPE_CARD;
		break;
	case INPUT_TYPE_RETURN:
		inputDualFlag = RESET;
		if (adminPwdEasyFlag == SUCCESS && menuCurrent == MENU_MODIFY_ADMIN)
		{
			return MENU_MASTER;
		}
		else if (appAddKeysFlag == SET)
		{
			return backupMenu;
		}
		else
		{
			return Menu_GetFatherMenu(menuCurrent);
		}
	}
	//MENU_LOG("add res :%d %d\r\n", inputKeysNumber, res);
	MENU_LOG("add res :%d\r\n", res);
	switch (res)
	{
	case ADD_ONCE_OK: //第一次输入ok
		Oled_PopUpDisplay(POPUPS_EnterAgain, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Audio_Play(QING_ZAI_CI_SHU_RU_XIN_MI_MA, RESET);
		inputDualFlag = SET;
		return MENU_MAX;

	case ADD_SUCCESS: //添加成功
		inputDualFlag = RESET;
		//上报网络  Leo
		if (menuCurrent == MENU_MODIFY_ADMIN)
		{
			Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
			//Device_DelayMs(20); // 单线语音在后板，播报时屏蔽中断，OS2OS接收中断异常，语音被插播
			Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			// ui 弹窗
			menu = (adminPwdEasyFlag == SUCCESS) ? MENU_MANAGE_MODE : MENU_USER_SET;
			Local_LockSaveRecord(EVENT_TYPE_PROGRAM, USERS_TYPE_PWD, EVENT_CODE_MODIFY, USERS_ID_ADMIN);//----
		}
		else
		{
			Local_LockSaveRecord(EVENT_TYPE_PROGRAM, keyType, EVENT_CODE_ADD, inputKeysNumber);
			Audio_Play(TIAN_JIA_CHENG_GONG_BIAN_HAO, RESET);
			Menu_PlayKeyNum(keyType, inputKeysNumber); // 播放添加成功的语音
			//Device_DelayMs(20); // 单线语音在后板，播报时屏蔽中断，OS2OS接收中断异常，语音被插播

			// TODO: 播报成功编号
			Oled_PopUpDisplay(POPUPS_AddSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			// ui 弹窗
			menu = (Menu_IsKeysLibFull(menuCurrent) == SUCCESS) ? MENU_USER_SET : Menu_GetFatherMenu(menuCurrent);
		}
		appAddKeysResult = SET; //标记添加成功
		break;

	case ADD_ERR_TOO_SIMPLE: //密码太简单
		Audio_Play(MI_MA_GUO_YU_JIAN_DAN, RESET);
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		Oled_PopUpDisplay(POPUPS_PwdTooSimple, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Oled_PopUpDisplay(POPUPS_PleaseEnterAgain, POPUPS_NULL, POP_UP_WINDOW_TIME);
		inputDualFlag = RESET;
		menu = menuCurrent;
		break;
	case ADD_ERR_CARD_EXISTS: //卡片已存在
	case ADD_ERR_EXISTS:	  //密码已存在
		(menuCurrent == MENU_MODIFY_ADMIN) ? Audio_Play(SHE_ZHI_SHI_BAI, RESET) : Audio_Play(TIAN_JIA_SHI_BAI, RESET);
		if (menuCurrent == MENU_ADD_PWD || menuCurrent == MENU_MODIFY_ADMIN)
		{
			Audio_Play(MI_MA_YI_CUN_ZAI, RESET);
			if (menuCurrent == MENU_ADD_PWD)
				Oled_PopUpDisplay(POPUPS_AddFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			else
				Oled_PopUpDisplay(POPUPS_SettingFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			Oled_PopUpDisplay(POPUPS_PwdExists, POPUPS_NULL, POP_UP_WINDOW_TIME);
		}
		else if (menuCurrent == MENU_ADD_CARD)
		{
			Audio_Play(KA_PIAN_YI_CUN_ZAI, RESET);//
			Oled_PopUpDisplay(POPUPS_AddFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			Oled_PopUpDisplay(POPUPS_CardExist, POPUPS_NULL, POP_UP_WINDOW_TIME);
		}
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		inputDualFlag = RESET;
		menu = menuCurrent;
		break;

	case ADD_ERR_NOT_SAME: //两次不一致
		(menuCurrent == MENU_MODIFY_ADMIN) ? Audio_Play(SHE_ZHI_SHI_BAI, RESET) : Audio_Play(TIAN_JIA_SHI_BAI, RESET);
		Audio_Play(LIANG_CI_SHU_RU_DE_MI_MA_BU_YI_ZHI, RESET);
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		if (menuCurrent == MENU_ADD_PWD)
			Oled_PopUpDisplay(POPUPS_AddFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
		else
			Oled_PopUpDisplay(POPUPS_SettingFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Oled_PopUpDisplay(POPUPS_PwdNotSame, POPUPS_NULL, POP_UP_WINDOW_TIME);
		inputDualFlag = RESET;
		menu = menuCurrent;
		break;

	case ADD_ERR_RANGE: //密码长度范围错误
		(menuCurrent == MENU_MODIFY_ADMIN) ? Audio_Play(SHE_ZHI_SHI_BAI, RESET) : Audio_Play(TIAN_JIA_SHI_BAI, RESET);
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		if (menuCurrent == MENU_ADD_PWD)
			Oled_PopUpDisplay(POPUPS_AddFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
		else
			Oled_PopUpDisplay(POPUPS_SettingFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Oled_PopUpDisplay(POPUPS_PwdRange, POPUPS_NULL, POP_UP_WINDOW_TIME);
		//			(currentMenu == MENU_ADD_PWD) ? Ui_CreatePopUpsEvent(POPUPS_AddFailed, POPUPS_PwdRange, 1500) : Ui_CreatePopUpsEvent(POPUPS_SettingFailed, POPUPS_PwdRange, 1500);
		inputDualFlag = RESET;
		menu = menuCurrent;
		break;

	case ADD_ERROR: //添加失败
		inputDualFlag = RESET;
		if (menuCurrent == MENU_MODIFY_ADMIN)
		{
			Audio_Play(SHE_ZHI_SHI_BAI, RESET);
			Oled_PopUpDisplay(POPUPS_SettingFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			// UI 弹窗
			menu = (adminPwdEasyFlag == SUCCESS) ? MENU_MASTER : Menu_GetFatherMenu(menuCurrent);
		}
		else
		{
			Audio_Play(TIAN_JIA_SHI_BAI, RESET);
			Oled_PopUpDisplay(POPUPS_AddFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
			// UI 弹窗
			menu = Menu_GetFatherMenu(menuCurrent);
		}
		break;
	}

	if (appAddKeysFlag != RESET)
	{
		SYS_CALL(Remote_AddOrDelKeys_Cb, res);
		menu = (res == ADD_SUCCESS) ? backupMenu : MENU_MAX;
	}
	MENU_LOG("appAddKeysFlag:%d, menu:%d\r\n", appAddKeysFlag, menu);
	return menu;
}

/**
 * @brief  删除指纹模板 延迟删除;
 *	删除指纹模板时，操作flash会关中断;
 * 这时候3090发过来的数据有可能会丢失;
 *
 * @note
 */

static uint8_t del_finger_num = 0;
static void Finger_DeleteImageCb(TimerHandle_stu_t handle)
{
	Finger_DeleteImage(del_finger_num, 1);
}

/**
 * @brief  删除密钥功能逻辑处理
 *
 * @note
 */
static MenuType_enum_t Menu_DelKeys(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType)
{
	uint8_t keysType, keysNumber = 0XFF;
	UsersNumStatus_enum_t res;
	uint8_t input_number = 0;


	switch (menuCurrent)
	{
	case MENU_DEL_PWD:
		keysType = USERS_TYPE_PWD;
		break;
	case MENU_DEL_FPT:
		keysType = USERS_TYPE_FPT;
		break;
	case MENU_DEL_CARD:
		keysType = USERS_TYPE_CARD;
		break;
	case MENU_INPUT_NUMBER_DEL_KEYS:
		keysType = USERS_TYPE_PWD;//
		break;
	default:
		return Menu_GetFatherMenu(menuCurrent);
	}
	if (inputType == INPUT_TYPE_CHAR && dataLen == 3 && MENU_INPUT_NUMBER_DEL_KEYS == menuCurrent)  // 按编号删除
	{
		
		MENU_LOG("pDataBuffer[0]:%d \r\n", pDataBuffer[0]);
		MENU_LOG("pDataBuffer[1]:%d \r\n", pDataBuffer[1]);
		MENU_LOG("pDataBuffer[2]:%d \r\n", pDataBuffer[2]);

		input_number = pDataBuffer[0]*100 + pDataBuffer[1]*10 + pDataBuffer[2];
		MENU_LOG("input_number:%d\r\n", input_number);

		keysNumber = Menu_InputData2KeysNumber(pDataBuffer, dataLen); //将输入的数据转换成密钥编号

		if(input_number >= AUDIO_PLAY_PWD_NUM_START && input_number < AUDIO_PLAY_FPT_NUM_START) //001-050 密码
        {	
			MENU_LOG("InputKeysNumber:%d\r\n", keysNumber);
			keysNumber -= AUDIO_PLAY_PWD_NUM_START; // 减去虚增编号
			
		}
		else if(input_number >= AUDIO_PLAY_FPT_NUM_START && input_number < AUDIO_PLAY_CARD_NUM_START) // 051-100 指纹
		{
			keysType = USERS_TYPE_FPT;
			inputType = INPUT_TYPE_FP;  // 切换输入类型为指纹
			keysNumber -= AUDIO_PLAY_FPT_NUM_START; // 减去虚增编号
		}
		else if(input_number >= AUDIO_PLAY_CARD_NUM_START && input_number <= AUDIO_PLAY_CARD_NUM_END)  // 101-150 卡片
		{
			keysType = USERS_TYPE_CARD;
			// input_number -101
			SYS_CALL(MC60_Send_Del_Id_Nfc, input_number - AUDIO_PLAY_CARD_NUM_START);
		}
		else
		{
			Audio_Play(SHU_RU_CUO_WU, RESET);
			Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
			MENU_LOG("input_number 000 or more than 150 to this:%d\r\n", input_number);
			return menuCurrent;
		}

		res = SYS_CALL(Users_KeyNumIsExist, keysType, keysNumber);
		if (res == USERS_NUMBER_IDLE) //编号不存在
		{
			Audio_Play(BIAN_HAO_BU_CUN_ZAI, RESET);
			Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
			Oled_PopUpDisplay(POPUPS_NumNotExist, POPUPS_NULL, POP_UP_WINDOW_TIME);
			//弹窗 UI
			return menuCurrent;
		}
		else if (res == USERS_NUMBER_ERROR && keysType != USERS_TYPE_CARD)
		{
			Audio_Play(SHU_RU_CUO_WU, RESET);
			Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
			Oled_PopUpDisplay(POPUPS_InputError, POPUPS_NULL, POP_UP_WINDOW_TIME);
			Oled_PopUpDisplay(POPUPS_PleaseEnterAgain, POPUPS_NULL, POP_UP_WINDOW_TIME);
			//弹窗 UI
			return menuCurrent; // menuNext;
		}

	}
	else if (inputType == INPUT_TYPE_CHAR)
	{
		keysNumber = SYS_CALL(Users_PwdIsExist, pDataBuffer, dataLen);
		if (keysNumber >= SYS_CALL(Users_GetKeysQty, USERS_TYPE_PWD))
		{
			Audio_Play(SHAN_CHU_SHI_BAI, RESET); //密码不存在
			// TODO: Audio_Play(MI_MA_BU_CUN_ZAI, RESET); //密码不存在
			Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
			// Oled_PopUpDisplay(POPUPS_CardNotExist, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return menuCurrent; // Menu_GetFatherMenu(menuCurrent);
		}
	}
	else if (inputType == INPUT_TYPE_FP)
	{
		keysNumber = *pDataBuffer;
		keysType = USERS_TYPE_FPT;
		res = SYS_CALL(Users_KeyNumIsExist, USERS_TYPE_FPT, keysNumber); // keysType
		if (res == USERS_NUMBER_IDLE || USERS_NUMBER_ERROR == res) //编号不存在
		{
			MENU_LOG("SHAN_CHU_SHI_BAI 001\n");
			Audio_Play(SHAN_CHU_SHI_BAI, RESET); //指纹不存在
			Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
			Oled_PopUpDisplay(POPUPS_FPNotExist, POPUPS_NULL, POP_UP_WINDOW_TIME);
			return menuCurrent; // Menu_GetFatherMenu(menuCurrent);
		}
	}
	else if (inputType == INPUT_TYPE_CARD)
	{
		// keysNumber = SYS_CALL(Users_CardIsExist, pDataBuffer, dataLen);
		// if (keysNumber >= SYS_CALL(Users_GetKeysQty, USERS_TYPE_CARD))
		// {
		// 	Audio_Play(SHAN_CHU_SHI_BAI, RESET); //卡片不存在
		// 	Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		// 	Oled_PopUpDisplay(POPUPS_CardNotExist, POPUPS_NULL, POP_UP_WINDOW_TIME);
		// 	return menuCurrent; // Menu_GetFatherMenu(menuCurrent);
		// }
	}
	else if (inputType == INPUT_TYPE_RETURN)
	{
		return Menu_GetFatherMenu(menuCurrent);
	}
	uint8_t cardQty = 0, fptQty = 0, cnt = 0, del = 0;
	cardQty = SYS_CALL(Users_GetQty, USERS_TYPE_CARD, 0);
	fptQty = SYS_CALL(Users_GetQty, USERS_TYPE_FPT, 0);
	cnt = cardQty + fptQty;
	if (SYS_CALL(Users_GetSafeMode) == SET)
	{
		if (cnt > 1)
			del = 1;
		if (keysType == USERS_TYPE_PWD)
			del = 1;
	}
	else
	{
		del = 1;
	}
	if (del == 1)
	{
		int num  = -1;
		if (inputType == INPUT_TYPE_CHAR && dataLen != 3 && menuCurrent == MENU_DEL_PWD)
		{
			num = SYS_CALL(Users_Del, USERS_TYPE_PWD, 0XFF, pDataBuffer, dataLen);
		}
		else //if (inputType == INPUT_TYPE_FP)//---------  编号
		{
		 	num = SYS_CALL(Users_Del, keysType, keysNumber, NULL, 0);
			MENU_LOG(" delete finger %d keysType %d num %d\n", keysType, keysNumber, num);
		 	// num = SYS_CALL(Users_Del, USERS_TYPE_FPT, keysNumber, NULL, 0);
		}
		// else  //if (inputType == INPUT_TYPE_CARD)//
		// {
		// 	//删除单个卡片
		// 	SYS_CALL(MC60_Del_Single_Nfc);
		// }
		//SYS_CALL(MC60_Del_Single_Nfc);

		//MENU_LOG("keysType:%d \r\n", keysType);
		//MENU_LOG("input_number:%d \r\n", input_number);
		//if( input_number >= 1 && input_number <= 100) 
	  	//{
			if ((uint8_t)num == keysNumber || (menuCurrent == MENU_DEL_PWD && num >= 0))
			{
				MENU_LOG(">>>>>>>>>>>SHAN_CHU_CHENG_GONG 001\n");
				if (keysType != USERS_TYPE_CARD)//卡片的处理放在MC60
				{
					Audio_Play(SHAN_CHU_CHENG_GONG, RESET); //删除成功
				}
				Oled_PopUpDisplay(POPUPS_DelSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
				Local_LockSaveRecord(EVENT_TYPE_PROGRAM, keysType, EVENT_CODE_DELETE, keysNumber);
				if (keysType == USERS_TYPE_FPT)
				{
					del_finger_num = keysNumber;
					OSAL_TimerCreate(Finger_DeleteImageCb, 300, RESET);//TODO:串口指纹是否可以直接发送命令? 
				}
				return (Menu_IsKeysLibEmpty(menuCurrent) == SUCCESS) ? MENU_USER_SET : menuCurrent; // Menu_GetFatherMenu(menuCurrent);
			}
			else
			{
				MENU_LOG("SHAN_CHU_SHI_BAI 002\n");
				if (keysType != USERS_TYPE_CARD)//卡片的处理放在MC60
				{
					Audio_Play(SHAN_CHU_SHI_BAI, RESET); //删除失败
				}
				Oled_PopUpDisplay(POPUPS_DelFailed, POPUPS_PleaseEnterAgain, POP_UP_WINDOW_TIME);
				return menuCurrent;
			}
	  	//}
	}
	else
	{
		Audio_Play(SHAN_CHU_SHI_BAI_WEI_AN_QUAN_MO_SHI, RESET);
		Oled_PopUpDisplay(POPUPS_DelFailed, POPUPS_ForSafeMode, POP_UP_WINDOW_TIME);
		return MENU_USER_SET;
	}
}

/**
 * @brief  编号输入逻辑处理（添加密钥时）
 *
 * @note
 */
static MenuType_enum_t Menu_AddKeysInputNumber(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType)
{
	uint8_t keysType;
	UsersNumStatus_enum_t res;
	if (inputType == INPUT_TYPE_RETURN)
	{
		return Menu_GetFatherMenu(menuCurrent);
	}

	inputKeysNumber = Menu_InputData2KeysNumber(pDataBuffer, dataLen); //将输入的数据转换成密钥编号
	MENU_LOG("InputKeysNumber:%d\r\n", inputKeysNumber);
	switch (menuNext)
	{
	case MENU_ADD_PWD:
		keysType = USERS_TYPE_PWD;
		break;
	case MENU_ADD_FPT:
		keysType = USERS_TYPE_FPT;
		break;
	case MENU_ADD_CARD:
		keysType = USERS_TYPE_CARD;
		break;
	case MENU_ADD_ADMIN_PWD:
		keysType = USERS_TYPE_PWD;
		break;
	case MENU_ADD_ADMIN_FPT:
		keysType = USERS_TYPE_FPT;
		break;
	default:
		return menuCurrent;
	}
	res = SYS_CALL(Users_KeyNumIsExist, keysType, inputKeysNumber);
	MENU_LOG("keysType :%d, res:%d\r\n", keysType, res);
	if (res == USERS_NUMBER_IDLE) //密钥编号未占用
	{
		return menuNext;
	}
	else if (res == USERS_NUMBER_EXISTED) //密钥编号已占用
	{
		Audio_Play(BIAN_HAO_YI_CUN_ZAI, RESET);
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		Oled_PopUpDisplay(POPUPS_NumExist, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Oled_PopUpDisplay(POPUPS_PleaseEnterAgain, POPUPS_NULL, POP_UP_WINDOW_TIME);
		//弹窗 UI
	}
	else //非法密钥编号
	{
		Audio_Play(SHU_RU_CUO_WU, RESET);
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		Oled_PopUpDisplay(POPUPS_InputError, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Oled_PopUpDisplay(POPUPS_PleaseEnterAgain, POPUPS_NULL, POP_UP_WINDOW_TIME);
		//弹窗 UI
	}
	return menuCurrent;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  菜单切换逻辑处理
 * @note
 * @param  cur_menu：当前菜单
 * @param  enter_menu：要进入的菜单
 *
 * @return 返回跳转后的菜单编号
 */
static MenuType_enum_t Menu_MenuSwitchProcess(MenuType_enum_t cur_menu, MenuType_enum_t enter_menu)
{
	if (enter_menu == MENU_ADD_PWD || enter_menu == MENU_ADD_FPT || enter_menu == MENU_ADD_CARD || enter_menu == MENU_ADD_ADMIN_PWD || enter_menu == MENU_ADD_ADMIN_FPT)
	{
		if (Menu_IsKeysLibFull(enter_menu) == SUCCESS) //检查能不能进入添加密钥菜单
		{
			return cur_menu; //已满：添加失败
		}
		else if (cur_menu == MENU_USER_SET && appAddKeysFlag == RESET)
		{
			menuNext = enter_menu;
			return MENU_INPUT_NUMBER_ADD_KEYS;
		}
		 else if (enter_menu == MENU_ADD_PWD)
		 {
			// autoKeysNumPwd = SYS_CALL(Users_GetValidId, USERS_TYPE_PWD);
			
        	//autoKeysNumFpt = SYS_CALL(Users_GetValidId, USERS_TYPE_FPT);
        	//autoKeysNumCard = SYS_CALL(Users_GetValidId, USERS_TYPE_CARD);

		 }
		 else if (enter_menu == MENU_ADD_FPT)
		 {
			inputKeysNumber = SYS_CALL(Users_GetValidId, USERS_TYPE_FPT);

		 }
	}
	else if (enter_menu == MENU_DEL_PWD || enter_menu == MENU_DEL_FPT || enter_menu == MENU_DEL_CARD || enter_menu == MENU_DEL_ALL_PWD || enter_menu == MENU_DEL_ALL_FPT || enter_menu == MENU_DEL_ALL_CARD)
	{
		if (enter_menu == MENU_DEL_PWD )
		{
			SYS_CALL(MC60_Del_Single_Nfc);
		}
		// else if  (enter_menu == MENU_DEL_ALL_PWD )
		// {
		// 	MENU_LOG("MC60_Del_All_Nfc001\n");
		// 	SYS_CALL(MC60_Del_All_Nfc);
		// }
		
		if (Menu_IsKeysLibEmpty(enter_menu) == SUCCESS) //检查能不能进入删除密钥菜单
		{
			return cur_menu; //为空：删除失败
		}
		else if (cur_menu == MENU_USER_SET)
		{
			menuNext = enter_menu;
			//取消删除单个NFC
			//SYS_CALL(MC60_Del_Single_Nfc_Cancel);//
			return MENU_DEL_SELECT;
		}
	}
	else if (enter_menu == MENU_MODIFY_ADMIN)
	{
		inputKeysNumber = USERS_ID_ADMIN;
	}

	if (enter_menu == MENU_ADD_FPT)
	{
		Finger_WorkModeSet(FINGER_WORK_MODE_ADD, inputKeysNumber); // TODO 切换指纹任务工作模式  inputKeysNumber
	}
	else if (enter_menu == MENU_ADD_CARD)
	{
		//SYS_CALL(Card_SetWorkMode, CARD_MODE_ADD); // TODO 切换卡片任务工作模式
		//发添加卡片指令
		SYS_CALL(MC60_Add_Single_Nfc);
	}
	else if (cur_menu == MENU_ADD_FPT && enter_menu != MENU_ADD_FPT)
	{
		Finger_WorkModeSet(FINGER_WORK_MODE_VERIFY, 0XFF); // TODO 指纹改成验证模式
	}
	else if (cur_menu == MENU_ADD_CARD && enter_menu != MENU_ADD_CARD)
	{
		//SYS_CALL(Card_SetWorkMode, CARD_MODE_VERIFY); // TODO 切换卡片任务工作模式
		//发取消添加卡片指令
		SYS_CALL(MC60_Add_Single_Nfc_Cancel);
	}
	if (cur_menu == MENU_DEL_PWD && enter_menu != MENU_DEL_PWD)
	{
		//取消删除单个NFC
		SYS_CALL(MC60_Del_Single_Nfc_Cancel);//
	}

	/* 进入防伪码菜单 */
	if (enter_menu == MENU_CODE && cur_menu != MENU_CODE)
	{
		uint8_t code[32] = {0};
		OSAL_NvRead(SYS_OFFSET(systemCode), code, 32);
		if (strlen((char *)code) <= 0) //未刷防伪码
		{
			// FuncNetwork_SetNetworkDeviceStatus(NET_DEV_ZGB, NETWORK_DEV_STA_PROGRAM);//发消息给zigbee任务，进入烧防伪码模式
			Audio_Play(QING_SHUA_FANG_WEI_MA, RESET);
			Oled_PopUpDisplay(POPUPS_ScanCode, POPUPS_NULL, POP_UP_WINDOW_TIME);
			enter_menu = cur_menu;
		}
		else
		{
			Audio_PlayString((char *)code); //播放防伪码
			memset(inputBox_data.buffer, 0, 32);
			memcpy(inputBox_data.buffer, code, 32);
#if defined(OLED_FUNCTION)

#else
			enter_menu = cur_menu;
#endif
		}
	}
	/* 退出防伪码菜单 */
	else if (enter_menu != MENU_CODE && cur_menu == MENU_CODE)
	{
		// FuncNetwork_SetNetworkDeviceStatus(NET_DEV_ZGB, NETWORK_DEV_STA_OPEN);//发消息给zigbee任务，进入正常模式
	}

	/* 虚拟菜单：根据实际情况切换 */
	if (enter_menu == MENU_BLE)
	{
		// NvSystemParam_stu_t sysParam;
		// FuncNvMan_ReadSystemParam(&sysParam);
		// enter_menu = (sysParam.bluetoothStatus != BLE_POWER_OFF && (ble_off == 0)) ? MENU_BLE_SET : MENU_BLE_ON;
	}
	else if (enter_menu == MENU_DEL_ONE_KEYS)
	{
		enter_menu = menuNext;
	}

#if defined(OLED_FUNCTION)
	if (enter_menu == MENU_TIME_SET || enter_menu == MENU_DATE_SET)
	{
		uint8_t time[6] = {0};
		uint32_t timestamp = 0;
		OSAL_TimeGet(&timestamp, T_UTC);
		if (enter_menu == MENU_TIME_SET)
			timestamp += 28800; // UTC时间+8小时 显示中国时区时间
		OSAL_UTC2Time(timestamp, time);
		if (enter_menu == MENU_TIME_SET)
		{
			timeSetBox1_data.buffer[0] = time[3] / 10;
			timeSetBox1_data.buffer[1] = time[3] % 10;
			timeSetBox1_data.buffer[2] = time[4] / 10;
			timeSetBox1_data.buffer[3] = time[4] % 10;
			timeSetBox1_data.buffer[4] = time[5] / 10;
			timeSetBox1_data.buffer[5] = time[5] % 10;
		}
		else
		{
			dateSetBox1_data.buffer[0] = time[0] / 10;
			dateSetBox1_data.buffer[1] = time[0] % 10;
			dateSetBox1_data.buffer[2] = time[1] / 10;
			dateSetBox1_data.buffer[3] = time[1] % 10;
			dateSetBox1_data.buffer[4] = time[2] / 10;
			dateSetBox1_data.buffer[5] = time[2] % 10;
		}
	}
	else if (enter_menu == MENU_ORDER_RECORD)
	{
		uint16_t record_qty = SYS_CALL(Record_GetQty, 0, RECORD_MAX_QTY, 0XFF);
		recordListDataBox1.len = record_qty; //获取光标最大值
		MENU_LOG("recordListDataBox1.len:%d\r\n", recordListDataBox1.len);
		recordListDataBox1.current = 0;
		if (recordListDataBox1.len == 0xFFFF)
		{
			//弹窗编号不存在
			return cur_menu; //返回当前菜单
		}
		if (g_inputThreeNumber != 0)
		{
			if (g_inputThreeNumber < record_qty)
			{
				recordListDataBox1.current = g_inputThreeNumber;
				g_inputThreeNumber = 0;
			}
			else
			{
				//弹框编号不存在
				g_inputThreeNumber = 0;
				Audio_Play(BIAN_HAO_BU_CUN_ZAI, RESET);
				Oled_PopUpDisplay(POPUPS_NumNotExist, POPUPS_NULL, POP_UP_WINDOW_TIME);
				return MENU_INPUT_NUMBER_RECORD;
			}
		}
	}
#endif
	return enter_menu;
}

/**
 * @brief  选择框光标移动
 * @note   根据输入的按键，来移动选择框的光标
 *
 * @param  pCur：选择框光标变量地址
 * @param  len：选择框长度
 * @param  pSelectData：选择框数据地址
 * @param  key：当前按下的键值
 *
 * @return 选中了对应的条目返回SET，否则返回RESET
 */
static FlagStatus Menu_SelectBoxCurrentMove(uint16_t *pCur, uint8_t len, SelectBoxData_stu_t *const pSelectData, char key)
{
	uint8_t i;
	Oled_BreakPopUpDisplay();
#ifdef OLED_FUNCTION
	if (key == '#')
	{
		return SET;
	}
	else if (key == '0')
	{
		*pCur = (*pCur < len - 1) ? (*pCur + 1) : 0;
	}
	else if (key == '8')
	{
		*pCur = (*pCur > 0) ? (*pCur - 1) : (len - 1);
	}
	else
	{
#endif
		for (i = 0; i < len; i++)
		{
			if (pSelectData[i].item == key)
			{
				*pCur = i;
				return SET;
			}
		}
		/* 输入错误，请重新输入 */
		Audio_Play(SHU_RU_CUO_WU, RESET);
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		Oled_PopUpDisplay(POPUPS_InputError, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Oled_PopUpDisplay(POPUPS_PleaseEnterAgain, POPUPS_NULL, POP_UP_WINDOW_TIME);
#ifdef OLED_FUNCTION
	}
#endif
	return RESET;
}

/**
 * @brief  选择框处理
 * @note   用户操作了触摸键盘，处理选择框逻辑
 *
 * @param  pBox：选择框指针
 * @param  key：按键值
 */
static void Menu_SelectBoxProcess(BoxData_stu_t *const pBox, char key)
{
	SelectBoxData_stu_t *const pSelectBoxData = (SelectBoxData_stu_t *)pBox->data;
	MenuType_enum_t menu;

	if (Menu_SelectBoxCurrentMove(&(pBox->current), pBox->len, pSelectBoxData, key) == SET)
	{
		// MENU_LOG("item: %s   %s\r\n", pSelectBoxData[pBox->current].ui[0], pSelectBoxData[pBox->current].ui[1]);

		/* 执行条目功能、并切换菜单 */
		if (pSelectBoxData[pBox->current].pFun != NULL)
		{
			menuDelAll = (MenuType_enum_t)pSelectBoxData[pBox->current].menu;
			pSelectBoxData[pBox->current].pFun();
		}

		if (pSelectBoxData[pBox->current].menu < MENU_MAX)
		{
			menu = (MenuType_enum_t)pSelectBoxData[pBox->current].menu; //跳转该条目对应菜单
		}
		else
		{
			menu = Menu_GetFatherMenu(menuCurrent); //跳转到上一个菜单
		}
		Menu_SwitchMenu(menu);
		pBox->current = 0;
	}
	else
	{
		/* 光标移动：刷新UI（打断方式）、播放当前条目语音 */
		//刷新UI  Leo

#ifdef OLED_FUNCTION
		Audio_Play(pSelectBoxData[pBox->current].audio[0], RESET);
		Audio_Play(pSelectBoxData[pBox->current].audio[1], RESET);
#else
		Menu_SwitchMenu(menuCurrent);
#endif
	}
}

/**
 * @brief  输入框处理
 * @note
 *
 * @param  pBox：选择框指针
 * @param  key：按键值
 */
static void Menu_InputBoxProcess(BoxData_stu_t *const pBox, char key)
{
	const MenuFunction_fun_t menuFunction = menuDataList[(int)menuCurrent].pFunction;
	InputBoxData_stu_t *const pInputBoxData = (InputBoxData_stu_t *)pBox->data;
	MenuType_enum_t menu;

	if (key == '#') //确认键
	{
		if (menuFunction != NULL)
		{
			menu = menuFunction(pInputBoxData->buffer, pBox->current, INPUT_TYPE_CHAR);
			if (menu != MENU_MAX)
			{
				Menu_SwitchMenu(menu);
			}
			pBox->current = 0;
		}
	}
	else //数字键
	{
		if (pBox->current < pBox->len && pBox->current < INPUT_BUFFER_SIZE)
		{
			pInputBoxData->buffer[pBox->current++] = key - '0';
			Oled_BreakPopUpDisplay();
		}
		else
		{
#if defined(OLED_FUNCTION)
			if (menuCurrent == MENU_TIME_SET || menuCurrent == MENU_DATE_SET)
			{
				pBox->current = 0;
				pInputBoxData->buffer[pBox->current++] = key - '0';
			}
			else
#endif
			{
				pBox->current = 0;
				Audio_Play(SHU_RU_CUO_WU, RESET);
				Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
				Oled_PopUpDisplay(POPUPS_InputError, POPUPS_NULL, POP_UP_WINDOW_TIME);
				Oled_PopUpDisplay(POPUPS_PleaseEnterAgain, POPUPS_NULL, POP_UP_WINDOW_TIME);
			}
		}
		//刷新UI（打断方式）
	}
}

/**
 * @brief  处理菜单返回逻辑
 *
 * @note
 */
static void Menu_ReturnProcess(BoxData_stu_t *const pBox)
{
	const MenuFunction_fun_t menuFunction = menuDataList[(int)menuCurrent].pFunction;
	MenuType_enum_t menu = menuDataList[(int)menuCurrent].menu_father;

	if (pBox != NULL)
	{
		if (pBox->type == BOX_TYPE_INPUT && pBox->current > 0)
		{
			MENU_LOG("Clear input\r\n");
			pBox->current = 0;
			if (menuCurrent == MENU_MASTER)
			{
				asteriskKeyTimes = 1;
			}
			Oled_BreakPopUpDisplay();
			Menu_SwitchMenu(menuCurrent);
			return;
		}
	}

#if defined(OLED_FUNCTION)
	//日期时间设置在没输入数据的情况下不需要调用该设置的menuFunction函数
	if (menuCurrent == MENU_TIME_SET || menuCurrent == MENU_DATE_SET)
	{
	}
	else
#endif
	{
		if (menuFunction != NULL)
		{
			menu = menuFunction(NULL, 0, INPUT_TYPE_RETURN);
		}
	}
	Oled_BreakPopUpDisplay();
	Menu_SwitchMenu(menu);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  用户输入处理
 * @note
 *
 * @param  type：输入类型
 * @param  pData：数据
 * @param  len：数据长度
 */
void Menu_UserInputProcess(InputDataType_enum_t inputType, uint8_t *pData, uint8_t len)
{
	BoxData_stu_t *const pBox = menuDataList[(int)menuCurrent].box[0];
	const MenuFunction_fun_t menuFunction = menuDataList[(int)menuCurrent].pFunction;

	if (inputType == INPUT_TYPE_CHAR && *pData == '*')
	{
		Menu_ReturnProcess(pBox); //返回逻辑处理
	}
	else if (inputType == INPUT_TYPE_CHAR && pBox != NULL)
	{
		if (pBox->type == BOX_TYPE_SELECT)
		{
			Menu_SelectBoxProcess(pBox, *pData); //选择框逻辑处理
		}
		else if (pBox->type == BOX_TYPE_INPUT)
		{
			Menu_InputBoxProcess(pBox, *pData); //输入框逻辑处理
		}
		else if (pBox->type == BOX_TYPE_LIST) //记录列表输入框处理
		{
			Menu_ListBoxProcess(pBox, *pData);
		}
		else
		{
			Oled_BreakPopUpDisplay();
			/* 该菜单没有输入框、选择框：按了键盘就播放当前菜单提示音 */
			Audio_Play(menuDataList[(int)menuCurrent].audio[0], RESET);
			Audio_Play(menuDataList[(int)menuCurrent].audio[1], RESET);
		}
	}
	else if (inputType == INPUT_TYPE_FP || inputType == INPUT_TYPE_CARD)
	{
		Oled_BreakPopUpDisplay();
		if (menuFunction != NULL) //执行菜单功能回调
		{
			Menu_SwitchMenu(menuFunction(pData, len, inputType));
		}
		if (pBox != NULL)
		{
			pBox->current = 0;
		}
	}
	/* 更新系统锁定状态 */
	Menu_GetSystemLockedStatus();
}

/**
 * @brief  获取当前菜单编号
 * @note
 *
 * @return 菜单编号
 */
MenuType_enum_t Menu_GetCurrentMenu(void)
{
	return menuCurrent;
}
/**
 * @brief  获取App添加标识
 * @note
 *
 * @return App添加标识
 */
FlagStatus Menu_GetAppAddFlag(void)
{
	return appAddKeysFlag;
}

/**
 * @brief  获取指定菜单的父菜单（节点）
 * @note
 * @param  menu：指定的菜单
 *
 * @return 父菜单编号
 */
MenuType_enum_t Menu_GetFatherMenu(MenuType_enum_t menu)
{
	if (menu >= MENU_MAX)
	{
		return MENU_MASTER;
	}
	if (menuDataList[(int)menu].menu_father >= MENU_MAX)
	{
		return MENU_MASTER;
	}
	return menuDataList[(int)menu].menu_father;
}

/**
 * @brief  获取密钥验证记录
 * @note
 */
void Menu_GetVerifyRecord(EventRecord_stu_t *pRecord)
{
	*pRecord = verifyRecord;
}

/**
 * @brief  退出 系统锁定状态
 *
 * @note
 */
void Menu_ExitSystemLocked(void)
{
	if (verifyFailCount >= VERIFY_FAIL_NUM)
	{
		//! 取消LED闪烁显示
		Led_Set("LED2", LED_FLASH, 0, 0);
		// Led_Set("LED2", LED_ON, 0XFF, 0);
	}
	verifyFailCount = 0;
	masterMenuBox1.state = ENABLE;
	masterMenuBox2.state = ENABLE;
	masterMenuBox3.state = DISABLE;
	OSAL_NvWrite(SYS_OFFSET(verifyErrCount), &verifyFailCount, sizeof(verifyFailCount));
}

/**
 * @brief  获取系统锁定状态
 * @note
 *
 * @param  type：输入的设备类型
 *               如果该设备输入错误次数超过10次，表示正处于锁定状态
 * @return 返回锁定剩余时间
 */
uint8_t Menu_GetSystemLockedStatus(void)
{
	uint32_t timestamp = 0;
	MENU_LOG("verifyFailCount:%d\r\n", verifyFailCount);
	if (verifyFailCount >= VERIFY_FAIL_NUM)
	{
		OSAL_TimeGet(&timestamp, T_UTC);
		MENU_LOG("timestamp:%d,systemLockedTimestamp:%d\r\n", timestamp, systemLockedTimestamp);
		if (timestamp >= systemLockedTimestamp)
		{
			Menu_ExitSystemLocked();
			return 0;
		}
		else
		{
			return (systemLockedTimestamp - timestamp) / 60 + 1;
		}
	}
	return 0;
}

/**
 * @brief  处理验证失败
 * @note
 *
 */
uint8_t Menu_ProcessVerifyFailed(void)
{
	uint32_t timestamp;

	//shortKeyLockStudy = RESET;
	inputDualFlag = RESET;
	Led_Set("LED", LED_OFF, 0, 0);
	Led_Set("LED13579", LED_FLASH, 2, 100);
	Led_Set("LED_PWM_R", LED_FLASH, 2, 100);
	Led_Set("LED_LOCK", LED_ON, 0, 0);
	Finger_LedControl(FINGER_FLASHING_LED_MODE, FINGER_LED_RED, FINGER_LED_RED, LED_COUNTS); // 失败： 闪红灯1次
	Oled_PopUpDisplay(POPUPS_VerifyFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	Audio_Play(YAN_ZHENG_SHI_BAI_2, RESET);
	Led_Set("LED_OPEN", LED_OFF, 0, 0);
	Led_Set("LED_CLOSE", LED_FLASH, 1, 2000);
	OSAL_TimeGet(&timestamp, T_UTC);
	if (timestamp > verifyFailTimestamp + 60) //300
	{
		verifyFailCount = 0; // 5分钟后清零错误计数
	}
	verifyFailTimestamp = timestamp;
	verifyFailCount++;
	OSAL_NvWrite(SYS_OFFSET(verifyErrCount), &verifyFailCount, sizeof(verifyFailCount));
	// if (verifyFailCount >= 3)
	// {
	// 	Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET);
	// }
	if (verifyFailCount >= VERIFY_FAIL_NUM)
	{
		Menu_EnterSystemLocked();
	}
	MENU_LOG("verifyFailCount:%d\r\n", verifyFailCount);
}

/* 当前菜单BOX指针 */
static BoxData_stu_t *pCurrentMenuBox = NULL;
static FlagStatus continueFlag = SET;

void Menu_PlayMenuAudioList(uint16_t num)
{
	static uint8_t play_return_menu = 0;
	MENU_LOG("audio Num:%d\r\n\r\n\r\n", num);
	if (pCurrentMenuBox->type != BOX_TYPE_SELECT || continueFlag == RESET)
	{
		return;
	}
	num = num % AUDIO_LANGUAGE_ADDR_OFFSET;
	SelectBoxData_stu_t *pSelectBoxData = (SelectBoxData_stu_t *)(pCurrentMenuBox->data);
	if (pCurrentMenuBox->current < pCurrentMenuBox->len - 1)
	{
		static uint16_t lastAudioNum = 0;
		if (num < YU_YIN_1 || num > YU_YIN_9)
		{
			lastAudioNum = num;
		}
		if (num == pSelectBoxData[pCurrentMenuBox->current].audio[1] && lastAudioNum == pSelectBoxData[pCurrentMenuBox->current].audio[0])
		{
			pCurrentMenuBox->current++; //光标+1，再次播放下一条语音
			//更新UI
			// SelectBoxData_stu_t *pSelectBoxData = (SelectBoxData_stu_t *)(pCurrentMenuBox->data);
			Audio_Play(pSelectBoxData[pCurrentMenuBox->current].audio[0], RESET);
			Audio_Play(pSelectBoxData[pCurrentMenuBox->current].audio[1], RESET);
			MENU_LOG("22audio Num:%d,  %d\r\n\r\n\r\n", pSelectBoxData[pCurrentMenuBox->current].audio[0], pSelectBoxData[pCurrentMenuBox->current].audio[1]);
			MENU_LOG("pCurrentMenuBox->current:%d,pCurrentMenuBox->len:%d\r\n", pCurrentMenuBox->current, pCurrentMenuBox->len - 1);
		}
	}
	else if (num == pSelectBoxData[pCurrentMenuBox->len - 1].audio[1] && play_return_menu == 0)
	{
		MENU_LOG("1111audio Num:%d\r\n\r\n\r\n", num);
		Audio_Play(FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN, RESET);
		play_return_menu = 1;
	}

	if (num == FAN_HUI_SHANG_JI_CAI_DAN_QING_AN_SHANG_SUO_JIAN || num == AN_JIAN_YIN_DI)
	{
		play_return_menu = 0;
	}
}

/**
 * @brief  插入一条语音
 * @note   根据需要，插入一条合适的提示语音
 *
 */
void Menu_InsertMenuAudio(uint16_t playAudio)
{
	uint16_t audio = playAudio % AUDIO_LANGUAGE_ADDR_OFFSET; 

	switch (audio)
	{
		case QING_XIU_GAI_GUAN_LI_MI_MA:
		case QING_SHU_RU_BIAN_HAO:
		case QING_SHU_RU_XIN_MI_MA:
			break;
		default:
			// 不需要插播 直接返回
			return;
	}

	// 插播语音
	Audio_Play(YI_KAI_SUO_JIAN_JIE_SHU, RESET);
}
/**
 * @brief  刷新一次当前菜单语音
 * @note   每次菜单切换会调用该函数，从头播放一次选择框语音
 *         无选择框的菜单，播放当前菜单提示音
 *
 * @param  pBox：BOX指针
 */
void Menu_UpDateMenuAudio(BoxData_stu_t *pBox, uint16_t *pAudio)
{
	if (pBox == NULL || pAudio == NULL)
	{
		return;
	}
	pCurrentMenuBox = pBox;
	if (pBox->type == BOX_TYPE_SELECT)
	{
		SelectBoxData_stu_t *pSelectBoxData = (SelectBoxData_stu_t *)(pCurrentMenuBox->data);
		/* 播放当前菜单选择框语音 */
		Audio_Play(pSelectBoxData[pCurrentMenuBox->current].audio[0], RESET);
		Audio_Play(pSelectBoxData[pCurrentMenuBox->current].audio[1], RESET);
		pCurrentMenuBox->current = 0;
		continueFlag = SET;
	}
	else
	{
		if (pAudio[0] != 0 || pAudio[1] != 0)
		{
			/* 播放当前菜单提示音 */
			Audio_Play(pAudio[0], RESET);
			// if ((pAudio[0] % AUDIO_LANGUAGE_ADDR_OFFSET) == QING_XIU_GAI_GUAN_LI_MI_MA)
			// {
			// 	// 插播语音
			// 	Audio_Play(YI_KAI_SUO_JIAN_JIE_SHU, RESET);
			// }
			Menu_InsertMenuAudio(pAudio[0]);
			Audio_Play(pAudio[1], RESET);
		}
	}
}

/**
 * @brief  菜单切换函数
 * @note
 * @param  menu：需要进入的菜单
 */
void Menu_SwitchMenu(MenuType_enum_t menu)
{
	FlagStatus reprotFlag = RESET;

	menu = Menu_MenuSwitchProcess(menuCurrent, menu);
	if (menu >= MENU_MAX)
	{
		return;
	}

	if (menuCurrent != menu)
	{
		if (menu == MENU_MASTER)
		{
			/* 退出管理菜单 */
			if (appAddKeysFlag == RESET)
			{
				Audio_Play(YI_TUI_CHU_GUAN_LI_MO_SHI, RESET);
				Oled_PopUpDisplay(POPUPS_ExitMasterMode, POPUPS_NULL, POP_UP_WINDOW_TIME);
				Audio_SetMuteMode(isMuteflag);
				if(isMuteflag == SET)
				{
					Audio_SetVolume(60);
				}
				Audio_SetNotMute(ENTER_MANAGE_NOMUTE_FLAG,RESET);
			}
			else if (appAddKeysResult == RESET)
			{
				Audio_Play(TIAN_JIA_SHI_BAI, RESET);
			}
			reprotFlag = SET;
		}
		else if (menuCurrent == MENU_MASTER)
		{
			/* 进入管理菜单 */
			reprotFlag = SET;
		}

		appAddKeysFlag = RESET; //菜单有变化就把添加标志清除
		appAddKeysResult = RESET;
	}

	menuCurrent = menu;
	MENU_LOG("menuCurrent:%d\r\n", menuCurrent);

	if (menuDataList[(int)menuCurrent].box[0] != NULL)
	{
#ifdef OLED_FUNCTION
		if (menuCurrent != MENU_ORDER_RECORD)
		{
			menuDataList[(int)menuCurrent].box[0]->current = 0; //当前菜单光标清0
		}
#else
		menuDataList[(int)menuCurrent].box[0]->current = 0; //当前菜单光标清0
#endif
	}

	// if (menuCurrent == MENU_DISCONNECT_NETWORK)
	// {
	// 	menuCurrent = menuCurrent;
	// }

	/* 刷新当前菜单语音 */
	Menu_UpDateMenuAudio(menuDataList[(int)menuCurrent].box[0], menuDataList[(int)menuCurrent].audio);
#ifdef OLED_FUNCTION
	Oled_UpDateMenuDisplay(menuDataList[(int)menuCurrent].box);
#endif
	/* 刷新当前菜单UI（非打断方式） */
	// Leo

	/* 进入/退出管理菜单，网络上报 */
	if (reprotFlag != RESET)
	{
		SYS_CALL(Remote_SettingChangeReport);
	}
}

/**
 * @brief  启动app添加密钥
 *
 */
ErrorStatus Menu_StartAppAddKeys(uint8_t keysType, uint8_t keysNum) //, uint8_t keysAttr, NvPloy_stu_t *ploy)
{
#if !defined(CARD_FUNCTION)
	if (keysType == USERS_TYPE_CARD)
	{
		return ERROR;
	}
#endif

	//打断当前的语音播放
	Audio_Play(STOP_PLAY, SET);

	if (keysType == USERS_TYPE_FPT || keysType == USERS_TYPE_CARD || keysType == USERS_TYPE_PWD)
	{
		if (appAddKeysFlag == RESET)
		{
			backupMenu = menuCurrent;
		}
		inputKeysNumber = keysNum;
		switch (keysType)
		{
		case USERS_TYPE_FPT:
			Menu_SwitchMenu(MENU_ADD_FPT);
			break;
		// case USERS_TYPE_PWD:  Menu_SwitchMenu(MENU_ADD_PWD);  break;
		case USERS_TYPE_CARD:
			Menu_SwitchMenu(MENU_ADD_CARD);
			break;
		}
		appAddKeysFlag = SET;
		MENU_LOG("app add keys ok...");
		// appAddKeysAttr = keysAttr;//暂存属性
		// appAddKeysPloy = *ploy;   //暂存策略
		return SUCCESS;
	}
	return ERROR;
}

void Menu_ResetFlag(void)
{
	inputDualFlag = RESET; //两次输入标志
	asteriskKeyTimes = 0;  //星键输入次数
}
/**
 * @brief  显示屏更新电量函数
 *
 */
void Menu_UpdateBattery(int battery)
{
	masterMenuBox4.current = battery & 0xff;
}
void Menu_UpdateSysLanguage(uint8_t Language)
{
	g_SysLanguage = Language;
}

#if defined(OLED_FUNCTION)
/**
 * @brief  设置系统时间
 *
 * @note
 */
static MenuType_enum_t Menu_UpdateSysTime(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType)
{

	uint8_t bcdtime[6] = {0}, bcdtime1[6] = {0};
	OSAL_TimeGet(bcdtime, T_TIME);
	bcdtime[3] = ((pDataBuffer[0]) * 10) + (pDataBuffer[1]);
	bcdtime[4] = ((pDataBuffer[2]) * 10) + (pDataBuffer[3]);
	bcdtime[5] = ((pDataBuffer[4]) * 10) + (pDataBuffer[5]);
	OSAL_TimeSet(bcdtime, T_TIME);
	OSAL_TimeGet(bcdtime1, T_TIME);
	MENU_LOG("%d-%d-%d-%d-%d-%d\r\n", bcdtime1[0], bcdtime1[1], bcdtime1[2], bcdtime1[3], bcdtime1[4], bcdtime1[5]);
	if (bcdtime[3] == bcdtime1[3] && bcdtime[4] == bcdtime1[4] && bcdtime[5] == bcdtime1[5])
	{
		uint32_t timeStamp = 0;
		Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
		Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
		OSAL_TimeGet(&timeStamp, T_UTC);
		timeStamp -= 28800;
		OSAL_TimeSet(&timeStamp, T_UTC);
	}
	else
	{
		Audio_Play(SHE_ZHI_SHI_BAI, RESET);
		Oled_PopUpDisplay(POPUPS_SettingFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	}
	return MENU_SYSTEM_SET;
}

/**
 * @brief  设置系统日期
 *
 * @note
 */
static MenuType_enum_t Menu_UpdateSysDate(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType)
{

	uint8_t bcdtime[6] = {0}, bcdtime1[6] = {0};
	OSAL_TimeGet(bcdtime, T_TIME);
	bcdtime[0] = ((pDataBuffer[0]) * 10) + (pDataBuffer[1]);
	bcdtime[1] = ((pDataBuffer[2]) * 10) + (pDataBuffer[3]);
	bcdtime[2] = ((pDataBuffer[4]) * 10) + (pDataBuffer[5]);
	MENU_LOG("bcdtime   :%d,%d,%d\r\n", bcdtime[0], bcdtime[1], bcdtime[2]);
	MENU_LOG("%d,%d,%d,%d,%d,%d\r\n", pDataBuffer[0], pDataBuffer[1], pDataBuffer[2], pDataBuffer[3], pDataBuffer[4], pDataBuffer[5]);
	OSAL_TimeSet(bcdtime, T_TIME);
	OSAL_TimeGet(bcdtime1, T_TIME);
	MENU_LOG("%d-%d-%d-%d-%d-%d\r\n", bcdtime1[0], bcdtime1[1], bcdtime1[2], bcdtime1[3], bcdtime1[4], bcdtime1[5]);
	if (bcdtime[0] == bcdtime1[0] && bcdtime[1] == bcdtime1[1] && bcdtime[2] == bcdtime1[2])
	{
		Audio_Play(SHE_ZHI_CHENG_GONG, RESET);
		Oled_PopUpDisplay(POPUPS_SettingSucceed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	}
	else
	{
		Audio_Play(SHE_ZHI_SHI_BAI, RESET);
		Oled_PopUpDisplay(POPUPS_SettingFailed, POPUPS_NULL, POP_UP_WINDOW_TIME);
	}
	return MENU_SYSTEM_SET;
}

static MenuType_enum_t Menu_InputThreeNumber(uint8_t *pDataBuffer, uint8_t dataLen, InputDataType_enum_t inputType)
{

	uint16_t temp = 0;
	if (inputType == INPUT_TYPE_RETURN)
	{
		return Menu_GetFatherMenu(menuCurrent);
	}
	if (dataLen != 3)
	{
		Audio_Play(SHU_RU_CUO_WU, RESET);
		Audio_Play(QING_CHONG_XIN_SHU_RU, RESET);
		Oled_PopUpDisplay(POPUPS_InputError, POPUPS_NULL, POP_UP_WINDOW_TIME);
		Oled_PopUpDisplay(POPUPS_PleaseEnterAgain, POPUPS_NULL, POP_UP_WINDOW_TIME);
		return menuCurrent;
	}
	for (; dataLen; dataLen--)
	{
		temp = temp * 10;
		temp = temp + *pDataBuffer++;
	}
	MENU_LOG("InputThreeNumber:%d\r\n", temp);
	g_inputThreeNumber = temp;
	return MENU_ORDER_RECORD;
}

/**
 * @brief  更新记录条数
 * @note   会出现在查询记录时  这个过程中又产生一条记录
 *
 * @param
 * @param
 */
void Menu_UpdateRecordLength(void)
{
	// recordListDataBox1.len =  FuncNvMan_RecordNum2CurrentPos(0xFFFF);
}
#endif

/**
 * @brief  列表框数据处理
 * @note   用户操作了触摸键盘，处理光标上移下移
 *
 * @param  pBox：列表框BOX指针
 * @param  key：按键值
 */
static void Menu_ListBoxProcess(BoxData_stu_t *const pBox, char key)
{
	if (key == '0')
	{
		pBox->current = (pBox->current < pBox->len - 1) ? (pBox->current + 1) : 0;
	}
	else if (key == '8')
	{
		pBox->current = (pBox->current > 0) ? (pBox->current - 1) : (pBox->len - 1);
	}
}

static ListBoxStr_t Menu_RecordListBoxCallback(uint16_t cur, uint16_t last_cur)
{
	EventRecord_stu_t record1, record2, recordTmp;
	static char string[2][4][17];
	uint8_t type1 = 0, type2 = 0, type_tmp1 = 0, type_tmp2 = 0;
	uint16_t num1 = 0, num2 = 0, num_tmp1 = 0, num_tmp2 = 0;
	const char *pListText[][2] =
		{
			"[A] 门锁被撬    ", "[A] Force unlock",	 /*0*/
			"[A] 布防报警    ", "[A] Arming      ",	 /*1*/
			"[A] 劫持报警    ", "[A] Hijack      ",	 /*2*/
			"[A] 机械钥匙报警", "[A] Unlock-KEY  ",	 /*3*/
			"[A] 门锁异常    ", "[A] Lock fault  ",	 /*4*/
			"[A] 系统锁定    ", "[A] Sys locked  ",	 /*5*/
			"[U] 密码        ", "[U] PIN         ",	 /*6*/
			"[U] 卡片        ", "[U] Card        ",	 /*7*/
			"[U] 指纹        ", "[U] FPT         ",	 /*8*/
			"[U] APP         ", "[U] APP         ",	 /*9*/
			"[O] 添加密码    ", "[O] Add pwd     ",	 /*10*/
			"[O] 删除密码    ", "[O] Del pwd     ",	 /*11*/
			"[O] 清除密码库  ", "[O] Del pwd lib ",	 /*12*/
			"[O]修改管理密码 ", "[O] Modify Admin",	 /*13*/
			"[O] 添加卡片    ", "[O] Add card    ",	 /*14*/
			"[O] 删除卡片    ", "[O] Del card    ",	 /*15*/
			"[O] 清除卡片库  ", "[O] Del card lib",	 /*16*/
			"[O] 添加指纹    ", "[O] Add FPT     ",	 /*17*/
			"[O] 删除指纹    ", "[O] Del FPT     ",	 /*18*/
			"[O] 清除指纹库  ", "[O] Del FPT lib ",	 /*19*/
			"[A] 低电报警    ", "[A] Low Power    ", /*20*/
			"[1] 密码        ", "[1] PIN         ",	 /*21*/
			"[1] 卡片        ", "[1] Card        ",	 /*22*/
			"[1] 指纹        ", "[1] FPT         ",	 /*23*/
			"[2] 密码        ", "[2] PIN         ",	 /*24*/
			"[2] 卡片        ", "[2] Card        ",	 /*25*/
			"[2] 指纹        ", "[2] FPT         ",	 /*26*/
		};
	MENU_LOG("\r\n\r\n cur :%d,last_cur %d\r\n", cur, last_cur);
	type1 = SYS_CALL(Record_GetRecordRule, &record1, &num1, last_cur);
	type2 = SYS_CALL(Record_GetRecordRule, &record2, &num2, cur);
	for (int i = 0; i < 2; i++) //两种语言
	{
		uint8_t time[6] = {0};
		if (cur > last_cur) //光标下移
		{
			type_tmp1 = type1; // last_cur
			type_tmp2 = type2; // current
			num_tmp1 = num1;   // last_cur
			num_tmp2 = num2;   // current
		}
		else //光标上移
		{
			type_tmp1 = type2;
			type_tmp2 = type1;
			num_tmp1 = num2;
			num_tmp2 = num1;
		}

		if (cur > last_cur)
			memcpy(&recordTmp, &record1, sizeof(EventRecord_stu_t));
		else
			memcpy(&recordTmp, &record2, sizeof(EventRecord_stu_t));
		OSAL_UTC2Time(recordTmp.timestamp, time);
		MENU_LOG("recordTmp eventType:%d,eventSource:%d,eventCode:%d\r\n", recordTmp.eventType, recordTmp.eventSource, recordTmp.eventCode);
		switch (type_tmp1)
		{
		case EVENT_TYPE_OPEN_CLOSE: //开门记录
		{
			switch (recordTmp.eventSource)
			{
			case USERS_TYPE_PWD:
				memcpy(&string[i][0][0], pListText[6][i], 16);
				break;
			case USERS_TYPE_FPT:
				memcpy(&string[i][0][0], pListText[8][i], 16);
				break;
			case USERS_TYPE_CARD:
				memcpy(&string[i][0][0], pListText[7][i], 16);
				break;
			case USERS_TYPE_APP:
				memcpy(&string[i][0][0], pListText[9][i], 16);
				break;
			default:
				break;
			}

			if (recordTmp.userID >= 100)
			{
				//特殊密钥
				string[i][0][9] = (recordTmp.userID / 100) + '0';
				string[i][0][10] = (recordTmp.userID / 10 % 10) + '0';
				string[i][0][11] = (recordTmp.userID % 10) + '0';
			}
			else
			{
				string[i][0][9] = (recordTmp.userID / 10) + '0';
				string[i][0][10] = (recordTmp.userID % 10) + '0';
			}
		}
		break;
		// case 1://安全模式开门秘钥1
		// {
		// 	// EventRecord_stu_t opendoor;
		// 	//  if(cur > last_cur)  memcpy(&opendoor,&record1.data,sizeof(EventRecord_stu_t));
		// 	//  else  memcpy(&opendoor,&record2.data,sizeof(EventRecord_stu_t));
		// 	// memcpy(time,&opendoor.openTime,6);
		// 	switch(recordTmp.eventSource)
		// 	{
		// 		case USERS_TYPE_PWD:
		// 			memcpy(&string[i][0][0],pListText[21][i],16);
		// 			break;
		// 		case USERS_TYPE_FPT:
		// 			memcpy(&string[i][0][0],pListText[23][i],16);
		// 			break;
		// 		case USERS_TYPE_CARD:
		// 			memcpy(&string[i][0][0],pListText[22][i],16);
		// 			break;
		// 		default:
		// 			break;
		// 	}
		// 	string[i][0][9] = (recordTmp.userID/10)+'0';
		//     string[i][0][10] = (recordTmp.userID%10)+'0';

		// }
		// break;
		// case 2://安全模式开门秘钥2
		// {
		// 	// EventRecord_stu_t opendoor;
		// 	//  if(cur > last_cur)  memcpy(&opendoor,&record1.data,sizeof(EventRecord_stu_t)) ;
		// 	//  else  memcpy(&opendoor,&record2.data,sizeof(EventRecord_stu_t));
		// 	// memcpy(time,&opendoor.openTime,6);
		// 	switch(opendoor.keys2Type)
		// 	{
		// 		case USERS_TYPE_PWD:
		// 			memcpy(&string[i][0][0],pListText[24][i],16);
		// 			break;
		// 		case USERS_TYPE_FPT:
		// 			memcpy(&string[i][0][0],pListText[26][i],16);
		// 			break;
		// 		case USERS_TYPE_CARD:
		// 			memcpy(&string[i][0][0],pListText[25][i],16);
		// 			break;
		// 		default:
		// 			break;
		// 	}
		// 	string[i][0][9] = (recordTmp.userID/10)+'0';
		//     string[i][0][10] = (recordTmp.userID%10)+'0';
		// }
		// break;
		case EVENT_TYPE_PROGRAM:
		{
			switch (recordTmp.eventCode)
			{
			case EVENT_CODE_ADD:
				switch (recordTmp.eventSource)
				{
				case USERS_TYPE_PWD:
					memcpy(&string[i][0][0], pListText[10][i], 16);
					break;
				case USERS_TYPE_FPT:
					memcpy(&string[i][0][0], pListText[17][i], 16);
					break;
				case USERS_TYPE_CARD:
					memcpy(&string[i][0][0], pListText[14][i], 16);
					break;
				default:
					break;
				}
				string[i][0][13] = (recordTmp.userID / 10) + '0';
				string[i][0][14] = (recordTmp.userID % 10) + '0';
				break;
			case EVENT_CODE_DELETE:
				if (recordTmp.userID == 0xff)
				{
					switch (recordTmp.eventSource)
					{
					case USERS_TYPE_PWD:
						memcpy(&string[i][0][0], pListText[12][i], 16);
						break;
					case USERS_TYPE_FPT:
						memcpy(&string[i][0][0], pListText[19][i], 16);
						break;
					case USERS_TYPE_CARD:
						memcpy(&string[i][0][0], pListText[16][i], 16);
						break;
					default:
						break;
					}
					break;
				}
				else
				{
					switch (recordTmp.eventSource)
					{
					case USERS_TYPE_PWD:
						memcpy(&string[i][0][0], pListText[11][i], 16);
						break;
					case USERS_TYPE_FPT:
						memcpy(&string[i][0][0], pListText[18][i], 16);
						break;
					case USERS_TYPE_CARD:
						memcpy(&string[i][0][0], pListText[15][i], 16);
						break;
					default:
						break;
					}
					string[i][0][13] = (recordTmp.userID / 10) + '0';
					string[i][0][14] = (recordTmp.userID % 10) + '0';
					break;
				}

			case EVENT_CODE_MODIFY:
				memcpy(&string[i][0][0], pListText[13][i], 16);
				break;
			default:
				break;
			}
		}
		break;
		case EVENT_TYPE_ALARM:
		{
			switch (recordTmp.eventCode)
			{
			case EVENT_CODE_SYS_LOCK:
				memcpy(&string[i][0][0], pListText[5][i], 16);
				break;
			case EVENT_CODE_HIJACK:
				memcpy(&string[i][0][0], pListText[2][i], 16);
				break;
			case EVENT_CODE_PRY_LOCK:
				memcpy(&string[i][0][0], pListText[0][i], 16);
				break;
			case EVENT_CODE_MECHANIC_KEY:
				memcpy(&string[i][0][0], pListText[3][i], 16);
				break;
			case EVENT_CODE_LOW_POWER:
				memcpy(&string[i][0][0], pListText[20][i], 16);
				break;
			case EVENT_CODE_LOCK_ERR:
				memcpy(&string[i][0][0], pListText[4][i], 16);
				break;
			case EVENT_CODE_ARMING:
				memcpy(&string[i][0][0], pListText[1][i], 16);
				break;
			default:
				break;
			}
		}
		break;
		default:
			break;
		}
		string[i][1][0] = num_tmp1 / 100 + '0';		   //编号/100  +'0';
		string[i][1][1] = (num_tmp1 % 100) / 10 + '0'; //(编号%100)/10  +'0';
		string[i][1][2] = (num_tmp1 % 10) + '0';	   //(编号%10) +'0';
		string[i][1][3] = ' ';
		string[i][1][4] = ((time[1] / 10)) + '0';
		string[i][1][5] = (time[1] % 10) + '0';
		string[i][1][6] = '-';
		string[i][1][7] = ((time[2] / 10)) + '0';
		string[i][1][8] = (time[2] % 10) + '0';
		string[i][1][9] = ' ';
		string[i][1][10] = ((time[3] / 10)) + '0';
		string[i][1][11] = (time[3] % 10) + '0';
		string[i][1][12] = ':';
		string[i][1][13] = ((time[4] / 10)) + '0';
		string[i][1][14] = (time[4] % 10) + '0';
		string[i][1][15] = ' ';

		if (cur > last_cur)
			memcpy(&recordTmp, &record2, sizeof(EventRecord_stu_t));
		else
			memcpy(&recordTmp, &record1, sizeof(EventRecord_stu_t));
		OSAL_UTC2Time(recordTmp.timestamp, time);
		switch (type_tmp2)
		{
		case EVENT_TYPE_OPEN_CLOSE:
		{
			switch (recordTmp.eventSource)
			{
			case USERS_TYPE_PWD:
				memcpy(&string[i][2][0], pListText[6][i], 16);
				break;
			case USERS_TYPE_FPT:
				memcpy(&string[i][2][0], pListText[8][i], 16);
				break;
			case USERS_TYPE_CARD:
				memcpy(&string[i][2][0], pListText[7][i], 16);
				break;
			case USERS_TYPE_APP:
				memcpy(&string[i][2][0], pListText[9][i], 16);
				break;
			default:
				break;
			}
			if (recordTmp.userID >= 100)
			{
				//特殊密钥
				string[i][2][9] = (recordTmp.userID / 100) + '0';
				string[i][2][10] = (recordTmp.userID / 10 % 10) + '0';
				string[i][2][11] = (recordTmp.userID % 10) + '0';
			}
			else
			{
				string[i][2][9] = (recordTmp.userID / 10) + '0';
				string[i][2][10] = (recordTmp.userID % 10) + '0';
			}
		}
		break;
		// case 1:
		// {
		// 	// EventRecord_stu_t opendoor;
		// 	//  if(cur > last_cur) memcpy(&opendoor,&record2.data,sizeof(EventRecord_stu_t)) ;
		// 	//  else  memcpy(&opendoor,&record1.data,sizeof(EventRecord_stu_t));
		// 	// memcpy(time,&opendoor.openTime,6);
		// 	switch(recordTmp.eventSource)
		// 	{
		// 		case USERS_TYPE_PWD:
		// 			memcpy(&string[i][2][0],pListText[21][i],16);
		// 			break;
		// 		case USERS_TYPE_FPT:
		// 			memcpy(&string[i][2][0],pListText[23][i],16);
		// 			break;
		// 		case USERS_TYPE_CARD:
		// 			memcpy(&string[i][2][0],pListText[22][i],16);
		// 			break;
		// 		default:
		// 			break;
		// 	}
		// 	string[i][2][9] = (recordTmp.userID/10)+'0';
		//     string[i][2][10] = (recordTmp.userID%10)+'0';
		// }
		// break;
		// case 2:
		// {
		// 	// EventRecord_stu_t opendoor;
		// 	//  if(cur > last_cur) memcpy(&opendoor,&record2.data,sizeof(EventRecord_stu_t)) ;
		// 	//  else memcpy(&opendoor,&record1.data,sizeof(EventRecord_stu_t));
		// 	// memcpy(time,&opendoor.openTime,6);
		// 	switch(recordTmp.eventSource)
		// 	{
		// 		case USERS_TYPE_PWD:
		// 			memcpy(&string[i][2][0],pListText[24][i],16);
		// 			break;
		// 		case USERS_TYPE_FPT:
		// 			memcpy(&string[i][2][0],pListText[26][i],16);
		// 			break;
		// 		case USERS_TYPE_CARD:
		// 			memcpy(&string[i][2][0],pListText[25][i],16);
		// 			break;
		// 		default:
		// 			break;
		// 	}
		// 	string[i][2][9] = (recordTmp.userID/10)+'0';
		//     string[i][2][10] = (recordTmp.userID%10)+'0';
		// }
		// break;
		case EVENT_TYPE_PROGRAM:
		{
			switch (recordTmp.eventCode)
			{
			case EVENT_CODE_ADD:
				switch (recordTmp.eventSource)
				{
				case USERS_TYPE_PWD:
					memcpy(&string[i][2][0], pListText[10][i], 16);
					break;
				case USERS_TYPE_FPT:
					memcpy(&string[i][2][0], pListText[17][i], 16);
					break;
				case USERS_TYPE_CARD:
					memcpy(&string[i][2][0], pListText[14][i], 16);
					break;
				default:
					break;
				}
				string[i][2][13] = (recordTmp.userID / 10) + '0';
				string[i][2][14] = (recordTmp.userID % 10) + '0';
				break;
			case EVENT_CODE_DELETE:
				if (recordTmp.userID == 0xff)
				{
					switch (recordTmp.eventSource)
					{
					case USERS_TYPE_PWD:
						memcpy(&string[i][2][0], pListText[12][i], 16);
						break;
					case USERS_TYPE_FPT:
						memcpy(&string[i][2][0], pListText[19][i], 16);
						break;
					case USERS_TYPE_CARD:
						memcpy(&string[i][2][0], pListText[16][i], 16);
						break;
					default:
						break;
					}
					break;
				}
				else
				{
					switch (recordTmp.eventSource)
					{
					case USERS_TYPE_PWD:
						memcpy(&string[i][2][0], pListText[11][i], 16);
						break;
					case USERS_TYPE_FPT:
						memcpy(&string[i][2][0], pListText[18][i], 16);
						break;
					case USERS_TYPE_CARD:
						memcpy(&string[i][2][0], pListText[15][i], 16);
						break;
					default:
						break;
					}
					string[i][2][13] = (recordTmp.userID / 10) + '0';
					string[i][2][14] = (recordTmp.userID % 10) + '0';
					break;
				}
			case EVENT_CODE_MODIFY:
				memcpy(&string[i][2][0], pListText[13][i], 16);
				break;
			default:
				break;
			}
		}
		break;
		case EVENT_TYPE_ALARM:
		{
			switch (recordTmp.eventCode)
			{
			case EVENT_CODE_SYS_LOCK:
				memcpy(&string[i][2][0], pListText[5][i], 16);
				break;
			case EVENT_CODE_HIJACK:
				memcpy(&string[i][2][0], pListText[2][i], 16);
				break;
			case EVENT_CODE_PRY_LOCK:
				memcpy(&string[i][2][0], pListText[0][i], 16);
				break;
			case EVENT_CODE_MECHANIC_KEY:
				memcpy(&string[i][2][0], pListText[3][i], 16);
				break;
			case EVENT_CODE_LOW_POWER:
				memcpy(&string[i][2][0], pListText[20][i], 16);
				break;
			case EVENT_CODE_LOCK_ERR:
				memcpy(&string[i][2][0], pListText[4][i], 16);
				break;
			case EVENT_CODE_ARMING:
				memcpy(&string[i][2][0], pListText[1][i], 16);
				break;
			default:
				break;
			}
		}
		break;
		default:
			break;
		}
		string[i][3][0] = num_tmp2 / 100 + '0';		   //编号/100  +'0';
		string[i][3][1] = (num_tmp2 % 100) / 10 + '0'; //(编号%100)/10  +'0';
		string[i][3][2] = (num_tmp2 % 10) + '0';	   //(编号%10) +'0';
		string[i][3][3] = ' ';
		string[i][3][4] = ((time[1] / 10)) + '0';
		string[i][3][5] = (time[1] % 10) + '0';
		string[i][3][6] = '-';
		string[i][3][7] = ((time[2] / 10)) + '0';
		string[i][3][8] = (time[2] % 10) + '0';
		string[i][3][9] = ' ';
		string[i][3][10] = ((time[3] / 10)) + '0';
		string[i][3][11] = (time[3] % 10) + '0';
		string[i][3][12] = ':';
		string[i][3][13] = ((time[4] / 10)) + '0';
		string[i][3][14] = (time[4] % 10) + '0';
		string[i][3][15] = ' ';
	}
	return string;
}

/**
 * @brief  获取系统语言函数
 *
 */
uint8_t Menu_GetSysLanguage(void)
{
	return g_SysLanguage;
}


/**
 * @brief  退出管理菜单
 *
 */
uint8_t Menu_Exit_Managemode(void) 
{
	Menu_SwitchMenu(MENU_MASTER);
}

/**
 * @brief  设置app添加卡片的结果
 *
 */
void Set_Appaddkey_Res(FlagStatus Result) 
{
	appAddKeysResult = Result;
}

/**
 * @brief  菜单初始化
 *
 * @note
 */
void Menu_Init(uint32_t wakeId)
{
	uint32_t timestamp;

	/* 进入主菜单 */
	Menu_SwitchMenu(MENU_MASTER);

	/* 读取当前时间 */
	OSAL_TimeGet(&timestamp, T_UTC);
	/* 刷新输入错误时间戳 */
	if (verifyFailTimestamp == 0)
	{
		verifyFailTimestamp = timestamp;
	}

	/* 系统锁定状态下重新上电，就重新刷新系统锁定时间 */
	OSAL_NvRead(SYS_OFFSET(verifyErrCount), &verifyFailCount, sizeof(verifyFailCount));
	// if (systemLockedTimestamp == 0 && verifyFailCount >= VERIFY_FAIL_NUM)
	if (verifyFailCount >= VERIFY_FAIL_NUM)
	{
		if (wakeId == 0xff)
		{
			systemLockedTimestamp = timestamp + 60; //锁定60s
		}
		masterMenuBox1.state = DISABLE;
		masterMenuBox2.state = DISABLE;
		masterMenuBox3.state = ENABLE;
	}
	/* 更新系统锁定状态 */
	SYS_API(Menu_GetSystemLockedStatus);
	SYS_API(Menu_ProcessVerifyFailed);
	Menu_GetSystemLockedStatus();
	Menu_ResetFlag();
}
