#include "component.h"
#include "local_main.h"
#include "time.h"
#include "local_sub.h"
#include "local_lock.h"

#define LOCAL_SUB_LOG(format, ...) OSAL_LOG(C_BLUE format C_NONE, ##__VA_ARGS__)

TimerHandle_stu_t timer_handle_t = NULL;

/* 布防状态 */
__EFRAM static ArmingStatus_enum_t armingStatus = ARMING_STA_STOP;

/* 报警次数 */
__EFRAM static uint8_t armingAlarmTimes;
__EFRAM static uint8_t lockAbnormalAlarmTimes;
__EFRAM static uint8_t forceUnlockAlarmTimes;
__EFRAM static uint8_t mechUnlockAlarmTimes;

#define ARMING_ALARM_TIMES 4	   //布防报警次数（4次约10S）
#define TAMPER_ALARM_TIMES 10	   //撬锁报警次数
#define LOCK_ERR_ALARM_TIMES 10	   //锁体异常报警次数
#define MECH_UNLOCK_ALARM_TIMES 20 //机械钥匙开锁报警次数

/* 布防按键 */
#define ARMING_KEY "8"
#define ARMING_LED "LED8"

//设置布防状态
int LocalSub_SetArmingStatus(ArmingStatus_enum_t status)
{
	uint8_t report = 0;
	LOCAL_SUB_LOG("LocalSub_SetArmingStatus(%d)\r\n", status);
	if (status == ARMING_STA_START)
	{
		Led_Set("LED", LED_OFF, 0, 0);
		Led_Set(ARMING_LED, LED_ON, 0, 0); //布防灯
	}
	else if (status == ARMING_STA_ENTER)
	{
		Local_LockSetSysFlag(SYS_FLAG_ARMING_STATUS, true); //设置启动布防
		report = 1;
	}
	else if (status == ARMING_STA_STOP)
	{
		if (armingStatus == ARMING_STA_ENTER)
		{
			Local_LockSetSysFlag(SYS_FLAG_ARMING_STATUS, false); //取消布防
			report = 1;
		}
	}
	armingStatus = status;
	if (report)
	{
		SYS_CALL(Remote_SettingChangeReport);
	}
	return 0;
}

//获取布防状态
ArmingStatus_enum_t LocalSub_GetArmingStatus(void)
{
	return armingStatus;
}

//退出启动布防的模式（退出显示0键的状态）
int LocalSub_ExitStartArming(void)
{
	if (armingStatus == ARMING_STA_START)
	{
		LocalSub_SetArmingStatus(ARMING_STA_STOP);
		Led_Set("LED", LED_OFF, 0, 0);
	}
	return 0;
}

//核对0键，进入布防状态
bool LocalSub_CheckZeroKeyEnterArming(uint8_t key)
{
	if (key == ARMING_KEY[0] && armingStatus == ARMING_STA_START)
	{
		Audio_Play(YI_QI_DONG_BU_FANG, RESET);
		LocalSub_SetArmingStatus(ARMING_STA_ENTER);
		return true;
	}
	return false;
}

//设置报警
void LocalSub_SetAlarm(bool status, AlarmType_enum_t type)
{
	uint8_t sys_flag = 0;
	EventRecord_stu_t record = {
		.eventType = EVENT_TYPE_ALARM,
		.eventCode = EVENT_CODE_ARMING,
		.eventSource = 0xFF,
		.userID = 0xFF,
		.appID = APP_ID_LOCAL,
	};
	OSAL_TimeGet(&record.timestamp, T_UTC);
	LOCAL_SUB_LOG("LocalSub_SetAlarm(%d, %d)\r\n", status, type);
	if (status != false)
	{
		// if (FuncTest_GetProductionTestMode() != TESTMODE_NORMAL)
		// {
		//     return; //测试模式下，不报警
		// }
	}
	if (type == ALARM_TYPE_ARMING)
	{
		armingAlarmTimes = ARMING_ALARM_TIMES * status;
		OSAL_NvRead(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag));
		sys_flag = (status == true) ? (sys_flag |= SYS_FLAG_ARMING_ALARM) : (sys_flag &= ~(SYS_FLAG_ARMING_ALARM));
		OSAL_NvWrite(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag)); //标记正在布防报警状态
		if (status != false)
		{
			Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET);
			record.eventCode = EVENT_CODE_ARMING;
		}
	}
	else if (type == ALARM_TYPE_FORCE_UNLOCK)
	{
		forceUnlockAlarmTimes = TAMPER_ALARM_TIMES * status;
		OSAL_NvRead(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag));
		sys_flag = (status == true) ? (sys_flag |= SYS_FLAG_TAMPER_ALARM) : (sys_flag &= ~(SYS_FLAG_TAMPER_ALARM));
		OSAL_NvWrite(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag)); //标记正在撬锁报警
		if (status != false)
		{
			Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET);
			Beep_Set(1, 1000); 
			record.eventCode = EVENT_CODE_PRY_LOCK;
		}
	}
	else if (type == ALARM_TYPE_LOCK_ABNORMAL)
	{
		lockAbnormalAlarmTimes = LOCK_ERR_ALARM_TIMES * status; //锁体异常报警
		if (status != false)
		{
			Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET);
			record.eventCode = EVENT_CODE_LOCK_ERR;
		}
	}
	else if (type == ALARM_TYPE_MECH_UNLOCK) //机械钥匙开锁报警
	{
		mechUnlockAlarmTimes = MECH_UNLOCK_ALARM_TIMES * status;
		OSAL_NvRead(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag));
		sys_flag = (status == true) ? (sys_flag |= SYS_FLAG_MECHKEY_ALARM) : (sys_flag &= ~(SYS_FLAG_MECHKEY_ALARM));
		OSAL_NvWrite(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag)); //标记正在机械钥匙报警
		if (status != false)
		{
			Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET);
			record.eventCode = EVENT_CODE_MECHANIC_KEY;
		}
	}
	if (status)
	{
		uint8_t activeStatu = SYS_CALL(Users_IsLockActived, NULL);
		if (activeStatu == SUCCESS)
		{
			SYS_CALL(Record_Save, &record);
			SYS_CALL(Remote_RecordReport, &record);
		}
	}
}

//检查报警状态
bool LocalSub_CheckAlarmStatus(void)
{
	if (armingAlarmTimes || lockAbnormalAlarmTimes || forceUnlockAlarmTimes || mechUnlockAlarmTimes)
	{
		return true;
	}
	return false;
}

//处理报警事件，间隔3s执行一次
static void LocalSub_ProcessAlarm_Cb(TimerHandle_stu_t handle)
{
	uint8_t sys_flag = 0;
	if (forceUnlockAlarmTimes > 0)
	{
		forceUnlockAlarmTimes--;
		if (forceUnlockAlarmTimes == 0)
		{
			OSAL_NvRead(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag));
			sys_flag &= ~(SYS_FLAG_TAMPER_ALARM);
			OSAL_NvWrite(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag)); //标记正在机械钥匙报警
		}
		Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET); //撬锁报警
		Beep_Set(1, 1000);						  //上电先响一声
	}
	else if (armingAlarmTimes > 0)
	{
		armingAlarmTimes--;
		if (armingAlarmTimes == 0)
		{
			OSAL_NvRead(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag));
			sys_flag &= ~(SYS_FLAG_ARMING_ALARM);
			OSAL_NvWrite(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag)); //标记正在布防报警
		}
		Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET); //布防报警
	}
	else if (lockAbnormalAlarmTimes > 0)
	{
		lockAbnormalAlarmTimes--;
		Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET); //锁体异常报警
	}
	else if (mechUnlockAlarmTimes > 0)
	{
		mechUnlockAlarmTimes--;
		if (mechUnlockAlarmTimes == 0)
		{
			OSAL_NvRead(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag));
			sys_flag &= ~(SYS_FLAG_MECHKEY_ALARM);
			OSAL_NvWrite(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag)); //标记正在机械钥匙报警
		}
		Audio_Play(BAO_JING_YIN_DA_DA_DA, RESET); //机械钥匙报警
	}
}

//初始化
void LocalSub_Init(TimerHandle_stu_t *TimerHandle)
{
	LocalSub_ExitStartArming();
	uint8_t sys_flag = 0;
	OSAL_NvRead(SYS_OFFSET(sys_flag), &sys_flag, sizeof(sys_flag));
	/* 恢复布防状态 */
	armingStatus = (Local_LockGetSysFlag(SYS_FLAG_ARMING_STATUS) == true) ? ARMING_STA_ENTER : ARMING_STA_STOP;
	/* 恢复报警状态 */
	lockAbnormalAlarmTimes = 0;
	forceUnlockAlarmTimes = ((sys_flag & SYS_FLAG_TAMPER_ALARM) != 0) ? 20 : 0;
	armingAlarmTimes = ((sys_flag & SYS_FLAG_ARMING_ALARM) != 0) ? ARMING_ALARM_TIMES : 0;
	mechUnlockAlarmTimes = ((sys_flag & SYS_FLAG_MECHKEY_ALARM) != 0) ? 20 : 0;

	// /* 恢复反锁灯状态 */
	// if (NvMan_GetSysFlag(SYS_FLAG_ANTI_LOCK_STATUS) == true) //已反锁
	// {
	// 	func_master_led_control(LED_STA_ANTI_LOCK, 0);
	// }

	/* 创建报警定时器 */
	*TimerHandle = OSAL_TimerCreate(LocalSub_ProcessAlarm_Cb, 3000, SET);
	OSAL_TimerSet(*TimerHandle, TIM_RUN_ONLY_WAKE); //设置定时器仅唤醒状态工作
}
