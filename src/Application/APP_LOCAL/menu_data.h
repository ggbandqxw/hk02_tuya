#ifndef __FUNC_MENU_DATA_H__
#define __FUNC_MENU_DATA_H__

#include "menu.h"
#include "component.h"
#define INPUT_BUFFER_SIZE (32) //输入缓存size

static void Menu_DeleteAllKeys(void);
static void Menu_SetOpenModeNormal(void);
static void Menu_SetOpenModeSafe(void);
static void Menu_SetAudioOn(void);
static void Menu_SetAudioOnHighVolume(void);
static void Menu_SetAudioOnLowVolume(void);

static void Menu_SetAudioOff(void);
static void Menu_SetLanguageChinese(void);
static void Menu_SetLanguageEnglish(void);
static void Menu_SetLanguageSpanish(void);
static void Menu_SetLanguagePortuguese(void);
static void Menu_SetLanguageRussian(void);
static void Menu_SetLanguageKazak(void);

static void Menu_OpenAutoLock(void);
static void Menu_CloseAutoLock(void);
// static void Menu_OpenVacationMode(void);
// static void Menu_CloseVacationMode(void);

static void Menu_AddNetwork(void);
static void Menu_DelNetwork(void);
static void Menu_DisconnectBLE(void);
static void Menu_OpenBLE(void);
static void Menu_CloseBLE(void);
static void Menu_SecurityCode(void);
ErrorStatus FuncCalendar_ReadTime(uint8_t *time);
static void Menu_ConnectBLE(void);
static uint8_t inPutBuff[INPUT_BUFFER_SIZE];

static InputBoxData_stu_t inputBox_data =
	{
		.buffer = inPutBuff, //输入框DATA域
};

/* 主菜单：输入框数据 */
static BoxData_stu_t masterMenuBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = SET,
		.xpos = 0,
		.ypos = 6,
		.current = 0, //当前输入的位数
		.len = 16,	  //最大可输入位数
		.data = &inputBox_data,
};

/* 主菜单：文本框数据 */
static TextBoxData_s1tu_t masterMenuBox2_data =
	{
		.text = {"请输入密码", "Input PIN Code:"}};
static BoxData_stu_t masterMenuBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 4,
		.data = &masterMenuBox2_data,
};

/* 主菜单：文本框数据 */
static TextBoxData_s1tu_t masterMenuBox3_data =
	{
		.text = {"系统已锁定", "System Locked"}};
static BoxData_stu_t masterMenuBox3 =
	{
		.type = BOX_TYPE_TEXT,
		.state = DISABLE,
		.xpos = 0,
		.ypos = 4,
		.data = &masterMenuBox3_data,
};

static BoxData_stu_t masterMenuBox4 =
	{
		.type = BOX_TYPE_BATTERY,
		.state = ENABLE,
		.xpos = 100,
		.ypos = 1,
		.current = 0,
};

static BoxData_stu_t masterMenuBox5 =
	{
		.type = BOX_TYPE_RTC,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,
		.data = OSAL_TimeGet,
};

/* 管理模式菜单：选择框数据 */
static SelectBoxData_stu_t manageModeBox1_data[] =
	{
		{'1', NULL, MENU_USER_SET, {TIAN_JIA_YONG_HU_QING_AN, YU_YIN_1}, {"1.添加用户", "1.add password "}},
		{'2', NULL, MENU_DEL_SELECT, {SHAN_CHU_YONG_HU_QING_AN, YU_YIN_2}, {"2.删除用户", "2.del password"}},
		{'3', NULL, MENU_AUTO_LOCK_SET, {ZI_DONG_SHANG_SUO_GONG_NENG_QING_AN, YU_YIN_3}, {"3.自动上锁设置", "3.auto lock set"}},
		{'4', NULL, MENU_AUDIO_SET, {YIN_LIANG_DIAO_JIE_QING_AN, YU_YIN_4}, {"4.语音设置", "4.volume set"}},
		//{'5', NULL, MENU_VACATION_SET, {JIA_QI_MO_SHI_QING_AN, YU_YIN_5}, {"5.假期模式设置", "5.vacation mode set"}},
};
static BoxData_stu_t manageModeBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,							 //当前光标位置
		.len = OSAL_LENGTH(manageModeBox1_data), //条目总数
		.data = manageModeBox1_data,
};

/* 用户设置菜单：选择框数据 */
static SelectBoxData_stu_t userSetBox1_data[] =
	{
		{'1', NULL, MENU_MODIFY_ADMIN, XIU_GAI_GUAN_LI_YUAN_YONG_HU_QING_AN, YU_YIN_1, "1.修改管理员密码", "1. add admin Pwd"},//修改管理员密码请按1 --001      
		{'2', NULL, MENU_ADDUSER_SET, TIAN_JIA_PU_TONG_YONG_HU_QING_AN, YU_YIN_2, "2.添加普通用户  ", "2.Add User PIN"},
};
static BoxData_stu_t userSetBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,						  //当前光标位置
		.len = OSAL_LENGTH(userSetBox1_data), //条目总数
		.data = userSetBox1_data,
};

/* 普通用户设置菜单：选择框数据 */
static SelectBoxData_stu_t normaluserSetBox1_data[] =
	{	
		{'1', NULL, MENU_ADD_FPT, TIAN_JIA_ZHI_WEN_QING_AN, YU_YIN_1, "1.添加指纹", "1.Add User fpt"},
		{'2', NULL, MENU_ADD_PWD, TIAN_JIA_MI_MA_QING_AN, YU_YIN_2, "2.添加密码", "1.Add User PIN"},
		{'3', NULL, MENU_ADD_CARD, TIAN_JIA_KA_PIAN_QING_AN, YU_YIN_3, "3.添加卡片  ", "3.add card"},//添加卡片请按 --002
}; 
static BoxData_stu_t normaluserSetBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,						  //当前光标位置
		.len = OSAL_LENGTH(normaluserSetBox1_data), //条目总数
		.data = normaluserSetBox1_data,
};

/* （添加密钥）输入编号菜单：输入框数据 */
static BoxData_stu_t addKeysInputNumBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = RESET,
		.xpos = 0,
		.ypos = 2,
		.current = 0, //当前输入的位数
		.len = 3,	  //最大可输入位数  2 // --003
		.data = &inputBox_data,
};

/* （添加密钥）编号输入菜单：文本框数据 */
static TextBoxData_s1tu_t addKeysInputNumBox2_data =
	{
		.text = {"请输入编号：", "Input Num："}};
static BoxData_stu_t addKeysInputNumBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &addKeysInputNumBox2_data,
};

/* 修改管理员密码菜单：输入框数据 */
static BoxData_stu_t modifyAdminBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = SET,
		.xpos = 0,
		.ypos = 2,
		.current = 0, //当前输入的位数
		.len = 12,	  //最大可输入位数
		.data = &inputBox_data,
};

/* 修改管理员密码菜单：文本框数据 */
static TextBoxData_s1tu_t modifyAdminBox2_data =
	{
		.text = {"请修改管理员密码", "Modify Admin PIN"}};
static BoxData_stu_t modifyAdminBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &modifyAdminBox2_data,
};

/* 添加密码菜单：输入框数据 */
static BoxData_stu_t addPwdBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = SET,
		.xpos = 0,
		.ypos = 2,
		.current = 0, //当前输入的位数
		.len = 12,	  //最大可输入位数
		.data = &inputBox_data,
};

/* 添加密码菜单：文本框数据 */
static TextBoxData_s1tu_t addPwdBox2_data =
	{
		.text = {"请输入密码", "Input PIN Code:"}};
static BoxData_stu_t addPwdBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &addPwdBox2_data,
};

/* 添加指纹菜单：文本框数据 */
static TextBoxData_s1tu_t addFingerBox1_data =
	{
		.text = {"请按指纹", "Input FP"}};
static BoxData_stu_t addFingerBox1 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 32,
		.ypos = 3,
		.data = &addFingerBox1_data,
};

/* 添加卡片菜单：文本框数据 */
static TextBoxData_s1tu_t addCardBox1_data =
	{
		.text = {"请 刷 卡", "Slot Card"}};
static BoxData_stu_t addCardBox1 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 32,
		.ypos = 3,
		.data = &addCardBox1_data,
};

/* 删除选择菜单：选择框数据 */
static SelectBoxData_stu_t delSelectBox1_data[] =
	{
		{'1', NULL, MENU_INPUT_NUMBER_DEL_KEYS, {AN_BIAN_HAO_SHAN_CHU_QING_AN, YU_YIN_1}, {"1.按编号删除", "1.Del pwd by number"}},
		//{'2', NULL, MENU_DEL_PWD, {AN_SHU_RU_NEI_RONG_SHAN_CHU_QING_AN, YU_YIN_2}, {"2.按输入内容删除", "2.Del pwd by input data"}},
		//{'3', Menu_DeleteAllKeys, MENU_DEL_ALL_PWD, {QING_KONG_SUO_YOU_YONG_HU_QING_AN, YU_YIN_3}, {"3.清空所有用户", "3.Del All pwd"}},

		{'2', NULL, MENU_DEL_PWD, {AN_SHU_RU_NEI_RONG_SHAN_CHU_QING_AN, YU_YIN_2}, {"2.按输入内容删除", "2.Del pwd by input data"}},
		{'3', Menu_DeleteAllKeys, MENU_DEL_ALL_PWD, {QING_KONG_SUO_YOU_YONG_HU_QING_AN, YU_YIN_3}, {"3.清空所有用户", "3.Del All pwd"}},
};// --004
static BoxData_stu_t delSelectBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,							//当前光标位置
		.len = OSAL_LENGTH(delSelectBox1_data), //条目总数
		.data = delSelectBox1_data,
};

/* 删除密码菜单：输入框数据 */
static BoxData_stu_t delPwdBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = RESET,
		.xpos = 0,
		.ypos = 2,
		.current = 0, //当前输入的位数
		.len = 12,	  //最大可输入位数
		.data = &inputBox_data,
};

/* 删除密码菜单：文本框数据 */
static TextBoxData_s1tu_t delPwdBox2_data =
	{
#if KOS_PARAM_USERS_QTY_PWD == 20 
		.text = {"请输入编号 00-19", "Input Num 00-19"}
#elif defined(PWD_QTY_MAX_45)
		.text = {"请输入编号 00-44", "Input Num 00-44"}
#else
		.text = {"请输入编号 00-09", "Input Num 00-09"}
#endif
};
static BoxData_stu_t delPwdBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &delPwdBox2_data,
};

/* 删除指纹菜单：输入框数据 */
static BoxData_stu_t delFingerBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = RESET,
		.xpos = 0,
		.ypos = 2,
		.current = 0, //当前输入的位数
		.len = 2,	  //最大可输入位数
		.data = &inputBox_data,
};

/* 删除指纹菜单：文本框数据 */
static TextBoxData_s1tu_t delFingerBox2_data =
	{
		.text = {"请输入指纹或编号", "Num Or FP"}};
static BoxData_stu_t delFingerBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &delFingerBox2_data,
};

/* 删除卡片菜单：输入框数据 */
static BoxData_stu_t delCardBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = RESET,
		.xpos = 0,
		.ypos = 2,
		.current = 0, //当前输入的位数
		.len = 2,	  //最大可输入位数
		.data = &inputBox_data,
};

/* 删除卡片菜单：文本框数据 */
static TextBoxData_s1tu_t delCardBox2_data =
	{
		.text = {"请刷卡或输入编号", "Num Or card"}};
static BoxData_stu_t delCardBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &delCardBox2_data,
};

/* 开门模式菜单：选择框数据 */
// static SelectBoxData_stu_t vacationSetModeBox1_data[] =
// 	{
// 		{'1', Menu_OpenVacationMode, MENU_MAX, {KAI_QI_QING_AN, YU_YIN_1}, {"1.开启假期模式", "1.open vacation Mode"}},
// 		{'2', Menu_CloseVacationMode, MENU_MAX, {GUAN_BI_QING_AN, YU_YIN_2}, {"2.关闭假期模式", "2.close vacation Mode"}},
// };
// static BoxData_stu_t vacationModeBox1 =
// 	{
// 		.type = BOX_TYPE_SELECT,
// 		.state = ENABLE,
// 		.xpos = 0,
// 		.ypos = 0,
// 		.current = 0,						   //当前光标位置
// 		.len = OSAL_LENGTH(vacationSetModeBox1_data), //条目总数
// 		.data = vacationSetModeBox1_data,
// };


/* （添加密钥）输入编号菜单：输入框数据 */
static BoxData_stu_t setAutoLockTimeBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = RESET,
		.xpos = 0,
		.ypos = 2,
		.current = 0, //当前输入的位数
		.len = 2,	  //最大可输入位数  2
		.data = &inputBox_data,
};

/* （添加密钥）编号输入菜单：文本框数据 */
static TextBoxData_s1tu_t setAutoLockTimeBox2_data =
	{
		.text = {"请输入时间：", "Input time："}};
static BoxData_stu_t setAutoLockTimeBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &setAutoLockTimeBox2_data,
};

/* 自动上锁设置菜单：选择框数据 */
static SelectBoxData_stu_t autoLockSetBox1_data[] =
	{
		{'1', NULL, MENU_AUTO_LOCK_TIME_SET, {KAI_QI_ZI_DONG_SHANG_SUO_GONG_NENG_QING_AN, YU_YIN_1}, {"1.开启自动上锁", "1.open auto lock"}},
		{'2', Menu_CloseAutoLock, MENU_MAX, {GUAN_BI_ZI_DONG_SHANG_SUO_GONG_NENG_QING_AN, YU_YIN_2}, {"2.关闭自动上锁", "2.close auto lock"}},
};
static BoxData_stu_t autoLockSetBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,							//当前光标位置
		.len = OSAL_LENGTH(autoLockSetBox1_data), //条目总数
		.data = autoLockSetBox1_data,
};

/* 语音设置菜单：选择框数据 */
static SelectBoxData_stu_t audioSetBox1_data[] =
	{
		{'1', Menu_SetAudioOff, MENU_MAX, {JING_YIN_QING_AN, YU_YIN_1}, {"1.静音模式", "2.Voice Guid Off"}},
		{'2', Menu_SetAudioOnLowVolume, MENU_MAX, {DI_YIN_QING_AN, YU_YIN_2}, {"2.低音量模式", "1.Voice Guid On Low Volume"}},
		{'3', Menu_SetAudioOn, MENU_MAX, {ZHONG_YIN_QING_AN, YU_YIN_3}, {"3.中音量模式", "2.Voice Guid Off high Volume"}},
		{'4', Menu_SetAudioOnHighVolume, MENU_MAX, {GAO_YIN_QING_AN, YU_YIN_4}, {"4.高音量模式", "1.Voice Guid On Medium Volume"}},
};
static BoxData_stu_t audioSetBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,						   //当前光标位置
		.len = OSAL_LENGTH(audioSetBox1_data), //条目总数
		.data = audioSetBox1_data,
};

/* 语言设置菜单：选择框数据 */

#ifdef LANG_KAZAK
static SelectBoxData_stu_t languageSetBox1_data[] =
	{
		// {'1', Menu_SetLanguageKazak, MENU_MAX,{HA_SA_KE_YU_QING_AN,YU_YIN_1},	{ "4.哈萨克语请按 	 ",  "3.Chinese 	  "}},
		// {'2', Menu_SetLanguageRussian, MENU_MAX,{E_YU_QING_AN,YU_YIN_2},   		{ "3.俄语请按	  ",  "3.Chinese	   "}},
		// {'3', Menu_SetLanguageEnglish, MENU_MAX, {YING_WEN_QING_AN, YU_YIN_3}, 	{"2.英文模式      ", "2.English       "}},
		// {'4', Menu_SetLanguageChinese, MENU_MAX, {ZHONG_WEN_QING_AN, YU_YIN_4}, {"1.中文模式      ", "1.Chinese       "}},	
	};
#else
static SelectBoxData_stu_t languageSetBox1_data[] =
	{
		// {'1', Menu_SetLanguageChinese, MENU_MAX, {ZHONG_WEN_QING_AN, YU_YIN_1}, {"1.中文模式      ", "1.Chinese       "}},
		// {'2', Menu_SetLanguageEnglish, MENU_MAX, {YING_WEN_QING_AN, YU_YIN_2}, {"2.英文模式      ", "2.English       "}},
		// #if LANGUAGE_QTY>2
		// {'3', Menu_SetLanguageSpanish, MENU_MAX,{XI_BAN_YA_YU_QING_AN,YU_YIN_3},    {"3.西班牙语请按      ",  "4.English       "}},
		// {'4', Menu_SetLanguagePortuguese, MENU_MAX,{PU_TAO_YA_YU_QING_AN,YU_YIN_4},  {  "4.葡萄牙语请按      ",  "4.English       "}},
 		// {'5', Menu_SetLanguageRussian, MENU_MAX,{E_YU_QING_AN,YU_YIN_5},   { "5.俄语请按      ",  "3.Chinese       "}},
		// #endif
	};
#endif

static BoxData_stu_t languageSetBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,							  //当前光标位置
		.len = OSAL_LENGTH(languageSetBox1_data), //条目总数
		.data = languageSetBox1_data,
};

/* 记录查询菜单：选择框数据 */
static SelectBoxData_stu_t recordQueryBox1_data[] =
	{
// 		{'1', NULL, MENU_CODE, {XI_TONG_CHA_XUN_QING_AN, YU_YIN_1}, {"1.系统查询      ", "1.System Query  "}},
// #if defined(OLED_FUNCTION)
// 		{'2', NULL, MENU_ORDER_RECORD, {SHUN_XU_CHA_XUN_QING_AN, YU_YIN_2}, {"2.顺序查询      ", "2.Order Query   "}},
// 		{'3', NULL, MENU_INPUT_NUMBER_RECORD, {BIAN_HAO_CHA_XUN_QING_AN, YU_YIN_3}, {"3.编号查询      ", "3.Number Query  "}},
// #endif
};
static BoxData_stu_t recordQueryBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,							  //当前光标位置
		.len = OSAL_LENGTH(recordQueryBox1_data), //条目总数
		.data = recordQueryBox1_data,
};

/* 扩展菜单：选择框数据 */
static SelectBoxData_stu_t extendBox1_data[] =
	{
		// {'1', Menu_AddNetwork, MENU_MASTER, {JIA_RU_WANG_LUO_QING_AN, YU_YIN_1}, {"1.加入网络      ", "1.Add Network   "}},
		// {'2',  Menu_DelNetwork,  MENU_DISCONNECT_NETWORK,  {TUI_CHU_WANG_LUO_QING_AN,   YU_YIN_2},    {"2.退出网络      ",    "2.Del Network   "}},
};
static BoxData_stu_t extendBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,						 //当前光标位置
		.len = OSAL_LENGTH(extendBox1_data), //条目总数
		.data = extendBox1_data,
};

/* 蓝牙设置菜单：选择框数据 */
static SelectBoxData_stu_t bleSetBox1_data[] =
	{
		// {'1', Menu_ConnectBLE, MENU_CONNECT_NETWORK, {LIAN_JIE_LAN_YA_QING_AN, YU_YIN_1}, {"1.连接蓝牙      ", "1.Connect Ble   "}},
		// {'2', Menu_DisconnectBLE, MENU_DISCONNECT_NETWORK, {DUAN_KAI_LAN_YA_QING_AN, YU_YIN_2}, {"2.断开蓝牙      ", "2.Disconnect Ble"}},
		// {'3', Menu_CloseBLE, MENU_MAX, {GUAN_BI_LAN_YA_QING_AN, YU_YIN_3}, {"3.关闭蓝牙模块  ", "3.Disable Ble   "}},
};
static BoxData_stu_t bleSetBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,						 //当前光标位置
		.len = OSAL_LENGTH(bleSetBox1_data), //条目总数
		.data = bleSetBox1_data,
};

/* 蓝牙开启菜单：选择框数据 */
static SelectBoxData_stu_t bleOnBox1_data[] =
	{
		// {'1', Menu_OpenBLE, MENU_BLE_SET, {KAI_QI_LAN_YA_QING_AN, YU_YIN_1}, {"1.开启蓝牙模块  ", "1.Enable Ble    "}},
};
static BoxData_stu_t bleOnBox1 =
	{
		.type = BOX_TYPE_SELECT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.current = 0,						//当前光标位置
		.len = OSAL_LENGTH(bleOnBox1_data), //条目总数
		.data = bleOnBox1_data,
};

/* 配网菜单：文本框数据 */
static TextBoxData_s1tu_t configNetworkBox1_data =
	{
		.text = {"请稍后...", "Wait ..."}};
static BoxData_stu_t configNetworkBox1 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 35,
		.ypos = 3,
		.data = &configNetworkBox1_data,
};

/* 防伪码菜单：文本框数据 */
static TextBoxData_s1tu_t codeBox1_data =
	{
		.text = {"请刷防伪码", "Scan Code"}};
static BoxData_stu_t codeBox1 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 24,
		.ypos = 3,
		.data = &codeBox1_data,
};

/* 防伪码菜单：文本框数据 */
static TextBoxData_s1tu_t codeBox2_data =
	{
		.text = {"防伪码:", "Code:"}};

static BoxData_stu_t codeBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 40,
		.ypos = 1,
		.data = &codeBox2_data,
};

/*显示具体防伪码内容*/
static BoxData_stu_t codeBox3 =
	{
		.type = BOX_TYPE_VARIABLE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &inputBox_data,
};

static InputBoxData_stu_t dateSetBox1_data =
	{
		.buffer = inPutBuff,
		.dis_format = "20  -  -  ",
};
/* 日期设置菜单  输入框数据*/
static BoxData_stu_t dateSetBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.xpos = 24,
		.ypos = 3,
		.len = 6,
		.data = &dateSetBox1_data,
};
/* 日期设置菜单  文本框数据*/
static TextBoxData_s1tu_t dateSetBox2_data =
	{
		.text = {"日期设置", "Date Set:"}};
static BoxData_stu_t dateSetBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = DISABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &dateSetBox2_data,
};

static InputBoxData_stu_t timeSetBox1_data =
	{
		.buffer = inPutBuff,
		.dis_format = "  :  :  ",
};
/* 时间设置菜单  输入框数据*/
static BoxData_stu_t timeSetBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.xpos = 32,
		.ypos = 3,
		.len = 6,
		.data = &timeSetBox1_data,
};
/* 时间设置菜单  文本框数据*/
static TextBoxData_s1tu_t timeSetBox2_data =
	{
		.text = {"时间设置", "Time Set:"}};
static BoxData_stu_t timeSetBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = DISABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &timeSetBox2_data,
};

/*输入编号查询菜单   输入3位标号*/
static BoxData_stu_t inPutRecordNumBox1 =
	{
		.type = BOX_TYPE_INPUT,
		.state = ENABLE,
		.hide = RESET,
		.xpos = 0,
		.ypos = 2,
		.current = 0, //当前输入的位数
		.len = 3,	  //最大可输入位数
		.data = &inputBox_data,
};

/*输入编号查询菜单   提示文本*/
static TextBoxData_s1tu_t inPutRecordNumBox2_data =
	{
		.text = {"请输入三位编号:", "Input Num："}};
static BoxData_stu_t inPutRecordNumBox2 =
	{
		.type = BOX_TYPE_TEXT,
		.state = ENABLE,
		.xpos = 0,
		.ypos = 0,
		.data = &inPutRecordNumBox2_data,
};

static ListBoxStr_t Menu_RecordListBoxCallback(uint16_t cur, uint16_t last_cur);
/*记录消息列表框*/
static BoxData_stu_t recordListDataBox1 =
	{
		.type = BOX_TYPE_LIST,
		.state = ENABLE,
		.hide = RESET,
		.xpos = 0,
		.ypos = 0,
		.len = 0,
		.current = 0,						//当前输入的位数
		.data = Menu_RecordListBoxCallback, //
};

#endif /* __FUNC_MENU_DATA_H__ */
