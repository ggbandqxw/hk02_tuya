#ifndef _LOCAL_SUB_H
#define _LOCAL_SUB_H
#include "local_main.h"

/* 布防状态 */
typedef enum
{
    ARMING_STA_STOP,
    ARMING_STA_START,
    ARMING_STA_ENTER,
} ArmingStatus_enum_t;

/* 报警类型 */
typedef enum
{
    ALARM_TYPE_ARMING,        //布防
    ALARM_TYPE_LOCK_ABNORMAL, //锁体异常
    ALARM_TYPE_FORCE_UNLOCK,  //撬锁
    ALARM_TYPE_MECH_UNLOCK,   //机械钥匙
} AlarmType_enum_t;

int LocalSub_SetArmingStatus(ArmingStatus_enum_t status);
int LocalSub_ExitStartArming(void);
bool LocalSub_CheckZeroKeyEnterArming(uint8_t key);
void LocalSub_SetAlarm(bool status, AlarmType_enum_t type);
bool LocalSub_CheckAlarmStatus(void);
void LocalSub_Init(TimerHandle_stu_t *TimerHandle);
ArmingStatus_enum_t LocalSub_GetArmingStatus(void);
#endif
