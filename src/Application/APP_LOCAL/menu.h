﻿/*
 * @Author: qixuewen 1935217683@qq.com
 * @Date: 2024-01-12 09:41:13
 * @LastEditors: qixuewen 1935217683@qq.com
 * @LastEditTime: 2024-06-18 11:46:19
 * @FilePath: \omicro\src\Application\APP_LOCAL\menu.h
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%A
 */
#ifndef __FUNC_MENU_H__
#define __FUNC_MENU_H__

#include "osal.h"

typedef enum
{
	MENU_MASTER = 0,			// 主菜单
	MENU_MANAGE_MODE,			// 管理模式菜单
	MENU_USER_SET,				// 用户设置菜单
	MENU_INPUT_NUMBER_ADD_KEYS, // 添加密钥编号输入菜单
	MENU_MODIFY_ADMIN,			// 修改管理密码菜单
	MENU_ADD_PWD,				// 添加密码菜单
	MENU_ADD_FPT,				// 添加指纹菜单
	MENU_ADD_CARD,				// 添加卡片菜单
	MENU_DEL_SELECT,			// 删除选择菜单
	MENU_DEL_PWD,				// 删除密码菜单
	MENU_DEL_FPT,				// 删除指纹菜单
	MENU_DEL_CARD,				// 删除卡片菜单
	MENU_OPEN_MODE,				// 开门模式菜单
	MENU_SYSTEM_SET,			// 系统设置菜单
	MENU_AUDIO_SET,				// 语音设置菜单
	MENU_LANGUAGE_SET,			// 语言设置菜单
	MENU_RECORD_QUERY,			// 记录查询菜单
	MENU_EXTEND,				// 扩展功能菜单
	MENU_BLE_ON,				// 蓝牙开启菜单
	MENU_BLE_SET,				// 蓝牙设置菜单
	MENU_CONNECT_NETWORK,		// 配网菜单
	MENU_DISCONNECT_NETWORK,	// 退网菜单
	MENU_CODE,					// 防伪码菜单
#if defined(OLED_FUNCTION)
	MENU_DATE_SET,			  // 日期设置菜单
	MENU_TIME_SET,			  // 时间设置菜单
	MENU_INPUT_NUMBER_RECORD, // 开锁记录编号输入菜单
	MENU_ORDER_RECORD,		  // 顺序记录查询
#endif

	MENU_BLE,					// 蓝牙菜单（虚拟菜单）
	MENU_DEL_ONE_KEYS,			// 删除一个密钥菜单（虚拟菜单）
	MENU_AUTO_LOCK_SET,			// 自动上锁设置菜单
	//MENU_VACATION_SET,			// 假期模式设置菜单
	MENU_INPUT_NUMBER_DEL_KEYS, // 删除密钥编号输入菜单 -- 输入编号删除
	MENU_ADD_ADMIN_PWD,			// 添加管理密码
	MENU_ADD_ADMIN_FPT,			// 添加管理密码
	MENU_DEL_ALL_PWD,           // 删除全部密码
	MENU_DEL_ALL_FPT,           // 删除全部指纹
	MENU_DEL_ALL_CARD,          // 删除全部卡片
	MENU_ADDUSER_SET,			// 添加用户设置菜单
	MENU_AUTO_LOCK_TIME_SET,	// 自动上锁时间设置菜单
	MENU_MAX,
} MenuType_enum_t;

/* 用户输入数据类型 */
typedef enum
{
	INPUT_TYPE_CHAR,   //字符类型
	INPUT_TYPE_FP,     //指纹类型
	INPUT_TYPE_CARD,   //卡片类型
	INPUT_TYPE_RETURN,
}InputDataType_enum_t;


// ErrorStatus Menu_StartAppAddKeys(uint8_t keysType, uint8_t keysNum, uint8_t keysAttr, NvPloy_stu_t *ploy);
void Menu_ResetFlag(void);

void Menu_Init(uint32_t wakeId);
void Menu_UserInputProcess(InputDataType_enum_t inputType, uint8_t *pData, uint8_t len);
void Menu_SwitchMenu(MenuType_enum_t menu);
MenuType_enum_t Menu_GetFatherMenu(MenuType_enum_t menu);
MenuType_enum_t Menu_GetCurrentMenu(void);
FlagStatus Menu_GetAppAddFlag(void);
// void Menu_GetVerifyRecord(NvOpenRecord_stu_t *pRecord);
void Menu_ExitSystemLocked(void);
uint8_t Menu_ProcessVerifyFailed(void);
uint8_t Menu_GetSystemLockedStatus(void);
void Menu_UpdateBattery(int battery);
void  Menu_UpdateSysLanguage(uint8_t Language);
uint8_t  Menu_GetSysLanguage(void);
void Menu_UpdateRecordLength(void);
void Menu_PlayMenuAudioList(uint16_t num);
ErrorStatus Menu_StartAppAddKeys(uint8_t keysType, uint8_t keysNum);


#endif /* __FUNC_MENU_H__ */
