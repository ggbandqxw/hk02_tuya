/*
 * @Author: qixuewen 1935217683@qq.com
 * @Date: 2024-05-20 13:53:43
 * @LastEditors: qixuewen 1935217683@qq.com
 * @LastEditTime: 2024-06-18 20:19:40
 * @FilePath: \omicro\src\Application\APP_LOCAL\local_lock.h
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
#ifndef _LOCAL_LOCK_H
#define _LOCAL_LOCK_H
#include "local_main.h"

#define DEFAULT_AUDO_LOCK_TIME 20                   //默认由传感器控制上锁
#define B2B_CMD_BUTTON_STA 0X04                     //机械按键状态上报
#define B2B_CMD_REPORT_TOUCH_HANDLE_STA 0X05        //后板上报感应把手状态
#define B2B_CMD_DOOR_BELL_CONTROL 0X06              //门铃播报
#define B2B_CMD_LOCK_REPORT 0X09                    //锁体上报
#define B2B_CMD_LOCK_CONTROL 0X10                   //锁体控制命令
#define B2B_CMD_GET_BACK_BOARD_VER 0X14             //获取后板版本
#define B2B_CMD_SET_DOOR_DIR 0X16                   //设置门锁方向
#define B2B_CMD_LOCK_POWER 0X17                     //设置门锁有没有天地钩
#define B2B_CMD_MENU_SET_TOUCH_HANDLE_FUNCTION 0X18 //菜单设置感应把手功能开启关闭
#define B2B_CMD_TOUCH_HANDLE_SET_REPORT 0X19        //后板快捷键设置感应把手功能开启关闭

/* 上锁类型 */
#define LOCKED_TYPE_SLOT 0XFA      // 长按插舌上锁
#define LOCKED_TYPE_CLOSE 0XFB     // 长按CLOSE上锁
#define LOCKED_TYPE_AUTO_MODE 0XFC // 切换到自动模式上锁
#define LOCKED_TYPE_10S 0XFD       // 10s自动上锁

/* 锁体类型 */
#define LOCK_TYPE_524 1 //524锁体
#define LOCK_TYPE_824 2 //824锁体

void Local_LockInit(AppHandle_t app_handle);
void Local_LockSaveRecord(uint8_t event_type, uint8_t event_source, uint8_t event_code, uint8_t user_id);
bool Local_LockGetSysFlag(uint8_t flag);
bool Local_LockSetSysFlag(uint8_t flag, bool state);
LockAction_enum_t local_LockGetLockStatus(void);
uint8_t Local_LockGetTouchKeyLockTime(void);
TimerHandle_stu_t Local_LockGetAutoLockTimer(void);
ErrorStatus Lock_SleepProcess(void);

ErrorStatus Local_LockControl(uint8_t userType, uint8_t key_id, LockCtrl_enum_t control);
ErrorStatus Local_LockSetDoorDirection(uint8_t dir);
ErrorStatus Local_LockSetOpenDoorPower(uint8_t power);
void Local_SetAntiLock(void);
uint8_t Local_LockGetAmMode(void);
uint8_t Local_LockGetLockType(void);
void Local_LockProcessWakeSrc(uint32_t wake_src);
extern uint32_t wakeUpCheckRemainTime(void);

#endif
