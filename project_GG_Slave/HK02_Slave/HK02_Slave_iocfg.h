/**
 * @file slave_iocfg.h
 * @brief 
 * 
 * @version 0.1
 * @date 2024-01-13
 * 
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 * 
 * @note          鼎新同创・智能锁
 *  
 * @par 修改日志: 
 * <1> 2024-01-15 v0.1 huangliwu 创建初始版本
 * *************************************************************************
 */
#ifndef __D601_Slave_IOCFG_H__
#define __D601_Slave_IOCFG_H__


//#define PIN_UART0_TX   0 //5
//#define PIN_UART0_RX   1 //6

// #define PIN_BOOT_UART1_TX 5  // TODO:boot 串口要修改和前后版通信串口一致【OTA】
// #define PIN_BOOT_UART1_RX 6

// #define PIN_BOOT_UART1_TX_MASK HS6621P_BIT_MASK(PIN_BOOT_UART1_TX)
// #define PIN_BOOT_UART1_RX_MASK HS6621P_BIT_MASK(PIN_BOOT_UART1_RX)

//MC60 模组
#define PIN_MC60_UART0_TX 21
#define PIN_MC60_UART0_RX 20
#define PIN_MCU_WAKE_BLE  23
#define PIN_BLE_WAKE_MCU  22
#define PIN_MC60_UART0_TX_MASK HS6621P_BIT_MASK(PIN_MC60_UART0_TX)
#define PIN_MC60_UART0_RX_MASK HS6621P_BIT_MASK(PIN_MC60_UART0_RX)
#define PIN_MCU_WAKE_BLE_MASK  HS6621P_BIT_MASK(PIN_MCU_WAKE_BLE)
#define PIN_BLE_WAKE_MCU_MASK  HS6621P_BIT_MASK(PIN_BLE_WAKE_MCU)

#define BUTTON_RESET_PIN    9
#define BUTTON_ONOFF_PIN    25 //--
#define HOMEKIT_PIN         10 //未做处理//
#define HOMEKIT_NRST_PIN    24 //未做处理// ----------
#define BUTTON_RESET_PIN_MASK  HS6621P_BIT_MASK(BUTTON_RESET_PIN)
#define BUTTON_ONOFF_PIN_MASK  HS6621P_BIT_MASK(BUTTON_ONOFF_PIN)
#define HOMEKIT_PIN_MASK       HS6621P_BIT_MASK(HOMEKIT_PIN)
#define HOMEKIT_NRST_PIN_MASK  HS6621P_BIT_MASK(HOMEKIT_NRST_PIN)

#define OPTO3_PIN     1  //--
#define OPTO2_PIN     11
#define OPTO1_PIN     13
#define OPTO_EN_PIN   14
#define OPTO3_PIN_MASK HS6621P_BIT_MASK(OPTO3_PIN)
#define OPTO2_PIN_MASK HS6621P_BIT_MASK(OPTO2_PIN)
#define OPTO1_PIN_MASK  HS6621P_BIT_MASK(OPTO1_PIN)
#define OPTO_EN_PIN_MASK  HS6621P_BIT_MASK(OPTO_EN_PIN)

#define MOTOR_EN_VCC_PIN   15
#define MOTOR_EN_PIN   16
#define MOTOR_F_PIN   17
#define MOTOR_R_PIN   18
#define MOTO_OVP_ADC_PIN   12// 驱动做adc检测
#define MOTOR_EN_VCC_PIN_MASK HS6621P_BIT_MASK(MOTOR_EN_VCC_PIN)
#define MOTOR_EN_PIN_MASK HS6621P_BIT_MASK(MOTOR_EN_PIN)
#define MOTOR_F_PIN_MASK  HS6621P_BIT_MASK(MOTOR_F_PIN)
#define MOTOR_R_PIN_MASK  HS6621P_BIT_MASK(MOTOR_R_PIN)
#define MOTO_OVP_ADC_PIN_MASK  HS6621P_BIT_MASK(MOTO_OVP_ADC_PIN)

// os_os wakeup 
#define B2B_INT_PIN       19

#ifndef B2B_INT_UART_RX
#define B2B_INT_UART_RX 0  // B2B唤醒是否使用RX引脚 0 不使用 1 使用
#endif

// os_os uart
#define OS2OS_UART_TX_PIN 5
#define OS2OS_UART_RX_PIN 6
#define B2B_INT_PIN_MASK HS6621P_BIT_MASK(B2B_INT_PIN)
#define OS2OS_UART_TX_PIN_MASK HS6621P_BIT_MASK(OS2OS_UART_TX_PIN)
#define OS2OS_UART_RX_PIN_MASK  HS6621P_BIT_MASK(OS2OS_UART_RX_PIN)

//audio 
// #define AUDIO_ONELINE_BUSY_PIN 26
// #define AUDIO_ONELINE_DATA_PIN 27
// #define AUDIO_ONELINE_BUSY_PIN_MASK HS6621P_BIT_MASK(AUDIO_ONELINE_BUSY_PIN)
// #define AUDIO_ONELINE_DATA_PIN_MASK HS6621P_BIT_MASK(AUDIO_ONELINE_DATA_PIN)

// Debug
#define LOG_UART_PIN 4
#define LOG_UART_PIN_MASK HS6621P_BIT_MASK(LOG_UART_PIN)

#define LOCK_DAI_HALL_NUM 1 //---
// #define BACK_NO_BRAKE       //--

#define UN_USED_IO_MASK (PIN_MC60_UART0_TX_MASK | PIN_MC60_UART0_RX_MASK | PIN_MCU_WAKE_BLE_MASK | PIN_BLE_WAKE_MCU_MASK \
| BUTTON_RESET_PIN_MASK | BUTTON_ONOFF_PIN_MASK | HOMEKIT_PIN_MASK | HOMEKIT_NRST_PIN_MASK | OPTO3_PIN_MASK | OPTO2_PIN_MASK \
| OPTO1_PIN_MASK | OPTO_EN_PIN_MASK | MOTOR_EN_VCC_PIN_MASK | MOTOR_EN_PIN_MASK | MOTOR_F_PIN_MASK | MOTOR_R_PIN_MASK \
| MOTO_OVP_ADC_PIN_MASK | B2B_INT_PIN_MASK |OS2OS_UART_TX_PIN_MASK | OS2OS_UART_RX_PIN_MASK | LOG_UART_PIN_MASK)

#if (B2B_INT_UART_RX)
#define PIN_MAP                                                                           \
{                                                                                         \
        {vPIN_B10, (uint32_t)BUTTON_RESET_PIN},                                           \
        {vPIN_B21, (uint32_t)OPTO1_PIN},           /* 开锁光耦 */                          \
        {vPIN_B20, (uint32_t)OPTO2_PIN},           /* 上锁光耦  */                          \
        {vPIN_B33, (uint32_t)OPTO3_PIN},           /* 上锁光耦2 */                          \
        {vPIN_C2, (uint32_t)MOTOR_EN_PIN},         /* 电机使能 */                          \
        {vPIN_C0, (uint32_t)OPTO_EN_PIN},          /* 光耦使能 */                           \
        {vPIN_C45, (uint32_t)MOTOR_EN_VCC_PIN},    /* 电机供电电压控制脚（电压高时降压）*/    \
		{vPIN_I1,  (uint32_t)OS2OS_UART_RX_PIN},      /* OS-OSWAKEUP       */               \
        {vPIN_I22, (uint32_t)PIN_BLE_WAKE_MCU},       /* BLE唤醒MCU            */            \
        {vPIN_C46, (uint32_t)PIN_MCU_WAKE_BLE},       /* MCU唤醒BLE           */             \
        {vPIN_B11,  (uint32_t)BUTTON_ONOFF_PIN},      /* 开关锁按键       */               \
        {vPIN_B34,  (uint32_t)HOMEKIT_PIN},          /* homekit按键       */               \
}
#else
#define PIN_MAP                                                                           \
{                                                                                         \
        {vPIN_B10, (uint32_t)BUTTON_RESET_PIN},                                           \
        {vPIN_B21, (uint32_t)OPTO1_PIN},           /* 开锁光耦 */                          \
        {vPIN_B20, (uint32_t)OPTO2_PIN},           /* 上锁光耦  */                          \
        {vPIN_B33, (uint32_t)OPTO3_PIN},           /* 上锁光耦2 */                          \
        {vPIN_C2, (uint32_t)MOTOR_EN_PIN},         /* 电机使能 */                          \
        {vPIN_C0, (uint32_t)OPTO_EN_PIN},          /* 光耦使能 */                           \
        {vPIN_C45, (uint32_t)MOTOR_EN_VCC_PIN},    /* 电机供电电压控制脚（电压高时降压）*/    \
		{vPIN_I1,  (uint32_t)B2B_INT_PIN},            /* OS-OSWAKEUP       */               \
        {vPIN_I22, (uint32_t)PIN_BLE_WAKE_MCU},       /* BLE唤醒MCU            */            \
        {vPIN_C46, (uint32_t)PIN_MCU_WAKE_BLE},       /* MCU唤醒BLE           */             \
        {vPIN_B11,  (uint32_t)BUTTON_ONOFF_PIN},      /* 开关锁按键       */               \
        {vPIN_B34,  (uint32_t)HOMEKIT_PIN},          /* homekit按键       */               \
}
#endif

#endif