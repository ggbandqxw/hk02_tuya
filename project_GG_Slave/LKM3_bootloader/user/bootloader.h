#ifndef __BOOT_LOADER_H__
#define __BOOT_LOADER_H__

/* App CRC32校验值长度 */
#define __APP_SIGNATURE_SIZE (4)

/* 昂瑞微flash 基地址*/
#define ONMICRO_FLASH_BASE_ADDR (0x00400000)

/* xip 基地址*/
#define ADDR_BUS_INSIDE_BASE 0x50000000
#define ADDR_BUS_OUT_BASE_SF1 0x52000000
#define ADDR_BUS_OUT_BASE_SF2 0x54000000

/* BootLoader SIZE */
#define BOOTLOADER_FLASH_SIZE (0x5000)
/* 主程序 SIZE */
#define APP_FLASH_SIZE (0x40000)

/* BootLoader 起始地址空间 */
#define BOOTLOADER_FLASH_ADDR (0x00403000)
/* 主程序 起始地址空间 */
#define APP_CODE_ADDR (BOOTLOADER_FLASH_ADDR + BOOTLOADER_FLASH_SIZE)
/* OTA 固件缓存区起始地址空间 */
#define OTA_CODE_ADDR (APP_CODE_ADDR + APP_FLASH_SIZE)

/********************************************
         固件信息位置
********************************************/
#define APP_FW_VERSION_INFOR_ADDR (OTA_CODE_ADDR - 0x100)
#define APP_HW_VERSION_INFOR_ADDR (APP_FW_VERSION_INFOR_ADDR + 0x20)
#define DEVICE_TYPE_INFOR_ADDR (APP_HW_VERSION_INFOR_ADDR + 0x20)

#endif