@echo ------------------------------------------
@echo   Post-build commands for CCM
@echo ------------------------------------------




@set RENAME=..\..\Tools\ccm_rename_log.exe
@set BOOT_DIR=..\LKM3_bootloader\hex

@set BOOT_NAME_MASTER=onm_master_bootloader
@set BOOT_NAME_SLAVE=onm_slave_bootloader

@set MASTER_BOOTSIZE=24576

@set SLAVE_BOOTSIZE=%1

@set APPSIZE=%2
@set OUTPUT_DIR=%3
@set PRJ_NAME=%4

@echo PRJ_NAME

@set HEX_TOOL=..\..\Tools\hex2bin_new.exe
%HEX_TOOL% 1 %SLAVE_BOOTSIZE% %BOOT_DIR%\%BOOT_NAME_SLAVE%.hex %OUTPUT_DIR%\%BOOT_NAME_SLAVE%_pad.bin
%HEX_TOOL% 1 %MASTER_BOOTSIZE% %BOOT_DIR%\%BOOT_NAME_MASTER%.hex %OUTPUT_DIR%\%BOOT_NAME_MASTER%_pad.bin
%HEX_TOOL% 1 %APPSIZE% %OUTPUT_DIR%\%PRJ_NAME%.hex %OUTPUT_DIR%\%PRJ_NAME%_sign_pad.bin

copy /b %OUTPUT_DIR%\%BOOT_NAME_SLAVE%_pad.bin+%OUTPUT_DIR%\%PRJ_NAME%_sign_pad.bin %OUTPUT_DIR%\slave.bin

copy /b %OUTPUT_DIR%\%BOOT_NAME_MASTER%_pad.bin+%OUTPUT_DIR%\%PRJ_NAME%_sign_pad.bin %OUTPUT_DIR%\master.bin

del %BOOT_NAME_SLAVE%_pad.bin /s
del %BOOT_NAME_MASTER%_pad.bin /s
%RENAME% %OUTPUT_DIR%\master.bin %OUTPUT_DIR%\slave.bin %OUTPUT_DIR%\%PRJ_NAME%_sign_pad.bin %OUTPUT_DIR%\
IF NOT EXIST ".\Flyme" MD ".\Flyme"
copy %OUTPUT_DIR%\*.bin  .\Flyme\*.bin
del %OUTPUT_DIR%\*.bin
del %OUTPUT_DIR%\*.hex
::.\zzkeil_kill.bat
::pause