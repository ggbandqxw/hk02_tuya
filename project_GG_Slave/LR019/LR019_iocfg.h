/**
 * @file slave_iocfg.h
 * @brief 
 * 
 * @version 0.1
 * @date 2024-01-13
 * 
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 * 
 * @note          鼎新同创・智能锁
 *  
 * @par 修改日志: 
 * <1> 2024-01-15 v0.1 huangliwu 创建初始版本
 * *************************************************************************
 */
#ifndef __LR019_IOCFG_H__
#define __LR019_IOCFG_H__

#define NOT_USE_PIN 2 //3, 7, 8, 0

#define WIFI_EN_PIN 1

#define PIN_UART0_TX   0 //5
#define PIN_UART0_RX   1 //6

#define PIN_BOOT_UART1_TX 0  // TODO:boot 串口要修改和前后版通信串口一致【OTA】
#define PIN_BOOT_UART1_RX 1

#define BUTTON_RESET_PIN    9

#define HALL2_PIN     10
#define OPTO2_PIN     11
#define OPTO1_PIN     13
#define OPTO_EN_PIN   14

#define MOTOR_EN_VCC_PIN   15
#define MOTOR_EN_PIN   16

#define MOTOR_F_PIN   17
#define MOTOR_R_PIN   18

#define MOTO_OVP_ADC_PIN   12// 

// os_os wakeup 
#define B2B_INT_PIN       19
// os_os uart
#define OS2OS_UART_TX_PIN 20
#define OS2OS_UART_RX_PIN 21

#define TOUCH1_INT_PIN NOT_USE_PIN
#define TOUCH1_RST_PIN NOT_USE_PIN
#define LED_WHIT_PIN NOT_USE_PIN
#define LED_G_PIN NOT_USE_PIN
#define LED_R_PIN NOT_USE_PIN
#define AUDIO_ONELINE_DATA_PIN NOT_USE_PIN
#define AUDIO_ONELINE_BUSY_PIN NOT_USE_PIN

//uart finger 
#define FINGER_INT_PIN   NOT_USE_PIN
#define FINGER_POWER_PIN NOT_USE_PIN
#define FG_UART_TX_PIN   NOT_USE_PIN
#define FG_UART_RX_PIN   NOT_USE_PIN

//adc.c
#define BATTERY_ADC_PIN 8
/* ADC通道的分压比例 */
#define BATTERY_VOLTAGE_SCALE 4.9

#define LOCK_DAI_HALL_NUM 1
#define BACK_NO_BRAKE

#endif
