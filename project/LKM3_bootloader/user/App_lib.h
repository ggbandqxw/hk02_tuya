#ifndef __LIB_H
#define __LIB_H
#include <stdint.h>
void *lib_mem_cpy(void *dst, const void *src, uint16_t n);
uint32_t lib_dwData_Swop(uint32_t data);
uint16_t lib_wData_Swop(uint16_t data);
void lib_wData_Swop_Copy(uint16_t *Structbuf, uint16_t *buf, uint8_t len);
uint8_t lib_sum_lb(uint8_t *buf, uint8_t n);
uint16_t lib_sum_lh(uint8_t *buf, uint8_t n);
void lib_buble_sorth(uint16_t a[], uint16_t n);
void lib_buble_sortw(uint32_t a[], uint32_t n);
void lib_b2h(uint8_t *buf, uint16_t *hbuf, uint16_t n);
void lib_h2b(uint16_t *hbuf, uint8_t *buf, uint16_t n);
uint8_t lib_cmp_id(uint8_t *id1, uint8_t *id2, uint8_t n);
uint16_t calccrc(uint8_t crcbuf, uint16_t crc);
uint16_t chkcrc(uint8_t *buf, uint16_t len);
uint8_t Bin_2_Bcd(uint8_t BinValue);
uint8_t Bcd_2_Bin(uint8_t BcdValue);
unsigned long CalcCRC32(unsigned char *s, unsigned int len, unsigned int crc_in);
#endif /* __LIB_H */
