/**
 * @file master_iocfg.h
 * @brief 
 * 
 * @version 0.1
 * @date 2024-01-13
 * 
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 * 
 * @note          鼎新同创・智能锁
 *  
 * @par 修改日志: 
 * <1> 2024-01-15 v0.1 huangliwu 创建初始版本
 * *************************************************************************
 */
#ifndef __QS23_IOCFG_H__
#define __QS23_IOCFG_H__

#define NOT_USE_PIN 2 //3, 7, 0, 1, 21, 25, 26, 27, 28, 29, 30, 31 

#define PIN_UART0_TX   0//
#define PIN_UART0_RX   1

#define PIN_BOOT_UART1_TX 0  // TODO:boot 串口要修改和前后版通信串口一致【OTA】
#define PIN_BOOT_UART1_RX 1

//adc.c
#define BATTERY_ADC_PIN 8
/* ADC通道的分压比例 */
#define BATTERY_VOLTAGE_SCALE 4.9



// os_os uart
#define OS2OS_UART_TX_PIN 5
#define OS2OS_UART_RX_PIN 6
// os_os wakeup 
#define B2B_INT_PIN 9

//USB_OK
#define USB_OK_PIN  10

//audio 
#define AUDIO_ONELINE_BUSY_PIN 11
#define AUDIO_ONELINE_DATA_PIN 12

//uart finger 
#define FINGER_INT_PIN 14 // 15
#define FINGER_POWER_PIN 13 //16
#define FG_UART_TX_PIN 16 //14
#define FG_UART_RX_PIN 15 //15

//TSM12
#define IIC_SDA_PIN 17
#define IIC_SCL_PIN 18
#define TOUCH1_INT_PIN 19
#define TOUCH1_RST_PIN 20

//LED 
#define LED_WHIT_PIN 22
#define LED_G_PIN 23
#define LED_R_PIN 24

//NOT USE 
#define HALL2_PIN      NOT_USE_PIN
#define OPTO2_PIN     NOT_USE_PIN
#define OPTO1_PIN     NOT_USE_PIN
#define OPTO_EN_PIN   NOT_USE_PIN
#define MOTOR_EN_VCC_PIN   NOT_USE_PIN
#define MOTOR_EN_PIN   NOT_USE_PIN

#define MOTOR_F_PIN   NOT_USE_PIN
#define MOTOR_R_PIN   NOT_USE_PIN


#define BUZZER_PIN          NOT_USE_PIN
#define BUTTON_RESET_PIN    NOT_USE_PIN

#endif