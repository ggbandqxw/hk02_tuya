/**
 ******************************************************************************
 * @file         application.h
 * @brief        定义应用层一些功能开关、产品型号等信息
 *               根据具体产品对功能做相应的添加、删除
 * @author       XQ
 * @date         2021-2-5
 * @version      V001
 * @copyright    CPTSTR
 *
 * @note         鼎欣同创·智能锁
 *
 ******************************************************************************
 */
#ifndef __APPLICATION_H__
#define __APPLICATION_H__
//=============================================================================
//固件版本信息，用于固件刷机使用
//=============================================================================
#define HARDWARE_VER "V001"                         //硬件版本 一旦正式发布固件后就不能更改 OTA 需要Check
#define FIRMWARE_VER "QS23_V1.0.0"                   //
#define DEV_TYPE "C1C6AD1C92D61D6AE5D33118062AEFF7" //产品PID 最大32个字符  一旦正式发布固件后就不能更改，OTA 需要Check该PID 相同才允许升级
                                                    //
#define BLE_FIRMWARE_VER 0X010107                   // v1.0.0
//=============================================================================
//通过NB模组上传到安居微堂后台的版本信息
//=============================================================================
#define FIRMWARE_VERSION 0x0117
#define HARDWARE_VERSION 0x1002

#endif /* __APPLICATION_H__ */
