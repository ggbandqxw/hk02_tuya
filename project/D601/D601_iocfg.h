/**
 * @file master_iocfg.h
 * @brief 
 * 
 * @version 0.1
 * @date 2024-01-13
 * 
 * @copyright  Copyright (c) 2020~2030 ShenZhen dingxintongchuang Technology Co., Ltd.
 * All rights reserved.
 * 
 * @note          鼎新同创・智能锁
 *  
 * @par 修改日志: 
 * <1> 2024-01-15 v0.1 huangliwu 创建初始版本
 * *************************************************************************
 */
#ifndef __D601_IOCFG_H__
#define __D601_IOCFG_H__

// HK01LD240509000002 :  484B30314C44323430353039303030303032
#define DEFAULT_SN {0x48, 0x4B, 0x30, 0x31, 0x4C, 0x44, 0x32, 0x34, 0x30, 0x35, 0x30, 0x39, 0x30, 0x30, 0x30, 0x30, 0x30, 0x32} // 0x31

//产品ID:   I1J6MY-BLE : 49314A364D592D424C45 
#define PRODUCT_ID {0x49, 0x31, 0x4A, 0x36, 0x4D, 0x59, 0x2D, 0x42, 0x4C, 0x45}

#define PIN_UART0_TX   0//
#define PIN_UART0_RX   1

#define PIN_BOOT_UART1_TX 0  // TODO:boot 串口要修改和前后版通信串口一致【OTA】
#define PIN_BOOT_UART1_RX 1

//adc.c
#define BATTERY_ADC_PIN 8
#define BATTERY_ADC_PIN_MASK HS6621P_BIT_MASK(BATTERY_ADC_PIN)
/* ADC通道的分压比例 */
#define BATTERY_VOLTAGE_SCALE 4.9



// os_os uart
#define OS2OS_UART_TX_PIN 5
#define OS2OS_UART_RX_PIN 6
#define OS2OS_UART_TX_PIN_MASK HS6621P_BIT_MASK(OS2OS_UART_TX_PIN)
#define OS2OS_UART_RX_PIN_MASK HS6621P_BIT_MASK(OS2OS_UART_RX_PIN)
// os_os wakeup 
#define B2B_INT_PIN 9
#define B2B_INT_PIN_MASK HS6621P_BIT_MASK(B2B_INT_PIN)

#ifndef B2B_INT_UART_RX
#define B2B_INT_UART_RX 0  // B2B唤醒是否使用RX引脚 0 不使用 1 使用
#endif

//USB_OK
#define USB_OK_PIN  10 //代码未做处理  输入下拉 //---
#define USB_OK_PIN_MASK HS6621P_BIT_MASK(USB_OK_PIN)

//audio 
// #define AUDIO_ONELINE_BUSY_PIN 11
// #define AUDIO_ONELINE_DATA_PIN 12

//uart finger 
#define FINGER_INT_PIN 14 // 15
#define FINGER_POWER_PIN 13 //16
#define FG_UART_TX_PIN 16 //14
#define FG_UART_RX_PIN 15 //15
#define FINGER_INT_PIN_MASK HS6621P_BIT_MASK(FINGER_INT_PIN)
#define FINGER_POWER_PIN_MASK HS6621P_BIT_MASK(FINGER_POWER_PIN)
#define FG_UART_TX_PIN_MASK HS6621P_BIT_MASK(FG_UART_TX_PIN)
#define FG_UART_RX_PIN_MASK HS6621P_BIT_MASK(FG_UART_RX_PIN)

//TSM12
#define IIC_SDA_PIN 17
#define IIC_SCL_PIN 18
#define TOUCH1_INT_PIN 19
#define TOUCH1_RST_PIN 20
#define IIC_SDA_PIN_MASK HS6621P_BIT_MASK(IIC_SDA_PIN)
#define IIC_SCL_PIN_MASK HS6621P_BIT_MASK(IIC_SCL_PIN)
#define TOUCH1_INT_PIN_MASK HS6621P_BIT_MASK(TOUCH1_INT_PIN)
#define TOUCH1_RST_PIN_MASK HS6621P_BIT_MASK(TOUCH1_RST_PIN)

//LED 
#define LED_WHIT_PIN 22
#define LED_WHIT_PIN_MASK HS6621P_BIT_MASK(LED_WHIT_PIN)

#define UN_USED_IO_MASK (BATTERY_ADC_PIN_MASK | OS2OS_UART_TX_PIN_MASK | OS2OS_UART_RX_PIN_MASK | B2B_INT_PIN_MASK | USB_OK_PIN_MASK \
| FINGER_INT_PIN_MASK | FINGER_POWER_PIN_MASK | FG_UART_TX_PIN_MASK | FG_UART_RX_PIN_MASK | IIC_SDA_PIN_MASK | IIC_SCL_PIN_MASK \
| TOUCH1_INT_PIN_MASK | TOUCH1_RST_PIN_MASK | LED_WHIT_PIN_MASK)

#if (B2B_INT_UART_RX)
#define PIN_MAP                                                                           \
{                                                                                         \
        {vPIN_I21, (uint32_t)TOUCH1_INT_PIN},      /* TSM12-INT1 */                        \
        {vPIN_C38, (uint32_t)TOUCH1_RST_PIN},      /* TOUCH-RST  */                        \
        {vPIN_L18, (uint32_t)LED_WHIT_PIN},        /* 键盘整体背光控制 */                    \
        {vPIN_I1,  (uint32_t)OS2OS_UART_RX_PIN},   /* OS-OSWAKEUP       */               \
        {vPIN_I5, (uint32_t)FINGER_INT_PIN},       /* UART FING-INT1   */                \
        {vPIN_C29, (uint32_t)FINGER_POWER_PIN},    /* UART FING-POWER  */                \
}
#else
#define PIN_MAP                                                                           \
{                                                                                         \
        {vPIN_I21, (uint32_t)TOUCH1_INT_PIN},      /* TSM12-INT1 */                        \
        {vPIN_C38, (uint32_t)TOUCH1_RST_PIN},      /* TOUCH-RST  */                        \
        {vPIN_L18, (uint32_t)LED_WHIT_PIN},        /* 键盘整体背光控制 */                    \
        {vPIN_I1,  (uint32_t)B2B_INT_PIN},            /* OS-OSWAKEUP       */               \
        {vPIN_I5, (uint32_t)FINGER_INT_PIN},          /* UART FING-INT1   */                \
        {vPIN_C29, (uint32_t)FINGER_POWER_PIN},       /* UART FING-POWER  */                \
}
#endif

#endif